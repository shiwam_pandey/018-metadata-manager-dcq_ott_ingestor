package com.accenture.avs.be.dcq.vod.ingestor.manager;

import com.accenture.avs.be.framework.cache.Configuration;

public interface CategoryIngestManager {

	/**
	 * @param mode
	 * @param indexDate
	 * @param config
	 */
	void initializeCategoryIngestor(String mode, String indexDate, Configuration config);

}
