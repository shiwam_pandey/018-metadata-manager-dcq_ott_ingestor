package com.accenture.avs.be.dcq.vod.ingestor.v1.rest.controllers;

import javax.xml.bind.JAXBException;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.bundle.asset.BundleDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.service.BundleIngestionV1Service;
import com.accenture.avs.be.framework.annotation.AvsRestController;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.cache.Configurator;
import com.accenture.avs.be.framework.configuration.CacheConstants;
import com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.commons.lib.support.CommonConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@AvsRestController
@Api(tags="Bundle Ingestion Services" ,value = "Bundle API's", description = "API's pertaining to create/update vod bundles")
public class BundleV1Controller {
	

	@Autowired
	private Configurator configurator;
	
	@Autowired
	private BundleIngestionV1Service bundleService;
	
	
	@RequestMapping(value = "/v1/bundle", method = RequestMethod.POST)
	@ApiOperation(value = "bundleIngestion", notes = "Aggregating multiple EPISODES/MOVIES into a single package as a SEASON type of content (or) aggregating multiple SEASONS into a single package as a SERIES type of content", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses({
		@ApiResponse(code=400, message="ACN_3000: Missing Parameter [param name]"
				+ "\nACN_3019: Invalid parameter [param name]" 
				+ "\nACN_3258: Parsing Json data failed"),
		@ApiResponse(code=200, message="ACN_200: OK"),
		@ApiResponse(code=500, message="ACN_300: GENERIC ERROR")
	})
	public @ResponseBody
	ResponseEntity<GenericResponse> bundleIngestion(@RequestBody BundleDTO bundle) throws ApplicationException {
		GenericResponse genericResponse = null;
		try{
		Configuration config = configurator.getConfiguration(ThreadContext.get(CommonConstants.LogParamsL4j2.TN));
		genericResponse = bundleService.bundleIngestion(bundle, config);
		}catch (ApplicationException ae) {
			throw ae;
		}catch (JAXBException e) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR, new NestedParameters(e.getMessage()));
		}
	catch (Exception e) {
			throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR);
		}
		return ResponseEntity.ok(genericResponse);
	}

}


