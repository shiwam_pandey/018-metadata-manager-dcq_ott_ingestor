package com.accenture.avs.be.dcq.vod.ingestor.service;

import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.exception.ApplicationException;

public interface EmfService {
	GenericResponse activateOrDeactivateEmf(String emfName,String status) throws ApplicationException;
}
