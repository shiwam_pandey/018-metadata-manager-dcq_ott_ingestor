package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.io.Serializable;

import com.accenture.avs.be.dcq.vod.ingestor.dto.ESVodChannelDTO.TechnicalPackagesDTO;


/**
 * @author karthik.vadla
 *
 */
public class TechnicalPackagesResultsDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private byte[] xmlFile;
	private TechnicalPackagesDTO[] techPackagesDTOs;

	public TechnicalPackagesDTO[] getTechPackagesDTOs() {
		return techPackagesDTOs;
	}

	public void setTechPackagesDTOs(TechnicalPackagesDTO[] techPackagesDTOs) {
		this.techPackagesDTOs = techPackagesDTOs;
	}

	public byte[] getXmlFile() {
		return xmlFile;
	}

	public void setXmlFile(byte[] xmlFile) {
		this.xmlFile = xmlFile;
	}

}
