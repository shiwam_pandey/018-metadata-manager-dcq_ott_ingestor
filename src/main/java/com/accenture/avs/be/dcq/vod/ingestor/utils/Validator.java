package com.accenture.avs.be.dcq.vod.ingestor.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.XMLConstants;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.commons.lang.StringUtils;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;


import com.accenture.avs.be.dcq.vod.ingestor.dto.bundle.asset.Bundle;
import com.accenture.avs.be.dcq.vod.ingestor.dto.bundle.asset.Bundle.BundleContentList;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.be.framework.exception.ConfigurationException;
import com.accenture.avs.be.framework.utils.BooleanConverter;
import com.accenture.avs.be.framework.utils.BooleanConverter.BooleanAvsAdapter;
import com.accenture.avs.be.framework.utils.Utilities;
import com.accenture.avs.persistence.technicalcatalogue.ContentEntity;


/**
 * @author karthik.vadla
 *
 */
public class Validator {

	public static void checkParameter(String parameterName, String value, Configuration config) throws ApplicationException, ConfigurationException {
		if (value == null || value.trim().isEmpty()) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER, new NestedParameters(parameterName));
		}
	}
	
	public static Integer checkParameterValue(String parameterName, String value, Configuration config) throws ApplicationException, ConfigurationException {
		if (StringUtils.isEmpty(value)||value.equalsIgnoreCase("null")) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER, new NestedParameters(parameterName));
		} 
		try {
			return Integer.parseInt(value);
		} catch (NumberFormatException e) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(parameterName));
		}
	}
	
	public static void checkPushNotificationParameter(String parameterName, String value, Configuration config) throws ApplicationException, ConfigurationException {
		if (value == null || value.trim().isEmpty()) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER, new NestedParameters(parameterName));
		}
		else if((value !=null && !value.trim().isEmpty()) && !(value.equalsIgnoreCase("set") || value.equalsIgnoreCase("remove"))){
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3020_VALIDATE_REQUEST_ERROR, new NestedParameters(parameterName));
		}
	}

	public static void checkParameter(String parameterName, Object value, Configuration config) throws ApplicationException, ConfigurationException {
		if (value == null) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER, new NestedParameters(parameterName));
		}
		if (value instanceof Collection<?>) {
			if (((Collection<?>) value).isEmpty()) {
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER, new NestedParameters(parameterName));
			}
		}
	}
	
	public static void checkParameter(String parameterName, List<Object> value, Configuration config) throws ApplicationException, ConfigurationException {
		if (value == null || value.isEmpty()) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(parameterName));
		}
	}
	
	public static void checkParameterLength(String value, int maxLength, Configuration config) throws ApplicationException, ConfigurationException {
		if (value == null) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER);
		} else if (value.length() > maxLength) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER + " Check parameter length");
		}
	}

	public static void checkParameterNameLength(String parameterName, String value, int maxLength, Configuration config) throws ApplicationException, ConfigurationException {
		if (value == null) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER, new NestedParameters(parameterName));
		} else if (value.length() > maxLength) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(parameterName));
		}
	}
	public static void checkParameterValidityRange(String parameterName, String[] validRange, String value, Configuration config) throws ApplicationException,
			ConfigurationException {
		for (int i = 0; i < validRange.length; i++)
			if (validRange[i].equalsIgnoreCase(value)) {
				return;}
		throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(parameterName));
	}

	public static void checkParameterValidityRangeCaseSensitive(String parameterName, String[] validRange, String value,
			Configuration config) throws ApplicationException, ConfigurationException {
		for (int i = 0; i < validRange.length; i++)
			if (validRange[i].equals(value)) {
				return;}
		throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(parameterName));
	}
	
	public static void checkParameterValidityRange(String[] validRange, String value, Configuration config) throws ApplicationException, ConfigurationException {
		checkParameterValidityRange(null, validRange, value, config);
	}

	public static <T> void checkParameterValidityRange(String parameterName, T[] validRange, T value, Configuration config) throws ApplicationException,
			ConfigurationException {
		for (int i = 0; i < validRange.length; i++) {
			if (validRange[i].equals(value)) {
				return;
			}
		}
		throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(parameterName));
	}

	public static void checkParameterValidityRange(String parameterName, String[] validRange, String[] values, Configuration config) throws ApplicationException,
			ConfigurationException {
		if (Arrays.asList(validRange).containsAll(Arrays.asList(values))) {
			return;}
		throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(parameterName));
	}

	public static boolean checkDateValidityFormat(String parameterName, SimpleDateFormat format, String date, Configuration config) throws ApplicationException,
			ConfigurationException {
		try {
			format.parse(date);
			return true;
		} catch (Exception e) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(parameterName));
		}
	}

	public static Long validateLongParameter(String parameterName, String parameterValue, Configuration config) throws ApplicationException, ConfigurationException {
		if (StringUtils.isEmpty(parameterValue)) {
			return null;
		}
		try {
			return Long.parseLong(parameterValue);
		} catch (NumberFormatException e) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(parameterName));
		}
	}

	public static Integer validateIntParameter(String parameterName, String parameterValue, Configuration config) throws ApplicationException,
			ConfigurationException {
		if (StringUtils.isEmpty(parameterValue)) {
			return null;
		}
		try {
			return Integer.parseInt(parameterValue);
		} catch (NumberFormatException e) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(parameterName));
		}
	}
	
	public static Integer validateIntNotNegativeParameter(String parameterName, String parameterValue, Configuration config) throws ApplicationException,
  		ConfigurationException {
    Integer valueInt = null;
		if (!StringUtils.isEmpty(parameterValue)) {
			valueInt = Validator.validateIntParameter(parameterName, parameterValue, config);
			if ( valueInt != null && valueInt.intValue() < 0 ) {
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(parameterName));
			}
		}
		return valueInt;
	}
	
	public static Integer validateIntParameterForUpcoming(String parameterName, String parameterValue, Configuration config) throws ApplicationException,
	ConfigurationException {
		try {
			return Integer.parseInt(parameterValue);
		} catch (NumberFormatException e) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(parameterName));
		}
	}
	
	

	public static void checkParameterFormat(String parameterName, String value, String validPattern, Configuration config) throws ApplicationException,
			ConfigurationException {
		if (!value.matches(validPattern)) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(parameterName));
		}
	}

	public static void checkPinFormat(String parameterName, String value, Configuration config) throws ApplicationException, ConfigurationException {
		final String PIN_FORMAT = "^[0-9]{4}$";
		if (!value.matches(PIN_FORMAT)) {
			throw new ApplicationException(MessageKeys.ERROR_BE_3219_WRONG_PIN_FORMAT, new NestedParameters(parameterName));

		}
	}
	
	public static void sanitizingParams(String parameterName, String parameterValue) throws ApplicationException {
		if(!Utilities.isEmpty(parameterValue) && !parameterValue.matches("[\\w*\\s*]*")) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(parameterName + DcqVodIngestorConstants.SANITIZE_PARAMS));
		}
	}

	public static boolean validateBooleanParameter(String parameterName, String value, Configuration config) throws ApplicationException, ConfigurationException {
		boolean check = true;
		for (BooleanAvsAdapter bool : BooleanConverter.BooleanAvsAdapter.values()) {
			if (bool.getValue().equalsIgnoreCase(value)) {
				check = false;
				break;
			}
		}
		if (check) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(parameterName));
		}
		return BooleanConverter.getBooleanValue(value);
	}
	
/*	public static boolean validateByteParameter(String parameterName, String value, Configuration config) throws ApplicationException, ConfigurationException {
		boolean check = false;
		try{
			for (BooleanAvsAdapter bool : BooleanConverter.BooleanAvsAdapter.values()) {
				if (bool.getValue().equals(Byte.valueOf(value))) {
					check = true;
					break;
				}
			}
			if (!check) {
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER) + " " + parameterName);
			}
		} catch (NumberFormatException e) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER) + " " + parameterName);
		}
		return BooleanConverter.getBooleanValue(Byte.valueOf(value));
	}*/
	
	public static void checkAtLeastOneParameterValorized(List<String> paramList, Configuration config) throws ApplicationException, ConfigurationException{
		if (paramList != null){
			for (String string : paramList) {
				if (!StringUtils.isEmpty(string)){
					return;
				}
			}
		}
		throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER + " All parameters");
	}
	
	public static void validateEmailFormat(String parameterName, String value, String validPattern, Configuration config) throws ApplicationException, ConfigurationException {
		Pattern pattern = Pattern.compile(String.valueOf(validPattern.trim()));
		Matcher matcher = pattern.matcher(String.valueOf(value.trim()));
	
		if (!matcher.matches()) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(parameterName));
		}
	}
	
	public static void validateParameterPattern(String parameterName, String value, String validPattern, Configuration config) throws ApplicationException, ConfigurationException {
		Pattern pattern = Pattern.compile(String.valueOf(validPattern.trim()));
		Matcher matcher = pattern.matcher(String.valueOf(value.trim()));
	
		if (!matcher.matches()) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(parameterName));
		}
	}
	
	public static void validateParameterPattern(String parameterName, String value, String validPattern, Configuration config, String messageKey) throws ApplicationException, ConfigurationException {
		Pattern pattern = Pattern.compile(String.valueOf(validPattern.trim()));
		Matcher matcher = pattern.matcher(String.valueOf(value.trim()));
	
		if (!matcher.matches()) {
			throw new ApplicationException(messageKey, new NestedParameters(parameterName));
		}
	}
	
	public static boolean isParamValid(String[] validRange, String value, Configuration config) throws ApplicationException, ConfigurationException {
		for (int i = 0; i < validRange.length; i++)
			if (validRange[i].equalsIgnoreCase(value)) {
				return true;}
		return false;
	}
	
	public static void validateUsernameParameter(String parameterName, String username, String validPattern, Configuration config) throws ApplicationException, ConfigurationException {
		if(!username.matches(validPattern)) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(parameterName));
		}
		
	}
	
	public static void validateStartTimeParameter(String parameterName, String parameterValue, Configuration config) throws ApplicationException, ConfigurationException  {
		try {
			long result = Long.parseLong(parameterValue) * 1000L; 
            if (result < Long.parseLong(config.getConstants().getValue(com.accenture.avs.be.dcq.vod.ingestor.utils.CacheConstants.START_TIME_LIMIT)) ){
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(parameterName));
			}
		} catch (NumberFormatException e) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(parameterName));
		}
	}
	public static Boolean validateBooleanEnabledParameter(String parameterName, String parameterValue, Configuration config) throws ApplicationException, ConfigurationException{
		
			if(parameterValue != null && parameterValue.equalsIgnoreCase("Y")){
				return true;
			}
			else{
				return false;
			}
		
	}
	
	public static void checkAtleastFolderIDFolderTypeParametersValorized(List<String> paramList, Configuration config) throws ApplicationException, ConfigurationException{
		if (paramList != null){
			for (String string : paramList) {
				if (!StringUtils.isEmpty(string)){
					return;
				}
			}
		}
		throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER + "Atleast folderID or folderType should be provided ");
	}
	
	public static void checkLanguagecodeParameterLength(String value, Configuration config) throws ApplicationException, ConfigurationException {
		 if (!Utilities.isEmpty(value) && value.length() != 3) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_8050_LANGUAGE_INVALID);
		}
	}
	
	
	public static void checkParameterRangeMinMax(String paramName, Integer value, Integer minValue, Integer maxValue, Configuration config) throws ApplicationException, ConfigurationException {
		Validator.checkParameter(paramName, value, config);
		if (minValue != null && value < minValue || maxValue != null && value > maxValue) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(paramName));
		}
	}
	
	public static Long validateLongNotNegativeParameter(String parameterName, String parameterValue, Configuration config) throws ApplicationException,
		ConfigurationException {
			Long valueLong = null;
	if (!StringUtils.isEmpty(parameterValue)) {
		valueLong = Validator.validateLongParameter(parameterName, parameterValue, config);
		if ( valueLong != null && valueLong.longValue() < 0 ) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(parameterName));
		}
	}
	return valueLong;
}
	public static void validateCancellationParameter(String parameterName, String value, Configuration config) throws ApplicationException, ConfigurationException {
		if (!Utilities.isEmpty(value)) {
			String cancellationReasons = config.getConstants().getValue(com.accenture.avs.be.dcq.vod.ingestor.utils.CacheConstants.CANCELLATION_REASON_LIST);
			if(null != cancellationReasons) {
				List<String> cancellationReasonList = Arrays.asList(cancellationReasons.split(","));
				if(!cancellationReasonList.contains(value)) {
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(parameterName));
				}
			}
		}
	}
	
	public static void validateTerminationReasonParameter(String parameterName, String value, Configuration config) throws ApplicationException, ConfigurationException {
		if (!Utilities.isEmpty(value)) {
			String terminationReasons = config.getConstants().getValue(com.accenture.avs.be.dcq.vod.ingestor.utils.CacheConstants.TERMINATION_REASON_LIST);
			if(null != terminationReasons) {
				List<String> terminationReasonsList = Arrays.asList(terminationReasons.split(","));
				if(!terminationReasonsList.contains(value)) {
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(parameterName));
				}
			}
		}
	}
	public static void checkParameter(String parameterName, Boolean value, Configuration config) throws ApplicationException, ConfigurationException {
		if (value == null) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER, new NestedParameters(parameterName));
		}
	}
	
	public static void checkArrayNumber(String arrayNumbers, Configuration config) throws ApplicationException, ConfigurationException {
		if (!arrayNumbers.matches("^[0-9,]+$")) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER);
		}
	}
	
	public static void checkArrayNumber(String parameterName, String arrayNumbers, Configuration config) throws ApplicationException, ConfigurationException {
		if (!arrayNumbers.matches("^[0-9,]+$")) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(parameterName));
		}
	}
	
	public static void checkArrayString(String parameterName, String arrayNumbers, Configuration config) throws ApplicationException, ConfigurationException {
		if (!arrayNumbers.matches("^[a-zA-Z,]+$")) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(parameterName));
		}
	}
	public static void checkAtLeastOneParameterPassed(List<String> paramList, Configuration config) throws ApplicationException, ConfigurationException{
		if (paramList != null){
			for (int i=0 ; i<paramList.size(); i++) {
				if (!StringUtils.isEmpty(paramList.get(i))){
					return;
				}
			}
		}
		throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER + ". Provide atleast one of the mandatory params: paymentMethod or monthlyBillingDay or weeklyBillingDay ");
	}
	
	public static void weeklyBillingCheck(String paramName, String paramValue, Configuration config) throws ApplicationException, ConfigurationException{
		List<String> weeks = new ArrayList<String>();
		weeks.add("Sunday");
		weeks.add("Monday");
		weeks.add("Tuesday");
		weeks.add("Wednesday");
		weeks.add("Thursday");
		weeks.add("Friday");
		weeks.add("Saturday");
		if (null != paramValue) {
			if (!weeks.contains(paramValue)) {
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(paramName));
			}
		}
		
	}
	
	public static void monthlyBillingCheck(String paramName, Integer paramValue, Configuration config) throws ApplicationException,ConfigurationException{
		
		 if ( null!= paramValue && (paramValue<1 || paramValue>28) ) {
			 throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(paramName));
		}

	}
	/**
	 * 
	 * @param xsdPath
	 * @param xmlPath
	 * @return boolean
	 * @throws Exception
	 * @throws IOException
	 */
	public static boolean validateXMLSchema(InputStream xsd, String xmlPath) throws Exception {
		try {
			SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Source source = new StreamSource(xsd);
			Schema schema = factory.newSchema(source);
			javax.xml.validation.Validator validator = schema.newValidator();
			validator.validate(new StreamSource(new StringReader(xmlPath)));
		} 
		catch (SAXParseException ex) {
			//log.logMessage("SAX Exception: {}", ex.getMessage());
			throw ex;
		}catch (SAXException ex) {
			//log.logMessage("SAX Exception: {}", ex.getMessage());
			throw ex;
		} 
		catch (Exception e) {
			//log.logMessage("Exception: {}", e.getMessage());
			throw e;
		}
		return true;
	}
	
	public static boolean isEmpty(Object s) {
		return (s == null);
	}
	public static <T> boolean isEmptyCollection(Collection<T> s) {
		return (s == null || s.isEmpty());
	}
	
	public static String list2String(List<String> list, String separator, int maxCharNum)
	{
		StringBuffer resultStr = new StringBuffer();
		Iterator<String> iterator = list.iterator();
		int resultStrLenght = -1;
		if(iterator.hasNext())
		{
			String firstElement = iterator.next();
			if( (( resultStrLenght + firstElement.length() + separator.length() ) <= maxCharNum))	{			
				resultStr.append(firstElement);}
		}else
			return "";
		
		
		for (;iterator.hasNext();) 
		{
			resultStrLenght = resultStr.length();			
			String element = iterator.next();
			if( (( resultStrLenght + element.length() + separator.length() ) > maxCharNum)) {
				break;}
			
			resultStr.append(separator+element);
		}
		
		return resultStr.toString();
	}
	
	
	public static Timestamp xmlGregorianCalendar2DateTime(
			XMLGregorianCalendar endTime) throws ParseException {
		
		
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		  Date curDate = new Date();
		  
		  String curDateString = formatter.format(curDate);
		  String tempDate = curDateString + " " + endTime;
		  
		  DateFormat formatterDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		  Date curDateTime = formatterDateTime.parse(tempDate);
		return new Timestamp(curDateTime.getTime());
	}
	public static Timestamp xmlGregorianCalendar2Date(XMLGregorianCalendar xGCal)
	{
		long dateInMillis = (xGCal.toGregorianCalendar()).getTimeInMillis();
		return new Timestamp(dateInMillis);
	}
	public static XMLGregorianCalendar Date2XmlGregorianCalendar(Timestamp tmstp) throws DatatypeConfigurationException
	{
		Date date = new Date();
		date.setTime(tmstp.getTime());
		
		GregorianCalendar c = new GregorianCalendar();
		c.setTime(date);
	
		XMLGregorianCalendar date2;
		date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

		return date2;
	}
	public static XMLGregorianCalendar Date2XmlGregorianCalendar(Date tmstp) throws DatatypeConfigurationException
	{
		Date date = new Date();
		date.setTime(tmstp.getTime());
		
		GregorianCalendar c = new GregorianCalendar();
		c.setTime(date);
	
		XMLGregorianCalendar date2;
		date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

		return date2;
	}

	public static void validateBundle(Bundle bundle, ContentEntity bundleContent, Configuration config) throws ApplicationException {
		// TODO Auto-generated method stub
		if(bundleContent == null){
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters("Bundle does not exist"));
		}
		// Validate if the contents in xml are present in the DB, if present return
		// true, else return false
		boolean contentCheckFlag = true;
		String contentName =null; 
		for (BundleContentList.ExternalContentId externalContentId : bundle.getBundleContentList().getExternalContentId()) {
			contentCheckFlag = bundleContent != null;
			if (!contentCheckFlag) {
				contentName = externalContentId.getValue();
				break;
			}
		}
		if(!contentCheckFlag){
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters(contentName + " Content of Bundle does not exist"));
		}
		// Set<String> contentDeviceTypeListFromDB = getContentblacklistDevices(bundle);
		// List<String> bundleDeviceTypeListFromXML= new ArrayList<String>();
		if (bundle.getProductInfo() != null) {
			if (bundle.getBundleId() == null) {
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
						new NestedParameters("Bundle Id and Product Info both can be null or it should have value"));
			}
		}
	}
	
	// validating whether Blacklisted device types are consistent
	public static String getAppropriateError(ContentEntity content) {
		String error = "Bundle device types not consistent with contained content items";
		if (content.getExtendedContentAttribute().getObjectType() == null
				|| content.getExtendedContentAttribute().getObjectType().equalsIgnoreCase("Bundle")) {
			error = "Bundle device types not consistent with contained content items";}
		if (content.getExtendedContentAttribute().getObjectType().equalsIgnoreCase("Group_Of_Bundle")) {
			error = "Group Of Bundle device types not consistent with contained bundle items";}
		return error;
	}

	// validating whether Blacklist device types section is available
	public static String getAppropriateMandatoryError(ContentEntity content) {
		String error = "Bundle BlackList deviceTypes section is mandatory";
		if (content.getExtendedContentAttribute().getObjectType() == null
				|| content.getExtendedContentAttribute().getObjectType().equalsIgnoreCase("Bundle")) {
			error = "Bundle BlackList deviceTypes section is mandatory ";}
		if (content.getExtendedContentAttribute().getObjectType().equalsIgnoreCase("Group_Of_Bundle")) {
			error = "Group Of Bundle BlackList deviceTypes section is mandatory ";}
		return error;
	}
	
	public static void isValidUrl(String url, String message) throws ApplicationException 
    { 
        /* Try creating a valid URL */
		try {
			if (!DcqVodIngestorUtils.isEmpty(url)) {
				String isPanic = System.getenv("EXTENSION_CHECK_PANIC");
				String allowedRegex = System.getenv("ALLOWED_CHARACTERS") + "*" + System.getenv("ALLOWED_CHARACTERS");
				String extensionRegex = System.getenv("ALLOWED_EXTENSION");

				boolean flag = false;
				flag = IsMatch(url, allowedRegex);
				if (flag && isPanic.equalsIgnoreCase("N")) {
					int p = url.lastIndexOf(".");
					if (p > 0) {
						String e = url.substring(p + 1);
						flag = IsMatch(e, extensionRegex);
					}
				}
				if (!flag) {
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters(message));
				}
			}
			
		}
        // If there was an Exception 
        // while creating URL object 
        catch (Exception e) { 
        	throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(message));
            
        } 
    }
	
	private static boolean IsMatch(String s, String pattern) {
        try {
            Pattern patt = Pattern.compile(pattern);
            Matcher matcher = patt.matcher(s);
            return matcher.matches();
        } catch (RuntimeException e) {
        return false;
    }
        
        
}
	public static void sanitizingParamsForSpecialParams(String parameterName, String parameterValue)
			throws ApplicationException {
		if (!Utilities.isEmpty(parameterValue) && !parameterValue.matches("[\\w*\\s*\\.*\\,*\\-*\\:*\\/*\\%*\\|*]*")) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters(parameterName + DcqVodIngestorConstants.SANITIZE_PARAMS));
		}
	}
	
}