
package com.accenture.avs.be.dcq.vod.ingestor.content.dto;

import java.io.Serializable;


public class GenericResponse implements Serializable
{

    /**
     * 
     * (Required)
     * 
     */
    private String resultCode;
    /**
     * 
     * (Required)
     * 
     */
    private String resultDescription;
    /**
     * 
     * (Required)
     * 
     */
    private Integer executionTime;
    private Object resultObj;
    private final static long serialVersionUID = -7969816461738518082L;

    /**
     * 
     * (Required)
     * 
     * @return
     *     The resultCode
     */
    public String getResultCode() {
        return resultCode;
    }

    /**
     * 
     * (Required)
     * 
     * @param resultCode
     *     The resultCode
     */
    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The resultDescription
     */
    public String getResultDescription() {
        return resultDescription;
    }

    /**
     * 
     * (Required)
     * 
     * @param resultDescription
     *     The resultDescription
     */
    public void setResultDescription(String resultDescription) {
        this.resultDescription = resultDescription;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The executionTime
     */
    public Integer getExecutionTime() {
        return executionTime;
    }

    /**
     * 
     * (Required)
     * 
     * @param executionTime
     *     The executionTime
     */
    public void setExecutionTime(Integer executionTime) {
        this.executionTime = executionTime;
    }

    /**
     * 
     * @return
     *     The resultObj
     */
    public Object getResultObj() {
        return resultObj;
    }

    /**
     * 
     * @param resultObj
     *     The resultObj
     */
    public void setResultObj(Object resultObj) {
        this.resultObj = resultObj;
    }

}
