package com.accenture.avs.be.dcq.vod.ingestor.config;


import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@ConfigurationProperties(prefix = "config.technicalcatalogue.datasource")
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "technicalcatalogueEntityManagerFactory",
        transactionManagerRef = "technicalcatalogueTransactionManager",
        basePackages = "com.accenture.avs.*")
public class DcqVodIngestorDatasourceConfig extends HikariConfig{

	public static final String PU_DATASOURCE_NAME = "technical_catalogueDS";
	
	@Bean(name = "technicalcatalogueDataSource")
    @Primary
    public DataSource dataSource() {
        return new HikariDataSource(this);
    }
 
    @Bean(name = "technicalcatalogueEntityManagerFactory")
    @Primary
    public LocalContainerEntityManagerFactoryBean technicalcatalogueEntityManagerFactory(@Qualifier("technicalcatalogueDataSource") final DataSource dataSource) {
        final LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setJpaVendorAdapter(this.vendorAdaptor());
        entityManagerFactoryBean.setDataSource(dataSource);
        entityManagerFactoryBean.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        entityManagerFactoryBean.setPersistenceUnitName(PU_DATASOURCE_NAME);
        entityManagerFactoryBean.setPackagesToScan("com.accenture.avs.*");
        entityManagerFactoryBean.setJpaProperties(this.jpaHibernateProperties());
        entityManagerFactoryBean.afterPropertiesSet();
        return entityManagerFactoryBean;
    }
 
    
    @Bean(name = "technicalcatalogueTransactionManager")
    @Primary
    public PlatformTransactionManager technicalcatalogueTransactionManager( @Qualifier("technicalcatalogueEntityManagerFactory") final EntityManagerFactory emf) {
        return new JpaTransactionManager(emf);
    }
 
    private HibernateJpaVendorAdapter vendorAdaptor() {
        final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        // put all the adapter properties here, such as show sql
        return vendorAdapter;
    }
 
    private Properties jpaHibernateProperties() {
        final Properties properties = new Properties();
        properties.setProperty("hibernate.format_sql", "false");
        properties.setProperty("hibernate.show_sql", "false");
        properties.setProperty("hibernate.enable_lazy_load_no_trans", "true");
        properties.setProperty("hibernate.id.new_generator_mappings","false");
        properties.setProperty("hibernate.connection.autocommit","false");
        return properties;
    }
		
}
