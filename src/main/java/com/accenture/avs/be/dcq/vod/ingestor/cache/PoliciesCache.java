package com.accenture.avs.be.dcq.vod.ingestor.cache;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.content.dto.Policies;
import com.accenture.avs.be.dcq.vod.ingestor.content.dto.Policy;
import com.accenture.avs.be.dcq.vod.ingestor.utils.CacheConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorRestUrls;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.JsonUtils;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.utils.Utilities;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.commons.lib.LoggerWrapper.OtherSystemCallType;
import com.accenture.avs.commons.lib.httpclient.HttpClientAdapter;
import com.accenture.avs.commons.lib.httpclient.HttpClientAdapter.HttpClientAdapterConfiguration;
import com.accenture.avs.commons.lib.httpclient.HttpClientAdapter.HttpClientException;

/**
 * @author karthik.vadla
 *
 */
@Component
public class PoliciesCache {

	private static final LoggerWrapper log = new LoggerWrapper(PoliciesCache.class);
	
	
	@Autowired
	private DcqVodIngestorRestUrls dcqVodIngestorRestUrl;
	private Map<String, String> policyCache;
	/**
	 * @param concurrentStreamInPanic
	 * @param concurrentStreamUrl
	 * @return
	 * @throws ApplicationException 
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	public Map<String, String> loadPolicies(Configuration config) throws ApplicationException, JsonParseException, JsonMappingException, IOException{
		long startTime = new Date().getTime();
		GenericResponse genericResponse = null;
		try{
			if(config.getConstants().getValue(CacheConstants.CONCURRENT_STREAM_IN_PANIC).equalsIgnoreCase("N")){
				log.logMessage("Loading policy cache");
				String concurrentStreamUrl = dcqVodIngestorRestUrl.CONCURRENT_STREAM_MS_URL;
				Policies policyResponse;
				policyCache =new HashMap<String, String>();
				ObjectMapper mapper = new ObjectMapper();
				Map<String, String> paramsMap = new HashMap<String, String>();
				paramsMap.put("ruleType", "content,contentOutOfHome");
				log.logCallToOtherSystemRequestBody(DcqVodIngestorConstants.CONCURRENT_STREAM, DcqVodIngestorConstants.POLICIES, "",
						OtherSystemCallType.INTERNAL);
				HttpClientAdapterConfiguration httpConfig = DcqVodIngestorUtils.getHttpClientAdapterConfiguration("tenantDefault", config);
				String response = HttpClientAdapter.doGet(httpConfig, concurrentStreamUrl, paramsMap, null, null);
				log.logCallToOtherSystemResponseBody(DcqVodIngestorConstants.CONCURRENT_STREAM, DcqVodIngestorConstants.POLICIES, response,
						OtherSystemCallType.INTERNAL);
				
				JsonNode jsonNode = mapper.readTree(response);
				policyResponse = mapper.readValue(jsonNode.findValue("resultObj"), Policies.class);
				for(Policy policy : policyResponse.getPolicies()){
					policyCache.put(policy.getPolicyId(), policy.getPolicyName());
				}
			}else{
				log.logMessage("ConcurrentStream in Panic");
			}
		
		}catch (HttpClientException hce) {
			log.logError(hce);
			genericResponse = JsonUtils.parseJson( hce.getResponseBody(),GenericResponse.class);
			String clientResponse = hce.getResponseBody();
			if((HttpStatus.SERVICE_UNAVAILABLE.value() == hce.getResponseCode())
					||(HttpStatus.NOT_FOUND.value()== hce.getResponseCode() && Utilities.isEmpty(clientResponse))
					||(HttpStatus.NOT_FOUND.value()== hce.getResponseCode() && clientResponse.startsWith("<html>"))) {
				policyCache = null;
			}
		} catch (Exception ex) {
			log.logError(ex);
		}finally {		
				DcqVodIngestorUtils.logExternalSystemEnd(genericResponse, DcqVodIngestorConstants.CONCURRENT_STREAM, DcqVodIngestorConstants.POLICIES, 
						null,startTime, OtherSystemCallType.INTERNAL, log);

			
		}
		return policyCache;
	}
	
	/**
	 * @return Policy cache
	 */
	public Map<String, String> getPolicies(){
		return policyCache;
	}
}
