package com.accenture.avs.be.dcq.vod.ingestor.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.dto.LanguageDTO;
import com.accenture.avs.be.dcq.vod.ingestor.manager.LanguageManager;
import com.accenture.avs.be.dcq.vod.ingestor.repository.LanguageRepository;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.configuration.CacheConstants;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.persistence.technicalcatalogue.LanguageMetadataEntity;

@Component
public class LanguageManagerImpl implements LanguageManager
{

	@Autowired
	private LanguageRepository languageRepository;
	
	@Override
	public GenericResponse<LanguageDTO> activateOrDeactivateLanguage(String status, String langCode) throws ApplicationException {
		// TODO Auto-generated method stub
		GenericResponse<LanguageDTO> genericResponse = null;
		LanguageMetadataEntity languageMetadataEntity=languageRepository.retrieveByLanguageCode(langCode);
		if (languageMetadataEntity==null) {
			throw new ApplicationException(
					CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters(DcqVodIngestorConstants.LANGUAGECONSTANTS.LANGUAGECODE+" does not exists"));
		}
		LanguageDTO langDto = new LanguageDTO();
		languageMetadataEntity.setIsActive(status.charAt(0));
		
		langDto.setLanguageId(languageMetadataEntity.getLanguageID());
		langDto.setLanguageCode(languageMetadataEntity.getLanguageCode());
		langDto.setLanguageName(languageMetadataEntity.getLanguageName());
		langDto.setIsDefault(languageMetadataEntity.getIsDefault());
		langDto.setIsActive(languageMetadataEntity.getIsActive());
		
		
		languageRepository.save(languageMetadataEntity);
		genericResponse = new GenericResponse<>(DcqVodIngestorConstants.ACN_200, DcqVodIngestorConstants.OK,
				langDto);

		
		return genericResponse;
	}
	

}
