package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.io.Serializable;

/**
 * @author karthik.vadla
 *
 */
public class BundleDetailsDTO implements Serializable{
	private static final long serialVersionUID = -2681353497942819024L;
	private Integer cpId;
	private Integer bundleContentId;
	private Integer position;
	private String objectType;
	private String objectSubtype;
	
	public BundleDetailsDTO(Integer cpId,Integer bundleContentId, Integer position,String objectType,String objectSubtype) {
		super();
		this.cpId=cpId;
		this.bundleContentId = bundleContentId;
		this.position = position;
		this.objectType = objectType;
		this.objectSubtype = objectSubtype;
	}

	public BundleDetailsDTO() {

	}

	public Integer getCpId() {
		return cpId;
	}

	public void setCpId(Integer cpId) {
		this.cpId = cpId;
	}

	public Integer getBundleContentId() {
		return bundleContentId;
	}

	public void setBundleContentId(Integer bundleContentId) {
		this.bundleContentId = bundleContentId;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public String getObjectType() {
		return objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public String getObjectSubtype() {
		return objectSubtype;
	}

	public void setObjectSubtype(String objectSubtype) {
		this.objectSubtype = objectSubtype;
	}


}
