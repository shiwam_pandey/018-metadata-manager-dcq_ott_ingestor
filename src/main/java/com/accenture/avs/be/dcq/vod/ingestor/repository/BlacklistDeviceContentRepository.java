package com.accenture.avs.be.dcq.vod.ingestor.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.avs.persistence.technicalcatalogue.BlacklistDeviceEntity;
/**
 * @author karthik.vadla
 *
 */
@Repository
public interface BlacklistDeviceContentRepository extends JpaRepository<BlacklistDeviceEntity,Integer>{

	@Modifying
	@Transactional
	void deleteByContentId(@Param("contentId")Integer contentId);
	
	List<Long> getDeviceTypeIdByContentId(@Param("contentId")Integer contentId);

}
