package com.accenture.avs.be.dcq.vod.ingestor.rest.controllers;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.avs.be.dcq.vod.ingestor.content.dto.VlcContentPlaylist;
import com.accenture.avs.be.dcq.vod.ingestor.dto.Response;
import com.accenture.avs.be.dcq.vod.ingestor.service.VlcContentService;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.framework.annotation.AvsRestController;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.cache.Configurator;
import com.accenture.avs.be.framework.configuration.CacheConstants;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.commons.lib.support.CommonConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@AvsRestController
@Api(tags="Vlc Content Ingestion Services" ,value = "Vlc Content Ingestion API's", description = "API's pertaining to vlc content ingestion services")
public class VlcContentController {
	
	private static final LoggerWrapper log = new LoggerWrapper(VlcContentController.class);

	@Autowired
private	Configurator configurator;
	
	@Autowired
	private VlcContentService vlcContentService;
	
	@Deprecated
	@RequestMapping(value = "/vlc/{channelId}/{vlcPublishedDate}", method = RequestMethod.POST)
	@ApiOperation(value = "updateVlcContent", notes = "Please find the page for the IFA: https://fleet.alm.accenture.com/avswiki/display/AVSM3DOC/.DCQ+VOD+Ingestor-XML+v6.5")
	public @ResponseBody
	ResponseEntity<GenericResponse> updateVlcContent(@PathVariable Integer channelId,@PathVariable String vlcPublishedDate,@RequestBody(required = false) String csvVlcContent,			
			@RequestParam(required = false,defaultValue="true") boolean sync, @RequestParam(required = false,defaultValue="false") boolean defaultVlc) throws ApplicationException {
		Response response = new Response();
		Configuration config = configurator.getConfiguration(ThreadContext.get(CommonConstants.LogParamsL4j2.TN));
		log.logStartApi();
		try {
			vlcContentService.updateVlcContent(channelId, vlcPublishedDate, csvVlcContent, sync, defaultVlc,ThreadContext.get(CommonConstants.LogParamsL4j2.TN), config);
		} catch (ApplicationException ae) {
			if(ae.getExceptionId().contains("3019")||ae.getExceptionId().contains("3000")) {
				
				if(ae.getParameters().getParameters()[0]!=null && ae.getParameters().getParameters()[0].contains("sdp")) {
					response.setResultCode(DcqVodIngestorConstants.KO);
					response.setErrorCode("ERR008");
					response.setErrorDescription(ae.getParameters().getParameters()[0]);
					return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
				}else {
					response.setResultCode(DcqVodIngestorConstants.KO);
					response.setErrorCode("ERR001-VALIDATION");
					response.setErrorDescription("Invalid Parameter "+ae.getParameters().getParameters()[0]);
					return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
				}
			}
		}
		catch (Exception e) {
			response.setResultCode(DcqVodIngestorConstants.KO);
			response.setErrorCode("ERR004-GENERIC");
			response.setErrorDescription(e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			}
		return ResponseEntity.ok(response);
	}

	@ApiOperation(value = "upsertVlcContent", notes = "This service is to ingest a VLC content")
	@RequestMapping(value="/v1/vlccontent",method = RequestMethod.POST,consumes=DcqVodIngestorConstants.APPLICATION_JASON)
	public @ApiResponses({
		@ApiResponse(code = 400, message = "ACN_3000: Missing Parameter - Mandatory parameters not provided\nACN_3019: Invalid parameter - Invalid input parameters passed\nACN_3258: Parsing JSON data failed - Input JSON is not well formed or have some syntax issues"),
		@ApiResponse(code = 200, message = "ACN_200: OK - Request processed successfully"),
		@ApiResponse(code = 500, message = "ACN_300: GENERIC ERROR - Some unexpected errors while processing requests like database failure etc.,")})
		@ResponseBody ResponseEntity<GenericResponse> updateVlcContent(@ApiParam(value="request for the VlcContent Ingestion",required = true) @RequestBody(required=true) VlcContentPlaylist vlcPlayList) throws ApplicationException{
		GenericResponse genericResponse = null;
		long startTime = System.currentTimeMillis();
		Configuration config = configurator.getConfiguration(ThreadContext.get(CommonConstants.LogParamsL4j2.TN));
		log.logStartApi();
		try {
			genericResponse = vlcContentService.updateVlcContent(vlcPlayList, config, ThreadContext.get(CommonConstants.LogParamsL4j2.TN));
		}
		catch(ApplicationException e) {
			log.logError(e);
			throw e;
		}
		catch(Exception e) {
			log.logError(e);
			throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR);
		}
		log.logMethodEnd(System.currentTimeMillis()-startTime);
		return ResponseEntity.ok(genericResponse);
	}

}
