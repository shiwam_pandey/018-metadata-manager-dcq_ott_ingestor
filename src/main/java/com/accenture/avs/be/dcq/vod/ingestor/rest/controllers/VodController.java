package com.accenture.avs.be.dcq.vod.ingestor.rest.controllers;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.avs.be.dcq.vod.ingestor.dto.Response;
import com.accenture.avs.be.dcq.vod.ingestor.service.VodService;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.framework.annotation.AvsRestController;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.cache.Configurator;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.commons.lib.support.CommonConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@AvsRestController
@Api(tags="VOD Ingestion services" ,value = "VOD API's", description = "API's pertaining to create/update/delete vod content")
public class VodController {

	@Autowired
	private Configurator configurator;
	
	@Autowired
	private VodService vodService;

	/**
	 * @param vodXml
	 * @return
	 * @throws ApplicationException
	 */
	
	@ApiOperation(value = "upsertVodContent", notes = "Please find the page for the IFA: https://fleet.alm.accenture.com/avswiki/display/AVSM3DOC/.DCQ+VOD+Ingestor-XML+v6.5. \n Creating or updating a VOD content", consumes=MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses({
		@ApiResponse(code = 400, message = "ACN_3000: Missing Parameter - Mandatory parameters not provided."
				+ "\nACN_3019: Invalid parameter - Invalid input parameters passed."
				+ "\nACN_9011: Region Name already exists - Given region name already exists."
				+ "\nACN_9029: Unable to update the region as at least one default region should be present."
				+ "\nACN_8033: Unable to update the region, invalid region hierarchy - Non existing region type id passed."
				+ "\nACN_8006: Invalid geo-attribute value - Invalid geo attributes values passed	."
				+ "\nACN_9012: Duplicate Region - If same region already exists."),
		@ApiResponse(code = 200, message = "ACN_200: Region added successfully - Request processed successfully"),
		@ApiResponse(code = 500, message = "ACN_300: GENERIC ERROR - Any other unexpected errors,")}) 
	@Deprecated
	@RequestMapping(value = "/vod/{contentId}", method = RequestMethod.POST)
	public @ResponseBody
	ResponseEntity<Response> upsertContent(
			@RequestBody String vodXml){
		Response response = new Response();
		try {
			Configuration config = configurator.getConfiguration(ThreadContext.get(CommonConstants.LogParamsL4j2.TN));
			vodService.ingestContent(vodXml, config);
		} catch (ApplicationException ae) {
			if(ae.getExceptionId().contains("3019")||ae.getExceptionId().contains("3000")) {
				
				if(ae.getParameters().getParameters()[0]!=null && ae.getParameters().getParameters()[0].contains("sdp")) {
					response.setResultCode(DcqVodIngestorConstants.KO);
					response.setErrorCode("ERR008");
					response.setErrorDescription(ae.getParameters().getParameters()[0]);
					return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
				}else {
					response.setResultCode(DcqVodIngestorConstants.KO);
					response.setErrorCode("ERR001-VALIDATION");
					response.setErrorDescription("Invalid Parameter "+ae.getParameters().getParameters()[0]);
					return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
				}
			}else {
				response.setResultCode(DcqVodIngestorConstants.KO);
				response.setErrorCode("ERR004-GENERIC");
				response.setErrorDescription(ae.getMessage());
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			}
		}
		catch (Exception e) {
			response.setResultCode(DcqVodIngestorConstants.KO);
			response.setErrorCode("ERR004-GENERIC");
			response.setErrorDescription(e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			}
		return ResponseEntity.ok(response);
	}
	
}
