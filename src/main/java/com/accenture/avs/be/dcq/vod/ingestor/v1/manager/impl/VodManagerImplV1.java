package com.accenture.avs.be.dcq.vod.ingestor.v1.manager.impl;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.accenture.avs.be.dcq.vod.ingestor.cache.DeviceChannelsCache;
import com.accenture.avs.be.dcq.vod.ingestor.cache.PoliciesCache;
import com.accenture.avs.be.dcq.vod.ingestor.cache.PropertiesCache;
import com.accenture.avs.be.dcq.vod.ingestor.cache.TRCCache;
import com.accenture.avs.be.dcq.vod.ingestor.content.dto.ContentPlatformVideoType;
import com.accenture.avs.be.dcq.vod.ingestor.content.dto.GetTransactionRateCardResponse;
import com.accenture.avs.be.dcq.vod.ingestor.content.dto.TRCEnablerRequest;
import com.accenture.avs.be.dcq.vod.ingestor.repository.AudiolangRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.BlacklistDeviceContentRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.CategoryRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ChannelRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ChapterRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ContentCategoryRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ContentLinkingRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ContentPlatformRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ContentRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.EmfRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.LanguageRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.MultiLanguageChapterRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.MultiLanguageContentRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.MultiLanguageSceneRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ObjectEmfRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ObjectPlatformRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.RelContentExtRatRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.RelPlatformTechnicalRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.SceneRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.SubtitlesRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.TechnicalPackageRepository;
import com.accenture.avs.be.dcq.vod.ingestor.sdp.manager.SDPManager;
import com.accenture.avs.be.dcq.vod.ingestor.utils.CacheConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.JsonUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.Validator;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.AudioLanguageDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.ChapterDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.ChapterDTO.MultiLanguageChapterDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.ContentLinkingDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.CopyProtectionDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.EmfAttributeDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.ExtensionsDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.ExtensionsDTO.CategoryDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.ExtensionsDTO.PlatformDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.ExtensionsDTO.TransactionRateCardDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.MultiLanguageMetadataDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.PropertyDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.SceneDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.SceneDTO.MultiLanguageSceneDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.SubTitleDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.VodRequestDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.manager.VodManagerV1;
import com.accenture.avs.be.dcq.vod.ingestor.v1.utils.CopyProtectionUtilsV1;
import com.accenture.avs.be.dcq.vod.ingestor.v1.utils.VodUtilitiesV1;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.be.framework.exception.ConfigurationException;
import com.accenture.avs.be.framework.utils.Utilities;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.persistence.technicalcatalogue.AudiolangEntity;
import com.accenture.avs.persistence.technicalcatalogue.AudiolangEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.BlackListDeviceEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.BlacklistDeviceEntity;
import com.accenture.avs.persistence.technicalcatalogue.CategoryEntity;
import com.accenture.avs.persistence.technicalcatalogue.ChannelEntity;
import com.accenture.avs.persistence.technicalcatalogue.ChapterEntity;
import com.accenture.avs.persistence.technicalcatalogue.ChapterEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.ContentCategoryEntity;
import com.accenture.avs.persistence.technicalcatalogue.ContentCategoryEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.ContentEntity;
import com.accenture.avs.persistence.technicalcatalogue.ContentLinkingEntity;
import com.accenture.avs.persistence.technicalcatalogue.ContentLinkingEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.ContentPlatformEntity;
import com.accenture.avs.persistence.technicalcatalogue.ContentTypeEntity;
import com.accenture.avs.persistence.technicalcatalogue.EmfEntity;
import com.accenture.avs.persistence.technicalcatalogue.ExtendedContentAttributeEntity;
import com.accenture.avs.persistence.technicalcatalogue.LanguageMetadataEntity;
import com.accenture.avs.persistence.technicalcatalogue.MultiLanguageContentEntity;
import com.accenture.avs.persistence.technicalcatalogue.MultiLanguageContentEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.MultilanguageChapterEntity;
import com.accenture.avs.persistence.technicalcatalogue.MultilanguageChapterEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.MultilanguageSceneEntity;
import com.accenture.avs.persistence.technicalcatalogue.MultilanguageSceneEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.ObjectEmfEntity;
import com.accenture.avs.persistence.technicalcatalogue.ObjectEmfEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.ObjectPlatformEntity;
import com.accenture.avs.persistence.technicalcatalogue.RelContentExtRatEntity;
import com.accenture.avs.persistence.technicalcatalogue.RelContentExtRatEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.RelPlatformTechnicalEntity;
import com.accenture.avs.persistence.technicalcatalogue.RelPlatformTechnicalEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.SceneEntity;
import com.accenture.avs.persistence.technicalcatalogue.SceneEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.SubtitleEntity;
import com.accenture.avs.persistence.technicalcatalogue.SubtitleEntityPK;

/**
 * @author karthik.vadla
 *
 */
@Component
@Transactional(rollbackFor = { ApplicationException.class, RuntimeException.class })
public class VodManagerImplV1 implements VodManagerV1 {

	private static final LoggerWrapper log = new LoggerWrapper(VodManagerImplV1.class);

	private static final String DEFAULT_IS_NO_ADULT = "N";
	private static final String DEFAULT_IS_PUSLISHED = "Y";
	private static final List<String> commonDataType = new ArrayList<String>();

	static {
		commonDataType.add("BUNDLE");
		commonDataType.add("GROUP_OF_BUNDLES");
		commonDataType.add("GROUP_OF_BUNDLE");
	}

	private static final String PIPE_STRING = "|";
	private static final String COMMA_STRING = ",";
	private static final String LANG_META_DATA_IS_ACTIVE = "Y";
	private static final String LANG_META_DATA_IS_DEFAULT = "N";

	@Autowired
	private DeviceChannelsCache deviceChannelsCache;
	@Autowired
	private TRCCache trcCache;
	@Autowired
	private PropertiesCache propertyCache;
	@Autowired
	private PoliciesCache policyCache;
	@Autowired
	private CopyProtectionUtilsV1 copyProtectionUtilsV1;

	private List<String> propertiesList = new ArrayList<String>();

	@Autowired
	private ContentRepository contentRepository;
	@Autowired
	private CategoryRepository categoryRepository;
	@Autowired
	private ContentCategoryRepository contentCategoryRepository;
	@Autowired
	private RelContentExtRatRepository relcontentExtRatRepository;
	@Autowired
	private AudiolangRepository audioLangRepository;

	@Autowired
	private SubtitlesRepository subtitlesRepository;

	@Autowired
	private ContentLinkingRepository contentLinkingRepository;

	@Autowired
	private RelPlatformTechnicalRepository relPlatformTechnicalRepository;

	@Autowired
	private ContentPlatformRepository contentPlatformRepository;

	@Autowired
	private MultiLanguageContentRepository multiLanguageContentRepository;
	@Autowired
	private MultiLanguageSceneRepository multiLanguageSceneRepository;
	@Autowired
	private MultiLanguageChapterRepository multiLanguageChapterRepository;

	@Autowired
	private BlacklistDeviceContentRepository blacklistDeviceContentRepository;

	@Autowired
	private SceneRepository sceneRepository;
	@Autowired
	private ChapterRepository chapterRepository;

	@Autowired
	private ObjectEmfRepository objectEmfRepository;

	@Autowired
	private LanguageRepository languageRepository;

	@Autowired
	private EmfRepository emfRepository;
	
	@Autowired
	private ObjectPlatformRepository objectPlatformRepository;
	@Autowired
	private TechnicalPackageRepository technicalPackageRepository;

	@Autowired
	private ChannelRepository channelRepository;

	@Autowired
	private SDPManager sdpManager;
	@Autowired
	private VodUtilitiesV1 vodUtilitiesV1;

	@Override
	public Integer ingestContent(VodRequestDTO vodDTO, Configuration config)
			throws ConfigurationException, ApplicationException, JsonParseException, JsonMappingException, IOException, ParseException{

		List<TRCEnablerRequest> trcEnablerRequestList = null;
		List<Long> duplicateList = null;

		if (vodDTO.getContentId() == null) {
			Long id = contentRepository.getContentIdByExternalId(vodDTO.getExternalId());
			if (id == null || id == 0) {
				id = contentRepository.getMaxId();
				if(id == null || id == 0){
					id = 0L;
				}
				id = id + 1;
			}
			vodDTO.setContentId(id);
			log.logMessage("Mapping asset to content externalId " + vodDTO.getExternalId());
		
			} else {
				Long id = contentRepository.getContentIdByExternalId(vodDTO.getExternalId());

				if (id != null && !id.equals(vodDTO.getContentId())) {
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters("ExternalId already exists for other content"));
				}
				log.logMessage("Mapping asset to content for contentId: " + vodDTO.getContentId() + " , externalId "
						+ vodDTO.getExternalId());
			
			}

		if (config.getConstants().getValue(CacheConstants.COPY_PROTECTION_IN_PANIC).equalsIgnoreCase("N")) {

			String isCopyProtected = null;

			if (!Validator.isEmpty(vodDTO.getIsCopyProtected())) {

				isCopyProtected = vodDTO.getIsCopyProtected();
				List<CopyProtectionDTO> copyProtections = null;
				CopyProtectionDTO copyProtectionDTO = null;
				if (null != vodDTO.getCopyProtections()) {
					copyProtections = new ArrayList<>();
					for (CopyProtectionDTO copyProtection : vodDTO.getCopyProtections()) {
						copyProtectionDTO = new CopyProtectionDTO();
						copyProtectionDTO.setSecurityCode(copyProtection.getSecurityCode());
						copyProtectionDTO.setSecurityOption(copyProtection.getSecurityOption());
						copyProtections.add(copyProtectionDTO);
					}
				}
				log.logMessage("copy protection validations start");
				copyProtectionUtilsV1.validateCopyProtections(isCopyProtected, copyProtections, config);
				log.logMessage("copy protection validations end");

			} else if (Validator.isEmpty(vodDTO.getIsCopyProtected())
					&& (!Validator.isEmpty(vodDTO.getCopyProtections()))) {
				throw new ApplicationException("ERROR_BE_ACTION_ACN_9810_COPY_PROTECTION_NOW_ALLOWED");

			}

		} else {
			log.logMessage("CopyProtection is in Panic");
		}

		// Start mapping Asset to Content
		ContentEntity content = new ContentEntity();

		Set<ContentPlatformEntity> contentPlatforms = null;
		Set<RelContentExtRatEntity> relContentExtRats = null;
		Set<ContentCategoryEntity> contentCategories = null;
		Set<SceneEntity> listOfScenes = null;
		Set<ChapterEntity> listOfChapters = null;
		Set<MultilanguageSceneEntity> listOfMultilanguageScenes = null;
		Set<MultilanguageChapterEntity> listOfMultilanguageChapters = null;
		Set<ObjectPlatformEntity> scenePlatforms = null;
		Set<ObjectPlatformEntity> chapterPlatforms = null;
		Set<ObjectEmfEntity> objectEmfAttributes = null;

		List<TRCEnablerRequest> trcEnablerRequest = null;

		content.setContentId(vodDTO.getContentId().intValue());
		content.setContentProvider(vodDTO.getContentProvider());
		content.setExternalContentId(vodDTO.getExternalId());

		ContentTypeEntity contentType = new ContentTypeEntity();
		contentType.setName(vodDTO.getType());
		content.setContentType(contentType);
		// content.setType(contentType);
		content.setTitle(vodDTO.getTitle());
		content.setDuration(vodDTO.getDuration().intValue());

		if (!Utilities.isEmpty(vodDTO.getExtensions())
				&& !CollectionUtils.isEmpty(vodDTO.getExtensions().getExtendedRatings())) {

			for (String extRating : vodDTO.getExtensions().getExtendedRatings()) {

				if (!Arrays.asList(DcqVodIngestorConstants.ALLOWED_PC_EXTENDED_RATINGS_TYPES).contains(extRating)) {
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters("extended ratings . Expected values S,T,H,D,A,G"));

				}

			}
		}
		

		// AS per story 18901 start
		if (!Utilities.isEmpty(vodDTO.getExtensions()) && !Utilities.isEmpty(vodDTO.getExtensions().getTransactionRateCards())) {

			trcEnablerRequestList = new ArrayList<TRCEnablerRequest>();
			duplicateList = new ArrayList<Long>();
			Map<Long, GetTransactionRateCardResponse> trcIdCache = trcCache.getTrcIdCache();
			Map<String, GetTransactionRateCardResponse> trcNameCache = trcCache.getTrcNameCache();

			if (null == trcIdCache || null == trcNameCache) {
				throw new ApplicationException(MessageKeys.ERROR_MS_ACN_901_COMMERCE_NOT_AVAILABLE);
			}

			if (trcIdCache == null || trcIdCache.isEmpty() || trcNameCache == null || trcNameCache.isEmpty()) {
				log.logMessage("Calling Configuration MS to reload the propertiesCache");
				trcIdCache = trcCache.loadTRCS(config);
				trcNameCache = trcCache.loadTRCList(config);

			}

			// Check TRC dates are overlapping or not
			validateTRCList(trcIdCache, trcNameCache, vodDTO.getExtensions().getTransactionRateCards());
			for (TransactionRateCardDTO trc : vodDTO.getExtensions().getTransactionRateCards()) {
				GetTransactionRateCardResponse transactionRateCard = null;
				TRCEnablerRequest trcEnabler = new TRCEnablerRequest();

				if (null != trc.getTransactionRateCardId()) {
						transactionRateCard = trcIdCache.get(trc.getTransactionRateCardId());

						if (null == transactionRateCard || ("N").equals(transactionRateCard.getIsActive())) {
							throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
									new NestedParameters("TRC"));

						}

						if (transactionRateCard.getEndDate() != null
								&& transactionRateCard.getEndDate().before(new Date())) {
							throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
									new NestedParameters("Expired TRC provided"));

						}

						trcEnabler.setDescription(transactionRateCard.getDescription());
						trcEnabler.setEndDate(transactionRateCard.getEndDate());
						trcEnabler.setIsActive(transactionRateCard.getIsActive());
						trcEnabler.setName(transactionRateCard.getName());
						trcEnabler.setPlatformList(transactionRateCard.getPlatformList());
						trcEnabler.setPriceCategory(transactionRateCard.getPriceCategory());
						trcEnabler.setStartDate(transactionRateCard.getStartDate());
						trcEnabler.setTransactionRateCardId(transactionRateCard.getTransactionRateCardId());
						trcEnabler.setVideoTypeList(transactionRateCard.getVideoTypeList());
						trcEnabler.setRentalPeriod(transactionRateCard.getRentalPeriod());
						trcEnabler.setTransactionRateCardId(transactionRateCard.getTransactionRateCardId());
						if (!Utilities.isEmpty(trc.getEnablerCommercialPackages()) && !Utilities.isEmptyCollection(
								trc.getEnablerCommercialPackages())) {
							trcEnabler.setRequiredSolutionofferName(
									trc.getEnablerCommercialPackages());
						}
						if (!Utilities.isEmptyCollection(duplicateList)
								&& duplicateList.contains(trcEnabler.getTransactionRateCardId())) {
							throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
									new NestedParameters("Duplicate TRC found"));

						}
						trcEnablerRequestList.add(trcEnabler);
						duplicateList.add(trcEnabler.getTransactionRateCardId());
					} else if (null != trcNameCache.get(trc.getExternalId())) {
						transactionRateCard = trcNameCache.get(trc.getExternalId());

						if (null == transactionRateCard || ("N").equals(transactionRateCard.getIsActive())) {
							throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
									new NestedParameters("TRC"));

						}

						if (transactionRateCard.getEndDate() != null
								&& transactionRateCard.getEndDate().before(new Date())) {
							throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
									new NestedParameters("Expired TRC provided"));

						}
						trcEnabler.setDescription(transactionRateCard.getDescription());
						trcEnabler.setEndDate(transactionRateCard.getEndDate());
						trcEnabler.setIsActive(transactionRateCard.getIsActive());
						trcEnabler.setName(transactionRateCard.getName());
						trcEnabler.setPlatformList(transactionRateCard.getPlatformList());
						trcEnabler.setPriceCategory(transactionRateCard.getPriceCategory());
						trcEnabler.setTransactionRateCardId(transactionRateCard.getTransactionRateCardId());
						trcEnabler.setStartDate(transactionRateCard.getStartDate());
						trcEnabler.setTransactionRateCardId(transactionRateCard.getTransactionRateCardId());
						trcEnabler.setVideoTypeList(transactionRateCard.getVideoTypeList());
						trcEnabler.setRentalPeriod(transactionRateCard.getRentalPeriod());
						if (!Utilities.isEmpty(trc.getEnablerCommercialPackages()) && !Utilities.isEmptyCollection(
								trc.getEnablerCommercialPackages())) {
							trcEnabler.setRequiredSolutionofferName(
									trc.getEnablerCommercialPackages());
						}
						if (!Utilities.isEmptyCollection(duplicateList)
								&& duplicateList.contains(trcEnabler.getTransactionRateCardId())) {
							throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
									new NestedParameters("Duplicate TRC found"));

						}
						trcEnablerRequestList.add(trcEnabler);
						duplicateList.add(trcEnabler.getTransactionRateCardId());

					}

					else {
						throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
								new NestedParameters("TRC"));

				} 
			}

		}

		// AS per story 18901 end

		// AVS-12973 start
		propertiesList = new ArrayList<>();
		if (vodDTO.getProperties() != null && !vodDTO.getProperties().isEmpty()) {
			Map<String, String> propertiesCache = propertyCache.getProperties();

			if (propertiesCache == null || propertiesCache.isEmpty()) {
				// AVS-18140 start - reloading the cache inorder to fetch latest properties
				log.logMessage("Calling Configuration MS to reload the propertiesCache");
				propertiesCache = propertyCache.loadProperties(config);
				// AVS-18140 stop
			}
			if (null == propertiesCache) {
				throw new ApplicationException(MessageKeys.ERROR_MS_ACN_902_CONFIGURATION_NOT_AVAILABLE);

			}
			// check asset property with property cache
			for (PropertyDTO property : vodDTO.getProperties()) {
				String propertyName = property.getPropertyName();
				if (propertiesCache == null || null == propertiesCache.get(propertyName)) {
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters("Property"));

				}
				// check extendedmetadata is valid
				String propertyExtendedMetadata = property.getExtendedMetadata();
				if (!StringUtils.isEmpty(propertyExtendedMetadata)
						&& !JsonUtils.isJSONValid(propertyExtendedMetadata)) {
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters("ExtendedMetadata"));

				}
				propertiesList.add(propertyName);
			}
		}

		// AVS-12973 stop

		/* AVS-13824 Start */
		content.setIsOutOfHome(vodDTO.getIsNotAvailableOutOfHome());
		/* AVS-13824 End */

		content.setGenre(Validator.list2String(vodDTO.getGenres(), ",", 500));

		content.setIsHd(vodDTO.getIsHD());
		content.setEpisodeTitle(vodDTO.getEpisodeTitle());

		if (vodDTO.getEpisodeId() != null) {
			content.setEpisode(vodDTO.getEpisodeId().intValue());
		}
		if (vodDTO.getIsEncrypted() != null) {
			content.setIsEncrypted(vodDTO.getIsEncrypted());
		}

		/*if (vodDTO.getCategory() != null) {
			content.setCategoryId(asset.getCategory().intValue());
		}*/

		if (vodDTO.getEntitlementId() != null) {
			content.setEntitlementId(vodDTO.getEntitlementId().intValue());
		}

		content.setContractStart(vodDTO.getContractStartDate());
		content.setContractEnd(vodDTO.getContractEndDate());

		if (vodDTO.getIsRecommended() == null) {
			content.setIsRecommended("N");// default is not reccomended
		} else {
			content.setIsRecommended(vodDTO.getIsRecommended());
		}
		// Language mapping. The multilanguage mapping is in MAPPING COMPOSED OBJECTS
		// code
		if (vodDTO.getLanguage() != null) {
			content.setLanguage(vodDTO.getLanguage());
		}
		if (vodDTO.getShortDescription() != null) {
			content.setShortDescription(vodDTO.getShortDescription());
		}
		if (vodDTO.getLongDescription() != null) {
			content.setLongDescription(vodDTO.getLongDescription());
		}
		if (vodDTO.getEventPlace() != null) {
			content.setEventPlace(vodDTO.getEventPlace());
		}
		if (vodDTO.getYear() != null) {
			content.setContentYear(String.valueOf(vodDTO.getYear()));
		}
		if (vodDTO.getLastBroadcastDate() != null) {
			content.setLastBroadcastDate(vodDTO.getLastBroadcastDate());
		}

		if (vodDTO.getSeasonId() != null) {
			content.setSeason(vodDTO.getSeasonId());
		}
		if (vodDTO.getSeries() != null) {
			content.setSeries(vodDTO.getSeries().intValue());
		}
		if (vodDTO.getStarRating() != null) {
			content.setStarRating(vodDTO.getStarRating());
		}
		if (vodDTO.getIsGeoBlocked() == null) {
			content.setIsGeoBlocked("N"); // default is not geoblocked
		} else {
			content.setIsGeoBlocked(vodDTO.getIsGeoBlocked());
		}
		if (vodDTO.getAdditionalData() != null) {
			content.setAdditionalData00(vodDTO.getAdditionalData());
		}
		/*
		 * MAPPING COMPOSED OBJECTS - START
		 */
		// Price category
		// PriceCategory priceCat = new PriceCategory();
		if (vodDTO.getPriceCategoryId() != null) {
			content.setPriceCategoryId(vodDTO.getPriceCategoryId().longValue());
		}

		// PC_LEVEL
		String pcLevelValue = config.getPcLevels().getKey(vodDTO.getParentalRating());

		if (pcLevelValue == null) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters("Parental control level " + vodDTO.getParentalRating()
							+ " does not exist in AVS! The asset will be discared."));

		} else {
			content.setPcLevel(pcLevelValue);
		}

		// LastChannel
		ChannelEntity channel = null;
		if (vodDTO.getLastBroadcastChannelId() != null) {
			channel = getChannelName(vodDTO.getLastBroadcastChannelId());
			if (channel == null) {
				throw new ApplicationException("ERROR_BE_ACTION_8013_CHANNEL_ID_NOT_FOUND");
			} else {
				content.setBroadcastChannelName(channel.getName());
			}

		}

		// List objects
		List<String> directors = vodDTO.getDirectors();
		if (directors != null) {
			content.setDirectors(Validator.list2String(directors, ",", 800));}

		List<String> actors = vodDTO.getActors();
		if (actors != null) {
			content.setActors(Validator.list2String(actors, ",", 800));}

		List<String> anchors = vodDTO.getAnchors();
		if (anchors != null) {
			content.setAnchors(Validator.list2String(anchors, ",", 800));}

		List<String> authors = vodDTO.getAuthors();
		if (authors != null) {
			content.setAuthors(Validator.list2String(authors, ",", 800));}

		if (vodDTO.getCountriesOfOrigin() != null) {
			content.setCountry(Validator.list2String(vodDTO.getCountriesOfOrigin(), ",", 800));}

		ExtensionsDTO extensions = vodDTO.getExtensions();

		if (extensions == null) {
			content.setIsAdult(DEFAULT_IS_NO_ADULT); // Default value
		} else {
			content.setIsAdult(extensions.getIsAdult());
		}

		// content.setContentPlatforms(getVodContentPlatform(asset, extensions,
		// config));

		contentPlatforms = getVodContentPlatform(vodDTO, extensions, config);

		// content.setRelContentExtRats(getVodRelContentExtRat(asset, extensions,
		// config));

		relContentExtRats = getVodRelContentExtRat(vodDTO, extensions, config);

		// content.setContentCategories(getVodContentCategory(asset, extensions));

		contentCategories = getVodContentCategory(vodDTO, extensions);
		// content.setDecryptKeyRepository(getVodDecryptKey(asset, extensions));

		// Start: Added for 4.2 New Object
		content.setExtendedContentAttribute(getExtendedContentAttributes(vodDTO, channel));
		content.setContentLinking(getContentLinking(vodDTO));

		// start AVS - 13658
		if (config.getConstants().getValue(CacheConstants.CONCURRENT_STREAM_IN_PANIC).equalsIgnoreCase("N")) {
			if (vodDTO.getStreamPolicies() != null) {

				if (vodDTO.getStreamPolicies() != null
						&& !vodDTO.getStreamPolicies().isEmpty()) {

					Map<String, String> policyMap = policyCache.getPolicies();
					// AVS-20827 start
					if (policyMap == null || policyMap.isEmpty()) {
						log.logMessage("Calling concurrent stream to reload policyMap");
						policyMap = policyCache.loadPolicies(config);
					}
					
					if(policyMap==null) {
						throw new ApplicationException(MessageKeys.ERROR_MS_ACN_921_CONCURRENT_STREAM_NOT_AVAILABLE);
					}
					// AVS-20827 stop
					for (String policyId : vodDTO.getStreamPolicies()) {

						if (policyMap.get(policyId) == null) {
							throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
									new NestedParameters("StreamPolicies"));

						}

					}
				}
			}
		} else {
			if (vodDTO.getStreamPolicies() != null) {
				if (!vodDTO.getStreamPolicies().isEmpty()) {
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters("Policy does not allowed due to concurrentStream is in panic"));

				}
			}
		}
		// end AVS - 13658
		Set<BlacklistDeviceEntity> blacklistDeviceEntities = null;

		// Added for 4.3 device based packaging
		if (vodDTO.getBlackListDeviceTypes() != null
				&& vodDTO.getBlackListDeviceTypes().size() > 0) {
			blacklistDeviceEntities = getBlacklistDeviceContent(vodDTO);
		}

		if (content.getContentLinking() != null && !content.getContentLinking().isEmpty()) {
			boolean defaultTrailor = false;
			boolean defaultPoster = false;
			Integer defaultTrailorId = -1;
			Integer defaultPosterId = -1;

			for (ContentLinkingEntity linking : content.getContentLinking()) {

				if (!defaultTrailor && "trailer".equalsIgnoreCase(linking.getChildSubtype())
						&& "Y".equalsIgnoreCase(linking.getIsChildDefault())) {
					defaultTrailor = true;
					defaultTrailorId = linking.getId().getChildContentId();
				}
				if (!defaultPoster && "poster".equalsIgnoreCase(linking.getChildSubtype())
						&& "Y".equalsIgnoreCase(linking.getIsChildDefault())) {
					defaultPoster = true;
					defaultPosterId = linking.getId().getChildContentId();
				}
			}
			boolean equalTrailorContentPlatform = false;
			boolean equalPosterContentPlatform = false;

			boolean isBundleOrGroupOfBundle = false;
			if (content.getExtendedContentAttribute() != null
					&& content.getExtendedContentAttribute().getObjectType() != null) {
				isBundleOrGroupOfBundle = commonDataType
						.contains(content.getExtendedContentAttribute().getObjectType().toUpperCase());
			}
			if (defaultTrailorId != -1) {
				ContentEntity trailorContent = null;
				trailorContent = getContentDetailByContentId(defaultTrailorId);

				if (trailorContent == null) {
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters("Content Id " + defaultTrailorId + " not Present"));

				}
				equalTrailorContentPlatform = validateContentPlatform(
						getContentPlatformVideoType(new ArrayList<>(contentPlatforms), isBundleOrGroupOfBundle, config),
						getContentPlatformVideoType(trailorContent.getContentPlatforms(), isBundleOrGroupOfBundle,
								config));

				if (!equalTrailorContentPlatform && defaultTrailor) {
					throw new ApplicationException("ERROR_BE_ACTION_ACN_9811_TRAILER_PLATFORM_SHOULD_MATCH");
				}
				updateTrailerAndPosterUrl(contentPlatforms, trailorContent.getContentPlatforms(), null,
						isBundleOrGroupOfBundle);
			}

			if (defaultPosterId != -1) {
				ContentEntity posterContent = getContentDetailByContentId(defaultPosterId);

				if (posterContent == null) {
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters("Content Id " + defaultPosterId + " not Present"));

				}

				equalPosterContentPlatform = validateContentPlatform(
						getContentPlatformVideoType(new ArrayList<>(contentPlatforms), isBundleOrGroupOfBundle, config),
						getContentPlatformVideoType(posterContent.getContentPlatforms(), isBundleOrGroupOfBundle,
								config));

				if (!equalPosterContentPlatform && defaultPoster) {
					throw new ApplicationException("ERROR_BE_ACTION_ACN_9812_POSTER_PLATFORM_SHOULD_MATCH");

				}
				updateTrailerAndPosterUrl(contentPlatforms, null, posterContent.getContentPlatforms(),
						isBundleOrGroupOfBundle);
			}

		}
		// End: Added for 4.2 New Object
		/*
		 * MAPPING COMPOSED OBJECTS - END
		 */
		// AssetToWrite assetToWrite = new AssetToWrite();
		/* 4.3 multi language metadata */
		Set<MultiLanguageContentEntity> multiLanguageContents = null;
		Set<LanguageMetadataEntity> languageMetadataSet = null;
		if (vodDTO.getMultiLanguageMetadata() != null && !vodDTO.getMultiLanguageMetadata().isEmpty()) {
			Set<MultiLanguageContentEntity> multilangMetadataSet = new HashSet<MultiLanguageContentEntity>();
			Set<LanguageMetadataEntity> langMetadataSet = new HashSet<LanguageMetadataEntity>();

			for (int i = 0; i < vodDTO.getMultiLanguageMetadata().size(); i++) {
				MultiLanguageContentEntity multilangMetadata = new MultiLanguageContentEntity();
				MultiLanguageMetadataDTO metaDataMulti = vodDTO
						.getMultiLanguageMetadata().get(i);

				if (metaDataMulti.getLanguageCode() != null) {
					MultiLanguageContentEntityPK contentIdLanguageIdPK = new MultiLanguageContentEntityPK();
					contentIdLanguageIdPK.setLanguageCode(metaDataMulti.getLanguageCode());
					contentIdLanguageIdPK.setContentId(vodDTO.getContentId().intValue());
					multilangMetadata.setId(contentIdLanguageIdPK);
				}
				if (metaDataMulti.getCountries() != null) {
						multilangMetadata.setCountry(
								stringWithPipeComma(metaDataMulti.getCountries(), COMMA_STRING));
				}
				if (metaDataMulti.getEpisodeTitle() != null && !metaDataMulti.getEpisodeTitle().equals("")) {
					multilangMetadata.setEpisodeTitle(metaDataMulti.getEpisodeTitle());
				}
				if (metaDataMulti.getGenres() != null) {
						multilangMetadata.setGenre(
								stringWithPipeComma(metaDataMulti.getGenres(), COMMA_STRING));
				}
				if (metaDataMulti.getLongDescription() != null && !metaDataMulti.getLongDescription().equals("")) {
					multilangMetadata.setLongDescription(metaDataMulti.getLongDescription());
				}

				if (metaDataMulti.getSearchKeywords() != null) {
						multilangMetadata.setSearchKeywords(
								stringWithPipeComma(metaDataMulti.getSearchKeywords(), PIPE_STRING)
										.replace(",", "|"));
				}
				if (metaDataMulti.getShortDescription() != null && !metaDataMulti.getShortDescription().equals("")) {
					multilangMetadata.setShortDescription(metaDataMulti.getShortDescription());
				}
				if (metaDataMulti.getTitle() != null && !metaDataMulti.getTitle().equals("")) {
					multilangMetadata.setTitle(metaDataMulti.getTitle());
				}
				if (metaDataMulti.getTitleBrief() != null && !metaDataMulti.getTitleBrief().equals("")) {
					multilangMetadata.setTitleBrief(metaDataMulti.getTitleBrief());
				}

				multilangMetadataSet.add(multilangMetadata);
				LanguageMetadataEntity languageMetada = new LanguageMetadataEntity();
				languageMetada.setIsActive(LANG_META_DATA_IS_ACTIVE.charAt(0));
				languageMetada.setIsDefault(LANG_META_DATA_IS_DEFAULT.charAt(0));
				if (metaDataMulti.getLanguageCode() != null && !metaDataMulti.getLanguageCode().equals("")) {

					if (validateLangCode(metaDataMulti.getLanguageCode())) {
						throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
								new NestedParameters("Language code not in ISO standards."));

					} else {
						languageMetada.setLanguageCode(metaDataMulti.getLanguageCode());
					}

				}
				if (metaDataMulti.getLanguageName() != null && !metaDataMulti.getLanguageName().equals("")) {
					languageMetada.setLanguageName(metaDataMulti.getLanguageName());
				}
				langMetadataSet.add(languageMetada);

				if (!StringUtils.isEmpty(metaDataMulti.getExtendedMetadata())
						&& !JsonUtils.isJSONValid(metaDataMulti.getExtendedMetadata())) {
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters("ExtendedMetadata"));

				}
			}
			// content.setMultiLanguageContentEntity(multilangMetadataSet);

			multiLanguageContents = multilangMetadataSet;
			languageMetadataSet = langMetadataSet;// TODO
			// assetToWrite.setLanguageMetada(langMetadataSet);
			// assetToWrite.setMultilangMetadata(multilangMetadataSet);

		}

		// START: Segment within Content of AVS 4.3
		listOfScenes = new LinkedHashSet<SceneEntity>(); // for holding list of all scenes
		listOfChapters = new LinkedHashSet<ChapterEntity>(); // for holding list of all chapters
		listOfMultilanguageScenes = new LinkedHashSet<MultilanguageSceneEntity>(); // for holding list of all
																					// multilanguage scenes
		listOfMultilanguageChapters = new LinkedHashSet<MultilanguageChapterEntity>(); // for holding list of all
																						// multilanguage chapters

		Set<String> contentLanguageList = new HashSet<String>();
		Set<MultiLanguageContentEntity> multilangMetadatas = multiLanguageContents;

		// Added defaultLang for Content
		contentLanguageList.add(vodDTO.getDefaultLanguage());

		if (!CollectionUtils.isEmpty(multilangMetadatas)) {
			for (MultiLanguageContentEntity multilangMetadata : multilangMetadatas) {
				contentLanguageList.add(multilangMetadata.getId().getLanguageCode().toUpperCase());
			}
		}

		List<SceneDTO> scenesList = vodDTO.getScenes();

		if (scenesList != null) {
			// iterating <SceneList>
			for (SceneDTO sceneObj : scenesList) {

				if (!StringUtils.isEmpty(sceneObj.getExtendedMetadata())
						&& !JsonUtils.isJSONValid(sceneObj.getExtendedMetadata())) {
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters("ExtendedMetadata"));

				}

				Set<String> sceneLanguageList = new LinkedHashSet<String>(); // for holding all languages supported by
																				// this scene
				sceneLanguageList.add(sceneObj.getMetadataLanguage());

				List<MultiLanguageSceneDTO> sceneLanguages = sceneObj.getMultiLanguageScenes();
				if (sceneLanguages != null) {
					for (MultiLanguageSceneDTO multilanguageScene : sceneLanguages) {

						if (!StringUtils.isEmpty(multilanguageScene.getExtendedMetadata())
								&& !JsonUtils.isJSONValid(multilanguageScene.getExtendedMetadata())) {
							throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
									new NestedParameters(
											"ExtendedMetadata"));

						}
						sceneLanguageList.add(multilanguageScene.getMetadataLanguage());
					}
				}

				if (contentLanguageList.contains(sceneObj.getMetadataLanguage().toUpperCase())) {
					// for each <Scene> in <SceneList>
					SceneEntity scene = new SceneEntity();
					SceneEntityPK id = new SceneEntityPK();
					id.setSceneId(sceneObj.getSceneId());
					id.setContentId(content.getContentId().longValue());
					scene.setCompId(id);
					scene.setAction(sceneObj.getAction());
					scene.setActors(sceneObj.getActors());
					scene.setAdTag(sceneObj.getAdTag());
					scene.setAudio(sceneObj.getAudio());
					scene.setBriefTitle(sceneObj.getTitleBrief());
					scene.setCharacters(sceneObj.getCharacters());
					scene.setCompany(sceneObj.getCompany());
					scene.setContentTitle(content.getTitle());

					scene.setStartTime(vodUtilitiesV1.getDateFromHHMMSS(sceneObj.getStartTime(), "Scene startTime"));
					scene.setEndTime(vodUtilitiesV1.getDateFromHHMMSS(sceneObj.getEndTime(), "Scene endTime"));
					scene.setEvent(sceneObj.getEvent());
					scene.setExtendedLocation(sceneObj.getExtendedLocation());
					scene.setGenre(sceneObj.getGenre());
					scene.setKeywords(sceneObj.getKeywords());
					scene.setLocation(sceneObj.getLocation());
					scene.setMetadataLanguage(sceneObj.getMetadataLanguage());
					scene.setMusicians(sceneObj.getMusicians());
					scene.setOrderId(sceneObj.getOrderId());

					scene.setTitle(sceneObj.getTitle());

					List<EmfAttributeDTO> emfAttributes = sceneObj.getEmfAttributes();
					
					Set<ObjectEmfEntity> sceneObjectEmfs = null;
					Set<ObjectEmfEntity> multilanguageSceneObjectEmfs = null;

					if (emfAttributes != null) {
						sceneObjectEmfs = new LinkedHashSet<ObjectEmfEntity>();
						// iterating <EMFAttributes> in <Scene>
						for (EmfAttributeDTO emfAttribute : emfAttributes) {
							// for each <EMFAttribute>
							// check if emf attribute language is supported by scene or not. If not
							// supported, that attribute will not be published.
							if (sceneLanguageList.contains(emfAttribute.getMetadataLanguage())) {
								if (emfAttribute.getEmfName() != null && !emfAttribute.getEmfName().isEmpty()) {
									ObjectEmfEntity attributes = new ObjectEmfEntity();
									ObjectEmfEntityPK objectEmfPK = new ObjectEmfEntityPK();
									objectEmfPK.setEmfId(emfAttribute.getEmfId().intValue());
									objectEmfPK.setLanguageCode(emfAttribute.getMetadataLanguage());
									objectEmfPK.setObjId(scene.getCompId().getSceneId().intValue());
									objectEmfPK.setObjType(3);
									attributes.setId(objectEmfPK);
									attributes.setEmfValue(emfAttribute.getEmfValue());
									attributes.setEmfName(emfAttribute.getEmfName());
									sceneObjectEmfs.add(attributes);
								} 
							} else {
								throw new ApplicationException("ERROR_BE_ACTION_ACN_9813_EMF_NOT_SUPPORT_BY_SCENE_LANGUAGE");

							}
						}
					}

					scene.setEmfAttributes(sceneObjectEmfs);

					List<com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.SceneDTO.PlatformDTO> platforms = sceneObj
							.getPlatforms();
					scenePlatforms = new LinkedHashSet<ObjectPlatformEntity>();

					// iterating <PlatformList> in <Scene>
					for (com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.SceneDTO.PlatformDTO platformObj : platforms) {
						ObjectPlatformEntity platform = new ObjectPlatformEntity();
						platform.setObjectId(scene.getCompId().getSceneId());
						platform.setObjectType("SCENE");
						
						if(!config.getPlatforms().isValidKey(platformObj.getName())) {
							throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters("Scene platform name"));
						}
						platform.setName(platformObj.getName());
						platform.setPictureUrl(platformObj.getPictureUrl());
						platform.setPictureResolution(platformObj.getPictureResolution());
						scenePlatforms.add(platform);
					}

					scene.setPlatforms(scenePlatforms);
					listOfScenes.add(scene);

					if (sceneLanguages != null) {
						// iterating <Multilanguage> in <Scene>
						for (MultiLanguageSceneDTO sceneLanguage : sceneLanguages) {
							if (contentLanguageList.contains(sceneLanguage.getMetadataLanguage().toUpperCase())) {
								// for each <SceneLanguage>
								MultilanguageSceneEntity multilanguageScene = new MultilanguageSceneEntity();
								MultilanguageSceneEntityPK multilanguageScenePK = new MultilanguageSceneEntityPK();
								multilanguageScenePK.setSceneId(sceneObj.getSceneId());
								multilanguageScenePK.setContentId(content.getContentId().longValue());
								multilanguageScenePK.setMetadataLanguage(sceneLanguage.getMetadataLanguage());
								multilanguageScene.setId(multilanguageScenePK);
								multilanguageScene.setAction(sceneLanguage.getAction());
								multilanguageScene.setActors(sceneLanguage.getActors());
								multilanguageScene.setAdTag(sceneLanguage.getAdTag());
								multilanguageScene.setAudio(sceneLanguage.getAudio());
								multilanguageScene.setBriefTitle(sceneLanguage.getTitleBrief());
								multilanguageScene.setCharacters(sceneLanguage.getCharacters());
								multilanguageScene.setCompany(sceneLanguage.getCompany());
								multilanguageScene.setContentTitle(content.getTitle());
								multilanguageScene
										.setEndTime(vodUtilitiesV1.getDateFromHHMMSS(sceneObj.getEndTime(),"Scene endTime"));
								multilanguageScene.setEvent(sceneLanguage.getEvent());
								multilanguageScene.setExtendedLocation(sceneLanguage.getExtendedLocation());
								multilanguageScene.setGenre(sceneLanguage.getGenre());
								multilanguageScene.setKeywords(sceneLanguage.getKeywords());
								multilanguageScene.setLocation(sceneLanguage.getLocation());
								multilanguageScene.setMusicians(sceneLanguage.getMusicians());
								multilanguageScene.setOrderId(sceneLanguage.getOrderId());
								multilanguageScene
										.setStartTime(vodUtilitiesV1.getDateFromHHMMSS(sceneObj.getStartTime(), "Scene startTime"));
								multilanguageScene.setTitle(sceneLanguage.getTitle());

								List<EmfAttributeDTO> multilanguageEmfAttributes = sceneLanguage
										.getEmfAttributes();

								if (multilanguageEmfAttributes != null) {
									multilanguageSceneObjectEmfs = new LinkedHashSet<ObjectEmfEntity>();
									// iterating <EMFAttributes> in <SceneLanguage>
									for (EmfAttributeDTO emfAttribute : multilanguageEmfAttributes) {
										// check if emf attribute language is supported by scene or not. If not
										// supported, that attribute will not be published.
										if (sceneLanguageList.contains(emfAttribute.getMetadataLanguage())) {
											if (emfAttribute.getEmfName() != null && !emfAttribute.getEmfName().isEmpty()) {
												ObjectEmfEntity attributes = new ObjectEmfEntity();
												ObjectEmfEntityPK objectEmfPK = new ObjectEmfEntityPK();
												objectEmfPK.setEmfId( emfAttribute.getEmfId().intValue());
												objectEmfPK.setLanguageCode(emfAttribute.getMetadataLanguage());
												objectEmfPK.setObjId(scene.getCompId().getSceneId().intValue());
												objectEmfPK.setObjType(3);
												attributes.setId(objectEmfPK);
												attributes.setEmfValue(emfAttribute.getEmfValue());
												attributes.setEmfName(emfAttribute.getEmfName());
												multilanguageSceneObjectEmfs.add(attributes);
											}
										} else {
											throw new ApplicationException("ERROR_BE_ACTION_ACN_9813_EMF_NOT_SUPPORT_BY_SCENE_LANGUAGE");

										}
									}
								}

								multilanguageScene.setEmfAttributes(multilanguageSceneObjectEmfs);
								listOfMultilanguageScenes.add(multilanguageScene);
							} else {
									throw new ApplicationException("ERROR_BE_ACTION_ACN_9815_CONTENT_NOT_SUPPORT_BY_MULTI_LANGUAGE_SCENE");
							}
						}
					}
				} else {

						throw new ApplicationException("ERROR_BE_ACTION_ACN_9816_CONTENT_NOT_SUPPORT_BY_SCENE");
				}

			}
		}


		List<ChapterDTO> chapterList = vodDTO.getChapters();


		if (chapterList != null) {
			// iterating <ChapterList> tag
			for (ChapterDTO chapterObj : chapterList) {

				List<MultiLanguageChapterDTO> chapterLanguages = chapterObj
						.getMultiLanguageChapters();
			
				if (contentLanguageList.contains(chapterObj.getMetadataLanguage())) {
					// for each <Chapter> in <ChapterList>
					ChapterEntity chapter = new ChapterEntity();
					ChapterEntityPK chapterPK = new ChapterEntityPK();
					chapter.setBriefTitle(chapterObj.getBriefTitle());
					chapterPK.setChapterId(chapterObj.getChapterId());
					chapterPK.setContentId(content.getContentId().longValue());
					chapter.setCompId(chapterPK);

					chapter.setStartTime(vodUtilitiesV1.getDateFromHHMMSS(chapterObj.getStartTime(), "Chapter startTime"));
					chapter.setEndTime(vodUtilitiesV1.getDateFromHHMMSS(chapterObj.getEndTime(), "Chapter endTime"));
					chapter.setMetadataLanguage(chapterObj.getMetadataLanguage());
					chapter.setOrderId(chapterObj.getOrderId());

					List<com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.ChapterDTO.PlatformDTO> platforms = chapterObj
							.getPlatforms();
					chapterPlatforms = new LinkedHashSet<ObjectPlatformEntity>();

					// iterating <PlatformList> in <Chapter>
					for (com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.ChapterDTO.PlatformDTO platformObj : platforms) {
						ObjectPlatformEntity platform = new ObjectPlatformEntity();
						platform.setObjectId(chapter.getCompId().getChapterId());
						platform.setObjectType("CHAPTER");
						
						if(!config.getPlatforms().isValidKey(platformObj.getName())) {
							throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters("chapter platform name"));
						}
						platform.setName(platformObj.getName());
						platform.setPictureUrl(platformObj.getPictureUrl());
						platform.setPictureResolution(platformObj.getPictureResolution());
						chapterPlatforms.add(platform);
					}

					chapter.setPlatforms(chapterPlatforms);
					listOfChapters.add(chapter);

					if (chapterLanguages != null) {
						// iterating <Multilanguage> in <Chapter>
						for (MultiLanguageChapterDTO chapterLanguage : chapterLanguages) {
							if (contentLanguageList.contains(chapterLanguage.getMetadataLanguage())) {
								// for each <ChapterLanguage>
								MultilanguageChapterEntity multilanguageChapter = new MultilanguageChapterEntity();
								MultilanguageChapterEntityPK multilanguageChapterPK = new MultilanguageChapterEntityPK();
								multilanguageChapterPK.setChapterId(chapterObj.getChapterId());
								multilanguageChapterPK.setContentId(content.getContentId().longValue());
								multilanguageChapterPK.setMetadataLanguage(chapterLanguage.getMetadataLanguage());
								multilanguageChapter.setId(multilanguageChapterPK);
								multilanguageChapter.setBriefTitle(chapterLanguage.getBriefTitle());
								multilanguageChapter
										.setEndTime(vodUtilitiesV1.getDateFromHHMMSS(chapterObj.getEndTime(), "Chapter startTime"));
								multilanguageChapter.setOrderId(chapterLanguage.getOrderId());
								multilanguageChapter
										.setStartTime(vodUtilitiesV1.getDateFromHHMMSS(chapterObj.getStartTime(),"Chapter endTime"));
								listOfMultilanguageChapters.add(multilanguageChapter);
							} else {
									throw new ApplicationException("ERROR_BE_ACTION_ACN_9817_CONTENT_NOT_SUPPORT_BY_MULTI_LANGUAGE_CHAPTER");
							}
						}
					}
				} else {
						throw new ApplicationException("ERROR_BE_ACTION_ACN_9818_CONTENT_NOT_SUPPORT_BY_CHAPTER");
				}
			}
		}

		// START: Operator defined Attributes of AVS 4.3
		Map<ObjectEmfEntityPK, String> contentEmfMap = new LinkedHashMap<ObjectEmfEntityPK, String>();
		Set<ObjectEmfEntity> contentObjectEmfs = new LinkedHashSet<ObjectEmfEntity>();
		List<EmfAttributeDTO> emfAttributes = vodDTO.getEmfAttributes();
		if (emfAttributes != null) {
			for (EmfAttributeDTO emfAttribute : emfAttributes) {
				if (contentLanguageList.contains(emfAttribute.getMetadataLanguage())) {
					if (emfAttribute.getEmfName() != null && !emfAttribute.getEmfName().isEmpty()) {
						ObjectEmfEntityPK objectEmfPK = new ObjectEmfEntityPK();
						objectEmfPK.setEmfId(emfAttribute.getEmfId().intValue());
						objectEmfPK.setLanguageCode(emfAttribute.getMetadataLanguage());
						objectEmfPK.setObjId(content.getContentId());
						objectEmfPK.setObjType(1);

						// seperator(#:&) is used for appending EMF name and value
						contentEmfMap.put(objectEmfPK, emfAttribute.getEmfName() + "#:&" + emfAttribute.getEmfValue());
					}
				} else {
						throw new ApplicationException("ERROR_BE_ACTION_ACN_9814_EMF_NOT_SUPPORT_BY_CONTENT_LANGUAGE");
				}
			}
		}

		// Set<ObjectEmfPK> ids = contentEmfMap.keySet();

		// sonar fixes for Performance - Inefficient use of keySet iterator instead of
		// entrySet iterator
		for (Map.Entry<ObjectEmfEntityPK, String> entry : contentEmfMap.entrySet()) {
			ObjectEmfEntity oe = new ObjectEmfEntity();
			ObjectEmfEntityPK objectEmfPK = entry.getKey();
			oe.setId(objectEmfPK);
			String[] nameValue = contentEmfMap.get(objectEmfPK).split("#:&");
			oe.setEmfName(nameValue[0]);
			oe.setEmfValue(nameValue[1]);
			contentObjectEmfs.add(oe);
		}
		/*
		 * for (ObjectEmfPK objectEmfPK : ids) { ObjectEmf oe = new ObjectEmf();
		 * oe.setId(objectEmfPK); String[] nameValue =
		 * contentEmfMap.get(objectEmfPK).split("#:&"); oe.setEmfName(nameValue[0]);
		 * oe.setValue(nameValue[1]); contentObjectEmfs.add(oe); }
		 */

		objectEmfAttributes = contentObjectEmfs;
		// END: Operator defined Attributes of AVS 4.3

		if (!StringUtils.isEmpty(vodDTO.getExtendedMetadata()) && !JsonUtils.isJSONValid(vodDTO.getExtendedMetadata())) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters("ExtendedMetadata"));

		}

		// AVS-12973 stop
		if (!Utilities.isEmptyCollection(trcEnablerRequestList)) {
			trcEnablerRequest = trcEnablerRequestList;
		}

		Set<ContentPlatformEntity> contentPlatformSet = writeContent(content, contentPlatforms, relContentExtRats,
				contentCategories, listOfScenes, listOfChapters, multiLanguageContents, listOfMultilanguageScenes,
				listOfMultilanguageChapters, scenePlatforms, chapterPlatforms, objectEmfAttributes,
				blacklistDeviceEntities, languageMetadataSet, config);

		if ("N".equalsIgnoreCase(config.getConstants().getValue(CacheConstants.SDP_IN_PANIC))) {
			try {
				sdpManager.callToSDP(null,trcEnablerRequest, content, contentPlatformSet, null, config);
			} catch (Exception e) {
				log.logError(e);
				throw e;
			}
		} else {
			log.logMessage("Skiping all SDP calls");
		}

		return content.getContentId();

	}

	private ContentEntity getContentDetailByContentId(Integer contentId) {
		log.logMessage("In getContentDetailByContentId Start ");
		// String query = "from com.accenture.avs.ci.domain.Content content where
		// content.contentId=?";
		ContentEntity content = null;
		if (contentRepository.existsById(contentId)) {
			content = contentRepository.findById(contentId).get();
		}

		log.logMessage("In getContentDetailByContentId End ");
		return content;
	}

	private boolean validateContentPlatform(Set<Object> contentPlatformVideoType,
			Set<Object> defaultContentPlatformVideoType) {

		boolean flag = false;
		if (contentPlatformVideoType != null && defaultContentPlatformVideoType != null
				&& !contentPlatformVideoType.isEmpty() && !defaultContentPlatformVideoType.isEmpty()) {
			contentPlatformVideoType.removeAll(defaultContentPlatformVideoType);
			flag = contentPlatformVideoType.isEmpty();
		}

		return flag;
	}

	private void updateTrailerAndPosterUrl(Set<ContentPlatformEntity> contentContentPlatform,
			List<ContentPlatformEntity> trailorContentPlatforms, List<ContentPlatformEntity> posterContentPlatforms,
			boolean isBundleOrGroupOfBundle) {

		trailorContentPlatforms = trailorContentPlatforms == null ? new ArrayList<ContentPlatformEntity>()
				: trailorContentPlatforms;
		posterContentPlatforms = posterContentPlatforms == null ? new ArrayList<ContentPlatformEntity>()
				: posterContentPlatforms;

		for (ContentPlatformEntity contentPlatform : contentContentPlatform) {
			if ('Y' == contentPlatform.getIsPublished()) {
				for (ContentPlatformEntity trailorContentPlatform : trailorContentPlatforms) {
					if ('Y' == trailorContentPlatform.getIsPublished()) {
						if (isBundleOrGroupOfBundle) {
							// for bundle/group of bundles, skip videoType check
							if (contentPlatform.getPlatform().equals(trailorContentPlatform.getPlatform())) {
								contentPlatform.setTrailerUrl(trailorContentPlatform.getVideoUrl());
							}
						} else {
							if (contentPlatform.getPlatform().equals(trailorContentPlatform.getPlatform())
									&& contentPlatform.getVideoName().equals(trailorContentPlatform.getVideoName())) {
								contentPlatform.setTrailerUrl(trailorContentPlatform.getVideoUrl());
							}
						}
					}
				}
				for (ContentPlatformEntity posterContentPlatform : posterContentPlatforms) {
					if ('Y' == posterContentPlatform.getIsPublished()) {
						if (isBundleOrGroupOfBundle) {
							// for bundle/group of bundles, skip videoType check
							if (contentPlatform.getPlatform().equals(posterContentPlatform.getPlatform())) {
								contentPlatform.setPictureUrl(posterContentPlatform.getPictureUrl());
							}
						} else {
							if (contentPlatform.getPlatform().equals(posterContentPlatform.getPlatform())
									&& contentPlatform.getVideoName().equals(posterContentPlatform.getVideoName())) {
								contentPlatform.setPictureUrl(posterContentPlatform.getPictureUrl());
							}
						}
					}
				}
			}
		}
	}

	private Set<Object> getContentPlatformVideoType(List<ContentPlatformEntity> contentPlatforms,
			boolean isBundleOrGroupOfBundle, Configuration config) throws ConfigurationException, ApplicationException {
		log.logMessage("In getContentPlatformVideoType ");
		Set<Object> contentPlatformVideoTypeSet = new HashSet<Object>();
		for (ContentPlatformEntity platform : contentPlatforms) {
			if ('Y' == platform.getIsPublished()) {
				if (isBundleOrGroupOfBundle) {
					contentPlatformVideoTypeSet.add(platform.getPlatform());
				} else {
					ContentPlatformVideoType contentPlatformVideoType = new ContentPlatformVideoType();
					// Platform validation AVS 6634
					// if("Y".equalsIgnoreCase(platformMap.get(platform.getPlatform()))){
					if (config.getPlatforms().isValidKey(platform.getPlatform())) {
						contentPlatformVideoType.setPlatform(platform.getPlatform());
					} else {
						throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
								new NestedParameters("Platform does not exists or inactive"));
					}
					// Videotype validation AVS 6634
					if (config.getVideoTypeCache().isValidKey(platform.getVideoName())) {
						contentPlatformVideoType.setVideoType(platform.getVideoName());
					} else {
						throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
								new NestedParameters("VideoType does not exists or inactive"));
					}
					contentPlatformVideoTypeSet.add(contentPlatformVideoType);
				}
			}
		}
		return contentPlatformVideoTypeSet;
	}

	// Start: Added for 4.2 New Object

	/**
	 * @param channel
	 * @since 4.2 Create Extended Attribute Object.
	 * 
	 * @param Asset
	 *            asset
	 * @return ExtendedContentAttributes
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 * @throws Exception
	 * 
	 */
	private ExtendedContentAttributeEntity getExtendedContentAttributes(VodRequestDTO asset, ChannelEntity channel) throws JsonParseException, JsonMappingException, IOException {
		ExtendedContentAttributeEntity extendedContentAttributes = new ExtendedContentAttributeEntity();
		extendedContentAttributes.setAdvTags(asset.getAdvTags());

		if (asset.getIsSkipJumpEnabled() != null && asset.getIsSkipJumpEnabled() != null) {
			extendedContentAttributes.setIsSkipJumpEnabled(asset.getIsSkipJumpEnabled());
		} else {
			extendedContentAttributes.setIsSkipJumpEnabled("N");
		}

		if (asset.getIsTrickPlayEnabled() != null && asset.getIsTrickPlayEnabled() != null) {
			extendedContentAttributes.setIsTrickPlayEnabled(asset.getIsTrickPlayEnabled());
		} else {
			extendedContentAttributes.setIsTrickPlayEnabled("N");
		}

		if (asset.getIsDisAllowedAdv() != null && asset.getIsDisAllowedAdv() != null) {
			extendedContentAttributes.setDisAllowedAdv(asset.getIsDisAllowedAdv());
		} else {
			extendedContentAttributes.setDisAllowedAdv("N");
		}

		extendedContentAttributes.setContentId(asset.getContentId().intValue());
		if (asset.getIsParentObject() == null) {
			extendedContentAttributes.setIsParent("Y");
		} else {
			extendedContentAttributes.setIsParent(asset.getIsParentObject());
		}
		if (asset.getIsLatest() != null) {
			extendedContentAttributes.setLatest(asset.getIsLatest());
		}
		extendedContentAttributes.setObjectSubtype(asset.getContentSubType());
		extendedContentAttributes.setObjectType(asset.getContentType());
		if (asset.getIsOnAir() != null) {
			extendedContentAttributes.setOnAir(asset.getIsOnAir());
		}
		if (asset.getIsPopularEpisode() != null) {
			extendedContentAttributes.setPopularEpisode(asset.getIsPopularEpisode());
		}
		extendedContentAttributes.setTitleBrief(asset.getTitleBrief());

		if (asset.getOriginalAirDate() != null) {
			extendedContentAttributes
					.setOriginalAirDate(asset.getOriginalAirDate());
		}
		if (asset.getSearchKeywords() != null && !asset.getSearchKeywords().isEmpty()) {

			extendedContentAttributes
					.setSerchKeywords(Validator.list2String(asset.getSearchKeywords(), "|", 1000));
		}

		if (asset.getDefaultLanguage() != null) {
			extendedContentAttributes.setDefaultLang(asset.getDefaultLanguage());
		}

		if (asset.getIsSurroundSound() != null) {
			extendedContentAttributes.setIsSurroundSound(asset.getIsSurroundSound());
		}
		extendedContentAttributes.setItemURL(asset.getItemUrl());

		if (asset.getProgramReferenceName() != null) {
			extendedContentAttributes.setProgramReferenceName(asset.getProgramReferenceName());
		}

		if (channel != null) {
			extendedContentAttributes.setBroadcastChannelId(channel.getChannelId());
		}

		if (asset.getProgramCategory() != null) {
			extendedContentAttributes.setProgramCategory(asset.getProgramCategory());
		}
		/*
		 * If the Job is called from web then file path will be set in Job service. If
		 * the Job is called from batch then file path taken from fileNameVod in the
		 * JobContext.
		 * 
		 * 
		 */

		String contentJSONStr = JsonUtils.writeAsJsonStringWithoutNull(asset);
		if (!StringUtils.isEmpty(contentJSONStr)) {
			// extendedContentAttributes.setSourceFile(xmlString.getBytes());
			extendedContentAttributes.setSourceFile(contentJSONStr.getBytes("UTF-8"));
		}
		return extendedContentAttributes;
	}

	/**
	 * @since 4.2 Create Content Linking Object.
	 * 
	 * @param Asset
	 *            asset
	 * @return Set<ContentLinking>
	 * 
	 */
	private Set<ContentLinkingEntity> getContentLinking(VodRequestDTO asset) {
		Set<ContentLinkingEntity> contentLinkings = null;
		ContentLinkingEntity contentLinking = null;
		ContentLinkingEntityPK linkingPK = null;
		if (asset.getContentLinkings() != null && !asset.getContentLinkings().isEmpty()) {
			contentLinkings = new LinkedHashSet<ContentLinkingEntity>(
					asset.getContentLinkings().size());
			for (ContentLinkingDTO contentLinkingDTO : asset
					.getContentLinkings()) {
				contentLinking = new ContentLinkingEntity();
				contentLinking.setChildSubtype(contentLinkingDTO.getSubType());
				linkingPK = new ContentLinkingEntityPK();
				linkingPK.setChildContentId(Integer.parseInt(String.valueOf(contentLinkingDTO.getContentId())));
				linkingPK.setParentContentId(asset.getContentId().intValue());
				contentLinking.setId(linkingPK);

				if (contentLinkingDTO.getIsDefault() != null) {
					contentLinking.setIsChildDefault(contentLinkingDTO.getIsDefault());
				}
				if (contentLinkingDTO.getOrderId() != null) {
					contentLinking.setOrderId(contentLinkingDTO.getOrderId().intValue());
				}

				contentLinkings.add(contentLinking);
			}
		}
		return contentLinkings;
	}

	// End: Added for 4.2 New Object

	private Set<BlacklistDeviceEntity> getBlacklistDeviceContent(VodRequestDTO asset) throws ApplicationException {
		Set<BlacklistDeviceEntity> blacklistDeviceContents = new HashSet<BlacklistDeviceEntity>();
		List<String> deviceTypeList = asset.getBlackListDeviceTypes();
		if (deviceTypeList != null) {
			if (deviceTypeList != null && !deviceTypeList.isEmpty()) {

				Map<String, Long> deviceChannels = deviceChannelsCache.getDeviceChannels();
				if(deviceChannels==null) {
					throw new ApplicationException(MessageKeys.ERROR_MS_ACN_916_SDP_NOT_AVAILABLE);
				}
				for (Iterator<String> iterator = deviceTypeList.iterator(); iterator.hasNext();) {
					String deviceTypes = iterator.next();

					if (deviceChannels.containsKey(deviceTypes)) {

						BlacklistDeviceEntity blacklistDeviceContent = new BlacklistDeviceEntity();
						BlackListDeviceEntityPK blacklistDeviceContentPK = new BlackListDeviceEntityPK();
						blacklistDeviceContentPK.setDevicetypeId(deviceChannels.get(deviceTypes));
						blacklistDeviceContentPK.setContentId(asset.getContentId().intValue());
						blacklistDeviceContent.setId(blacklistDeviceContentPK);
						blacklistDeviceContents.add(blacklistDeviceContent);
					}

					else {
						log.logMessage("unknown device type " + deviceTypeList);
						throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
								new NestedParameters("Devicetype:" + deviceTypes));
					}
				}
			}

		}
		return blacklistDeviceContents;
	}

	/**
	 * Support method for retrieve channelName by the channelId.
	 * 
	 * @param channelId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private ChannelEntity getChannelName(Long channelId) {
		ChannelEntity channel = null;
		if (channelRepository.existsById(channelId.intValue())) {
			channel = channelRepository.findById(channelId.intValue()).get();
		}
		return channel;
	}

	/**
	 * Retrieve the set of Platform objects related to the Content.
	 * 
	 * @param Asset
	 *            vodElement
	 * @param ExtensionsDTO
	 *            extensions
	 * @throws ApplicationException
	 * @throws ConfigurationException
	 * @throws Exception
	 * 
	 */
	private Set<ContentPlatformEntity> getVodContentPlatform(VodRequestDTO vodElement, ExtensionsDTO extensions,
			Configuration config) throws ApplicationException, ConfigurationException {
		Set<ContentPlatformEntity> platformContentList = new HashSet<ContentPlatformEntity>();

		if (extensions != null) {
			List<PlatformDTO> platformListObj = extensions.getPlatforms();
				for (PlatformDTO platformDTO : platformListObj) {
					ContentPlatformEntity contentPlatform = new ContentPlatformEntity();

					if (!StringUtils.isEmpty(platformDTO.getExtendedMetadata())
							&& !JsonUtils.isJSONValid(platformDTO.getExtendedMetadata())) {
						throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
								new NestedParameters("ExtendedMetadata"));
					}

					contentPlatform.setContentId(vodElement.getContentId().intValue());

					// fix for defect AVS-6885
					if (null != platformDTO.getPlatformName() && config.getPlatforms().isValidKey(platformDTO.getPlatformName()))
						contentPlatform.setPlatform(platformDTO.getPlatformName());
					else {

						throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
								new NestedParameters("Platform"));

					}
					if (platformDTO.getVideoType() != null && !platformDTO.getVideoType().equals("")) {
						if (config.getVideoTypeCache().isValidKey(platformDTO.getVideoType())) {
							contentPlatform.setVideoName(platformDTO.getVideoType());
						} else {
							throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
									new NestedParameters("VideoType"));
						}
					} else {
						contentPlatform.setVideoName("NA");
					}
					contentPlatform.setTrailerUrl(platformDTO.getTrailerUrl());
					contentPlatform.setVideoUrl(platformDTO.getVideoUrl());
					contentPlatform.setIsPublished(DEFAULT_IS_PUSLISHED.charAt(0));// always Y when a platform exists in
																					// xml
					contentPlatform.setPictureUrl(platformDTO.getPictureUrl());
					contentPlatform.setDrmInfo(platformDTO.getDrmInfo());
					if (platformDTO.getDownload() != null) {
						if (platformDTO.getDownload().getDownloadMaxDays() != null) {
							contentPlatform.setDwnMaxDays(platformDTO.getDownload().getDownloadMaxDays().intValue());
						}
						if (platformDTO.getDownload().getDownloadMaxNumber() != null) {
							contentPlatform.setDwnNumber(platformDTO.getDownload().getDownloadMaxNumber().intValue());
						}

						if (platformDTO.getDownload().getDownloadHoursFromFirstPlay() != null) {
							contentPlatform
									.setDwnNumberHours(platformDTO.getDownload().getDownloadHoursFromFirstPlay().intValue());
						}
					}

					if (platformDTO.getStreamingType() != null) {
						contentPlatform.setStreamingType(platformDTO.getStreamingType());
					}

					if (platformDTO.getContractStartDate() == null) {
						contentPlatform
						.setContractStart(vodElement.getContractStartDate());
					}else {
						contentPlatform
								.setContractStart(platformDTO.getContractStartDate());
					}

					if (platformDTO.getContractEndDate() == null) {
						contentPlatform
						.setContractEnd(vodElement.getContractEndDate());
					}else {
						contentPlatform
						.setContractEnd(platformDTO.getContractEndDate());
					}
/*
					if (vodElement.getLanguage() == null && vodElement.getLanguages() == null
							&& platformDTO.getLanguages() == null) {
						throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
								new NestedParameters(
										"Both 'languages' and 'language' element are missed in vod xml file."));
					}*/

					// Manage releted table Audiolang, Subtitles
					contentPlatform.setAudiolangs(getVodAudiolang(platformDTO.getAudioLanguages(), vodElement.getContentId()));
					contentPlatform.setSubtitles(getVodSubtitles(platformDTO.getSubtitles(), vodElement.getContentId(),
							contentPlatform.getCpId()));
					contentPlatform.setRelPlatformTechnicals(
							getVodRelPlatformTechnical(platformDTO.getPackageIds(), vodElement.getContentId()));

					platformContentList.add(contentPlatform);
				}
		}
		return platformContentList;
	}

	/**
	 * Retrieve the set of RelContentExtRat objects related to the Content.
	 * 
	 * @param Asset
	 *            vodElement
	 * @param ExtensionsDTO
	 *            extensions
	 * @throws ConfigurationException
	 * 
	 */
	private Set<RelContentExtRatEntity> getVodRelContentExtRat(VodRequestDTO vodElement, ExtensionsDTO extensions,
			Configuration config) throws ConfigurationException {
		Set<RelContentExtRatEntity> relContentExtRatList = new HashSet<RelContentExtRatEntity>();

		if (extensions == null) {
			if (vodElement.getContentId() != null) {
				log.logMessage("No ExtendedRating related to the Content in the Asset XML file for contentId "
						+ vodElement.getContentId());
			} else {
				log.logMessage(
						"No ExtendedRating related to the Content in the Asset XML file for externalContentId "
								+ vodElement.getExternalId());
			}
	} else {
		List<String> extRatingList = extensions.getExtendedRatings();// tag ExtendedRating
		if (extRatingList != null && !extRatingList.isEmpty()) {// if exists rate type

			for (String extendedRating : extRatingList) {
				RelContentExtRatEntityPK relContentExtRatPK = new RelContentExtRatEntityPK();
				RelContentExtRatEntity relContentExtRat = new RelContentExtRatEntity();
				relContentExtRatPK.setContentId(vodElement.getContentId().intValue());

				String pcLevel = config.getPcExtendedRatings().getKey(extendedRating);
				// Long pcLevel = pcExtRatMap.get(iterator.next().value());

				if (pcLevel == null) {
					log.logMessage(
							"No PC level retrieved from PcExternalRating in input: " + extendedRating);
				}
				else{
				relContentExtRatPK.setPcId(Integer.parseInt(pcLevel));
				}
				relContentExtRat.setId(relContentExtRatPK);
				relContentExtRatList.add(relContentExtRat);
			}
		}
	
	}
		return relContentExtRatList;

	}

	/**
	 * Retrieve the set of RelPlatformTechnical objects related to the
	 * ContentPlatform variant.
	 * 
	 * @param PackageList
	 *            platformPackageList
	 * @param Long
	 *            contentId
	 * @throws ApplicationException
	 * 
	 */
	private List<RelPlatformTechnicalEntity> getVodRelPlatformTechnical(List<Integer> packageIdList, Long contentId)
			throws ApplicationException {
		List<RelPlatformTechnicalEntity> relPlatformTechnicalList = new ArrayList<RelPlatformTechnicalEntity>();
		if (packageIdList != null) {

			Map<Integer, String> technicalPackages = new HashMap<>();

			List<Object[]> technicalPackageObject = technicalPackageRepository.retrievePackagesNotRvodByPackageIds(packageIdList);
			for(Object[] technicalPackage : technicalPackageObject) {
				technicalPackages.put((int)technicalPackage[0], technicalPackage[1]!=null?technicalPackage[1].toString():null);
			}
			List<String> technicalPkgProperties = new ArrayList<>();

			for (Integer packageId : packageIdList) {
				String propertyName = technicalPackages.get(packageId);
				technicalPkgProperties.add(propertyName);
			}

			if (propertiesList.isEmpty()) {
				for (String propertyName : technicalPkgProperties) {
					if (!StringUtils.isEmpty(propertyName)) {
						throw new ApplicationException("ERROR_BE_ACTION_ACN_9819_INCOMPATIBILITY_ASSET_TECH_PKG_PROPERTIES");

					}
				}
			
			} else {
				for (String propertyName : propertiesList) {
					if (technicalPkgProperties != null && !technicalPkgProperties.contains(null)
							&& !technicalPkgProperties.contains(propertyName)) {
						throw new ApplicationException("ERROR_BE_ACTION_ACN_9819_INCOMPATIBILITY_ASSET_TECH_PKG_PROPERTIES");

					}
				}
			}

			for (Integer packageId :  packageIdList) {

				if (technicalPackages.containsKey(packageId.intValue())) {

					RelPlatformTechnicalEntity relPlatformTechnical = new RelPlatformTechnicalEntity();
					RelPlatformTechnicalEntityPK relPlatformTechnicalPK = new RelPlatformTechnicalEntityPK();
					relPlatformTechnicalPK.setPackageId(packageId.intValue());
					relPlatformTechnicalPK.setSbundleId(-1);
					ContentEntity contentEntity = new ContentEntity();
					contentEntity.setContentId(contentId.intValue());
					relPlatformTechnical.setContent(contentEntity);
					relPlatformTechnical.setId(relPlatformTechnicalPK);
					relPlatformTechnicalList.add(relPlatformTechnical);
				} else {
					log.logMessage("Package id " + packageId
							+ " not found. No association is created for this package in RelContentTechnical.");
				}
			}
		}

		return relPlatformTechnicalList;

	}
	

	/**
	 * Retrieve the set of ContentCategory objects related to the Content.
	 * 
	 * @param Asset
	 *            vodElement
	 * @param ExtensionsDTO
	 *            extensions
	 * 
	 */
	private Set<ContentCategoryEntity> getVodContentCategory(VodRequestDTO vodElement, ExtensionsDTO extensions) {
		Set<ContentCategoryEntity> contentCategoryList = new HashSet<ContentCategoryEntity>();
		if (extensions != null) {
				List<CategoryDTO> categoryIdList = extensions.getCategories();
				if (categoryIdList != null) {
					for (CategoryDTO categoryDTO : categoryIdList) {
						ContentCategoryEntity contentCategory = new ContentCategoryEntity();
						Long categoryId = getCategoryIdFromExt(categoryDTO.getCategoryExternalId());
						if (categoryId == null) {
							log.logMessage(
									"No category retrieved from externalId in input. Any record inserted in Content_Category");
						} else {
							ContentCategoryEntityPK contentCategoryPk = new ContentCategoryEntityPK();
							contentCategoryPk.setCategoryId(categoryId.intValue());
							contentCategoryPk.setContentId(vodElement.getContentId().intValue());
							contentCategory.setId(contentCategoryPk);
							if (categoryDTO.getOrderId() != null) {
								contentCategory.setPosition(categoryDTO.getOrderId().intValue());}

							// Start: Added for 4.2 New Object
							if (categoryDTO.getIsPrimary() == null) {
								contentCategory.setIsPrimary("N");
							} else {
								contentCategory.setIsPrimary(categoryDTO.getIsPrimary());
							}

							if (categoryDTO.getStartDate() != null) {
								contentCategory.setStartDate(categoryDTO.getStartDate());}

							if (categoryDTO.getEndDate() != null) {
								contentCategory
										.setEndDate(categoryDTO.getEndDate());}
							// End: Added for 4.2 New Object

							contentCategoryList.add(contentCategory);
						}

					}
				}
		}
		return contentCategoryList;
	}

	/**
	 * Support method for retrieve categoryId by the externalCategory id.
	 * 
	 * @param channelId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Long getCategoryIdFromExt(String categoryExternalId) {
		Long categoryId = null;
		List<CategoryEntity> category = new ArrayList<>();
		try {
			category = categoryRepository.retrieveByExternald(categoryExternalId);
		} catch (Exception e) {
			log.logError(e, "Error during retrieving categoryId from database by externalId " + categoryExternalId);
		}
		if (category != null && !category.isEmpty()) {
			categoryId = category.get(0).getCategoryId().longValue();
		} else {
			log.logMessage("No results found for category with externalId: " + categoryExternalId);
		}

		return categoryId;
	}

	/**
	 * Retrieve the set of Audiolang objects related to the ContentPlatform
	 * variants.
	 * 
	 * @param CategoryContent
	 *            vodElement
	 * 
	 */
	private List<AudiolangEntity> getVodAudiolang(List<AudioLanguageDTO> languages, Long contentId) {
		List<AudiolangEntity> contentAudiolangList = new ArrayList<AudiolangEntity>();
		if (languages == null) {
			return contentAudiolangList;
		}

		for (AudioLanguageDTO lang : languages) {
			contentAudiolangList.add(getAudiolang(lang, contentId));
		}

		return contentAudiolangList;
	}

	/**
	 * Support method for retrieve Audiolang by the LangType.
	 * 
	 * @param LangType
	 * @param contentId
	 * @param boolean
	 *            isPreferred
	 * @return
	 */
	private AudiolangEntity getAudiolang(AudioLanguageDTO lang, Long contentId) {
		AudiolangEntity al = new AudiolangEntity();
		AudiolangEntityPK alPK = new AudiolangEntityPK();

		alPK.setLangId(lang.getAudioId());
		ContentEntity content = new ContentEntity();
		content.setContentId(contentId.intValue());
		al.setContent(content);
		al.setId(alPK);
		al.setLabel(lang.getAudioLanguageName());

		if (lang.getIsPreferred()==null) {
			al.setIsPreferred("N");
		} else {
			al.setIsPreferred(lang.getIsPreferred());
		}

		al.setStreamId(lang.getStreamId());
		if (lang.getAudioLanguageCode() != null) {
			al.setLangCode(lang.getAudioLanguageCode());
		}
		return al;
	}

	/**
	 * Retrieve the set of Subtitles objects related to the Content.
	 * 
	 * @param CategoryContent
	 *            vodElement
	 * 
	 */
	private List<SubtitleEntity> getVodSubtitles(List<SubTitleDTO> platformSubtitle, Long contentId,
			Integer cpId) {
		List<SubtitleEntity> contentSubtitlesList = new ArrayList<SubtitleEntity>();

		if (platformSubtitle == null) {
			return contentSubtitlesList;
		}

		for (SubTitleDTO sub : platformSubtitle) {
			SubtitleEntity subtitle = new SubtitleEntity();
			SubtitleEntityPK subtitlePK = new SubtitleEntityPK();

			ContentEntity content = new ContentEntity();
			content.setContentId(contentId.intValue());

			subtitlePK.setCpId(cpId);
			subtitlePK.setSubId(sub.getSubtitleId());
			subtitle.setContent(content);
			subtitle.setId(subtitlePK);
			subtitle.setLabel(sub.getSubtitleLanguageName());

			contentSubtitlesList.add(subtitle);
		}
		return contentSubtitlesList;
	}

	/**
	 * Retrieve the set of Subtitles objects related to the Content.
	 * 
	 * @param Asset
	 *            vodElement
	 * 
	 */
	@SuppressWarnings("unused")
	private Set<SubtitleEntity> getVodSubtitles(VodRequestDTO vodElement) {
		Set<SubtitleEntity> contentSubtitlesList = new HashSet<SubtitleEntity>();

		if (vodElement.getSubtitles() == null) {
			return contentSubtitlesList;
		}

		for (SubTitleDTO sub : vodElement.getSubtitles()) {
			SubtitleEntity subtitle = new SubtitleEntity();
			SubtitleEntityPK subtitlePK = new SubtitleEntityPK();
			ContentEntity content = new ContentEntity();
			content.setContentId(vodElement.getContentId().intValue());

			subtitlePK.setSubId(sub.getSubtitleId());
			subtitle.setContent(content);
			subtitle.setId(subtitlePK);
			subtitle.setLabel(sub.getSubtitleLanguageName());

			contentSubtitlesList.add(subtitle);
		}
		return contentSubtitlesList;
	}

	private String stringWithPipeComma(List<String> dataList, String pipe) {
		String idList = dataList.toString();
		String csv = idList.substring(1, idList.length() - 1).replace(pipe, pipe);
		return csv;
	}

	private static boolean validateLangCode(String langCode) {
		boolean validateLang = true;
		String[] languages = Locale.getISOLanguages();
		Map<String, Locale> localeMap = new HashMap<String, Locale>(languages.length);
		for (String language : languages) {
			Locale locale = new Locale(language);
			localeMap.put(locale.getISO3Language(), locale);
		}
		Set<String> ss = localeMap.keySet();
		for (Object o : ss) {
			if (langCode.equalsIgnoreCase(o.toString())) {
				validateLang = false;
			}
		}

		return validateLang;
	}

	private void validateTRCList(Map<Long, GetTransactionRateCardResponse> trcIdCache,
			Map<String, GetTransactionRateCardResponse> trcNameCache, List<TransactionRateCardDTO> trcList) throws ApplicationException {

		List<GetTransactionRateCardResponse> transactionRateCardList = new ArrayList<>();
		List<GetTransactionRateCardResponse> transactionRateCardCopyList = new ArrayList<>();

		for (TransactionRateCardDTO trc : trcList) {
			GetTransactionRateCardResponse trcResponse = null;
			if (null == trc.getTransactionRateCardId()) {
				trcResponse = trcNameCache.get(trc.getExternalId());
			} else {
				trcResponse = trcIdCache.get(trc.getTransactionRateCardId());
			}

			if (null == trcResponse || ("N").equals(trcResponse.getIsActive())) {
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
						new NestedParameters("TRC"));

			}
			transactionRateCardList.add(trcResponse);
			transactionRateCardCopyList.add(trcResponse);
		}

		for (GetTransactionRateCardResponse copyTRC : transactionRateCardCopyList) {

			Long rentalPeriod = copyTRC.getRentalPeriod();
			Date startDate = copyTRC.getStartDate();
			Date endDate = copyTRC.getEndDate();
			List<String> platforms = copyTRC.getPlatformList();
			List<String> videoTypes = copyTRC.getVideoTypeList();
			String priceCategoryId = copyTRC.getPriceCategory();

			for (GetTransactionRateCardResponse trc : transactionRateCardList) {

				boolean isExceptionRequired = false;
				boolean overLappingTRC = false;
				if (null == copyTRC.getTransactionRateCardId()
						|| !copyTRC.getTransactionRateCardId().equals(trc.getTransactionRateCardId())) {

					if (null != trc.getPlatformList() && null != platforms) {
						if (checkPlatformsAreEqual(trc.getPlatformList(), platforms)) {
							isExceptionRequired = true;
							overLappingTRC = true;
						} else {
							isExceptionRequired = false;
							overLappingTRC = false;
						}
					} else {
						isExceptionRequired = true;
						overLappingTRC = true;
					}

					if (isExceptionRequired) {
						if (null != trc.getVideoTypeList() && null != videoTypes) {

							if (checkVideoTypesAreEqual(trc.getVideoTypeList(), videoTypes)) {
								isExceptionRequired = true;
								overLappingTRC = true;
							} else {
								isExceptionRequired = false;
								overLappingTRC = false;
							}
						} else {
							isExceptionRequired = true;
							overLappingTRC = true;
						}
					}

					if (isExceptionRequired) {
						if (trc.getPriceCategory().equals(priceCategoryId)) {
							isExceptionRequired = true;
							overLappingTRC = true;
						} else {
							isExceptionRequired = false;
							overLappingTRC = false;
						}
					}

					if (isExceptionRequired) {
						if (trc.getRentalPeriod().equals(rentalPeriod)) {
							isExceptionRequired = true;
							overLappingTRC = true;
						} else {
							isExceptionRequired = false;
							overLappingTRC = false;
						}
					}

					if (isExceptionRequired) {
						if (trc.getStartDate().getTime() == startDate.getTime()) {
							isExceptionRequired = true;
							overLappingTRC = false;
						} else {
							isExceptionRequired = false;
							overLappingTRC = true;
						}
					}

					if (isExceptionRequired) {
						if ((trc.getEndDate() == null && endDate == null) || (trc.getEndDate() != null
								&& endDate != null && (trc.getEndDate().getTime() == endDate.getTime()))) {
							isExceptionRequired = true;
							overLappingTRC = false;
						} else {
							isExceptionRequired = false;
							overLappingTRC = true;
						}
					}

					if (overLappingTRC) {
						if (!((trc.getStartDate().before(startDate)
								&& (trc.getEndDate() != null && trc.getEndDate().before(startDate)))
								|| (startDate.before(trc.getStartDate())
										&& (endDate != null && endDate.before(trc.getStartDate()))))) {
							throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
									new NestedParameters("TRC[" + copyTRC.getName()
											+ "] startDate and endDate is overlapping with other TRC[" + trc.getName()
											+ "]"));

						}

					}

				}
			}

		}
	}

	/**
	 * This method is used to check the requested platfroms and existed platforms
	 * are equal or not.
	 * 
	 * @param platforms
	 * @param platformsNew
	 * @param config
	 * @throws ActionException
	 * @throws ConfigurationException
	 */
	public static boolean checkPlatformsAreEqual(List<String> platformList, List<String> platformListNew) {
		boolean isConflicted = false;

		final Set<String> platformSet = new HashSet<>(platformList);
		final Set<String> newPlatformSet = new HashSet<>(platformListNew);

		// Set<String> commonPlatforms = Sets.intersection(platformSet, newPlatformSet);
		boolean commonPlatformsExisted = platformSet.retainAll(newPlatformSet);
		if (platformSet.equals(newPlatformSet) || commonPlatformsExisted) {
			isConflicted = true;
		}
		return isConflicted;

	}

	/**
	 * This method is used to check the requested videoTypes and existing videoTypes
	 * are equal or not.
	 * 
	 * @param videoTypes
	 * @param videoTypesNew
	 * @param config
	 * @return
	 * @throws ActionException
	 * @throws ConfigurationException
	 */
	public static boolean checkVideoTypesAreEqual(List<String> videoTypeList, List<String> videoTypeListNew) {

		boolean isConflicted = false;
		final Set<String> videoTypeSet = new HashSet<>(videoTypeList);
		final Set<String> newVideoTypeSet = new HashSet<>(videoTypeListNew);
		// Set<String> commonVideoTypes = Sets.intersection(videoTypeSet,
		// newVideoTypeSet);
		boolean commonVideoTypesExisted = videoTypeSet.retainAll(newVideoTypeSet);
		if (videoTypeSet.equals(newVideoTypeSet) || commonVideoTypesExisted) {
			isConflicted = true;
		}

		return isConflicted;
	}

	/**
	 * @param content
	 * @param contentPlatforms
	 * @param relContentExtRats
	 * @param contentCategories
	 * @param listOfScenes
	 * @param listOfChapters
	 * @param multiLanguageContents
	 * @param listOfMultilanguageScenes
	 * @param listOfMultilanguageChapters
	 * @param scenePlatforms
	 * @param chapterPlatforms
	 * @param objectEmfAttributes
	 * @param blacklistDeviceEntities
	 * @param config
	 * @throws ApplicationException
	 * @throws Exception
	 */
	@Transactional(rollbackFor = { ApplicationException.class, RuntimeException.class })
	private Set<ContentPlatformEntity> writeContent(ContentEntity content, Set<ContentPlatformEntity> contentPlatforms,
			Set<RelContentExtRatEntity> relContentExtRats, Set<ContentCategoryEntity> contentCategories,
			Set<SceneEntity> listOfScenes, Set<ChapterEntity> listOfChapters,
			Set<MultiLanguageContentEntity> multiLanguageContents,
			Set<MultilanguageSceneEntity> listOfMultilanguageScenes,
			Set<MultilanguageChapterEntity> listOfMultilanguageChapters, Set<ObjectPlatformEntity> scenePlatforms,
			Set<ObjectPlatformEntity> chapterPlatforms, Set<ObjectEmfEntity> objectEmfAttributes,
			Set<BlacklistDeviceEntity> blacklistDeviceEntities, Set<LanguageMetadataEntity> languages,
			Configuration config) throws ApplicationException {

		Set<ContentPlatformEntity> contentPlatformSet = null;
		try {
			deleteContentRelatedData(content);

			contentRepository.save(content);

			if (languages != null) {
				for (LanguageMetadataEntity language : languages) {
					Integer id = languageRepository.retrieveIdByLanguageCode(language.getLanguageCode());
					if (id == null || id == 0) {
						languageRepository.save(language);
					}
				}

			}

			upsertSegmentDetails(content, listOfScenes, listOfChapters, listOfMultilanguageScenes,
					listOfMultilanguageChapters, scenePlatforms, chapterPlatforms, objectEmfAttributes);

			if (relContentExtRats != null) {
				relcontentExtRatRepository.saveAll(relContentExtRats);
			}

			if (contentCategories != null) {
				contentCategoryRepository.saveAll(contentCategories);
			}
			/*
			 * if (content.getContentLinking() != null) {
			 * contentLinkingRepository.saveAll(content.getContentLinking()); }
			 */
			if (multiLanguageContents != null) {
				multiLanguageContentRepository.saveAll(multiLanguageContents);
			}
			if (listOfScenes != null) {
				sceneRepository.saveAll(listOfScenes);
			}

			if (listOfChapters != null) {
				chapterRepository.saveAll(listOfChapters);
			}

			if (listOfMultilanguageScenes != null) {
				multiLanguageSceneRepository.saveAll(listOfMultilanguageScenes);
			}

			if (listOfMultilanguageChapters != null) {
				multiLanguageChapterRepository.saveAll(listOfMultilanguageChapters);
			}

			if (blacklistDeviceEntities != null) {
				blacklistDeviceContentRepository.saveAll(blacklistDeviceEntities);
			}
			contentPlatformSet = upsertContentPlatforms(contentPlatforms);
		} catch (ApplicationException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException();
		}
		return contentPlatformSet;

	}

	/**
	 * @param contentPlatforms
	 */
	private Set<ContentPlatformEntity> upsertContentPlatforms(Set<ContentPlatformEntity> contentPlatforms) {

		List<RelPlatformTechnicalEntity> relPlatformTechnicalList = new ArrayList<RelPlatformTechnicalEntity>();
		List<AudiolangEntity> audioLangList = new ArrayList<AudiolangEntity>();
		List<SubtitleEntity> subtitlesList = new ArrayList<SubtitleEntity>();
		for (ContentPlatformEntity contentPlatform : contentPlatforms) {

			List<RelPlatformTechnicalEntity> relPlatformTechnicalS = contentPlatform.getRelPlatformTechnicals();
			List<AudiolangEntity> audioLangs = contentPlatform.getAudiolangs();
			List<SubtitleEntity> subtitles = contentPlatform.getSubtitles();
			/* MODIFICA PER NON CANCELLARE PLATFORM */
			Integer cpIdIfExist = getCpIdByKey(contentPlatform);

			if (cpIdIfExist == null) {
				contentPlatform.setCpId(0);
			} else {
				contentPlatform.setCpId(cpIdIfExist);
			}
			Integer cpId = contentPlatformRepository.save(contentPlatform).getCpId();
			contentPlatform.setCpId(cpId);

			// Iterate the set of RelPlatformTechnicals save the RelPlatformTechnical
			// releted to a ContentPlatform
			for (Iterator<RelPlatformTechnicalEntity> iteratorTech = relPlatformTechnicalS.iterator(); iteratorTech
					.hasNext();) {
				RelPlatformTechnicalEntity relPlatformTech = iteratorTech.next();
				relPlatformTech.getId().setCpId(cpId);
				relPlatformTechnicalList.add(relPlatformTech);
			}

			// Iterate the set of Subtitles, save the Subtitle releted to a ContentPlatform
			for (Iterator<SubtitleEntity> iteratorSub = subtitles.iterator(); iteratorSub.hasNext();) {
				SubtitleEntity subtitle = iteratorSub.next();
				subtitle.getId().setCpId(cpId);
				subtitlesList.add(subtitle);
			}

			// Iterate the set of Audiolang, save the Audiolang releted to a ContentPlatform
			for (Iterator<AudiolangEntity> iteratorAud = audioLangs.iterator(); iteratorAud.hasNext();) {
				AudiolangEntity language = iteratorAud.next();
				language.getId().setCpId(cpId);
				audioLangList.add(language);
			}

		}
		relPlatformTechnicalRepository.saveAll(relPlatformTechnicalList);
		subtitlesRepository.saveAll(subtitlesList);
		audioLangRepository.saveAll(audioLangList);

		return contentPlatforms;
	}

	/**
	 * @param contentPlatform
	 * @return
	 */
	private Integer getCpIdByKey(ContentPlatformEntity contentPlatform) {

		Integer cpId = contentPlatformRepository.retriveByVideoTypeAndPlatformAndContentId(
				contentPlatform.getVideoName(), contentPlatform.getPlatform(), contentPlatform.getContentId());

		return cpId;
	}

	/**
	 * @param content
	 * @param listOfScenes
	 * @param listOfChapters
	 * @param listOfMultilanguageScenes
	 * @param listOfMultilanguageChapters
	 * @param scenePlatforms
	 * @param chapterPlatforms
	 * @param objectEmfAttributes
	 * @throws ApplicationException
	 */
	private void upsertSegmentDetails(ContentEntity content, Set<SceneEntity> listOfScenes,
			Set<ChapterEntity> listOfChapters, Set<MultilanguageSceneEntity> listOfMultilanguageScenes,
			Set<MultilanguageChapterEntity> listOfMultilanguageChapters, Set<ObjectPlatformEntity> scenePlatforms,
			Set<ObjectPlatformEntity> chapterPlatforms, Set<ObjectEmfEntity> objectEmfAttributes)
			throws ApplicationException {

		List<ObjectEmfEntity> objectEmfListForContent = new ArrayList<ObjectEmfEntity>();
		List<EmfEntity> emfListForName = null;
		// inserting or updating EMF attributes of Content
		for (ObjectEmfEntity objectEmf : objectEmfAttributes) {
			if (objectEmf != null) {

				emfListForName = emfRepository.retrieveByEmfName(objectEmf.getEmfName());
				// emfListForName = hibernateTemplate.find("from com.accenture.avs.ci.domain.Emf
				// e where e.emfName='"+objectEmf.getEmfName()+"'");
				if (emfListForName.isEmpty()) {
					EmfEntity emf = new EmfEntity();
					emf.setEmfName(objectEmf.getEmfName());
					emf.setIsActive("Y");
					if ("VIDEO".equals(content.getExtendedContentAttribute().getObjectType())) {
						Integer id = emfRepository.getMaxId();
						if(id == null || id == 0){
							id = 0;
						}
						emf.setEmfId(id + 1);
						emfRepository.save(emf);

						/*
						 * Serializable conEMFId = hibernateTemplate.save(emf); Long generatedConEMFId =
						 * (Long)conEMFId;
						 */
						objectEmf.getId().setEmfId(emf.getEmfId());
						objectEmfListForContent.add(objectEmf);
					}
				} else {
					objectEmf.getId().setEmfId(emfListForName.get(0).getEmfId());
					objectEmfListForContent.add(objectEmf);
					if ("VIDEO".equals(content.getExtendedContentAttribute().getObjectType())) {
						objectEmfRepository.save(objectEmf);
					}
				}

			}
		}

		// for each multilanguage scene
		for (MultilanguageSceneEntity multilanguageScene : listOfMultilanguageScenes) {
			List<MultilanguageSceneEntity> ms = multiLanguageSceneRepository.retrieveBySceneIdNotContentId(
					multilanguageScene.getId().getSceneId(), content.getContentId().longValue());
			// hibernateTemplate.find("from com.accenture.avs.ci.domain.MultilanguageScene
			// ms where ms.compId.sceneId=" + multilanguageScene.getCompId().getSceneId() +
			// " and ms.compId.contentId<>" +
			// multilanguageScene.getCompId().getContentId());
			if (!ms.isEmpty()) {
				log.logMessage("SceneId " + multilanguageScene.getId().getSceneId()
						+ " is already associated to other content.");
				throw new ApplicationException("ERROR_BE_ACTION_ACN_9821_SCENE_ALREADY_ASSOCIATED");
			}

			// for each multilanguage scene level emf attribute
			List<EmfEntity> emfListForName1 = null;
			if (multilanguageScene.getEmfAttributes() != null) {
				for (ObjectEmfEntity objectEmf : multilanguageScene.getEmfAttributes()) {
					if (objectEmf != null) {

						emfListForName1 = emfRepository.retrieveByEmfName(objectEmf.getEmfName());
						// emfListForName1 = hibernateTemplate.find("from
						// com.accenture.avs.ci.domain.Emf e where
						// e.emfName='"+objectEmf.getEmfName()+"'");
						if (emfListForName1.isEmpty()) {

							EmfEntity emf = new EmfEntity();
							emf.setEmfName(objectEmf.getEmfName());
							emf.setIsActive("Y");
							Integer id = emfRepository.getMaxId();
							if(id == null || id == 0){
								id = 0;
							}
							emf.setEmfId(id + 1);
							emfRepository.save(emf);
							// Long multiLangSceneEMFId = (Long)generatedEMFID;
							objectEmf.getId().setEmfId(emf.getEmfId());
							objectEmfRepository.save(objectEmf);
						} else {
							objectEmf.getId().setEmfId(emfListForName1.get(0).getEmfId());
							objectEmfRepository.save(objectEmf);
						}
					}
				}
			}
		}

		// for each scene
		List<EmfEntity> emfListForName2 = null;
		for (SceneEntity scene : listOfScenes) {
			List<SceneEntity> s = sceneRepository.retrieveBySceneIdNotContentId(scene.getCompId().getSceneId(),
					content.getContentId().longValue());
			// hibernateTemplate.find("from Scene s where s.compId.sceneId=" +
			// scene.getCompId().getSceneId() + " and s.compId.contentId<>" +
			// scene.getCompId().getContentId());
			if (!s.isEmpty()) {
				log.logMessage(
						"SceneId " + scene.getCompId().getSceneId() + " is already associated to other content.");
				throw new ApplicationException("ERROR_BE_ACTION_ACN_9821_SCENE_ALREADY_ASSOCIATED");

			}
			// for each scene level emf attribute
			if (scene.getEmfAttributes() != null) {
				for (ObjectEmfEntity objectEmf : scene.getEmfAttributes()) {
					if (objectEmf != null) {
						emfListForName2 = emfRepository.retrieveByEmfName(objectEmf.getEmfName());
						// hibernateTemplate.find("from com.accenture.avs.ci.domain.Emf e where
						// e.emfName='"+objectEmf.getEmfName()+"'");
						if (emfListForName2.isEmpty()) {
							EmfEntity emf = new EmfEntity();
							emf.setEmfName(objectEmf.getEmfName());
							emf.setIsActive("Y");
							Integer id = emfRepository.getMaxId();
							if(id == null || id == 0){
								id = 0;
							}
							emf.setEmfId(id + 1);
							emfRepository.save(emf);
							// Long sceneEMFId = (Long)generatedEMFID;
							objectEmf.getId().setEmfId(emf.getEmfId());
							objectEmfRepository.save(objectEmf);

						} else {
							objectEmf.getId().setEmfId(emfListForName2.get(0).getEmfId());
							objectEmfRepository.save(objectEmf);
						}
					}
				}
			}

			// for each platform of scene
			if (scenePlatforms != null) {
				for (ObjectPlatformEntity objectPlatform : scenePlatforms) {
					if (objectPlatform != null) {
						// insert into OBJECT_PLATFORM
						objectPlatformRepository.save(objectPlatform);
					}
				}
			}
		}

		// for each chapter
		for (ChapterEntity chapter : listOfChapters) {
			List<ChapterEntity> c = chapterRepository.retrieveByChapterIdNotContentId(
					chapter.getCompId().getChapterId(), content.getContentId().longValue());
			// hibernateTemplate.find("from com.accenture.avs.ci.domain.Chapter c where
			// c.compId.chapterId=" + chapter.getCompId().getChapterId() + " and
			// c.compId.contentId<>" + chapter.getCompId().getContentId());
			if (!c.isEmpty()) {
				log.logMessage(
						"ChapterId " + chapter.getCompId().getChapterId() + " is already associated to other content.");
				throw new ApplicationException("ERROR_BE_ACTION_ACN_9822_CHAPTER_ALREADY_ASSOCIATED");

			}

			// for each platform of chapter
			if (chapterPlatforms != null) {
				for (ObjectPlatformEntity objectPlatform : chapterPlatforms) {
					if (objectPlatform != null) {
						// insert into OBJECT_PLATFORM
						objectPlatformRepository.save(objectPlatform);
					}
				}
			}
		}

		// for each multilanguage chapter
		for (MultilanguageChapterEntity multilanguageChapter : listOfMultilanguageChapters) {
			List<MultilanguageChapterEntity> mc = multiLanguageChapterRepository.retrieveByChapterIdNotContentId(
					multilanguageChapter.getId().getChapterId(), content.getContentId().longValue());
			// hibernateTemplate.find("from com.accenture.avs.ci.domain.MultilanguageChapter
			// mc where mc.compId.chapterId=" +
			// multilanguageChapter.getCompId().getChapterId() + " and
			// mc.compId.contentId<>" + multilanguageChapter.getCompId().getContentId());
			if (!mc.isEmpty()) {
				log.logMessage("ChapterId " + multilanguageChapter.getId().getChapterId()
						+ " is already associated to other content.");
				throw new ApplicationException("ERROR_BE_ACTION_ACN_9822_CHAPTER_ALREADY_ASSOCIATED");

			}
		}

		// super.doWrite(hibernateTemplate, (List<? extends T>) newContentEmfList);
		objectEmfRepository.saveAll(objectEmfListForContent);

	}

	/**
	 * @param content
	 */
	private void deleteContentRelatedData(ContentEntity content) {

		contentCategoryRepository.deleteByContentId(content.getContentId());
		relcontentExtRatRepository.deleteByContentId(content.getContentId());
		subtitlesRepository.deleteByContentId(content.getContentId());

		audioLangRepository.deleteByContentId(content.getContentId());
		contentLinkingRepository.deleteByParentContentId(content.getContentId());

		List<Integer> lstPkgNotRVOD = technicalPackageRepository
				.retrieveTechPackagesNotRVodByContentId(content.getContentId());

		if (lstPkgNotRVOD != null && !lstPkgNotRVOD.isEmpty()) {
			for (int i = 0; i < lstPkgNotRVOD.size(); i++) {
				Integer pkgIdNotRVOD = lstPkgNotRVOD.get(i);
				relPlatformTechnicalRepository.deleteByContentIdAndPackageId(content.getContentId(), pkgIdNotRVOD);

			}

		}
		contentPlatformRepository.unPublishByContentId(content.getContentId());
		multiLanguageContentRepository.deleteByContentId(content.getContentId());
		blacklistDeviceContentRepository.deleteByContentId(content.getContentId());

		List<Long> sceneIds = sceneRepository.retrieveSceneIdsBycontentId(content.getContentId().longValue());

		for (Long sceneId : sceneIds) {
			objectEmfRepository.deleteByObjectTypeAndContentId(sceneId.intValue(), 3);
		}
		List<Long> chapterIds = chapterRepository.retrieveChapterIdsByContentId(content.getContentId().longValue());

		for (Long chapterId : chapterIds) {
			objectEmfRepository.deleteByObjectTypeAndContentId(chapterId.intValue(), 2);
		}
		multiLanguageSceneRepository.deleteByContentId(content.getContentId().longValue());
		sceneRepository.deleteByContentId(content.getContentId().longValue());
		multiLanguageChapterRepository.deleteByContentId(content.getContentId().longValue());
		chapterRepository.deleteByContentId(content.getContentId().longValue());
		objectEmfRepository.deleteByObjectTypeAndContentId(content.getContentId(), 1);

	}

}
