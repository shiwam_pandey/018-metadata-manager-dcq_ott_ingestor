package com.accenture.avs.be.dcq.vod.ingestor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.accenture.avs.persistence.technicalcatalogue.CategoryCMSEntity;

@Repository
public interface CategoryCMSRepository extends JpaRepository<CategoryCMSEntity,Integer>{

	CategoryCMSEntity findByCategoryId(@Param("categoryId")Integer categoryId);
}
