package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.io.Serializable;

/**
 * @author karthik.vadla
 *
 */
public class TechnicalPackageDetailsDTO implements Serializable{
	private static final long serialVersionUID = -2681353497942819024L;

	public TechnicalPackageDetailsDTO(Integer packageId, String packageName, String packageType,Integer cpId) {
		super();
		this.packageId = packageId;
		this.packageName = packageName;
		this.packageType = packageType;
		this.cpId=cpId;
	}

	public TechnicalPackageDetailsDTO() {

	}

	private Integer packageId;
	private String packageName;
	private String packageType ;
	private Integer cpId;

	public Integer getPackageId() {
		return packageId;
	}

	public void setPackageId(Integer packageId) {
		this.packageId = packageId;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getPackageType() {
		return packageType;
	}

	public void setPackageType(String packageType) {
		this.packageType = packageType;
	}
	public Integer getCpId() {
		return cpId;
	}

	public void setCpId(Integer cpId) {
		this.cpId = cpId;
	}
}
