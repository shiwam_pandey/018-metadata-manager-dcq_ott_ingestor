package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.math.BigInteger;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class Channel {

	@ApiModelProperty(required = true, value = "channel unique identifier",example="9") 
    private int channelId;
	@ApiModelProperty(required = true, value = "channel name . Max length 50",example="channel9") 
    private String name;
	@ApiModelProperty(required = true, value = "channel type . Max length 50. Possible values VLC, SVOD_CATCHUP",example="VLC") 
    private String type;
	@ApiModelProperty(required = true, value = "used to sort channels",example="1") 
    private int order;
	@ApiModelProperty(required = false,value = "external channel identifier",example="1") 
    private String externalId;
	@ApiModelProperty(required = false, value = "used to recognize if a channel streams adult content or not . Possible values Y or N",example="Y") 
    private String adult;
	@ApiModelProperty(required = false, value = "Channel Description",example="Channel19 most viewed in US") 
    private String description;
	@ApiModelProperty(required = true, value = "List of Platform where the channel must be published") 
    private List<Platforms> platforms;
	@ApiModelProperty(required = true, value = "Technical Packages associated to the channel",example="[ 767,263 ]") 
    private List<BigInteger> packageIds;
	@ApiModelProperty(required = false, value = "For Contextual Ads",example="Gender|Female|101|Mary Week") 
    private String advTags;
	@ApiModelProperty(required = false, value = "Flag to identify if advertising is disallowed on channel (Y/N)",example="Y") 
    private String isDisAllowedAdv;
	@ApiModelProperty(required = false, value = "Default channel number",example="1") 
    private Integer defaultChannelNumber;
	@ApiModelProperty(required = false, value = "Is active channel or not . Possible values Y or N",example="Y") 
    private String isActive;
	@ApiModelProperty(required = false, value = "Broad caster name",example="KPN") 
    private String broadcasterName;
	@ApiModelProperty(required = false, value = "ExtendedMetadata",example="{\"plot\":\"On a far planet...\",\"actorsListDetail\":[{\"Name\":\"Sam Worthington\",\"BirthDate\":\"03/02/1981\",\"BirthCity\":\"New York\"},{\"Name\":\"Sigourney Weaver\",\"BirthDate\":\"04/03/1955\",\"BirthCity\":\"New York\"}]}") 
    private String extendedMetadata;
	
	public int getChannelId() {
		return channelId;
	}
	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public String getExternalId() {
		return externalId;
	}
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	public String getAdult() {
		return adult;
	}
	public void setAdult(String adult) {
		this.adult = adult;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<Platforms> getPlatforms() {
		return platforms;
	}
	public void setPlatforms(List<Platforms> platforms) {
		this.platforms = platforms;
	}

	public List<BigInteger> getPackageIds() {
		return packageIds;
	}
	public void setPackageIds(List<BigInteger> packageIds) {
		this.packageIds = packageIds;
	}
	public String getAdvTags() {
		return advTags;
	}
	public void setAdvTags(String advTags) {
		this.advTags = advTags;
	}
	public String getIsDisAllowedAdv() {
		return isDisAllowedAdv;
	}
	public void setIsDisAllowedAdv(String isDisAllowedAdv) {
		this.isDisAllowedAdv = isDisAllowedAdv;
	}
	public Integer getDefaultChannelNumber() {
		return defaultChannelNumber;
	}
	public void setDefaultChannelNumber(Integer defaultChannelNumber) {
		this.defaultChannelNumber = defaultChannelNumber;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getBroadcasterName() {
		return broadcasterName;
	}
	public void setBroadcasterName(String broadcasterName) {
		this.broadcasterName = broadcasterName;
	}
	public String getExtendedMetadata() {
		return extendedMetadata;
	}
	public void setExtendedMetadata(String extendedMetadata) {
		this.extendedMetadata = extendedMetadata;
	}
    
    

}
