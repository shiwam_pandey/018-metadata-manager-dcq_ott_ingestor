package com.accenture.avs.be.dcq.vod.ingestor.content.dto;

import java.io.Serializable;
import java.util.List;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class VlcContentPlaylist implements Serializable{

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value="Unique Identifier of the channel",required=true,example="456")
	private Integer channelId;
	@ApiModelProperty(value="Date when the playlist is published",required=true,example="2019-02-10")
	private String playlistPublishedDate;
	@ApiModelProperty(value="list of playlist details",required=true)
	private List<Playlist> playlist;
	
	public List<Playlist> getPlaylist() {
		return playlist;
	}

	public void setPlaylist(List<Playlist> playlist) {
		this.playlist = playlist;
	}

	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}

	public String getPlaylistPublishedDate() {
		return playlistPublishedDate;
	}

	public void setPlaylistPublishedDate(String playlistPublishedDate) {
		this.playlistPublishedDate = playlistPublishedDate;
	}
}
