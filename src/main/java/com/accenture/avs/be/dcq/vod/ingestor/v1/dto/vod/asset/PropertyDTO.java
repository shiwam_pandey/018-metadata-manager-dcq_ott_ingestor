package com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author karthik.vadla
 *
 */
public class PropertyDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value="Name of Property.Used in multi-geo scenario",required=true,example="UK")
	private String propertyName;
	
	@ApiModelProperty(value="This field can contains a json with custom data",required=false,example="{\"plot\":\"On a far planet...\",\"actorsListDetail\":[{\"Name\":\"Sam Worthington\",\"BirthDate\":\"03/02/1981\",\"BirthCity\":\"New York\"},{\"Name\":\"Sigourney Weaver\",\"BirthDate\":\"04/03/1955\",\"BirthCity\":\"New York\"}]}")
	private String extendedMetadata;
	
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getExtendedMetadata() {
		return extendedMetadata;
	}
	public void setExtendedMetadata(String extendedMetadata) {
		this.extendedMetadata = extendedMetadata;
	}
	
}
