package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author karthik.vadla
 *
 */
public class ChannelIdPlaylistDateDTO implements Serializable{
	private static final long serialVersionUID = 1L;
		private Integer channelId;
		private Date playlistDate;
		private Integer rowId;
		
		public Integer getRowId() {
			return rowId;
		}
		public void setRowId(Integer rowId) {
			this.rowId = rowId;
		}
		
		public Integer getChannelId() {
			return channelId;
		}
		public void setChannelId(Integer channelId) {
			this.channelId = channelId;
		}
		public Date getPlaylistDate() {
			return playlistDate;
		}
		public void setPlaylistDate(Date playlistDate) {
			this.playlistDate = playlistDate;
		}
	}
	
