package com.accenture.avs.be.dcq.vod.ingestor.sdp.manager.impl;

import java.net.MalformedURLException;
import java.net.URL;

import com.accenture.sdp.csmfe.webservices.clients.ingestor.IngestorService;

public class SdpIngestorServiceSingleton {
	
	private static IngestorService service;

	private SdpIngestorServiceSingleton(){}


	private SdpIngestorServiceSingleton(String sdpUrlConnection) throws MalformedURLException{
		String urlString = sdpUrlConnection+ "IngestorService?wsdl";
		URL url =  new URL(urlString);
		if (service == null) { // sonar fix
		service=new IngestorService(url);
		}
	}

	public static IngestorService getInstance(String sdpUrlConnection) throws MalformedURLException{

		if (service == null) {
			new SdpIngestorServiceSingleton(sdpUrlConnection);
		}
		return service;
	}

}
