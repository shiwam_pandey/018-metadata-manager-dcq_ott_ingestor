package com.accenture.avs.be.dcq.vod.ingestor.manager.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.manager.CategoryAggregationManager;
import com.accenture.avs.be.dcq.vod.ingestor.repository.CategoryAggregationRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.CategoryCMSRepository;
import com.accenture.avs.be.dcq.vod.ingestor.utils.CategoryAggregationUtils;
import com.accenture.avs.persistence.technicalcatalogue.CategoryAggregationEntity;
import com.accenture.avs.persistence.technicalcatalogue.CategoryCMSEntity;

@Component
public class CategoryAggregationManagerImpl implements CategoryAggregationManager
{

	@Autowired
	private CategoryCMSRepository categoryCMSRepository;
	
	@Autowired
	private CategoryAggregationRepository categoryAggregationRepository;
	
	@Autowired
	private CategoryAggregationUtils categoryAggregationUtils;
	
	@Override
	public void reStoreCategoryAggregation() {
		deleteCategoryAggregation();
		insertCategoryAggregation();
		
	}

	@Override
	@Transactional
	public void insertCategoryAggregation() {
	List<CategoryCMSEntity> categoryEntities= categoryCMSRepository.findAll();
	
	ArrayList<CategoryAggregationEntity> categoryAggregationEntities= categoryAggregationUtils.insertCategoriesAggregation(categoryEntities);
	
    categoryAggregationRepository.saveAll(categoryAggregationEntities);
		
	}

	@Override
	public void deleteCategoryAggregation() {
		categoryAggregationRepository.deleteAll();
		
	}

}
