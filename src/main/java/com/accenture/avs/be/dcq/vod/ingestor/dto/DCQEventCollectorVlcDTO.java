package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.io.Serializable;

/**
 * @author s.gudivada
 *
 */
public class DCQEventCollectorVlcDTO implements Serializable{
	private static final long serialVersionUID = -460053666626358917L;
	private String action;
	private String contentType;
	private Long playlistPublishedDate;
	private Integer channelId;
	private String extChannelId;
	
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public Long getPlaylistPublishedDate() {
		return playlistPublishedDate;
	}
	public void setPlaylistPublishedDate(Long playlistPublishedDate) {
		this.playlistPublishedDate = playlistPublishedDate;
	}
	public Integer getChannelId() {
		return channelId;
	}
	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}
	public String getExtChannelId() {
		return extChannelId;
	}
	public void setExtChannelId(String extChannelId) {
		this.extChannelId = extChannelId;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
