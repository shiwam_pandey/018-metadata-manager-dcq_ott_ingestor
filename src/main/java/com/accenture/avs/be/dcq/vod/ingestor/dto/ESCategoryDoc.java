package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.io.Serializable;

/**
 * @author karthik.vadla
 *
 */
public class ESCategoryDoc implements Serializable {
	private static final long serialVersionUID = 1L;

	private RequestBean category;

	public RequestBean getCategory() {
		return category;
	}

	public void setCategory(RequestBean category) {
		this.category = category;
	}

	public static class RequestBean implements Serializable {

		private static final long serialVersionUID = 1L;
		private Integer categoryId;
		private Integer parentCategoryId;
		private String name;
		private String type;
		private Integer contentId;
		private Boolean isVisible;
		private String externalId;
		private Integer orderId;
		private String ratingType;
		private String pcLevel;
		private String[] pcExtendedRatings;
		private String channelCategory;
		private boolean hasNewContent;
		private String contentOrderType;
		private boolean isAdult;
		private String title;
		private boolean isProtected;

		public boolean getIsProtected() {
			return isProtected;
		}

		public void setIsProtected(boolean isProtected) {
			this.isProtected = isProtected;
		}

		public Integer getCategoryId() {
			return categoryId;
		}

		public void setCategoryId(Integer categoryId) {
			this.categoryId = categoryId;
		}

		public Integer getParentCategoryId() {
			return parentCategoryId;
		}

		public void setParentCategoryId(Integer parentCategoryId) {
			this.parentCategoryId = parentCategoryId;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public Integer getContentId() {
			return contentId;
		}

		public void setContentId(Integer contentId) {
			this.contentId = contentId;
		}

		public Boolean getIsVisible() {
			return isVisible;
		}

		public void setIsVisible(Boolean isVisible) {
			this.isVisible = isVisible;
		}

		public String getExternalId() {
			return externalId;
		}

		public void setExternalId(String externalId) {
			this.externalId = externalId;
		}

		public Integer getOrderId() {
			return orderId;
		}

		public void setOrderId(Integer orderId) {
			this.orderId = orderId;
		}

		public String getRatingType() {
			return ratingType;
		}

		public void setRatingType(String ratingType) {
			this.ratingType = ratingType;
		}

		public String getPcLevel() {
			return pcLevel;
		}

		public void setPcLevel(String pcLevel) {
			this.pcLevel = pcLevel;
		}

		public String[] getPcExtendedRatings() {
			return pcExtendedRatings;
		}

		public void setPcExtendedRatings(String[] pcExtendedRatings) {
			this.pcExtendedRatings = pcExtendedRatings;
		}

		public String getContentOrderType() {
			return contentOrderType;
		}

		public void setContentOrderType(String contentOrderType) {
			this.contentOrderType = contentOrderType;
		}

		public String getChannelCategory() {
			return channelCategory;
		}

		public void setChannelCategory(String channelCategory) {
			this.channelCategory = channelCategory;
		}

		public boolean isHasNewContent() {
			return hasNewContent;
		}

		public void setHasNewContent(boolean hasNewContent) {
			this.hasNewContent = hasNewContent;
		}

		public boolean getIsAdult() {
			return isAdult;
		}

		public void setAdult(boolean isAdult) {
			this.isAdult = isAdult;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

	}
}
