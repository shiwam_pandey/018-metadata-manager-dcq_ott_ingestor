package com.accenture.avs.be.dcq.vod.ingestor.manager.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.logging.log4j.ThreadContext;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.avs.be.dcq.vod.ingestor.dto.CategoryCMS;
import com.accenture.avs.be.dcq.vod.ingestor.manager.CategoryAggregationManager;
import com.accenture.avs.be.dcq.vod.ingestor.manager.CategoryManager;
import com.accenture.avs.be.dcq.vod.ingestor.manager.CategoryMetadataManager;
import com.accenture.avs.be.dcq.vod.ingestor.processor.DcqVodIngestionPreProcessor;
import com.accenture.avs.be.dcq.vod.ingestor.repository.CategoryCMSRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.CategoryRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ContentCategoryRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ContentRepository;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.cache.Configurator;
import com.accenture.avs.be.framework.configuration.CacheConstants;
import com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.be.framework.exception.ConfigurationException;
import com.accenture.avs.be.framework.utils.Utilities;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.commons.lib.support.CommonConstants;
import com.accenture.avs.persistence.technicalcatalogue.CategoryCMSEntity;
import com.accenture.avs.persistence.technicalcatalogue.CategoryEntity;
import com.accenture.avs.persistence.technicalcatalogue.ContentEntity;

/**
 * @author karthik.vadla
 *
 */
@Component
@Transactional(rollbackFor = { ApplicationException.class, Exception.class })
public class CategoryManagerImpl implements CategoryManager {

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private ContentCategoryRepository contentCategoryRepository;

	@Autowired
	private ContentRepository contentRepository;
	
	@Autowired
	private CategoryCMSRepository categoryCMSRepository;
	
	@Autowired
	private CategoryAggregationManager categoryAggregationManager;
	
	@Autowired
	private DcqVodIngestionPreProcessor dcqVodIngestionPreProcessor;
	
	@Autowired
	private Configurator configurator;

	@Autowired
	private CategoryMetadataManager categoryMetadataManager;
	
	@Autowired
	private EntityManager entityManager;
	
	private static final LoggerWrapper log = new LoggerWrapper(CategoryManagerImpl.class);

	@Override
	@Transactional(rollbackFor = { ApplicationException.class, Exception.class })
	public List<Integer> unpublishCategory(Integer categoryId, Configuration config) throws ApplicationException {

		List<Integer> contentIds = null;
		try {
			categoryRepository.unpublishCategory(categoryId);

			List<Integer> categoryIds = new ArrayList<>();
			categoryIds.add(categoryId);
			contentIds = contentCategoryRepository.retriveContentIdsByCategoryIds(categoryIds);
			contentCategoryRepository.deleteByCategoryIds(categoryIds);

		} catch (Exception e) {
			throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR);
		}
		return contentIds;
	}

	@Override
	public void ingestCategory(List<CategoryCMS> categoryCMSList) throws ConfigurationException, ApplicationException, JsonParseException, JsonMappingException, IOException {
		List<Integer> categoryIds=new ArrayList<>();
		for (CategoryCMS categoryCMS : categoryCMSList) {
			categoryIds.add(categoryCMS.getCategoryId());
			CategoryCMSEntity categoryEntity=mapCategoryEntity(categoryCMS);
			categoryCMSRepository.save(categoryEntity);
			entityManager.flush();
			entityManager.clear();
			
			if(categoryEntity.getParent_category_id()==null)
			{
			if(categoryCMS.getParentCategoryId().equals(categoryCMS.getCategoryId()) && (categoryCMS.getCategoryType().equals("NODE")))
			{
				categoryEntity.setParent_category_id(categoryCMS.getParentCategoryId());
				categoryCMSRepository.save(categoryEntity);
				entityManager.flush();
				entityManager.clear();
			}
			else 
			{
				throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,new NestedParameters("parent categoryId:"+categoryCMS.getParentCategoryId()));
			}
			}
			categoryAggregationManager.reStoreCategoryAggregation();
			
		}
		Configuration config = configurator.getConfiguration(ThreadContext.get(CommonConstants.LogParamsL4j2.TN));
	    dcqVodIngestionPreProcessor.refreshCachesCreateIndexes(DcqVodIngestorConstants.CATEGORY, "","", config);
		categoryMetadataManager.processAndWriteCategories(categoryIds, DcqVodIngestorUtils.getTransactionNumber(), DcqVodIngestorUtils.currentTime(), "","" , config);
	}

	public CategoryCMSEntity mapCategoryEntity(CategoryCMS categoryCMS) throws ApplicationException {
		CategoryCMSEntity categoryEntity = new CategoryCMSEntity();


		try {
		if (categoryCMS.getContentId() == null) {
			CategoryEntity categoryExists = categoryRepository
					.retrieveByCategoryid(categoryCMS.getCategoryId().intValue());
			if (categoryExists == null) {
				categoryEntity.setCategoryId(1000000000);
			}else {
				log.logMessage("Persisting category details");
				if(categoryExists.getContent() != null){
				categoryEntity.setContent_id(categoryExists.getContent().getContentId());
				}
			
			}
		} else {
			ContentEntity contentEntity = contentRepository
					.getContentByContentId(categoryCMS.getContentId().intValue());
			if (contentEntity == null) {
				throw new ApplicationException(MessageKeys.ERROR_BE_3089_CONTENT_NOT_FOUND_ON_DB,new NestedParameters("contentid:"+categoryCMS.getContentId()));
				
			} else {
				categoryEntity.setContent_id(contentEntity.getContentId());
				}
		}
		if (categoryCMS.getParentCategoryId() != null) {
			CategoryEntity parentCategoryExists = categoryRepository
					.retrieveByCategoryid(categoryCMS.getParentCategoryId().intValue());
			if(parentCategoryExists!=null)
			{
			categoryEntity.setParent_category_id(parentCategoryExists.getCategoryId());
			}
			
		}
		categoryEntity.setAdult(categoryCMS.getAdult());
		categoryEntity.setCategoryId(categoryCMS.getCategoryId().intValue());
		categoryEntity.setExternalId(categoryCMS.getExternalId());
		categoryEntity.setSourceFile(new String(categoryCMS.getSourceFile()));
		categoryEntity.setHasNew(categoryCMS.getAsNew());
		categoryEntity.setCategoryType(categoryCMS.getCategoryType());
		categoryEntity.setChannelCategory(!Utilities.isEmpty(categoryCMS.getChannelCategory())?categoryCMS.getChannelCategory():null);
		categoryEntity.setContentOrderType(categoryCMS.getContentOrderType());
		categoryEntity.setOrderId(categoryCMS.getOrderId()!=null?categoryCMS.getOrderId().intValue():0);
		categoryEntity.setName(categoryCMS.getName());
		categoryEntity.setPictureUrl(categoryCMS.getPictureUrl());
		categoryEntity.setTitle(categoryCMS.getTitle());
		categoryEntity.setIsVisible(categoryCMS.getIsVisible());
		categoryEntity.setExternalidPath(categoryCMS.getExternalidPath());
		categoryEntity.setIdPath(categoryCMS.getIdPath());
		
		}catch(ApplicationException e) {
			throw e;
		}catch(Exception e) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR);
		}
		return categoryEntity;
	}
	
}
