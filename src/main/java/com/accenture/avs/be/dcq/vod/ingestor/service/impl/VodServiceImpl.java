package com.accenture.avs.be.dcq.vod.ingestor.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.accenture.avs.be.dcq.vod.ingestor.cache.TRCCache;
import com.accenture.avs.be.dcq.vod.ingestor.content.dto.GetTransactionRateCardListResponse;
import com.accenture.avs.be.dcq.vod.ingestor.content.dto.TransactionRateCardListDto;
import com.accenture.avs.be.dcq.vod.ingestor.content.dto.TransactionRateCardResponse;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Asset;
import com.accenture.avs.be.dcq.vod.ingestor.manager.ContentMetadataManager;
import com.accenture.avs.be.dcq.vod.ingestor.manager.VodManager;
import com.accenture.avs.be.dcq.vod.ingestor.processor.DcqVodIngestionPreProcessor;
import com.accenture.avs.be.dcq.vod.ingestor.service.VodService;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorRestUrls;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.Validator;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.commons.lib.LoggerWrapper;

/**
 * @author karthik.vadla
 *
 */
@Service
public class VodServiceImpl implements VodService {

	@Autowired
	private ResourceLoader resourceLoader;
	@Autowired
	private VodManager vodManager;
	@Autowired
	private DcqVodIngestionPreProcessor dcqVodIngestionPreProcessor;
	@Autowired
	private ContentMetadataManager contentMetadataManager;
	@Autowired
	private TRCCache trcCache;
	@Autowired
	private DcqVodIngestorRestUrls dcqVodIngestorRestUrls;

	private static final LoggerWrapper log = new LoggerWrapper(VodServiceImpl.class);

	@Override
	public GenericResponse ingestContent(String vodXml, Configuration config) throws ApplicationException, Exception {
		log.logMessage("call to ingestContent ");
		GenericResponse genericResponse = null;
		StringReader reader = null;
		InputStream contentXSD = null;
		// validating the xml
		try {
			contentXSD = resourceLoader
					.getResource("classpath:dcq-vod-ingestor/" + DcqVodIngestorConstants.VOD.CONTENT_XSD)
					.getInputStream();
			if (DcqVodIngestorUtils.checkVulnerablesInInputXML(vodXml)) {
				throw new ApplicationException(
						com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
						new NestedParameters(DcqVodIngestorConstants.ERR_VULNERABLE_REQ));
			}
			final Pattern unescapedAmpersands = Pattern.compile("(&(?!amp;))");
			Matcher m = unescapedAmpersands.matcher(vodXml);
			String xmlWithAmpersandsEscaped = m.replaceAll("&amp;");
			boolean validXml = Validator.validateXMLSchema(contentXSD, xmlWithAmpersandsEscaped);
			if (validXml) {
				log.logMessage("Input XML is valid");
				JAXBContext jaxbContext = JAXBContext.newInstance(Asset.class);
				Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
				reader = new StringReader(xmlWithAmpersandsEscaped);
				Asset asset = (Asset) unmarshaller.unmarshal(reader);
				if (Validator.isEmpty(asset)) {
					log.logMessage("No content to write. The execution is completed.");
					genericResponse = new GenericResponse(DcqVodIngestorConstants.ACN_200, DcqVodIngestorConstants.OK,
							"");
					return genericResponse;
				}

				Integer contentId = vodManager.ingestContent(asset, xmlWithAmpersandsEscaped, config);
				List<Integer> assetIdList = new ArrayList<>();
				assetIdList.add(contentId);
				dcqVodIngestionPreProcessor.refreshCachesCreateIndexes(DcqVodIngestorConstants.CONTENT, "", "", config);
				contentMetadataManager.processandWriteContents(assetIdList, DcqVodIngestorUtils.getTransactionNumber(),
						DcqVodIngestorUtils.currentTime(), "", "", config);

			}
		} catch (ApplicationException e) {
			throw e;
		} catch (SAXParseException je) {
			log.logError(je);
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters("XML: " + je.getMessage()));

		} catch (SAXException je) {
			log.logError(je);
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters("XML: " + je.getMessage()));

		} catch (Exception e) {
			throw e;
		} finally {
			if (reader != null) {
				reader.close();
			}
			if (contentXSD != null)
				contentXSD.close();
		}
		genericResponse = new GenericResponse(DcqVodIngestorConstants.ACN_200, DcqVodIngestorConstants.OK, "");

		return genericResponse;
	}

	@Override
	public GenericResponse getTRC(String startIndex, String maxResults, Configuration config)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, Exception {

		GenericResponse response = null;
		TransactionRateCardListDto trcDto = new TransactionRateCardListDto();
		String getTRCUrl = dcqVodIngestorRestUrls.GET_TRC_COMMERCE_URL;

		try {
			Validator.validateIntNotNegativeParameter("startIndex", startIndex, config);
			Validator.validateIntNotNegativeParameter("maxResults", maxResults, config);

			startIndex = StringUtils.isBlank(startIndex) ? "0" : startIndex;
			maxResults = StringUtils.isBlank(maxResults) ? "50" : maxResults;

			maxResults = (Integer.parseInt(maxResults) > 50) ? "50" : maxResults;

			GetTransactionRateCardListResponse trcResponse = trcCache.loadCommerce(getTRCUrl, config, startIndex,
					maxResults);
			if (trcResponse == null) {
				throw new ApplicationException(MessageKeys.ERROR_MS_ACN_901_COMMERCE_NOT_AVAILABLE);
			}
			trcDto.setTotalResults(trcResponse.getTotalResults());
			trcDto.setTransactionRateCards(trcResponse.getTransactionRateCardList());
			response = new GenericResponse(DcqVodIngestorConstants.ACN_200, DcqVodIngestorConstants.OK, trcDto);

		} catch (ApplicationException ae) {
			throw ae;
		}
		return response;
	}

	@Override
	public TransactionRateCardResponse getLegacyTRC(String startIndex, String maxResults, Configuration config)
			throws Exception {

		TransactionRateCardResponse response = null;
		String getTRCUrl = dcqVodIngestorRestUrls.GET_TRC_COMMERCE_URL;

		Validator.validateIntNotNegativeParameter("from", startIndex, config);
		Validator.validateIntNotNegativeParameter("size", maxResults, config);

		GetTransactionRateCardListResponse trcResponse = trcCache.loadCommerce(getTRCUrl, config, startIndex,
				maxResults);
		response = new TransactionRateCardResponse(DcqVodIngestorConstants.OK, null, "", System.currentTimeMillis(),
				trcResponse);

		return response;
	}

	@Override
	public void unpublishContent(Integer contentId, Configuration config) throws Exception {
		List<Integer> unpublishContentIds;
		try {
			log.logMessage("Started Unpublishing Content {}", contentId);
			unpublishContentIds = vodManager.unpublishContent(contentId, config);
			// call to ES for unpublishing
			log.logMessage("Deleting contents from ES");
			if (unpublishContentIds != null && !unpublishContentIds.isEmpty()) {
				contentMetadataManager.unpublishContents(unpublishContentIds, config);
			}
			log.logMessage("Successfully deleted all contents from ES");
			log.logMessage("Successfully Unpublished Content {}", contentId);
		} catch (ApplicationException ae) {
			throw ae;
		} catch (Exception ex) {
			throw ex;
		}
	}
}
