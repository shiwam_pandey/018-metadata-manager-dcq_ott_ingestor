package com.accenture.avs.be.dcq.vod.ingestor.manager;

import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.exception.ApplicationException;

public interface EmfManager {
	
   GenericResponse activateOrReactiveEmf(String emfName,String status) throws ApplicationException;

}
