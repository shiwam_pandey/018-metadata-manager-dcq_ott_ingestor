package com.accenture.avs.be.dcq.vod.ingestor.dto;

public class ChannelDTO {

	private Integer channelId;
	private String externalId;
	public Integer getChannelId() {
		return channelId;
	}
	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}
	public String getExternalId() {
		return externalId;
	}
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	
	
	
}
