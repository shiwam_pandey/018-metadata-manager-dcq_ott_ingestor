package com.accenture.avs.be.dcq.vod.ingestor.processor.impl;

import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.manager.impl.DcqAssetStagingManager;
import com.accenture.avs.be.dcq.vod.ingestor.processor.DcqVodIngestionPostProcessor;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;

/**
 * @author karthik.vadla
 *
 */
@Component
public class DcqVodIngestionPostProcessorImpl implements DcqVodIngestionPostProcessor {
 
	@Autowired
private	DcqAssetStagingManager dcqAssetStagingManager;

	@Override
	public void updateStatusAsFailed( Map<Integer, String> failedIds,String assetType,Configuration config) throws ApplicationException {
		dcqAssetStagingManager.updateStatusAsFailed( failedIds,assetType, config);
	}

	@Override
	public void updatePlaylistStatusAsFailed( Map<Integer, String> failedIdsMap, Configuration config) throws ApplicationException {
		dcqAssetStagingManager.updatePlaylistStatusAsFailed( failedIdsMap, config);
		
	}

	@Override
	public void deletePublishedAssets (Set<Integer> publishedIds,String assetType,Configuration config) throws ApplicationException {
		dcqAssetStagingManager.deletePublishedAssets(publishedIds,assetType);
	}

	@Override
	public void deletePublishedChannel( Set<Integer> publishedIds,Configuration config) throws ApplicationException {
		dcqAssetStagingManager.deletePublishedChannel( publishedIds);
		
	} 

}
