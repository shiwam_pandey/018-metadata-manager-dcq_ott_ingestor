package com.accenture.avs.be.dcq.vod.ingestor.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.avs.persistence.technicalcatalogue.ContentLinkingEntity;
/**
 * @author karthik.vadla
 *
 */
@Repository
public interface ContentLinkingRepository extends JpaRepository<ContentLinkingEntity,Integer>{

	/**
	 * @param contentId
	 * @return
	 */
	public List<Object[]> retrieveParentContentId(@Param("contentId")Integer contentId);

	@Modifying
	@Transactional
	public void deleteByParentContentId(@Param("contentId")Integer contentId);

}
