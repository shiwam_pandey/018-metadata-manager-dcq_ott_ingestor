package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author karthik.vadla
 *
 */
public class VLCContentDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long contentId;
	private Long cpId;
	private String videoType;
	private Date startTime;
	private Date endTime;
	private String pcLevel;
	private String platform;
	
	
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	public Long getContentId() {
		return contentId;
	}
	public void setContentId(Long contentId) {
		this.contentId = contentId;
	}
	public Long getCpId() {
		return cpId;
	}
	public void setCpId(Long cpId) {
		this.cpId = cpId;
	}
	public String getVideoType() {
		return videoType;
	}
	public void setVideoType(String videoType) {
		this.videoType = videoType;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public String getPcLevel() {
		return pcLevel;
	}
	public void setPcLevel(String pcLevel) {
		this.pcLevel = pcLevel;
	}
	
}
