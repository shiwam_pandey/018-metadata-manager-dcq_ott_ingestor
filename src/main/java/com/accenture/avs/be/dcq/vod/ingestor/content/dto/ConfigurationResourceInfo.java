package com.accenture.avs.be.dcq.vod.ingestor.content.dto;

public class ConfigurationResourceInfo {
	private String resource;
	private CopyProtectionConfiguration configuration;
	private String id;
	private String author;
	private String version;
	private String status;
	private String[] tags;
	private Long creationTime;

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	public CopyProtectionConfiguration getConfiguration() {
		return configuration;
	}

	public void setConfiguration(CopyProtectionConfiguration configuration) {
		this.configuration = configuration;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String[] getTags() {
		return tags;
	}

	public void setTags(String[] tags) {
		this.tags = tags;
	}

	public Long getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Long creationTime) {
		this.creationTime = creationTime;
	}

}
