package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.io.Serializable;
import java.util.Arrays;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * The persistent class for the CATEGORY database table. Owner of categoryIds is
 * an external system.
 * 
 * @author BEA Workshop
 */
public class CategoryCMS implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull
	private Integer categoryId;

	@Pattern(regexp = "NODE|LEAF", message = "categoryType field must match 'NODE' or 'LEAF'")
	private String categoryType;

	private String channelCategory;

	@NotNull(message = "contentId field may not be empty")
	private Long contentId;

	@Pattern(regexp = "|TITLEASC|TITLEDESC|CONTRSTARTASC|CONTRSTARTDESC|EPISODEASC|EPISODEDESC|BROADDATEASC|BROADDATEDES|POSITIONASC|POSITIONDESC", message = "contentOrderType field must match 'TITLEASC' or 'TITLEDESC' or 'CONTRSTARTASC' or 'CONTRSTARTDESC' or "
			+ "'EPISODEASC' or 'EPISODEDESC' or 'BROADDATEASC' or 'BROADDATEDES' or 'POSITIONASC' or 'POSITIONDESC'")
	private String contentOrderType;

	private String asNew;

	@Pattern(regexp = "[Y|N]", message = "isVisible field must match 'Y' or 'N'")
	private String isVisible;

	@NotEmpty(message = "name field may not be empty")
	private String name;

	@NotNull(message = "orderId field may not be empty")
	private Long orderId;

	@NotNull(message = "parentCategoryId field may not be empty")
	private Integer parentCategoryId;

	private String contentTitle;

	@Pattern(regexp = "[Y|N]", message = "isAdult field must match 'Y' or 'N'")
	private String adult;
	private String pictureUrl;
	private String title;

	@NotNull(message = "externalId field may not be null")
	@NotEmpty(message = "externalId field may not be empty") // fix for AVS-6883
	private String externalId;

	private String idPath;

	private String externalidPath;

	private byte[] sourceFile;

	public CategoryCMS() {
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPictureUrl() {
		return pictureUrl;
	}

	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getChannelCategory() {
		return channelCategory;
	}

	public void setChannelCategory(String channelCategory) {
		this.channelCategory = channelCategory;
	}

	public Long getContentId() {
		return contentId;
	}

	public void setContentId(Long contentId) {
		this.contentId = contentId;
	}

	public String getContentOrderType() {
		return contentOrderType;
	}

	public void setContentOrderType(String contentOrderType) {
		this.contentOrderType = contentOrderType;
	}

	public String getAsNew() {
		return asNew;
	}

	public void setAsNew(String asNew) {
		this.asNew = asNew;
	}

	public String getIsVisible() {
		return isVisible;
	}

	public void setIsVisible(String isVisible) {
		this.isVisible = isVisible;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Integer getParentCategoryId() {
		return parentCategoryId;
	}

	public void setParentCategoryId(Integer parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}

	public String getContentTitle() {
		return contentTitle;
	}

	public void setContentTitle(String contentTitle) {
		this.contentTitle = contentTitle;
	}

	public void setAdult(String adult) {
		this.adult = adult;
	}

	public String getCategoryType() {
		return categoryType;
	}

	public String getAdult() {
		return adult;
	}

	public void setCategoryType(String categoryType) {
		if (categoryType.equalsIgnoreCase("LEAF")) {
			this.categoryType = "TYPE_VOD";
		} else if (categoryType.equalsIgnoreCase("NODE")) {
			this.categoryType = "TYPE_NODE";
		}
		this.categoryType = categoryType;

	}

	public boolean equals(Object other) {
		boolean flag;
		if (this == other) {
			flag = true;
		}
		if (other instanceof CategoryCMS) {
			CategoryCMS castOther = (CategoryCMS) other;
			flag = new EqualsBuilder().append(this.getCategoryId(), castOther.getCategoryId()).isEquals();
		} else {
			flag = false;
		}
		return flag;
	}

	public int hashCode() {
		return new HashCodeBuilder().append(getCategoryId()).toHashCode();
	}

	public String toString() {
		return new ToStringBuilder(this).append("categoryId", getCategoryId()).toString();
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getIdPath() {
		return idPath;
	}

	public void setIdPath(String idPath) {
		this.idPath = idPath;
	}

	public String getExternalidPath() {
		return externalidPath;
	}

	public void setExternalidPath(String externalidPath) {
		this.externalidPath = externalidPath;
	}

	public byte[] getSourceFile() {
		return this.sourceFile = Arrays.copyOf(sourceFile, sourceFile.length);
	}

	public void setSourceFile(byte[] sourceFile) {
		if (sourceFile == null) {
			this.sourceFile = new byte[0];
		} else {
			this.sourceFile = Arrays.copyOf(sourceFile, sourceFile.length);
		}
	}

}
