package com.accenture.avs.be.dcq.vod.ingestor.gateway.output;

import java.util.Map;

/**
 * The output all gateways will produce.
 */
public interface GatewayOutput {
	/**
	 *  add sample codes
	 * 
	 * @return the response code
	 */
	public String getResultCode();
	
	/**
	 * @return a descriptive message
	 */
	public String getResultMessage();
	
	/**
	 * @return true in case of error and false otherwise
	 */
	public boolean hasErrors();
	
	/**
	 * @return additional data structure
	 */
	public Object getExtObject();
	
	/**
	 * @return true if the output error should be forced and false otherwise
	 */
	public boolean isForceError();
	
	/**
	 * @return Map with parameters to add to session
	 */
	
	public Map<String, String> getExtSessionMap();
	
}
