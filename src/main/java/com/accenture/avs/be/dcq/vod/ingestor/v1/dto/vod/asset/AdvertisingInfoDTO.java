package com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author karthik.vadla
 *
 */
public class AdvertisingInfoDTO implements Serializable{

	@ApiModelProperty(value="Network Identifier",required=true,example="5345")
	private String networkId;
	
	@ApiModelProperty(value="Advertising content ID",required=true,example="32545")
	private String advertisingContentId;
	
	@ApiModelProperty(value="List of Cue point",required=false,example="")
	private List<CuePointDTO> cuePoints;
	
	public String getNetworkId() {
		return networkId;
	}

	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}

	public String getAdvertisingContentId() {
		return advertisingContentId;
	}

	public void setAdvertisingContentId(String advertisingContentId) {
		this.advertisingContentId = advertisingContentId;
	}

	public List<CuePointDTO> getCuePoints() {
		return cuePoints;
	}

	public void setCuePoints(List<CuePointDTO> cuePoints) {
		this.cuePoints = cuePoints;
	}

	public static class CuePointDTO implements Serializable{
		
		@ApiModelProperty(value="The position of the break in content time in seconds",required=true,example="461.528")
		private Double contentStartTimePosition;
		
		@ApiModelProperty(value="The ending time position in seconds of the embedded ads in content time.",required=false,example="641.528")
		private Double contentEndTimePosition;
		
		@ApiModelProperty(value="TimePositionClass",required=true,example="MIDROLL")
		private String timePositionClass;
		
		@ApiModelProperty(value="AdFormat",required=true,example="CLEAN")
		private String adFormat;
		
		@ApiModelProperty(value="The amount of time that cannot be used for next ads",required=false,example="1")
		private Integer timeToNextAdUnit;
		
		@ApiModelProperty(value="CustomSlotId",required=false,example="Adbreak_2")
		private String customSlotId;
		
		@ApiModelProperty(value="Used to manually set the cue point sequence of the video",required=false,example="2")
		private Long orderId;
		
		
		public Double getContentStartTimePosition() {
			return contentStartTimePosition;
		}
		public void setContentStartTimePosition(Double contentStartTimePosition) {
			this.contentStartTimePosition = contentStartTimePosition;
		}
		public Double getContentEndTimePosition() {
			return contentEndTimePosition;
		}
		public void setContentEndTimePosition(Double contentEndTimePosition) {
			this.contentEndTimePosition = contentEndTimePosition;
		}
		public String getTimePositionClass() {
			return timePositionClass;
		}
		public void setTimePositionClass(String timePositionClass) {
			this.timePositionClass = timePositionClass;
		}
		public String getAdFormat() {
			return adFormat;
		}
		public void setAdFormat(String adFormat) {
			this.adFormat = adFormat;
		}
		public Integer getTimeToNextAdUnit() {
			return timeToNextAdUnit;
		}
		public void setTimeToNextAdUnit(Integer timeToNextAdUnit) {
			this.timeToNextAdUnit = timeToNextAdUnit;
		}
		public String getCustomSlotId() {
			return customSlotId;
		}
		public void setCustomSlotId(String customSlotId) {
			this.customSlotId = customSlotId;
		}
		public Long getOrderId() {
			return orderId;
		}
		public void setOrderId(Long orderId) {
			this.orderId = orderId;
		}
		
		
	}
}
