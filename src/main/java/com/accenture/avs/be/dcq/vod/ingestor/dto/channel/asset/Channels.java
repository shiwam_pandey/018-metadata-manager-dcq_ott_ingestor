//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.08.21 at 12:20:21 PM IST 
//


package com.accenture.avs.be.dcq.vod.ingestor.dto.channel.asset;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Channel" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="ChannelId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="Name" type="{http://accenture.avs.commontypes}StrMax50"/>
 *                   &lt;element name="Type" type="{http://accenture.avs.commontypes}StrMax50"/>
 *                   &lt;element name="Order" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="ExternalId" type="{http://accenture.avs.commontypes}StrMax255" minOccurs="0"/>
 *                   &lt;element name="Adult" type="{http://accenture.avs.commontypes}FlagTypeYN" minOccurs="0"/>
 *                   &lt;element name="Description" type="{http://accenture.avs.commontypes}StrMin1Max1000" minOccurs="0"/>
 *                   &lt;element name="PlatformList" type="{http://accenture.avs.channeltypes}platformList"/>
 *                   &lt;element name="PackageList" type="{http://accenture.avs.commontypes}packageList"/>
 *                   &lt;element name="AdvTags" type="{http://accenture.avs.commontypes}StrMin1Max1000" minOccurs="0"/>
 *                   &lt;element name="IsDisAllowedAdv" type="{http://accenture.avs.commontypes}FlagTypeYN" minOccurs="0"/>
 *                   &lt;element name="DefaultChannelNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="IsActive" type="{http://accenture.avs.commontypes}FlagTypeYN" minOccurs="0"/>
 *                   &lt;element name="BroadcasterName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ExtendedMetadata" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "channel"
})
@XmlRootElement(name = "Channels")
public class Channels {

    @XmlElement(name = "Channel", required = true)
    private List<Channels.Channel> channel;

    /**
     * Gets the value of the channel property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the channel property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getChannel().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Channels.Channel }
     * 
     * 
     */
    public List<Channels.Channel> getChannel() {
        if (channel == null) {
            channel = new ArrayList<Channels.Channel>();
        }
        return this.channel;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="ChannelId" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="Name" type="{http://accenture.avs.commontypes}StrMax50"/>
     *         &lt;element name="Type" type="{http://accenture.avs.commontypes}StrMax50"/>
     *         &lt;element name="Order" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="ExternalId" type="{http://accenture.avs.commontypes}StrMax255" minOccurs="0"/>
     *         &lt;element name="Adult" type="{http://accenture.avs.commontypes}FlagTypeYN" minOccurs="0"/>
     *         &lt;element name="Description" type="{http://accenture.avs.commontypes}StrMin1Max1000" minOccurs="0"/>
     *         &lt;element name="PlatformList" type="{http://accenture.avs.channeltypes}platformList"/>
     *         &lt;element name="PackageList" type="{http://accenture.avs.commontypes}packageList"/>
     *         &lt;element name="AdvTags" type="{http://accenture.avs.commontypes}StrMin1Max1000" minOccurs="0"/>
     *         &lt;element name="IsDisAllowedAdv" type="{http://accenture.avs.commontypes}FlagTypeYN" minOccurs="0"/>
     *         &lt;element name="DefaultChannelNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="IsActive" type="{http://accenture.avs.commontypes}FlagTypeYN" minOccurs="0"/>
     *         &lt;element name="BroadcasterName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ExtendedMetadata" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class Channel {

        @XmlElement(name = "ChannelId")
        private int channelId;
        @XmlElement(name = "Name", required = true)
        private String name;
        @XmlElement(name = "Type", required = true)
        private String type;
        @XmlElement(name = "Order")
        private int order;
        @XmlElement(name = "ExternalId")
        private String externalId;
        @XmlElement(name = "Adult")
        @XmlSchemaType(name = "string")
        private FlagTypeYN adult;
        @XmlElement(name = "Description")
        private String description;
        @XmlElement(name = "PlatformList", required = true)
        private PlatformList platformList;
        @XmlElement(name = "PackageList", required = true)
        private PackageList packageList;
        @XmlElement(name = "AdvTags")
        private String advTags;
        @XmlElement(name = "IsDisAllowedAdv")
        @XmlSchemaType(name = "string")
        private FlagTypeYN isDisAllowedAdv;
        @XmlElement(name = "DefaultChannelNumber")
        private Integer defaultChannelNumber;
        @XmlElement(name = "IsActive")
        @XmlSchemaType(name = "string")
        private FlagTypeYN isActive;
        @XmlElement(name = "BroadcasterName")
        private String broadcasterName;
        @XmlElement(name = "ExtendedMetadata")
        private String extendedMetadata;

        /**
         * Gets the value of the channelId property.
         * 
         */
        public int getChannelId() {
            return channelId;
        }

        /**
         * Sets the value of the channelId property.
         * 
         */
        public void setChannelId(int value) {
            this.channelId = value;
        }

        /**
         * Gets the value of the name property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getName() {
            return name;
        }

        /**
         * Sets the value of the name property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setName(String value) {
            this.name = value;
        }

        /**
         * Gets the value of the type property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getType() {
            return type;
        }

        /**
         * Sets the value of the type property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setType(String value) {
            this.type = value;
        }

        /**
         * Gets the value of the order property.
         * 
         */
        public int getOrder() {
            return order;
        }

        /**
         * Sets the value of the order property.
         * 
         */
        public void setOrder(int value) {
            this.order = value;
        }

        /**
         * Gets the value of the externalId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExternalId() {
            return externalId;
        }

        /**
         * Sets the value of the externalId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExternalId(String value) {
            this.externalId = value;
        }

        /**
         * Gets the value of the adult property.
         * 
         * @return
         *     possible object is
         *     {@link FlagTypeYN }
         *     
         */
        public FlagTypeYN getAdult() {
            return adult;
        }

        /**
         * Sets the value of the adult property.
         * 
         * @param value
         *     allowed object is
         *     {@link FlagTypeYN }
         *     
         */
        public void setAdult(FlagTypeYN value) {
            this.adult = value;
        }

        /**
         * Gets the value of the description property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescription() {
            return description;
        }

        /**
         * Sets the value of the description property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescription(String value) {
            this.description = value;
        }

        /**
         * Gets the value of the platformList property.
         * 
         * @return
         *     possible object is
         *     {@link PlatformList }
         *     
         */
        public PlatformList getPlatformList() {
            return platformList;
        }

        /**
         * Sets the value of the platformList property.
         * 
         * @param value
         *     allowed object is
         *     {@link PlatformList }
         *     
         */
        public void setPlatformList(PlatformList value) {
            this.platformList = value;
        }

        /**
         * Gets the value of the packageList property.
         * 
         * @return
         *     possible object is
         *     {@link PackageList }
         *     
         */
        public PackageList getPackageList() {
            return packageList;
        }

        /**
         * Sets the value of the packageList property.
         * 
         * @param value
         *     allowed object is
         *     {@link PackageList }
         *     
         */
        public void setPackageList(PackageList value) {
            this.packageList = value;
        }

        /**
         * Gets the value of the advTags property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAdvTags() {
            return advTags;
        }

        /**
         * Sets the value of the advTags property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAdvTags(String value) {
            this.advTags = value;
        }

        /**
         * Gets the value of the isDisAllowedAdv property.
         * 
         * @return
         *     possible object is
         *     {@link FlagTypeYN }
         *     
         */
        public FlagTypeYN getIsDisAllowedAdv() {
            return isDisAllowedAdv;
        }

        /**
         * Sets the value of the isDisAllowedAdv property.
         * 
         * @param value
         *     allowed object is
         *     {@link FlagTypeYN }
         *     
         */
        public void setIsDisAllowedAdv(FlagTypeYN value) {
            this.isDisAllowedAdv = value;
        }

        /**
         * Gets the value of the defaultChannelNumber property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getDefaultChannelNumber() {
            return defaultChannelNumber;
        }

        /**
         * Sets the value of the defaultChannelNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setDefaultChannelNumber(Integer value) {
            this.defaultChannelNumber = value;
        }

        /**
         * Gets the value of the isActive property.
         * 
         * @return
         *     possible object is
         *     {@link FlagTypeYN }
         *     
         */
        public FlagTypeYN getIsActive() {
            return isActive;
        }

        /**
         * Sets the value of the isActive property.
         * 
         * @param value
         *     allowed object is
         *     {@link FlagTypeYN }
         *     
         */
        public void setIsActive(FlagTypeYN value) {
            this.isActive = value;
        }

        /**
         * Gets the value of the broadcasterName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBroadcasterName() {
            return broadcasterName;
        }

        /**
         * Sets the value of the broadcasterName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBroadcasterName(String value) {
            this.broadcasterName = value;
        }

        /**
         * Gets the value of the extendedMetadata property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExtendedMetadata() {
            return extendedMetadata;
        }

        /**
         * Sets the value of the extendedMetadata property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExtendedMetadata(String value) {
            this.extendedMetadata = value;
        }

    }

}
