package com.accenture.avs.be.dcq.vod.ingestor.manager.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.accenture.avs.be.dcq.vod.ingestor.dto.EmfDTO;
import com.accenture.avs.be.dcq.vod.ingestor.manager.EmfManager;
import com.accenture.avs.be.dcq.vod.ingestor.repository.EmfRepository;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.configuration.CacheConstants;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.persistence.technicalcatalogue.EmfEntity;

@Component
public class EmfManagerImpl implements EmfManager {
	@Autowired
	private EmfRepository emfRepository;

	@Override
	public GenericResponse activateOrReactiveEmf(String emfName, String status) throws ApplicationException {
		// TODO Auto-generated method stub
		GenericResponse genericResponse = null;
		EmfEntity emfEntity;
		EmfDTO emfDto = null;
		List<EmfEntity> emfEntites = emfRepository.retrieveByEmfName(emfName);
		if (CollectionUtils.isEmpty(emfEntites)) {

			throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters(DcqVodIngestorConstants.EMFCONSTANTS.EMFNAME + " does not exist"));

		} else {
			emfEntity = emfEntites.get(0);
			emfEntity.setIsActive(status);
			emfRepository.save(emfEntity);
			emfDto = new EmfDTO();
			emfDto.setEmfId(emfEntity.getEmfId());
			if (emfEntity.getEmfName().length() <= 50) {
				emfDto.setEmfName(emfEntity.getEmfName());
			} else {
				throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
						new NestedParameters(DcqVodIngestorConstants.EMFCONSTANTS.EMFNAME,
								"emfName not more than 50 characters"));
			}
			emfDto.setIsActive(emfEntity.getIsActive());
			emfDto.setUpdateDate(new java.util.Date().getTime());
		}
		genericResponse = new GenericResponse(DcqVodIngestorConstants.ACN_200, DcqVodIngestorConstants.OK, emfDto);

		return genericResponse;
	}

}
