package com.accenture.avs.be.dcq.vod.ingestor.v1.sdp.manager.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.datatype.DatatypeConfigurationException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.content.dto.TRCEnablerRequest;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.AddSolOffer;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.PriceCategoryList;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.RentalPeriod;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.RequiredSolutionOfferList;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.SolutionOffer;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.SolutionOfferList;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ContentPlatformRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.RelPlatformTechnicalRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.TechnicalPackageRepository;
import com.accenture.avs.be.dcq.vod.ingestor.utils.CacheConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorRestUrls;
import com.accenture.avs.be.dcq.vod.ingestor.utils.Validator;
import com.accenture.avs.be.dcq.vod.ingestor.v1.sdp.manager.SDPV1Manager;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.be.framework.exception.ConfigurationException;
import com.accenture.avs.be.framework.utils.Utilities;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.commons.lib.LoggerWrapper.OtherSystemCallType;
import com.accenture.avs.persistence.technicalcatalogue.ContentEntity;
import com.accenture.avs.persistence.technicalcatalogue.ContentPlatformEntity;
import com.accenture.avs.persistence.technicalcatalogue.RelPlatformTechnicalEntity;
import com.accenture.avs.persistence.technicalcatalogue.RelPlatformTechnicalEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.TechnicalPackageEntity;
import com.accenture.avs.persistence.technicalcatalogue.TeckPkgTypeEntity;
import com.accenture.sdp.csmfe.webservices.clients.device.DeviceChannelType;
import com.accenture.sdp.csmfe.webservices.clients.device.SearchAllDeviceChannelsResponse;
import com.accenture.sdp.csmfe.webservices.clients.ingestor.EnablerSolOfferListRequest;
import com.accenture.sdp.csmfe.webservices.clients.ingestor.PartyGroupNameInfoRequest;
import com.accenture.sdp.csmfe.webservices.clients.ingestor.PartyGroupNameListRequest;
import com.accenture.sdp.csmfe.webservices.clients.ingestor.PriceCategoryLnkInfoRequest;
import com.accenture.sdp.csmfe.webservices.clients.ingestor.PriceCategoryLnkListRequest;
import com.accenture.sdp.csmfe.webservices.clients.ingestor.RequiredSolOfferListRequest;
import com.accenture.sdp.csmfe.webservices.clients.ingestor.SolutionOfferVodInfoRequest;
import com.accenture.sdp.csmfe.webservices.clients.ingestor.SolutionOfferVodListRequest;
import com.accenture.sdp.csmfe.webservices.clients.ingestor.TechnicalPackageDataRequest;
import com.accenture.sdp.csmfe.webservices.clients.ingestor.UpsertSolutionOfferVodResponse;


/**
 * @author karthik.vadla
 *
 */
@Component
public class SDPV1ManagerImpl implements SDPV1Manager {
	private static final LoggerWrapper log = new LoggerWrapper(SDPV1ManagerImpl.class);

	private static final String	PURCHASE_START_TIME	= "ST";
	private static final String	PURCHASE_END_TIME	= "ET";
	@Autowired
	private ContentPlatformRepository contentPlatformRepository;
	@Autowired
	private TechnicalPackageRepository technicalPackageRepository;
	@Autowired
	private RelPlatformTechnicalRepository relPlatformTechnicalRepository;
	@Autowired
	private DcqVodIngestorRestUrls dcqVodIngestorRestUrls;
	
	public void callToSDP(SolutionOfferList solutionOfferList, List<TRCEnablerRequest> trcEnablerRequest,
			ContentEntity content, Set<ContentPlatformEntity> contentPlatforms, List<String> assetPropertiesList,
			Configuration config) throws ConfigurationException, ApplicationException, JsonGenerationException, JsonMappingException, IOException {
		long startTime = System.currentTimeMillis();
		// if exist SolutionOfferList section
		if (solutionOfferList != null || trcEnablerRequest!=null) {
			// Create Technical Package from SolutionOfferList
			// Compare TeckPackage by pkgName and produce a List of tech pkg to create and to update
			
			generateSolutionOfferListObjects(solutionOfferList, content,contentPlatforms,assetPropertiesList,trcEnablerRequest, config);

		} else {// All SolutionOffer RVOD for the Content have to be disabled
			
			String socketTimeOut = config.getConstants().getValue(CacheConstants.HTTP_CLIENT_SOCKET_TIMEOUT);
			String connectiontimeOut = config.getConstants().getValue(CacheConstants.HTTP_CLIENT_CONNECTION_TIMEOUT);
			String sdpUrl = dcqVodIngestorRestUrls.SDP_URL;
			String sdpTenant = config.getConstants().getValue(CacheConstants.SDP_TENANT);
			
			SDPV1ClientManagerImpl sdpClientService = SDPV1ClientManagerImpl.getSdpClientService(Long.parseLong(connectiontimeOut), Long.parseLong(socketTimeOut));

			UpsertSolutionOfferVodResponse upsertSolutionOfferVod = sdpClientService.upsertSolutionOfferVodOnSDP(null, content.getContentId().longValue(), sdpUrl, sdpTenant);
			ObjectMapper mapper = new ObjectMapper();
			String responseBody = null;
			if(upsertSolutionOfferVod!=null){
				responseBody = mapper.writeValueAsString(upsertSolutionOfferVod);
			}
			log.logCallToOtherSystemResponseBody(DcqVodIngestorConstants.SDP, DcqVodIngestorConstants.UPSERT_SOL_OFFER_VOD, responseBody,
					OtherSystemCallType.INTERNAL);

			if (upsertSolutionOfferVod == null) {
				log.logCallToOtherSystemEnd(DcqVodIngestorConstants.SDP,DcqVodIngestorConstants.UPSERT_SOL_OFFER_VOD, "",
						"KO", "", "", System.currentTimeMillis() - startTime, OtherSystemCallType.INTERNAL);
				throw new ApplicationException(MessageKeys.ERROR_MS_ACN_916_SDP_NOT_AVAILABLE);
			}

			if (upsertSolutionOfferVod != null && !upsertSolutionOfferVod.getUpsertSolutionOfferVodResponse().getResultCode().equals("000")) {
				String responseSDP = "Sdp Service responseCode:'" + upsertSolutionOfferVod.getUpsertSolutionOfferVodResponse().getResultCode() + "' and responseDescritpion:'" + upsertSolutionOfferVod.getUpsertSolutionOfferVodResponse().getDescription() + "'. ";
				log.logMessage(responseSDP);
				String errorFromSDP = "";
				if (upsertSolutionOfferVod.getUpsertSolutionOfferVodResponse().getParameters().getParameter() != null && upsertSolutionOfferVod.getUpsertSolutionOfferVodResponse().getParameters().getParameter().size() > 0) {
					errorFromSDP = "Sdp Service in error for parameter:'" + upsertSolutionOfferVod.getUpsertSolutionOfferVodResponse().getParameters().getParameter().get(0).getName() + "' with value :'" + upsertSolutionOfferVod.getUpsertSolutionOfferVodResponse().getParameters().getParameter().get(0).getValue() + "'";
				}
				log.logMessage(errorFromSDP);
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,new NestedParameters("Error submitting request for upsert SolutionOfferVod on SDP for contentId = " + content.getContentId()+". " + errorFromSDP));
			}
		}
		
	}

	private void generateSolutionOfferListObjects(SolutionOfferList solutionOfferList, ContentEntity content, Set<ContentPlatformEntity> contentPlatforms,
			List<String> assetPropertiesList, List<TRCEnablerRequest> trcEnablerRequest, Configuration config) throws ConfigurationException, ApplicationException, JsonGenerationException, JsonMappingException, IOException {
		List<TechnicalPackageEntity> lstTechPkg = new ArrayList<TechnicalPackageEntity>();
		Long contentId = content.getContentId().longValue();
		String socketTimeOut = config.getConstants().getValue(CacheConstants.HTTP_CLIENT_SOCKET_TIMEOUT);
		String connectiontimeOut = config.getConstants().getValue(CacheConstants.HTTP_CLIENT_CONNECTION_TIMEOUT);
		String sdpUrl = dcqVodIngestorRestUrls.SDP_URL;
		String sdpTenant = config.getConstants().getValue(CacheConstants.SDP_TENANT);
		
		SDPV1ClientManagerImpl sdpClientService = SDPV1ClientManagerImpl.getSdpClientService(Long.parseLong(connectiontimeOut), Long.parseLong(socketTimeOut));

		List<RelPlatformTechnicalEntity> relPlatformTechnicalTvodList = new ArrayList<RelPlatformTechnicalEntity>();
		SolutionOfferVodListRequest solOfferList = new SolutionOfferVodListRequest();

		if(!Utilities.isEmpty(solutionOfferList) && !Utilities.isEmptyCollection(solutionOfferList.getSolutionOffer())){
		for (int i = 0; i < solutionOfferList.getSolutionOffer().size(); i++) {
			SolutionOffer solutionOffer = solutionOfferList.getSolutionOffer().get(i);

			// Retrieve starttime and endtime for varian PLATFORM_VIDEOTYPE
			Date cpStartTime = getVariantStartEndTime(content, contentPlatforms, solutionOffer.getPlatformName(), solutionOffer.getVideoType(), PURCHASE_START_TIME);
			Date cpEndTime = getVariantStartEndTime(content, contentPlatforms,solutionOffer.getPlatformName(), solutionOffer.getVideoType(), PURCHASE_END_TIME);

			for (int z = 0; z < solutionOffer.getRentalPeriodList().getRentalPeriod().size(); z++) {

				RentalPeriod rentalPeriod = solutionOffer.getRentalPeriodList().getRentalPeriod().get(z);

				// Set Package type RVOD
				TechnicalPackageEntity techPkg = new TechnicalPackageEntity();
				String pkgName = generateObjectName(solutionOffer, rentalPeriod, content, "PKG_");
				techPkg.setPackageName(pkgName);
				techPkg.setPackageDescription("Package for content: " + content.getContentId());
				techPkg.setIsEnabled("Y");

				if (rentalPeriod.getHours() == 0) {
					techPkg.setRentalPeriod(null);
				} else {
					techPkg.setRentalPeriod((int)rentalPeriod.getHours());
				}

				TeckPkgTypeEntity type = new TeckPkgTypeEntity();
				type.setPackageType("RVOD");
				techPkg.setTeckPkgType(type);

				// Prepare SolutionOffer section

				// FIXME: must retrieve the CP_ID from a map saved in the session and not using a query

				Integer cpId = contentPlatformRepository.retriveByVideoTypeAndPlatformAndContentId(solutionOffer.getVideoType(), solutionOffer.getPlatformName(), contentId.intValue());

				if (rentalPeriod.getAdditionalSolOfferList() != null && !rentalPeriod.getAdditionalSolOfferList().getAddSolOffer().isEmpty()) {
					// create tech pkg
					int addSolOfferIncrement = 0;
					for (AddSolOffer addSolOffer : rentalPeriod.getAdditionalSolOfferList().getAddSolOffer()) {
						// solOfferToWrite = new SolutionOfferVodInfoRequest();

						SolutionOfferVodInfoRequest solOfferToWrite = new SolutionOfferVodInfoRequest();
						//AVS-12973 start
						String propertyName = addSolOffer.getPropertyName();
						if(null != assetPropertiesList &&!assetPropertiesList.isEmpty()){
						 if(propertyName!=null && !assetPropertiesList.contains(propertyName)){
								throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,new NestedParameters("Incompatibility between asset level properties and commercial package external ID : "+addSolOffer.getExternalId()));
								
							}
						}else if(null == assetPropertiesList && propertyName!= null){
							throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,new NestedParameters("Incompatibility between asset level properties and commercial package external ID : "+addSolOffer.getExternalId()));
							
						}
						
						PartyGroupNameListRequest partyGroupNameListRequest = new PartyGroupNameListRequest();
						if(null != propertyName){
							PartyGroupNameInfoRequest partyGroupNameInfoRequest = new PartyGroupNameInfoRequest();
							partyGroupNameInfoRequest.setPartyGroupName(propertyName);
							partyGroupNameListRequest.getPartyGroup().add(partyGroupNameInfoRequest);
						}
						solOfferToWrite.setPartyGroups(partyGroupNameListRequest);
						//AVS-12973 stop
						
						
						
						solOfferToWrite.setExternalId(addSolOffer.getExternalId());
						solOfferToWrite.setCostCategory(addSolOffer.getCostCategoryId());
						
						solOfferToWrite.setRequiredSolOfferList(getRequiredSolOfferList(addSolOffer.getRequiredSolutionOfferList()));
						
						
						/*for(String requiredSolutionOffer : addSolOffer.getRequiredSolutionOfferList().getSolutionOfferExternalId()){
							solOfferToWrite.getRequiredSolOfferList().add(requiredSolutionOffer);
						}*/
						
						//						solOfferToWrite.setRequiredSolutionOfferList(getRequiredSolutionOfferList(addSolOffer.getRequiredSolutionOfferList()));
						String addSolOfferIncrementValue="";
						PriceCategoryLnkListRequest pcListReq = new PriceCategoryLnkListRequest();
						for (PriceCategoryList priceCategoryList : addSolOffer.getPriceCategoryList()) {
							PriceCategoryLnkInfoRequest pcInfoReq = new PriceCategoryLnkInfoRequest();
							pcInfoReq.setPaymentTypeId(1L);
							pcInfoReq.setPriceCategoryName(priceCategoryList.getPriceCategoryId());
							pcListReq.getPriceCategory().add(pcInfoReq);
							addSolOfferIncrementValue = priceCategoryList.getPriceCategoryId();
						}
						solOfferToWrite.setNrcPrices(pcListReq);
						if (addSolOffer.getCommerceModel() != null && !addSolOffer.getCommerceModel().trim().isEmpty()) {
							solOfferToWrite.setCommerceModel(addSolOffer.getCommerceModel());
						}
						// solOfferList.getSolutionOffer().add(solOfferToWrite);
						String solutionOfferName = generatePackageNameForAdditionalSolutionOffer(solutionOffer, rentalPeriod, content, "SOL_", addSolOffer.getCommerceModel());
						if((addSolOffer.getCommerceModel()==null || "".equals(addSolOffer.getCommerceModel())) && solutionOfferName!=null && !"".equals(solutionOfferName) && !"".equals(addSolOfferIncrementValue)){
							
							solOfferToWrite.setSolutionOfferName(solutionOfferName+"_"+addSolOfferIncrementValue);
							
						}else{
							solOfferToWrite.setSolutionOfferName(solutionOfferName);
						}
						
						addSolOfferIncrement++;
						solOfferToWrite.setSolutionOfferTitle(content.getTitle());
						solOfferToWrite.setSolutionOfferDescription("SolutionOffer related to the content: " + content.getTitle());
						/*
						 * solOfferToWrite.setCostCategory(rentalPeriod.getPriceCategoryId());
						 * solOfferToWrite.setExternalId(rentalPeriod.getExternalId());
						 */
						solOfferToWrite.setProfile(rentalPeriod.getProductProfile());
						solOfferToWrite.setSolutionId(1L);

						// 4.3

						// Manage start/end date for SolutionOfferTVOD
						try {
							// Find start time of TVOD product in RentalPeriod object
							if (rentalPeriod.getPurchaseStartDateTime() != null) {
								solOfferToWrite.setStartDate(Validator.Date2XmlGregorianCalendar(Validator.xmlGregorianCalendar2Date(rentalPeriod.getPurchaseStartDateTime())));
							} else {
								if (cpStartTime != null) {
									// Set start time of TVOD product in ContentPlatform object
									solOfferToWrite.setStartDate(Validator.Date2XmlGregorianCalendar(cpStartTime));
								} else {
									// Set start time of TVOD product in Content object
									solOfferToWrite.setStartDate(Validator.Date2XmlGregorianCalendar(content.getContractStart()));
								}

							}
							// Find end time of TVOD product in RentalPeriod object
							if (rentalPeriod.getPurchaseEndDateTime() != null) {
								solOfferToWrite.setEndDate(Validator.Date2XmlGregorianCalendar(Validator.xmlGregorianCalendar2Date(rentalPeriod.getPurchaseEndDateTime())));
							} else {
								if (cpEndTime != null) {
									// Set end time of TVOD product in ContentPlatform object
									solOfferToWrite.setEndDate(Validator.Date2XmlGregorianCalendar(cpEndTime));
								} else {
									// Set end time of TVOD product in Content object
									solOfferToWrite.setEndDate(Validator.Date2XmlGregorianCalendar(content.getContractEnd()));
								}

							}

						} catch (DatatypeConfigurationException e) {
							throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,new NestedParameters("Error during SDP operation for formatting the Start and End date!"));
							
						}
						/*
						 * 
						 * PriceCategoryLnkListRequest pcListReq = new PriceCategoryLnkListRequest(); PriceCategoryLnkInfoRequest pcInfoReq = new
						 * PriceCategoryLnkInfoRequest(); // FIXME:retrive a PaymentTypeId. At the moment is fixed to 1
						 * pcInfoReq.setPaymentTypeId(1L); pcInfoReq.setPriceCategoryName(rentalPeriod.getPriceCategoryId());
						 * pcListReq.getPriceCategory().add(pcInfoReq); solOfferToWrite.setNrcPrices(pcListReq);
						 */

						TechnicalPackageDataRequest technicalPackageDataRequest = new TechnicalPackageDataRequest();
						technicalPackageDataRequest.setContentPlatformId(cpId.longValue());

						// FIXME: must check the existence of a tech pkg with a one shot query and not using a query for any package
						Long pkgIdByName = technicalPackageRepository.retrievePackageIdByName(pkgName);
						if (pkgIdByName == null) {

							// Create pkg
							technicalPackageRepository.save(techPkg);
							
							techPkg.setExternalId(techPkg.getPackageId().toString());
							
							technicalPackageRepository.save(techPkg);
							
							// Manage relPlatformTechnical related to the package and contentPlatform
							RelPlatformTechnicalEntity relPlatformTechnicalTvod = new RelPlatformTechnicalEntity();
							RelPlatformTechnicalEntityPK relPlatformTechnicalPKTvod = new RelPlatformTechnicalEntityPK();

							relPlatformTechnicalPKTvod.setCpId(cpId);
							relPlatformTechnicalPKTvod.setPackageId(techPkg.getPackageId());
							relPlatformTechnicalPKTvod.setSbundleId(-1);
							relPlatformTechnicalTvod.setId(relPlatformTechnicalPKTvod);
							ContentEntity contentEntity = new ContentEntity();
							contentEntity.setContentId(contentId.intValue());
							relPlatformTechnicalTvod.setContent(contentEntity);

							log.logMessage("Saving relPlatformTech with couple cpid,pkgId: " + relPlatformTechnicalTvod.getId().getCpId() + "," + relPlatformTechnicalTvod.getId().getPackageId());

							relPlatformTechnicalTvodList.add(relPlatformTechnicalTvod);

//							// FIXME: Verify if the offer alreayd exits not with a query but insert/update on SDP with only one step.
//							if (techPkg.getExternalID() == null) {
//								String offerName = generatePackageNameForAdditionalSolutionOffer(solutionOffer, rentalPeriod, content, "OFF_", null);
//
//								Long offerId = sdpClientService.upsertOfferOnSDP(techPkg, offerName, sdpUrlConnection, sdpTenant, lstTechPkg);
//
//								if (offerId == null) {
//									throw new AVSConnectionToSdpException("Error during SDP operation!", new Throwable("Error submitting request for update Offer on SDP for packageName = " + techPkg.getPackageName()));
//								} else {
//									techPkg.setExternalID(offerId);
//								}
//							}

							// Set packageId for the new technical package
							technicalPackageDataRequest.setTechnicalPackageId(techPkg.getPackageId().longValue());
							solOfferToWrite.setTechnicalPackageData(technicalPackageDataRequest);
							solOfferList.getSolutionOffer().add(solOfferToWrite);

							// Technical Pkge already exist flow
						} else {
							log.logMessage("Package :" + pkgName + " already exist. Working on SolutionOffer...");

							// Set packageId for the technical package that already exist
							technicalPackageDataRequest.setTechnicalPackageId(pkgIdByName);
							solOfferToWrite.setTechnicalPackageData(technicalPackageDataRequest);
							solOfferList.getSolutionOffer().add(solOfferToWrite);

						}

					}
				}

				else {

					SolutionOfferVodInfoRequest solOfferToWrite = new SolutionOfferVodInfoRequest();
					solOfferToWrite.setSolutionOfferName(generatePackageNameForAdditionalSolutionOffer(solutionOffer, rentalPeriod, content, "SOL_", null));
					solOfferToWrite.setSolutionOfferTitle(content.getTitle());
					solOfferToWrite.setSolutionOfferDescription("SolutionOffer related to the content: " + content.getTitle());
					solOfferToWrite.setCostCategory(rentalPeriod.getPriceCategoryId());
					solOfferToWrite.setExternalId(rentalPeriod.getExternalId());
					solOfferToWrite.setProfile(rentalPeriod.getProductProfile());
					solOfferToWrite.setSolutionId(1L);

					// Manage start/end date for SolutionOfferTVOD
					try {
						// Find start time of TVOD product in RentalPeriod object
						if (rentalPeriod.getPurchaseStartDateTime() != null) {
							solOfferToWrite.setStartDate(Validator.Date2XmlGregorianCalendar(Validator.xmlGregorianCalendar2Date(rentalPeriod.getPurchaseStartDateTime())));
						} else {
							if (cpStartTime != null) {
								// Set start time of TVOD product in ContentPlatform object
								solOfferToWrite.setStartDate(Validator.Date2XmlGregorianCalendar(cpStartTime));
							} else {
								// Set start time of TVOD product in Content object
								solOfferToWrite.setStartDate(Validator.Date2XmlGregorianCalendar(content.getContractStart()));
							}

						}
						// Find end time of TVOD product in RentalPeriod object
						if (rentalPeriod.getPurchaseEndDateTime() != null) {
							solOfferToWrite.setEndDate(Validator.Date2XmlGregorianCalendar(Validator.xmlGregorianCalendar2Date(rentalPeriod.getPurchaseEndDateTime())));
						} else {
							if (cpEndTime != null) {
								// Set end time of TVOD product in ContentPlatform object
								solOfferToWrite.setEndDate(Validator.Date2XmlGregorianCalendar(cpEndTime));
							} else {
								// Set end time of TVOD product in Content object
								solOfferToWrite.setEndDate(Validator.Date2XmlGregorianCalendar(content.getContractEnd()));
							}

						}

					} catch (DatatypeConfigurationException e) {
						throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,new NestedParameters("Error during SDP operation for formatting the Start and End date!"));
						
					}

					PriceCategoryLnkListRequest pcListReq = new PriceCategoryLnkListRequest();
					PriceCategoryLnkInfoRequest pcInfoReq = new PriceCategoryLnkInfoRequest();
					// FIXME:retrive a PaymentTypeId. At the moment is fixed to 1
					pcInfoReq.setPaymentTypeId(1L);
					pcInfoReq.setPriceCategoryName(rentalPeriod.getPriceCategoryId());
					pcListReq.getPriceCategory().add(pcInfoReq);
					solOfferToWrite.setNrcPrices(pcListReq);

					TechnicalPackageDataRequest technicalPackageDataRequest = new TechnicalPackageDataRequest();
					technicalPackageDataRequest.setContentPlatformId(cpId.longValue());

					// FIXME: must check the existence of a tech pkg with a one shot query and not using a query for any package
					Long pkgIdByName = technicalPackageRepository.retrievePackageIdByName(pkgName);
					if (pkgIdByName == null) {

						// Create pkg
						technicalPackageRepository.save(techPkg);
						
						techPkg.setExternalId(techPkg.getPackageId().toString());
						
						technicalPackageRepository.save(techPkg);
						
						// Manage relPlatformTechnical related to the package and contentPlatform
						RelPlatformTechnicalEntity relPlatformTechnicalTvod = new RelPlatformTechnicalEntity();
						RelPlatformTechnicalEntityPK relPlatformTechnicalPKTvod = new RelPlatformTechnicalEntityPK();

						relPlatformTechnicalPKTvod.setCpId(cpId);
						relPlatformTechnicalPKTvod.setPackageId(techPkg.getPackageId());
						relPlatformTechnicalPKTvod.setSbundleId(-1);
						relPlatformTechnicalTvod.setId(relPlatformTechnicalPKTvod);
						ContentEntity contentEntity = new ContentEntity();
						contentEntity.setContentId(contentId.intValue());
						relPlatformTechnicalTvod.setContent(contentEntity);

						log.logMessage("Saving relPlatformTech with couple cpid,pkgId: " + relPlatformTechnicalTvod.getId().getCpId() + "," + relPlatformTechnicalTvod.getId().getPackageId());

						relPlatformTechnicalTvodList.add(relPlatformTechnicalTvod);

//						// FIXME: Verify if the offer alreayd exits not with a query but insert/update on SDP with only one step.
//						if (techPkg.getExternalID() == null) {
//							String offerName = generateObjectName(solutionOffer, rentalPeriod, content, "OFF_");
//
//							Long offerId = sdpClientService.upsertOfferOnSDP(techPkg, offerName, sdpUrlConnection, sdpTenant, lstTechPkg);
//
//							if (offerId == null) {
//								throw new AVSConnectionToSdpException("Error during SDP operation!", new Throwable("Error submitting request for update Offer on SDP for packageName = " + techPkg.getPackageName()));
//							} else {
//								techPkg.setExternalID(offerId);
//							}
//						}

						// Set packageId for the new technical package
						technicalPackageDataRequest.setTechnicalPackageId(techPkg.getPackageId().longValue());
						solOfferToWrite.setTechnicalPackageData(technicalPackageDataRequest);
						solOfferList.getSolutionOffer().add(solOfferToWrite);

						// Technical Pkge already exist flow
					} else {
						log.logMessage("Package :" + pkgName + " already exist. Working on SolutionOffer...");

						// Set packageId for the technical package that already exist
						technicalPackageDataRequest.setTechnicalPackageId(pkgIdByName);
						solOfferToWrite.setTechnicalPackageData(technicalPackageDataRequest);
						solOfferList.getSolutionOffer().add(solOfferToWrite);

					}

				}

			}

		}
		}
		else if (!Utilities.isEmptyCollection(trcEnablerRequest)){
			List<String> uniqueteList = new ArrayList<String>();

			for(TRCEnablerRequest transactionRateCard:trcEnablerRequest){
				Set<String> videoTypes= new HashSet<>();
				Set<String> platforms= new HashSet<>();
				Map<String, Long> cpList = new HashMap<>();
				
		
				
				
				
			TechnicalPackageEntity techPkg = new TechnicalPackageEntity();
			String pkgName = generateTRCPackageName(transactionRateCard, content, "PKG_");
			techPkg.setPackageName(pkgName);
			techPkg.setPackageDescription("Package for content: " + content.getContentId());
			techPkg.setIsEnabled("Y");
			techPkg.setRentalPeriod(transactionRateCard.getRentalPeriod().intValue());
			TeckPkgTypeEntity type = new TeckPkgTypeEntity();
			type.setPackageType("RVOD");
			techPkg.setTeckPkgType(type);
			
			// Prepare SolutionOffer section

			// FIXME: must retrieve the CP_ID from a map saved in the session and not using a query

			if(!Utilities.isEmptyCollection(contentPlatforms)){
				for(ContentPlatformEntity contentPlatform:contentPlatforms){
					videoTypes.add(contentPlatform.getVideoName());
					platforms.add(contentPlatform.getPlatform());
					cpList.put(contentPlatform.getPlatform()+","+contentPlatform.getVideoName(), contentPlatform.getCpId().longValue());
					
				}
			}
			
			if(Utilities.isEmpty(transactionRateCard.getPlatformList())){
				transactionRateCard.setPlatformList(new ArrayList<>(platforms));
			}
			
			if(Utilities.isEmpty(transactionRateCard.getVideoTypeList())){
				transactionRateCard.setVideoTypeList(new ArrayList<>(videoTypes));
			}
			
			
			
			
			for(String platform:transactionRateCard.getPlatformList()){
				for(String videoType:transactionRateCard.getVideoTypeList()){
					Long pkgIdByName = technicalPackageRepository.retrievePackageIdByName(pkgName);

					//Long cpId = getContentPlatformId(hibernateTemplate, contentId, platform, videoType);
					Long cpId=cpList.get(platform+","+videoType);
					if(platforms.contains(platform) && videoTypes.contains(videoType) && !Utilities.isEmpty(cpId)){
					SolutionOfferVodInfoRequest solOfferToWrite = new SolutionOfferVodInfoRequest();
					if(!Utilities.isEmptyCollection(transactionRateCard.getRequiredSolutionofferName())){
				//	solOfferToWrite.setRequiredSolOfferList(getRequiredSolOffer(transactionRateCard.getRequiredSolutionofferName()));
						//AVS-50295
						solOfferToWrite.setEnablerSolOfferList(getEnablerSolOffer(transactionRateCard.getRequiredSolutionofferName()));
					}

					//String prefix,String platform,String videoType,String hours,Content content,String trcName
					String solutionOfferName = generatePackageNameForTRCSolutionOffer("SOL_", platform, videoType,!Utilities.isEmpty(transactionRateCard.getRentalPeriod())? transactionRateCard.getRentalPeriod().toString():"", content,transactionRateCard.getName());
					solOfferToWrite.setSolutionOfferName(solutionOfferName);
					solOfferToWrite.setSolutionOfferTitle(content.getTitle());
					solOfferToWrite.setSolutionOfferDescription("SolutionOffer related to the content: " + content.getTitle());
					solOfferToWrite.setSolutionId(1L);
					solOfferToWrite.setTrcId(transactionRateCard.getTransactionRateCardId());
					solOfferToWrite.setExternalId(solOfferToWrite.getSolutionOfferName());
					if(transactionRateCard.getStartDate()!=null){
						try {
							// Set start time of TVOD product in ContentPlatform object
							solOfferToWrite.setStartDate(Validator.Date2XmlGregorianCalendar(new java.sql.Timestamp(transactionRateCard.getStartDate().getTime())));
						} catch (DatatypeConfigurationException e) {
							// TODO Auto-generated catch block
							log.logError(e);
						}
					}
					else{
						try {
							solOfferToWrite.setStartDate(Validator.Date2XmlGregorianCalendar(getVariantStartEndTime(content, contentPlatforms, platform, videoType, PURCHASE_START_TIME)));
						} catch (DatatypeConfigurationException e) {
							// TODO Auto-generated catch block
							log.logError(e);
						}
					
					}
					
					if(transactionRateCard.getEndDate()!=null){
						try {
							// Set start time of TVOD product in ContentPlatform object
							solOfferToWrite.setEndDate(Validator.Date2XmlGregorianCalendar(new java.sql.Timestamp(transactionRateCard.getEndDate().getTime())));
						} catch (DatatypeConfigurationException e) {
							// TODO Auto-generated catch block
							log.logError(e);
						}
					}
					else{
						try {
							solOfferToWrite.setEndDate(Validator.Date2XmlGregorianCalendar(getVariantStartEndTime(content,contentPlatforms, platform, videoType, PURCHASE_END_TIME)));
						} catch (DatatypeConfigurationException e) {
							// TODO Auto-generated catch block
							log.logError(e);
						}
					
					}
					TechnicalPackageDataRequest technicalPackageDataRequest = new TechnicalPackageDataRequest();
					technicalPackageDataRequest.setContentPlatformId(cpId);
					if(pkgIdByName==null){
						// Create pkg
						technicalPackageRepository.save(techPkg);
						
						techPkg.setExternalId(techPkg.getPackageId().toString());
						
						technicalPackageRepository.save(techPkg);
			
					}
						
					String uniqueness = new StringBuffer(cpId.toString()).append("_").append(techPkg.getPackageId()!=null?techPkg.getPackageId():pkgIdByName).toString();
						// Manage relPlatformTechnical related to the package and contentPlatform
					if(uniqueteList!=null  && !uniqueteList.contains(uniqueness))	{
					RelPlatformTechnicalEntity relPlatformTechnicalTvod = new RelPlatformTechnicalEntity();
						RelPlatformTechnicalEntityPK relPlatformTechnicalPKTvod = new RelPlatformTechnicalEntityPK();

						relPlatformTechnicalPKTvod.setCpId(cpId.intValue());
						relPlatformTechnicalPKTvod.setPackageId(techPkg.getPackageId()!=null?techPkg.getPackageId():pkgIdByName.intValue());
						relPlatformTechnicalPKTvod.setSbundleId(-1);
						relPlatformTechnicalTvod.setId(relPlatformTechnicalPKTvod);
						ContentEntity contentEntity = new ContentEntity();
						contentEntity.setContentId(contentId.intValue());
						relPlatformTechnicalTvod.setContent(contentEntity);
						String unique = new StringBuffer(cpId.toString()).append("_").append(techPkg.getPackageId()!=null?techPkg.getPackageId():pkgIdByName).toString();
						uniqueteList.add(unique);
						relPlatformTechnicalTvodList.add(relPlatformTechnicalTvod);
						
					}
					technicalPackageDataRequest.setTechnicalPackageId(techPkg.getPackageId()!=null?techPkg.getPackageId():pkgIdByName);
					solOfferToWrite.setTechnicalPackageData(technicalPackageDataRequest);
					solOfferList.getSolutionOffer().add(solOfferToWrite);

					/*}
					else{
						logger.info("Package :" + pkgName + " already exist. Working on SolutionOffer...");
						technicalPackageDataRequest.setTechnicalPackageId(pkgIdByName);
						solOfferToWrite.setTechnicalPackageData(technicalPackageDataRequest);
						solOfferList.getSolutionOffer().add(solOfferToWrite);

					}*/
						
					}


					
			
				}
			}

			}
			
		}
		
		// Manage SolutionOffer
		if (solOfferList.getSolutionOffer().size() > 0) {
			UpsertSolutionOfferVodResponse upsertSolutionOfferVod = sdpClientService.upsertSolutionOfferVodOnSDP(solOfferList, contentId, sdpUrl, sdpTenant);

			if (upsertSolutionOfferVod == null) {
				throw new ApplicationException(MessageKeys.ERROR_MS_ACN_916_SDP_NOT_AVAILABLE);
			}

			if (upsertSolutionOfferVod != null && !upsertSolutionOfferVod.getUpsertSolutionOfferVodResponse().getResultCode().equals("000")) {
				String responseSDP = "Sdp Service responseCode:'" + upsertSolutionOfferVod.getUpsertSolutionOfferVodResponse().getResultCode() + "' and responseDescritpion:'" + upsertSolutionOfferVod.getUpsertSolutionOfferVodResponse().getDescription() + "'. ";
				log.logMessage(responseSDP);
				String errorFromSDP = "";
				if (upsertSolutionOfferVod.getUpsertSolutionOfferVodResponse().getParameters().getParameter() != null && upsertSolutionOfferVod.getUpsertSolutionOfferVodResponse().getParameters().getParameter().size() > 0) {
					errorFromSDP = "Sdp Service in error for parameter:'" + upsertSolutionOfferVod.getUpsertSolutionOfferVodResponse().getParameters().getParameter().get(0).getName() + "' with value :'" + upsertSolutionOfferVod.getUpsertSolutionOfferVodResponse().getParameters().getParameter().get(0).getValue() + "'";
				}
				log.logMessage(errorFromSDP);
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,new NestedParameters("Error submitting request for upsert SolutionOfferVod on SDP for contentId = " + contentId+ ". " + errorFromSDP));
				
			}
		}
		else{
			log.logMessage("Commercial packages are not created for contentId "+ contentId);
		}

		// Update package with externalId
		if (!lstTechPkg.isEmpty()) {
			technicalPackageRepository.saveAll(lstTechPkg);
		}
		else{
			log.logMessage("Technical packages are not created for contentId "+ contentId);
		}

		// Save relPlatformTechnical related to the package and contentPlatform
		if (!relPlatformTechnicalTvodList.isEmpty()) {
			relPlatformTechnicalRepository.saveAll(relPlatformTechnicalTvodList);
		}
		
		
	}
	
	//AVS-50295
		private EnablerSolOfferListRequest getEnablerSolOffer(
				List<String> requiredSolutionOffer) {
			// TODO Auto-generated method stub
			EnablerSolOfferListRequest enablerSolutionOfferList = new EnablerSolOfferListRequest();
			
			if(!Utilities.isEmptyCollection(requiredSolutionOffer)){
			for(String id : requiredSolutionOffer){
				enablerSolutionOfferList.getEnablerSolutionOfferId().add(id);
			}
			}
			return enablerSolutionOfferList;
		}
	
	private String generateObjectName(SolutionOffer solutionOffer, RentalPeriod rentalPeriod, ContentEntity content, String prefix) {
		String offerName = null;

		offerName = prefix + solutionOffer.getPlatformName() + "_" + solutionOffer.getVideoType() + "_" + rentalPeriod.getHours() + "_" + content.getContentId();

		return offerName;
	}
	private RequiredSolOfferListRequest getRequiredSolOfferList(
			RequiredSolutionOfferList requiredSolutionOfferList) {
		// TODO Auto-generated method stub
		RequiredSolOfferListRequest requiredSolOfferListRequest = new RequiredSolOfferListRequest();
		
		if(requiredSolutionOfferList!=null){
		for(String id : requiredSolutionOfferList.getSolutionOfferExternalId()){
			requiredSolOfferListRequest.getRequiredSolutionOfferId().add(id);
		}
		}
		return requiredSolOfferListRequest;
	}
	
	private RequiredSolOfferListRequest getRequiredSolOffer(
			List<String> requiredSolutionOffer) {
		// TODO Auto-generated method stub
		RequiredSolOfferListRequest requiredSolOfferListRequest = new RequiredSolOfferListRequest();
		
		if(!Utilities.isEmptyCollection(requiredSolutionOffer)){
		for(String id : requiredSolutionOffer){
			requiredSolOfferListRequest.getRequiredSolutionOfferId().add(id);
		}
		}
		return requiredSolOfferListRequest;
	}

	private String generateTRCPackageName(TRCEnablerRequest solutionOffer,  ContentEntity content, String prefix) {
		String offerName = null;
		offerName = prefix + solutionOffer.getName() +"_"+ solutionOffer.getRentalPeriod() + "_" + content.getContentId();
		return offerName;
	}

	private String generatePackageNameForAdditionalSolutionOffer(SolutionOffer solutionOffer, RentalPeriod rentalPeriod, ContentEntity content, String prefix, String commerceModel) {
		String offerName = null;
		offerName = prefix + solutionOffer.getPlatformName() + "_" + solutionOffer.getVideoType() + "_" + rentalPeriod.getHours() +"_" + content.getContentId()+ (commerceModel != null && !commerceModel.isEmpty() ? "_" + commerceModel : "");
		return offerName;
	}
	
	private String generatePackageNameForTRCSolutionOffer(String prefix,String platform,String videoType,String hours,ContentEntity content,String trcName) {
		String offerName = null;
		offerName = prefix + platform + "_" + videoType+ "_" + hours +"_" + content.getContentId()+  "_" + trcName ;
		return offerName;
	}

	private Date getVariantStartEndTime(ContentEntity content, Set<ContentPlatformEntity> contentPlatforms, String platform, String videoType, String startOrEnd) {

		Set<ContentPlatformEntity> contentPlatformSet = contentPlatforms;
		Date startOrEndDate = null;

		for (Iterator<ContentPlatformEntity> iterator = contentPlatformSet.iterator(); iterator.hasNext();) {

			ContentPlatformEntity contentPlatform = new ContentPlatformEntity();
			contentPlatform = iterator.next();


			if (contentPlatform.getPlatform().equals(platform) && contentPlatform.getVideoName().equals(videoType)) {
				if (startOrEnd.equals(PURCHASE_START_TIME)) {
					if (contentPlatform.getContractStart() != null) {
						startOrEndDate = contentPlatform.getContractStart();
					}
				} else {
					if (contentPlatform.getContractEnd() != null) {
						startOrEndDate = contentPlatform.getContractEnd();
					}
				}
			}

		}

		return startOrEndDate;
	}

	@Override
	public Map<String, Long> searchAllDeviceChannels(Configuration config) throws ConfigurationException, JsonGenerationException, JsonMappingException, IOException {
		
		Map<String, Long> deviceChannels =  new HashMap<>();
		String socketTimeOut = config.getConstants().getValue(CacheConstants.HTTP_CLIENT_SOCKET_TIMEOUT);
		String connectiontimeOut = config.getConstants().getValue(CacheConstants.HTTP_CLIENT_CONNECTION_TIMEOUT);
		String sdpUrl = dcqVodIngestorRestUrls.SDP_URL;
		String sdpTenant = config.getConstants().getValue(CacheConstants.SDP_TENANT);
		
		SDPV1ClientManagerImpl sdpClientService = SDPV1ClientManagerImpl.getSdpClientService(Long.parseLong(connectiontimeOut), Long.parseLong(socketTimeOut));

		SearchAllDeviceChannelsResponse response = sdpClientService.getDeviceChannels(sdpUrl, sdpTenant);
		
		if(response!=null && response.getSearchAllDeviceChannelsResponse()!=null) {
		for(DeviceChannelType deviceChannel : response.getSearchAllDeviceChannelsResponse().getDeviceChannels().getDeviceChannel()) {
			deviceChannels.put( deviceChannel.getDeviceChannelName(),deviceChannel.getDeviceChannelId());
		}
		}else {
			deviceChannels = null;
		}
		
		return deviceChannels;
	}


	
}



