package com.accenture.avs.be.dcq.vod.ingestor.service;

import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ConfigurationException;

/**
 * @author karthik.vadla
 *
 */
public interface DcqVodIngestorService {

	GenericResponse dcqVodIngestor(String ingestionType, String mode, String switchAlias, Configuration config) throws ApplicationException;

	GenericResponse dynamicUpdate(Configuration config) throws ApplicationException;

	GenericResponse switckOrRollbackAlias(String mode, Configuration config) throws ApplicationException, ConfigurationException;
}
