package com.accenture.avs.be.dcq.vod.ingestor.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.avs.persistence.dto.TechnicalPackageDetailsDTO;
import com.accenture.avs.persistence.technicalcatalogue.RelPlatformTechnicalEntity;
/**
 * @author karthik.vadla
 *
 */
@Repository
public interface RelPlatformTechnicalRepository extends JpaRepository<RelPlatformTechnicalEntity,Integer>{

	/**
	 * @param allPublishedCpIds
	 * @return
	 */
	public List<TechnicalPackageDetailsDTO> retrieveTechnicalPakcgeDetailsbyCpIds(@Param("cpIds") List<Integer> allPublishedCpIds);

	@Modifying
	@Transactional
	public void deleteByContentIdAndPackageId(@Param("contentId") Integer contentId, @Param("packageId") Integer pkgIdNotRVOD);
	
	@Transactional
	@Modifying
	void deleteByBundleId(@Param("boundleId")Integer bundleId);
	
	@Transactional
	@Modifying
	void deleteByPackageId(@Param("packageId") Integer packageId);
	
	public List<RelPlatformTechnicalEntity> retriveByContentIdAndContentPlatformId(@Param("cpId") Integer cpId, @Param("contentId")Integer contentId);
	
	public RelPlatformTechnicalEntity getSBundleByContentIdAndPackageId(@Param("contentId") Integer contentId, @Param("cpId") Integer cpId,@Param("packageIds") Integer packageIds );

}
