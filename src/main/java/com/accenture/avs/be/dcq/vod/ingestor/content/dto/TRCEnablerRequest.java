package com.accenture.avs.be.dcq.vod.ingestor.content.dto;

import java.util.Date;
import java.util.List;

public class TRCEnablerRequest {
	 private static final long serialVersionUID = 1L;
	 
	    private Long transactionRateCardId;
	    private String name;
	    private String description;
	    private Long rentalPeriod;
	    private List<String> platformList;
	    private List<String> videoTypeList;
	    private String priceCategory;
	    private Date startDate;
	    private Date endDate;
	    private String isActive;
		private List<String> requiredSolutionofferName;

	    public List<String> getRequiredSolutionofferName() {
			return requiredSolutionofferName;
		}



		public void setRequiredSolutionofferName(List<String> requiredSolutionofferName) {
			this.requiredSolutionofferName = requiredSolutionofferName;
		}




	    
	   
	  /*  *//**
	     * @param transactionRateCardId
	     * @param name
	     * @param description
	     * @param rentalPeriod
	     * @param platformList
	     * @param videoTypeList
	     * @param priceCategory
	     * @param startDate
	     * @param endDate
	     * @param isActive
	     *//*
	    public GetTransactionRateCardResponse(Long transactionRateCardId, String name, String description,
	        Long rentalPeriod, List<String> platformList, List<String> videoTypeList, String priceCategory,
	        Date startDate, Date endDate, String isActive) {
	        super();
	        this.transactionRateCardId = transactionRateCardId;
	        this.name = name;
	        this.description = description;
	        this.rentalPeriod = rentalPeriod;
	        this.platformList = platformList;
	        this.videoTypeList = videoTypeList;
	        this.priceCategory = priceCategory;
	        this.startDate = startDate;
	        this.endDate = endDate;
	        this.isActive = isActive;
	    }*/



	    /**
	     * @return transactionRateCardId
	     */
	    public Long getTransactionRateCardId() {
	        return transactionRateCardId;
	    }

	    
	    /**
	     * @param transactionRateCardId
	     */
	    public void setTransactionRateCardId(Long transactionRateCardId) {
	        this.transactionRateCardId = transactionRateCardId;
	    }

	    
	    /**
	     * @return name
	     */
	    public String getName() {
	        return name;
	    }


	    
	    /**
	     * @param name
	     */
	    public void setName(String name) {
	        this.name = name;
	    }


	    
	    /**
	     * @return description
	     */
	    public String getDescription() {
	        return description;
	    }


	    
	    /**
	     * @param description
	     */
	    public void setDescription(String description) {
	        this.description = description;
	    }


	    
	    /**
	     * @return priceCategory
	     */
	    public String getPriceCategory() {
	        return priceCategory;
	    }


	    
	    /**
	     * @param priceCategory
	     */
	    public void setPriceCategory(String priceCategory) {
	        this.priceCategory = priceCategory;
	    }


	    
	    /**
	     * @return rentalPeriod
	     */
	    public Long getRentalPeriod() {
	        return rentalPeriod;
	    }


	    
	    /**
	     * @param rentalPeriod
	     */
	    public void setRentalPeriod(Long rentalPeriod) {
	        this.rentalPeriod = rentalPeriod;
	    }


	    
	    /**
	     * @return platformList
	     */
	    public List<String> getPlatformList() {
	        return platformList;
	    }


	    
	    /**
	     * @param platformList
	     */
	    public void setPlatformList(List<String> platformList) {
	        this.platformList = platformList;
	    }


	    
	    /**
	     * @return videoTypeList
	     */
	    public List<String> getVideoTypeList() {
	        return videoTypeList;
	    }


	    
	    /**
	     * @param videoTypeList
	     */
	    public void setVideoTypeList(List<String> videoTypeList) {
	        this.videoTypeList = videoTypeList;
	    }


	    
	    /**
	     * @return startDate
	     */
	    public Date getStartDate() {
	        return startDate;
	    }


	    
	    /**
	     * @param startDate
	     */
	    public void setStartDate(Date startDate) {
	        this.startDate = startDate;
	    }


	    
	    /**
	     * @return endDate
	     */
	    public Date getEndDate() {
	        return endDate;
	    }


	    
	    /**
	     * @param endDate
	     */
	    public void setEndDate(Date endDate) {
	        this.endDate = endDate;
	    }



	    
	    /**
	     * @return
	     */
	    public String getIsActive() {
	        return isActive;
	    }



	    
	    /**
	     * @param isActive
	     */
	    public void setIsActive(String isActive) {
	        this.isActive = isActive;
	    }


	   


}
