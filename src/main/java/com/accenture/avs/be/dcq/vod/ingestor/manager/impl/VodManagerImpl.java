package com.accenture.avs.be.dcq.vod.ingestor.manager.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.accenture.avs.be.dcq.vod.ingestor.cache.DeviceChannelsCache;
import com.accenture.avs.be.dcq.vod.ingestor.cache.PoliciesCache;
import com.accenture.avs.be.dcq.vod.ingestor.cache.PropertiesCache;
import com.accenture.avs.be.dcq.vod.ingestor.cache.TRCCache;
import com.accenture.avs.be.dcq.vod.ingestor.content.dto.ContentPlatformVideoType;
import com.accenture.avs.be.dcq.vod.ingestor.content.dto.CopyProtectionDTO;
import com.accenture.avs.be.dcq.vod.ingestor.content.dto.GetTransactionRateCardResponse;
import com.accenture.avs.be.dcq.vod.ingestor.content.dto.TRCEnablerRequest;
import com.accenture.avs.be.dcq.vod.ingestor.dto.CommercialPackagesResponseDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.CommercialPackagesResponseDTO.CommercialPackage;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Actors;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.AddSolOffer;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Anchors;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Asset;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.BlackListDeviceTypes;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.CategoryId;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.CategoryList;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Chapter;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Chapter.MultiLanguage.ChapterLanguage;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.ChapterList;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.CopyProtection;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Directors;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.EMFAttribute;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.EMFAttributeList;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.ExtendedRatingList;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Extensions;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.LangType;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Languages;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.PackageList;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Platform;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.PlatformList;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Producers;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Property;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.RentalPeriod;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Scene.MultiLanguage;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Scene.MultiLanguage.SceneLanguage;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.SceneList;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.SolutionOffer;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.SolutionOfferList;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Subtitle;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.TRC;
import com.accenture.avs.be.dcq.vod.ingestor.manager.VodManager;
import com.accenture.avs.be.dcq.vod.ingestor.repository.AudiolangRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.BlacklistDeviceContentRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.BundleAggregationRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.CategoryRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ChannelRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ChapterRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ContentCategoryRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ContentLinkingRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ContentPlatformRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ContentRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.EmfRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.LanguageRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.MultiLanguageChapterRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.MultiLanguageContentRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.MultiLanguageSceneRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ObjectEmfRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ObjectPlatformRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.RelContentExtRatRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.RelPlatformTechnicalRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.SceneRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.SubtitlesRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.TechnicalPackageRepository;
import com.accenture.avs.be.dcq.vod.ingestor.sdp.manager.SDPManager;
import com.accenture.avs.be.dcq.vod.ingestor.sdp.manager.impl.SdpClientManagerImpl;
import com.accenture.avs.be.dcq.vod.ingestor.utils.CacheConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.CopyProtectionUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorRestUrls;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.JsonUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.Validator;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.be.framework.exception.ConfigurationException;
import com.accenture.avs.be.framework.utils.Utilities;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.commons.lib.LoggerWrapper.OtherSystemCallType;
import com.accenture.avs.commons.lib.httpclient.HttpClientAdapter;
import com.accenture.avs.commons.lib.httpclient.HttpClientAdapter.HttpClientAdapterConfiguration;
import com.accenture.avs.commons.lib.httpclient.HttpClientAdapter.HttpClientException;
import com.accenture.avs.persistence.technicalcatalogue.AudiolangEntity;
import com.accenture.avs.persistence.technicalcatalogue.AudiolangEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.BlackListDeviceEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.BlacklistDeviceEntity;
import com.accenture.avs.persistence.technicalcatalogue.CategoryEntity;
import com.accenture.avs.persistence.technicalcatalogue.ChannelEntity;
import com.accenture.avs.persistence.technicalcatalogue.ChapterEntity;
import com.accenture.avs.persistence.technicalcatalogue.ChapterEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.ContentCategoryEntity;
import com.accenture.avs.persistence.technicalcatalogue.ContentCategoryEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.ContentEntity;
import com.accenture.avs.persistence.technicalcatalogue.ContentLinkingEntity;
import com.accenture.avs.persistence.technicalcatalogue.ContentLinkingEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.ContentPlatformEntity;
import com.accenture.avs.persistence.technicalcatalogue.ContentTypeEntity;
import com.accenture.avs.persistence.technicalcatalogue.EmfEntity;
import com.accenture.avs.persistence.technicalcatalogue.ExtendedContentAttributeEntity;
import com.accenture.avs.persistence.technicalcatalogue.LanguageMetadataEntity;
import com.accenture.avs.persistence.technicalcatalogue.MultiLanguageContentEntity;
import com.accenture.avs.persistence.technicalcatalogue.MultiLanguageContentEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.MultilanguageChapterEntity;
import com.accenture.avs.persistence.technicalcatalogue.MultilanguageChapterEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.MultilanguageSceneEntity;
import com.accenture.avs.persistence.technicalcatalogue.MultilanguageSceneEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.ObjectEmfEntity;
import com.accenture.avs.persistence.technicalcatalogue.ObjectEmfEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.ObjectPlatformEntity;
import com.accenture.avs.persistence.technicalcatalogue.RelContentExtRatEntity;
import com.accenture.avs.persistence.technicalcatalogue.RelContentExtRatEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.RelPlatformTechnicalEntity;
import com.accenture.avs.persistence.technicalcatalogue.RelPlatformTechnicalEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.SceneEntity;
import com.accenture.avs.persistence.technicalcatalogue.SceneEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.SubtitleEntity;
import com.accenture.avs.persistence.technicalcatalogue.SubtitleEntityPK;

/**
 * @author karthik.vadla
 *
 */
@Component
@Transactional(rollbackFor = { ApplicationException.class, RuntimeException.class })
public class VodManagerImpl implements VodManager {

	private static final LoggerWrapper log = new LoggerWrapper(VodManagerImpl.class);

	private static final String DEFAULT_IS_NO_ADULT = "N";
	private static final String DEFAULT_IS_PUSLISHED = "Y";
	private static final List<String> commonDataType = new ArrayList<String>();
	
	private static ObjectMapper mapper = new ObjectMapper();

	static {
		commonDataType.add("BUNDLE");
		commonDataType.add("GROUP_OF_BUNDLES");
		commonDataType.add("GROUP_OF_BUNDLE");
	}

	private static final String PIPE_STRING = "|";
	private static final String COMMA_STRING = ",";
	private static final String LANG_META_DATA_IS_ACTIVE = "Y";
	private static final String LANG_META_DATA_IS_DEFAULT = "N";

	@Autowired
	private DeviceChannelsCache deviceChannelsCache;
	@Autowired
	private TRCCache trcCache;
	@Autowired
	private PropertiesCache propertyCache;
	@Autowired
	private PoliciesCache policyCache;
	@Autowired
	private CopyProtectionUtils copyProtectionUtils;
	
	@Autowired
	private BundleAggregationRepository bundleAggRepository;

	private List<String> propertiesList = new ArrayList<String>();

	@Autowired
	private ContentRepository contentRepository;
	@Autowired
	private CategoryRepository categoryRepository;
	@Autowired
	private ContentCategoryRepository contentCategoryRepository;
	@Autowired
	private RelContentExtRatRepository relcontentExtRatRepository;
	@Autowired
	private AudiolangRepository audioLangRepository;

	@Autowired
	private SubtitlesRepository subtitlesRepository;

	@Autowired
	private ContentLinkingRepository contentLinkingRepository;

	@Autowired
	private RelPlatformTechnicalRepository relPlatformTechnicalRepository;

	@Autowired
	private ContentPlatformRepository contentPlatformRepository;

	@Autowired
	private MultiLanguageContentRepository multiLanguageContentRepository;
	@Autowired
	private MultiLanguageSceneRepository multiLanguageSceneRepository;
	@Autowired
	private MultiLanguageChapterRepository multiLanguageChapterRepository;

	@Autowired
	private BlacklistDeviceContentRepository blacklistDeviceContentRepository;

	@Autowired
	private SceneRepository sceneRepository;
	@Autowired
	private ChapterRepository chapterRepository;

	@Autowired
	private ObjectEmfRepository objectEmfRepository;

	@Autowired
	private LanguageRepository languageRepository;

	@Autowired
	private EmfRepository emfRepository;
	
	@Autowired
	private ObjectPlatformRepository objectPlatformRepository;
	@Autowired
	private TechnicalPackageRepository technicalPackageRepository;

	@Autowired
	private ChannelRepository channelRepository;

	@Autowired
	private SDPManager sdpManager;
	
	@Autowired
	private DcqVodIngestorRestUrls dcqVodIngestorRestUrls;

	@Override
	public Integer ingestContent(Asset asset, String contentXML, Configuration config)
			throws ConfigurationException, ApplicationException, UnsupportedEncodingException, ParseException,Exception {

		List<TRCEnablerRequest> trcEnablerRequestList = null;
		List<Long> duplicateList = null;

		if (asset.getContentId() == null) {
			Long id = contentRepository.getContentIdByExternalId(asset.getHouseId());
			if (id == null || id == 0) {
				id = contentRepository.getMaxId();
				if(id == null || id == 0){
					id = 0L;
				}
				id = id + 1;
			}
			asset.setContentId(id);
			log.logMessage("Mapping asset to content externalId " + asset.getHouseId());
		
		} else {
			Long id = contentRepository.getContentIdByExternalId(asset.getHouseId());

			if (id != null && !id.equals(asset.getContentId().longValue())) {
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
						new NestedParameters("HouseId already exists for other content"));
			}
			log.logMessage("Mapping asset to content for contentId: " + asset.getContentId() + " , externalId "
					+ asset.getHouseId());
		}

		if (config.getConstants().getValue(CacheConstants.COPY_PROTECTION_IN_PANIC).equalsIgnoreCase("N")) {

			String isCopyProtected = null;

			if (!Validator.isEmpty(asset.getIsCopyProtected())) {

				isCopyProtected = asset.getIsCopyProtected().value();
				List<CopyProtectionDTO> copyProtections = null;
				CopyProtectionDTO copyProtectionDTO = null;
				if (null != asset.getCopyProtectionList()) {
					copyProtections = new ArrayList<>();
					for (CopyProtection copyProtection : asset.getCopyProtectionList().getCopyProtection()) {
						copyProtectionDTO = new CopyProtectionDTO();
						copyProtectionDTO.setSecurityCode(copyProtection.getSecurityCode());
						copyProtectionDTO.setSecurityOption(copyProtection.getSecurityOption());
						copyProtections.add(copyProtectionDTO);
					}
				}
				log.logMessage("copy protection validations start");
				copyProtectionUtils.validateCopyProtections(isCopyProtected, copyProtections, config);
				log.logMessage("copy protection validations end");

			} else if (Validator.isEmpty(asset.getIsCopyProtected())
					&& (!Validator.isEmpty(asset.getCopyProtectionList())
							&& !Validator.isEmpty(asset.getCopyProtectionList().getCopyProtection()))) {
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
						new NestedParameters("CopyProtections not allowed when IsCopyProtected is N"));

			}

		} else {
			log.logMessage("CopyProtection is in Panic");
		}

		// Start mapping Asset to Content
		ContentEntity content = new ContentEntity();

		Set<ContentPlatformEntity> contentPlatforms = null;
		Set<RelContentExtRatEntity> relContentExtRats = null;
		Set<ContentCategoryEntity> contentCategories = null;
		Set<SceneEntity> listOfScenes = null;
		Set<ChapterEntity> listOfChapters = null;
		Set<MultilanguageSceneEntity> listOfMultilanguageScenes = null;
		Set<MultilanguageChapterEntity> listOfMultilanguageChapters = null;
		Set<ObjectPlatformEntity> scenePlatforms = null;
		Set<ObjectPlatformEntity> chapterPlatforms = null;
		Set<ObjectEmfEntity> objectEmfAttributes = null;

		List<TRCEnablerRequest> trcEnablerRequest = null;
		SolutionOfferList solutionOfferList = null;

		content.setContentId(asset.getContentId().intValue());
		content.setContentProvider(asset.getContentProvider());
		content.setExternalContentId(asset.getHouseId());

		ContentTypeEntity contentType = new ContentTypeEntity();
		contentType.setName(asset.getType());
		content.setContentType(contentType);
		// content.setType(contentType);
		content.setTitle(asset.getTitle());
		content.setDuration(asset.getDuration());

		// AS per story 18901 start
		if (!Utilities.isEmpty(asset.getExtensions().getTRCList())) {

			if (!Utilities.isEmpty(asset.getExtensions())
					&& !Utilities.isEmpty(asset.getExtensions().getSolutionOfferList())) {
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
						new NestedParameters("Both SolutionOfferList and TRC are not allowed"));
			}

			trcEnablerRequestList = new ArrayList<TRCEnablerRequest>();
			duplicateList = new ArrayList<Long>();
			Map<Long, GetTransactionRateCardResponse> trcIdCache = trcCache.getTrcIdCache();
			Map<String, GetTransactionRateCardResponse> trcNameCache = trcCache.getTrcNameCache();

			if (null == trcIdCache || null == trcNameCache) {
				throw new ApplicationException(MessageKeys.ERROR_MS_ACN_901_COMMERCE_NOT_AVAILABLE);
			}

			if (trcIdCache == null || trcIdCache.isEmpty() || trcNameCache == null || trcNameCache.isEmpty()) {
				log.logMessage("Calling Configuration MS to reload the propertiesCache");
				trcIdCache = trcCache.loadTRCS(config);
				trcNameCache = trcCache.loadTRCList(config);

			}

			// Check TRC dates are overlapping or not
			validateTRCList(trcIdCache, trcNameCache, asset.getExtensions().getTRCList().getTRC());
			for (TRC trc : asset.getExtensions().getTRCList().getTRC()) {
				GetTransactionRateCardResponse transactionRateCard = null;
				TRCEnablerRequest trcEnabler = new TRCEnablerRequest();

				if (null == trc.getTRCId().getValue()) {
					
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters("Invalid TRC"));
				} else {

					if (null == trc.getTRCId().getIsExternalId() || (null != trc.getTRCId().getIsExternalId()
							&& ("N").equals(trc.getTRCId().getIsExternalId().toString().trim()))) {
						transactionRateCard = trcIdCache.get(Long.parseLong(trc.getTRCId().getValue()));

						if (null == transactionRateCard || ("N").equals(transactionRateCard.getIsActive())) {
							throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
									new NestedParameters("Invalid TRC"));

						}

						if (transactionRateCard.getEndDate() != null
								&& transactionRateCard.getEndDate().before(new Date())) {
							throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
									new NestedParameters("Expired TRC provided"));

						}

						trcEnabler.setDescription(transactionRateCard.getDescription());
						trcEnabler.setEndDate(transactionRateCard.getEndDate());
						trcEnabler.setIsActive(transactionRateCard.getIsActive());
						trcEnabler.setName(transactionRateCard.getName());
						trcEnabler.setPlatformList(transactionRateCard.getPlatformList());
						trcEnabler.setPriceCategory(transactionRateCard.getPriceCategory());
						trcEnabler.setStartDate(transactionRateCard.getStartDate());
						trcEnabler.setTransactionRateCardId(transactionRateCard.getTransactionRateCardId());
						trcEnabler.setVideoTypeList(transactionRateCard.getVideoTypeList());
						trcEnabler.setRentalPeriod(transactionRateCard.getRentalPeriod());
						trcEnabler.setTransactionRateCardId(transactionRateCard.getTransactionRateCardId());
						if (!Utilities.isEmpty(trc.getEnablerCommercialPackageList()) && !Utilities.isEmptyCollection(
								trc.getEnablerCommercialPackageList().getCommercialPackageExternalId())) {
							trcEnabler.setRequiredSolutionofferName(
									trc.getEnablerCommercialPackageList().getCommercialPackageExternalId());
						}
						if (!Utilities.isEmptyCollection(duplicateList)
								&& duplicateList.contains(trcEnabler.getTransactionRateCardId())) {
							throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
									new NestedParameters("Duplicate TRC found"));

						}
						trcEnablerRequestList.add(trcEnabler);
						duplicateList.add(trcEnabler.getTransactionRateCardId());
					} else if (null != trcNameCache.get(trc.getTRCId().getValue())
							&& ("Y").equals(trc.getTRCId().getIsExternalId().toString().trim())) {
						transactionRateCard = trcNameCache.get(trc.getTRCId().getValue());

						if (null == transactionRateCard || ("N").equals(transactionRateCard.getIsActive())) {
							throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
									new NestedParameters("Invalid TRC"));

						}

						if (transactionRateCard.getEndDate() != null
								&& transactionRateCard.getEndDate().before(new Date())) {
							throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
									new NestedParameters("Expired TRC provided"));

						}
						trcEnabler.setDescription(transactionRateCard.getDescription());
						trcEnabler.setEndDate(transactionRateCard.getEndDate());
						trcEnabler.setIsActive(transactionRateCard.getIsActive());
						trcEnabler.setName(transactionRateCard.getName());
						trcEnabler.setPlatformList(transactionRateCard.getPlatformList());
						trcEnabler.setPriceCategory(transactionRateCard.getPriceCategory());
						trcEnabler.setTransactionRateCardId(transactionRateCard.getTransactionRateCardId());
						trcEnabler.setStartDate(transactionRateCard.getStartDate());
						trcEnabler.setTransactionRateCardId(transactionRateCard.getTransactionRateCardId());
						trcEnabler.setVideoTypeList(transactionRateCard.getVideoTypeList());
						trcEnabler.setRentalPeriod(transactionRateCard.getRentalPeriod());
						if (!Utilities.isEmpty(trc.getEnablerCommercialPackageList()) && !Utilities.isEmptyCollection(
								trc.getEnablerCommercialPackageList().getCommercialPackageExternalId())) {
							trcEnabler.setRequiredSolutionofferName(
									trc.getEnablerCommercialPackageList().getCommercialPackageExternalId());
						}
						if (!Utilities.isEmptyCollection(duplicateList)
								&& duplicateList.contains(trcEnabler.getTransactionRateCardId())) {
							throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
									new NestedParameters("Duplicate TRC found"));

						}
						trcEnablerRequestList.add(trcEnabler);
						duplicateList.add(trcEnabler.getTransactionRateCardId());

					}

					else {
						throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
								new NestedParameters("Invalid TRC"));

					}
				

				}

			}

		}

		// AS per story 18901 end

		// AVS-12973 start
		propertiesList = new ArrayList<>();
		if (asset.getProperties() != null && !asset.getProperties().getProperty().isEmpty()) {
			Map<String, String> propertiesCache = propertyCache.getProperties();

			if (propertiesCache == null || propertiesCache.isEmpty()) {
				// AVS-18140 start - reloading the cache inorder to fetch latest properties
				log.logMessage("Calling Configuration MS to reload the propertiesCache");
				propertiesCache = propertyCache.loadProperties(config);
				// AVS-18140 stop
			}
			if (null == propertiesCache) {
				throw new ApplicationException(MessageKeys.ERROR_MS_ACN_902_CONFIGURATION_NOT_AVAILABLE);

			}
			// check asset property with property cache
			for (Property property : asset.getProperties().getProperty()) {
				String propertyName = property.getPropertyName();
				if (propertiesCache == null || null == propertiesCache.get(propertyName)) {
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters("Property does not exists"));

				}
				// check extendedmetadata is valid
				String propertyExtendedMetadata = property.getExtendedMetadata();
				if (!StringUtils.isEmpty(propertyExtendedMetadata)
						&& !JsonUtils.isJSONValid(propertyExtendedMetadata)) {
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters("Invalid ExtendedMetadata json for Property " + propertyName));

				}
				propertiesList.add(propertyName);
			}
		}

		// AVS-12973 stop

		/* AVS-13824 Start */
		content.setIsOutOfHome(asset.getIsNotAvailableOutOfHome());
		/* AVS-13824 End */

		if (asset.getGenre() == null && ((asset.getGenreList() == null)
				|| ((asset.getGenreList() != null && asset.getGenreList().getGenreItem() == null))
				|| (asset.getGenreList() != null && asset.getGenreList().getGenreItem() != null
						&& asset.getGenreList().getGenreItem().isEmpty()))) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
					new NestedParameters("Genre or GenreList is mandatory"));
		}

		content.setGenre(asset.getGenre());
		if ((asset.getGenreList() != null && asset.getGenreList().getGenreItem() != null
				&& !asset.getGenreList().getGenreItem().isEmpty())) {
			content.setGenre(Validator.list2String(asset.getGenreList().getGenreItem(), ",", 500));
		}

		content.setIsHd(asset.getIsHD().value());
		content.setEpisodeTitle(asset.getEpisodeTitle());

		if (asset.getEpisodeId() != null) {
			content.setEpisode(asset.getEpisodeId().intValue());
		}
		if (asset.getIsEncrypted() != null) {
			content.setIsEncrypted(asset.getIsEncrypted().value());
		}

		if (asset.getCategory() != null) {
			content.setCategoryId(asset.getCategory().intValue());
		}

		if (asset.getEntitlementId() != null) {
			content.setEntitlementId(asset.getEntitlementId().intValue());
		}

		content.setContractStart(Validator.xmlGregorianCalendar2Date(asset.getStartDateTime()));
		content.setContractEnd(Validator.xmlGregorianCalendar2Date(asset.getEndDateTime()));

		if (asset.getIsRecommended() == null) {
			content.setIsRecommended("N");// default is not reccomended
		} else {
			content.setIsRecommended(asset.getIsRecommended().value());
					}
		// Language mapping. The multilanguage mapping is in MAPPING COMPOSED OBJECTS
		// code
		if (asset.getLanguage() != null) {
			content.setLanguage(asset.getLanguage());
		}
		if (asset.getSummaryShort() != null) {
			content.setShortDescription(asset.getSummaryShort());
		}
		if (asset.getSummaryLong() != null) {
			content.setLongDescription(asset.getSummaryLong());
		}
		if (asset.getEventPlace() != null) {
			content.setEventPlace(asset.getEventPlace());
		}
		if (asset.getYear() != null) {
			content.setContentYear(String.valueOf(asset.getYear().getYear()));
		}
		if (asset.getLastBroadcastDate() != null) {
			content.setLastBroadcastDate(Validator.xmlGregorianCalendar2Date(asset.getLastBroadcastDate()));
		}

		if (asset.getSeason() != null) {
			content.setSeason(asset.getSeason().toString());
		}
		if (asset.getSeries() != null) {
			content.setSeries(asset.getSeries().intValue());
		}
		if (asset.getStarRating() != null) {
			content.setStarRating(asset.getStarRating());
		}
		if (asset.getGeoBlocking() == null) {
			content.setIsGeoBlocked("N"); // default is not geoblocked
		} else {
			content.setIsGeoBlocked(asset.getGeoBlocking().getDefault().value());
		}
		if (asset.getAdditionalData() != null) {
			content.setAdditionalData00(asset.getAdditionalData());
		}
		/*
		 * MAPPING COMPOSED OBJECTS - START
		 */
		// Price category
		// PriceCategory priceCat = new PriceCategory();
		if (asset.getPriceCategory() != null) {
			content.setPriceCategoryId(asset.getPriceCategory().longValue());
		}

		// PC_LEVEL
		String pcLevelValue = config.getPcLevels().getKey(asset.getParentalRating());

		if (pcLevelValue == null) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters("Parental control level " + asset.getParentalRating()
							+ " does not exist in AVS! The asset will be discared."));

		} else {
			content.setPcLevel(pcLevelValue);
		}

		// LastChannel
		ChannelEntity channel = null;
		if (asset.getLastBroadcastChannel() != null) {
			channel = getChannelName(asset.getLastBroadcastChannel());
			if (channel == null) {
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
						new NestedParameters("The related ChannelName does not exist!"));
			} else {
				content.setBroadcastChannelName(channel.getName());
			}

		}

		// List objects
		Directors r = asset.getDirectors();
		if (r != null) {
			content.setDirectors(Validator.list2String(r.getDirector(), ",", 800));}

		Actors a = asset.getActors();
		if (a != null) {
			content.setActors(Validator.list2String(a.getActor(), ",", 800));}

		Anchors c = asset.getAnchors();
		if (c != null) {
			content.setAnchors(Validator.list2String(c.getAnchor(), ",", 800));}

		Producers prod = asset.getProducers();
		if (prod != null) {
			content.setAuthors(Validator.list2String(prod.getProducer(), ",", 800));}

		if (asset.getCountriesOfOrigin() != null && asset.getCountriesOfOrigin().getCountryOfOrigin() != null) {
			content.setCountry(Validator.list2String(asset.getCountriesOfOrigin().getCountryOfOrigin(), ",", 800));}

		Extensions extensions = asset.getExtensions();

		if (extensions == null) {
			content.setIsAdult(DEFAULT_IS_NO_ADULT); // Default value
		} else {
			content.setIsAdult(extensions.getIsAdult().value());
		}

		// content.setContentPlatforms(getVodContentPlatform(asset, extensions,
		// config));

		contentPlatforms = getVodContentPlatform(asset, extensions, config);

		// content.setRelContentExtRats(getVodRelContentExtRat(asset, extensions,
		// config));

		relContentExtRats = getVodRelContentExtRat(asset, extensions, config);

		// content.setContentCategories(getVodContentCategory(asset, extensions));

		contentCategories = getVodContentCategory(asset, extensions);

		// Start: Added for 4.2 New Object
		content.setExtendedContentAttribute(getExtendedContentAttributes(asset, channel, contentXML));
		content.setContentLinking(getContentLinking(asset));

		// start AVS - 13658
		if (config.getConstants().getValue(CacheConstants.CONCURRENT_STREAM_IN_PANIC).equalsIgnoreCase("N")) {
			if (asset.getStreamPolicies() != null) {

				if (asset.getStreamPolicies().getPolicyId() != null
						&& !asset.getStreamPolicies().getPolicyId().isEmpty()) {

					Map<String, String> policyMap = policyCache.getPolicies();
					// AVS-20827 start
					if (policyMap == null || policyMap.isEmpty()) {
						log.logMessage("Calling concurrent stream to reload policyMap");
						policyMap = policyCache.loadPolicies(config);
					}
					
					if(policyMap==null) {
						throw new ApplicationException(MessageKeys.ERROR_MS_ACN_921_CONCURRENT_STREAM_NOT_AVAILABLE);
					}
					// AVS-20827 stop
					for (String policyId : asset.getStreamPolicies().getPolicyId()) {

						if (policyMap.get(policyId) == null) {
							throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
									new NestedParameters("Policy does not exists or not allowed"));

						}

					}
				}
			}
		} else {
			if (asset.getStreamPolicies() != null) {
				if (asset.getStreamPolicies().getPolicyId() != null
						&& !asset.getStreamPolicies().getPolicyId().isEmpty()) {
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters("Policy does not allowed due to concurrentStream is in panic"));

				}
			}
		}
		// end AVS - 13658
		Set<BlacklistDeviceEntity> blacklistDeviceEntities = null;

		// Added for 4.3 device based packaging
		if (asset.getBlackListDeviceTypes() != null && asset.getBlackListDeviceTypes().getDeviceType() != null
				&& asset.getBlackListDeviceTypes().getDeviceType().size() > 0) {
			blacklistDeviceEntities = getBlacklistDeviceContent(asset);
		}

		if (content.getContentLinking() != null && !content.getContentLinking().isEmpty()) {
			boolean defaultTrailor = false;
			boolean defaultPoster = false;
			Integer defaultTrailorId = -1;
			Integer defaultPosterId = -1;

			for (ContentLinkingEntity linking : content.getContentLinking()) {

				if (!defaultTrailor && "trailer".equalsIgnoreCase(linking.getChildSubtype())
						&& "Y".equalsIgnoreCase(linking.getIsChildDefault())) {
					defaultTrailor = true;
					defaultTrailorId = linking.getId().getChildContentId();
				}
				if (!defaultPoster && "poster".equalsIgnoreCase(linking.getChildSubtype())
						&& "Y".equalsIgnoreCase(linking.getIsChildDefault())) {
					defaultPoster = true;
					defaultPosterId = linking.getId().getChildContentId();
				}
			}
			boolean equalTrailorContentPlatform = false;
			boolean equalPosterContentPlatform = false;

			boolean isBundleOrGroupOfBundle = false;
			if (content.getExtendedContentAttribute() != null
					&& content.getExtendedContentAttribute().getObjectType() != null) {
				isBundleOrGroupOfBundle = commonDataType
						.contains(content.getExtendedContentAttribute().getObjectType().toUpperCase());
			}
			if (defaultTrailorId != -1) {
				ContentEntity trailorContent = null;
				trailorContent = getContentDetailByContentId(defaultTrailorId);

				if (trailorContent == null) {
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters("Content Id " + defaultTrailorId + " not Present"));

				}
				equalTrailorContentPlatform = validateContentPlatform(
						getContentPlatformVideoType(new ArrayList<>(contentPlatforms), isBundleOrGroupOfBundle, config),
						getContentPlatformVideoType(trailorContent.getContentPlatforms(), isBundleOrGroupOfBundle,
								config));

				if (!equalTrailorContentPlatform && defaultTrailor) {
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters("Platform of Trailer content should match with Platform of Content"));
				}
				updateTrailerAndPosterUrl(contentPlatforms, trailorContent.getContentPlatforms(), null,
						isBundleOrGroupOfBundle);
			}

			if (defaultPosterId != -1) {
				ContentEntity posterContent = getContentDetailByContentId(defaultPosterId);

				if (posterContent == null) {
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters("Content Id " + defaultPosterId + " not Present"));

				}

				equalPosterContentPlatform = validateContentPlatform(
						getContentPlatformVideoType(new ArrayList<>(contentPlatforms), isBundleOrGroupOfBundle, config),
						getContentPlatformVideoType(posterContent.getContentPlatforms(), isBundleOrGroupOfBundle,
								config));

				if (!equalPosterContentPlatform && defaultPoster) {
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters("Platform of Poster content should match with Platform of Content"));

				}
				updateTrailerAndPosterUrl(contentPlatforms, null, posterContent.getContentPlatforms(),
						isBundleOrGroupOfBundle);
			}

		}
		// End: Added for 4.2 New Object
		/*
		 * MAPPING COMPOSED OBJECTS - END
		 */
		// AssetToWrite assetToWrite = new AssetToWrite();
		/* 4.3 multi language metadata */
		Set<MultiLanguageContentEntity> multiLanguageContents = null;
		Set<LanguageMetadataEntity> languageMetadataSet = null;
		if (asset.getMultilangMetadataList() != null && asset.getMultilangMetadataList().getMultilangMetadata() != null
				&& asset.getMultilangMetadataList().getMultilangMetadata().size() != 0) {
			Set<MultiLanguageContentEntity> multilangMetadataSet = new HashSet<MultiLanguageContentEntity>();
			Set<LanguageMetadataEntity> langMetadataSet = new HashSet<LanguageMetadataEntity>();

			for (int i = 0; i < asset.getMultilangMetadataList().getMultilangMetadata().size(); i++) {
				MultiLanguageContentEntity multilangMetadata = new MultiLanguageContentEntity();
				com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.MultilangMetadata metaDataMulti = asset
						.getMultilangMetadataList().getMultilangMetadata().get(i);

				if (metaDataMulti.getLangCode() != null) {
					MultiLanguageContentEntityPK contentIdLanguageIdPK = new MultiLanguageContentEntityPK();
					contentIdLanguageIdPK.setLanguageCode(metaDataMulti.getLangCode());
					contentIdLanguageIdPK.setContentId(asset.getContentId().intValue());
					multilangMetadata.setId(contentIdLanguageIdPK);
				}
				if (metaDataMulti.getCountryList() != null) {
					if (metaDataMulti.getCountryList().getCountry() != null) {
						multilangMetadata.setCountry(
								stringWithPipeComma(metaDataMulti.getCountryList().getCountry(), COMMA_STRING));}
				}
				if (metaDataMulti.getEpisodeTitle() != null && !metaDataMulti.getEpisodeTitle().equals("")) {
					multilangMetadata.setEpisodeTitle(metaDataMulti.getEpisodeTitle());
				}
				if (metaDataMulti.getGenreList() != null) {
					if (metaDataMulti.getGenreList().getGenreItem() != null) {
						multilangMetadata.setGenre(
								stringWithPipeComma(metaDataMulti.getGenreList().getGenreItem(), COMMA_STRING));
					}
				}
				if (metaDataMulti.getLongDescription() != null && !metaDataMulti.getLongDescription().equals("")) {
					multilangMetadata.setLongDescription(metaDataMulti.getLongDescription());
				}

				if (metaDataMulti.getSearchKeywordList() != null) {
					if (metaDataMulti.getSearchKeywordList().getKeyword() != null) {
						multilangMetadata.setSearchKeywords(
								stringWithPipeComma(metaDataMulti.getSearchKeywordList().getKeyword(), PIPE_STRING)
										.replace(",", "|"));
					}
				}
				if (metaDataMulti.getShortDescription() != null && !metaDataMulti.getShortDescription().equals("")) {
					multilangMetadata.setShortDescription(metaDataMulti.getShortDescription());
				}
				if (metaDataMulti.getTitle() != null && !metaDataMulti.getTitle().equals("")) {
					multilangMetadata.setTitle(metaDataMulti.getTitle());
				}
				if (metaDataMulti.getTitleBrief() != null && !metaDataMulti.getTitleBrief().equals("")) {
					multilangMetadata.setTitleBrief(metaDataMulti.getTitleBrief());
				}

				multilangMetadataSet.add(multilangMetadata);
				LanguageMetadataEntity languageMetada = new LanguageMetadataEntity();
				languageMetada.setIsActive(LANG_META_DATA_IS_ACTIVE.charAt(0));
				languageMetada.setIsDefault(LANG_META_DATA_IS_DEFAULT.charAt(0));
				if (metaDataMulti.getLangCode() != null && !metaDataMulti.getLangCode().equals("")) {

					if (validateLangCode(metaDataMulti.getLangCode())) {
						throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
								new NestedParameters("Language code not in ISO standards."));

					} else {
						languageMetada.setLanguageCode(metaDataMulti.getLangCode());
					}

				}
				if (metaDataMulti.getLangName() != null && !metaDataMulti.getLangName().equals("")) {
					languageMetada.setLanguageName(metaDataMulti.getLangName());
				}
				langMetadataSet.add(languageMetada);

				if (!StringUtils.isEmpty(metaDataMulti.getExtendedMetadata())
						&& !JsonUtils.isJSONValid(metaDataMulti.getExtendedMetadata())) {
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters("Invalid ExtendedMetadata json for " + content.getContentId()));

				}
			}
			// content.setMultiLanguageContentEntity(multilangMetadataSet);

			multiLanguageContents = multilangMetadataSet;
			languageMetadataSet = langMetadataSet;// TODO
			// assetToWrite.setLanguageMetada(langMetadataSet);
			// assetToWrite.setMultilangMetadata(multilangMetadataSet);

		}

		// START: Segment within Content of AVS 4.3
		listOfScenes = new LinkedHashSet<SceneEntity>(); // for holding list of all scenes
		listOfChapters = new LinkedHashSet<ChapterEntity>(); // for holding list of all chapters
		listOfMultilanguageScenes = new LinkedHashSet<MultilanguageSceneEntity>(); // for holding list of all
																					// multilanguage scenes
		listOfMultilanguageChapters = new LinkedHashSet<MultilanguageChapterEntity>(); // for holding list of all
																						// multilanguage chapters
		List<com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Scene> sceneList = null;

		Set<String> contentLanguageList = new HashSet<String>();
		Set<MultiLanguageContentEntity> multilangMetadatas = multiLanguageContents;

		// Added defaultLang for Content
		contentLanguageList.add(asset.getDefaultLang());

		if (!CollectionUtils.isEmpty(multilangMetadatas)) {
			for (MultiLanguageContentEntity multilangMetadata : multilangMetadatas) {
				contentLanguageList.add(multilangMetadata.getId().getLanguageCode().toUpperCase());
			}
		}

		SceneList scenesList = asset.getSceneList();
		if (scenesList != null) {
			sceneList = scenesList.getScene();
		}

		if (sceneList != null) {
			// iterating <SceneList>
			for (com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Scene sceneObj : sceneList) {

				if (!StringUtils.isEmpty(sceneObj.getExtendedMetadata())
						&& !JsonUtils.isJSONValid(sceneObj.getExtendedMetadata())) {
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters("Invalid ExtendedMetadata json for " + content.getContentId()));

				}

				Set<String> sceneLanguageList = new LinkedHashSet<String>(); // for holding all languages supported by
																				// this scene
				sceneLanguageList.add(sceneObj.getMetadataLanguage());

				List<SceneLanguage> sceneLanguages = null;
				MultiLanguage multilanguageForScene = sceneObj.getMultiLanguage();
				if (multilanguageForScene != null) {
					sceneLanguages = multilanguageForScene.getSceneLanguage();
					for (SceneLanguage multilanguageScene : sceneLanguages) {

						if (!StringUtils.isEmpty(multilanguageScene.getExtendedMetadata())
								&& !JsonUtils.isJSONValid(multilanguageScene.getExtendedMetadata())) {
							throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
									new NestedParameters(
											"Invalid ExtendedMetadata json for " + content.getContentId()));

						}
						sceneLanguageList.add(multilanguageScene.getMetadataLanguage());
					}
				}

				if (contentLanguageList.contains(sceneObj.getMetadataLanguage().toUpperCase())) {
					// for each <Scene> in <SceneList>
					SceneEntity scene = new SceneEntity();
					SceneEntityPK id = new SceneEntityPK();
					id.setSceneId(sceneObj.getSceneId());
					id.setContentId(content.getContentId().longValue());
					scene.setCompId(id);
					scene.setAction(sceneObj.getAction());
					scene.setActors(sceneObj.getActors());
					scene.setAdTag(sceneObj.getAdTag());
					scene.setAudio(sceneObj.getAudio());
					scene.setBriefTitle(sceneObj.getBriefTitle());
					scene.setCharacters(sceneObj.getCharacters());
					scene.setCompany(sceneObj.getCompany());
					scene.setContentTitle(content.getTitle());

					scene.setStartTime(Validator.xmlGregorianCalendar2DateTime(sceneObj.getStartTime()));
					scene.setEndTime(Validator.xmlGregorianCalendar2DateTime(sceneObj.getEndTime()));
					scene.setEvent(sceneObj.getEvent());
					scene.setExtendedLocation(sceneObj.getExtendedLocation());
					scene.setGenre(sceneObj.getGenre());
					scene.setKeywords(sceneObj.getKeywords());
					scene.setLocation(sceneObj.getLocation());
					scene.setMetadataLanguage(sceneObj.getMetadataLanguage());
					scene.setMusicians(sceneObj.getMusicians());
					scene.setOrderId(sceneObj.getOrderId());

					scene.setTitle(sceneObj.getTitle());

					EMFAttributeList sceneEmfAttributeList = sceneObj.getEMFAttributeList();
					List<EMFAttribute> emfAttributes = null;
					if (sceneEmfAttributeList != null) {
						emfAttributes = sceneEmfAttributeList.getEMFAttribute();
					}
					Set<ObjectEmfEntity> sceneObjectEmfs = null;
					Set<ObjectEmfEntity> multilanguageSceneObjectEmfs = null;

					if (emfAttributes != null) {
						sceneObjectEmfs = new LinkedHashSet<ObjectEmfEntity>();
						// iterating <EMFAttributes> in <Scene>
						for (EMFAttribute emfAttribute : emfAttributes) {
							// for each <EMFAttribute>
							// check if emf attribute language is supported by scene or not. If not
							// supported, that attribute will not be published.
							if (sceneLanguageList.contains(emfAttribute.getMetadataLanguage())) {
								if (emfAttribute.getName() != null && !emfAttribute.getName().isEmpty()) {
									ObjectEmfEntity attributes = new ObjectEmfEntity();
									ObjectEmfEntityPK objectEmfPK = new ObjectEmfEntityPK();
									objectEmfPK.setEmfId((int) emfAttribute.getEMFId());
									objectEmfPK.setLanguageCode(emfAttribute.getMetadataLanguage());
									objectEmfPK.setObjId(scene.getCompId().getSceneId().intValue());
									objectEmfPK.setObjType(3);
									attributes.setId(objectEmfPK);
									attributes.setEmfValue(emfAttribute.getValue());
									attributes.setEmfName(emfAttribute.getName());
									sceneObjectEmfs.add(attributes);
								} else {
									throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
											new NestedParameters("Name of the EMF Attribute " + emfAttribute.getEMFId()
													+ "for Scene " + scene.getCompId().getSceneId()
													+ "is null or empty."));

								}
							} else {
								throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
										new NestedParameters("The EMFAttribute cannot be published as language"
												+ emfAttribute.getMetadataLanguage() + " is not supported by the scene "
												+ sceneObj.getSceneId()));

							}
						}
					}

					scene.setEmfAttributes(sceneObjectEmfs);

					List<com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Scene.PlatformList.Platform> platforms = sceneObj
							.getPlatformList().getPlatform();
					scenePlatforms = new LinkedHashSet<ObjectPlatformEntity>();

					// iterating <PlatformList> in <Scene>
					for (com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Scene.PlatformList.Platform platformObj : platforms) {
						ObjectPlatformEntity platform = new ObjectPlatformEntity();
						platform.setObjectId(scene.getCompId().getSceneId());
						platform.setObjectType("SCENE");
						platform.setName(platformObj.getName());
						Validator.isValidUrl(platformObj.getPictureUrl(), "Scene platform pictureUrl");
						platform.setPictureUrl(platformObj.getPictureUrl());
						platform.setPictureResolution(platformObj.getPictureResolution());
						scenePlatforms.add(platform);
					}

					scene.setPlatforms(scenePlatforms);
					listOfScenes.add(scene);

					if (sceneLanguages != null) {
						// iterating <Multilanguage> in <Scene>
						for (SceneLanguage sceneLanguage : sceneLanguages) {
							if (contentLanguageList.contains(sceneLanguage.getMetadataLanguage().toUpperCase())) {
								// for each <SceneLanguage>
								MultilanguageSceneEntity multilanguageScene = new MultilanguageSceneEntity();
								MultilanguageSceneEntityPK multilanguageScenePK = new MultilanguageSceneEntityPK();
								multilanguageScenePK.setSceneId(sceneObj.getSceneId());
								multilanguageScenePK.setContentId(content.getContentId().longValue());
								multilanguageScenePK.setMetadataLanguage(sceneLanguage.getMetadataLanguage());
								multilanguageScene.setId(multilanguageScenePK);
								multilanguageScene.setAction(sceneLanguage.getAction());
								multilanguageScene.setActors(sceneLanguage.getActors());
								multilanguageScene.setAdTag(sceneLanguage.getAdTag());
								multilanguageScene.setAudio(sceneLanguage.getAudio());
								multilanguageScene.setBriefTitle(sceneLanguage.getBriefTitle());
								multilanguageScene.setCharacters(sceneLanguage.getCharacters());
								multilanguageScene.setCompany(sceneLanguage.getCompany());
								multilanguageScene.setContentTitle(content.getTitle());
								multilanguageScene
										.setEndTime(Validator.xmlGregorianCalendar2Date(sceneObj.getEndTime()));
								multilanguageScene.setEvent(sceneLanguage.getEvent());
								multilanguageScene.setExtendedLocation(sceneLanguage.getExtendedLocation());
								multilanguageScene.setGenre(sceneLanguage.getGenre());
								multilanguageScene.setKeywords(sceneLanguage.getKeywords());
								multilanguageScene.setLocation(sceneLanguage.getLocation());
								multilanguageScene.setMusicians(sceneLanguage.getMusicians());
								multilanguageScene.setOrderId(sceneLanguage.getOrderId());
								multilanguageScene
										.setStartTime(Validator.xmlGregorianCalendar2Date(sceneObj.getStartTime()));
								multilanguageScene.setTitle(sceneLanguage.getTitle());

								EMFAttributeList multilanguageSceneEmfAttributeList = sceneLanguage
										.getEMFAttributeList();
								List<EMFAttribute> multilanguageEmfAttributes = null;
								if (multilanguageSceneEmfAttributeList != null) {
									multilanguageEmfAttributes = multilanguageSceneEmfAttributeList.getEMFAttribute();
								}

								if (multilanguageEmfAttributes != null) {
									multilanguageSceneObjectEmfs = new LinkedHashSet<ObjectEmfEntity>();
									// iterating <EMFAttributes> in <SceneLanguage>
									for (EMFAttribute emfAttribute : multilanguageEmfAttributes) {
										// check if emf attribute language is supported by scene or not. If not
										// supported, that attribute will not be published.
										if (sceneLanguageList.contains(emfAttribute.getMetadataLanguage())) {
											if (emfAttribute.getName() != null && !emfAttribute.getName().isEmpty()) {
												ObjectEmfEntity attributes = new ObjectEmfEntity();
												ObjectEmfEntityPK objectEmfPK = new ObjectEmfEntityPK();
												objectEmfPK.setEmfId((int) emfAttribute.getEMFId());
												objectEmfPK.setLanguageCode(emfAttribute.getMetadataLanguage());
												objectEmfPK.setObjId(scene.getCompId().getSceneId().intValue());
												objectEmfPK.setObjType(3);
												attributes.setId(objectEmfPK);
												attributes.setEmfValue(emfAttribute.getValue());
												attributes.setEmfName(emfAttribute.getName());
												multilanguageSceneObjectEmfs.add(attributes);
											} else {
												throw new ApplicationException(
														MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
														new NestedParameters("Name of the EMF Attribute"
																+ emfAttribute.getEMFId() + " for Multilanguage Scene"
																+ scene.getCompId().getSceneId()
																+ " is null or empty."));

											}
										} else {
											throw new ApplicationException(
													MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
													new NestedParameters(
															"The EMFAttribute cannot be published as language "
																	+ emfAttribute.getMetadataLanguage()
																	+ " is not supported bythe scene "
																	+ sceneObj.getSceneId()));

										}
									}
								}

								multilanguageScene.setEmfAttributes(multilanguageSceneObjectEmfs);
								listOfMultilanguageScenes.add(multilanguageScene);
							} else {
								if (content.getContentId() == null) {
									

									throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
											new NestedParameters(
													"Content for externalContentId " + content.getExternalContentId()
															+ " does not support the metadata language "
															+ sceneLanguage.getMetadataLanguage()
															+ " for the Multilanguage Scene " + sceneObj.getSceneId()));

								} else {

									throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
											new NestedParameters("Content " + content.getContentId()
													+ " does not support the metadata language "
													+ sceneLanguage.getMetadataLanguage()
													+ " for the Multilanguage Scene " + sceneObj.getSceneId()));
								}
							}
						}
					}
				} else {

					if (content.getContentId() == null) {

						throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
								new NestedParameters("Content for externalContentId " + content.getExternalContentId()
										+ " does not support the metadata language " + sceneObj.getMetadataLanguage()
										+ " for the Scene " + sceneObj.getSceneId()));

					} else {

						throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
								new NestedParameters("Content " + content.getContentId()
										+ " does not support the metadata language " + sceneObj.getMetadataLanguage()
										+ " for the Scene " + sceneObj.getSceneId()));
					}
				}

			}
		}

		List<Chapter> chapterList = null;

		ChapterList chapterLists = asset.getChapterList();

		if (chapterLists != null) {
			chapterList = chapterLists.getChapter();
		}

		if (chapterList != null) {
			// iterating <ChapterList> tag
			for (com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Chapter chapterObj : chapterList) {

				List<ChapterLanguage> chapterLanguages = null;
				com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Chapter.MultiLanguage multilanguageForChapter = chapterObj
						.getMultiLanguage();
				if (multilanguageForChapter != null) {
					chapterLanguages = multilanguageForChapter.getChapterLanguage();
				}

				if (contentLanguageList.contains(chapterObj.getMetadataLanguage())) {
					// for each <Chapter> in <ChapterList>
					ChapterEntity chapter = new ChapterEntity();
					ChapterEntityPK chapterPK = new ChapterEntityPK();
					chapter.setBriefTitle(chapterObj.getBriefTitle());
					chapterPK.setChapterId(chapterObj.getChapterId());
					chapterPK.setContentId(content.getContentId().longValue());
					chapter.setCompId(chapterPK);

					chapter.setStartTime(Validator.xmlGregorianCalendar2DateTime(chapterObj.getStartTime()));
					chapter.setEndTime(Validator.xmlGregorianCalendar2DateTime(chapterObj.getEndTime()));
					chapter.setMetadataLanguage(chapterObj.getMetadataLanguage());
					chapter.setOrderId(chapterObj.getOrderId());

					List<com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Chapter.PlatformList.Platform> platforms = chapterObj
							.getPlatformList().getPlatform();
					chapterPlatforms = new LinkedHashSet<ObjectPlatformEntity>();

					// iterating <PlatformList> in <Chapter>
					for (com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Chapter.PlatformList.Platform platformObj : platforms) {
						ObjectPlatformEntity platform = new ObjectPlatformEntity();
						platform.setObjectId(chapter.getCompId().getChapterId());
						platform.setObjectType("CHAPTER");
						platform.setName(platformObj.getName());
						Validator.isValidUrl(platformObj.getPictureUrl(), "Chapter platform pictureUrl");
						platform.setPictureUrl(platformObj.getPictureUrl());
						platform.setPictureResolution(platformObj.getPictureResolution());
						chapterPlatforms.add(platform);
					}

					chapter.setPlatforms(chapterPlatforms);
					listOfChapters.add(chapter);

					if (chapterLanguages != null) {
						// iterating <Multilanguage> in <Chapter>
						for (ChapterLanguage chapterLanguage : chapterLanguages) {
							if (contentLanguageList.contains(chapterLanguage.getMetadataLanguage())) {
								// for each <ChapterLanguage>
								MultilanguageChapterEntity multilanguageChapter = new MultilanguageChapterEntity();
								MultilanguageChapterEntityPK multilanguageChapterPK = new MultilanguageChapterEntityPK();
								multilanguageChapterPK.setChapterId(chapterObj.getChapterId());
								multilanguageChapterPK.setContentId(content.getContentId().longValue());
								multilanguageChapterPK.setMetadataLanguage(chapterLanguage.getMetadataLanguage());
								multilanguageChapter.setId(multilanguageChapterPK);
								multilanguageChapter.setBriefTitle(chapterLanguage.getBriefTitle());
								multilanguageChapter
										.setEndTime(Validator.xmlGregorianCalendar2Date(chapterObj.getEndTime()));
								multilanguageChapter.setOrderId(chapterLanguage.getOrderId());
								multilanguageChapter
										.setStartTime(Validator.xmlGregorianCalendar2Date(chapterObj.getStartTime()));
								listOfMultilanguageChapters.add(multilanguageChapter);
							} else {

								if (content.getContentId() == null) {
									throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
											new NestedParameters("Content for ExternalContentId"
													+ content.getExternalContentId()
													+ " does not support the metadata language "
													+ chapterLanguage.getMetadataLanguage()
													+ " for the Multilanguage Chapter " + chapterObj.getChapterId()));

								} else {
									throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
											new NestedParameters("Content " + content.getContentId()
													+ " does not support the metadata language "
													+ chapterLanguage.getMetadataLanguage()
													+ " for the Multilanguage Chapter " + chapterObj.getChapterId()));
								}
							}
						}
					}
				} else {
					if (content.getContentId() == null) {
						throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
								new NestedParameters("Content for ExternalContentId" + content.getExternalContentId()
										+ " does not support the metadata language " + chapterObj.getMetadataLanguage()
										+ " for the Chapter " + chapterObj.getChapterId()));

					} else {
						throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
								new NestedParameters("Content " + content.getContentId()
										+ " does not support the metadata language " + chapterObj.getMetadataLanguage()
										+ " for the Chapter " + chapterObj.getChapterId()));
					}
				}
			}
		}

		// START: Operator defined Attributes of AVS 4.3
		Map<ObjectEmfEntityPK, String> contentEmfMap = new LinkedHashMap<ObjectEmfEntityPK, String>();
		Set<ObjectEmfEntity> contentObjectEmfs = new LinkedHashSet<ObjectEmfEntity>();
		List<EMFAttribute> emfAttributes = null;
		EMFAttributeList emfAttributeList = asset.getEMFAttributeList();
		if (emfAttributeList != null) {
			emfAttributes = emfAttributeList.getEMFAttribute();
		}

		if (emfAttributes != null) {
			for (EMFAttribute emfAttribute : emfAttributes) {
				if (contentLanguageList.contains(emfAttribute.getMetadataLanguage())) {
					if (emfAttribute.getName() != null && !emfAttribute.getName().isEmpty()) {
						ObjectEmfEntityPK objectEmfPK = new ObjectEmfEntityPK();
						objectEmfPK.setEmfId((int) emfAttribute.getEMFId());
						objectEmfPK.setLanguageCode(emfAttribute.getMetadataLanguage());
						objectEmfPK.setObjId(content.getContentId());
						objectEmfPK.setObjType(1);

						// seperator(#:&) is used for appending EMF name and value
						contentEmfMap.put(objectEmfPK, emfAttribute.getName() + "#:&" + emfAttribute.getValue());
					} else {
						if (content.getContentId() == null) {
							throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
									new NestedParameters("Name of the EMF Attribute " + emfAttribute.getEMFId()
											+ " for ExternalContentId " + content.getExternalContentId()
											+ " is null or empty."));
						} else {
							throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
									new NestedParameters("Name of the EMF Attribute " + emfAttribute.getEMFId()
											+ " for Content " + content.getContentId() + " is null or empty."));
						}
					}
				} else {
					if (content.getContentId() == null) {
						throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
								new NestedParameters("The EMFAttribute cannot be published as language "
										+ emfAttribute.getMetadataLanguage()
										+ " is not supported by the externalContentId "
										+ content.getExternalContentId()));
					} else {

						throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
								new NestedParameters("The EMFAttribute cannot be published as language "
										+ emfAttribute.getMetadataLanguage() + " is not supported by the content "
										+ content.getContentId()));
					}
				}
			}
		}

		// Set<ObjectEmfPK> ids = contentEmfMap.keySet();

		// sonar fixes for Performance - Inefficient use of keySet iterator instead of
		// entrySet iterator
		for (Map.Entry<ObjectEmfEntityPK, String> entry : contentEmfMap.entrySet()) {
			ObjectEmfEntity oe = new ObjectEmfEntity();
			ObjectEmfEntityPK objectEmfPK = entry.getKey();
			oe.setId(objectEmfPK);
			String[] nameValue = contentEmfMap.get(objectEmfPK).split("#:&");
			oe.setEmfName(nameValue[0]);
			oe.setEmfValue(nameValue[1]);
			contentObjectEmfs.add(oe);
		}
		/*
		 * for (ObjectEmfPK objectEmfPK : ids) { ObjectEmf oe = new ObjectEmf();
		 * oe.setId(objectEmfPK); String[] nameValue =
		 * contentEmfMap.get(objectEmfPK).split("#:&"); oe.setEmfName(nameValue[0]);
		 * oe.setValue(nameValue[1]); contentObjectEmfs.add(oe); }
		 */

		objectEmfAttributes = contentObjectEmfs;
		// END: Operator defined Attributes of AVS 4.3

		// if solutionOfferListExist
		if (extensions != null) {
			if (asset.getExtensions().getSolutionOfferList() != null) {
				// AVS-12973 start // check commercial package level property in property cache
				solutionOfferList = asset.getExtensions().getSolutionOfferList();
				if (!solutionOfferList.getSolutionOffer().isEmpty()) {
					for (SolutionOffer solutionOffer : solutionOfferList.getSolutionOffer()) {
						if (solutionOffer.getRentalPeriodList() != null
								&& !solutionOffer.getRentalPeriodList().getRentalPeriod().isEmpty()) {
							for (RentalPeriod rentalPeriod : solutionOffer.getRentalPeriodList().getRentalPeriod()) {
								if (rentalPeriod.getAdditionalSolOfferList() != null
										&& !rentalPeriod.getAdditionalSolOfferList().getAddSolOffer().isEmpty()) {
									Map<String, String> propertiesCache = propertyCache.getProperties();
									for (AddSolOffer addSolOffer : rentalPeriod.getAdditionalSolOfferList()
											.getAddSolOffer()) {
										String propertyName = addSolOffer.getPropertyName();
										if (propertyName != null && propertiesCache.get(propertyName) == null) {
											throw new ApplicationException(
													MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
													new NestedParameters("Property does not exists"));

										}
									}
								}
							}
						}
					}
				}
				// AVS-12973 stop
				// assetToWrite.setSolutionOfferList(asset.getExtensions().getSolutionOfferList());
			}
		}

		if (!StringUtils.isEmpty(asset.getExtendedMetadata()) && !JsonUtils.isJSONValid(asset.getExtendedMetadata())) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters("Invalid ExtendedMetadata json for " + content.getContentId()));

		}

		// AVS-12973 stop
		if (!Utilities.isEmptyCollection(trcEnablerRequestList)) {
			trcEnablerRequest = trcEnablerRequestList;
		}

		Set<ContentPlatformEntity> contentPlatformSet = writeContent(content, contentPlatforms, relContentExtRats,
				contentCategories, listOfScenes, listOfChapters, multiLanguageContents, listOfMultilanguageScenes,
				listOfMultilanguageChapters, scenePlatforms, chapterPlatforms, objectEmfAttributes,
				blacklistDeviceEntities, languageMetadataSet, config);

		if ("N".equalsIgnoreCase(config.getConstants().getValue(CacheConstants.SDP_IN_PANIC))) {
			try {
				sdpManager.callToSDP(solutionOfferList, trcEnablerRequest, content, contentPlatformSet, null, config);
			} catch (Exception e) {
				log.logError(e);
				throw e;
			}
		} else {
			log.logMessage("Skiping all SDP calls");
		}

		return content.getContentId();

	}

	private ContentEntity getContentDetailByContentId(Integer contentId) {
		log.logMessage("In getContentDetailByContentId Start ");
		// String query = "from com.accenture.avs.ci.domain.Content content where
		// content.contentId=?";
		ContentEntity content = null;
		if (contentRepository.existsById(contentId)) {
			content = contentRepository.findById(contentId).get();
		}

		log.logMessage("In getContentDetailByContentId End ");
		return content;
	}

	private boolean validateContentPlatform(Set<Object> contentPlatformVideoType,
			Set<Object> defaultContentPlatformVideoType) {

		boolean flag = false;
		if (contentPlatformVideoType != null && defaultContentPlatformVideoType != null
				&& !contentPlatformVideoType.isEmpty() && !defaultContentPlatformVideoType.isEmpty()) {
			contentPlatformVideoType.removeAll(defaultContentPlatformVideoType);
			flag = contentPlatformVideoType.isEmpty();
		}

		return flag;
	}

	private void updateTrailerAndPosterUrl(Set<ContentPlatformEntity> contentContentPlatform,
			List<ContentPlatformEntity> trailorContentPlatforms, List<ContentPlatformEntity> posterContentPlatforms,
			boolean isBundleOrGroupOfBundle) {

		trailorContentPlatforms = trailorContentPlatforms == null ? new ArrayList<ContentPlatformEntity>()
				: trailorContentPlatforms;
		posterContentPlatforms = posterContentPlatforms == null ? new ArrayList<ContentPlatformEntity>()
				: posterContentPlatforms;

		for (ContentPlatformEntity contentPlatform : contentContentPlatform) {
			if ('Y' == contentPlatform.getIsPublished()) {
				for (ContentPlatformEntity trailorContentPlatform : trailorContentPlatforms) {
					if ('Y' == trailorContentPlatform.getIsPublished()) {
						if (isBundleOrGroupOfBundle) {
							// for bundle/group of bundles, skip videoType check
							if (contentPlatform.getPlatform().equals(trailorContentPlatform.getPlatform())) {
								contentPlatform.setTrailerUrl(trailorContentPlatform.getVideoUrl());
							}
						} else {
							if (contentPlatform.getPlatform().equals(trailorContentPlatform.getPlatform())
									&& contentPlatform.getVideoName().equals(trailorContentPlatform.getVideoName())) {
								contentPlatform.setTrailerUrl(trailorContentPlatform.getVideoUrl());
							}
						}
					}
				}
				for (ContentPlatformEntity posterContentPlatform : posterContentPlatforms) {
					if ('Y' == posterContentPlatform.getIsPublished()) {
						if (isBundleOrGroupOfBundle) {
							// for bundle/group of bundles, skip videoType check
							if (contentPlatform.getPlatform().equals(posterContentPlatform.getPlatform())) {
								contentPlatform.setPictureUrl(posterContentPlatform.getPictureUrl());
							}
						} else {
							if (contentPlatform.getPlatform().equals(posterContentPlatform.getPlatform())
									&& contentPlatform.getVideoName().equals(posterContentPlatform.getVideoName())) {
								contentPlatform.setPictureUrl(posterContentPlatform.getPictureUrl());
							}
						}
					}
				}
			}
		}
	}

	private Set<Object> getContentPlatformVideoType(List<ContentPlatformEntity> contentPlatforms,
			boolean isBundleOrGroupOfBundle, Configuration config) throws ConfigurationException, ApplicationException {
		log.logMessage("In getContentPlatformVideoType ");
		Set<Object> contentPlatformVideoTypeSet = new HashSet<Object>();
		for (ContentPlatformEntity platform : contentPlatforms) {
			if ('Y' == platform.getIsPublished()) {
				if (isBundleOrGroupOfBundle) {
					contentPlatformVideoTypeSet.add(platform.getPlatform());
				} else {
					ContentPlatformVideoType contentPlatformVideoType = new ContentPlatformVideoType();
					// Platform validation AVS 6634
					// if("Y".equalsIgnoreCase(platformMap.get(platform.getPlatform()))){
					if (config.getPlatforms().isValidKey(platform.getPlatform())) {
						contentPlatformVideoType.setPlatform(platform.getPlatform());
					} else {
						throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
								new NestedParameters("Platform " + platform.getPlatform()
										+ " is not valid because does not exists or inactive"));
					}
					// Videotype validation AVS 6634
					if (config.getVideoTypeCache().isValidKey(platform.getVideoName())) {
						contentPlatformVideoType.setVideoType(platform.getVideoName());
					} else {
						throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
								new NestedParameters("VideoType " + platform.getVideoName()
										+ " is not valid because does not exists or inactive"));
					}
					contentPlatformVideoTypeSet.add(contentPlatformVideoType);
				}
			}
		}
		return contentPlatformVideoTypeSet;
	}

	// Start: Added for 4.2 New Object

	/**
	 * @param channel
	 * @since 4.2 Create Extended Attribute Object.
	 * 
	 * @param Asset
	 *            asset
	 * @return ExtendedContentAttributes
	 * @throws UnsupportedEncodingException
	 * @throws Exception
	 * 
	 */
	private ExtendedContentAttributeEntity getExtendedContentAttributes(Asset asset, ChannelEntity channel,
			String contentXML) throws UnsupportedEncodingException {
		ExtendedContentAttributeEntity extendedContentAttributes = new ExtendedContentAttributeEntity();
		extendedContentAttributes.setAdvTags(asset.getAdvTags());

		if (asset.getIsSkipJumpEnabled() != null && asset.getIsSkipJumpEnabled().value() != null) {
			extendedContentAttributes.setIsSkipJumpEnabled(asset.getIsSkipJumpEnabled().value());
		} else {
			extendedContentAttributes.setIsSkipJumpEnabled("N");
		}

		if (asset.getIsTrickPlayEnabled() != null && asset.getIsTrickPlayEnabled().value() != null) {
			extendedContentAttributes.setIsTrickPlayEnabled(asset.getIsTrickPlayEnabled().value());
		} else {
			extendedContentAttributes.setIsTrickPlayEnabled("N");
		}

		if (asset.getIsDisAllowedAdv() != null && asset.getIsDisAllowedAdv().value() != null) {
			extendedContentAttributes.setDisAllowedAdv(asset.getIsDisAllowedAdv().value());
		} else {
			extendedContentAttributes.setDisAllowedAdv("N");
		}

		extendedContentAttributes.setContentId(asset.getContentId().intValue());
		if (asset.getIsParentObject() == null) {
			extendedContentAttributes.setIsParent("Y");
		} else {
			extendedContentAttributes.setIsParent(asset.getIsParentObject().value());
		}
		if (asset.getLatest() != null) {
			extendedContentAttributes.setLatest(asset.getLatest().value());
		}
		extendedContentAttributes.setObjectSubtype(asset.getObjectSubtype());
		extendedContentAttributes.setObjectType(asset.getObjectType());
		if (asset.getOnAir() != null) {
			extendedContentAttributes.setOnAir(asset.getOnAir().value());
		}
		if (asset.getPopularEpisode() != null) {
			extendedContentAttributes.setPopularEpisode(asset.getPopularEpisode().value());
		}
		extendedContentAttributes.setTitleBrief(asset.getTitleBrief());

		if (asset.getOriginalAirDate() != null) {
			extendedContentAttributes
					.setOriginalAirDate(Validator.xmlGregorianCalendar2Date(asset.getOriginalAirDate()));
		}
		if (asset.getSearchKeywordList() != null && asset.getSearchKeywordList().getKeyword() != null
				&& !asset.getSearchKeywordList().getKeyword().isEmpty()) {

			extendedContentAttributes
					.setSerchKeywords(Validator.list2String(asset.getSearchKeywordList().getKeyword(), "|", 1000));
		}

		if (asset.getDefaultLang() != null) {
			extendedContentAttributes.setDefaultLang(asset.getDefaultLang());
		}

		if (asset.getIsSurroundSound() != null) {
			extendedContentAttributes.setIsSurroundSound(asset.getIsSurroundSound().value());
		}
		extendedContentAttributes.setItemURL(asset.getItemUrl());

		if (asset.getProgramReferenceName() != null) {
			extendedContentAttributes.setProgramReferenceName(asset.getProgramReferenceName());
		}

		if (channel != null) {
			extendedContentAttributes.setBroadcastChannelId(channel.getChannelId());
		}

		if (asset.getProgramCategory() != null) {
			extendedContentAttributes.setProgramCategory(asset.getProgramCategory());
		}
		/*
		 * If the Job is called from web then file path will be set in Job service. If
		 * the Job is called from batch then file path taken from fileNameVod in the
		 * JobContext.
		 * 
		 * 
		 */

		if (!StringUtils.isEmpty(contentXML)) {
			// extendedContentAttributes.setSourceFile(xmlString.getBytes());
			extendedContentAttributes.setSourceFile(contentXML.getBytes("UTF-8"));
		}
		return extendedContentAttributes;
	}

	/**
	 * @since 4.2 Create Content Linking Object.
	 * 
	 * @param Asset
	 *            asset
	 * @return Set<ContentLinking>
	 * 
	 */
	private Set<ContentLinkingEntity> getContentLinking(Asset asset) {
		Set<ContentLinkingEntity> contentLinkings = null;
		ContentLinkingEntity contentLinking = null;
		ContentLinkingEntityPK linkingPK = null;
		if (asset.getContentLinking() != null && !asset.getContentLinking().getContentLinkingId().isEmpty()) {
			contentLinkings = new LinkedHashSet<ContentLinkingEntity>(
					asset.getContentLinking().getContentLinkingId().size());
			for (com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.ContentLinkingId contentLinkingId : asset
					.getContentLinking().getContentLinkingId()) {
				contentLinking = new ContentLinkingEntity();
				contentLinking.setChildSubtype(contentLinkingId.getSubtype());
				linkingPK = new ContentLinkingEntityPK();
				linkingPK.setChildContentId(Integer.parseInt(String.valueOf(contentLinkingId.getValue())));
				linkingPK.setParentContentId(asset.getContentId().intValue());
				contentLinking.setId(linkingPK);

				if (contentLinkingId.getDefault() != null) {
					contentLinking.setIsChildDefault(contentLinkingId.getDefault().value());
				}
				if (contentLinkingId.getOrderId() != null) {
					contentLinking.setOrderId(contentLinkingId.getOrderId().intValue());
				}

				contentLinkings.add(contentLinking);
			}
		}
		return contentLinkings;
	}

	// End: Added for 4.2 New Object

	private Set<BlacklistDeviceEntity> getBlacklistDeviceContent(Asset asset) throws ApplicationException {
		Set<BlacklistDeviceEntity> blacklistDeviceContents = new HashSet<BlacklistDeviceEntity>();
		BlackListDeviceTypes blackListDeviceTypes = asset.getBlackListDeviceTypes();
		if (blackListDeviceTypes != null) {
			List<String> deviceTypeList = blackListDeviceTypes.getDeviceType();
			if (deviceTypeList != null && !deviceTypeList.isEmpty()) {

				Map<String, Long> deviceChannels = deviceChannelsCache.getDeviceChannels();
				if(deviceChannels==null) {
					throw new ApplicationException(MessageKeys.ERROR_MS_ACN_916_SDP_NOT_AVAILABLE);
				}
				for (Iterator<String> iterator = deviceTypeList.iterator(); iterator.hasNext();) {
					String deviceTypes = iterator.next();

					if (deviceChannels.containsKey(deviceTypes)) {

						BlacklistDeviceEntity blacklistDeviceContent = new BlacklistDeviceEntity();
						BlackListDeviceEntityPK blacklistDeviceContentPK = new BlackListDeviceEntityPK();
						blacklistDeviceContentPK.setDevicetypeId(deviceChannels.get(deviceTypes));
						blacklistDeviceContentPK.setContentId(asset.getContentId().intValue());
						blacklistDeviceContent.setId(blacklistDeviceContentPK);
						blacklistDeviceContents.add(blacklistDeviceContent);
					}

					else {
						log.logMessage("unknown device type " + blackListDeviceTypes.getDeviceType());
						throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
								new NestedParameters("unknown devicetype:" + deviceTypes));
					}
				}
			}

		}
		return blacklistDeviceContents;
	}

	/**
	 * Support method for retrieve channelName by the channelId.
	 * 
	 * @param channelId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private ChannelEntity getChannelName(Long channelId) {
		ChannelEntity channel = null;
		if (channelRepository.existsById(channelId.intValue())) {
			channel = channelRepository.findById(channelId.intValue()).get();
		}
		return channel;
	}

	/**
	 * Retrieve the set of Platform objects related to the Content.
	 * 
	 * @param Asset
	 *            vodElement
	 * @param Extensions
	 *            extensions
	 * @throws ApplicationException
	 * @throws ConfigurationException
	 * @throws Exception
	 * 
	 */
	private Set<ContentPlatformEntity> getVodContentPlatform(Asset vodElement, Extensions extensions,
			Configuration config) throws ApplicationException, ConfigurationException {
		Set<ContentPlatformEntity> platformContentList = new HashSet<ContentPlatformEntity>();

		if (extensions != null) {
			PlatformList platformListObj = extensions.getPlatformList();
			if (platformListObj != null) {
				List<Platform> platformList = platformListObj.getPlatform();
				for (Iterator<Platform> iterator = platformList.iterator(); iterator.hasNext();) {
					ContentPlatformEntity contentPlatform = new ContentPlatformEntity();

					Platform platform = iterator.next();

					if (!StringUtils.isEmpty(platform.getExtendedMetadata())
							&& !JsonUtils.isJSONValid(platform.getExtendedMetadata())) {
						throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
								new NestedParameters("Invalid ExtendedMetadata json for " + vodElement.getContentId()));
					}

					contentPlatform.setContentId(vodElement.getContentId().intValue());

					// fix for defect AVS-6885
					if (null != platform.getName() && config.getPlatforms().isValidKey(platform.getName()))
						contentPlatform.setPlatform(platform.getName());
					else {

						throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
								new NestedParameters("Platform"));

					}
					if (platform.getVideoType() != null && !platform.getVideoType().equals("")) {
						if (config.getVideoTypeCache().isValidKey(platform.getVideoType())) {
							contentPlatform.setVideoName(platform.getVideoType());
						} else {
							throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
									new NestedParameters("VideoType"));
						}
					} else {
						contentPlatform.setVideoName("NA");
					}
					
					/*AVS-48640 START*/
					if(platform.getTrailerUrl() != null) {
						Validator.isValidUrl(platform.getTrailerUrl().getValue(), "trailerUrl");
						contentPlatform.setTrailerUrl(platform.getTrailerUrl().getValue());
					}
					/*AVS-48640 END*/
					Validator.isValidUrl(platform.getVideoUrl().getValue(), "videoUrl");
					contentPlatform.setVideoUrl(platform.getVideoUrl().getValue());
					contentPlatform.setIsPublished(DEFAULT_IS_PUSLISHED.charAt(0));// always Y when a platform exists in
																					// xml
					Validator.isValidUrl(platform.getPosterAsset().getValue(), "pictureUrl");
					contentPlatform.setPictureUrl(platform.getPosterAsset().getValue());
					contentPlatform.setDrmInfo(platform.getVideoUrl().getDecryptKey());
					if (platform.getDownload() != null) {
						if (platform.getDownload().getMaxDaysAvailable() != null) {
							contentPlatform.setDwnMaxDays(platform.getDownload().getMaxDaysAvailable().intValue());
						}
						if (platform.getDownload().getNumDWNAvailable() != null) {
							contentPlatform.setDwnNumber(platform.getDownload().getNumDWNAvailable().intValue());
						}

						if (platform.getDownload().getNumHoursFromFirstPlay() != null) {
							contentPlatform
									.setDwnNumberHours(platform.getDownload().getNumHoursFromFirstPlay().intValue());
						}
					}

					if (platform.getStreamingType() != null) {
						contentPlatform.setStreamingType(platform.getStreamingType());
					}

					if (platform.getStartDateTime() == null) {
						contentPlatform
						.setContractStart(Validator.xmlGregorianCalendar2Date(vodElement.getStartDateTime()));
					} else {
						contentPlatform
								.setContractStart(Validator.xmlGregorianCalendar2Date(platform.getStartDateTime()));
						}

					if (platform.getEndDateTime() == null) {
						contentPlatform
								.setContractEnd(Validator.xmlGregorianCalendar2Date(vodElement.getEndDateTime()));
					} else {
						contentPlatform.setContractEnd(Validator.xmlGregorianCalendar2Date(platform.getEndDateTime()));
					
					}

					if (vodElement.getLanguage() == null && vodElement.getLanguages() == null
							&& platform.getLanguages() == null) {
						throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
								new NestedParameters(
										"Both 'languages' and 'language' element are missed in vod xml file."));
					}

					// Manage releted table Audiolang, Subtitles
					contentPlatform.setAudiolangs(getVodAudiolang(platform.getLanguages(), vodElement.getContentId()));
					contentPlatform.setSubtitles(getVodSubtitles(platform.getSubtitles(), vodElement.getContentId(),
							contentPlatform.getCpId()));
					contentPlatform.setRelPlatformTechnicals(
							getVodRelPlatformTechnical(platform.getPackageList(), vodElement.getContentId()));

					platformContentList.add(contentPlatform);
				}
			}
		}
		return platformContentList;
	}

	/**
	 * Retrieve the set of RelContentExtRat objects related to the Content.
	 * 
	 * @param Asset
	 *            vodElement
	 * @param Extensions
	 *            extensions
	 * @throws ConfigurationException
	 * 
	 */
	private Set<RelContentExtRatEntity> getVodRelContentExtRat(Asset vodElement, Extensions extensions,
			Configuration config) throws ConfigurationException {
		Set<RelContentExtRatEntity> relContentExtRatList = new HashSet<RelContentExtRatEntity>();

		if (extensions != null) {
			ExtendedRatingList extRatingListObj = extensions.getExtendedRatingList(); // tag ExtendedRatingList
			if (extRatingListObj != null) {
				List<String> extRatingList = extRatingListObj.getExtendedRating(); // tag ExtendedRating
				if (extRatingList != null && !extRatingList.isEmpty()) {// if exists rate type

					for (Iterator<String> iterator = extRatingList.iterator(); iterator.hasNext();) {
						RelContentExtRatEntityPK relContentExtRatPK = new RelContentExtRatEntityPK();
						RelContentExtRatEntity relContentExtRat = new RelContentExtRatEntity();
						relContentExtRatPK.setContentId(vodElement.getContentId().intValue());

						String pcLevel = config.getPcExtendedRatings().getKey(iterator.next());
						// Long pcLevel = pcExtRatMap.get(iterator.next().value());

						if (pcLevel == null) {
							log.logMessage(
									"No PC level retrieved from PcExternalRating in input: " + iterator.next());
						}
						else{
						relContentExtRatPK.setPcId(Integer.parseInt(pcLevel));
						}
						relContentExtRat.setId(relContentExtRatPK);
						relContentExtRatList.add(relContentExtRat);
					}
				}
			} else {
				if (vodElement.getContentId() == null) {
					log.logMessage(
							"No ExtendedRating related to the Content in the Asset XML file for externalContentId "
									+ vodElement.getHouseId());
				} else {
					log.logMessage("No ExtendedRating related to the Content in the Asset XML file for contentId "
							+ vodElement.getContentId());
				}
			}
		}
		return relContentExtRatList;

	}

	/**
	 * Retrieve the set of RelPlatformTechnical objects related to the
	 * ContentPlatform variant.
	 * 
	 * @param PackageList
	 *            platformPackageList
	 * @param Long
	 *            contentId
	 * @throws ApplicationException
	 * 
	 */
	private List<RelPlatformTechnicalEntity> getVodRelPlatformTechnical(PackageList platformPackageList, Long contentId)
			throws ApplicationException {
		List<RelPlatformTechnicalEntity> relPlatformTechnicalList = new ArrayList<RelPlatformTechnicalEntity>();
		if (platformPackageList != null) {

			List<BigInteger> packageIdsList = platformPackageList.getPackage();

			List<Integer> packageIdList = new ArrayList<Integer>();

			if (packageIdsList != null) {
				for (BigInteger packageId : packageIdsList) {
					packageIdList.add(packageId.intValue());
				}
			}
			Map<Integer, String> technicalPackages = new HashMap<>();

			List<Object[]> technicalPackageObject = technicalPackageRepository.retrievePackagesNotRvodByPackageIds(packageIdList);
			for(Object[] technicalPackage : technicalPackageObject) {
				technicalPackages.put((int)technicalPackage[0], technicalPackage[1]!=null?technicalPackage[1].toString():null);
			}
			List<String> technicalPkgProperties = new ArrayList<>();

			for (Integer packageId : packageIdList) {
				String propertyName = technicalPackages.get(packageId);
				technicalPkgProperties.add(propertyName);
			}

			if (propertiesList.isEmpty()) {
				for (String propertyName : technicalPkgProperties) {
					if (!StringUtils.isEmpty(propertyName)) {
						throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
								new NestedParameters(
										"Incompatibility between asset level properties and technical package"));

					}
				}
			
			} else {
				for (String propertyName : propertiesList) {
					if (technicalPkgProperties != null && !technicalPkgProperties.contains(null)
							&& !technicalPkgProperties.contains(propertyName)) {
						throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
								new NestedParameters(
										"Incompatibility between asset level properties and technical package"));

					}
				}
			
				}

			for (Iterator<BigInteger> iterator2 = packageIdsList.iterator(); iterator2.hasNext();) {

				BigInteger b = iterator2.next();
				Long packageId = b.longValue();

				if (technicalPackages.containsKey(packageId.intValue())) {

					RelPlatformTechnicalEntity relPlatformTechnical = new RelPlatformTechnicalEntity();
					RelPlatformTechnicalEntityPK relPlatformTechnicalPK = new RelPlatformTechnicalEntityPK();
					relPlatformTechnicalPK.setPackageId(packageId.intValue());
					relPlatformTechnicalPK.setSbundleId(-1);
					ContentEntity contentEntity = new ContentEntity();
					contentEntity.setContentId(contentId.intValue());
					relPlatformTechnical.setContent(contentEntity);
					relPlatformTechnical.setId(relPlatformTechnicalPK);
					relPlatformTechnicalList.add(relPlatformTechnical);
				} else {
					log.logMessage("Package id " + packageId
							+ " not found. No association is created for this package in RelContentTechnical.");
				}
			}
		}

		return relPlatformTechnicalList;

	}

	/**
	 * Retrieve the set of ContentCategory objects related to the Content.
	 * 
	 * @param Asset
	 *            vodElement
	 * @param Extensions
	 *            extensions
	 * 
	 */
	private Set<ContentCategoryEntity> getVodContentCategory(Asset vodElement, Extensions extensions) {
		Set<ContentCategoryEntity> contentCategoryList = new HashSet<ContentCategoryEntity>();
		// CR TO make category list not mandatory
		boolean deleteContentCategoryOnWriterStep = false;
		// END CR TO make category list not mandatory
		if (extensions != null) {
			CategoryList categorListObj = extensions.getCategoryList();
			if (categorListObj != null) {
				List<CategoryId> categoryIdList = categorListObj.getCategoryId();
				if (categoryIdList != null) {
					for (Iterator<CategoryId> iterator = categoryIdList.iterator(); iterator.hasNext();) {
						// CR TO make category list not mandatory
						deleteContentCategoryOnWriterStep = true;
						// END CR TO make category list not mandatory
						ContentCategoryEntity contentCategory = new ContentCategoryEntity();
						CategoryId categoryIterator = iterator.next();
						Long categoryId = getCategoryIdFromExt(categoryIterator.getValue());
						if (categoryId == null) {
							log.logMessage(
									"No category retrieved from externalId in input. Any record inserted in Content_Category");
						} else {
							ContentCategoryEntityPK contentCategoryPk = new ContentCategoryEntityPK();
							contentCategoryPk.setCategoryId(categoryId.intValue());
							contentCategoryPk.setContentId(vodElement.getContentId().intValue());
							contentCategory.setId(contentCategoryPk);
							if (categoryIterator.getPosition() != null) {
								contentCategory.setPosition(categoryIterator.getPosition().intValue());
							}
							// Start: Added for 4.2 New Object
							if (categoryIterator.getIsPrimary() == null) {
								contentCategory.setIsPrimary("N");
							} else {
								contentCategory.setIsPrimary(categoryIterator.getIsPrimary().value());
							}

							if (categoryIterator.getStartDate() != null) {
								contentCategory.setStartDate(
										Validator.xmlGregorianCalendar2Date(categoryIterator.getStartDate()));}

							if (categoryIterator.getEndDate() != null) {
								contentCategory
										.setEndDate(Validator.xmlGregorianCalendar2Date(categoryIterator.getEndDate()));}
							// End: Added for 4.2 New Object

							contentCategoryList.add(contentCategory);
						
						}

					}
				}
			}
		}
		// END CR TO make category list not mandatory
		return contentCategoryList;
	}

	/**
	 * Support method for retrieve categoryId by the externalCategory id.
	 * 
	 * @param channelId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Long getCategoryIdFromExt(String categoryExternalId) {
		Long categoryId = null;
		List<CategoryEntity> category = new ArrayList<>();
		try {
			category = categoryRepository.retrieveByExternald(categoryExternalId);
		} catch (Exception e) {
			log.logError(e, "Error during retrieving categoryId from database by externalId " + categoryExternalId);
		}
		if (category != null && !category.isEmpty()) {
			categoryId = category.get(0).getCategoryId().longValue();
		} else {
			log.logMessage("No results found for category with externalId: " + categoryExternalId);
		}

		return categoryId;
	}

	/**
	 * Retrieve the set of Audiolang objects related to the ContentPlatform
	 * variants.
	 * 
	 * @param CategoryContent
	 *            vodElement
	 * 
	 */
	private List<AudiolangEntity> getVodAudiolang(Languages languages, Long contentId) {
		List<AudiolangEntity> contentAudiolangList = new ArrayList<AudiolangEntity>();
		if (languages == null) {
			return contentAudiolangList;
		}
		contentAudiolangList.add(audiolangFromLangType(languages.getPreferredLang(), contentId, true));

		for (LangType lang : languages.getLang()) {
			contentAudiolangList.add(audiolangFromLangType(lang, contentId, false));
		}

		return contentAudiolangList;
	}

	/**
	 * Support method for retrieve Audiolang by the LangType.
	 * 
	 * @param LangType
	 * @param contentId
	 * @param boolean
	 *            isPreferred
	 * @return
	 */
	private AudiolangEntity audiolangFromLangType(LangType lang, Long contentId, boolean isPreferred) {
		AudiolangEntity al = new AudiolangEntity();
		AudiolangEntityPK alPK = new AudiolangEntityPK();

		alPK.setLangId(lang.getId());
		ContentEntity content = new ContentEntity();
		content.setContentId(contentId.intValue());
		al.setContent(content);
		al.setId(alPK);
		al.setLabel(lang.getLabel());

		if (isPreferred) {
			al.setIsPreferred("Y");
		} else {
			al.setIsPreferred("N");
		}

		al.setStreamId(lang.getStream().intValue());
		if (lang.getCode() != null) {
			al.setLangCode(lang.getCode());
		}
		return al;
	}

	/**
	 * Retrieve the set of Subtitles objects related to the Content.
	 * 
	 * @param CategoryContent
	 *            vodElement
	 * 
	 */
	private List<SubtitleEntity> getVodSubtitles(
			com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Subtitles platformSubtitle, Long contentId,
			Integer cpId) {
		List<SubtitleEntity> contentSubtitlesList = new ArrayList<SubtitleEntity>();

		if (platformSubtitle == null) {
			return contentSubtitlesList;
		}

		platformSubtitle.getSubtitle();
		for (Subtitle sub : platformSubtitle.getSubtitle()) {
			SubtitleEntity subtitle = new SubtitleEntity();
			SubtitleEntityPK subtitlePK = new SubtitleEntityPK();

			ContentEntity content = new ContentEntity();
			content.setContentId(contentId.intValue());

			subtitlePK.setCpId(cpId);
			subtitlePK.setSubId(sub.getId());
			subtitle.setContent(content);
			subtitle.setId(subtitlePK);
			subtitle.setLabel(sub.getLabel());

			contentSubtitlesList.add(subtitle);
		}
		return contentSubtitlesList;
	}

	/**
	 * Retrieve the set of Subtitles objects related to the Content.
	 * 
	 * @param Asset
	 *            vodElement
	 * 
	 */
	@SuppressWarnings("unused")
	private Set<SubtitleEntity> getVodSubtitles(Asset vodElement) {
		Set<SubtitleEntity> contentSubtitlesList = new HashSet<SubtitleEntity>();

		if (vodElement.getSubtitles() == null) {
			return contentSubtitlesList;
		}

		vodElement.getSubtitles().getSubtitle();
		for (Subtitle sub : vodElement.getSubtitles().getSubtitle()) {
			SubtitleEntity subtitle = new SubtitleEntity();
			SubtitleEntityPK subtitlePK = new SubtitleEntityPK();
			ContentEntity content = new ContentEntity();
			content.setContentId(vodElement.getContentId().intValue());

			subtitlePK.setSubId(sub.getId());
			subtitle.setContent(content);
			subtitle.setId(subtitlePK);
			subtitle.setLabel(sub.getLabel());

			contentSubtitlesList.add(subtitle);
		}
		return contentSubtitlesList;
	}

	private String stringWithPipeComma(List<String> dataList, String pipe) {
		String idList = dataList.toString();
		String csv = idList.substring(1, idList.length() - 1).replace(pipe, pipe);
		return csv;
	}

	private static boolean validateLangCode(String langCode) {
		boolean validateLang = true;
		String[] languages = Locale.getISOLanguages();
		Map<String, Locale> localeMap = new HashMap<String, Locale>(languages.length);
		for (String language : languages) {
			Locale locale = new Locale(language);
			localeMap.put(locale.getISO3Language(), locale);
		}
		Set ss = localeMap.keySet();
		for (Object o : ss) {
			if (langCode.equalsIgnoreCase(o.toString())) {
				validateLang = false;
			}
		}

		return validateLang;
	}

	private void validateTRCList(Map<Long, GetTransactionRateCardResponse> trcIdCache,
			Map<String, GetTransactionRateCardResponse> trcNameCache, List<TRC> trcList) throws ApplicationException {

		List<GetTransactionRateCardResponse> transactionRateCardList = new ArrayList<>();
		List<GetTransactionRateCardResponse> transactionRateCardCopyList = new ArrayList<>();

		for (TRC trc : trcList) {
			GetTransactionRateCardResponse trcResponse = null;
			if (null == trc.getTRCId().getIsExternalId() || (null != trc.getTRCId().getIsExternalId()
					&& ("N").equals(trc.getTRCId().getIsExternalId().toString().trim()))) {
				trcResponse = trcIdCache.get(Long.parseLong(trc.getTRCId().getValue()));
			} else {
				trcResponse = trcNameCache.get(trc.getTRCId().getValue());
			}

			if (null == trcResponse || ("N").equals(trcResponse.getIsActive())) {
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
						new NestedParameters("Invalid TRC"));

			}
			transactionRateCardList.add(trcResponse);
			transactionRateCardCopyList.add(trcResponse);
		}

		for (GetTransactionRateCardResponse copyTRC : transactionRateCardCopyList) {

			Long rentalPeriod = copyTRC.getRentalPeriod();
			Date startDate = copyTRC.getStartDate();
			Date endDate = copyTRC.getEndDate();
			List<String> platforms = copyTRC.getPlatformList();
			List<String> videoTypes = copyTRC.getVideoTypeList();
			String priceCategoryId = copyTRC.getPriceCategory();

			for (GetTransactionRateCardResponse trc : transactionRateCardList) {

				boolean isExceptionRequired = false;
				boolean overLappingTRC = false;
				if (null == copyTRC.getTransactionRateCardId()
						|| !copyTRC.getTransactionRateCardId().equals(trc.getTransactionRateCardId())) {

					if (null != trc.getPlatformList() && null != platforms) {
						if (checkPlatformsAreEqual(trc.getPlatformList(), platforms)) {
							isExceptionRequired = true;
							overLappingTRC = true;
						} else {
							isExceptionRequired = false;
							overLappingTRC = false;
						}
					} else {
						isExceptionRequired = true;
						overLappingTRC = true;
					}

					if (isExceptionRequired) {
						if (null != trc.getVideoTypeList() && null != videoTypes) {

							if (checkVideoTypesAreEqual(trc.getVideoTypeList(), videoTypes)) {
								isExceptionRequired = true;
								overLappingTRC = true;
							} else {
								isExceptionRequired = false;
								overLappingTRC = false;
							}
						} else {
							isExceptionRequired = true;
							overLappingTRC = true;
						}
					}

					if (isExceptionRequired) {
						if (trc.getPriceCategory().equals(priceCategoryId)) {
							isExceptionRequired = true;
							overLappingTRC = true;
						} else {
							isExceptionRequired = false;
							overLappingTRC = false;
						}
					}

					if (isExceptionRequired) {
						if (trc.getRentalPeriod().equals(rentalPeriod)) {
							isExceptionRequired = true;
							overLappingTRC = true;
						} else {
							isExceptionRequired = false;
							overLappingTRC = false;
						}
					}

					if (isExceptionRequired) {
						if (trc.getStartDate().getTime() == startDate.getTime()) {
							isExceptionRequired = true;
							overLappingTRC = false;
						} else {
							isExceptionRequired = false;
							overLappingTRC = true;
						}
					}

					if (isExceptionRequired) {
						if ((trc.getEndDate() == null && endDate == null) || (trc.getEndDate() != null
								&& endDate != null && (trc.getEndDate().getTime() == endDate.getTime()))) {
							isExceptionRequired = true;
							overLappingTRC = false;
						} else {
							isExceptionRequired = false;
							overLappingTRC = true;
						}
					}

					if (overLappingTRC) {
						if (!((trc.getStartDate().before(startDate)
								&& (trc.getEndDate() != null && trc.getEndDate().before(startDate)))
								|| (startDate.before(trc.getStartDate())
										&& (endDate != null && endDate.before(trc.getStartDate()))))) {
							throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
									new NestedParameters("TRC[" + copyTRC.getName()
											+ "] startDate and endDate is overlapping with other TRC[" + trc.getName()
											+ "]"));

						}

					}

				}
			}

		}
	}

	/**
	 * This method is used to check the requested platfroms and existed platforms
	 * are equal or not.
	 * 
	 * @param platforms
	 * @param platformsNew
	 * @param config
	 * @throws ActionException
	 * @throws ConfigurationException
	 */
	public static boolean checkPlatformsAreEqual(List<String> platformList, List<String> platformListNew) {
		boolean isConflicted = false;

		final Set<String> platformSet = new HashSet<>(platformList);
		final Set<String> newPlatformSet = new HashSet<>(platformListNew);

		// Set<String> commonPlatforms = Sets.intersection(platformSet, newPlatformSet);
		boolean commonPlatformsExisted = platformSet.retainAll(newPlatformSet);
		if (platformSet.equals(newPlatformSet) || commonPlatformsExisted) {
			isConflicted = true;
		}
		return isConflicted;

	}

	/**
	 * This method is used to check the requested videoTypes and existing videoTypes
	 * are equal or not.
	 * 
	 * @param videoTypes
	 * @param videoTypesNew
	 * @param config
	 * @return
	 * @throws ActionException
	 * @throws ConfigurationException
	 */
	public static boolean checkVideoTypesAreEqual(List<String> videoTypeList, List<String> videoTypeListNew) {

		boolean isConflicted = false;
		final Set<String> videoTypeSet = new HashSet<>(videoTypeList);
		final Set<String> newVideoTypeSet = new HashSet<>(videoTypeListNew);
		// Set<String> commonVideoTypes = Sets.intersection(videoTypeSet,
		// newVideoTypeSet);
		boolean commonVideoTypesExisted = videoTypeSet.retainAll(newVideoTypeSet);
		if (videoTypeSet.equals(newVideoTypeSet) || commonVideoTypesExisted) {
			isConflicted = true;
		}

		return isConflicted;
	}

	/**
	 * @param content
	 * @param contentPlatforms
	 * @param relContentExtRats
	 * @param contentCategories
	 * @param listOfScenes
	 * @param listOfChapters
	 * @param multiLanguageContents
	 * @param listOfMultilanguageScenes
	 * @param listOfMultilanguageChapters
	 * @param scenePlatforms
	 * @param chapterPlatforms
	 * @param objectEmfAttributes
	 * @param blacklistDeviceEntities
	 * @param config
	 * @throws ApplicationException
	 * @throws Exception
	 */
	@Transactional(rollbackFor = { ApplicationException.class, RuntimeException.class })
	private Set<ContentPlatformEntity> writeContent(ContentEntity content, Set<ContentPlatformEntity> contentPlatforms,
			Set<RelContentExtRatEntity> relContentExtRats, Set<ContentCategoryEntity> contentCategories,
			Set<SceneEntity> listOfScenes, Set<ChapterEntity> listOfChapters,
			Set<MultiLanguageContentEntity> multiLanguageContents,
			Set<MultilanguageSceneEntity> listOfMultilanguageScenes,
			Set<MultilanguageChapterEntity> listOfMultilanguageChapters, Set<ObjectPlatformEntity> scenePlatforms,
			Set<ObjectPlatformEntity> chapterPlatforms, Set<ObjectEmfEntity> objectEmfAttributes,
			Set<BlacklistDeviceEntity> blacklistDeviceEntities, Set<LanguageMetadataEntity> languages,
			Configuration config) throws ApplicationException {

		Set<ContentPlatformEntity> contentPlatformSet = null;
		try {
			deleteContentRelatedData(content);

			contentRepository.save(content);

			if (languages != null) {
				for (LanguageMetadataEntity language : languages) {
					Integer id = languageRepository.retrieveIdByLanguageCode(language.getLanguageCode());
					if (id == null || id == 0) {
						languageRepository.save(language);
					}
				}

			}

			upsertSegmentDetails(content, listOfScenes, listOfChapters, listOfMultilanguageScenes,
					listOfMultilanguageChapters, scenePlatforms, chapterPlatforms, objectEmfAttributes);

			if (relContentExtRats != null) {
				relcontentExtRatRepository.saveAll(relContentExtRats);
			}

			if (contentCategories != null) {
				contentCategoryRepository.saveAll(contentCategories);
			}
			/*
			 * if (content.getContentLinking() != null) {
			 * contentLinkingRepository.saveAll(content.getContentLinking()); }
			 */
			if (multiLanguageContents != null) {
				multiLanguageContentRepository.saveAll(multiLanguageContents);
			}
			if (listOfScenes != null) {
				sceneRepository.saveAll(listOfScenes);
			}

			if (listOfChapters != null) {
				chapterRepository.saveAll(listOfChapters);
			}

			if (listOfMultilanguageScenes != null) {
				multiLanguageSceneRepository.saveAll(listOfMultilanguageScenes);
			}

			if (listOfMultilanguageChapters != null) {
				multiLanguageChapterRepository.saveAll(listOfMultilanguageChapters);
			}

			if (blacklistDeviceEntities != null) {
				blacklistDeviceContentRepository.saveAll(blacklistDeviceEntities);
			}
			contentPlatformSet = upsertContentPlatforms(contentPlatforms);
		} catch (ApplicationException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException();
		}
		return contentPlatformSet;

	}

	/**
	 * @param contentPlatforms
	 */
	private Set<ContentPlatformEntity> upsertContentPlatforms(Set<ContentPlatformEntity> contentPlatforms) {

		List<RelPlatformTechnicalEntity> relPlatformTechnicalList = new ArrayList<RelPlatformTechnicalEntity>();
		List<AudiolangEntity> audioLangList = new ArrayList<AudiolangEntity>();
		List<SubtitleEntity> subtitlesList = new ArrayList<SubtitleEntity>();
		for (ContentPlatformEntity contentPlatform : contentPlatforms) {

			List<RelPlatformTechnicalEntity> relPlatformTechnicalS = contentPlatform.getRelPlatformTechnicals();
			List<AudiolangEntity> audioLangs = contentPlatform.getAudiolangs();
			List<SubtitleEntity> subtitles = contentPlatform.getSubtitles();
			/* MODIFICA PER NON CANCELLARE PLATFORM */
			Integer cpIdIfExist = getCpIdByKey(contentPlatform);

			if (cpIdIfExist == null) {
				contentPlatform.setCpId(0);
			} else {
				contentPlatform.setCpId(cpIdIfExist);
			}
			Integer cpId = contentPlatformRepository.save(contentPlatform).getCpId();
			contentPlatform.setCpId(cpId);

			// Iterate the set of RelPlatformTechnicals save the RelPlatformTechnical
			// releted to a ContentPlatform
			for (Iterator<RelPlatformTechnicalEntity> iteratorTech = relPlatformTechnicalS.iterator(); iteratorTech
					.hasNext();) {
				RelPlatformTechnicalEntity relPlatformTech = iteratorTech.next();
				relPlatformTech.getId().setCpId(cpId);
				relPlatformTechnicalList.add(relPlatformTech);
			}

			// Iterate the set of Subtitles, save the Subtitle releted to a ContentPlatform
			for (Iterator<SubtitleEntity> iteratorSub = subtitles.iterator(); iteratorSub.hasNext();) {
				SubtitleEntity subtitle = iteratorSub.next();
				subtitle.getId().setCpId(cpId);
				subtitlesList.add(subtitle);
			}

			// Iterate the set of Audiolang, save the Audiolang releted to a ContentPlatform
			for (Iterator<AudiolangEntity> iteratorAud = audioLangs.iterator(); iteratorAud.hasNext();) {
				AudiolangEntity language = iteratorAud.next();
				language.getId().setCpId(cpId);
				audioLangList.add(language);
			}

		}
		relPlatformTechnicalRepository.saveAll(relPlatformTechnicalList);
		subtitlesRepository.saveAll(subtitlesList);
		audioLangRepository.saveAll(audioLangList);

		return contentPlatforms;
	}

	/**
	 * @param contentPlatform
	 * @return
	 */
	private Integer getCpIdByKey(ContentPlatformEntity contentPlatform) {

		Integer cpId = contentPlatformRepository.retriveByVideoTypeAndPlatformAndContentId(
				contentPlatform.getVideoName(), contentPlatform.getPlatform(), contentPlatform.getContentId());

		return cpId;
	}

	/**
	 * @param content
	 * @param listOfScenes
	 * @param listOfChapters
	 * @param listOfMultilanguageScenes
	 * @param listOfMultilanguageChapters
	 * @param scenePlatforms
	 * @param chapterPlatforms
	 * @param objectEmfAttributes
	 * @throws ApplicationException
	 */
	private void upsertSegmentDetails(ContentEntity content, Set<SceneEntity> listOfScenes,
			Set<ChapterEntity> listOfChapters, Set<MultilanguageSceneEntity> listOfMultilanguageScenes,
			Set<MultilanguageChapterEntity> listOfMultilanguageChapters, Set<ObjectPlatformEntity> scenePlatforms,
			Set<ObjectPlatformEntity> chapterPlatforms, Set<ObjectEmfEntity> objectEmfAttributes)
			throws ApplicationException {

		List<ObjectEmfEntity> objectEmfListForContent = new ArrayList<ObjectEmfEntity>();
		List<EmfEntity> emfListForName = null;
		// inserting or updating EMF attributes of Content
		for (ObjectEmfEntity objectEmf : objectEmfAttributes) {
			if (objectEmf != null) {

				emfListForName = emfRepository.retrieveByEmfName(objectEmf.getEmfName());
				// emfListForName = hibernateTemplate.find("from com.accenture.avs.ci.domain.Emf
				// e where e.emfName='"+objectEmf.getEmfName()+"'");
				if (emfListForName.isEmpty()) {
					EmfEntity emf = new EmfEntity();
					emf.setEmfName(objectEmf.getEmfName());
					emf.setIsActive("Y");
					if ("VIDEO".equals(content.getExtendedContentAttribute().getObjectType())) {
						Integer id = emfRepository.getMaxId();
						if(id == null || id == 0){
							id = 0;
						}
						emf.setEmfId(id + 1);
						emfRepository.save(emf);

						/*
						 * Serializable conEMFId = hibernateTemplate.save(emf); Long generatedConEMFId =
						 * (Long)conEMFId;
						 */
						objectEmf.getId().setEmfId(emf.getEmfId());
						objectEmfListForContent.add(objectEmf);
					}
				} else {
					objectEmf.getId().setEmfId(emfListForName.get(0).getEmfId());
					objectEmfListForContent.add(objectEmf);
					if ("VIDEO".equals(content.getExtendedContentAttribute().getObjectType())) {
						objectEmfRepository.save(objectEmf);
					}
				}

			}
		}

		// for each multilanguage scene
		for (MultilanguageSceneEntity multilanguageScene : listOfMultilanguageScenes) {
			List<MultilanguageSceneEntity> ms = multiLanguageSceneRepository.retrieveBySceneIdNotContentId(
					multilanguageScene.getId().getSceneId(), content.getContentId().longValue());
			// hibernateTemplate.find("from com.accenture.avs.ci.domain.MultilanguageScene
			// ms where ms.compId.sceneId=" + multilanguageScene.getCompId().getSceneId() +
			// " and ms.compId.contentId<>" +
			// multilanguageScene.getCompId().getContentId());
			if (!ms.isEmpty()) {
				log.logMessage("SceneId " + multilanguageScene.getId().getSceneId()
						+ " is already associated to other content.");
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
						new NestedParameters("SceneId " + multilanguageScene.getId().getSceneId()
								+ " is already associated to other content."));

			}

			// for each multilanguage scene level emf attribute
			List<EmfEntity> emfListForName1 = null;
			if (multilanguageScene.getEmfAttributes() != null) {
				for (ObjectEmfEntity objectEmf : multilanguageScene.getEmfAttributes()) {
					if (objectEmf != null) {

						emfListForName1 = emfRepository.retrieveByEmfName(objectEmf.getEmfName());
						// emfListForName1 = hibernateTemplate.find("from
						// com.accenture.avs.ci.domain.Emf e where
						// e.emfName='"+objectEmf.getEmfName()+"'");
						if (emfListForName1.isEmpty()) {

							EmfEntity emf = new EmfEntity();
							emf.setEmfName(objectEmf.getEmfName());
							emf.setIsActive("Y");
							Integer id = emfRepository.getMaxId();
							if(id == null || id == 0){
								id = 0;
							}
							emf.setEmfId(id + 1);
							emfRepository.save(emf);
							// Long multiLangSceneEMFId = (Long)generatedEMFID;
							objectEmf.getId().setEmfId(emf.getEmfId());
							objectEmfRepository.save(objectEmf);
						} else {

							objectEmf.getId().setEmfId(emfListForName1.get(0).getEmfId());
							objectEmfRepository.save(objectEmf);
						
						}
					}
				}
			}
		}

		// for each scene
		List<EmfEntity> emfListForName2 = null;
		for (SceneEntity scene : listOfScenes) {
			List<SceneEntity> s = sceneRepository.retrieveBySceneIdNotContentId(scene.getCompId().getSceneId(),
					content.getContentId().longValue());
			// hibernateTemplate.find("from Scene s where s.compId.sceneId=" +
			// scene.getCompId().getSceneId() + " and s.compId.contentId<>" +
			// scene.getCompId().getContentId());
			if (!s.isEmpty()) {
				log.logMessage(
						"SceneId " + scene.getCompId().getSceneId() + " is already associated to other content.");
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(
						"SceneId " + scene.getCompId().getSceneId() + " is already associated to other content."));

			}
			// for each scene level emf attribute
			if (scene.getEmfAttributes() != null) {
				for (ObjectEmfEntity objectEmf : scene.getEmfAttributes()) {
					if (objectEmf != null) {
						emfListForName2 = emfRepository.retrieveByEmfName(objectEmf.getEmfName());
						// hibernateTemplate.find("from com.accenture.avs.ci.domain.Emf e where
						// e.emfName='"+objectEmf.getEmfName()+"'");
						if (emfListForName2.isEmpty()) {

							EmfEntity emf = new EmfEntity();
							emf.setEmfName(objectEmf.getEmfName());
							emf.setIsActive("Y");
							Integer id = emfRepository.getMaxId();
							if(id == null || id == 0){
								id = 0;
							}
							emf.setEmfId(id + 1);
							emfRepository.save(emf);
							// Long sceneEMFId = (Long)generatedEMFID;
							objectEmf.getId().setEmfId(emf.getEmfId());
							objectEmfRepository.save(objectEmf);
						} else {
							
							objectEmf.getId().setEmfId(emfListForName2.get(0).getEmfId());
							objectEmfRepository.save(objectEmf);
						

						}
					}
				}
			}

			// for each platform of scene
			if (scenePlatforms != null) {
				for (ObjectPlatformEntity objectPlatform : scenePlatforms) {
					if (objectPlatform != null) {
						// insert into OBJECT_PLATFORM
						objectPlatformRepository.save(objectPlatform);
					}
				}
			}
		}

		// for each chapter
		for (ChapterEntity chapter : listOfChapters) {
			List<ChapterEntity> c = chapterRepository.retrieveByChapterIdNotContentId(
					chapter.getCompId().getChapterId(), content.getContentId().longValue());
			// hibernateTemplate.find("from com.accenture.avs.ci.domain.Chapter c where
			// c.compId.chapterId=" + chapter.getCompId().getChapterId() + " and
			// c.compId.contentId<>" + chapter.getCompId().getContentId());
			if (!c.isEmpty()) {
				log.logMessage(
						"ChapterId " + chapter.getCompId().getChapterId() + " is already associated to other content.");
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
						new NestedParameters("ChapterId " + chapter.getCompId().getChapterId()
								+ " is already associated to other content."));

			}

			// for each platform of chapter
			if (chapterPlatforms != null) {
				for (ObjectPlatformEntity objectPlatform : chapterPlatforms) {
					if (objectPlatform != null) {
						// insert into OBJECT_PLATFORM
						objectPlatformRepository.save(objectPlatform);
					}
				}
			}
		}

		// for each multilanguage chapter
		for (MultilanguageChapterEntity multilanguageChapter : listOfMultilanguageChapters) {
			List<MultilanguageChapterEntity> mc = multiLanguageChapterRepository.retrieveByChapterIdNotContentId(
					multilanguageChapter.getId().getChapterId(), content.getContentId().longValue());
			// hibernateTemplate.find("from com.accenture.avs.ci.domain.MultilanguageChapter
			// mc where mc.compId.chapterId=" +
			// multilanguageChapter.getCompId().getChapterId() + " and
			// mc.compId.contentId<>" + multilanguageChapter.getCompId().getContentId());
			if (!mc.isEmpty()) {
				log.logMessage("ChapterId " + multilanguageChapter.getId().getChapterId()
						+ " is already associated to other content.");
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
						new NestedParameters("ChapterId " + multilanguageChapter.getId().getChapterId()
								+ " is already associated to other content."));

			}
		}

		// super.doWrite(hibernateTemplate, (List<? extends T>) newContentEmfList);
		objectEmfRepository.saveAll(objectEmfListForContent);

	}

	/**
	 * @param content
	 */
	private void deleteContentRelatedData(ContentEntity content) {

		contentCategoryRepository.deleteByContentId(content.getContentId());
		relcontentExtRatRepository.deleteByContentId(content.getContentId());
		subtitlesRepository.deleteByContentId(content.getContentId());

		audioLangRepository.deleteByContentId(content.getContentId());
		contentLinkingRepository.deleteByParentContentId(content.getContentId());

		List<Integer> lstPkgNotRVOD = technicalPackageRepository
				.retrieveTechPackagesNotRVodByContentId(content.getContentId());

		if (lstPkgNotRVOD != null && !lstPkgNotRVOD.isEmpty()) {
			for (int i = 0; i < lstPkgNotRVOD.size(); i++) {
				Integer pkgIdNotRVOD = lstPkgNotRVOD.get(i);
				relPlatformTechnicalRepository.deleteByContentIdAndPackageId(content.getContentId(), pkgIdNotRVOD);

			}

		}
		contentPlatformRepository.unPublishByContentId(content.getContentId());
		multiLanguageContentRepository.deleteByContentId(content.getContentId());
		blacklistDeviceContentRepository.deleteByContentId(content.getContentId());

		List<Long> sceneIds = sceneRepository.retrieveSceneIdsBycontentId(content.getContentId().longValue());

		for (Long sceneId : sceneIds) {
			objectEmfRepository.deleteByObjectTypeAndContentId(sceneId.intValue(), 3);
		}
		List<Long> chapterIds = chapterRepository.retrieveChapterIdsByContentId(content.getContentId().longValue());

		for (Long chapterId : chapterIds) {
			objectEmfRepository.deleteByObjectTypeAndContentId(chapterId.intValue(), 2);
		}
		multiLanguageSceneRepository.deleteByContentId(content.getContentId().longValue());
		sceneRepository.deleteByContentId(content.getContentId().longValue());
		multiLanguageChapterRepository.deleteByContentId(content.getContentId().longValue());
		chapterRepository.deleteByContentId(content.getContentId().longValue());
		objectEmfRepository.deleteByObjectTypeAndContentId(content.getContentId(), 1);

	}
	
	

	@Transactional(rollbackFor = { ApplicationException.class, Exception.class })
	@Override
	public List<Integer> unpublishContent(Integer contentId, Configuration config) throws Exception {
		ContentEntity content = null;
		log.logMessage("Started Unpublishing Content {}", contentId);
		Set<Integer> contentIds = null;
		List<Integer> unpublishContentIds = null;

		try {
			
			contentPlatformRepository.unPublishByContentId(contentId);
			log.logMessage("Unpublished Platform for content");

			content = getContentDetailByContentId(contentId);
			if (content != null) {
				contentIds = new HashSet<Integer>();
				contentIds.add(content.getContentId());
				// contentIds = new HashSet<Long>();
				ExtendedContentAttributeEntity contentAttribute = content.getExtendedContentAttribute();
				if (contentAttribute != null && commonDataType.contains(contentAttribute.getObjectType())) {
					List<Integer> technicalPackageIds = null;
					// get the technical package Ids

					List<Object> techPkgIds = bundleAggRepository.retrieveTechPkdIdsByBundleContentId(contentId);
					log.logMessage("Retrieved technical package ids for the content");
					if (techPkgIds != null && !techPkgIds.isEmpty()) {
						technicalPackageIds = new ArrayList<Integer>();
						for (Object teckPkgId : techPkgIds) {
							technicalPackageIds.add((Integer) teckPkgId);
						}
					}
					List<Long> solutionOfferIds = null;
					if (technicalPackageIds != null) {
						solutionOfferIds = getCommercialPackagesByTechnicalPackages(technicalPackageIds,
								config);
					}
					log.logMessage("Retrieved solution offer Ids for the content");
					if (solutionOfferIds != null && !solutionOfferIds.isEmpty()) {
						String socketTimeOut = config.getConstants().getValue("HTTP_CLIENT_SOCKET_TIMEOUT");
						String connectiontimeOut = config.getConstants().getValue("HTTP_CLIENT_CONNECTION_TIMEOUT");
						String sdpTenant = config.getConstants().getValue("SDP_TENANT");
						String sdpUrlConnection = dcqVodIngestorRestUrls.SDP_URL;
						SdpClientManagerImpl sdpClientService = SdpClientManagerImpl
								.getSdpClientService(Long.parseLong(connectiontimeOut), Long.parseLong(socketTimeOut));
						for (Long solId : solutionOfferIds) {
							sdpClientService.changeStatusSolutionOfferOnSDP(solId, "Inactive", sdpUrlConnection,
									sdpTenant);
						}
					}
					// delete SVOD mappings
					relPlatformTechnicalRepository.deleteByBundleId(contentId);
					log.logMessage("Deleted SVOD mappings for the content");
					List<Object> contentsOfGrpOfBundle;

					contentsOfGrpOfBundle = bundleAggRepository
							.retrieveContentsByBundleContentId(content.getContentId());

					if (!Utilities.isEmpty(contentsOfGrpOfBundle)) {

						for (Object bundleIdsOfGrpBundle : contentsOfGrpOfBundle) {
							List<Object> bundles = bundleAggRepository
									.retrieveContentsByBundleContentId((Integer) bundleIdsOfGrpBundle);

							if (!Utilities.isEmpty(bundles) && !bundles.isEmpty()) {

								for (Object contId : bundles) {
									List<Object> bundlecontents = bundleAggRepository
											.retrieveContentsByBundleContentId((Integer) contId);

									if (bundlecontents.isEmpty()) {
										contentIds.add((Integer) contId);
									}

								}
							}
							contentIds.add((Integer) bundleIdsOfGrpBundle);
						}

					}

				}
				if (contentIds != null && !contentIds.isEmpty()) {
					unpublishContentIds = new ArrayList<Integer>();
					unpublishContentIds.addAll(contentIds);
				}
			}
			
		} catch (ApplicationException ae) {
			log.logError(ae);
			throw ae;
		} catch (Exception e) {
			log.logError(e);
			throw e;
		}
		return unpublishContentIds;
	}
	
	private List<Long> getCommercialPackagesByTechnicalPackages(List<Integer> technicalPackageIds, Configuration config)
			throws JsonParseException, JsonMappingException, IOException, ApplicationException, ConfigurationException {
		long startTime = new Date().getTime();
		CommercialPackagesResponseDTO commercialPackagesResponse = null;
		String resultCode = "";
		String resultDesc = "";
		String getCommercialPackageUrl = dcqVodIngestorRestUrls.COMMERCIAL_PACKAGE_DETAILS_V2_URL;
	//	String getCommercialPackageUrl = "http://commerce-ms-st-64.openshift.avsocp-accenture.com/avsbe-commerce-ms/v2/commercialPackages";
		List<Long> commercialPkgIds = null;

		List<String> techPkgs = technicalPackageIds.stream().map(Object::toString).collect(Collectors.toList());

		String technicalPackages = String.join(",", techPkgs);
		
		Map<String, String> headerParamsMap = new HashMap<String, String>();

		Map<String, String> paramsMap = new HashMap<String, String>();
		paramsMap.put("technicalPackages", technicalPackages);

		try {
			HttpClientAdapterConfiguration httpConfig = DcqVodIngestorUtils
					.getHttpClientAdapterConfiguration("tenantDefault", config);
			String response = HttpClientAdapter.doGet(httpConfig, getCommercialPackageUrl, paramsMap, null,
					headerParamsMap);

			JsonNode jsonNode = mapper.readTree(response);
			commercialPackagesResponse = mapper.readValue(jsonNode.findValue("resultObj"),
					CommercialPackagesResponseDTO.class);
 
			if (commercialPackagesResponse != null && commercialPackagesResponse.getCommercialPackages() != null) {
				commercialPkgIds = new ArrayList<Long>();
				List<CommercialPackage> commercialPackages = commercialPackagesResponse.getCommercialPackages();
				for (CommercialPackage commPkg : commercialPackages) {
					commercialPkgIds.add(commPkg.getCommercialPackageId().longValue());
				}
			}

			resultCode = mapper.readValue(jsonNode.findValue("resultCode"), String.class);
			resultDesc = mapper.readValue(jsonNode.findValue("resultDescription"), String.class);
			log.logCallToOtherSystemEnd("Commerce", "getCommercialPackagesByTechnicalPackages", "",
					DcqVodIngestorConstants.OK, resultCode, resultDesc, System.currentTimeMillis() - startTime,
					OtherSystemCallType.INTERNAL);

		} catch (HttpClientException hce) {
			DcqVodIngestorUtils.checkServiceUnavailability(hce, MessageKeys.ERROR_MS_ACN_901_COMMERCE_NOT_AVAILABLE);
		} catch (Exception e) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3212_INTERNAL_SERVER_ERROR);
		}

		return commercialPkgIds;
	}

}
