package com.accenture.avs.be.dcq.vod.ingestor.content.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @author v.kumar.korada TransactionRateCardResponse
 *
 */
public class GetTransactionRateCardResponse implements Serializable{
    
    
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(required = true, value = "TRC identifier",example = "1")
    private Long transactionRateCardId;
    @ApiModelProperty(required = true, value = "Unique name that can be used as external ID in provisioning interfaces",dataType="string",example = "TVOD_Pricing_1548226893")
    private String name;
    @ApiModelProperty(required = false, value = "Short description explaining what this TRC represents",dataType="string",example = "TVOD_Pricing_1548226893")
    private String description;
    @ApiModelProperty(required = true, value = "Rental period in hours",example = "60")
    private Long rentalPeriod;
    @ApiModelProperty(required = false, value = "Platform list . Contains zero or more Platforms. Empty or null means this TRC is applicable to all Platforms currently provisioned in the system",example = "[\"PCTV\"]")
    //private List<String> platforms;
    private List<String> platformList;
    @ApiModelProperty(required = false, value = "Video type list . Contains zero or more Video Types. Empty or null means this TRC is applicable to all VideoTypes currently provisioned in the system",example = "[\"HD\"]")
    //private List<String> videoTypes;
    private List<String> videoTypeList;
    @ApiModelProperty(required = true, value = "PriceCategory",example = "REF35")
    private String priceCategory;
    @ApiModelProperty(required = true, value = "Price",example = "25.00")
    private String price;
    @ApiModelProperty(required = true, value = "currency",example = "EUR")
    private String currency;
    @ApiModelProperty(required = true, value = "value of frequency",example = "0")
    private Long frequencyValue;
    @ApiModelProperty(required = true, value = "type of frequency",example = "DAY")
    private String frequencyType;
    @ApiModelProperty(required = false, value = "defines the start date of this TRC",dataType="number",example = "1551377996000")
    private Date startDate;
    @ApiModelProperty(required = false, value = "defines the end date of this TRC",dataType="number",example = "1551395996000")
    private Date endDate;
    @ApiModelProperty(required = true, value = "defines the Active of the TRC",example = "Y")
    private String isActive;
	public Long getTransactionRateCardId() {
		return transactionRateCardId;
	}
	public void setTransactionRateCardId(Long transactionRateCardId) {
		this.transactionRateCardId = transactionRateCardId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getRentalPeriod() {
		return rentalPeriod;
	}
	public void setRentalPeriod(Long rentalPeriod) {
		this.rentalPeriod = rentalPeriod;
	}

	
	public List<String> getPlatformList() {
		return platformList;
	}
	public void setPlatformList(List<String> platformList) {
		this.platformList = platformList;
	}
	public List<String> getVideoTypeList() {
		return videoTypeList;
	}
	public void setVideoTypeList(List<String> videoTypeList) {
		this.videoTypeList = videoTypeList;
	}
	public String getPriceCategory() {
		return priceCategory;
	}
	public void setPriceCategory(String priceCategory) {
		this.priceCategory = priceCategory;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public Long getFrequencyValue() {
		return frequencyValue;
	}
	public void setFrequencyValue(Long frequencyValue) {
		this.frequencyValue = frequencyValue;
	}
	public String getFrequencyType() {
		return frequencyType;
	}
	public void setFrequencyType(String frequencyType) {
		this.frequencyType = frequencyType;
	}
	@JsonFormat(shape = JsonFormat.Shape.NUMBER)
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	@JsonFormat(shape = JsonFormat.Shape.NUMBER)
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	




  
    
    
}
