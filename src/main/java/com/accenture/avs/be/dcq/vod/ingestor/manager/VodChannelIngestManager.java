package com.accenture.avs.be.dcq.vod.ingestor.manager;

import com.accenture.avs.be.framework.cache.Configuration;

/**
 * @author karthik.vadla
 *
 */
public interface VodChannelIngestManager {

	void intializeVodChannelIngestor(String mode, String indexDate, Configuration config);

}
