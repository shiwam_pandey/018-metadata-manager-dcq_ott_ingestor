package com.accenture.avs.be.dcq.vod.ingestor.cache;

import java.util.Date;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.dto.CopyProtectionConfiguration;
import com.accenture.avs.be.dcq.vod.ingestor.utils.CacheConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ConfigurationException;
import com.accenture.avs.be.lib.configuration.ConfigurationClient;
import com.accenture.avs.be.lib.configuration.client.registry.ConfigurationRegistry;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.commons.lib.LoggerWrapper.OtherSystemCallType;

/**
 * @author karthik.vadla
 *
 */
@Component
public class CopyProtectionsCache {

	private static final LoggerWrapper log = new LoggerWrapper(CopyProtectionsCache.class);
	private CopyProtectionConfiguration copyProtectionConfiguration;
	private static String configurationMsInPanic = DcqVodIngestorConstants.CONFIGURATION_MS_IN_PANIC;
	private static String COPY_PROTECTION_CONFIGURATION = "copyProtections";

	public CopyProtectionConfiguration getCopyProtectionSchemes() {
		return copyProtectionConfiguration;
	}

	public Boolean isCopyProtectionInPanic(Configuration config) throws ConfigurationException {
		final String COPY_PROTECTION_IN_PANIC = config.getConstants().getValue(CacheConstants.COPY_PROTECTION_IN_PANIC);
		log.logMessage("COPY_PROTECTION_IN_PANIC:" + COPY_PROTECTION_IN_PANIC);
		boolean flag;
		if (COPY_PROTECTION_IN_PANIC != null && COPY_PROTECTION_IN_PANIC.equalsIgnoreCase(DcqVodIngestorConstants.NO)) {
			flag = false;
		} else {
			flag = true;
		}
		return flag;
	}

	/**
	 * To fetch CopyProtection scheme details from configuration-MS
	 * 
	 * @return
	 * @throws DcqVodIngestorException
	 */
	public CopyProtectionConfiguration loadCopyProtectionsCache(Configuration config) throws ApplicationException {

		long startTime = new Date().getTime();
		boolean finalLogResponse = false;

		try {
			log.logMessage("Loading Copy protections cache");
			if (configurationMsInPanic.equalsIgnoreCase(DcqVodIngestorConstants.NO)
					&& !isCopyProtectionInPanic(config)) {
				copyProtectionConfiguration = new CopyProtectionConfiguration();
				ConfigurationRegistry.registerClass(CopyProtectionConfiguration.class);
				String[] resources = { COPY_PROTECTION_CONFIGURATION };
				log.logCallToOtherSystemRequestBody(DcqVodIngestorConstants.CONFIGURATION,
						DcqVodIngestorConstants.COPY_PROTECTIONS, "", OtherSystemCallType.INTERNAL);
				Map<String, CopyProtectionConfiguration> copyProtectionsMap = (Map<String, CopyProtectionConfiguration>) ConfigurationClient
						.getMicroserviceConfiguration(resources);

				log.logCallToOtherSystemResponseBody(DcqVodIngestorConstants.CONFIGURATION,
						DcqVodIngestorConstants.COPY_PROTECTIONS, DcqVodIngestorConstants.OK,
						OtherSystemCallType.INTERNAL);
				log.logCallToOtherSystemEnd(DcqVodIngestorConstants.CONFIGURATION,
						DcqVodIngestorConstants.COPY_PROTECTIONS, "", DcqVodIngestorConstants.OK, "", "",
						System.currentTimeMillis() - startTime, OtherSystemCallType.INTERNAL);

				copyProtectionConfiguration = copyProtectionsMap.get(COPY_PROTECTION_CONFIGURATION);
				log.logMessage("CopyProtections loaded into cache");

			} else {
				log.logMessage("Configuration MS is in Panic");
			}
		} catch (Exception e) {
			finalLogResponse = true;
			copyProtectionConfiguration = null;
			log.logError(e, "Error while loading CopyProtection Cache");
		} finally {
			if (finalLogResponse) {
				log.logCallToOtherSystemEnd(DcqVodIngestorConstants.CONFIGURATION,
						DcqVodIngestorConstants.COPY_PROTECTIONS, "", DcqVodIngestorConstants.KO, "", "",
						System.currentTimeMillis() - startTime, OtherSystemCallType.INTERNAL);
			}
		}
		return copyProtectionConfiguration;
	}

	public CopyProtectionConfiguration getCopyProtectionConfiguration() {
		return copyProtectionConfiguration;
	}
}
