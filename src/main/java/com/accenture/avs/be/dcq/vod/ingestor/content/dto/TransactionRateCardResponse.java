package com.accenture.avs.be.dcq.vod.ingestor.content.dto;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.accenture.avs.be.framework.bean.GenericResponse;

@JsonSerialize(include=Inclusion.NON_NULL)
public class TransactionRateCardResponse extends GenericResponse{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String resultCode;
	private String resultDescription;
	private Long systemTime;
	private Object resultObject;
	
	public TransactionRateCardResponse(String resultCode,String resultDescription,String totalResult, Long systemTime,Object resultObject){
		this.resultCode=resultCode;
		this.resultDescription=resultDescription;
		this.systemTime=systemTime;
		this.resultObject=resultObject;
	}
	
	
	public TransactionRateCardResponse() {
		// TODO Auto-generated constructor stub
	}


	public String getResultCode() {
		return resultCode;
	}
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	public String getResultDescription() {
		return resultDescription;
	}
	public void setResultDescription(String resultDescription) {
		this.resultDescription = resultDescription;
	}
	
	
	public Long getSystemTime() {
		return systemTime;
	}


	public void setSystemTime(Long systemTime) {
		this.systemTime = systemTime;
	}


	public Object getResultObject() {
		return resultObject;
	}
	public void setResultObject(Object resultObject) {
		this.resultObject = resultObject;
	}

}
