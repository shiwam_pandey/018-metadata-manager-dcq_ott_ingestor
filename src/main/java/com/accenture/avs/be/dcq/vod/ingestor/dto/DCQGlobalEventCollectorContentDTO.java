package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.io.Serializable;

/**
 * @author s.gudivada
 *
 */
public class DCQGlobalEventCollectorContentDTO implements Serializable{
	private static final long serialVersionUID = -460053666626358917L;
	
	private String payload;
	private String action;
	private String contentType;
	private Integer contentId;
	private String extContentId;
	
	public String getPayload() {
		return payload;
	}
	public void setPayload(String payload) {
		this.payload = payload;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public Integer getContentId() {
		return contentId;
	}
	public void setContentId(Integer contentId) {
		this.contentId = contentId;
	}
	public String getExtContentId() {
		return extContentId;
	}
	public void setExtContentId(String extContentId) {
		this.extContentId = extContentId;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
