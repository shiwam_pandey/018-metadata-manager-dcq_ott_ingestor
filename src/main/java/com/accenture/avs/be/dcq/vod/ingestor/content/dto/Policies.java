
package com.accenture.avs.be.dcq.vod.ingestor.content.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;



public class Policies implements Serializable
{

    /**
     * 
     * (Required)
     * 
     */
    private List<Policy> policies = new ArrayList<Policy>();
    private final static long serialVersionUID = -6953490775208725008L;

    /**
     * 
     * (Required)
     * 
     * @return
     *     The policies
     */
    public List<Policy> getPolicies() {
        return policies;
    }

    /**
     * 
     * (Required)
     * 
     * @param policies
     *     The policies
     */
    public void setPolicies(List<Policy> policies) {
        this.policies = policies;
    }

}
