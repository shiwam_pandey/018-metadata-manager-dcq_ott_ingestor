package com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author karthik.vadla
 *
 */
public class ChapterDTO implements Serializable {

	/**
		 * 
		 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Unique Identifier of chapter", required = true, example = "1134311")
	private Long chapterId;

	@ApiModelProperty(value = "Start time of chapter", required = true, example = "00:00:00")
	private String startTime;
	
	@ApiModelProperty(value="End time of chapter",required=true,example="00:30:00")
	private String endTime;
	
	@ApiModelProperty(value="Title of chapter",required=true,example="PK intro scene")
	private String briefTitle;
	
	@ApiModelProperty(value="Order id of scene",required=true,example="2")
	private Long orderId;
	
	@ApiModelProperty(value="Meta Data for different languages",required=true,example="eng")
	private String metadataLanguage;
	
	@ApiModelProperty(value="Multi lang metadata details",required=false,example="")
	private List<MultiLanguageChapterDTO> multiLanguageChapters;
	
	@ApiModelProperty(value="List of Platform details",required=false,example="")
	private List<PlatformDTO> platforms;

	public Long getChapterId() {
		return chapterId;
	}

	public void setChapterId(Long chapterId) {
		this.chapterId = chapterId;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getBriefTitle() {
		return briefTitle;
	}

	public void setBriefTitle(String briefTitle) {
		this.briefTitle = briefTitle;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getMetadataLanguage() {
		return metadataLanguage;
	}

	public void setMetadataLanguage(String metadataLanguage) {
		this.metadataLanguage = metadataLanguage;
	}

	public List<MultiLanguageChapterDTO> getMultiLanguageChapters() {
		return multiLanguageChapters;
	}

	public void setMultiLanguageChapters(List<MultiLanguageChapterDTO> multiLanguageChapters) {
		this.multiLanguageChapters = multiLanguageChapters;
	}

	public List<PlatformDTO> getPlatforms() {
		return platforms;
	}

	public void setPlatforms(List<PlatformDTO> platforms) {
		this.platforms = platforms;
	}

	public static class MultiLanguageChapterDTO implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		
		@ApiModelProperty(value="Title of chapter",required=true,example="PK intro scene")
		private String briefTitle;
		
		@ApiModelProperty(value="Order id of scene",required=true,example="3")
		private Long orderId;
		
		@ApiModelProperty(value="Default language used during chapter",required=true,example="english")
		private String metadataLanguage;

		public String getBriefTitle() {
			return briefTitle;
		}

		public void setBriefTitle(String briefTitle) {
			this.briefTitle = briefTitle;
		}

		public Long getOrderId() {
			return orderId;
		}

		public void setOrderId(Long orderId) {
			this.orderId = orderId;
		}

		public String getMetadataLanguage() {
			return metadataLanguage;
		}

		public void setMetadataLanguage(String metadataLanguage) {
			this.metadataLanguage = metadataLanguage;
		}

	}

	public static class PlatformDTO implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		@ApiModelProperty(value="Platform where object is published",required=true,example="PCTV")
		private String name;
		
		@ApiModelProperty(value="Picture url",required=true,example="http://www.pk.com/pic")
		private String pictureUrl;
		
		@ApiModelProperty(value="Resolution of picture",required=true,example="1024*768")
		private String pictureResolution;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getPictureUrl() {
			return pictureUrl;
		}

		public void setPictureUrl(String pictureUrl) {
			this.pictureUrl = pictureUrl;
		}

		public String getPictureResolution() {
			return pictureResolution;
		}

		public void setPictureResolution(String pictureResolution) {
			this.pictureResolution = pictureResolution;
		}

	}

}
