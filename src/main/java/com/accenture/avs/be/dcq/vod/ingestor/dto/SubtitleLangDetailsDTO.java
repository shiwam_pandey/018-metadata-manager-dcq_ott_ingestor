package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.io.Serializable;

/**
 * @author karthik.vadla
 *
 */
public class SubtitleLangDetailsDTO implements Serializable{
	private static final long serialVersionUID = -2681353497942819024L;
	private Integer cpId;
	private String subId;
	private String label;
	
	public SubtitleLangDetailsDTO(Integer cpId, String subId, String label) {
		super();
		this.cpId=cpId;
		this.subId = subId;
		this.label = label;
	}

	public SubtitleLangDetailsDTO() {
	}

	public Integer getCpId() {
		return cpId;
	}

	public void setCpId(Integer cpId) {
		this.cpId = cpId;
	}

	public String getSubId() {
		return subId;
	}

	public void setSubId(String subId) {
		this.subId = subId;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

}
