package com.accenture.avs.be.dcq.vod.ingestor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.avs.persistence.technicalcatalogue.MultiLanguageContentEntity;
/**
 * @author karthik.vadla
 *
 */
@Repository
public interface MultiLanguageContentRepository extends JpaRepository<MultiLanguageContentEntity,Integer>{

	@Modifying
	@Transactional
	void deleteByContentId(@Param("contentId")Integer contentId);


}
