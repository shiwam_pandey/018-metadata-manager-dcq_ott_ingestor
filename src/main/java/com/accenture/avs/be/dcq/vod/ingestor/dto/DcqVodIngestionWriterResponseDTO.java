package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

/**
 * @author karthik.vadla
 *
 */
public class DcqVodIngestionWriterResponseDTO implements Serializable{
	private static final long serialVersionUID = -460053666626358917L;
	private Set<Integer> publishedIds;
	private Map<Integer,String> failedIdsMap;
	
	
	
	public Set<Integer> getPublishedIds() {
		return publishedIds;
	}
	public void setPublishedIds(Set<Integer> publishedIds) {
		this.publishedIds = publishedIds;
	}
	public Map<Integer, String> getFailedIdsMap() {
		return failedIdsMap;
	}
	public void setFailedIdsMap(Map<Integer, String> failedIdsMap) {
		this.failedIdsMap = failedIdsMap;
	}
	
}
