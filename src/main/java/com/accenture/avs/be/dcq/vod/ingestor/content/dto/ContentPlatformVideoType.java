/**
 * 
 */
package com.accenture.avs.be.dcq.vod.ingestor.content.dto;

/**
 * @author hussain.ali.asgar.d
 * 
 */
public class ContentPlatformVideoType {

	private String platform;
	private String videoType;

	/**
	 * @return the platform
	 */
	public String getPlatform() {
		return platform;
	}

	/**
	 * @param platform the platform to set
	 */
	public void setPlatform(String platform) {
		this.platform = platform;
	}

	/**
	 * @return the videoType
	 */
	public String getVideoType() {
		return videoType;
	}

	/**
	 * @param videoType the videoType to set
	 */
	public void setVideoType(String videoType) {
		this.videoType = videoType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((platform == null) ? 0 : platform.hashCode());
		result = prime * result + ((videoType == null) ? 0 : videoType.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		boolean flag = true;
		if (this == obj) {
			flag = true;
		}
		if (obj == null) {
			flag = false;
		} else {
			if (getClass() == obj.getClass()) {
				ContentPlatformVideoType other = (ContentPlatformVideoType) obj;
				if (platform == null) {
					if (other.platform != null) {
						flag = false;
					}
				} else if (!platform.equals(other.platform)) {
					flag = false;
				}
				if (videoType == null) {
					if (other.videoType != null) {
						flag = false;
					}
				} else if (!videoType.equals(other.videoType)) {
					flag = false;
				}
			} else {
				flag = false;
			}
		}
		return flag;
	}

}
