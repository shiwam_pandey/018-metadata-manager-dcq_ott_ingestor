package com.accenture.avs.be.dcq.vod.ingestor.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.ws.WebServiceException;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accenture.avs.be.dcq.vod.ingestor.dto.DeviceTypeDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.DeviceTypeListDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.DeviceTypeResponseDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.ResponseDeviceType;
import com.accenture.avs.be.dcq.vod.ingestor.service.DeviceService;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorRestUrls;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.Validator;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ConfigurationException;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.commons.lib.LoggerWrapper.OtherSystemCallType;
import com.accenture.avs.commons.lib.httpclient.HttpClientAdapter;
import com.accenture.avs.commons.lib.httpclient.HttpClientAdapter.HttpClientAdapterConfiguration;

/**
 * @author karthik.vadla
 *
 */
@Service
public class DeviceServiceImpl implements DeviceService{

	private static final LoggerWrapper log = new LoggerWrapper(DeviceServiceImpl.class);
	
	@Autowired
private	DcqVodIngestorRestUrls dcqVodIngestorRestUrls;
	
	/* (non-Javadoc)
	 * @see com.accenture.avs.be.dcq.vod.ingestor.service.DeviceService#getDeviceTypes(com.accenture.avs.be.framework.cache.Configuration)
	 */
	@Override
	public GenericResponse getDeviceTypes(String startIndex, String maxResults, String sortBy, String sortOrder, Configuration config)
			throws ApplicationException, ConfigurationException, Exception {
		GenericResponse genericResponse = new GenericResponse();
		
		DeviceTypeListDTO response = getDeviceTypeList(config);
		
		List<DeviceTypeDTO> subList = new ArrayList<>();
		
		Validator.validateIntNotNegativeParameter(DcqVodIngestorConstants.START_INDEX, startIndex, config);
		Validator.validateIntNotNegativeParameter(DcqVodIngestorConstants.MAX_RESULTS, maxResults, config);
	
		int from = StringUtils.isBlank(startIndex)? 0 : Integer.parseInt(startIndex);
		int size = StringUtils.isBlank(maxResults)? 50 : Integer.parseInt(maxResults) ;
		if(from + size > response.getDeviceType().size()) {
			size = response.getDeviceType().size();
		}else {
			size = from + size;
		}
		
		if(size>50) {
			size=50;
		}
		
		subList = response.getDeviceType().subList(from, size);
	    
		DeviceTypeResponseDTO deviceTypeResponse = new DeviceTypeResponseDTO();
		deviceTypeResponse.setTotalResults(response.getDeviceType().size());
		//List<DeviceTypeDTO> deviceTypes = new ArrayList<>();
		deviceTypeResponse.setDeviceTypes(subList);
		genericResponse.setResultCode(DcqVodIngestorConstants.ACN_200);
		genericResponse.setResultDescription(DcqVodIngestorConstants.OK);
		genericResponse.setResultObj(deviceTypeResponse);
		return genericResponse;
	}

	/* (non-Javadoc)
	 * @see com.accenture.avs.be.dcq.vod.ingestor.service.DeviceService#getDeviceTypesForLegacy(com.accenture.avs.be.framework.cache.Configuration)
	 */
	@Override
	public ResponseDeviceType getDeviceTypesForLegacy(Configuration config)
			throws ApplicationException, ConfigurationException, Exception {
		ResponseDeviceType deviceTypeResponse = new ResponseDeviceType();
		
		DeviceTypeListDTO response = getDeviceTypeList(config);
		deviceTypeResponse.setErrorCode("");
		deviceTypeResponse.setErrorDescription("");
		deviceTypeResponse.setResultCode(DcqVodIngestorConstants.OK);
		deviceTypeResponse.setResultObj(response);
		
		return deviceTypeResponse;
	}
	
	/**
	 * @param config
	 * @return
	 * @throws Exception
	 */
	private DeviceTypeListDTO getDeviceTypeList(Configuration config) throws Exception {
		long startTime = new Date().getTime();
		String resultCode= "";
		String resultDesc = "";
		String deviceTypeUrl = dcqVodIngestorRestUrls.SDP_DEVICE_TYPES_URL;
		DeviceTypeListDTO deviceTypeListResponse = null;
		boolean finalLogResponse = false;
		try{
		ObjectMapper mapper = new ObjectMapper();
		 Map<String, String> headerParamsMap = new HashMap<String, String>();
		 headerParamsMap.put("tenantName", "tenant_1");
		 Map<String, String> paramsMap = new HashMap<String, String>();
		
		HttpClientAdapterConfiguration httpConfig = DcqVodIngestorUtils.getHttpClientAdapterConfiguration("tenantDefault", config);
		String response = HttpClientAdapter.doGet(httpConfig, deviceTypeUrl, paramsMap, null,headerParamsMap);
		
log.logMessage("getDeviceTypeList response : {}",response);
		
		JsonNode jsonNode = mapper.readTree(response);
		deviceTypeListResponse = mapper.readValue(jsonNode.findValue("deviceTypeList"), DeviceTypeListDTO.class);
		
		resultCode = mapper.readValue(jsonNode.findValue("resultCode"), String.class);
		resultDesc = mapper.readValue(jsonNode.findValue("errorDescription"), String.class);
		
		
		log.logCallToOtherSystemEnd(DcqVodIngestorConstants.SDP, "getDeviceTypeList", "", "OK", resultCode, resultDesc,
				System.currentTimeMillis() - startTime, OtherSystemCallType.INTERNAL);
		
		}catch(WebServiceException e){
			log.logError(e,"Encountered error opening connection to sdp");
			finalLogResponse = true;
			throw new ApplicationException(MessageKeys.ERROR_MS_ACN_916_SDP_NOT_AVAILABLE);
		}
		catch(Exception e){
			finalLogResponse = true;
			log.logError(e,"Encountered error opening connection to sdp");
			throw new ApplicationException(MessageKeys.ERROR_MS_ACN_916_SDP_NOT_AVAILABLE);
		}
		finally{
			if(finalLogResponse) {
				log.logCallToOtherSystemEnd(DcqVodIngestorConstants.SDP,"getDeviceTypeList", "",
						"KO", "", "", System.currentTimeMillis() - startTime, OtherSystemCallType.INTERNAL);}
		}
		return deviceTypeListResponse;
	}


	
}
