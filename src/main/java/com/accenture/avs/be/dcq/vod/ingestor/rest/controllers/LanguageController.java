package com.accenture.avs.be.dcq.vod.ingestor.rest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.avs.be.dcq.vod.ingestor.dto.Response;
import com.accenture.avs.be.dcq.vod.ingestor.service.LanguageService;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.framework.annotation.AvsRestController;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.exception.ApplicationException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@AvsRestController
@Api(tags="Metadata Language Services",value = "Language activation API's", description = "API's pertaining to activate,deactivate metadata language")
public class LanguageController {

	@Autowired
private	LanguageService languageService;

	
	/**
	 * @param langCode
	 * @return
	 * @throws ApplicationException
	 */
	@Deprecated
	@RequestMapping(value = "/activationLanguage", method = RequestMethod.PUT)
	@ApiOperation(value="activateLanguage (For backward compatibility)",notes = "Please find the page for the IFA: https://fleet.alm.accenture.com/avswiki/display/AVSM3DOC/.DCQ+VOD+Ingestor-XML+v6.5. \n To activate metadata language by languageCode.")
	@ApiImplicitParams({
		@ApiImplicitParam(name="langCode", value="language ISO code", dataType="string")})
	@ApiResponses({
		@ApiResponse(code=400, message="ERR001-VALIDATION: Missing Parameter"),
		@ApiResponse(code=500, message="ERR004-GENERIC: GENERIC ERROR")
	})
	public @ResponseBody ResponseEntity<Response> setLanguageActive(@RequestParam(required = false) String langCode) throws ApplicationException {
		Response response = new Response();
		GenericResponse langResp=new GenericResponse();
		try {
			langResp = languageService.activateOrDeactivateLanguage(DcqVodIngestorConstants.LANGUAGECONSTANTS.PUT_IS_ACTIVE, langCode);
			response.setResultObj(langResp.getResultObj());
		}catch (ApplicationException ae) {
			if(ae.getExceptionId().contains("3019") && ae.getParameters().getParameters()[0].contains("does not exists")) {
				response.setResultCode(DcqVodIngestorConstants.KO);
				response.setErrorCode("ERR011");
				response.setErrorDescription("Language does not exist on AVS database");
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			}
			else if(ae.getExceptionId().contains("3019")) {
					response.setResultCode(DcqVodIngestorConstants.KO);
					response.setErrorCode("ERR010");
					response.setErrorDescription("Language Code not found on ISO codes");
					return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
				}else if(ae.getExceptionId().contains("3000")) { {
					response.setResultCode(DcqVodIngestorConstants.KO);
					response.setErrorCode("ERR001-VALIDATION");
					response.setErrorDescription("Invalid Parameter "+ ae.getParameters().getParameters()[0]);
					return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
				}
				}
		}
		catch (Exception e) {
			response.setResultCode(DcqVodIngestorConstants.KO);
			response.setErrorCode("ERR004-GENERIC");
			response.setErrorDescription(e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			
			}
		return ResponseEntity.ok(response);
	}
	
	//deactive the EMF isActive FLag 
	/**
	 * @param langCode
	 * @return
	 * @throws ApplicationException
	 */
	@Deprecated
	@RequestMapping(value = "/activationLanguage", method = RequestMethod.DELETE)
	@ApiOperation(value="deactivateLanguage (For backward compatibility)",notes = "Please find the page for the IFA: https://fleet.alm.accenture.com/avswiki/display/AVSM3DOC/.DCQ+VOD+Ingestor-XML+v6.5. \n To deactivate metadata language by languageCode.")
	@ApiImplicitParams({
		@ApiImplicitParam(name="langCode", value="language ISO code", dataType="string")})
	@ApiResponses({
		@ApiResponse(code=400, message="ERR001-VALIDATION: Missing Parameter"),
		@ApiResponse(code=500, message="ERR004-GENERIC: GENERIC ERROR")
	})
	public @ResponseBody ResponseEntity<Response> setLanguageDeactive(@RequestParam(required = false) String langCode) throws ApplicationException{
		Response response = new Response();
		GenericResponse langResp=new GenericResponse();
		try {
			langResp = languageService.activateOrDeactivateLanguage(DcqVodIngestorConstants.LANGUAGECONSTANTS.DELETE_IS_ACTIVE, langCode);
			response.setResultObj(langResp.getResultObj());
		}catch (ApplicationException ae) {
			if(ae.getExceptionId().contains("3019") && ae.getParameters().getParameters()[0].contains("does not exists")) {
				response.setResultCode(DcqVodIngestorConstants.KO);
				response.setErrorCode("ERR011");
				response.setErrorDescription("Language does not exist on AVS database");
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			}
			else if(ae.getExceptionId().contains("3019")) {
					response.setResultCode(DcqVodIngestorConstants.KO);
					response.setErrorCode("ERR010");
					response.setErrorDescription("Language Code not found on ISO codes");
					return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
				}else if(ae.getExceptionId().contains("3000")) { {
					response.setResultCode(DcqVodIngestorConstants.KO);
					response.setErrorCode("ERR001-VALIDATION");
					response.setErrorDescription("Invalid Parameter "+ ae.getParameters().getParameters()[0]);
					return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
				}
				}
		}
		catch (Exception e) {
			response.setResultCode(DcqVodIngestorConstants.KO);
			response.setErrorCode("ERR004-GENERIC");
			response.setErrorDescription(e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			
			}
		return ResponseEntity.ok(response);
	}
	
	
	
}
