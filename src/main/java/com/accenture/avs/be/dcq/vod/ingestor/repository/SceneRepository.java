package com.accenture.avs.be.dcq.vod.ingestor.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.avs.persistence.technicalcatalogue.SceneEntity;
/**
 * @author karthik.vadla
 *
 */
@Repository
public interface SceneRepository extends JpaRepository<SceneEntity,Integer>{

	List<Long> retrieveSceneIdsBycontentId(@Param("contentId")Long contentId);

	@Modifying
	@Transactional
	void deleteByContentId(@Param("contentId")Long contentId);

	List<SceneEntity> retrieveBySceneIdNotContentId(@Param("sceneId")Long sceneId, @Param("contentId")Long contentId);

}
