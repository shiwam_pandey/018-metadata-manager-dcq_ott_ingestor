package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.io.Serializable;

/**
 * @author karthik.vadla
 *
 */
public class ESVLCContent implements Serializable {

	private static final long serialVersionUID = 1L;	
	
	private VlcContent vlcContent;
	
	

	public VlcContent getVlcContent() {
		return vlcContent;
	}

	public void setVlcContent(VlcContent vlcContent) {
		this.vlcContent = vlcContent;
	}
	
	public static class VlcContent implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		private Long contentId;
		private Metadata metadata;
		private Channel channel;
		private String platformName;
		
		
		public String getPlatformName() {
			return platformName;
		}
		public void setPlatformName(String platformName) {
			this.platformName = platformName;
		}
		
		/**
		 * @return the contentId
		 */
		public Long getContentId() {
			return contentId;
		}
		/**
		 * @param contentId the contentId to set
		 */
		public void setContentId(Long contentId) {
			this.contentId = contentId;
		}
		public Metadata getMetadata() {
			return metadata;
		}
		public void setMetadata(Metadata metadata) {
			this.metadata = metadata;
		}
		public Channel getChannel() {
			return channel;
		}
		public void setChannel(Channel channel) {
			this.channel = channel;
		}
		
		
	}
	
	public static class Metadata implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		private Long contentId;
		private Long startTime;
		private Long endTime;
		private Long playlistPublishedDate;
		private String videoType;
		private Long cpId;
		private Long duration;
		private Boolean isEncrypted;
		private Boolean isGeoBlocked;
		private String pcLevel;
		private String contentTitle;
		private String objectType;
		private String objectSubType;
		private String titleBrief;
		
		public Long getContentId() {
			return contentId;
		}
		public void setContentId(Long contentId) {
			this.contentId = contentId;
		}
		public Long getStartTime() {
			return startTime;
		}
		public void setStartTime(Long startTime) {
			this.startTime = startTime;
		}
		public Long getEndTime() {
			return endTime;
		}
		public void setEndTime(Long endTime) {
			this.endTime = endTime;
		}
		public Long getPlaylistPublishedDate() {
			return playlistPublishedDate;
		}
		public void setPlaylistPublishedDate(Long playlistPublishedDate) {
			this.playlistPublishedDate = playlistPublishedDate;
		}
		public String getVideoType() {
			return videoType;
		}
		public void setVideoType(String videoType) {
			this.videoType = videoType;
		}
		public Long getCpId() {
			return cpId;
		}
		public void setCpId(Long cpId) {
			this.cpId = cpId;
		}
		public Long getDuration() {
			return duration;
		}
		public void setDuration(Long duration) {
			this.duration = duration;
		}
		public Boolean getIsEncrypted() {
			return isEncrypted;
		}
		public void setIsEncrypted(Boolean isEncrypted) {
			this.isEncrypted = isEncrypted;
		}
		public Boolean getIsGeoBlocked() {
			return isGeoBlocked;
		}
		public void setIsGeoBlocked(Boolean isGeoBlocked) {
			this.isGeoBlocked = isGeoBlocked;
		}
		public String getPcLevel() {
			return pcLevel;
		}
		public void setPcLevel(String pcLevel) {
			this.pcLevel = pcLevel;
		}
		public String getContentTitle() {
			return contentTitle;
		}
		public void setContentTitle(String contentTitle) {
			this.contentTitle = contentTitle;
		}
		public String getObjectType() {
			return objectType;
		}
		public void setObjectType(String objectType) {
			this.objectType = objectType;
		}
		public String getObjectSubType() {
			return objectSubType;
		}
		public void setObjectSubType(String objectSubType) {
			this.objectSubType = objectSubType;
		}
		public String getTitleBrief() {
			return titleBrief;
		}
		public void setTitleBrief(String titleBrief) {
			this.titleBrief = titleBrief;
		}
		
	}
	
	public static class Channel implements Serializable {

		private static final long serialVersionUID = 1L;
		
		private Long channelId;
		private String channelName;
		private String advTags;
		private String externalId;
		
		
		public Long getChannelId() {
			return channelId;
			
		}
		public void setChannelId(Long channelId) {
			this.channelId = channelId;
		}
		public String getChannelName() {
			return channelName;
		}
		public void setChannelName(String channelName) {
			this.channelName = channelName;
		}
		public String getAdvTags() {
			return advTags;
		}
		public void setAdvTags(String advTags) {
			this.advTags = advTags;
		}
		public String getExternalId() {
			return externalId;
		}
		public void setExternalId(String externalId) {
			this.externalId = externalId;
		}
		
	}
	
}
