package com.accenture.avs.be.dcq.vod.ingestor.v1.service.impl;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.ThreadContext;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.accenture.avs.be.dcq.vod.ingestor.dto.CategoryCMS;
import com.accenture.avs.be.dcq.vod.ingestor.manager.CategoryMetadataManager;
import com.accenture.avs.be.dcq.vod.ingestor.manager.ContentMetadataManager;
import com.accenture.avs.be.dcq.vod.ingestor.manager.impl.DcqVodIngestorPropertiesManager;
import com.accenture.avs.be.dcq.vod.ingestor.processor.DcqVodIngestionPreProcessor;
import com.accenture.avs.be.dcq.vod.ingestor.repository.CategoryCMSRepository;
import com.accenture.avs.be.dcq.vod.ingestor.service.impl.CategoryServiceImpl;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.JsonUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.Validator;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.contentcategory.asset.Category;
import com.accenture.avs.be.dcq.vod.ingestor.v1.manager.CategoryV1Manager;
import com.accenture.avs.be.dcq.vod.ingestor.v1.service.CategoryV1Service;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.cache.Configurator;
import com.accenture.avs.be.framework.configuration.CacheConstants;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.be.framework.exception.ConfigurationException;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.commons.lib.support.CommonConstants;
import com.accenture.avs.persistence.technicalcatalogue.CategoryCMSEntity;
import com.fasterxml.jackson.core.JsonProcessingException;

@Service
public class CategoryV1ServiceImpl implements CategoryV1Service {

	private static final LoggerWrapper log = new LoggerWrapper(CategoryServiceImpl.class);

	@Autowired
	private CategoryV1Manager categoryManager;

	@Autowired
	private DcqVodIngestionPreProcessor dcqVodIngestionPreProcessor;

	@Autowired
	private DcqVodIngestorPropertiesManager dcqVodIngestorPropertiesManager;

	@Autowired
	private Configurator configurator;

	@Autowired
	private CategoryCMSRepository categoryCMSRepository;

	@Autowired
	private CategoryMetadataManager categoryMetadataManager;
	
	@Autowired
	private ContentMetadataManager contentMetadataManager;
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.accenture.avs.be.dcq.vod.ingestor.service.CategoryService#
	 * unpublishCategory(java.lang.String,
	 * com.accenture.avs.be.framework.cache.Configuration)
	 */
	@Override
	public GenericResponse unpublishCategory(String categoryId, Configuration config)
			throws ApplicationException, ConfigurationException, JsonParseException, JsonMappingException, IOException {
		GenericResponse genericResponse = null;
		try {

			Validator.validateIntNotNegativeParameter("categoryId", categoryId, config);
			List<Integer> contentIds = categoryManager.unpublishCategory(Integer.parseInt(categoryId), config);
			List<Integer> categoryIds = new ArrayList<>();
			categoryIds.add(Integer.parseInt(categoryId));

			dcqVodIngestionPreProcessor.refreshCachesCreateIndexes(DcqVodIngestorConstants.CATEGORY, "", "", config);
			categoryMetadataManager.processAndWriteCategories(categoryIds, DcqVodIngestorUtils.getTransactionNumber(),
					DcqVodIngestorUtils.currentTime(), "", "", config);

			if (contentIds != null && !contentIds.isEmpty()) {
				dcqVodIngestionPreProcessor.refreshCachesCreateIndexes(DcqVodIngestorConstants.CONTENT, "", "", config);
				contentMetadataManager.processandWriteContents(contentIds, DcqVodIngestorUtils.getTransactionNumber(),
						DcqVodIngestorUtils.currentTime(), "", "", config);
			}
		} catch (ApplicationException ae) {
			throw ae;
		} catch (Exception e) {
			throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR);
		}
		genericResponse = new GenericResponse(DcqVodIngestorConstants.ACN_200, DcqVodIngestorConstants.OK, "");
		return genericResponse;
	}
	
	@Override
	public GenericResponse upsertCategory(Category categoryJsonRequest, Configuration config) throws JsonParseException, JsonMappingException, ConfigurationException, IOException, ApplicationException {
		GenericResponse genericResponse = null;
		List<CategoryCMS> categoryCMSDtos = null;

		try {
			if (Objects.nonNull(categoryJsonRequest)) {
				categoryCMSDtos = readAllLines(categoryJsonRequest);
				insertCategoriesIdPath(categoryCMSDtos);
				categoryManager.ingestCategory(categoryCMSDtos);
				log.logMessage("Successfully read Category data");

				// save data to ES

			} else {
				throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
						new NestedParameters("category input data"));
			}

			genericResponse = new GenericResponse(DcqVodIngestorConstants.ACN_200, DcqVodIngestorConstants.OK,
					DcqVodIngestorConstants.EMPTY);

		} catch (DataIntegrityViolationException cex) {
			//cex.printStackTrace();
			log.logError(cex);
			throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters("channelId or contentId or videoType"));
		} catch (ApplicationException ae) {
			log.logError(ae);
			throw ae;

		}
		return genericResponse;
	}

	public void insertCategoriesIdPath(List<CategoryCMS> categories) {

		Long categoryId = 0L;
		Long parentCategoryId = 0L;
		Long pParentCategoryId = 0L;
		Long pCategoryId = 0L;
		String idRoot;
		String externalIdRoot;
		StringBuffer externalIdRootBuf; // = new StringBuffer(); // fix for ADT
										// 15096
		StringBuffer rootToLeafIdPathBuf; // = new StringBuffer();
		StringBuffer rootToLeafExternalIdPathBuf; // = new StringBuffer();

		for (int y = 0; y < categories.size(); y++) {
			// fix for ADT 15096 : start
			externalIdRootBuf = new StringBuffer();
			rootToLeafIdPathBuf = new StringBuffer();
			rootToLeafExternalIdPathBuf = new StringBuffer();
			// fix for ADT 15096 : end
			CategoryCMS category = categories.get(y);
			if (Objects.nonNull(category)) {
				categoryId = category.getCategoryId().longValue();
				parentCategoryId = category.getParentCategoryId().longValue();
				idRoot = categoryId.toString();
				externalIdRoot = category.getExternalId();
				if (categoryId.equals(parentCategoryId)) {
					log.logMessage("----root id is == " + idRoot + "   ----");
					log.logMessage("----External root id is == " + externalIdRoot + "   ----");
					category.setIdPath(idRoot);
					category.setExternalidPath(externalIdRoot);
				} else {
					pParentCategoryId = parentCategoryId;
					externalIdRootBuf.append(externalIdRoot);
					do {
						idRoot += ";" + pParentCategoryId;
						CategoryCMSEntity cat = categoryCMSRepository.findByCategoryId(pParentCategoryId.intValue());
						if (cat != null) {
							pParentCategoryId = cat.getParent_category_id().longValue();
							pCategoryId = cat.getCategoryId().longValue();
							externalIdRootBuf.append(";" + cat.getExternalId());
						} else {
							break;
						}
					} while (!pCategoryId.equals(pParentCategoryId));

					/*
					 * logger.info("----root id is == "+idRoot+"   ----");
					 * logger.info("----External root id is == "
					 * +externalIdRoot+"   ----");
					 */
					// to correctly valorized starting from root category to
					// leaf category e.g ROOT;NODE;NODE;LEAF for ID path
					externalIdRoot = externalIdRootBuf.toString();
					String[] words = idRoot.split(";");
					String rootToLeafIdPath = "";

					rootToLeafIdPathBuf.append(rootToLeafIdPath);
					for (int i = 0; i < words.length; i++) {
						String word = words[words.length - 1 - i];
						rootToLeafIdPathBuf.append(word + ";");
						// rootToLeafIdPath+=word+";";
					}
					rootToLeafIdPath = rootToLeafIdPathBuf.toString();
					log.logMessage("Root to leaf id path is " + rootToLeafIdPath);
					// to correctly valorized starting from root category to
					// leaf category e.g ROOT;NODE;NODE;LEAF for External ID
					// path
					words = externalIdRoot.split(";");
					String rootToLeafExternalIdPath = "";
					rootToLeafExternalIdPathBuf.append(rootToLeafExternalIdPath);
					for (int i = 0; i < words.length; i++) {
						String word = words[words.length - 1 - i];
						rootToLeafExternalIdPathBuf.append(word + ";");
						// rootToLeafExternalIdPath+=word+";";
					}
					rootToLeafExternalIdPath = rootToLeafExternalIdPathBuf.toString();
					log.logMessage("Root to leaf External id path is " + rootToLeafExternalIdPath);
					category.setIdPath(rootToLeafIdPath.substring(0, rootToLeafIdPath.length() - 1));
					category.setExternalidPath(
							rootToLeafExternalIdPath.substring(0, rootToLeafExternalIdPath.length() - 1));
					externalIdRoot = null;
					idRoot = null;
				}
			}
		}
	}

	public List<CategoryCMS> readAllLines(Category categoryJsonRequest) throws ApplicationException {
		List<CategoryCMS> categoryCMSDTOs = new ArrayList<CategoryCMS>();
		log.logMessage("Reading input lines and convert to VLC data");
		try {
			Configuration config = configurator.getConfiguration(ThreadContext.get(CommonConstants.LogParamsL4j2.TN));
			CategoryCMS categoryCMS = new CategoryCMS();
			categoryCMS.setCategoryId(categoryJsonRequest.getCategoryId());
			if (categoryCMS.getCategoryId() == null) {
				throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
						new NestedParameters(DcqVodIngestorConstants.CATEGORYCONSTANTSV1.CATEGORY_ID));
			}
			categoryCMS.setParentCategoryId(categoryJsonRequest.getParentCategoryId());
			if (categoryCMS.getParentCategoryId() == null) {
				throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
						new NestedParameters(DcqVodIngestorConstants.CATEGORYCONSTANTSV1.PARENT_CATEGORY_ID));
			}
			categoryCMS.setName(categoryJsonRequest.getName());
			if (StringUtils.isEmpty(categoryCMS.getName())) {
				throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
						new NestedParameters(DcqVodIngestorConstants.CATEGORYCONSTANTSV1.NAME));
			}
			Validator.checkParameterNameLength(DcqVodIngestorConstants.CATEGORYCONSTANTSV1.NAME, categoryCMS.getName(),
					DcqVodIngestorConstants.CATEGORYCONSTANTS.MAX_LENGTH_NAME, config);
			if (StringUtils.isEmpty(categoryJsonRequest.getType())) {
				throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
						new NestedParameters(DcqVodIngestorConstants.CATEGORYCONSTANTSV1.TYPE));
			}
			categoryCMS.setCategoryType(categoryJsonRequest.getType());
			Validator.checkParameterValidityRange("type", DcqVodIngestorConstants.ALLOWED_CATEGORY_TYPES,
					categoryCMS.getCategoryType(), config);
			categoryCMS.setIsVisible(categoryJsonRequest.getIsVisible());
			if (StringUtils.isEmpty(categoryCMS.getIsVisible())) {
				throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
						new NestedParameters(DcqVodIngestorConstants.CATEGORYCONSTANTSV1.IS_VISIBLE));
			}
			Validator.checkParameterValidityRange("IsVisible", DcqVodIngestorConstants.ALLOWED_FLAGS,
					categoryCMS.getIsVisible(), config);

			categoryCMS.setChannelCategory(categoryJsonRequest.getCatalogueIdentifier());
			
			if (Objects.nonNull(categoryJsonRequest.getCatalogueIdentifier())
					&& !config.getPlatforms().copyCache().containsKey(categoryJsonRequest.getCatalogueIdentifier())) {
				throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
						new NestedParameters(DcqVodIngestorConstants.CATEGORYCONSTANTSV1.CATALOG_IDENTIFIER));
			}
			/*
			 * if (categoryCMS.getChannelCategory() == null) { throw new
			 * ApplicationException( CacheConstants.MessageKeys.
			 * ERROR_BE_ACTION_3000_MISSING_PARAMETER, new
			 * NestedParameters(DcqVodIngestorConstants.CATEGORYCONSTANTSV1.
			 * CATALOG_IDENTIFIER)); }
			 */
			if (categoryJsonRequest.getOrderId() == null) {
				throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
						new NestedParameters(DcqVodIngestorConstants.CATEGORYCONSTANTSV1.ORDER_ID));
			}
			categoryCMS.setOrderId(categoryJsonRequest.getOrderId().longValue());
			categoryCMS.setAdult(categoryJsonRequest.getIsAdult());
			if (StringUtils.isEmpty(categoryCMS.getAdult())) {
				throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
						new NestedParameters(DcqVodIngestorConstants.CATEGORYCONSTANTSV1.ADULT));
			}
			Validator.checkParameterValidityRange("isAdult", DcqVodIngestorConstants.ALLOWED_FLAGS,
					categoryCMS.getAdult(), config);
			
			categoryCMS.setExternalId(categoryJsonRequest.getExternalId());
			if (StringUtils.isEmpty(categoryCMS.getExternalId())) {
				throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
						new NestedParameters(DcqVodIngestorConstants.CATEGORYCONSTANTSV1.EXTERNAL_ID));
			}
			categoryCMS.setContentOrderType(categoryJsonRequest.getContentOrderType());
			if (Objects.nonNull(categoryCMS.getContentOrderType())) {
				Validator.checkParameterValidityRange("contentOrderType",
						DcqVodIngestorConstants.ALLOWED_CONTENTORDERTYPES, categoryCMS.getContentOrderType(), config);
			}
			
			if(Objects.nonNull(categoryJsonRequest.getContentId())){
				categoryCMS.setContentId(categoryJsonRequest.getContentId().longValue());	
			}

			categoryCMS.setAsNew("N");
			getSourcefile(categoryCMS, categoryJsonRequest);
			
			//ES Validation
			
			if(Objects.nonNull(categoryJsonRequest.getIsProtected())){
				Validator.checkParameterValidityRange("isProtected", DcqVodIngestorConstants.ALLOWED_FLAGS,
						categoryJsonRequest.getIsProtected(), config);	
			}

			if(ArrayUtils.isNotEmpty(categoryJsonRequest.getPcExtendedRatings())){
				for (String PcExtendedRating : categoryJsonRequest.getPcExtendedRatings()) {
					if (Objects.isNull(config.getPcExtendedRatings().getKey(PcExtendedRating))) {
						throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
								new NestedParameters(DcqVodIngestorConstants.CATEGORYCONSTANTSV1.PC_EXTENDED_RATINGS));
					}
				}
			}
			
			if (Objects.nonNull(categoryJsonRequest.getPcLevel())
					&& Objects.isNull((config.getPcLevels().getKey(categoryJsonRequest.getPcLevel())))) {
					throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters(DcqVodIngestorConstants.CATEGORYCONSTANTSV1.PCLEVEL));
			}

			categoryCMSDTOs.add(categoryCMS);

		} catch (ApplicationException ap) {
			log.logMessage(ap.getMessage());
			throw ap;
		} catch (Exception e) {
			log.logMessage(e.getMessage());
			throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR);
		}
		return categoryCMSDTOs;
	}

	private void getSourcefile(CategoryCMS categoryCMS, Category categoryJsonRequest)
			throws JsonParseException, JsonMappingException, IOException {
		try {
			categoryCMS.setSourceFile(
					JsonUtils.writeAsJsonStringWithoutNull(categoryJsonRequest).getBytes(Charset.forName("UTF-8")));
		} catch (JsonProcessingException e) {
			log.logMessage("Error in setting source file :{}", e);
			throw e;
		}

	}
}
