
package com.accenture.avs.be.dcq.vod.ingestor.content.dto;

import java.io.Serializable;

/**
 * @author harini.nandyala
 *
 */
public class Properties implements Serializable {

	private static final long serialVersionUID = 1L;
	private Property[] properties;

	public Property[] getProperties() {
		return properties;
	}

	public void setProperties(Property[] properties) {
		this.properties = properties;
	}

	/*
	 * public List<Property> getProperty() { return properties; } public void
	 * setProperty(List<Property> property) { this.properties = property; }
	 */

}
