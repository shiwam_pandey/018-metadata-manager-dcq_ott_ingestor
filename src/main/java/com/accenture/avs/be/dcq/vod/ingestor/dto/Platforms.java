package com.accenture.avs.be.dcq.vod.ingestor.dto;

import io.swagger.annotations.ApiModelProperty;

public class Platforms {

	@ApiModelProperty(required = true, value = "Platform Device. Possible values PCTV, OTTSTB, IPAD, ANDROID, XBOX",example="PCTV") 
    private String name;
	@ApiModelProperty(required = false, value = "used to recognize if a channel must be visible or not . Possible values Y or N",example="Y")
    private String isPublished;
	@ApiModelProperty(required = false, value = "streaming URL of the channel . Max length 1000",example="VideoURL")
    private String videoURL;
	@ApiModelProperty(required = false, value = "streaming URL of the trailer of the channel . Max length 1000",example="TrailerURL")
    private String trailerURL;
	@ApiModelProperty(required = true, value = "small logo URL of the channel . Max length 100",example="LogoSmall")
    private String logoSmall;
	@ApiModelProperty(required = true, value = "medium logo URL of the channel . Max length 100",example="LogoMedium")
    private String logoMedium;
	@ApiModelProperty(required = true, value = "big logo URL of the channel . Max length 100",example="LogoBig")
    private String logoBig;
	@ApiModelProperty(required = true, value = "Identify if a channel is online (Extended) or not (Standard) . Possible values Standard or Extended",example="Standard")
    private String group;
	@ApiModelProperty(required = false, value = "extendedMetadata. Contains a json object with custom fields for customization purpose.",example="{\"plot\":\"On a far planet...\",\"actorsListDetail\":[{\"Name\":\"Sam Worthington\",\"BirthDate\":\"03/02/1981\",\"BirthCity\":\"New York\"},{\"Name\":\"Sigourney Weaver\",\"BirthDate\":\"04/03/1955\",\"BirthCity\":\"New York\"}]}")
    private String extendedMetadata;
    
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getIsPublished() {
		return isPublished;
	}
	public void setIsPublished(String isPublished) {
		this.isPublished = isPublished;
	}
	public String getVideoURL() {
		return videoURL;
	}
	public void setVideoURL(String videoURL) {
		this.videoURL = videoURL;
	}
	public String getTrailerURL() {
		return trailerURL;
	}
	public void setTrailerURL(String trailerURL) {
		this.trailerURL = trailerURL;
	}
	public String getLogoSmall() {
		return logoSmall;
	}
	public void setLogoSmall(String logoSmall) {
		this.logoSmall = logoSmall;
	}
	public String getLogoMedium() {
		return logoMedium;
	}
	public void setLogoMedium(String logoMedium) {
		this.logoMedium = logoMedium;
	}
	public String getLogoBig() {
		return logoBig;
	}
	public void setLogoBig(String logoBig) {
		this.logoBig = logoBig;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getExtendedMetadata() {
		return extendedMetadata;
	}
	public void setExtendedMetadata(String extendedMetadata) {
		this.extendedMetadata = extendedMetadata;
	}
    
    
    
    	
   
}
