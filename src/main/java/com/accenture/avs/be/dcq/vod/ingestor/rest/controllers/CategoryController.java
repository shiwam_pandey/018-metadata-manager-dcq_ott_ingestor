package com.accenture.avs.be.dcq.vod.ingestor.rest.controllers;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.avs.be.dcq.vod.ingestor.dto.Response;
import com.accenture.avs.be.dcq.vod.ingestor.service.CategoryService;
import com.accenture.avs.be.framework.annotation.AvsRestController;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.cache.Configurator;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.commons.lib.support.CommonConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@AvsRestController
@Api(tags = "Category Services", value = "API’s pertaining to create,delete categories", description = "API's pertaining to dcqVodIngestor functional Area")
public class CategoryController {

	private static final LoggerWrapper log = new LoggerWrapper(CategoryController.class);
	
	@Autowired
    private	Configurator configurator;
	
	@Autowired
	private CategoryService categoryService;
	
	@Deprecated
	@RequestMapping(value = "/category/{categoryId}", method = RequestMethod.POST)
	@ApiOperation(value = "upsertCategory", notes = "Please find the page for the IFA: https://fleet.alm.accenture.com/avswiki/display/AVSM3DOC/.DCQ+VOD+Ingestor-XML+v6.5")
	public @ResponseBody
	ResponseEntity<GenericResponse> upsertCategory(@RequestBody String csvCategory,@PathVariable("categoryId") String categoryId,
			@RequestParam(required = false,defaultValue="true") boolean sync) throws ApplicationException {
		GenericResponse genericResponse = null;
		Response response = new Response();
		Configuration config = configurator.getConfiguration(ThreadContext.get(CommonConstants.LogParamsL4j2.TN));
		log.logStartApi();
		Long startTime = System.currentTimeMillis();
		try {
			genericResponse=categoryService.upsertCategory(csvCategory, config);
		} catch (ApplicationException ae) {
			if(ae.getExceptionId().contains("3019")||ae.getExceptionId().contains("3000")||ae.getExceptionId().contains("3089")) {
				response.setResultCode("KO");
				response.setErrorCode("ERR001-VALIDATION");
				response.setErrorDescription("Invalid Parameter "+ae.getParameters().getParameters()[0]);
			}
			else
			{
					response.setResultCode("KO");
					response.setErrorCode("ERR004-GENERIC");
					response.setErrorDescription(ae.getMessage());	
				
			}

		} catch (Exception e) {
			response.setResultCode("KO");
			response.setErrorCode("ERR004-GENERIC");
			response.setErrorDescription(e.getMessage());	
			}
		
		log.logMethodEnd(System.currentTimeMillis() - startTime);
		return ResponseEntity.ok(response);
	}

	/**
	 * @param categoryId
	 * @return
	 * @throws ApplicationException
	 */
	@Deprecated
	@RequestMapping(value = "/category/{categoryId}", method = RequestMethod.DELETE)
	@ApiOperation(value= "deleteCategory", notes = "Please find the page for the IFA: https://fleet.alm.accenture.com/avswiki/display/AVSM3DOC/.DCQ+VOD+Ingestor-XML+v6.5")
	public @ResponseBody
	ResponseEntity<GenericResponse> deleteCategory(
			@PathVariable("categoryId") String categoryId) throws ApplicationException {
		Response response = new Response();
		try {
			Configuration config = configurator.getConfiguration(ThreadContext.get(CommonConstants.LogParamsL4j2.TN));
			categoryService.unpublishCategory(categoryId, config);
		} catch (ApplicationException ae) {
			if(ae.getExceptionId().contains("3019")||ae.getExceptionId().contains("3000")) {
					response.setResultCode("KO");
					response.setErrorCode("ERR001-VALIDATION");
					response.setErrorDescription("Invalid Parameter "+ae.getParameters().getParameters()[0]);
					return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
				}
			}
		catch (Exception e) {
			response.setResultCode("KO");
			response.setErrorCode("ERR004-GENERIC");
			response.setErrorDescription(e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			
			}
		return ResponseEntity.ok(response);
	}
	
}
