package com.accenture.avs.be.dcq.vod.ingestor.processor;

import java.util.List;

import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionReaderResponseDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionWriterRequestDTO;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;

/**
 * @author karthik.vadla
 *
 */
public interface DcqVodIngestionProcessor {

	List<DcqVodIngestionWriterRequestDTO> processMetadataConstructESDocs(
			DcqVodIngestionReaderResponseDTO readerResponseDTO, String assetType, String transactionNumber, String mode,
			String indexDate, Configuration config) throws ApplicationException;

	void processSwitchAlias(Configuration config) throws ApplicationException;

	void processRollbackAlias(Configuration config) throws ApplicationException;

}
