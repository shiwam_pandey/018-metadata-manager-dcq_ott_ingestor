package com.accenture.avs.be.dcq.vod.ingestor.reader.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.dto.ChannelIdPlaylistDateDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionReaderResponseDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.VLCChannelIDPubDateDTO;
import com.accenture.avs.be.dcq.vod.ingestor.reader.DcqVodIngestionReader;
import com.accenture.avs.be.dcq.vod.ingestor.repository.DcqAssetStagingRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.DcqPlaylistStagingRepository;
import com.accenture.avs.be.dcq.vod.ingestor.utils.CacheConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.commons.lib.LoggerWrapper;

/**
 * @author karthik.vadla
 *
 */
@Component
public class DcqVodIngestionReaderImpl implements DcqVodIngestionReader {
	private static final LoggerWrapper log = new LoggerWrapper(DcqVodIngestionReaderImpl.class);

	@Autowired
private	DcqAssetStagingRepository dcqAssetStagingRepository;
	@Autowired
private	DcqPlaylistStagingRepository dcqPlaylistStagingRepository;
	@Override
	public DcqVodIngestionReaderResponseDTO retrieveAssetIdsFromStaging(String assetType, Configuration config) throws ApplicationException {
		List<Integer> assetIds = null;
		DcqVodIngestionReaderResponseDTO ingestionReaderResponseDTO = new DcqVodIngestionReaderResponseDTO();
		long startTime = System.currentTimeMillis();
		log.logMessage("Retrieve the assetIds from staging table of IngestorType : {} - Start.", assetType );
		try {

			if (assetType.equals(DcqVodIngestorConstants.CONTENT) || assetType.equals(DcqVodIngestorConstants.CATEGORY)
					|| assetType.equals(DcqVodIngestorConstants.VODCHANNEL)) {
				assetIds = dcqAssetStagingRepository.retrieveByAssetType(assetType, DcqVodIngestorConstants.READY, DcqVodIngestorConstants.IN_PROGRESS, DcqVodIngestorConstants.FAILED, Integer.parseInt(config.getConstants().getValue(CacheConstants.DCQ_VOD_INGESTOR_MAX_RETRY)));
				log.logMessage("Retrieval of the assetIds from staging table of IngestorType: {}  - Completed,took: {} ms" , assetType , (System.currentTimeMillis() - startTime)
						);
				log.logMessage("Number of assetIds from staging table to be processed of IngestorType: {} are : {}" , assetType , (assetIds == null ? 0 : assetIds.size()));
				if (null != assetIds) {
					assetIds = new ArrayList<Integer>(new LinkedHashSet<Integer>(assetIds));
					log.logMessage("Asset Ids to Process for IngestorType: {} are {} " , assetType , assetIds.toString());
				}
 
				if (null != assetIds && !assetIds.isEmpty()) {
					long startTimeforUpdate = System.currentTimeMillis();
					log.logMessage("Update the status (to INPROGRESS) assetIds in staging table of Type: {} - Start." , assetType );
					log.logMessage("TRANSACTION_START");
					dcqAssetStagingRepository.deleteDuplicateIds(assetType);
					dcqAssetStagingRepository.updateStatus(assetIds,assetType, DcqVodIngestorConstants.IN_PROGRESS);
					log.logMessage("TRANSACTION_END");
					log.logMessage("Update the status (to INPROGRESS) assetIds in staging table of Type: {} - Completed,took: {} ms" , assetType 
							, (System.currentTimeMillis() - startTimeforUpdate) );
				}
				ingestionReaderResponseDTO.setAssetIds(assetIds);
				ingestionReaderResponseDTO.setStartTime(DcqVodIngestorUtils.currentTime());
				return ingestionReaderResponseDTO;
			} else if (assetType.equals(DcqVodIngestorConstants.VLCCONTENT)) {

			}
		}  catch (Exception e) {
			log.logError(e);
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR, new NestedParameters(e.getMessage()));
		}
		ingestionReaderResponseDTO.setAssetIds(assetIds);
		return ingestionReaderResponseDTO;
	}

	@Override
	public DcqVodIngestionReaderResponseDTO retrieveChannelIdsFromPlaylistStaging(String assetType, Configuration config) throws ApplicationException {
		DcqVodIngestionReaderResponseDTO dcqVodIngestionReaderResponseDTO = null;
		long startTime = System.currentTimeMillis();
		log.logMessage("Retrieve the assetIds from Playlist Staging table of IngestorType : {} - Start." , assetType );
		try {

			List<Object[]> resultList = dcqPlaylistStagingRepository.retrieveChannelIds(DcqVodIngestorConstants.READY, DcqVodIngestorConstants.IN_PROGRESS, DcqVodIngestorConstants.FAILED, Integer.parseInt(config.getConstants().getValue(CacheConstants.DCQ_VOD_INGESTOR_MAX_RETRY)));
			log.logMessage("Retrieve the assetIds from Playlist Staging table of IngestorType: {} - Completed,took: {} ms" , assetType, (System.currentTimeMillis() - startTime)
					);

			if (null != resultList && !resultList.isEmpty()) {

				List<ChannelIdPlaylistDateDTO> channelIdPlaylistDateDTOs = new ArrayList<ChannelIdPlaylistDateDTO>();
				List<Integer> rowIds = new ArrayList<Integer>();
				dcqVodIngestionReaderResponseDTO = new DcqVodIngestionReaderResponseDTO();
				dcqVodIngestionReaderResponseDTO.setChannelIdPlaylistDateDTOList(channelIdPlaylistDateDTOs);
				Set<VLCChannelIDPubDateDTO> vLCChannelIDPubDateDTOsSet = new HashSet<VLCChannelIDPubDateDTO>();

				for (Object[] result : resultList) {
					VLCChannelIDPubDateDTO vLCChannelIDPubDateDTO = new VLCChannelIDPubDateDTO((Integer) result[0], (Date) result[1]);
					if (vLCChannelIDPubDateDTOsSet != null && !vLCChannelIDPubDateDTOsSet.contains(vLCChannelIDPubDateDTO)) {
						ChannelIdPlaylistDateDTO channelIdPlaylistDateDTO = new ChannelIdPlaylistDateDTO();
						channelIdPlaylistDateDTO.setChannelId((Integer) result[0]);
						channelIdPlaylistDateDTO.setPlaylistDate((Date) result[1]);
						channelIdPlaylistDateDTOs.add(channelIdPlaylistDateDTO);
						vLCChannelIDPubDateDTOsSet.add(vLCChannelIDPubDateDTO);
						channelIdPlaylistDateDTO.setRowId((Integer) result[2]);
						rowIds.add((Integer) result[2]);
					}
				}

				long startTimeforUpdate = System.currentTimeMillis();
				log.logMessage("Update the status (to INPROGRESS) rowIds in playlist staging table of Type: {}- Start. " , assetType );
				dcqPlaylistStagingRepository.deleteDuplicateIds();
				dcqPlaylistStagingRepository.updateStatus(rowIds, DcqVodIngestorConstants.IN_PROGRESS);
				dcqVodIngestionReaderResponseDTO.setStartTime(DcqVodIngestorUtils.currentTime());
				log.logMessage("TRANSACTION_END");
				log.logMessage("Update the status (to INPROGRESS) rowIds in playlist staging table of Type: {} - Completed,took: {} ms" , assetType 
						,(System.currentTimeMillis() - startTimeforUpdate));

			}

		} catch (Exception e) {
			log.logError(e);
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR, new NestedParameters(e.getMessage()));
		}

		return dcqVodIngestionReaderResponseDTO;
	}
}
