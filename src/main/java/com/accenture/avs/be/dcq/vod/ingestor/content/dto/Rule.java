
package com.accenture.avs.be.dcq.vod.ingestor.content.dto;

import java.io.Serializable;


public class Rule implements Serializable
{

    private String ruleId;
    /**
     * 
     * (Required)
     * 
     */
    private String ruleType;
    private String appliedOn;
    /**
     * 
     * (Required)
     * 
     */
    private Integer streamLimit;
    private String ruleName;
    private final static long serialVersionUID = -5631241410717191944L;

    public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	/**
     * 
     * @return
     *     The ruleId
     */
    public String getRuleId() {
        return ruleId;
    }

    /**
     * 
     * @param ruleId
     *     The ruleId
     */
    public void setRuleId(String ruleId) {
        this.ruleId = ruleId;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The ruleType
     */
    public String getRuleType() {
        return ruleType;
    }

    /**
     * 
     * (Required)
     * 
     * @param ruleType
     *     The ruleType
     */
    public void setRuleType(String ruleType) {
        this.ruleType = ruleType;
    }

    /**
     * 
     * @return
     *     The appliedOn
     */
    public String getAppliedOn() {
        return appliedOn;
    }

    /**
     * 
     * @param appliedOn
     *     The appliedOn
     */
    public void setAppliedOn(String appliedOn) {
        this.appliedOn = appliedOn;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The streamLimit
     */
    public Integer getStreamLimit() {
        return streamLimit;
    }

    /**
     * 
     * (Required)
     * 
     * @param streamLimit
     *     The streamLimit
     */
    public void setStreamLimit(Integer streamLimit) {
        this.streamLimit = streamLimit;
    }

}
