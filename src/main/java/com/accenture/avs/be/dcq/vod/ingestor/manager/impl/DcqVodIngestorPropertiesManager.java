package com.accenture.avs.be.dcq.vod.ingestor.manager.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.cache.EsSettingsMappingsCache;
import com.accenture.avs.be.dcq.vod.ingestor.cache.LanguageCache;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.commons.lib.LoggerWrapper;

/**
 * @author karthik.vadla
 *
 */
@Component
public class DcqVodIngestorPropertiesManager {
	private static LoggerWrapper log = new LoggerWrapper(DcqVodIngestorPropertiesManager.class);
	
	@Autowired
	private ResourceLoader resourceLoader;
	
	@Autowired
	private LanguageCache languageCache;
	
	@Autowired
	private EsSettingsMappingsCache esCache;
	
	private Map<String, String> mappingFilesMap;
	private Map<String, String> settingFilesMap;
	
	private static Properties vlcContentMappingProperties;
	
	private static Properties categoryMappingProperties;
	
	/**
	 * @throws IOException
	 */
	public void loadEsMappingFiles() throws IOException {
		long startTime = System.currentTimeMillis();	
		mappingFilesMap = new HashMap<String, String>();
		
		Map<String, String> languagesCache = languageCache.getLanguages();
		
		for (String langCode : languagesCache.keySet()) {
			mappingFilesMap.put(DcqVodIngestorConstants.CONTENT_IN_LOWER +DcqVodIngestorConstants.UNDERSCORE + langCode.toLowerCase(),
					esCache.getEsSettingsMappingsConfigurationsMap()
							.get(DcqVodIngestorConstants.CONTENT_IN_LOWER +DcqVodIngestorConstants.UNDERSCORE+ langCode.toLowerCase() +DcqVodIngestorConstants.UNDERSCORE +DcqVodIngestorConstants.MAPPING_IN_LOWER));
			mappingFilesMap.put(DcqVodIngestorConstants.CATEGORY_IN_LOWER +DcqVodIngestorConstants.UNDERSCORE+ langCode.toLowerCase(),
					esCache.getEsSettingsMappingsConfigurationsMap()
							.get(DcqVodIngestorConstants.CATEGORY_IN_LOWER +DcqVodIngestorConstants.UNDERSCORE+ langCode.toLowerCase() +DcqVodIngestorConstants.UNDERSCORE+DcqVodIngestorConstants.MAPPING_IN_LOWER));
			mappingFilesMap.put(DcqVodIngestorConstants.VLCCONTENT_IN_LOWER +DcqVodIngestorConstants.UNDERSCORE+ langCode.toLowerCase(),
					esCache.getEsSettingsMappingsConfigurationsMap().get(
							DcqVodIngestorConstants.VLCCONTENT_IN_LOWER +DcqVodIngestorConstants.UNDERSCORE+ langCode.toLowerCase() +DcqVodIngestorConstants.UNDERSCORE+DcqVodIngestorConstants.MAPPING_IN_LOWER));
		}
		mappingFilesMap.put(DcqVodIngestorConstants.VODCHANNEL_IN_LOWER,
				esCache.getEsSettingsMappingsConfigurationsMap()
						.get(DcqVodIngestorConstants.VODCHANNEL_IN_LOWER +DcqVodIngestorConstants.UNDERSCORE+DcqVodIngestorConstants.MAPPING_IN_LOWER));

		log.logMethodEnd(System.currentTimeMillis()-startTime);
	}

	public void loadEsSettingFiles() throws IOException {
		long startTime = System.currentTimeMillis();	
		settingFilesMap = new HashMap<String, String>();
		
		Map<String, String> languagesCache = languageCache.getLanguages();

		for (String langCode : languagesCache.keySet()) {

			settingFilesMap.put(DcqVodIngestorConstants.CONTENT_IN_LOWER +DcqVodIngestorConstants.UNDERSCORE+ langCode.toLowerCase(),
					esCache.getEsSettingsMappingsConfigurationsMap()
							.get(DcqVodIngestorConstants.CONTENT_IN_LOWER +DcqVodIngestorConstants.UNDERSCORE+ langCode.toLowerCase() +DcqVodIngestorConstants.UNDERSCORE+DcqVodIngestorConstants.SETTINGS_IN_LOWER));
			settingFilesMap.put(DcqVodIngestorConstants.CATEGORY_IN_LOWER +DcqVodIngestorConstants.UNDERSCORE+ langCode.toLowerCase(),
					esCache.getEsSettingsMappingsConfigurationsMap().get(
							DcqVodIngestorConstants.CATEGORY_IN_LOWER +DcqVodIngestorConstants.UNDERSCORE+ langCode.toLowerCase() +DcqVodIngestorConstants.UNDERSCORE+DcqVodIngestorConstants.SETTINGS_IN_LOWER));
			settingFilesMap.put(DcqVodIngestorConstants.VLCCONTENT_IN_LOWER +DcqVodIngestorConstants.UNDERSCORE+ langCode.toLowerCase(),
					esCache.getEsSettingsMappingsConfigurationsMap().get(
							DcqVodIngestorConstants.VLCCONTENT_IN_LOWER +DcqVodIngestorConstants.UNDERSCORE+ langCode.toLowerCase() +DcqVodIngestorConstants.UNDERSCORE+DcqVodIngestorConstants.SETTINGS_IN_LOWER));
		}
		settingFilesMap.put(DcqVodIngestorConstants.VODCHANNEL_IN_LOWER,
				esCache.getEsSettingsMappingsConfigurationsMap()
						.get(DcqVodIngestorConstants.VODCHANNEL_IN_LOWER +DcqVodIngestorConstants.UNDERSCORE+DcqVodIngestorConstants.SETTINGS_IN_LOWER));

		log.logMethodEnd(System.currentTimeMillis() - startTime);
	}
	
	
	public Properties loadVlcMappingProperties() throws IOException {
		long startTime = System.currentTimeMillis();	
		
		if (vlcContentMappingProperties != null){
			return vlcContentMappingProperties;			
		}
		vlcContentMappingProperties = new Properties();
		vlcContentMappingProperties.load(resourceLoader.getResource("classpath:dcq-vod-ingestor/"+DcqVodIngestorConstants.DCQ_VOD_INGESTOR_VLC_INPUT_MAPPING_FILE_NAME).getInputStream());
		
		log.logMethodEnd(System.currentTimeMillis()-startTime);
		return vlcContentMappingProperties;
	}
	
	public Properties loadCategoryMappingProperties() throws IOException {
		long startTime = System.currentTimeMillis();	
		
		if (categoryMappingProperties != null){
			return categoryMappingProperties;			
		}
		categoryMappingProperties = new Properties();
		categoryMappingProperties.load(resourceLoader.getResource("classpath:dcq-vod-ingestor/"+DcqVodIngestorConstants.DCQ_VOD_INGESTOR_CATEGORY_INPUT_MAPPING_FILE_NAME).getInputStream());
		
		log.logMethodEnd(System.currentTimeMillis()-startTime);
		return categoryMappingProperties;
	}
	
	public String getEsMapping(String ingestionType) {
		return mappingFilesMap.get(ingestionType);
	}
	
	public String getEsSettings(String ingestionType) {
		return settingFilesMap.get(ingestionType);
	}

}
