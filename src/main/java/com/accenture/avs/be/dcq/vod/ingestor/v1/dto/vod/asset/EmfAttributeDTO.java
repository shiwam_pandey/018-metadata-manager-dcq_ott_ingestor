package com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author karthik.vadla
 *
 */
public class EmfAttributeDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value="Identifier of an Object",required=true,example="7001")
	private Long emfId;
	
	@ApiModelProperty(value="EMF value" ,required=true,example="5")
	private String emfValue;
	
	@ApiModelProperty(value="EMF name , EMF name should be unique. In case of duplicate store the latest EMF name",required=true,example="plot7001")
	private String emfName;
	
	@ApiModelProperty(value="language code based on ISO 639-2-alpha3 standard",required=true,example="ENG")
	private String metadataLanguage;
	
	public Long getEmfId() {
		return emfId;
	}
	public void setEmfId(Long emfId) {
		this.emfId = emfId;
	}
	public String getEmfValue() {
		return emfValue;
	}
	public void setEmfValue(String emfValue) {
		this.emfValue = emfValue;
	}
	public String getEmfName() {
		return emfName;
	}
	public void setEmfName(String emfName) {
		this.emfName = emfName;
	}
	public String getMetadataLanguage() {
		return metadataLanguage;
	}
	public void setMetadataLanguage(String metadataLanguage) {
		this.metadataLanguage = metadataLanguage;
	}
	
	
}
