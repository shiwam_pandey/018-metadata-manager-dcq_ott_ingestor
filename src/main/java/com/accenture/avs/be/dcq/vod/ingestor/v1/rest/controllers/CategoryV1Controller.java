package com.accenture.avs.be.dcq.vod.ingestor.v1.rest.controllers;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.contentcategory.asset.Category;
import com.accenture.avs.be.dcq.vod.ingestor.v1.service.CategoryV1Service;
import com.accenture.avs.be.framework.annotation.AvsRestController;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.cache.Configurator;
import com.accenture.avs.be.framework.configuration.CacheConstants;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.commons.lib.support.CommonConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@AvsRestController
@Api(tags = "Category Services", value = "API’s pertaining to create,delete categories", description = "API's pertaining to dcqVodIngestor functional Area")
public class CategoryV1Controller {

	private static final LoggerWrapper log = new LoggerWrapper(CategoryV1Controller.class);

	@Autowired
	private Configurator configurator;

	@Autowired
	private CategoryV1Service categoryV1Service;

	@ApiOperation(value = "categoryIngestion", notes = "Creating or updating a category, which is used to categorize the contents for UI perspective", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses({ @ApiResponse(code = 200, message = "ACN_200: OK"),
			@ApiResponse(code = 400, message = "ACN_3000: Missing Parameter" + "\nACN_3019: Invalid parameter"),
			@ApiResponse(code = 404, message = "ACN_3304: Requested resource not found"),
			@ApiResponse(code = 500, message = "ACN_300: GENERIC ERROR") })
	@RequestMapping(value = "/v1/category", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<GenericResponse> upsertCategory(@RequestBody Category categoryRequest)
			throws ApplicationException {
		GenericResponse genericResponse = null;
		Configuration config = configurator.getConfiguration(ThreadContext.get(CommonConstants.LogParamsL4j2.TN));
		log.logStartApi();
		Long startTime = System.currentTimeMillis();
		try {
			genericResponse = categoryV1Service.upsertCategory(categoryRequest, config);
		} catch (ApplicationException ae) {
			log.logError(ae);
			throw ae;
		} catch (Exception e) {
			throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR);
		}

		log.logMethodEnd(System.currentTimeMillis() - startTime);
		return ResponseEntity.ok(genericResponse);
	}
	

	/**
	 * @param categoryId
	 * @return
	 * @throws ApplicationException
	 */
	@ApiOperation(value = "deleteCategory", notes = "To delete a category and also removes the content linking with this category", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses({ @ApiResponse(code = 200, message = "ACN_200: OK"),
			@ApiResponse(code = 400, message = "ACN_3019: Invalid parameter"),
			@ApiResponse(code = 404, message = "ACN_3304: Requested resource not found"),
			@ApiResponse(code = 500, message = "ACN_300: GENERIC ERROR") })
	@RequestMapping(value = "/v1/category/{categoryId}", method = RequestMethod.DELETE)
	public @ResponseBody
	ResponseEntity<GenericResponse> deleteCategory(
			@PathVariable("categoryId") String categoryId) throws ApplicationException {
		GenericResponse genericResponse = null;
		try {
			Configuration config = configurator.getConfiguration(ThreadContext.get(CommonConstants.LogParamsL4j2.TN));
			genericResponse = categoryV1Service.unpublishCategory(categoryId, config);
		} catch (ApplicationException ae) {
			log.logError(ae);
				throw ae;
			}
		catch (Exception e) {
				throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR);
			}
		return ResponseEntity.ok(genericResponse);
	}
	

}
