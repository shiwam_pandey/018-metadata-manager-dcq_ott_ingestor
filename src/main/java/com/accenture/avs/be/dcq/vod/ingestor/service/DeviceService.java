package com.accenture.avs.be.dcq.vod.ingestor.service;

import com.accenture.avs.be.dcq.vod.ingestor.dto.ResponseDeviceType;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ConfigurationException;

/**
 * @author karthik.vadla
 *
 */
public interface DeviceService {

	public GenericResponse getDeviceTypes(String startIndex, String maxResults, String sortBy, String sortOrder, Configuration config) throws ApplicationException, ConfigurationException, Exception;

	public ResponseDeviceType getDeviceTypesForLegacy(Configuration config) throws ApplicationException, ConfigurationException, Exception;

}
