package com.accenture.avs.be.dcq.vod.ingestor.dto;

import io.swagger.annotations.ApiModelProperty;

public class EmfDTO {

	@ApiModelProperty(required = true, value = "Unique Identifier of Emf")
	private Integer emfId;
	@ApiModelProperty(required = true, value = "Emf Name")
	private String emfName;
	@ApiModelProperty(required = true, value = "Flag Y/N for identifying if a Emf is active or not active.")
	private String isActive;
	@ApiModelProperty(required = false, value = "Emf Updated date",example="1545310594732")
	private long updateDate;

	
	public long getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(long updateDate) {
		this.updateDate = updateDate;
	}

	public Integer getEmfId() {
		return emfId;
	}

	public void setEmfId(Integer emfId) {
		this.emfId = emfId;
	}

	public String getEmfName() {
		return emfName;
	}

	public void setEmfName(String emfName) {
		this.emfName = emfName;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

}
