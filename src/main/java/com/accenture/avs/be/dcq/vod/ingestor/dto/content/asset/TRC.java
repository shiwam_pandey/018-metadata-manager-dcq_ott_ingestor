//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.09.27 at 03:24:49 PM IST 
//


package com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TRCId">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://accenture.mds.vods>StrMax100">
 *                 &lt;attribute name="IsExternalId" type="{http://accenture.mds.vods}FlagTypeYN" />
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element ref="{http://accenture.mds.vods}EnablerCommercialPackageList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "trcId",
    "enablerCommercialPackageList"
})
@XmlRootElement(name = "TRC")
public class TRC {

    @XmlElement(name = "TRCId", required = true)
    private TRC.TRCId trcId;
    @XmlElement(name = "EnablerCommercialPackageList")
    private EnablerCommercialPackageList enablerCommercialPackageList;

    /**
     * Gets the value of the trcId property.
     * 
     * @return
     *     possible object is
     *     {@link TRC.TRCId }
     *     
     */
    public TRC.TRCId getTRCId() {
        return trcId;
    }

    /**
     * Sets the value of the trcId property.
     * 
     * @param value
     *     allowed object is
     *     {@link TRC.TRCId }
     *     
     */
    public void setTRCId(TRC.TRCId value) {
        this.trcId = value;
    }

    /**
     * Gets the value of the enablerCommercialPackageList property.
     * 
     * @return
     *     possible object is
     *     {@link EnablerCommercialPackageList }
     *     
     */
    public EnablerCommercialPackageList getEnablerCommercialPackageList() {
        return enablerCommercialPackageList;
    }

    /**
     * Sets the value of the enablerCommercialPackageList property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnablerCommercialPackageList }
     *     
     */
    public void setEnablerCommercialPackageList(EnablerCommercialPackageList value) {
        this.enablerCommercialPackageList = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://accenture.mds.vods>StrMax100">
     *       &lt;attribute name="IsExternalId" type="{http://accenture.mds.vods}FlagTypeYN" />
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class TRCId {

        @XmlValue
        private String value;
        @XmlAttribute(name = "IsExternalId")
        private FlagTypeYN isExternalId;

        /**
         * Gets the value of the value property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * Gets the value of the isExternalId property.
         * 
         * @return
         *     possible object is
         *     {@link FlagTypeYN }
         *     
         */
        public FlagTypeYN getIsExternalId() {
            return isExternalId;
        }

        /**
         * Sets the value of the isExternalId property.
         * 
         * @param value
         *     allowed object is
         *     {@link FlagTypeYN }
         *     
         */
        public void setIsExternalId(FlagTypeYN value) {
            this.isExternalId = value;
        }

    }

}
