package com.accenture.avs.be.dcq.vod.ingestor.factories;

import java.util.HashMap;
import java.util.Map;

import com.accenture.avs.be.dcq.vod.ingestor.gateway.report.PublishedRecordsReportGatewayInterface;
import com.accenture.avs.be.dcq.vod.ingestor.gateway.report.impl.PublishedRecordsReportGateway;
import com.accenture.avs.be.dcq.vod.ingestor.processorplugin.DcqVodIngestionProcessorPlugin;
import com.accenture.avs.be.dcq.vod.ingestor.processorplugin.DcqVodIngestionProcessorPluginImpl;
import com.accenture.avs.be.dcq.vod.ingestor.timers.DcqVodIngestorInitialization;
import com.accenture.avs.be.dcq.vod.ingestor.utils.CacheConstants;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.commons.lib.LoggerWrapper;


/**
 * @author karthik.vadla
 *
 */
public class GatewayFactory {

	private static final LoggerWrapper log = new LoggerWrapper(GatewayFactory.class);
	private static Map<String, Object> map = new HashMap<String, Object>();

	@SuppressWarnings("unchecked")
	private static <T> T getGatewayInstance(Class<T> clazz, String name) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		Object obj = map.get(name);
		if (obj == null) {
			Class<?> clazz2 = Class.forName(name, true, DcqVodIngestorInitialization.classLoader);
			T obj2 = (T) clazz2.newInstance();
			map.put(name, obj2);
			return obj2;
		} else {
			return (T) obj;
		}
	}
	
	public static DcqVodIngestionProcessorPlugin getDcqVodIngestionProcessorPlugin(Configuration config) {
		try {
			String processorPluginClassName = config.getConstants().getValue(CacheConstants.DCQ_VOD_INGESTOR_PROCESSOR_PLUGIN_NAME);
			log.logMessage("Processor Gateway Class: {} " , processorPluginClassName);
			// instantiate and return the gateway
			return getGatewayInstance(DcqVodIngestionProcessorPlugin.class, processorPluginClassName);
		} catch (Exception e) {
			log.logMessage("error instantiation DcqVodIngestionProcessorPlugin - returning default one");
		}
		// return default gateway
		return new DcqVodIngestionProcessorPluginImpl();
	}
	
	public static PublishedRecordsReportGatewayInterface getPublishedRecordsReportGateway(Configuration config) {
		try{
			String reportGateWay = config.getConstants().getValue(CacheConstants.DCQ_VOD_INGESTOR_REPORT_GATEWAY);
			log.logMessage("ReportGateway :{} " , reportGateWay);       
			// instantiate and return the gateway
			return getGatewayInstance(PublishedRecordsReportGatewayInterface.class, reportGateWay);
		} catch(Exception e) {
			log.logMessage("error instantiation PublishedRecordsReportGatewayInterface - returning default one");
		}

		// return default gateway
		return new PublishedRecordsReportGateway();
	}
}
