package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.io.Serializable;

/**
 * @author karthik.vadla
 *
 */
public class Transaction implements Serializable{
	private static final long serialVersionUID = -4541737053918584302L;
	private String resultCode;
	private String errorCode;
	private String errorDescription = "";
	private long systemTime ;
	private Object resultObject ;
	
	public Transaction(){
		
	}
	public Transaction(String resultCode, String errorCode, String errorDescription, long systemTime,Object resultObject) {
		super();
		this.resultCode = resultCode;
		this.errorCode = errorCode;
		this.errorDescription = errorDescription;
		this.systemTime = systemTime;
		this.resultObject = resultObject;
	}
	public String getResultCode() {
		return resultCode;
	}
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	public String getErrorDescription() {
		return errorDescription;
	}
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
	public long getSystemTime() {
		return systemTime;
	}
	public void setSystemTime(long systemTime) {
		this.systemTime = systemTime;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public Object getResultObject() {
		return resultObject;
	}
	public void setResultObject(Object resultObject) {
		this.resultObject = resultObject;
	}
}
