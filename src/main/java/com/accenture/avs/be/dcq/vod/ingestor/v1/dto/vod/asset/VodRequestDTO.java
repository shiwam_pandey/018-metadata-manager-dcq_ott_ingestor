package com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author karthik.vadla
 *
 */
@ApiModel(value="VodRequest")
public class VodRequestDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "External Content Unique Identifier", required = true, example = "1000007")
	private String externalId;

	@ApiModelProperty(value = "Content Unique Identifier", required = false, example = "1000007")
	private Long contentId;

	@ApiModelProperty(value = "Type of the content. Replaced with contentType and contentSubType.", required = false, example = "TVOD")
	private String type;

	@ApiModelProperty(value = "For Object Type , Image , Video , Bundle , Group of bundle", required = true, example = "IMAGE")
	private String contentType;

	@ApiModelProperty(value = "For object subtype , VOD type of Episode , Series , season , Poster , Trailer", required = true, example = "POSTER")
	private String contentSubType;

	@ApiModelProperty(value = "Content Start Time", required = true, example = "2014-07-15T00:00:00")
	private Date contractStartDate;

	@ApiModelProperty(value = "Content End Time", required = true, example = "2017-12-15T00:00:00")
	private Date contractEndDate;

	@ApiModelProperty(value = "For Original Air Date", required = false, example = "2000-01-01T00:00:00.0000000")
	private Date originalAirDate;

	@ApiModelProperty(value = "For title brief", required = false, example = "test Poster Updated")
	private String titleBrief;

	@ApiModelProperty(value = "For Parent Object", required = false, example = "Y")
	private String isParentObject;

	@ApiModelProperty(value = "For Popular Episode", required = false, example = "Y")
	private String isPopularEpisode;

	@ApiModelProperty(value = "For OnAir content", required = false, example = "Y")
	private String isOnAir;

	@ApiModelProperty(value = "For Latest content", required = false, example = "Y")
	private String isLatest;

	@ApiModelProperty(value = "Keyword for search engine", required = false, example = "[\"PK\"]")
	private List<String> searchKeywords;

	@ApiModelProperty(value = "Unique Identifier for child content id", required = false, example = "[\"Y\"]")
	private List<ContentLinkingDTO> contentLinkings;

	@ApiModelProperty(value = "Keyword for Genre. This field is used by the User Taste feature. The feature will be unable to consider the content items where this field is missing", required = true, example = "[\"COMEDY\"]")
	private List<String> genres;

	@ApiModelProperty(value = "Content Provider Identifier , default value ‘AVS’", required = true, example = "CB")
	private String contentProvider;

	@ApiModelProperty(value = "Content Title", required = true, example = "PK")
	private String title;

	@ApiModelProperty(value = "Episode Title , For Serial Content", required = false, example = "PK")
	private String episodeTitle;

	@ApiModelProperty(value = "Content short description", required = false, example = "PK Movie")
	private String shortDescription;

	@ApiModelProperty(value = "Content long description", required = false, example = "PK Movie")
	private String longDescription;

	@ApiModelProperty(value = "Content directors list", required = false, example = "[\"karan johar\"]")
	private List<String> directors;

	@ApiModelProperty(value = "Content actors list", required = false, example = "[\"Amir Khan\"]")
	private List<String> actors;

	@ApiModelProperty(value = "Content anchors list", required = false, example = "[\"aada\"]")
	private List<String> anchors;

	@ApiModelProperty(value = "Content Producers list", required = false, example = "Adavi shesh")
	private List<String> authors;

	@ApiModelProperty(value = "Content Year of production. This field is used by the User Taste feature. The feature will be unable to consider the content items where this field is missing.", required = false, example = "2016")
	private String year;

	@ApiModelProperty(value = "Content Duration", required = true, example = "5766")
	private Long duration;

	@ApiModelProperty(value = "Content Production Countries List", required = false, example = "[\"USA\"]")
	private List<String> countriesOfOrigin;

	@ApiModelProperty(value = "Flag to indicate if the content can be returned from the recommendation engine", required = false, example = "Y")
	private String isRecommended;

	@ApiModelProperty(value = "rating of the content that could be set by the content provider", required = false, example = "2")
	private BigDecimal starRating;

	@ApiModelProperty(value = "Content original language. No validation is made on the value, use an appropriate standard for expressing language, e.g. https://www.iana.org/assignments/language-subtag-registry/language-subtag-registry.", required = false, example = "en")
	private String language;

	@ApiModelProperty(value = "Language subtitle , Used for asset transcoding workflow", required = false, example = "")
	private List<SubTitleDTO> subtitles;

	@ApiModelProperty(value = "GeoBlocking", required = false, example = "Y")
	private String isGeoBlocked;

	@ApiModelProperty(value = "Content Season , For Serial Content", required = false, example = "")
	private String seasonId;

	@ApiModelProperty(value = "Content series , Accepts positive values only", required = false, example = "")
	private Long series;

	@ApiModelProperty(value = "episodeId", required = false, example = "")
	private Long episodeId;

	@ApiModelProperty(value = "Episode Location, For Serial Content", required = false, example = "hyd")
	private String eventPlace;

	@ApiModelProperty(value = "HD flag", required = true, example = "Y")
	private String isHD;

	@ApiModelProperty(value = "Content Parental Rating , Parental rating values are validated according the specific levels shared with the customer", required = true, example = "G")
	private String parentalRating;

	@ApiModelProperty(value = "Content Price Category", required = false, example = "1")
	private Integer priceCategoryId;

	@ApiModelProperty(value = "Encrypted Flag", required = true, example = "Y")
	private String isEncrypted;

	@ApiModelProperty(value = "lastBroadcastChannelId", required = false, example = "114323")
	private Long lastBroadcastChannelId;

	@ApiModelProperty(value = "Broadcast Date , Mandatory if lastBroadcastChannel is present", required = false, example = "2015-03-10T00:00:00")
	private Date lastBroadcastDate;

	@ApiModelProperty(value = "Program category", required = false, example = "")
	private String programCategory;

	@ApiModelProperty(value = "Reference program Name", required = false, example = "")
	private String programReferenceName;

	@ApiModelProperty(value = "Entitlement Identifier", required = false, example = "")
	private Long entitlementId;

	@ApiModelProperty(value = "Generic field used for custom necessity", required = false, example = "")
	private String additionalData;

	@ApiModelProperty(value = "Extension Elements", required = false, example = "Y")
	private ExtensionsDTO extensions;

	@ApiModelProperty(value = "For Contextual Ads", required = false, example = "PK Aamir Khan")
	private String advTags;

	@ApiModelProperty(value = "Flag to identify the content is adfunded or not ‘Y’,’N’", required = false, example = "Y")
	private String isDisAllowedAdv;

	@ApiModelProperty(value = "Default language metadata for the content", required = false, example = "ENG")
	private String defaultLanguage;

	@ApiModelProperty(value = "", required = false, example = "")
	private List<MultiLanguageMetadataDTO> multiLanguageMetadata;

	@ApiModelProperty(value = "List of Scenes", required = false, example = "")
	private List<SceneDTO> scenes;

	@ApiModelProperty(value = "List of Chapters", required = false, example = "")
	private List<ChapterDTO> chapters;

	@ApiModelProperty(value = "EMFAttribute", required = false, example = "")
	private List<EmfAttributeDTO> emfAttributes;

	@ApiModelProperty(value = "List of device type name that should be blacklisted for the content", required = false, example = "IPAD")
	private List<String> blackListDeviceTypes;

	@ApiModelProperty(value = "isSurroundSound", required = false, example = "Y")
	private String isSurroundSound;

	@ApiModelProperty(value = "itemUrl", required = false, example = "")
	private String itemUrl;

	@ApiModelProperty(value = "ExtendedMetadata , Contains a json object with custom fields for customization purpose", required = false, example = "{\"plot\":\"On a far planet...\",\"actorsListDetail\":[{\"Name\":\"Sam Worthington\",\"BirthDate\":\"03/02/1981\",\"BirthCity\":\"New York\"},{\"Name\":\"Sigourney Weaver\",\"BirthDate\":\"04/03/1955\",\"BirthCity\":\"New York\"}]}")
	private String extendedMetadata;

	@ApiModelProperty(value = "ratingType", required = false, example = "VHMP")
	private String ratingType;

	@ApiModelProperty(value = "associatedWebsiteUrl", required = false, example = "http://cartoon.mp4")
	private String associatedWebsiteUrl;

	@ApiModelProperty(value = "infoPage", required = false, example = " real story")
	private String infoPage;

	@ApiModelProperty(value = "isSkipJumpEnabled", required = false, example = "Y")
	private String isSkipJumpEnabled;

	@ApiModelProperty(value = "isTrickPlayEnabled", required = false, example = "Y")
	private String isTrickPlayEnabled;

	@ApiModelProperty(value = "AdvertisingInfo List , This section is used for allowing the integration with Freewheel Advertising", required = false, example = "")
	private List<AdvertisingInfoDTO> advertisingInfo;

	@ApiModelProperty(value = "PolicyId list", required = false, example = "11234")
	private List<String> streamPolicies;

	@ApiModelProperty(value = "List of properties used in multi-geo scenario", required = false, example = "")
	private List<PropertyDTO> properties;

	@ApiModelProperty(value = "flag indicates whether copyProtection is to be applied or not.", required = false, example = "Y")
	private String isCopyProtected;

	@ApiModelProperty(value = "List of CopyProtections , if IsCopyProtected and CopyProtectionList not specified then default CopyProtections will be applied.if IsCopyProtected = Y, then CopyProtectionList is manadatory", required = false, example = "")
	private List<CopyProtectionDTO> copyProtections;

	@ApiModelProperty(value = "It identifies if the content is not playable out of home network environment , ‘Y’,’N’ :If this field is not present, it means the content is available out of home", required = false, example = "Y")
	private String isNotAvailableOutOfHome;

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getContentSubType() {
		return contentSubType;
	}

	public void setContentSubType(String contentSubType) {
		this.contentSubType = contentSubType;
	}

	public Date getContractStartDate() {
		return contractStartDate;
	}

	public void setContractStartDate(Date contractStartDate) {
		this.contractStartDate = contractStartDate;
	}

	public Date getContractEndDate() {
		return contractEndDate;
	}

	public void setContractEndDate(Date contractEndDate) {
		this.contractEndDate = contractEndDate;
	}

	public void setLastBroadcastChannelId(Long lastBroadcastChannelId) {
		this.lastBroadcastChannelId = lastBroadcastChannelId;
	}

	public Date getOriginalAirDate() {
		return originalAirDate;
	}

	public void setOriginalAirDate(Date originalAirDate) {
		this.originalAirDate = originalAirDate;
	}

	public String getTitleBrief() {
		return titleBrief;
	}

	public void setTitleBrief(String titleBrief) {
		this.titleBrief = titleBrief;
	}

	public String getIsParentObject() {
		return isParentObject;
	}

	public void setIsParentObject(String isParentObject) {
		this.isParentObject = isParentObject;
	}

	public String getIsPopularEpisode() {
		return isPopularEpisode;
	}

	public void setIsPopularEpisode(String isPopularEpisode) {
		this.isPopularEpisode = isPopularEpisode;
	}

	public String getIsOnAir() {
		return isOnAir;
	}

	public void setIsOnAir(String isOnAir) {
		this.isOnAir = isOnAir;
	}

	public String getIsLatest() {
		return isLatest;
	}

	public void setIsLatest(String isLatest) {
		this.isLatest = isLatest;
	}

	public List<String> getSearchKeywords() {
		return searchKeywords;
	}

	public void setSearchKeywords(List<String> searchKeywords) {
		this.searchKeywords = searchKeywords;
	}

	public List<ContentLinkingDTO> getContentLinkings() {
		return contentLinkings;
	}

	public void setContentLinkings(List<ContentLinkingDTO> contentLinkings) {
		this.contentLinkings = contentLinkings;
	}

	public List<String> getGenres() {
		return genres;
	}

	public void setGenres(List<String> genres) {
		this.genres = genres;
	}

	public String getContentProvider() {
		return contentProvider;
	}

	public void setContentProvider(String contentProvider) {
		this.contentProvider = contentProvider;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public Long getContentId() {
		return contentId;
	}

	public void setContentId(Long contentId) {
		this.contentId = contentId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getEpisodeTitle() {
		return episodeTitle;
	}

	public void setEpisodeTitle(String episodeTitle) {
		this.episodeTitle = episodeTitle;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getLongDescription() {
		return longDescription;
	}

	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}

	public List<String> getDirectors() {
		return directors;
	}

	public void setDirectors(List<String> directors) {
		this.directors = directors;
	}

	public List<String> getActors() {
		return actors;
	}

	public void setActors(List<String> actors) {
		this.actors = actors;
	}

	public List<String> getAnchors() {
		return anchors;
	}

	public void setAnchors(List<String> anchors) {
		this.anchors = anchors;
	}

	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public List<String> getCountriesOfOrigin() {
		return countriesOfOrigin;
	}

	public void setCountriesOfOrigin(List<String> countriesOfOrigin) {
		this.countriesOfOrigin = countriesOfOrigin;
	}

	public String getIsRecommended() {
		return isRecommended;
	}

	public void setIsRecommended(String isRecommended) {
		this.isRecommended = isRecommended;
	}

	public BigDecimal getStarRating() {
		return starRating;
	}

	public void setStarRating(BigDecimal starRating) {
		this.starRating = starRating;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public List<SubTitleDTO> getSubtitles() {
		return subtitles;
	}

	public void setSubtitles(List<SubTitleDTO> subtitles) {
		this.subtitles = subtitles;
	}

	public String getIsGeoBlocked() {
		return isGeoBlocked;
	}

	public void setIsGeoBlocked(String isGeoBlocked) {
		this.isGeoBlocked = isGeoBlocked;
	}

	public String getSeasonId() {
		return seasonId;
	}

	public void setSeasonId(String seasonId) {
		this.seasonId = seasonId;
	}

	public Long getSeries() {
		return series;
	}

	public void setSeries(Long series) {
		this.series = series;
	}

	public Long getEpisodeId() {
		return episodeId;
	}

	public void setEpisodeId(Long episodeId) {
		this.episodeId = episodeId;
	}

	public String getEventPlace() {
		return eventPlace;
	}

	public void setEventPlace(String eventPlace) {
		this.eventPlace = eventPlace;
	}

	public String getIsHD() {
		return isHD;
	}

	public void setIsHD(String isHD) {
		this.isHD = isHD;
	}

	public String getParentalRating() {
		return parentalRating;
	}

	public void setParentalRating(String parentalRating) {
		this.parentalRating = parentalRating;
	}

	public Integer getPriceCategoryId() {
		return priceCategoryId;
	}

	public void setPriceCategoryId(Integer priceCategoryId) {
		this.priceCategoryId = priceCategoryId;
	}

	public String getIsEncrypted() {
		return isEncrypted;
	}

	public void setIsEncrypted(String isEncrypted) {
		this.isEncrypted = isEncrypted;
	}

	public Long getLastBroadcastChannelId() {
		return lastBroadcastChannelId;
	}

	public void setLastBroadcastChannel(Long lastBroadcastChannelId) {
		this.lastBroadcastChannelId = lastBroadcastChannelId;
	}

	public Date getLastBroadcastDate() {
		return lastBroadcastDate;
	}

	public void setLastBroadcastDate(Date lastBroadcastDate) {
		this.lastBroadcastDate = lastBroadcastDate;
	}

	public String getProgramCategory() {
		return programCategory;
	}

	public void setProgramCategory(String programCategory) {
		this.programCategory = programCategory;
	}

	public String getProgramReferenceName() {
		return programReferenceName;
	}

	public void setProgramReferenceName(String programReferenceName) {
		this.programReferenceName = programReferenceName;
	}

	public Long getEntitlementId() {
		return entitlementId;
	}

	public void setEntitlementId(Long entitlementId) {
		this.entitlementId = entitlementId;
	}

	public String getAdditionalData() {
		return additionalData;
	}

	public void setAdditionalData(String additionalData) {
		this.additionalData = additionalData;
	}

	public ExtensionsDTO getExtensions() {
		return extensions;
	}

	public void setExtensions(ExtensionsDTO extensions) {
		this.extensions = extensions;
	}

	public String getAdvTags() {
		return advTags;
	}

	public void setAdvTags(String advTags) {
		this.advTags = advTags;
	}

	public String getIsDisAllowedAdv() {
		return isDisAllowedAdv;
	}

	public void setIsDisAllowedAdv(String isDisAllowedAdv) {
		this.isDisAllowedAdv = isDisAllowedAdv;
	}

	public String getDefaultLanguage() {
		return defaultLanguage;
	}

	public void setDefaultLanguage(String defaultLanguage) {
		this.defaultLanguage = defaultLanguage;
	}

	public List<MultiLanguageMetadataDTO> getMultiLanguageMetadata() {
		return multiLanguageMetadata;
	}

	public void setMultiLanguageMetadata(List<MultiLanguageMetadataDTO> multiLanguageMetadata) {
		this.multiLanguageMetadata = multiLanguageMetadata;
	}

	public List<SceneDTO> getScenes() {
		return scenes;
	}

	public void setScenes(List<SceneDTO> scenes) {
		this.scenes = scenes;
	}

	public List<ChapterDTO> getChapters() {
		return chapters;
	}

	public void setChapters(List<ChapterDTO> chapters) {
		this.chapters = chapters;
	}

	public List<EmfAttributeDTO> getEmfAttributes() {
		return emfAttributes;
	}

	public void setEmfAttributes(List<EmfAttributeDTO> emfAttributes) {
		this.emfAttributes = emfAttributes;
	}

	public List<String> getBlackListDeviceTypes() {
		return blackListDeviceTypes;
	}

	public void setBlackListDeviceTypes(List<String> blackListDeviceTypes) {
		this.blackListDeviceTypes = blackListDeviceTypes;
	}

	public String getIsSurroundSound() {
		return isSurroundSound;
	}

	public void setIsSurroundSound(String isSurroundSound) {
		this.isSurroundSound = isSurroundSound;
	}

	public String getItemUrl() {
		return itemUrl;
	}

	public void setItemUrl(String itemUrl) {
		this.itemUrl = itemUrl;
	}

	public String getExtendedMetadata() {
		return extendedMetadata;
	}

	public void setExtendedMetadata(String extendedMetadata) {
		this.extendedMetadata = extendedMetadata;
	}

	public String getRatingType() {
		return ratingType;
	}

	public void setRatingType(String ratingType) {
		this.ratingType = ratingType;
	}

	public String getAssociatedWebsiteUrl() {
		return associatedWebsiteUrl;
	}

	public void setAssociatedWebsiteUrl(String associatedWebsiteUrl) {
		this.associatedWebsiteUrl = associatedWebsiteUrl;
	}

	public String getInfoPage() {
		return infoPage;
	}

	public void setInfoPage(String infoPage) {
		this.infoPage = infoPage;
	}

	public String getIsSkipJumpEnabled() {
		return isSkipJumpEnabled;
	}

	public void setIsSkipJumpEnabled(String isSkipJumpEnabled) {
		this.isSkipJumpEnabled = isSkipJumpEnabled;
	}

	public String getIsTrickPlayEnabled() {
		return isTrickPlayEnabled;
	}

	public void setIsTrickPlayEnabled(String isTrickPlayEnabled) {
		this.isTrickPlayEnabled = isTrickPlayEnabled;
	}

	public List<AdvertisingInfoDTO> getAdvertisingInfo() {
		return advertisingInfo;
	}

	public void setAdvertisingInfo(List<AdvertisingInfoDTO> advertisingInfo) {
		this.advertisingInfo = advertisingInfo;
	}

	public List<String> getStreamPolicies() {
		return streamPolicies;
	}

	public void setStreamPolicies(List<String> streamPolicies) {
		this.streamPolicies = streamPolicies;
	}

	public List<PropertyDTO> getProperties() {
		return properties;
	}

	public void setProperties(List<PropertyDTO> properties) {
		this.properties = properties;
	}

	public String getIsCopyProtected() {
		return isCopyProtected;
	}

	public void setIsCopyProtected(String isCopyProtected) {
		this.isCopyProtected = isCopyProtected;
	}

	public List<CopyProtectionDTO> getCopyProtections() {
		return copyProtections;
	}

	public void setCopyProtections(List<CopyProtectionDTO> copyProtections) {
		this.copyProtections = copyProtections;
	}

	public String getIsNotAvailableOutOfHome() {
		return isNotAvailableOutOfHome;
	}

	public void setIsNotAvailableOutOfHome(String isNotAvailableOutOfHome) {
		this.isNotAvailableOutOfHome = isNotAvailableOutOfHome;
	}

}