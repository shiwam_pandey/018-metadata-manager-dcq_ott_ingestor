package com.accenture.avs.be.dcq.vod.ingestor.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.accenture.avs.persistence.technicalcatalogue.ChannelEntity;
/**
 * @author karthik.vadla
 *
 */
@Repository
public interface ChannelRepository extends JpaRepository<ChannelEntity,Integer>{

	/**
	 * @param intValue
	 * @return
	 */
	String retreiveChannelNameByChannelId(@Param("channelId")int intValue);

	/**
	 * @param vodChannelId
	 * @return
	 */
	List<Object[]> getTechnicalPkgId(Integer vodChannelId);
	
	/**
	 * @param vodChannelId
	 * @return
	 */
	Object getChannelXMlBlob(Integer vodChannelId);
	
	/**
	 * @param channelId
	 * @return
	 */
	ChannelEntity retreiveChannelByChannelId(@Param("channelId")Integer channelId);
	
	List<Integer> retrieveByDefaultChannelNumber(@Param("defaultChannelNumber")String defaultChannelNumber);
	
	Integer validateChannelId(@Param("channelId")Integer channelId);

	
}
