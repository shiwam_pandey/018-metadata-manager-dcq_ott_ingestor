package com.accenture.avs.be.dcq.vod.ingestor.dto;

import com.accenture.avs.be.framework.bean.GenericResponse;

public class DeviceTypeListResponse extends GenericResponse

{

	private final static long serialVersionUID = 1L;
	private String resultCode;
	private String errorCode;
	private String errorDescription;
	private DeviceTypeListDTO deviceTypeList;

	/**
	 * Gets the value of the resultCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getResultCode() {
		return resultCode;
	}

	/**
	 * Sets the value of the resultCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setResultCode(String value) {
		this.resultCode = value;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	/**
	 * Gets the value of the deviceTypeList property.
	 * 
	 * @return possible object is {@link DeviceTypeListDTO }
	 * 
	 */
	public DeviceTypeListDTO getDeviceTypeList() {
		return deviceTypeList;
	}

	/**
	 * Sets the value of the deviceTypeList property.
	 * 
	 * @param value
	 *            allowed object is {@link DeviceTypeListDTO }
	 * 
	 */
	public void setDeviceTypeList(DeviceTypeListDTO value) {
		this.deviceTypeList = value;
	}

	

}
