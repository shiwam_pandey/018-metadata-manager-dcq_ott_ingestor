package com.accenture.avs.be.dcq.vod.ingestor.v1.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accenture.avs.be.dcq.vod.ingestor.cache.LanguageCache;
import com.accenture.avs.be.dcq.vod.ingestor.manager.ContentMetadataManager;
import com.accenture.avs.be.dcq.vod.ingestor.processor.DcqVodIngestionPreProcessor;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.Validator;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.AdvertisingInfoDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.AdvertisingInfoDTO.CuePointDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.AudioLanguageDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.ChapterDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.ChapterDTO.MultiLanguageChapterDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.ContentLinkingDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.CopyProtectionDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.EmfAttributeDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.ExtensionsDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.ExtensionsDTO.CategoryDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.ExtensionsDTO.PlatformDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.MultiLanguageMetadataDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.PropertyDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.SceneDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.SceneDTO.MultiLanguageSceneDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.SubTitleDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.VodRequestDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.manager.VodManagerV1;
import com.accenture.avs.be.dcq.vod.ingestor.v1.service.VodServiceV1;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.be.framework.exception.ConfigurationException;
import com.accenture.avs.commons.lib.LoggerWrapper;

/**
 * @author karthik.vadla
 *
 */
@Service
public class VodServiceV1Impl implements VodServiceV1 {

	@Autowired
	private VodManagerV1 vodManagerV1;
	@Autowired
	private DcqVodIngestionPreProcessor dcqVodIngestionPreProcessor;
	@Autowired
	private ContentMetadataManager contentMetadataManager;
	@Autowired
	private LanguageCache languageCache;

	private static final LoggerWrapper log = new LoggerWrapper(VodServiceV1Impl.class);

	@Override
	public GenericResponse ingestContent(VodRequestDTO vodDTO, Configuration config)
			throws ApplicationException, Exception {
		log.logMessage("call to ingestContent ");
		GenericResponse genericResponse = null;
		try {
			validateVodDTORequest(vodDTO, config);
			Integer contentId = vodManagerV1.ingestContent(vodDTO, config);
			List<Integer> assetIdList = new ArrayList<>();
			assetIdList.add(contentId);
			dcqVodIngestionPreProcessor.refreshCachesCreateIndexes(DcqVodIngestorConstants.CONTENT, "", "", config);
			contentMetadataManager.processandWriteContents(assetIdList, DcqVodIngestorUtils.getTransactionNumber(),
					DcqVodIngestorUtils.currentTime(), "", "", config);

		} catch (ApplicationException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}
		genericResponse = new GenericResponse(DcqVodIngestorConstants.ACN_200, DcqVodIngestorConstants.OK, "");

		return genericResponse;
	}

	/**
	 * @param vodDTO
	 * @param config
	 * @throws ApplicationException
	 * @throws ConfigurationException
	 */
	private void validateVodDTORequest(VodRequestDTO vodDTO, Configuration config)
			throws ApplicationException, ConfigurationException {

		Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.EXTERNAL_ID, vodDTO.getExternalId(), config);
		Validator.sanitizingParamsForSpecialParams(DcqVodIngestorConstants.VODCONSTANTSV1.EXTERNAL_ID, vodDTO.getExternalId());

		Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.TYPE, vodDTO.getType(), config);
		Validator.checkParameterValidityRange(DcqVodIngestorConstants.VODCONSTANTSV1.TYPE,
				DcqVodIngestorConstants.ALLOWED_TYPE_VALUES.toArray(), vodDTO.getType(), config);

		Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.CONTENT_TYPE, vodDTO.getContentType(), config);
		Validator.checkParameterValidityRange(DcqVodIngestorConstants.VODCONSTANTSV1.CONTENT_TYPE,
				DcqVodIngestorConstants.ALLOWED_CONTENT_TYPE_VALUES.toArray(), vodDTO.getContentType(), config);

		Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.CONTENT_SUB_TYPE, vodDTO.getContentSubType(),
				config);
		
		Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.CONTRACT_START_DATE,
				vodDTO.getContractStartDate(), config);
		Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.CONTRACT_END_DATE, vodDTO.getContractEndDate(),
				config);

		Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.TITLE, vodDTO.getTitle(), config);
		
		Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.GENRES, vodDTO.getGenres(), config);
		Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.DURATION, vodDTO.getDuration(), config);
		Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.CONTENT_PROVIDER, vodDTO.getContentProvider(),
				config);
		
		Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.IS_HD, vodDTO.getIsHD(), config);
		Validator.checkParameterValidityRange(DcqVodIngestorConstants.VODCONSTANTSV1.IS_HD,
				DcqVodIngestorConstants.ALLOWED_YN_VALUES.toArray(), vodDTO.getIsHD(), config);

		Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.PARENTAL_RATING, vodDTO.getParentalRating(),
				config);

		Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.IS_ENCRYPTED, vodDTO.getIsEncrypted(), config);
		Validator.checkParameterValidityRange(DcqVodIngestorConstants.VODCONSTANTSV1.IS_ENCRYPTED,
				DcqVodIngestorConstants.ALLOWED_YN_VALUES.toArray(), vodDTO.getIsEncrypted(), config);


		if (vodDTO.getIsDisAllowedAdv() != null) {
			Validator.checkParameterValidityRange(DcqVodIngestorConstants.VODCONSTANTSV1.IS_DIS_ALLOWED_ADV,
					DcqVodIngestorConstants.ALLOWED_YN_VALUES.toArray(), vodDTO.getIsDisAllowedAdv(), config);
		}
		if (vodDTO.getIsGeoBlocked() != null) {
			Validator.checkParameterValidityRange(DcqVodIngestorConstants.VODCONSTANTSV1.IS_GEO_BLOCKED,
					DcqVodIngestorConstants.ALLOWED_YN_VALUES.toArray(), vodDTO.getIsGeoBlocked(), config);
		}
		if (vodDTO.getIsLatest() != null) {
			Validator.checkParameterValidityRange(DcqVodIngestorConstants.VODCONSTANTSV1.IS_LATEST,
					DcqVodIngestorConstants.ALLOWED_YN_VALUES.toArray(), vodDTO.getIsLatest(), config);
		}
		if (vodDTO.getIsNotAvailableOutOfHome() != null) {
			Validator.checkParameterValidityRange(DcqVodIngestorConstants.VODCONSTANTSV1.IS_NOT_AVAILABLE_OUT_OF_HOME,
					DcqVodIngestorConstants.ALLOWED_YN_VALUES.toArray(), vodDTO.getIsNotAvailableOutOfHome(), config);
		}
		if (vodDTO.getIsOnAir() != null) {
			Validator.checkParameterValidityRange(DcqVodIngestorConstants.VODCONSTANTSV1.IS_ON_AIR,
					DcqVodIngestorConstants.ALLOWED_YN_VALUES.toArray(), vodDTO.getIsOnAir(), config);
		}
		if (vodDTO.getIsParentObject() != null) {
			Validator.checkParameterValidityRange(DcqVodIngestorConstants.VODCONSTANTSV1.IS_PARENT_OBJECT,
					DcqVodIngestorConstants.ALLOWED_YN_VALUES.toArray(), vodDTO.getIsParentObject(), config);
		}
		if (vodDTO.getIsPopularEpisode() != null) {
			Validator.checkParameterValidityRange(DcqVodIngestorConstants.VODCONSTANTSV1.IS_POPULAR_EPISODE,
					DcqVodIngestorConstants.ALLOWED_YN_VALUES.toArray(), vodDTO.getIsPopularEpisode(), config);
		}
		if (vodDTO.getIsRecommended() != null) {
			Validator.checkParameterValidityRange(DcqVodIngestorConstants.VODCONSTANTSV1.IS_RECOMMENDED,
					DcqVodIngestorConstants.ALLOWED_YN_VALUES.toArray(), vodDTO.getIsRecommended(), config);
		}
		if (vodDTO.getIsSkipJumpEnabled() != null) {
			Validator.checkParameterValidityRange(DcqVodIngestorConstants.VODCONSTANTSV1.IS_SKIP_JUMP_ENABLED,
					DcqVodIngestorConstants.ALLOWED_YN_VALUES.toArray(), vodDTO.getIsSkipJumpEnabled(), config);
		}
		if (vodDTO.getIsSurroundSound() != null) {
			Validator.checkParameterValidityRange(DcqVodIngestorConstants.VODCONSTANTSV1.IS_SURROUND_SOUND,
					DcqVodIngestorConstants.ALLOWED_YN_VALUES.toArray(), vodDTO.getIsSurroundSound(), config);
		}
		if (vodDTO.getIsTrickPlayEnabled() != null) {
			Validator.checkParameterValidityRange(DcqVodIngestorConstants.VODCONSTANTSV1.IS_TRICK_PLAY_ENABLED,
					DcqVodIngestorConstants.ALLOWED_YN_VALUES.toArray(), vodDTO.getIsTrickPlayEnabled(), config);
		}
		if (vodDTO.getIsCopyProtected()!= null) {
			Validator.checkParameterValidityRange(DcqVodIngestorConstants.VODCONSTANTSV1.IS_COPY_PROTECTED,
					DcqVodIngestorConstants.ALLOWED_YN_VALUES.toArray(), vodDTO.getIsCopyProtected(), config);
		}

		//START MAINT-7307
		/*
		 * if(vodDTO.getLanguage()!=null) {
		 * if(!languageCache.getLanguages().containsKey(vodDTO.getLanguage())) { throw
		 * new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
		 * new NestedParameters("language")); } }
		 */
		//END
		
		if(vodDTO.getDefaultLanguage()!=null) {
			if(!languageCache.getLanguages().containsKey(vodDTO.getDefaultLanguage())) {
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters("defaultLanguage"));
			}
		}
		List<ContentLinkingDTO> contentLinkings = vodDTO.getContentLinkings();
		if(contentLinkings!=null) {
			for(ContentLinkingDTO contentLinking : contentLinkings) {
				Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.IS_DEFAULT, contentLinking.getIsDefault(),
						config);
				Validator.checkParameterValidityRange(DcqVodIngestorConstants.VODCONSTANTSV1.IS_DEFAULT,
						DcqVodIngestorConstants.ALLOWED_YN_VALUES.toArray(), contentLinking.getIsDefault(), config);
				
				Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.SUB_TYPE, contentLinking.getSubType(),
						config);
				Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.CONTENT_ID, contentLinking.getContentId(),
						config);
				Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.ORDER_ID, contentLinking.getOrderId(),
						config);
			}
		}
		
		
		
		List<SubTitleDTO> subTitles = vodDTO.getSubtitles();
		
		if(subTitles!=null) {
			for(SubTitleDTO subtitle : subTitles) {
				Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.SUBTITLE_ID, subtitle.getSubtitleId(),
						config);
				Validator.sanitizingParamsForSpecialParams(DcqVodIngestorConstants.VODCONSTANTSV1.SUBTITLE_ID, subtitle.getSubtitleId());
				Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.SUBTITLE_LANGUAGE_NAME, subtitle.getSubtitleLanguageName(),
						config);
				Validator.sanitizingParamsForSpecialParams(DcqVodIngestorConstants.VODCONSTANTSV1.SUBTITLE_LANGUAGE_NAME, subtitle.getSubtitleLanguageName());
			}
		}
		ExtensionsDTO extensionsDTO = vodDTO.getExtensions();

		if (extensionsDTO != null) {

			Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.IS_ADULT, extensionsDTO.getIsAdult(),
					config);
			Validator.checkParameterValidityRange(DcqVodIngestorConstants.VODCONSTANTSV1.IS_ADULT,
					DcqVodIngestorConstants.ALLOWED_YN_VALUES.toArray(), extensionsDTO.getIsAdult(), config);

			Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.PLATFORMS, extensionsDTO.getPlatforms(),
					config);

			for (PlatformDTO platformDTO : extensionsDTO.getPlatforms()) {
				Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.PLATFORM_NAME,
						platformDTO.getPlatformName(), config);
				Validator.sanitizingParamsForSpecialParams(DcqVodIngestorConstants.VODCONSTANTSV1.PLATFORM_NAME,platformDTO.getPlatformName());
						
				Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.VIDEO_URL, platformDTO.getVideoUrl(),
						config);
				Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.PICTURE_URL,
						platformDTO.getPictureUrl(), config);
				
				Validator.isValidUrl(platformDTO.getTrailerUrl(), DcqVodIngestorConstants.VODCONSTANTSV1.TRAILER_URL);
				Validator.isValidUrl(platformDTO.getVideoUrl(), DcqVodIngestorConstants.VODCONSTANTSV1.VIDEO_URL);
				Validator.isValidUrl(platformDTO.getPictureUrl(), DcqVodIngestorConstants.VODCONSTANTSV1.PICTURE_URL);
				
				List<SubTitleDTO> platformSubTitles = platformDTO.getSubtitles();
				
				if(subTitles!=null) {
					for(SubTitleDTO subtitle : platformSubTitles) {
						Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.SUBTITLE_ID, subtitle.getSubtitleId(),
								config);
						Validator.sanitizingParamsForSpecialParams(DcqVodIngestorConstants.VODCONSTANTSV1.SUBTITLE_ID, subtitle.getSubtitleId());
						Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.SUBTITLE_LANGUAGE_NAME, subtitle.getSubtitleLanguageName(),
								config);
						Validator.sanitizingParamsForSpecialParams(DcqVodIngestorConstants.VODCONSTANTSV1.SUBTITLE_LANGUAGE_NAME, subtitle.getSubtitleLanguageName());
					}
				}
				
				List<AudioLanguageDTO> audioLanguages = platformDTO.getAudioLanguages();
				if(audioLanguages!=null) {
					for(AudioLanguageDTO audioLanguage : audioLanguages) {
						Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.AUDIO_ID,
								audioLanguage.getAudioId(), config);
						Validator.sanitizingParamsForSpecialParams(DcqVodIngestorConstants.VODCONSTANTSV1.AUDIO_ID,audioLanguage.getAudioId());
						Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.AUDIO_LANGUAGE_CODE,
								audioLanguage.getAudioLanguageCode(), config);
						Validator.sanitizingParamsForSpecialParams(DcqVodIngestorConstants.VODCONSTANTSV1.AUDIO_LANGUAGE_CODE,audioLanguage.getAudioLanguageCode());
						Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.AUDIO_LANGUAGE_NAME,
								audioLanguage.getAudioLanguageName(), config);
						Validator.sanitizingParamsForSpecialParams(DcqVodIngestorConstants.VODCONSTANTSV1.AUDIO_LANGUAGE_NAME,audioLanguage.getAudioLanguageName());
						Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.STREAM_ID,
								audioLanguage.getStreamId(), config);
						Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.IS_PREFERRED,
								audioLanguage.getIsPreferred(), config);
						Validator.checkParameterValidityRange(DcqVodIngestorConstants.VODCONSTANTSV1.IS_PREFERRED,
								DcqVodIngestorConstants.ALLOWED_YN_VALUES.toArray(), audioLanguage.getIsPreferred(), config);
					}
				}
				
				
				
			}
			
			List<CategoryDTO> categories = extensionsDTO.getCategories();
			
			if(categories!=null) {
				for(CategoryDTO categoryDTO : categories) {
					Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.CATEGORY_EXTERNAL_ID,
							categoryDTO.getCategoryExternalId(), config);
					Validator.sanitizingParamsForSpecialParams(DcqVodIngestorConstants.VODCONSTANTSV1.CATEGORY_EXTERNAL_ID,categoryDTO.getCategoryExternalId());
							
					
					Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.IS_PRIMARY,
							categoryDTO.getIsPrimary(), config);
					
					Validator.checkParameterValidityRange(DcqVodIngestorConstants.VODCONSTANTSV1.IS_PRIMARY,
							DcqVodIngestorConstants.ALLOWED_YN_VALUES.toArray(), categoryDTO.getIsPrimary(), config);
				}

			}
			
		}

		validateEmfAttributes(vodDTO.getEmfAttributes(), config);

		List<SceneDTO> scenes = vodDTO.getScenes();
		if (scenes != null) {
			for (SceneDTO sceneDTO : scenes) {
				Validator.checkParameter("sceneId", sceneDTO.getSceneId(), config);
				Validator.checkParameter("scene startTime", sceneDTO.getStartTime(), config);
				Validator.checkParameter("scene endTime", sceneDTO.getEndTime(), config);
				Validator.checkParameter("scene action", sceneDTO.getAction(), config);
				Validator.checkParameter("scene actors", sceneDTO.getActors(), config);
				Validator.checkParameter("scene adTag", sceneDTO.getAdTag(), config);
				Validator.checkParameter("scene audio", sceneDTO.getAudio(), config);
				Validator.checkParameter("scene characters", sceneDTO.getCharacters(), config);
				Validator.checkParameter("scene company", sceneDTO.getCompany(), config);
				Validator.checkParameter("scene event", sceneDTO.getEvent(), config);
				Validator.checkParameter("scene extendedLocation", sceneDTO.getExtendedLocation(), config);
				Validator.checkParameter("scene genre", sceneDTO.getGenre(), config);
				Validator.checkParameter("scene keywords", sceneDTO.getKeywords(), config);
				Validator.checkParameter("scene location", sceneDTO.getLocation(), config);
				Validator.checkParameter("scene metadataLanguage", sceneDTO.getMetadataLanguage(), config);
				Validator.checkParameter("scene musicians", sceneDTO.getMusicians(), config);
				Validator.checkParameter("scene title", sceneDTO.getTitle(), config);
				Validator.checkParameter("scene titleBrief", sceneDTO.getTitleBrief(), config);
				Validator.checkParameter("scene orderId", sceneDTO.getOrderId(), config);

				List<MultiLanguageSceneDTO> sceneLanguages = sceneDTO.getMultiLanguageScenes();
				if (sceneLanguages != null) {
					for (MultiLanguageSceneDTO sceneLanguage : sceneLanguages) {
						Validator.checkParameter("multi language scene action", sceneLanguage.getAction(), config);
						Validator.checkParameter("multi language scene actors", sceneLanguage.getActors(), config);
						Validator.checkParameter("multi language scene adTag", sceneLanguage.getAdTag(), config);
						Validator.checkParameter("multi language scene audio", sceneLanguage.getAudio(), config);
						Validator.checkParameter("multi language scene characters", sceneLanguage.getCharacters(),
								config);
						Validator.checkParameter("multi language scene company", sceneLanguage.getCompany(), config);
						Validator.checkParameter("multi language scene event", sceneLanguage.getEvent(), config);
						Validator.checkParameter("multi language scene extendedLocation",
								sceneLanguage.getExtendedLocation(), config);
						Validator.checkParameter("multi language scene genre", sceneLanguage.getGenre(), config);
						Validator.checkParameter("multi language scene keywords", sceneLanguage.getKeywords(), config);
						Validator.checkParameter("multi language scene location", sceneLanguage.getLocation(), config);
						Validator.checkParameter("multi language scene metadataLanguage",
								sceneLanguage.getMetadataLanguage(), config);
						Validator.checkParameter("multi language scene musicians", sceneLanguage.getMusicians(),
								config);
						Validator.checkParameter("multi language scene title", sceneLanguage.getTitle(), config);
						Validator.checkParameter("multi language scene titleBrief", sceneLanguage.getTitleBrief(),
								config);
						Validator.checkParameter("multi language scene orderId", sceneLanguage.getOrderId(), config);

						validateEmfAttributes(sceneLanguage.getEmfAttributes(), config);
					}
				}
				validateEmfAttributes(sceneDTO.getEmfAttributes(), config);

				List<com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.SceneDTO.PlatformDTO> platformList = sceneDTO
						.getPlatforms();
				if (platformList != null) {
					for (com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.SceneDTO.PlatformDTO scenePlatform : platformList) {
						Validator.checkParameter("scene platform name", scenePlatform.getName(), config);
						Validator.checkParameter("scene platform pictureUrl", scenePlatform.getPictureUrl(), config);
						Validator.isValidUrl(scenePlatform.getPictureUrl(), "scene platform pictureUrl");
						Validator.checkParameter("scene platform pictureResolution",
								scenePlatform.getPictureResolution(), config);
					}
				}
			}
		}
			List<ChapterDTO> chapters = vodDTO.getChapters();
			if (chapters != null) {

				for (ChapterDTO chapterDTO : chapters) {
					Validator.checkParameter("chapterId", chapterDTO.getChapterId(), config);
					Validator.checkParameter("chapter startTime", chapterDTO.getStartTime(), config);
					Validator.checkParameter("chapter endTime", chapterDTO.getEndTime(), config);
					Validator.checkParameter("chapter orderId", chapterDTO.getOrderId(), config);
					Validator.checkParameter("chapter briefTitle", chapterDTO.getBriefTitle(), config);
					Validator.checkParameter("chapter metadataLanguage", chapterDTO.getMetadataLanguage(), config);

					List<MultiLanguageChapterDTO> chapterLanguages = chapterDTO.getMultiLanguageChapters();
					for (MultiLanguageChapterDTO chapterLanguage : chapterLanguages) {
						Validator.checkParameter("multi language chapter orderId", chapterLanguage.getOrderId(),
								config);
						Validator.checkParameter("multi language chapter briefTitle", chapterLanguage.getBriefTitle(),
								config);
						Validator.checkParameter("multi language chapter metadataLanguage",
								chapterLanguage.getMetadataLanguage(), config);
					}

					List<com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.ChapterDTO.PlatformDTO> platformList = chapterDTO
							.getPlatforms();
					if (platformList != null) {
						for (com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.ChapterDTO.PlatformDTO chapterPlatform : platformList) {
							Validator.checkParameter("chapter platform name", chapterPlatform.getName(), config);
							Validator.checkParameter("chapter platform pictureUrl", chapterPlatform.getPictureUrl(),
									config);
							Validator.isValidUrl(chapterPlatform.getPictureUrl(), "chapter platform pictureUrl");
							Validator.checkParameter("chapter platform pictureResolution",
									chapterPlatform.getPictureResolution(), config);
						}
					}

				}

			}
			List<MultiLanguageMetadataDTO> multiLanguageMetadataList = vodDTO.getMultiLanguageMetadata();
			if (multiLanguageMetadataList != null) {
				for (MultiLanguageMetadataDTO multiLanguageMetadataDTO : multiLanguageMetadataList) {
					Validator.checkParameter("multi language metadata languageCode",
							multiLanguageMetadataDTO.getLanguageCode(), config);
					Validator.checkParameter("multi language metadata languageName",
							multiLanguageMetadataDTO.getLanguageName(), config);
				}

			}
			List<PropertyDTO> properties = vodDTO.getProperties();
			if (properties != null) {

				for (PropertyDTO propertyDTO : properties) {
					Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.PROPERTY_NAME, propertyDTO.getPropertyName(), config);
					Validator.sanitizingParamsForSpecialParams(DcqVodIngestorConstants.VODCONSTANTSV1.PROPERTY_NAME, propertyDTO.getPropertyName());
				}
			}
			List<CopyProtectionDTO> copyProtections = vodDTO.getCopyProtections();
			if (copyProtections != null) {

				for (CopyProtectionDTO copyProtectionDTO : copyProtections) {
					Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.SECURITY_CODE, copyProtectionDTO.getSecurityCode(), config);
					Validator.sanitizingParams(DcqVodIngestorConstants.VODCONSTANTSV1.SECURITY_CODE, copyProtectionDTO.getSecurityCode());
					Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.SECURITY_OPTION, copyProtectionDTO.getSecurityOption(), config);
					Validator.sanitizingParams(DcqVodIngestorConstants.VODCONSTANTSV1.SECURITY_OPTION, copyProtectionDTO.getSecurityOption());
				}
			}
			
			List<AdvertisingInfoDTO> advertisingInfoList = vodDTO.getAdvertisingInfo();
			
			if(advertisingInfoList!=null) {
				for(AdvertisingInfoDTO advertisingInfo : advertisingInfoList) {
					Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.NETWORK_ID, advertisingInfo.getNetworkId(), config);
					Validator.sanitizingParamsForSpecialParams(DcqVodIngestorConstants.VODCONSTANTSV1.NETWORK_ID, advertisingInfo.getNetworkId());
					Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.ADVERTISING_CONTENT_ID, advertisingInfo.getAdvertisingContentId(), config);
					Validator.sanitizingParamsForSpecialParams(DcqVodIngestorConstants.VODCONSTANTSV1.ADVERTISING_CONTENT_ID, advertisingInfo.getAdvertisingContentId());
					
					List<CuePointDTO> cuepointList = advertisingInfo.getCuePoints();
					if(cuepointList!=null) {
						for(CuePointDTO cuePointDTO : cuepointList) {
							Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.CONTENT_START_TIME_POSITION, cuePointDTO.getContentStartTimePosition(), config);
							Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.AD_FORMAT, cuePointDTO.getAdFormat(), config);
							Validator.checkParameterValidityRange(DcqVodIngestorConstants.VODCONSTANTSV1.AD_FORMAT,
										DcqVodIngestorConstants.AD_FORMAT_VALUES.toArray(), cuePointDTO.getAdFormat(), config);
							
							Validator.checkParameter(DcqVodIngestorConstants.VODCONSTANTSV1.TIME_POSITION_CLASS, cuePointDTO.getTimePositionClass(), config);
							
							Validator.checkParameterValidityRange(DcqVodIngestorConstants.VODCONSTANTSV1.TIME_POSITION_CLASS,
									DcqVodIngestorConstants.TIME_POSITION_CLASS_VALUES.toArray(), cuePointDTO.getTimePositionClass(), config);
							
						}
					}
				}
			}

		
	}

	/**
	 * @param emfAttributes
	 * @param config
	 * @throws ApplicationException
	 * @throws ConfigurationException
	 */
	private void validateEmfAttributes(List<EmfAttributeDTO> emfAttributes, Configuration config)
			throws ApplicationException, ConfigurationException {

		if (emfAttributes != null) {
			for (EmfAttributeDTO emfAttribute : emfAttributes) {
				Validator.checkParameter("emfId", emfAttribute.getEmfId(), config);
				Validator.checkParameter("emfName", emfAttribute.getEmfName(), config);
				Validator.sanitizingParamsForSpecialParams("emfName", emfAttribute.getEmfName());
				Validator.checkParameter("emfValue", emfAttribute.getEmfValue(), config);
				Validator.sanitizingParamsForSpecialParams("emfValue", emfAttribute.getEmfValue());
				Validator.checkParameter("metadataLanguage", emfAttribute.getMetadataLanguage(), config);
				Validator.sanitizingParamsForSpecialParams("metadataLanguage", emfAttribute.getMetadataLanguage());
			}
		}

	}
}