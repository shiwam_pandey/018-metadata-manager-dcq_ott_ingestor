package com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author karthik.vadla
 *
 */
public class CopyProtectionDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value="Security code , if CopyProtection exists, then Security Code is manadatory",required=true,example="01")
	private String securityCode;
	
	@ApiModelProperty(value="Security option , if CopyProtection is exist then Security Option is manadatory",required=true,example="00000000")
	private String securityOption;
	
	
	public String getSecurityCode() {
		return securityCode;
	}
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
	public String getSecurityOption() {
		return securityOption;
	}
	public void setSecurityOption(String securityOption) {
		this.securityOption = securityOption;
	}
	
}
