package com.accenture.avs.be.dcq.vod.ingestor.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.avs.persistence.technicalcatalogue.ContentPlatformEntity;
/**
 * @author karthik.vadla
 *
 */
@Repository
public interface ContentPlatformRepository extends JpaRepository<ContentPlatformEntity,Integer>{

	/**
	 * @param contentId
	 * @return
	 */
	List<ContentPlatformEntity> retrieveByContentId(@Param("contentId") Integer contentId);

	/**
	 * @param bundleContentId
	 * @return
	 */
	List<String> retrievePublishedPlatformsByContentId(@Param("contentId")Integer bundleContentId);

	/**
	 * @param intValue
	 * @param platform
	 * @return
	 */
	List<Object[]> retrieveCpDetailsByContentId(int intValue, String platform);


	/**
	 * @param contentPlatformName
	 * @param numOfDays
	 * @return
	 */
	List<Integer> retrieveComingSoonContentIds(String contentPlatformName, String numOfDays);


	/**
	 * @param contentPlatformName
	 * @param numOfDays
	 * @return
	 */
	List<Integer> retrieveLeavingSoonContentIds(String contentPlatformName, String numOfDays);
	
	
	List<Integer> retrieveRecentlyAddedContentIds(String contentPlatformName, long duration);

	@Modifying
	@Transactional
	void unPublishByContentId(@Param("contentId")Integer contentId);

	Integer retriveByVideoTypeAndPlatformAndContentId(@Param("videoType")String videoName, @Param("platform")String platform, @Param("contentId")Integer contentId);
	
	List<Object[]> retrieveContentVideoTypesByContentIds(@Param("contentId")List<Integer> contentId);

}
