package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.io.Serializable;

/**
 * @author karthik.vadla
 *
 */
public class EMFAttributesDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String emfName;
	private String emfValue;
	
	
	public String getEmfName() {
		return emfName;
	}
	public void setEmfName(String emfName) {
		this.emfName = emfName;
	}
	public String getEmfValue() {
		return emfValue;
	}
	public void setEmfValue(String emfValue) {
		this.emfValue = emfValue;
	}

	
}
