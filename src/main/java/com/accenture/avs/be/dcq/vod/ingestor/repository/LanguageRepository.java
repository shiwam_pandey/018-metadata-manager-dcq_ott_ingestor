package com.accenture.avs.be.dcq.vod.ingestor.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.accenture.avs.persistence.technicalcatalogue.LanguageMetadataEntity;
/**
 * @author karthik.vadla
 *
 */
@Repository
public interface LanguageRepository extends JpaRepository<LanguageMetadataEntity,Integer>{

	List<LanguageMetadataEntity> findAll();

	Integer retrieveIdByLanguageCode(@Param("languageCode")String languageCode);
	LanguageMetadataEntity retrieveByLanguageCode(@Param("languageCode")String languageCode);


}
