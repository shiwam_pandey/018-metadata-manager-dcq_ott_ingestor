package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DeviceTypeListDTO implements Serializable

{
		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		private  List<DeviceTypeDTO> deviceType;

		public List<DeviceTypeDTO> getDeviceType() {
			if (deviceType == null) {
				deviceType = new ArrayList<DeviceTypeDTO>();
			}
			return this.deviceType;
		}

}
