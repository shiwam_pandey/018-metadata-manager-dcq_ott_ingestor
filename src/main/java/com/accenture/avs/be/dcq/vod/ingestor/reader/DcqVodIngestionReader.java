package com.accenture.avs.be.dcq.vod.ingestor.reader;

import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionReaderResponseDTO;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;

/**
 * @author karthik.vadla
 *
 */
public interface DcqVodIngestionReader {

	DcqVodIngestionReaderResponseDTO retrieveAssetIdsFromStaging(String assetType, Configuration config)
			throws ApplicationException;

	DcqVodIngestionReaderResponseDTO retrieveChannelIdsFromPlaylistStaging(String assetType, Configuration config)
			throws ApplicationException;

}
