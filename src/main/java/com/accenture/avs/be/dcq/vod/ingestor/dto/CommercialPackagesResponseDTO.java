package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class CommercialPackagesResponseDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	private List<CommercialPackage> commercialPackages;

	public List<CommercialPackage> getCommercialPackages() {
		return commercialPackages;
	}
	public void setCommercialPackages(List<CommercialPackage> commercialPackages) {
		this.commercialPackages = commercialPackages;
	}
	@Override
	public String toString() {
		return "CommercialPackagesResponse [commercialPackages=" + commercialPackages + "]";
	}
	
	public static class CommercialPackage implements Serializable{
		private static final long serialVersionUID = 1L;
		private Integer commercialPackageId;
		private String commercialPackageType;
		private String commercialPackageName;
		private String commercialPackageTitle;
		private String externalId;
		private String commercialPackageDescription;
		private Long statusId;
		private String statusName;
		private Long startDate;
		private Long endDate;
		private Long trialPeriod;
		private Long duration;
		private Boolean isBasicProfile;
		private Boolean isVisible;
		private Integer trcId;
		private String trcIsValid;
		private String commercialModel;
		private Double basePrice;
		private Double baseRecurringPrice;
		private String frequencyType;
		private Long frequencyValue;
		
		private Map<String, List<Object>> relationships;
		public Integer getCommercialPackageId() {
			return commercialPackageId;
		}
		public void setCommercialPackageId(Integer commercialPackageId) {
			this.commercialPackageId = commercialPackageId;
		}
		public String getCommercialPackageType() {
			return commercialPackageType;
		}
		public void setCommercialPackageType(String commercialPackageType) {
			this.commercialPackageType = commercialPackageType;
		}
		public String getCommercialPackageName() {
			return commercialPackageName;
		}
		public void setCommercialPackageName(String commercialPackageName) {
			this.commercialPackageName = commercialPackageName;
		}
		public String getCommercialPackageTitle() {
			return commercialPackageTitle;
		}
		public void setCommercialPackageTitle(String commercialPackageTitle) {
			this.commercialPackageTitle = commercialPackageTitle;
		}
		public String getExternalId() {
			return externalId;
		}
		public void setExternalId(String externalId) {
			this.externalId = externalId;
		}
		public String getCommercialPackageDescription() {
			return commercialPackageDescription;
		}
		public void setCommercialPackageDescription(String commercialPackageDescription) {
			this.commercialPackageDescription = commercialPackageDescription;
		}
		public Long getStatusId() {
			return statusId;
		}
		public void setStatusId(Long statusId) {
			this.statusId = statusId;
		}
		public String getStatusName() {
			return statusName;
		}
		public void setStatusName(String statusName) {
			this.statusName = statusName;
		}
		public Long getStartDate() {
			return startDate;
		}
		public void setStartDate(Long startDate) {
			this.startDate = startDate;
		}
		public Long getEndDate() {
			return endDate;
		}
		public void setEndDate(Long endDate) {
			this.endDate = endDate;
		}
		public Long getTrialPeriod() {
			return trialPeriod;
		}
		public void setTrialPeriod(Long trialPeriod) {
			this.trialPeriod = trialPeriod;
		}
		public Long getDuration() {
			return duration;
		}
		public void setDuration(Long duration) {
			this.duration = duration;
		}
		public Boolean getIsBasicProfile() {
			return isBasicProfile;
		}
		public void setIsBasicProfile(Boolean isBasicProfile) {
			this.isBasicProfile = isBasicProfile;
		}
		public Integer getTrcId() {
			return trcId;
		}
		public void setTrcId(Integer trcId) {
			this.trcId = trcId;
		}
		public String getCommercialModel() {
			return commercialModel;
		}
		public void setCommercialModel(String commercialModel) {
			this.commercialModel = commercialModel;
		}
		public Map<String, List<Object>> getRelationships() {
			return relationships;
		}
		public void setRelationships(Map<String, List<Object>> relationships) {
			this.relationships = relationships;
		}
		public String getTrcIsValid() {
			return trcIsValid;
		}
		public void setTrcIsValid(String trcIsValid) {
			this.trcIsValid = trcIsValid;
		}
		public Double getBasePrice() {
			return basePrice;
		}
		public void setBasePrice(Double basePrice) {
			this.basePrice = basePrice;
		}
		public Double getBaseRecurringPrice() {
			return baseRecurringPrice;
		}
		public void setBaseRecurringPrice(Double baseRecurringPrice) {
			this.baseRecurringPrice = baseRecurringPrice;
		}		
		public String getFrequencyType() {
			return frequencyType;
		}
		public void setFrequencyType(String frequencyType) {
			this.frequencyType = frequencyType;
		}
		public Long getFrequencyValue() {
			return frequencyValue;
		}
		public void setFrequencyValue(Long frequencyValue) {
			this.frequencyValue = frequencyValue;
		}
		
		public Boolean getIsVisible() {
			return isVisible;
		}
		public void setIsVisible(Boolean isVisible) {
			this.isVisible = isVisible;
		}
		
		@Override
		public String toString() {
			return "CommercialPackage [commercialPackageId=" + commercialPackageId + ", commercialPackageType="
					+ commercialPackageType + ", commercialPackageName=" + commercialPackageName
					+ ", commercialPackageTitle=" + commercialPackageTitle + ", externalId=" + externalId
					+ ", commercialPackageDescription=" + commercialPackageDescription + ", statusId=" + statusId
					+ ", statusName=" + statusName + ", startDate=" + startDate + ", endDate=" + endDate
					+ ", trialPeriod=" + trialPeriod + ", duration=" + duration + ", isBasicProfile=" + isBasicProfile
					+ ", isVisible=" + isVisible + ", trcId=" + trcId + ", trcIsValid=" + trcIsValid
					+ ", commercialModel=" + commercialModel + ", basePrice=" + basePrice + ", baseRecurringPrice="
					+ baseRecurringPrice + ", frequencyType=" + frequencyType + ", frequencyValue=" + frequencyValue
					+ ", relationships=" + relationships + "]";
		}
		
		
	}
}
