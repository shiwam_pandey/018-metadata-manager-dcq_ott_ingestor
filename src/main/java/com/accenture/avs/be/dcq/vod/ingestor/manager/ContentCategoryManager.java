package com.accenture.avs.be.dcq.vod.ingestor.manager;

import com.accenture.avs.be.dcq.vod.ingestor.dto.contentcategory.asset.Asset;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.contentcategory.asset.CategoryContent;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.persistence.technicalcatalogue.CategoryEntity;
import com.accenture.avs.persistence.technicalcatalogue.ContentEntity;


/**
 * @author karthik.vadla
 *
 */
public interface ContentCategoryManager {
	
public CategoryEntity retrieveCategoryByExternald(String externalId);
	
	public CategoryEntity retrieveCategoryByCategoryid(Integer categoryId);
	
	public ContentEntity retrieveContentByExternald(String externalId);
	
	public ContentEntity retrieveContentByContentid(Integer categoryId);
	
	public void ingestCategoryContent(Asset asset,String transactionNumber,Configuration config) throws ApplicationException,Exception;
	
	public void ingestCategoryContent(CategoryContent asset,String transactionNumber,Configuration config) throws ApplicationException,Exception;

}
