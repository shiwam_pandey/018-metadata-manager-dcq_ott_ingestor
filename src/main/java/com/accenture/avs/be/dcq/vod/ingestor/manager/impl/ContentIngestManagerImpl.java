package com.accenture.avs.be.dcq.vod.ingestor.manager.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionReaderResponseDTO;
import com.accenture.avs.be.dcq.vod.ingestor.manager.ContentIngestManager;
import com.accenture.avs.be.dcq.vod.ingestor.processor.DcqVodIngestionPreProcessor;
import com.accenture.avs.be.dcq.vod.ingestor.processor.DcqVodIngestionProcessor;
import com.accenture.avs.be.dcq.vod.ingestor.reader.DcqVodIngestionReader;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.commons.lib.LoggerWrapper.OtherSystemCallType;

/**
 * @author karthik.vadla
 *
 */
@Component
public class ContentIngestManagerImpl implements ContentIngestManager {
	private static LoggerWrapper log = new LoggerWrapper(ContentIngestManagerImpl.class);
	
	@Autowired
	private DcqVodIngestionPreProcessor dcqVodIngestionPreProcessor;
	
	@Autowired
	private DcqVodIngestionProcessor dcqVodIngestionProcessor;
	
	@Autowired
	private DcqVodIngestionReader dcqVodIngestionReader;
	@Autowired
	private DcqAssetStagingManager dcqAssetStagingManager;
	@Override
	public void initiateContentIngestor(String mode, String indexDate, Configuration config) {

		List<Integer> contentIds = null;
		String transactionNumber = DcqVodIngestorUtils.getTransactionNumber();
		try {

			log.logMessage("Refresh Caches,Check & CreateIndexes - Start");
			long sTime = System.currentTimeMillis();
			dcqVodIngestionPreProcessor.refreshCachesCreateIndexes(DcqVodIngestorConstants.CONTENT, mode,indexDate, config);
			log.logMessage(
					"Refresh Caches,Check & CreateIndexes - Completed,took:" + (System.currentTimeMillis() - sTime));

			long sTimeReader = System.currentTimeMillis();
			log.logMessage("Content Ingestor Reader - Start");

			DcqVodIngestionReaderResponseDTO ingestionReaderResponseDTO = dcqVodIngestionReader
					.retrieveAssetIdsFromStaging(DcqVodIngestorConstants.CONTENT, config);
			contentIds = ingestionReaderResponseDTO.getAssetIds();
			log.logMessage("Content Ingestor Reader - Completed,took: {} ms", (System.currentTimeMillis() - sTimeReader));

			if (null == contentIds || contentIds.isEmpty()) {
				log.logMessage("No ContentIds to Process, ContentIngestor Completes.");
				return;
			}

			dcqVodIngestionProcessor.processMetadataConstructESDocs(ingestionReaderResponseDTO,
					DcqVodIngestorConstants.CONTENT, transactionNumber,mode,indexDate, config);

		} catch (ApplicationException e) {
			if (null == contentIds || contentIds.isEmpty()) {
				log.logMessage(e.getMessage());
			} else {
				log.logMessage(e.getMessage());
				// TODO
				// String errorInfo = log.formatString(CallType.INTERNAL,
				// LoggerWrapper.getLoggingMethod(),
				// DcqVodIngestorUtils.getErrorStackTrace(e));
				String errorInfo = DcqVodIngestorUtils.formatString(OtherSystemCallType.INTERNAL, DcqVodIngestorUtils.getLoggingMethod(), 
						DcqVodIngestorUtils.getErrorStackTrace(e));
				dcqAssetStagingManager.updateStatus( errorInfo, contentIds,
						DcqVodIngestorConstants.CONTENT, config);
			}
		} catch (Exception e) {
			if (null == contentIds || contentIds.isEmpty()) {
				log.logMessage(e.getMessage());
			} else {
				log.logMessage("There is an error,Update the status of all ContentIds to FAILED. {}",e.getMessage());
				String errorInfo = DcqVodIngestorUtils.formatString(OtherSystemCallType.INTERNAL, DcqVodIngestorUtils.getLoggingMethod(), 
						DcqVodIngestorUtils.getErrorStackTrace(e));
				// String errorInfo = log.formatString(CallType.INTERNAL,
				// LoggerWrapper.getLoggingMethod(),
				// DcqVodIngestorUtils.getErrorStackTrace(e));
				dcqAssetStagingManager.updateStatus(errorInfo, contentIds,
						DcqVodIngestorConstants.CONTENT, config);
			}
		} 
	}

}
