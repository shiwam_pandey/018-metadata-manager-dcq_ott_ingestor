package com.accenture.avs.be.dcq.vod.ingestor.manager.impl;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.accenture.avs.be.dcq.vod.ingestor.cache.LanguageCache;
import com.accenture.avs.be.dcq.vod.ingestor.dto.ChannelIdPlaylistDateDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.DCQEventCollectorVlcDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionWriterRequestDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionWriterResponseDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.ESVLCContent;
import com.accenture.avs.be.dcq.vod.ingestor.dto.ESVLCContent.Channel;
import com.accenture.avs.be.dcq.vod.ingestor.dto.ESVLCContent.Metadata;
import com.accenture.avs.be.dcq.vod.ingestor.dto.ESVLCContent.VlcContent;
import com.accenture.avs.be.dcq.vod.ingestor.dto.VLCContentDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Asset;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.MultilangMetadata;
import com.accenture.avs.be.dcq.vod.ingestor.factories.GatewayFactory;
import com.accenture.avs.be.dcq.vod.ingestor.manager.VlcContentMetadataManager;
import com.accenture.avs.be.dcq.vod.ingestor.processor.DcqVodIngestionPostProcessor;
import com.accenture.avs.be.dcq.vod.ingestor.processor.VarnishCleanerManager;
import com.accenture.avs.be.dcq.vod.ingestor.processorplugin.DcqVodIngestionProcessorPlugin;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ChannelRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ContentRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ExtendedContentAttributesRepository;
import com.accenture.avs.be.dcq.vod.ingestor.utils.CacheConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.JsonUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.XmlHelper;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.MultiLanguageMetadataDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.VodRequestDTO;
import com.accenture.avs.be.dcq.vod.ingestor.writer.DcqVodIngestionWriter;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ConfigurationException;
import com.accenture.avs.be.framework.utils.BooleanConverter;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.persistence.technicalcatalogue.ChannelEntity;

/**
 * @author karthik.vadla
 *
 */
@Component
public class VlcContentMetadataManagerImpl implements VlcContentMetadataManager {
	private static LoggerWrapper log = new LoggerWrapper(VlcContentMetadataManagerImpl.class);
	
	@Autowired
	private ApplicationContext context;
	@Autowired
	private LanguageCache languageCache;
	
	@Autowired
	private ContentRepository contentRepository;
	
	@Autowired
	private ChannelRepository channelRepository;
	
	@Autowired
	private ExtendedContentAttributesRepository extendedContentAttributesRepository;
	@Autowired
	private DcqVodIngestionWriter dcqVodIngestionWriter;
	@Autowired
	private DcqVodIngestionPostProcessor dcqVodIngestionPostProcessor;
	@Autowired
	private VarnishCleanerManager varnishCleanerManager;
	private static JAXBContext jcAsset;
	static {
		try {
			jcAsset = JAXBContext.newInstance(Asset.class);
		} catch (Exception e) {
			log.logError(e);
		}
	}

	@Override
	public List<DcqVodIngestionWriterRequestDTO> processAndWriteVlcContents(List<ChannelIdPlaylistDateDTO> channelIdPlayList, String transactionNumber, String startTime, String mode, String indexDate, Configuration config) throws ConfigurationException, ApplicationException, JsonParseException, JsonMappingException, IOException {
		
		long sTimeWriter = System.currentTimeMillis();

		log.logMessage("VlcContent Ingestor Processor Plugin - Start");
		
		for(ChannelIdPlaylistDateDTO channelIdPlaylistDTO : channelIdPlayList) {
		List<DcqVodIngestionWriterRequestDTO> ingestionWriterRequestDTOs = constructEsDocsForVlcContent(channelIdPlaylistDTO.getChannelId(), channelIdPlaylistDTO.getPlaylistDate(), channelIdPlaylistDTO.getRowId(), transactionNumber, mode, indexDate, config);
		long sTimeProPlugin = System.currentTimeMillis();
		DcqVodIngestionProcessorPlugin dcqVodIngestionProcessorPlugin = GatewayFactory
				.getDcqVodIngestionProcessorPlugin(config);
		ingestionWriterRequestDTOs = dcqVodIngestionProcessorPlugin.updateMetadata(ingestionWriterRequestDTOs,
				DcqVodIngestorConstants.VLCCONTENT, transactionNumber, context);
		log.logMessage("VlcContent Ingestor Processor Plugin - Completed,took: {} ms",
				(System.currentTimeMillis() - sTimeProPlugin));

		log.logMessage("VlcContent Ingestor Writer - Start");

		DcqVodIngestionWriterResponseDTO dcqVodIngestionWriterResponseDTO = dcqVodIngestionWriter
				.writeToES(ingestionWriterRequestDTOs, mode, indexDate, config);
		Set<Integer> publishedIds = null;
		Map<Integer, String> failedIdsMap = null;
		if (null != dcqVodIngestionWriterResponseDTO) {
			publishedIds = dcqVodIngestionWriterResponseDTO.getPublishedIds();
			failedIdsMap = dcqVodIngestionWriterResponseDTO.getFailedIdsMap();
		}

		// Start:Added as part of Story AVS 9263
		if (null != dcqVodIngestionWriterResponseDTO
				&& !CollectionUtils.isEmpty(dcqVodIngestionWriterResponseDTO.getPublishedIds())) {
			log.logMessage("VlcContent Event Service - Start");
			int channelId = 0;
			for (DcqVodIngestionWriterRequestDTO ingestionWriterRequestDTO : ingestionWriterRequestDTOs) {

				if (dcqVodIngestionWriterResponseDTO.getPublishedIds()
						.contains(ingestionWriterRequestDTO.getEsUniqueId())) {
					ESVLCContent vlcContentDTO = (ESVLCContent) ingestionWriterRequestDTO.getEsDocObj();
					DCQEventCollectorVlcDTO vlccontent = new DCQEventCollectorVlcDTO();
					if (channelId != vlcContentDTO.getVlcContent().getChannel().getChannelId().intValue()) {
						vlccontent.setAction(DcqVodIngestorConstants.UPSERT);
						vlccontent.setContentType(DcqVodIngestorConstants.VLC_PLAYLIST);
						vlccontent.setPlaylistPublishedDate(
								vlcContentDTO.getVlcContent().getMetadata().getPlaylistPublishedDate());
						vlccontent
								.setChannelId(vlcContentDTO.getVlcContent().getChannel().getChannelId().intValue());
						channelId = vlcContentDTO.getVlcContent().getChannel().getChannelId().intValue();
						vlccontent.setExtChannelId(vlcContentDTO.getVlcContent().getChannel().getExternalId());
						String payLoad = JsonUtils.writeAsJsonStringWithoutNull(vlccontent);
						DcqVodIngestorUtils.sendEventCollectorReport(DcqVodIngestorConstants.DCQ_VLC_INGESTOR,
								DcqVodIngestorConstants.VLC_INGESTION, payLoad, config);
						log.logMessage("VlcContent Event Service - End");
					}
				}
			}

		}
		// End: AVS 9263

		log.logMessage("VlcContent Ingestor Writer - Completed,took: {} ms",
				(System.currentTimeMillis() - sTimeWriter));

		Boolean invalidateVarnish = false;
		long sTimePProcescor = System.currentTimeMillis();
		log.logMessage("VlcContent Ingestor Post Procescor - Start");
		if (null != publishedIds && !publishedIds.isEmpty()) {
			dcqVodIngestionPostProcessor.deletePublishedChannel(publishedIds, config);
			for (DcqVodIngestionWriterRequestDTO dcRequestDTO : ingestionWriterRequestDTOs) {
				Set<Integer> pubChannels = new HashSet<Integer>();
				for (Integer pubIds : publishedIds) {
					if (pubIds.equals(dcRequestDTO.getRowId())) {
						pubChannels.add(pubIds);
					}
				}
			}
			DcqVodIngestorUtils.sendPublishedReport(null, channelIdPlayList,
					startTime, DcqVodIngestorConstants.VLCCONTENT, config);
		}
		if (null != failedIdsMap && !failedIdsMap.isEmpty()) {
			dcqVodIngestionPostProcessor.updatePlaylistStatusAsFailed(failedIdsMap, config);
		}
		log.logMessage("VlcContent Ingestor Post Procescor - Completed,took: {} ms",
				(System.currentTimeMillis() - sTimePProcescor));


		Boolean invalidateVarnishInPanic = BooleanConverter
				.getBooleanValue(config.getConstants().getValue(CacheConstants.DCQ_VOD_VARNISH_INVALIDATOR_IN_PANIC));
		if (!invalidateVarnishInPanic && invalidateVarnish) {
			log.logMessage("Varnish Invalidator - Start");
			varnishCleanerManager.clean(config);
			log.logMessage("Varnish Invalidator - Completed,took: {} ms",
					(System.currentTimeMillis() - sTimePProcescor));
		}
		
		}
		return null;
	}
	
	public List<DcqVodIngestionWriterRequestDTO> constructEsDocsForVlcContent(Integer channelId,Date playListDate,Integer rowId, String transactionNumber, String mode, String indexDate, Configuration config) {
		long sTime = System.currentTimeMillis();
		log.logMessage("Constructing ES Doc(s) for VlcContent: {}:Date: {}-Start" , channelId,playListDate);
		List<DcqVodIngestionWriterRequestDTO> vlcContentDTOsList = null;
		Map<String, String> languagesCache = languageCache.getLanguages();
		
		
		try {
			boolean deleteFlag = false;
			
			for (String langCode : languagesCache.keySet()) {
				String writeAlias = DcqVodIngestorUtils.getWriteAlias(DcqVodIngestorConstants.VLCCONTENT_IN_LOWER+DcqVodIngestorConstants.UNDERSCORE+langCode.toLowerCase());
				
				DcqVodIngestorUtils.deleteVlcContentDocumentFromES(writeAlias,DcqVodIngestorConstants.ALL_IN_LOWER, channelId.longValue(), playListDate.getTime(), config);
				deleteFlag = true;
			}
			
			List<VLCContentDTO> vlcContentDTOs= getVlcContentDTO(channelId, playListDate);
					
			ChannelEntity channelEntity=channelRepository.retreiveChannelByChannelId(channelId);
			
			//Start:Added as part of Story AVS 9263
			if(deleteFlag){
						
						DCQEventCollectorVlcDTO vlccontent = new DCQEventCollectorVlcDTO();						
						vlccontent.setAction(DcqVodIngestorConstants.DELETE);
						vlccontent.setContentType(DcqVodIngestorConstants.VLC_PLAYLIST);
						vlccontent.setPlaylistPublishedDate(playListDate.getTime());
						vlccontent.setChannelId(channelId);
						if(null != channelEntity && StringUtils.isNotBlank(channelEntity.getExternalId())){
						vlccontent.setExtChannelId(channelEntity.getExternalId());
						}
						String payLoad = JsonUtils.writeAsJsonStringWithoutNull(vlccontent);
										
						DcqVodIngestorUtils.sendEventCollectorReport(DcqVodIngestorConstants.DCQ_VLC_INGESTOR,
																		DcqVodIngestorConstants.VLC_INGESTION,
																		payLoad, config);
						deleteFlag=false;
			}
			//End: AVS 9263
			
			
			
			
			Map<String, ESVLCContent> langSpecificMap=null;
			
			if (vlcContentDTOs != null && !vlcContentDTOs.isEmpty()) {
				
				vlcContentDTOsList = new ArrayList<DcqVodIngestionWriterRequestDTO>(); 
				langSpecificMap=new HashMap<String,ESVLCContent>();
				for (VLCContentDTO vlcContentDTO : vlcContentDTOs) {

					byte[] byteArr = null;

					if (vlcContentDTO.getContentId() != null) {
						byteArr = extendedContentAttributesRepository.retrieveContentXmlBlob(vlcContentDTO.getContentId().intValue());
					}
					langSpecificMap.clear();
					Asset asset = null;
					if(null != byteArr){
						try {
							asset = (Asset) XmlHelper.latestUnMarshaller(jcAsset, byteArr);
							addLangSpecificES(langSpecificMap, vlcContentDTO, asset, channelId, playListDate, channelEntity);
							
							for (String langAndCpId : langSpecificMap.keySet()) {
								String langCpId[]=langAndCpId.split(DcqVodIngestorConstants.HYPHEN_SYMBOL);
								String writeAlias = DcqVodIngestorUtils.getWriteAlias(DcqVodIngestorConstants.VLCCONTENT_IN_LOWER
										+ DcqVodIngestorConstants.UNDERSCORE + langCpId[0].toLowerCase());
								DcqVodIngestionWriterRequestDTO dcqVodIngestionWriterRequestDTO = new DcqVodIngestionWriterRequestDTO();
								dcqVodIngestionWriterRequestDTO.setEsDocObj(langSpecificMap.get(langCpId[0]+DcqVodIngestorConstants.HYPHEN_SYMBOL+vlcContentDTO.getCpId()));
								dcqVodIngestionWriterRequestDTO.setRowId(rowId);
								dcqVodIngestionWriterRequestDTO.setEsIndex(writeAlias);
								dcqVodIngestionWriterRequestDTO.setEsUniqueId(rowId);
								dcqVodIngestionWriterRequestDTO.setEsIndexType(DcqVodIngestorConstants.ALL_IN_LOWER);
								dcqVodIngestionWriterRequestDTO.setPlatformName(vlcContentDTO.getPlatform().toLowerCase());
								vlcContentDTOsList.add(dcqVodIngestionWriterRequestDTO);
								dcqVodIngestionWriterRequestDTO.setSourceFile(asset);
							}
						} catch(JAXBException jaxbe) {
							String text = new String(byteArr, StandardCharsets.UTF_8);
							VodRequestDTO vodDTO = JsonUtils.parseJson(text, VodRequestDTO.class);
							addLangSpecificESJson(langSpecificMap, vlcContentDTO, vodDTO, channelId, playListDate, channelEntity);
							
							for (String langAndCpId : langSpecificMap.keySet()) {
								String langCpId[]=langAndCpId.split(DcqVodIngestorConstants.HYPHEN_SYMBOL);
								String writeAlias = DcqVodIngestorUtils.getWriteAlias(DcqVodIngestorConstants.VLCCONTENT_IN_LOWER
										+ DcqVodIngestorConstants.UNDERSCORE + langCpId[0].toLowerCase());
								DcqVodIngestionWriterRequestDTO dcqVodIngestionWriterRequestDTO = new DcqVodIngestionWriterRequestDTO();
								dcqVodIngestionWriterRequestDTO.setEsDocObj(langSpecificMap.get(langCpId[0]+DcqVodIngestorConstants.HYPHEN_SYMBOL+vlcContentDTO.getCpId()));
								dcqVodIngestionWriterRequestDTO.setRowId(rowId);
								dcqVodIngestionWriterRequestDTO.setEsIndex(writeAlias);
								dcqVodIngestionWriterRequestDTO.setEsUniqueId(rowId);
								dcqVodIngestionWriterRequestDTO.setEsIndexType(DcqVodIngestorConstants.ALL_IN_LOWER);
								dcqVodIngestionWriterRequestDTO.setPlatformName(vlcContentDTO.getPlatform().toLowerCase());
								vlcContentDTOsList.add(dcqVodIngestionWriterRequestDTO);
								dcqVodIngestionWriterRequestDTO.setSourceFile(vodDTO);
							}
						}
						
					}
				}
			}
				 
		} catch (Exception e) {
			log.logError(e);
		}
		log.logMessage("Constructing ES Doc(s) for VlcContent: {} Pub Date: {} -Completed,ES Docs count: {},took: {}" , channelId,playListDate , (vlcContentDTOsList == null ? 0 : vlcContentDTOsList.size()) 
				, (System.currentTimeMillis() - sTime) );
		
		log.logMethodEnd(System.currentTimeMillis()-sTime);
		return vlcContentDTOsList;
	
	}
	
	
	private List<VLCContentDTO> getVlcContentDTO(Integer channelId, Date playListDate) {
		List<VLCContentDTO> vlcContentDTOs=new ArrayList<VLCContentDTO>();
		List<Object[]> results = null;
		if (channelId != null) {
			results = contentRepository.retrieveContentDetailsByChannelId(channelId, playListDate);
		}

		if (results != null && !results.isEmpty()) {
			
			VLCContentDTO vlcContentDTO=null;
			
			for (int i = 0; i < results.size(); i++) {
				
				vlcContentDTO=new VLCContentDTO();
				vlcContentDTOs.add(vlcContentDTO);
				
				Object[] objs = results.get(i);
				if (objs[0] != null) {
					vlcContentDTO.setContentId((Long.parseLong(objs[0].toString())));
				}
				if (objs[1] != null) {
					vlcContentDTO.setCpId((Long.parseLong(objs[1].toString())));
				}
				if (objs[2] != null) {
					vlcContentDTO.setVideoType(objs[2].toString());
				}
				if (objs[3] != null) {
					vlcContentDTO.setStartTime((Date) objs[3]);
				}
				if (objs[4] != null) {
					vlcContentDTO.setEndTime((Date) objs[4]);
				}
				if (objs[5] != null) {
					vlcContentDTO.setPcLevel(objs[5].toString());
				}
				
				if (objs[6] != null) {
					vlcContentDTO.setPlatform(objs[6].toString());
				}
			}
			
			
		}
		
		return vlcContentDTOs;
	}


	private void addLangSpecificES(Map<String, ESVLCContent> langSpecificMap, VLCContentDTO vlcContentDTO, Asset asset,
			Integer channelId, Date playListDate, ChannelEntity channelEntity) throws JAXBException {

		Map<String, String> languagesCache = languageCache.getLanguages();
		
		if (null != languagesCache && !languagesCache.isEmpty()) {
			for (String langCode : languagesCache.keySet()) {
				ESVLCContent esvlcContent = new ESVLCContent();
				langSpecificMap.put(langCode+DcqVodIngestorConstants.HYPHEN_SYMBOL+vlcContentDTO.getCpId(), esvlcContent);
				VlcContent vlcContent = new VlcContent();
				vlcContent.setContentId(vlcContentDTO.getContentId());
				if(vlcContentDTO.getPlatform()!=null){
					vlcContent.setPlatformName(vlcContentDTO.getPlatform().toLowerCase());
				}
				
				Metadata metadata = new Metadata();
				metadata.setContentId(vlcContentDTO.getContentId());
				metadata.setCpId(vlcContentDTO.getCpId());
				
				if (vlcContentDTO.getEndTime() != null) { 
					metadata.setEndTime(vlcContentDTO.getEndTime().getTime());
				}

				if (playListDate != null) {
					metadata.setPlaylistPublishedDate(playListDate.getTime());
				}

				if (vlcContentDTO.getStartTime() != null) {
					metadata.setStartTime(vlcContentDTO.getStartTime().getTime());
				}
				metadata.setPcLevel(vlcContentDTO.getPcLevel());
				metadata.setVideoType(vlcContentDTO.getVideoType());
				
				if(asset!=null){
					metadata.setContentTitle(asset.getTitle());
					metadata.setTitleBrief(asset.getTitleBrief());
				}
				
				Map<String, MultilangMetadata> assetMultiLangMap = null;
				if (null != asset && null != asset.getMultilangMetadataList()
						&& null != asset.getMultilangMetadataList().getMultilangMetadata()
						&& asset.getMultilangMetadataList().getMultilangMetadata().size() > 0) {
					assetMultiLangMap = new HashMap<String, MultilangMetadata>();
					for (MultilangMetadata multilangMetadata : asset.getMultilangMetadataList().getMultilangMetadata()) {
						assetMultiLangMap.put(multilangMetadata.getLangCode(), multilangMetadata);
					}
				}
				if (null != assetMultiLangMap && !assetMultiLangMap.isEmpty()) {
					MultilangMetadata mulLangMetadata = assetMultiLangMap.get(langCode);
					if (null != mulLangMetadata) {
						metadata.setContentTitle(mulLangMetadata.getTitle());
						metadata.setTitleBrief(mulLangMetadata.getTitleBrief());
					}
				}

				if (vlcContentDTO.getContentId() != null) {
					if (asset != null) {
						metadata.setDuration((long) asset.getDuration());
						if (asset.getIsEncrypted() != null
								&& asset.getIsEncrypted().toString().trim().equalsIgnoreCase(DcqVodIngestorConstants.SHORT_YES)) {
							metadata.setIsEncrypted(true);
						} else {
							metadata.setIsEncrypted(false);
						}
						if (asset.getGeoBlocking() != null && asset.getGeoBlocking().getDefault() != null
								&& asset.getGeoBlocking().getDefault().toString().trim().equalsIgnoreCase(DcqVodIngestorConstants.SHORT_YES)) {
							metadata.setIsGeoBlocked(true);
						} else {
							metadata.setIsGeoBlocked(false);
						}
						metadata.setObjectSubType(asset.getObjectSubtype());
						metadata.setObjectType(asset.getObjectType());
						
						Channel channel = new Channel();
						channel.setChannelId(channelId.longValue());
						channel.setChannelName(channelEntity.getName());
						channel.setAdvTags(channelEntity.getAdvTags());
						channel.setExternalId(channelEntity.getExternalId());						
						vlcContent.setChannel(channel);
						vlcContent.setMetadata(metadata);
						esvlcContent.setVlcContent(vlcContent);
					}
				}
				
			}

		}
	}
	
	private void addLangSpecificESJson(Map<String, ESVLCContent> langSpecificMap, VLCContentDTO vlcContentDTO, VodRequestDTO vodDTO,
			Integer channelId, Date playListDate, ChannelEntity channelEntity) throws JAXBException {

		Map<String, String> languagesCache = languageCache.getLanguages();
		
		if (null != languagesCache && !languagesCache.isEmpty()) {
			for (String langCode : languagesCache.keySet()) {
				ESVLCContent esvlcContent = new ESVLCContent();
				langSpecificMap.put(langCode+DcqVodIngestorConstants.HYPHEN_SYMBOL+vlcContentDTO.getCpId(), esvlcContent);
				VlcContent vlcContent = new VlcContent();
				vlcContent.setContentId(vlcContentDTO.getContentId());
				if(vlcContentDTO.getPlatform()!=null){
					vlcContent.setPlatformName(vlcContentDTO.getPlatform().toLowerCase());
				}
				
				Metadata metadata = new Metadata();
				metadata.setContentId(vlcContentDTO.getContentId());
				metadata.setCpId(vlcContentDTO.getCpId());
				
				if (vlcContentDTO.getEndTime() != null) { 
					metadata.setEndTime(vlcContentDTO.getEndTime().getTime());
				}

				if (playListDate != null) {
					metadata.setPlaylistPublishedDate(playListDate.getTime());
				}

				if (vlcContentDTO.getStartTime() != null) {
					metadata.setStartTime(vlcContentDTO.getStartTime().getTime());
				}
				metadata.setPcLevel(vlcContentDTO.getPcLevel());
				metadata.setVideoType(vlcContentDTO.getVideoType());
				
				if(vodDTO!=null){
					metadata.setContentTitle(vodDTO.getTitle());
					metadata.setTitleBrief(vodDTO.getTitleBrief());
				}
				
				Map<String, MultiLanguageMetadataDTO> assetMultiLangMap = null;
				if (null != vodDTO && null != vodDTO.getMultiLanguageMetadata()
							&& vodDTO.getMultiLanguageMetadata().size() > 0) {
					assetMultiLangMap = new HashMap<String, MultiLanguageMetadataDTO>();
					for (MultiLanguageMetadataDTO multiLanguageMetadataDTO : vodDTO.getMultiLanguageMetadata()) {
						assetMultiLangMap.put(multiLanguageMetadataDTO.getLanguageCode(), multiLanguageMetadataDTO);
					}
				}
				if (null != assetMultiLangMap && !assetMultiLangMap.isEmpty()) {
					MultiLanguageMetadataDTO mulLangMetadata = assetMultiLangMap.get(langCode);
					if (null != mulLangMetadata) {
						metadata.setContentTitle(mulLangMetadata.getTitle());
						metadata.setTitleBrief(mulLangMetadata.getTitleBrief());
					}
				}

				if (vlcContentDTO.getContentId() != null) {
					if (vodDTO != null) {
						metadata.setDuration((long) vodDTO.getDuration());
						if (vodDTO.getIsEncrypted() != null
								&& vodDTO.getIsEncrypted().toString().trim().equalsIgnoreCase(DcqVodIngestorConstants.SHORT_YES)) {
							metadata.setIsEncrypted(true);
						} else {
							metadata.setIsEncrypted(false);
						}
						if (vodDTO.getIsGeoBlocked() != null && vodDTO.getIsGeoBlocked().trim().equalsIgnoreCase(DcqVodIngestorConstants.SHORT_YES)) {
							metadata.setIsGeoBlocked(true);
						} else {
							metadata.setIsGeoBlocked(false);
						}
						metadata.setObjectSubType(vodDTO.getContentSubType());
						metadata.setObjectType(vodDTO.getContentType());
						
						Channel channel = new Channel();
						channel.setChannelId(channelId.longValue());
						channel.setChannelName(channelEntity.getName());
						channel.setAdvTags(channelEntity.getAdvTags());
						channel.setExternalId(channelEntity.getExternalId());						
						vlcContent.setChannel(channel);
						vlcContent.setMetadata(metadata);
						esvlcContent.setVlcContent(vlcContent);
					}
				}
				
			}

		}
	}
}


	
