package com.accenture.avs.be.dcq.vod.ingestor.manager;

import java.io.IOException;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import com.accenture.avs.be.dcq.vod.ingestor.dto.ChannelIdPlaylistDateDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionWriterRequestDTO;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ConfigurationException;

/**
 * @author karthik.vadla
 *
 */
public interface VlcContentMetadataManager {

	List<DcqVodIngestionWriterRequestDTO> processAndWriteVlcContents(List<ChannelIdPlaylistDateDTO> channelIdPlayList, String transactionNumber, String startTime, String mode, String indexDate, Configuration config) throws ConfigurationException, ApplicationException, JsonParseException, JsonMappingException, IOException;
	/*List<DcqVodIngestionWriterRequestDTO> constructEsDocsForVlcContent(Integer channelId, Date playlistDate,
			Integer rowId, String transactionNumber, String mode, String indexDate, Configuration config);*/

}
