package com.accenture.avs.be.dcq.vod.ingestor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.accenture.avs.persistence.technicalcatalogue.ContentPopularityEntity;

@Repository
public interface ContentPopularityRepository extends JpaRepository<ContentPopularityEntity, Integer> {

	public ContentPopularityEntity findByContentId(int contentId);
}

