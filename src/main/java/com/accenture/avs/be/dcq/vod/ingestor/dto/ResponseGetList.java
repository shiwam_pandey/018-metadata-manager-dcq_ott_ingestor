package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.accenture.avs.be.framework.bean.GenericResponse;


@SuppressWarnings("rawtypes")
public class ResponseGetList extends GenericResponse{

	private String resultCode;
	private String errorCode;
	private String errorDescription;
	private String message;
	private List resultObj;

	public final static String RESULT_CODE_OK = "OK";
	public final static String RESULT_CODE_KO = "KO";
	public final static String NULL = "";

	/**
	 * Creates response bean with default OK.
	 */
	public ResponseGetList() {
		this.resultCode = RESULT_CODE_OK;
		this.errorCode = NULL;
		this.errorDescription = NULL;
		this.message = NULL;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Map getResultObj() {
		Map<String,List> map = new HashMap<String,List>();
		map.put("packageDetails", resultObj);
		return map;
	}

	public void setResultObj(List resultObj) {
		this.resultObj = resultObj;
	}

}
