package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author karthik.vadla
 *
 */
public class DcqVodIngestionReaderResponseDTO implements Serializable{
	private static final long serialVersionUID = -460053666626358917L;
	private List<Integer> assetIds;
	private List<ChannelIdPlaylistDateDTO> channelIdPlaylistDateDTOList;
	private String startTime;
	public List<Integer> getAssetIds() {
		return assetIds;
	}
	public void setAssetIds(List<Integer> assetIds) {
		this.assetIds = assetIds;
	}
	public List<ChannelIdPlaylistDateDTO> getChannelIdPlaylistDateDTOList() {
		return channelIdPlaylistDateDTOList;
	}
	public void setChannelIdPlaylistDateDTOList(List<ChannelIdPlaylistDateDTO> channelIdPlaylistDateDTOList) {
		this.channelIdPlaylistDateDTOList = channelIdPlaylistDateDTOList;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	
}
