package com.accenture.avs.be.dcq.vod.ingestor.manager;

import java.io.IOException;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionWriterRequestDTO;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ConfigurationException;

/**
 * @author karthik.vadla
 *
 */
public interface VodChannelMetadataManager {

	List<DcqVodIngestionWriterRequestDTO> processAndWriteVodChannels(List<Integer> assetIdList, String transactionNumber,
			String startTime, String mode, String indexDate, Configuration config) throws ConfigurationException, ApplicationException, JsonParseException, JsonMappingException, IOException, JAXBException;
	
	/*List<DcqVodIngestionWriterRequestDTO> constructEsDocsForVodChannel(Integer assetId, String transactionNumber,
			String mode, String indexDate, Configuration config);*/

}
