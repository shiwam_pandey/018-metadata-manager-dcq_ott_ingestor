package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.io.Serializable;

public class CopyProtectionSchemeOption implements Serializable{
	
	/**
	 * @author naga.sireesha.meka
	 *
	 */
	private static final long serialVersionUID = 1L;
	private String securityOption;
	private String optionDescription;

	
	public CopyProtectionSchemeOption() {
		super();
	}

	public CopyProtectionSchemeOption(String securityOption, String optionDescription) {
		super();
		this.securityOption = securityOption;
		this.optionDescription = optionDescription;
	}


	public String getSecurityOption() {
		return securityOption;
	}



	public void setSecurityOption(String securityOption) {
		this.securityOption = securityOption;
	}



	public String getOptionDescription() {
		return optionDescription;
	}



	public void setOptionDescription(String optionDescription) {
		this.optionDescription = optionDescription;
	}



}
