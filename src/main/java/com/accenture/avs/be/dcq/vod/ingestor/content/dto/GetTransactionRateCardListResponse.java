package com.accenture.avs.be.dcq.vod.ingestor.content.dto;

import java.io.Serializable;
import java.util.List;



/**
 * @author kavya.sree.vanam GetTransactionRateCardResponse
 *
 */
public class GetTransactionRateCardListResponse implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long totalResults;
    private List<GetTransactionRateCardResponse> transactionRateCardList;
    
    
    public List<GetTransactionRateCardResponse> getTransactionRateCardList() {
        return transactionRateCardList;
    }

    
    public void setTransactionRateCardList(List<GetTransactionRateCardResponse> transactionRateCardList) {
        this.transactionRateCardList = transactionRateCardList;
    }

    public Long getTotalResults() {
        return totalResults;
    }
    
    public void setTotalResults(Long totalResults) {
        this.totalResults = totalResults;
    }

    
}
