package com.accenture.avs.be.dcq.vod.ingestor.sdp.manager;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;

import com.accenture.avs.be.dcq.vod.ingestor.content.dto.TRCEnablerRequest;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.SolutionOfferList;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ConfigurationException;
import com.accenture.avs.persistence.technicalcatalogue.ContentEntity;
import com.accenture.avs.persistence.technicalcatalogue.ContentPlatformEntity;

public interface SDPManager {

	void callToSDP(SolutionOfferList solutionOfferList, List<TRCEnablerRequest> trcEnablerRequest,
			ContentEntity content,Set<ContentPlatformEntity> contentPlatforms, List<String> assetPropertiesList, Configuration config) throws ConfigurationException, ApplicationException, JsonGenerationException, JsonMappingException, IOException;

	Map<String, Long> searchAllDeviceChannels(Configuration config) throws ConfigurationException, MalformedURLException, JsonGenerationException, JsonMappingException, IOException;

}
