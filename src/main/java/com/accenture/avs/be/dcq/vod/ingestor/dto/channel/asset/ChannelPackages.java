package com.accenture.avs.be.dcq.vod.ingestor.dto.channel.asset;

import java.io.Serializable;
import java.util.List;

public class ChannelPackages implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7026157558718897951L;
	private String type;
	private String title;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getPriceCategoryName() {
		return priceCategoryName;
	}
	public void setPriceCategoryName(String priceCategoryName) {
		this.priceCategoryName = priceCategoryName;
	}
	public String getPackageId() {
		return packageId;
	}
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}
	
	private String priceCategoryName;
	private String externalId;
	public String getExternalId() {
		return externalId;
	}
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	private String startDate;
	private String endDate;
	private String packageId;
	private List<Integer> channelIds;
	public List<Integer> getChannelIds() {
		return channelIds;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public void setChannelIds(List<Integer> channelIds) {
		this.channelIds = channelIds;
	}

}
