package com.accenture.avs.be.dcq.vod.ingestor.service;

import com.accenture.avs.be.dcq.vod.ingestor.dto.ResponseGetList;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ConfigurationException;

/**
 * @author karthik.vadla
 *
 */
public interface PackageService {

	/**
	 * @param propertyName
	 * @param startIndex
	 * @param maxResults
	 * @param sortby
	 * @param sortOrder
	 * @param config
	 * @return
	 * @throws ApplicationException
	 * @throws ConfigurationException
	 * @throws Exception
	 */
	public GenericResponse getPackages(String propertyName, String startIndex, String maxResults, String sortby, String sortOrder, Configuration config) throws ApplicationException, ConfigurationException, Exception;

	
	/**
	 * @param propertyName
	 * @param config
	 * @return
	 * @throws ApplicationException
	 * @throws Exception 
	 * @throws ConfigurationException 
	 */
	public ResponseGetList getPackagesForLegacy(String propertyName,Configuration config) throws ApplicationException, ConfigurationException, Exception;

}
