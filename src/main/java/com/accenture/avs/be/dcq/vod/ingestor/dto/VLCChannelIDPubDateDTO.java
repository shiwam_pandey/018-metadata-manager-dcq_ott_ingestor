package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.util.Date;

/**
 * @author karthik.vadla
 *
 */
public class VLCChannelIDPubDateDTO {
	
	private Integer channelId;
	private Date playListPubDate;
	
	
	
	
	public VLCChannelIDPubDateDTO(Integer channelId, Date playListPubDate) {
		this.channelId = channelId;
		this.playListPubDate = playListPubDate;
	}
	
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "VLCChannelIDPubDateDTO [channelId=" + channelId + ", playListPubDate=" + playListPubDate + "]";
	}





	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((channelId == null) ? 0 : channelId.hashCode());
		result = prime * result + ((playListPubDate == null) ? 0 : playListPubDate.hashCode());
		return result;
	}





	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		boolean flag = true;
		if (this == obj) {
			flag = true;
		}
		if (obj == null) {
			flag = false;
		} else {
			if (getClass() != obj.getClass()) {
				flag = false;
			}
			VLCChannelIDPubDateDTO other = (VLCChannelIDPubDateDTO) obj;
			if (channelId == null) {
				if (other.channelId != null) {
					flag = false;
				}
			} else if (!channelId.equals(other.channelId)) {
				flag = false;
			}
			if (playListPubDate == null) {
				if (other.playListPubDate != null) {
					flag = false;
				}
			} else if (!playListPubDate.equals(other.playListPubDate)) {

				flag = false;
			}
		}
		return flag;
	}





	/**
	 * @return the channelId
	 */
	public Integer getChannelId() {
		return channelId;
	}
	/**
	 * @param channelId the channelId to set
	 */
	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}
	/**
	 * @return the playListPubDate
	 */
	public Date getPlayListPubDate() {
		return playListPubDate;
	}
	/**
	 * @param playListPubDate the playListPubDate to set
	 */
	public void setPlayListPubDate(Date playListPubDate) {
		this.playListPubDate = playListPubDate;
	}
	
	

}
