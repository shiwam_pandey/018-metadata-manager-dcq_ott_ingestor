package com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author karthik.vadla
 *
 */
public class SceneDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value="Unique Identifier of scene",required=true,example="33641")
	private Long sceneId;
	
	@ApiModelProperty(value="Start time of scene",required=true,example=" 00:00:00")
	private String startTime;
	
	@ApiModelProperty(value="End time of scene",required=true,example="00:30:00")
	private String endTime;
	
	@ApiModelProperty(value="Title of scene",required=true,example="PK introduction")
	private String title;
	
	@ApiModelProperty(value="Brief title of scene",required=true,example="PK first scene")
	private String titleBrief;
	
	@ApiModelProperty(value="Keyword associated to scene",required=true,example="pk|aamir")
	private String keywords;
	
	@ApiModelProperty(value="Genre of scene",required=true,example="Action")
	private String genre;
	
	@ApiModelProperty(value="Order id of scene",required=true,example="1")
	private Long orderId;
	
	@ApiModelProperty(value="Ad tag associated to scene",required=true,example="PK")
	private String adTag;
	
	@ApiModelProperty(value="Event associated to scene",required=true,example="introduction")
	private String event;
	
	@ApiModelProperty(value="Action associated to scene",required=true,example="action")
	private String action;
	
	@ApiModelProperty(value="Location associated to scene",required=true,example="mumbai")
	private String location;
	
	@ApiModelProperty(value="Other location associated to scene",required=true,example="rajasthan")
	private String extendedLocation;
	
	@ApiModelProperty(value="Name of characters appearing in specific scene’s",required=true,example="PK|Aamir")
	private String characters;
	
	@ApiModelProperty(value="Name of actors appearing in specific scene’s",required=true,example="Aamir Khan")
	private String actors;
	
	@ApiModelProperty(value="Company represented in specific scene’s",required=true,example="ARKA")
	private String company;
	
	@ApiModelProperty(value="Soundtrack playing during scene",required=true,example="PK_hindi.mp4")
	private String audio;
	
	@ApiModelProperty(value="Musician for Sound track being played",required=true,example="pk music")
	private String musicians;
	
	@ApiModelProperty(value="Default language used during scene",required=true,example="ENG")
	private String metadataLanguage;
	
	@ApiModelProperty(value="List of EMFAttributes",required=false,example="")
	private List<EmfAttributeDTO> emfAttributes;
	
	@ApiModelProperty(value="ExtendedMetadata ,Contains a json object with custom fields for customization purpose.",required=false,example="{\"plot\":\"On a far planet...\",\"actorsListDetail\":[{\"Name\":\"Sam Worthington\",\"BirthDate\":\"03/02/1981\",\"BirthCity\":\"New York\"},{\"Name\":\"Sigourney Weaver\",\"BirthDate\":\"04/03/1955\",\"BirthCity\":\"New York\"}]}")
	private String extendedMetadata;
	
	@ApiModelProperty(value="Meta Data for different languages",required=false,example="")
	private List<MultiLanguageSceneDTO> multiLanguageScenes;
	
	@ApiModelProperty(value="List of Platform details",required=false,example="")
	private List<PlatformDTO> platforms;
	
	
	public Long getSceneId() {
		return sceneId;
	}
	public void setSceneId(Long sceneId) {
		this.sceneId = sceneId;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTitleBrief() {
		return titleBrief;
	}
	public void setTitleBrief(String titleBrief) {
		this.titleBrief = titleBrief;
	}
	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public String getAdTag() {
		return adTag;
	}
	public void setAdTag(String adTag) {
		this.adTag = adTag;
	}
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getExtendedLocation() {
		return extendedLocation;
	}
	public void setExtendedLocation(String extendedLocation) {
		this.extendedLocation = extendedLocation;
	}
	public String getCharacters() {
		return characters;
	}
	public void setCharacters(String characters) {
		this.characters = characters;
	}
	public String getActors() {
		return actors;
	}
	public void setActors(String actors) {
		this.actors = actors;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getAudio() {
		return audio;
	}
	public void setAudio(String audio) {
		this.audio = audio;
	}
	public String getMusicians() {
		return musicians;
	}
	public void setMusicians(String musicians) {
		this.musicians = musicians;
	}
	public String getMetadataLanguage() {
		return metadataLanguage;
	}
	public void setMetadataLanguage(String metadataLanguage) {
		this.metadataLanguage = metadataLanguage;
	}
	public List<EmfAttributeDTO> getEmfAttributes() {
		return emfAttributes;
	}
	public void setEmfAttributes(List<EmfAttributeDTO> emfAttributes) {
		this.emfAttributes = emfAttributes;
	}
	public String getExtendedMetadata() {
		return extendedMetadata;
	}
	public void setExtendedMetadata(String extendedMetadata) {
		this.extendedMetadata = extendedMetadata;
	}
	public List<MultiLanguageSceneDTO> getMultiLanguageScenes() {
		return multiLanguageScenes;
	}
	public void setMultiLanguageScenes(List<MultiLanguageSceneDTO> multiLanguageScenes) {
		this.multiLanguageScenes = multiLanguageScenes;
	}
	public List<PlatformDTO> getPlatforms() {
		return platforms;
	}
	public void setPlatforms(List<PlatformDTO> platforms) {
		this.platforms = platforms;
	}
	public static class MultiLanguageSceneDTO implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		@ApiModelProperty(value="scene details based on language",required=true,example="pk introduction")
		private String title;
		
		@ApiModelProperty(value="Brief title of scene",required=true,example="pk introduction")
		private String titleBrief;
		
		@ApiModelProperty(value="Keyword associated to scene",required=true,example="pk introduction")
		private String keywords;
		
		@ApiModelProperty(value="Genre of scene",required=true,example="Action")
		private String genre;
		
		@ApiModelProperty(value="Order id of scene",required=true,example="1")
		private Long orderId;
		
		@ApiModelProperty(value="Ad tag associated to scene",required=true,example="Pk introduction")
		private String adTag;
		
		@ApiModelProperty(value="Event associated to scene",required=true,example="pk introduction")
		private String event;
		
		@ApiModelProperty(value="Action associated to scene",required=true,example="pk introduction fight")
		private String action;
		
		@ApiModelProperty(value="Location associated to scene",required=true,example="mumbai")
		private String location;
		
		@ApiModelProperty(value="Other location associated to scene",required=true,example="pune")
		private String extendedLocation;
		
		@ApiModelProperty(value="Name of characters appearing in specific scene’s",required=true,example="Aamir Khan")
		private String characters;
		
		@ApiModelProperty(value="Name of actors appearing in specific scene’s",required=true,example="Aamir Khan")
		private String actors;
		
		@ApiModelProperty(value="Company represented in specific scene’s",required=true,example="ARKA")
		private String company;
		
		@ApiModelProperty(value="Soundtrack playing during scene",required=true,example="pk_telugu.mp4")
		private String audio;
		
		@ApiModelProperty(value="Musician for Sound track being played",required=true,example="Music media")
		private String musicians;
		
		@ApiModelProperty(value="Default language used during scene",required=true,example="English")
		private String metadataLanguage;
		
		@ApiModelProperty(value="ExtendedMetadata , Contains a json object with custom fields for customization purpose",required=false,example="{\"plot\":\"On a far planet...\",\"actorsListDetail\":[{\"Name\":\"Sam Worthington\",\"BirthDate\":\"03/02/1981\",\"BirthCity\":\"New York\"},{\"Name\":\"Sigourney Weaver\",\"BirthDate\":\"04/03/1955\",\"BirthCity\":\"New York\"}]}")
		private String extendedMetadata;
		
		@ApiModelProperty(value="List of EMFAttributes",required=false,example="")
		private List<EmfAttributeDTO> emfAttributes;
		
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getTitleBrief() {
			return titleBrief;
		}
		public void setTitleBrief(String titleBrief) {
			this.titleBrief = titleBrief;
		}
		public String getKeywords() {
			return keywords;
		}
		public void setKeywords(String keywords) {
			this.keywords = keywords;
		}
		public String getGenre() {
			return genre;
		}
		public void setGenre(String genre) {
			this.genre = genre;
		}
		public Long getOrderId() {
			return orderId;
		}
		public void setOrderId(Long orderId) {
			this.orderId = orderId;
		}
		public String getAdTag() {
			return adTag;
		}
		public void setAdTag(String adTag) {
			this.adTag = adTag;
		}
		public String getEvent() {
			return event;
		}
		public void setEvent(String event) {
			this.event = event;
		}
		public String getAction() {
			return action;
		}
		public void setAction(String action) {
			this.action = action;
		}
		public String getLocation() {
			return location;
		}
		public void setLocation(String location) {
			this.location = location;
		}
		public String getExtendedLocation() {
			return extendedLocation;
		}
		public void setExtendedLocation(String extendedLocation) {
			this.extendedLocation = extendedLocation;
		}
		public String getCharacters() {
			return characters;
		}
		public void setCharacters(String characters) {
			this.characters = characters;
		}
		public String getActors() {
			return actors;
		}
		public void setActors(String actors) {
			this.actors = actors;
		}
		public String getCompany() {
			return company;
		}
		public void setCompany(String company) {
			this.company = company;
		}
		public String getAudio() {
			return audio;
		}
		public void setAudio(String audio) {
			this.audio = audio;
		}
		public String getMusicians() {
			return musicians;
		}
		public void setMusicians(String musicians) {
			this.musicians = musicians;
		}
		public String getMetadataLanguage() {
			return metadataLanguage;
		}
		public void setMetadataLanguage(String metadataLanguage) {
			this.metadataLanguage = metadataLanguage;
		}
		public String getExtendedMetadata() {
			return extendedMetadata;
		}
		public void setExtendedMetadata(String extendedMetadata) {
			this.extendedMetadata = extendedMetadata;
		}
		public List<EmfAttributeDTO> getEmfAttributes() {
			return emfAttributes;
		}
		public void setEmfAttributes(List<EmfAttributeDTO> emfAttributes) {
			this.emfAttributes = emfAttributes;
		}
		
	}
	public static class PlatformDTO implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		@ApiModelProperty(value="Platform where object is published",required=true,example="PCTV")
		private String name;
		
		@ApiModelProperty(value="Picture url",required=true,example="http://www.pk.com/picture")
		private String pictureUrl;
		
		@ApiModelProperty(value="Resolution of picture",required=true,example="1024*768")
		private String pictureResolution;
		
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getPictureUrl() {
			return pictureUrl;
		}
		public void setPictureUrl(String pictureUrl) {
			this.pictureUrl = pictureUrl;
		}
		public String getPictureResolution() {
			return pictureResolution;
		}
		public void setPictureResolution(String pictureResolution) {
			this.pictureResolution = pictureResolution;
		}
		
	}
	
	
	
}
