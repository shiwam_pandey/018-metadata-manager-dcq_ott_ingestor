package com.accenture.avs.be.dcq.vod.ingestor.manager.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionReaderResponseDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionWriterRequestDTO;
import com.accenture.avs.be.dcq.vod.ingestor.manager.CategoryIngestManager;
import com.accenture.avs.be.dcq.vod.ingestor.processor.DcqVodIngestionPreProcessor;
import com.accenture.avs.be.dcq.vod.ingestor.processor.DcqVodIngestionProcessor;
import com.accenture.avs.be.dcq.vod.ingestor.reader.DcqVodIngestionReader;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.commons.lib.LoggerWrapper.OtherSystemCallType;

/**
 * @author karthik.vadla
 *
 */
@Component
public class CategoryIngestManagerImpl implements CategoryIngestManager {
	private static final LoggerWrapper log = new LoggerWrapper(CategoryIngestManagerImpl.class);
	
	@Autowired
	private DcqVodIngestionPreProcessor dcqVodIngestionPreProcessor;

	@Autowired
	private DcqVodIngestionProcessor dcqVodIngestionProcessor;
	@Autowired
	private DcqVodIngestionReader dcqVodIngestionReader;
	
	@Autowired
	private DcqAssetStagingManager dcqAssetStagingManager;
	
	@Override
	public void initializeCategoryIngestor(String mode, String indexDate, Configuration config) {
		log.logMessage("Initialize Category Ingestor.");
		List<Integer> categoryIds = null;
		String transactionNumber = DcqVodIngestorUtils.getTransactionNumber();

		try {

			log.logMessage("Refresh Caches,Check & CreateIndexes - Start");
			long sTime = System.currentTimeMillis();
			dcqVodIngestionPreProcessor.refreshCachesCreateIndexes(DcqVodIngestorConstants.CATEGORY, mode, indexDate, config);
			log.logMessage("Refresh Caches,Check & CreateIndexes - Completed,took: {} ms" , (System.currentTimeMillis() - sTime) );

			long sTimeReader = System.currentTimeMillis();
			log.logMessage("Category Ingestor Reader - Start");
			DcqVodIngestionReaderResponseDTO readerResponseDTO = dcqVodIngestionReader.retrieveAssetIdsFromStaging(DcqVodIngestorConstants.CATEGORY, config);
			categoryIds = readerResponseDTO.getAssetIds();
			log.logMessage("Category Ingestor Reader - Completed,took: {} ms" , (System.currentTimeMillis() - sTimeReader) );

			if (null == categoryIds || categoryIds.isEmpty()) {
				log.logMessage("No CategoryIds to Process,CategoryIngestor Completes.");
				return;
			}

			List<DcqVodIngestionWriterRequestDTO> ingestionWriterRequestDTOs = null;
			long sTimeProcessor = System.currentTimeMillis();
			log.logMessage("Category Ingestor Processor - Start");
			ingestionWriterRequestDTOs = dcqVodIngestionProcessor.processMetadataConstructESDocs(readerResponseDTO, DcqVodIngestorConstants.CATEGORY,
					transactionNumber, mode, indexDate, config);
			log.logMessage("Category Ingestor Processor - Completed,took: {} ms" , (System.currentTimeMillis() - sTimeProcessor) );
			
		} catch (ApplicationException e) {
			if (null == categoryIds || categoryIds.isEmpty()) {
				log.logMessage(e.getMessage());
			} else {
				log.logMessage("There is an error,Update the status of all CategoryIds to FAILED.");
				log.logMessage(e.getMessage());
				String errorInfo = DcqVodIngestorUtils.formatString(OtherSystemCallType.INTERNAL, DcqVodIngestorUtils.getLoggingMethod(), DcqVodIngestorUtils.getErrorStackTrace(e));
				dcqAssetStagingManager.updateStatus(errorInfo, categoryIds,DcqVodIngestorConstants.CATEGORY, config);
			}
		} catch (Exception e) {
			if (null == categoryIds || categoryIds.isEmpty()) {
				log.logError(e);
			} else {
				log.logMessage("There is an error,Update the status of all CategoryIds to FAILED.");
				log.logMessage(e.getMessage());
				String errorInfo = DcqVodIngestorUtils.formatString(OtherSystemCallType.INTERNAL, DcqVodIngestorUtils.getLoggingMethod(), DcqVodIngestorUtils.getErrorStackTrace(e));
				dcqAssetStagingManager.updateStatus( errorInfo, categoryIds,DcqVodIngestorConstants.CATEGORY, config);
			}
		} 
	}
}
