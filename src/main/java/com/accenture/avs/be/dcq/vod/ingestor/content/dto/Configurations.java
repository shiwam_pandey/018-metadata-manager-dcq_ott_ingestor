package com.accenture.avs.be.dcq.vod.ingestor.content.dto;

import java.util.List;

/**
 * 
 * @author karthik.reddy.dasani
 *
 *         used for Configuration-MS
 */
public class Configurations {
	private List<ConfigurationResourceInfo> configurations;

	public List<ConfigurationResourceInfo> getConfigurations() {
		return configurations;
	}

	public void setConfigurations(List<ConfigurationResourceInfo> configurations) {
		this.configurations = configurations;
	}

}
