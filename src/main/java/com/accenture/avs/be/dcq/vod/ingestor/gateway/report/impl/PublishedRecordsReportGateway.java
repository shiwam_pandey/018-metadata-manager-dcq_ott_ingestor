package com.accenture.avs.be.dcq.vod.ingestor.gateway.report.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.dto.ChannelIdPlaylistDateDTO;
import com.accenture.avs.be.dcq.vod.ingestor.gateway.report.PublishedRecordsReportGatewayInterface;
import com.accenture.avs.be.dcq.vod.ingestor.gateway.report.input.PublishedRecordsReportInput;
import com.accenture.avs.be.dcq.vod.ingestor.gateway.report.output.PublishedRecordsReportOutput;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;

/**
 * @author karthik.vadla
 *
 */
@Component
public class PublishedRecordsReportGateway implements PublishedRecordsReportGatewayInterface {

	private static final String SEPARATOR = ";";
	private Logger contentPublishedLog;
	private Logger categoryPublishedLog;
	private Logger vodChannelPublishedLog;
	private Logger vlcContentPublishedLog;

	public PublishedRecordsReportGateway() {
		contentPublishedLog = LogManager.getLogger("content-published");
		categoryPublishedLog = LogManager.getLogger("category-published");
		vodChannelPublishedLog = LogManager.getLogger("vodchannel-published");
		vlcContentPublishedLog = LogManager.getLogger("vlccontent-published");
	}

	private void append(String input, StringBuffer buffer) {
		if (input != null) {
			buffer.append(input);
		}
		buffer.append(SEPARATOR);
	}
	@Override
	public PublishedRecordsReportOutput execute(PublishedRecordsReportInput input) {
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		String nowDateStr = dateFormat.format(new Date());

		if (input.getContentType().equals(DcqVodIngestorConstants.CONTENT)) {
			for (Integer assetId : input.getAssetIds()) {
				StringBuffer buffer = new StringBuffer();
				append(assetId.toString(), buffer);
				append(input.getStartTime(), buffer);
				append(nowDateStr, buffer);
				buffer = buffer.deleteCharAt(buffer.length() - 1);
				contentPublishedLog.info(buffer.toString());
			}
		} else if (input.getContentType().equals(DcqVodIngestorConstants.CATEGORY)) {
			for (Integer assetId : input.getAssetIds()) {
				StringBuffer buffer = new StringBuffer();
				append(assetId.toString(), buffer);
				append(input.getStartTime(), buffer);
				append(nowDateStr, buffer);
				buffer = buffer.deleteCharAt(buffer.length() - 1);
				categoryPublishedLog.info(buffer.toString());
			}
		} else if (input.getContentType().equals(DcqVodIngestorConstants.VODCHANNEL)) {
			for (Integer assetId : input.getAssetIds()) {
				StringBuffer buffer = new StringBuffer();
				append(assetId.toString(), buffer);
				append(input.getStartTime(), buffer);
				append(nowDateStr, buffer);
				buffer = buffer.deleteCharAt(buffer.length() - 1);
				vodChannelPublishedLog.info(buffer.toString());
			}
		} else if (input.getContentType().equals(DcqVodIngestorConstants.VLCCONTENT)) {
			for (ChannelIdPlaylistDateDTO dto : input.getChannelIdPlaylistDateDTOs()) {
				StringBuffer buffer = new StringBuffer();
				append(dto.getChannelId().toString(), buffer);
				append(dto.getPlaylistDate().toString(), buffer);
				append(input.getStartTime(), buffer);
				append(nowDateStr, buffer);
				buffer = buffer.deleteCharAt(buffer.length() - 1);
				vlcContentPublishedLog.info(buffer.toString());
			}
		}
		return new PublishedRecordsReportOutput(DcqVodIngestorConstants.OK, null, false, null);
	}

}
