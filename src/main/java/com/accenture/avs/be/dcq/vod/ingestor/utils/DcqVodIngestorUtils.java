package com.accenture.avs.be.dcq.vod.ingestor.utils;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.WebServiceException;

import org.apache.commons.lang.StringUtils;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.http.HttpStatus;

import com.accenture.avs.be.dcq.vod.ingestor.dto.ChannelIdPlaylistDateDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestorServiceResponse;
import com.accenture.avs.be.dcq.vod.ingestor.dto.Transaction;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.FlagTypeYN;
import com.accenture.avs.be.dcq.vod.ingestor.factories.GatewayFactory;
import com.accenture.avs.be.dcq.vod.ingestor.gateway.report.PublishedRecordsReportGatewayInterface;
import com.accenture.avs.be.dcq.vod.ingestor.gateway.report.input.PublishedRecordsReportInput;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.be.framework.exception.ConfigurationException;
import com.accenture.avs.be.framework.utils.BooleanConverter;
import com.accenture.avs.be.framework.utils.Utilities;
import com.accenture.avs.commons.lib.JsonUtils;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.commons.lib.LoggerWrapper.OtherSystemCallType;
import com.accenture.avs.commons.lib.httpclient.HttpClientAdapter.HttpClientAdapterConfiguration;
import com.accenture.avs.commons.lib.httpclient.HttpClientAdapter.HttpClientException;
import com.accenture.avs.es.manager.ElasticSearchAdminClientManager;
import com.accenture.avs.es.manager.ElasticSearchIntegratedClientManager;
import com.accenture.avs.es.manager.impl.ElasticSearchAdminClientManagerImpl;
import com.accenture.avs.es.manager.impl.ElasticSearchIntegratedClientManagerImpl;
import com.accenture.avs.es.responses.GetAliasResponse;
import com.accenture.avs.es.utils.ElasticRestClientConfiguration;
import com.accenture.avs.es.utils.ElasticRestClientSingletonFactory;
import com.accenture.avs.eventcollector.EventCollector;
import com.accenture.avs.eventcollector.exceptions.EventCollectorException;

/**
 * @author karthik.vadla
 *
 */
public abstract class DcqVodIngestorUtils {
	private static final LoggerWrapper log = new LoggerWrapper(DcqVodIngestorUtils.class);
	private static final String CLOSING_EM = "Closing Entity Manager.";
	private static final String CLOSED_EM = "Entity Manager Closed.";
	public static void closeStream(Closeable stream) {
		if (stream != null) {
			try {
				stream.close();
			} catch (IOException ioe) {
				log.logMessage(ioe.getMessage());
			}
		}
	}
	public static boolean isEmpty(String s) {
		return (s == null || s.trim().isEmpty());
	}

	public static <T> boolean isEmptyCollection(Collection<T> s) {
		return (s == null || s.isEmpty());
	}

	public static <T> boolean isEmptyArray(Object[] s) {
		return (s == null || s.length == 0);
	}

	public static synchronized String getTransactionNumber() {
		String transaction_id = "-1";
		transaction_id = String.valueOf(System.currentTimeMillis() + 1);
		return transaction_id;
	}
	public static Date convertToDate(XMLGregorianCalendar date) {
		if (date == null) {
			return null;
		}
		return date.toGregorianCalendar().getTime();
	}

	public static Long convertToLong(XMLGregorianCalendar date) {
		if (date == null) {
			return null;
		}
		return date.toGregorianCalendar().getTime().getTime();
	}

	public static Boolean getBooleanValue(FlagTypeYN flagTypeVar) {
		if (flagTypeVar == null) {
			return null;
		}
		return flagTypeVar.value().equalsIgnoreCase(getStringValue(true));
	}
	public static Boolean getBooleanValue(String value) {
		if (value == null) {
			return null;
		}
		return value.equalsIgnoreCase(getStringValue(true));
	}
	public static String getStringValue(Boolean value) {
		if (value == null) {
			return null;
		}
		return value ? BooleanAvsAdapter.TRUE.getValue() : BooleanAvsAdapter.FALSE.getValue();
	}

	private enum BooleanAvsAdapter {
		TRUE("Y"), FALSE("N");
		private String booleanString;
		BooleanAvsAdapter(String booleanString) {
			this.booleanString = booleanString;
		}
		public String getValue() {
			return booleanString;
		}
	}

	public static String readFile(String fileName) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append("\n");
				line = br.readLine();
			}
			return sb.toString();
		} finally {
			br.close();
		}
	}

	public static String convertToUTF8(String s) {
		String out = null;
		try {
			out = new String(s.getBytes("UTF-8"), "ISO-8859-1");
		} catch (java.io.UnsupportedEncodingException e) {
			return null;
		}
		return out;
	}
	public static DcqVodIngestorServiceResponse buildErrorResponse(String errorCode, String paramName) {
		DcqVodIngestorServiceResponse genericResponse = new DcqVodIngestorServiceResponse();
		genericResponse.setErrorCode(errorCode);
		//genericResponse.setErrorDescription(DcqVodIngestorPropertiesManager.getInstance().getErrorDesc(errorCode) + " " + paramName);
		genericResponse.setResultCode(DcqVodIngestorConstants.KO);
		return genericResponse;
	}

	public static String buildErrorResponseString(String errorCode, String paramName) {
		String response = null;
		DcqVodIngestorServiceResponse genericResponse = new DcqVodIngestorServiceResponse();
		genericResponse.setErrorCode(errorCode);
		//genericResponse.setErrorDescription(DcqVodIngestorPropertiesManager.getInstance().getErrorDesc(errorCode) + " " + paramName);
		genericResponse.setResultCode(DcqVodIngestorConstants.KO);
		try {
			response = JsonUtils.writeAsJsonString(genericResponse);
		} catch (Exception e) {
			log.logError(e);
		}
		return response;
	}

	public static DcqVodIngestorServiceResponse buildErrorResponse(String errorCode) {
		DcqVodIngestorServiceResponse genericResponse = new DcqVodIngestorServiceResponse();
		genericResponse.setErrorCode(errorCode);
		//genericResponse.setErrorDescription(DcqVodIngestorPropertiesManager.getInstance().getErrorDesc(errorCode));
		genericResponse.setResultCode(DcqVodIngestorConstants.KO);
		return genericResponse;
	}
	public static String constructErrorResponse(String errorCode) {
		String response = null;
		Transaction transaction = new Transaction();
		transaction.setErrorCode(errorCode);
		//transaction.setErrorDescription(DcqVodIngestorPropertiesManager.getInstance().getErrorDesc(errorCode));
		transaction.setResultCode(DcqVodIngestorConstants.KO);
		try {
			response = JsonUtils.writeAsJsonString(transaction);
		} catch (Exception e) {
			log.logError(e);
		}
		return response;
	}
	public static void checkParameterValidityRange(String parameterName, String[] validRange, String value) throws ApplicationException {
		for (int i = 0; i < validRange.length; i++)
			if (validRange[i].equalsIgnoreCase(value)) {
				return;
			}
		//throw new ApplicationException(ERROR_CODES.ACN_302, parameterName + ": Param value is not in valid range");
		throw new ApplicationException("");
	}

	public static boolean isEmpty(Object s) {
		return (s == null);
	}

	public static DeleteResponse deleteDocumentFromES( String indexName, String indexType, String contentId, Configuration config) throws ConfigurationException {
		long sTime = System.currentTimeMillis();
		log.logMessage("Deleting Document From ES - Start");
		byte[]  dcqIngestorUser = getESDcqIngestorUser();
		byte[] dcqIngestorPassword = getESDcqIngestorPassword();
		ElasticSearchIntegratedClientManager integratedClientManager = getElasticSearchIntegratedClientManagerImpl(config);
		DeleteResponse deleteResponse = integratedClientManager.deleteDocumentById(indexName, indexType,dcqIngestorUser,dcqIngestorPassword, contentId);
		log.logMessage("Deleting Document From ES-Index: {};IndexType: {};UniqueId: {}-Completed,took: {}" , indexName , indexType , contentId 
				, (System.currentTimeMillis() - sTime));
		return deleteResponse;
	}

	public static void deleteVlcContentDocumentFromES(String indexName, String indexType, long channelId, long playlistPubDate, Configuration config) throws ConfigurationException {
		long sTime = System.currentTimeMillis();
		log.logMessage("deleteVlcContentDocumentFromES Documents From ES - Start");
		byte[]  dcqIngestorUser = getESDcqIngestorUser();
		byte[] dcqIngestorPassword = getESDcqIngestorPassword();
		ElasticSearchIntegratedClientManager integratedClientManager = getElasticSearchIntegratedClientManagerImpl(config);
		List<String> esIds = integratedClientManager.findVLCContentsByChannelAndPublishDate(indexName, indexType,dcqIngestorUser,dcqIngestorPassword, DcqVodIngestorConstants.VLC_CONTENT_CHANNEL_JSON_FIELD,
				DcqVodIngestorConstants.VLC_CONTENT_PLAYLISTPUBDATE_JSON_FIELD, channelId, playlistPubDate);
		if (esIds != null) {
			log.logMessage("deleteVlcContentDocumentFromES: {} document(s) are deleting From ES-Index: {} IndexType: {};UniqueId: {}Published Date: {}-Completed" , esIds.size()  , indexName , indexType 
					, channelId  , playlistPubDate );
			for (String esId : esIds) {
				integratedClientManager.deleteDocumentById(indexName, indexType,dcqIngestorUser,dcqIngestorPassword, esId);
			}
		}
		log.logMessage("deleteVlcContentDocumentFromES Documents From ES-Index: {};IndexType: {};UniqueId: {};Pub Date: {}-Completed,took: {}" , indexName  , indexType , channelId 
				,playlistPubDate,(System.currentTimeMillis() - sTime));
	}

	public static Long convertToLongChapterSceneTimes(XMLGregorianCalendar xmlDate) {
		if (xmlDate == null) {
			return null;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.add(Calendar.HOUR, xmlDate.getHour());
		calendar.add(Calendar.MINUTE, xmlDate.getMinute());
		calendar.add(Calendar.SECOND, xmlDate.getSecond());
		
		long time = xmlDate.getHour()*60*60000+xmlDate.getMinute()*60000+xmlDate.getSecond()*1000;
		return time;
		//return calendar.getTime().getTime();
	}

	public static String getYear(XMLGregorianCalendar xmlDate) {
		if (xmlDate == null) {
			return null;
		}
		String year = xmlDate.getYear() == 0 ? "" : String.valueOf(xmlDate.getYear());
		return year;
	}

	public static String getErrorStackTrace(Exception e) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		String str=sw.toString();

		try {

			sw.close();
			pw.close();
		} catch (IOException e1) {

			log.logError(e1);
		}

		return str;
	}

	public static void closeEM(EntityManager entityMgr) {
		if (entityMgr != null && entityMgr.isOpen()) {
			log.logMessage(CLOSING_EM);	
			entityMgr.close();
			log.logMessage(CLOSED_EM);
		}
	}

	public static String currentTime() {
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		return dateFormat.format(new Date());
	}

	public static void sendPublishedReport( Set<Integer> assetIds, List<ChannelIdPlaylistDateDTO> channelIdPlaylistDateDTOs,
			String startTime, String contentType, Configuration config) throws ConfigurationException {
		Boolean enableReporting = BooleanConverter.getBooleanValue(config.getConstants().getValue(CacheConstants.DCQ_VOD_PUBLISHED_REPORTS_ENABLED));
		log.logMessage("DCQ_VOD_PUBLISHED_REPORTS_ENABLED: {}",enableReporting);
		if (enableReporting) {
			try {
				PublishedRecordsReportGatewayInterface reportNew = GatewayFactory.getPublishedRecordsReportGateway(config);
				PublishedRecordsReportInput input = null;
				if (contentType.equals(DcqVodIngestorConstants.VLCCONTENT)) {
					input = new PublishedRecordsReportInput(channelIdPlaylistDateDTOs, startTime, contentType);
				} else {
					input = new PublishedRecordsReportInput( assetIds, startTime, contentType);
				}
				reportNew.execute(input);
			} catch (Exception e) {
				log.logError(e);
			}
		}
	}
	
	//Start AVS 9263
	
	/**
	 * This method is used to call the event collector service 
	 * @param eventSource
	 * @param eventType
	 * @param payLoad
	 * @throws ConfigurationException 
	 * @throws EventCollectorException
	 */
	public static void sendEventCollectorReport(String eventSource, String eventType, String payLoad, Configuration config) throws ApplicationException, ConfigurationException {
		try {
			if (BooleanConverter.getBooleanValue(config.getConstants().getValue(CacheConstants.EVENT_COLLECTOR_ENABLED))) {
				Map<String, String> headers = new HashMap<>();
				EventCollector.getInstance().sendEvent(eventSource, eventType, headers, null, payLoad, EventCollector.PayloadType.JSON, EventCollector.PayloadEncoding.UTF8);
			}
		} catch (EventCollectorException e) {
			log.logError(e);
		}
	}
	
	//End AVS 9263
	
	public static String getLoggingMethod() {
		StackTraceElement stackTraceElements[] = Thread.currentThread().getStackTrace();
		StackTraceElement caller = stackTraceElements[3];
		return caller.getMethodName();
	}
	
	private static String formatter(String... parts) {
		Iterator<String> it = Arrays.asList(parts).iterator();
		StringBuffer buffer = new StringBuffer(it.next());
		while (it.hasNext()) {
			buffer.append(" | ").append(it.next());
		}
		return buffer.toString();
	}

	public static String formatString(OtherSystemCallType type, String method, String message) {
		return formatter("CALL TYPE: " + type.name(), method, message);
	}
	
	public static HashMap<String, String> getHeaderMapFormat(HttpServletRequest request) {
		HashMap<String, String> requestMap = new HashMap<String, String>();
		Enumeration<String> enumeration = request.getHeaderNames();
		while (enumeration.hasMoreElements()) {
			String keyHeader = enumeration.nextElement();
			if (!StringUtils.isEmpty(keyHeader)) {
				String valueHeader = request.getHeader(keyHeader);
				requestMap.put(keyHeader, valueHeader);
			}
		}
		return requestMap;
	}
	
	public static GetAliasResponse getIndexByAliasName(String aliasName, Configuration config) throws ConfigurationException{
		byte[]  dcqIngestorUser = getESDcqIngestorUser();
		byte[] dcqIngestorPassword = getESDcqIngestorPassword();
		ElasticSearchAdminClientManager elasticSearchAdminClientManager = getElasticSearchAdminClientManagerImpl(config);
		GetAliasResponse getAliasResponse = elasticSearchAdminClientManager.getIndexByAliasName(aliasName, dcqIngestorUser, dcqIngestorPassword);
		return getAliasResponse;
	}

	public static RestClient getESRestClient(Configuration config) throws ConfigurationException {
		
		//Properties properties = DcqVodIngestorPropertiesManager.getInstance().loadEsSettings();
		Properties properties = new Properties();
		Map<String, Integer> hostAndPort = getEsHostAndPort(config);
		ElasticRestClientConfiguration esConfig = getElasticRestClientConfiguration(config);
		return ElasticRestClientSingletonFactory.getElasticRestClient(properties, hostAndPort,esConfig, true);
	}
	
	public static RestHighLevelClient getESHighLevelRestClient(Configuration config) throws ConfigurationException {
		//Properties properties = DcqVodIngestorPropertiesManager.getInstance().loadEsSettings();
				Properties properties = new Properties();
				Map<String, Integer> hostAndPort = getEsHostAndPort(config);
				ElasticRestClientConfiguration esConfig = getElasticRestClientConfiguration(config);
		return ElasticRestClientSingletonFactory.getHighLevelRestClient(properties, hostAndPort, esConfig, true);
	}
	
	public static ElasticSearchIntegratedClientManager getElasticSearchIntegratedClientManagerImpl(Configuration config) throws ConfigurationException {
		Properties properties = new Properties();
		ElasticRestClientConfiguration esConfig = getElasticRestClientConfiguration(config);
		ElasticSearchIntegratedClientManager integratedClientManager = new ElasticSearchIntegratedClientManagerImpl(properties, getEsHostAndPort(config),esConfig);
		return integratedClientManager;
		
	}
	
	public static ElasticSearchAdminClientManager getElasticSearchAdminClientManagerImpl(Configuration config) throws ConfigurationException {
		Map<String, Integer> hostAndPort = getEsHostAndPort(config);
		//Properties properties = DcqVodIngestorPropertiesManager.getInstance().loadEsSettings();
		Properties properties = new Properties();
		ElasticRestClientConfiguration esConfig = getElasticRestClientConfiguration(config);
		ElasticSearchAdminClientManager adminClientManager = new ElasticSearchAdminClientManagerImpl(properties, hostAndPort,esConfig);
		return adminClientManager;
		
	}
	
	public static ElasticRestClientConfiguration getElasticRestClientConfiguration(Configuration config) throws ConfigurationException {
		String httpMaxConnections = config.getConstants().getValue(CacheConstants.HTTP_MAX_CONNECTIONS);
		String httpconnectionsPerRoute = config.getConstants().getValue(CacheConstants.HTTP_MAX_CONNECTIONS_PER_ROUTE);
		String socketTimeOut = config.getConstants().getValue(CacheConstants.HTTP_CLIENT_SOCKET_TIMEOUT);
		String connectiontimeOut = config.getConstants().getValue(CacheConstants.HTTP_CLIENT_CONNECTION_TIMEOUT);
		
		ElasticRestClientConfiguration esConfig = new ElasticRestClientConfiguration();
		esConfig.setConnectionTimeout(Integer.parseInt(connectiontimeOut));
		esConfig.setMaxConnectionParam(Integer.parseInt(httpMaxConnections));
		esConfig.setMaxConnectionPerRoute(Integer.parseInt(httpconnectionsPerRoute));
		esConfig.setSocketTimeout(Integer.parseInt(socketTimeOut));
		return esConfig;
	}
	public static void createAlias(Configuration config, String indexName, String writeAlias) throws ConfigurationException {
		byte[]  dcqIngestorUser = getESDcqIngestorUser();
		byte[] dcqIngestorPassword = getESDcqIngestorPassword();
		ElasticSearchAdminClientManager elasticSearchAdminClientManager = getElasticSearchAdminClientManagerImpl(config);
		elasticSearchAdminClientManager.createAlias(indexName, writeAlias, dcqIngestorUser, dcqIngestorPassword);
	}
	
	public static void removeAlias(Configuration config, String indexName, String writeAlias) throws ConfigurationException {
		byte[]  dcqIngestorUser = getESDcqIngestorUser();
		byte[] dcqIngestorPassword = getESDcqIngestorPassword();
		ElasticSearchAdminClientManager elasticSearchAdminClientManager = getElasticSearchAdminClientManagerImpl(config);
		elasticSearchAdminClientManager.removeAlias(indexName, writeAlias, dcqIngestorUser, dcqIngestorPassword);
	}
	
	public static String getWriteAlias(String indexName) {
		return indexName + DcqVodIngestorConstants.UNDERSCORE + DcqVodIngestorConstants.WRITE;
	}

public static HttpClientAdapterConfiguration getHttpClientAdapterConfiguration( String tenantName, Configuration config) throws ConfigurationException {
	
	String httpMaxConnections = config.getConstants().getValue(CacheConstants.HTTP_MAX_CONNECTIONS);
	String httpconnectionsPerRoute = config.getConstants().getValue(CacheConstants.HTTP_MAX_CONNECTIONS_PER_ROUTE);
	String socketTimeOut = config.getConstants().getValue(CacheConstants.HTTP_CLIENT_SOCKET_TIMEOUT);
	String connectiontimeOut = config.getConstants().getValue(CacheConstants.HTTP_CLIENT_CONNECTION_TIMEOUT);
	
	HttpClientAdapterConfiguration httpConfig = new HttpClientAdapterConfiguration();
	httpConfig.setConnectionTimeout(Integer.parseInt(connectiontimeOut));
	httpConfig.setMaxConnectionParam(Integer.parseInt(httpMaxConnections));
	httpConfig.setMaxConnectionPerRoute(Integer.parseInt(httpconnectionsPerRoute));
	httpConfig.setSocketTimeout(Integer.parseInt(socketTimeOut));
	httpConfig.setTenantName(tenantName);
	return httpConfig;
}
public static String getIp(HttpServletRequest request) {
	String ip = getIpFromHeaderWrapped(request);
	if (isEmpty(ip)) {
		ip = request.getHeader("X-FORWARDED-FOR");  	
	}
	if (isEmpty(ip)) {
		ip = request.getRemoteAddr();  
	}
	if (isEmpty(ip)) {
		log.logMessage("Cannot retrieve ip address neither in session nor in header");
	}
	return ip;
}

private static String getIpFromHeaderWrapped(HttpServletRequest request) {
	Enumeration<String> names = request.getHeaderNames();
	while (names.hasMoreElements()) {
		String name = names.nextElement();
		if (name.equalsIgnoreCase(DcqVodIngestorConstants.CLIENT_IP)) {
			log.logMessage("ClientIp found in header. ClientIP: {} ",request.getHeader(name));
			return request.getHeader(name);
		}
	}
	try {
		InetAddress addr = InetAddress.getLocalHost();
		log.logMessage("clientIp parameter not found in header! [ hostAddress= %s ], [ hostname= %s ]", addr.getHostAddress(),
				addr.getHostName());
	} catch (UnknownHostException e) {
		log.logError(e);
	}
	return null;
}


/**
 * @return
 * @throws ConfigurationException 
 */
public static Map<String, Integer> getEsHostAndPort(Configuration config) throws ConfigurationException{
	String hostAndPort[] = config.getConstants().getValue(CacheConstants.ES_HOST_PORT).split(";");
	//String ip = "localhost:9200";
	//String hostAndPort[] = ip.split(";");
	Map<String, Integer> hostAndPorts = new HashMap<>();
	for(String host : hostAndPort) {
		String[] hostPort = host.split(":");
		hostAndPorts.put(hostPort[0], Integer.parseInt(hostPort[1]));
	}
	return hostAndPorts;
}

/**
 * Utility method for generate the Timestamp value given a XMLGregorianCalendar date.
 * 
 * @param XMLGregorianCalendar xGCal
 * @return Timestamp
 * 
 * */
public static Timestamp xmlGregorianCalendar2Date(XMLGregorianCalendar xGCal)
{
	long dateInMillis = (xGCal.toGregorianCalendar()).getTimeInMillis();
	return new Timestamp(dateInMillis);
}


/**check for the service availability
 * @param ex
 * @param errorMessage
 * @throws ApplicationException
 */
public static void checkServiceUnavailability(HttpClientException ex, String errorMessage) throws ApplicationException {
	long startTime=System.currentTimeMillis();
	String clientResponse = ex.getResponseBody();
	log.logMessage("Http client response :{}", clientResponse);
	if((HttpStatus.SERVICE_UNAVAILABLE.value() == ex.getResponseCode())
			||(HttpStatus.NOT_FOUND.value()== ex.getResponseCode() && Utilities.isEmpty(clientResponse))
			||(HttpStatus.NOT_FOUND.value()== ex.getResponseCode() && clientResponse.startsWith("<html>"))) {
		throw new ApplicationException(errorMessage);
	}
	log.logMethodEnd(System.currentTimeMillis()-startTime);
}

/** check for the service availability
 * @param ex
 * @param errorMessage
 * @param customeMessage
 * @throws ApplicationException
 */
public static void checkServiceUnavailability(HttpClientException ex, String errorMessage,String customeMessage) throws ApplicationException {
	long startTime=System.currentTimeMillis();
	String clientResponse = ex.getResponseBody();
	log.logMessage("Http client response :{}", clientResponse);
	if((HttpStatus.SERVICE_UNAVAILABLE.value() == ex.getResponseCode())
			||(HttpStatus.NOT_FOUND.value()== ex.getResponseCode() && Utilities.isEmpty(clientResponse))
			||(HttpStatus.NOT_FOUND.value()== ex.getResponseCode() && clientResponse.startsWith("<html>"))) {
		throw new ApplicationException(errorMessage,new NestedParameters(customeMessage));
	}
	log.logMethodEnd(System.currentTimeMillis()-startTime);
}

/**check for the service availability
 * @param ex
 * @param errorMessage
 * @throws ApplicationException
 */
public static void checkServiceUnavailability(WebServiceException ex, String errorMessage) throws ApplicationException {
	long startTime=System.currentTimeMillis();
	if(ex.getMessage()!=null && ex.getMessage().contains("503")) {
		throw new ApplicationException(errorMessage);
	}
	log.logMethodEnd(System.currentTimeMillis()-startTime);
}

public static void logExternalSystemEnd(GenericResponse genericResponse, String targetService, String targetApi,
        String request, Long startTime, OtherSystemCallType otherSystemCallType, LoggerWrapper log) {

 String resultCode = !Utilities.isEmpty(genericResponse) ? genericResponse.getResultCode() : DcqVodIngestorConstants.KO;
 String resultDescription = !Utilities.isEmpty(genericResponse) ? genericResponse.getResultDescription() : null;
 Object result = !Utilities.isEmpty(genericResponse) ? genericResponse.getResultObj() : null;
 long executionTime = System.currentTimeMillis()-startTime;
log.logCallToOtherSystemEnd(targetService, targetApi, request,
               !Utilities.isEmpty(result) ? result.toString() : null, resultCode, resultDescription, executionTime,
               otherSystemCallType);
} 


public static byte[] getESDcqIngestorUser(){
	
	byte[] userName = null;
	try {
		userName = System.getenv("ES_DCQ_INGESTOR_USER")!=null?System.getenv("ES_DCQ_INGESTOR_USER").getBytes("UTF-8"):"".getBytes("UTF-8");
	} catch (UnsupportedEncodingException e) {
		log.logError(e);
	}

	return userName;

}

public static  byte[] getESDcqIngestorPassword(){
	byte[] password = null;
	try {
		password = System.getenv("ES_DCQ_INGESTOR_PASSWORD")!=null?System.getenv("ES_DCQ_INGESTOR_PASSWORD").getBytes("UTF-8"):"".getBytes("UTF-8");
	} catch (UnsupportedEncodingException e) {
		log.logError(e);
	}
	return password;
}

public static boolean checkVulnerablesInInputXML(String inputXML) {
	log.logMessage("Validate input xml");
	if (inputXML != null && !inputXML.isEmpty()) {
		if(checkForWordMatch(inputXML, "<!ENTITY") || checkForWordMatch(inputXML, ".dtd")) {
			log.logMessage("Vulnerability found in input xml");
			return true;
		}
	}
	return false;
}
public static boolean checkForWordMatch(String src, String what) {
	final int length = what.length();
	if (length == 0) {
		return true;}
	final char firstLo = Character.toLowerCase(what.charAt(0));
	final char firstUp = Character.toUpperCase(what.charAt(0));
	for (int i = src.length() - length; i >= 0; i--) {
		final char ch = src.charAt(i);
		if (ch != firstLo && ch != firstUp) {
			continue;}
		if (src.regionMatches(true, i, what, 0, length)) {
			return true;
		}
	}
	return false;
}

}
