package com.accenture.avs.be.dcq.vod.ingestor.manager.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionReaderResponseDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionWriterRequestDTO;
import com.accenture.avs.be.dcq.vod.ingestor.manager.VodChannelIngestManager;
import com.accenture.avs.be.dcq.vod.ingestor.processor.DcqVodIngestionPreProcessor;
import com.accenture.avs.be.dcq.vod.ingestor.processor.DcqVodIngestionProcessor;
import com.accenture.avs.be.dcq.vod.ingestor.reader.DcqVodIngestionReader;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.commons.lib.LoggerWrapper.OtherSystemCallType;

/**
 * @author karthik.vadla
 *
 */
@Component
public class VodChannelIngestManagerImpl implements VodChannelIngestManager{
	private static final LoggerWrapper log = new LoggerWrapper(VodChannelIngestManagerImpl.class);

	@Autowired
	private DcqVodIngestionPreProcessor dcqVodIngestionPreProcessor;
	@Autowired
	private DcqVodIngestionProcessor dcqVodIngestionProcessor;
	@Autowired
	private DcqVodIngestionReader dcqVodIngestionReader;
	@Autowired
	private DcqAssetStagingManager dcqAssetStagingManager;
	
	@Override
	public void intializeVodChannelIngestor(String mode, String indexDate, Configuration config) {
		log.logMessage("Initialize VodChannel Ingestor.");
		List<Integer> vodChannelIds = null;
		String transactionNumber = DcqVodIngestorUtils.getTransactionNumber();

		try {
			log.logMessage("VodChannel Refresh Caches,Check & CreateIndexes - Start");
			long sTime = System.currentTimeMillis();
			dcqVodIngestionPreProcessor.refreshCachesCreateIndexes(DcqVodIngestorConstants.VODCHANNEL, mode,indexDate, config);
			log.logMessage("VodChannel Refresh Caches,Check & CreateIndexes - Completed,took: {} ms" , (System.currentTimeMillis() - sTime) );

			long sTimeReader = System.currentTimeMillis();
			log.logMessage("Vodchannel Ingestor Reader - Start");
			DcqVodIngestionReaderResponseDTO readerResponseDTO = dcqVodIngestionReader.retrieveAssetIdsFromStaging(DcqVodIngestorConstants.VODCHANNEL, config);
			vodChannelIds = readerResponseDTO.getAssetIds();
			log.logMessage("Vodchannel Ingestor Reader - Completed,took: {} ms" , (System.currentTimeMillis() - sTimeReader) );

			if (null == vodChannelIds || vodChannelIds.isEmpty()) {
				log.logMessage("No Vodchannel to Process,VodChannelngestor Completes.");
				return;
			}

			List<DcqVodIngestionWriterRequestDTO> ingestionWriterRequestDTOs = null;
			long sTimeProcessor = System.currentTimeMillis();
			log.logMessage("Vodchannel Ingestor Processor - Start");
			ingestionWriterRequestDTOs = dcqVodIngestionProcessor.processMetadataConstructESDocs(readerResponseDTO, DcqVodIngestorConstants.VODCHANNEL,
					transactionNumber, mode, indexDate, config);
			log.logMessage("Vodchannel Ingestor Processor - Completed,took: {} ms" , (System.currentTimeMillis() - sTimeProcessor) );

		} catch (ApplicationException e) {
			if (null == vodChannelIds || vodChannelIds.isEmpty()) {
				log.logMessage(e.getMessage());
			} else {
				log.logMessage(e.getMessage());
				String errorInfo = DcqVodIngestorUtils.formatString(OtherSystemCallType.INTERNAL, DcqVodIngestorUtils.getLoggingMethod(), DcqVodIngestorUtils.getErrorStackTrace(e));
				dcqAssetStagingManager.updateStatus( errorInfo, vodChannelIds,DcqVodIngestorConstants.VODCHANNEL, config);
			}
		} catch (Exception e) {
			if (null == vodChannelIds || vodChannelIds.isEmpty()) {
				log.logError(e);
			} else {
				log.logMessage(e.getMessage());
				String errorInfo = DcqVodIngestorUtils.formatString(OtherSystemCallType.INTERNAL, DcqVodIngestorUtils.getLoggingMethod(), DcqVodIngestorUtils.getErrorStackTrace(e));
				dcqAssetStagingManager.updateStatus(errorInfo, vodChannelIds,DcqVodIngestorConstants.VODCHANNEL, config);
			}
		} 
	}
}
