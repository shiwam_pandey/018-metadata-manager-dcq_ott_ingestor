package com.accenture.avs.be.dcq.vod.ingestor.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Component;

import com.accenture.avs.persistence.technicalcatalogue.CategoryAggregationEntity;
import com.accenture.avs.persistence.technicalcatalogue.CategoryAggregationEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.CategoryCMSEntity;

@Component
public class CategoryAggregationUtils {
	
	private static final String LEAF = "LEAF";
	
private	ArrayList<CategoryAggregationEntity> catAggList= new ArrayList<CategoryAggregationEntity>();
	
public ArrayList<CategoryAggregationEntity> insertCategoriesAggregation(List<CategoryCMSEntity> categories) {
	ArrayList<CategoryAggregationEntity> catAggList= new ArrayList<CategoryAggregationEntity>();
		HashMap<Integer, Integer> categoriesHashMap = new HashMap<Integer, Integer>();
		ArrayList<Integer> subcategories = new ArrayList<Integer>();
		
		for (int y=0;y<categories.size();y++){
			CategoryCMSEntity cat = new CategoryCMSEntity();
			cat = (CategoryCMSEntity) categories.get(y);
			categoriesHashMap.put(cat.getCategoryId(),cat.getParent_category_id());
			if(cat.getCategoryType().equals(LEAF)) {
				subcategories.add(cat.getCategoryId());
			}
		}
		
		for(int i=0;i<subcategories.size();i++) {
			recursionLeafs(catAggList,categoriesHashMap,subcategories.get(i),subcategories.get(i));
		}
		
		return catAggList;
		
	}
	
	private void recursionLeafs(ArrayList<CategoryAggregationEntity> catAggList,HashMap<Integer,Integer> categories,Integer leaf, Integer currentElement) {
		CategoryAggregationEntity catAgg= new CategoryAggregationEntity();
		CategoryAggregationEntityPK catAggPK= new CategoryAggregationEntityPK();
		
		if(categories.get(currentElement)!=null) {
			Integer parentCategoryId = categories.get(currentElement);
			//passo base
			if(currentElement.equals(parentCategoryId)) {
				catAggPK.setCategoryId(leaf);
				catAggPK.setLeafCategoryId(leaf);
				catAgg.setId(catAggPK);
				catAggList.add(catAgg);
			//passo ricorsivo
			} else {
				catAggPK.setCategoryId(parentCategoryId);
				catAggPK.setLeafCategoryId(leaf);
				catAgg.setId(catAggPK);
				catAggList.add(catAgg);
				recursionLeafs(catAggList,categories,leaf,parentCategoryId);
			}
		}
		
	}

}
