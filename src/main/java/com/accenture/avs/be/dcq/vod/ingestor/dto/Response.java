package com.accenture.avs.be.dcq.vod.ingestor.dto;

import com.accenture.avs.be.framework.bean.GenericResponse;

public class Response extends GenericResponse{

	private String resultCode;
	private String errorCode;
	private String errorDescription;
	private Long jobId;
	private String fileName;
	private Object resultObj;

	public final static String RESULT_CODE_OK = "OK";
	public final static String RESULT_CODE_KO = "KO";
	public final static String NULL = "";
	
	public final static String RESULT_CODE_OK_WARN = "OK_WARN";

	/**
	 * Creates response bean with default OK.
	 */
	public Response() {
		this.resultCode = RESULT_CODE_OK;
		this.errorCode = NULL;
		this.errorDescription = NULL;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	
	public Long getJobId() {
		return jobId;
	}

	public void setJobId(Long jobId) {
		this.jobId = jobId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Object getResultObj() {
		return resultObj;
	}

	public void setResultObj(Object resultObj) {
		this.resultObj = resultObj;
	}

	public Response setResponseKO(Response response) {
		this.setResultCode(RESULT_CODE_KO);
		return response;
	}
	
}
