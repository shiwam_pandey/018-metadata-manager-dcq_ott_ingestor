package com.accenture.avs.be.dcq.vod.ingestor.content.dto;

import java.io.Serializable;

/**
 * @author harini.nandyala
 *
 */
public class Property implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String propertyName;

    private String propertyDescription;

    public String getPropertyName ()
    {
        return propertyName;
    }

    public void setPropertyName (String propertyName)
    {
        this.propertyName = propertyName;
    }

    public String getPropertyDescription ()
    {
        return propertyDescription;
    }

    public void setPropertyDescription (String propertyDescription)
    {
        this.propertyDescription = propertyDescription;
    }
	
}
