package com.accenture.avs.be.dcq.vod.ingestor.rest.controllers;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.avs.be.dcq.vod.ingestor.dto.DeviceTypeResponseDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.ResponseDeviceType;
import com.accenture.avs.be.dcq.vod.ingestor.service.DeviceService;
import com.accenture.avs.be.framework.annotation.AvsRestController;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.cache.Configurator;
import com.accenture.avs.be.framework.configuration.CacheConstants;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.commons.lib.support.CommonConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@AvsRestController
@Api(tags="DeviceType Services",value = "DeviceType API's", description = "API's pertaining to get the deviceTypes")
public class DeviceController {

	@Autowired
private	Configurator configurator;

	@Autowired
	private DeviceService deviceService;

	
	/**
	 * @return
	 * @throws ApplicationException
	 */
	@RequestMapping(value = "/v1/devicetype", method = RequestMethod.GET)
	@ApiOperation(value="getDeviceTypes",notes = "To fetch all deviceTypes, this service will be used by CMS to select the blacklist deviceType while ingesting a specific VOD content")
	@ApiResponses({
		@ApiResponse(code=400, message="ACN_3019: Invalid parameter"),
		@ApiResponse(code=200, message="ACN_200: OK"),
		@ApiResponse(code=500, message="ACN_300: GENERIC ERROR")
	})
	@ApiImplicitParams({
		@ApiImplicitParam(name = "startIndex", value = "Starting index. Default value 0", dataType = "int", required = false),
		@ApiImplicitParam(name = "maxResults", value = "Maximum results to retrieve. Default value 50 .If value greater than 50 or not given then default value is considered.", dataType = "int", required = false)})
	public @ResponseBody
	ResponseEntity<GenericResponse<DeviceTypeResponseDTO>> getDeviceTypes(@RequestParam(required= false,value="startIndex") String startIndex,
			@RequestParam(required= false,value="maxResults") String maxResults) throws ApplicationException {
		GenericResponse genericResponse = null;
		try {
			Configuration config = configurator.getConfiguration(ThreadContext.get(CommonConstants.LogParamsL4j2.TN));
			genericResponse = deviceService.getDeviceTypes(startIndex, maxResults, "", "", config);
		} catch (ApplicationException ae) {
				throw ae;
			}
		catch (Exception e) {
				throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR);
			}
		return ResponseEntity.ok(genericResponse);
	}
	
	
	/**
	 * @return
	 * @throws ApplicationException
	 */
	@Deprecated
	@RequestMapping(value = "/getDeviceTypes", method = RequestMethod.GET)
	@ApiOperation(value="getDeviceTypes (For backward compatibility)",notes = "Please find the page for the IFA: https://fleet.alm.accenture.com/avswiki/display/AVSM3DOC/.DCQ+VOD+Ingestor-XML+v6.5. \n To fetch all deviceTypes, this service will be used by CMS to select the blacklist deviceType while ingesting a specific VOD content")
	@ApiResponses({
		@ApiResponse(code=400, message="ERR001-VALIDATION: Invalid parameter"),
		@ApiResponse(code=500, message="ERR004-GENERIC: GENERIC ERROR")
	})
	public @ResponseBody
	ResponseEntity<ResponseDeviceType> getDeviceTypes() throws ApplicationException {
		ResponseDeviceType response = new ResponseDeviceType();
		try {
			Configuration config = configurator.getConfiguration(ThreadContext.get(CommonConstants.LogParamsL4j2.TN));
			response = deviceService.getDeviceTypesForLegacy(config);
		} catch (ApplicationException ae) {
			if(ae.getExceptionId().contains("3019")||ae.getExceptionId().contains("3000")) {
					response.setResultCode("KO");
					response.setErrorCode("ERR001-VALIDATION");
					response.setErrorDescription("Invalid Parameter "+ae.getParameters().getParameters()[0]);
					return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
				}
		}
		catch (Exception e) {
			response.setResultCode("KO");
			response.setErrorCode("ERR004-GENERIC");
			response.setErrorDescription(e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			
			}
		return ResponseEntity.ok(response);
	}
	


}
