package com.accenture.avs.be.dcq.vod.ingestor.repository;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.avs.persistence.technicalcatalogue.CategoryEntity;
/**
 * @author karthik.vadla
 *
 */
@Repository
public interface CategoryRepository extends JpaRepository<CategoryEntity,Integer>{

	List<CategoryEntity> retrieveByExternald(@Param("externalId") String externalId);
	
	CategoryEntity retrieveByCategoryid(@Param("categoryId") Integer categoryId);
	
	@Modifying
	@Transactional
	void unpublishCategory(@Param("categoryId") Integer categoryId);
}
