package com.accenture.avs.be.dcq.vod.ingestor.gateway.report;

import com.accenture.avs.be.dcq.vod.ingestor.gateway.Gateway;
import com.accenture.avs.be.dcq.vod.ingestor.gateway.report.input.PublishedRecordsReportInput;
import com.accenture.avs.be.dcq.vod.ingestor.gateway.report.output.PublishedRecordsReportOutput;


/**
 * @author karthik.vadla
 *
 */
public interface PublishedRecordsReportGatewayInterface extends Gateway<PublishedRecordsReportInput, PublishedRecordsReportOutput> {

}
