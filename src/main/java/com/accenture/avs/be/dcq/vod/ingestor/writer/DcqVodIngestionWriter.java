package com.accenture.avs.be.dcq.vod.ingestor.writer;

import java.util.List;

import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionWriterRequestDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionWriterResponseDTO;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;

/**
 * @author karthik.vadla
 *
 */
public interface DcqVodIngestionWriter {
	DcqVodIngestionWriterResponseDTO writeToES(List<DcqVodIngestionWriterRequestDTO> dcqVodIngestionWriterRequestDTOs, String mode, String indexDate, Configuration config) throws ApplicationException;

	void dynamicMetadataUpdate(String indexName,String platformName, List<Object[]> comingSoonContents,
			List<Object[]> leavingSoonContents,List<Object[]> recentlyAddedContents, Configuration config) throws ApplicationException;

	void switchAlias(Configuration config) throws ApplicationException;
	void rollbackAlias(Configuration config) throws ApplicationException;

}
