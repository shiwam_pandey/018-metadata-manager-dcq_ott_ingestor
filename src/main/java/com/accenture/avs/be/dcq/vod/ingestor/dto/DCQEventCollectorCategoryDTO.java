package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.io.Serializable;

/**
 * @author s.gudivada
 *
 */
public class DCQEventCollectorCategoryDTO implements Serializable{
	private static final long serialVersionUID = -460053666626358917L;

	private String action;
	private String contentType;
	private Integer categoryId;
	private String extCategoryId;
	private String lang;
	
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	public String getExtCategoryId() {
		return extCategoryId;
	}
	public void setExtCategoryId(String extCategoryId) {
		this.extCategoryId = extCategoryId;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
