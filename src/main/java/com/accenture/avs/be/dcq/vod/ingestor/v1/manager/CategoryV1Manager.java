package com.accenture.avs.be.dcq.vod.ingestor.v1.manager;

import java.io.IOException;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import com.accenture.avs.be.dcq.vod.ingestor.dto.CategoryCMS;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ConfigurationException;

public interface CategoryV1Manager {

	List<Integer> unpublishCategory(Integer categoryId, Configuration config) throws ApplicationException, Exception;

	void ingestCategory(List<CategoryCMS> categoryCMS)
			throws ConfigurationException, ApplicationException, JsonParseException, JsonMappingException, IOException;
}
