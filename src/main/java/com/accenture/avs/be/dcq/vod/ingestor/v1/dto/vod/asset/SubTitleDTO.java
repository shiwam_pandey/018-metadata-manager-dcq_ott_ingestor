package com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author karthik.vadla
 *
 */
public class SubTitleDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value="Subtitle identifier,Mandatory if parent element is set",required=true,example="PK_Subtitles_SUB1")
	private String subtitleId;
	
	@ApiModelProperty(value="Subtitle label,Mandatory if parent element is set",required=true,example="ENG")
	private String subtitleLanguageName;
	public String getSubtitleId() {
		return subtitleId;
	}
	public void setSubtitleId(String subtitleId) {
		this.subtitleId = subtitleId;
	}
	public String getSubtitleLanguageName() {
		return subtitleLanguageName;
	}
	public void setSubtitleLanguageName(String subtitleLanguageName) {
		this.subtitleLanguageName = subtitleLanguageName;
	}
	
}
