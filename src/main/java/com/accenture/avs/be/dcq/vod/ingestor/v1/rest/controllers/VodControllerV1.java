package com.accenture.avs.be.dcq.vod.ingestor.v1.rest.controllers;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.avs.be.dcq.vod.ingestor.dto.Response;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.VodRequestDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.service.VodServiceV1;
import com.accenture.avs.be.dcq.vod.ingestor.service.VodService;
import com.accenture.avs.be.framework.annotation.AvsRestController;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.cache.Configurator;
import com.accenture.avs.be.framework.configuration.CacheConstants;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.commons.lib.support.CommonConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author karthik.vadla
 *
 */
@RestController
@AvsRestController
@Api(tags="VOD Ingestion services" ,value = "VOD API's", description = "API's pertaining to create/update/delete vod content")
public class VodControllerV1 {
	

	@Autowired
	private Configurator configurator;
	
	@Autowired
	private VodServiceV1 vodServiceV1;
	
	@Autowired
	private VodService vodService;
	
	@RequestMapping(value = "/v1/vod", method = RequestMethod.POST,consumes= {MediaType.APPLICATION_JSON_VALUE},produces= {MediaType.APPLICATION_JSON_VALUE})
	@ApiOperation(value="vodIngestion",notes = "This interface is used to ingest and also update vod content into our AVS system ")
	@ApiResponses({
		@ApiResponse(code=400, message="ACN_3000: Missing Parameter [param name]"
				+ "\nACN_3019: Invalid parameter [param name]" 
				+ "\nACN_3258: Parsing Json data failed"),
		@ApiResponse(code=200, message="ACN_200: OK"),
		@ApiResponse(code=500, message="ACN_300: GENERIC ERROR")
	})
	public @ResponseBody
	ResponseEntity<GenericResponse> upsertVod(@RequestBody VodRequestDTO vodDTO) throws ApplicationException {
		GenericResponse genericResponse = null;
		try{
		Configuration config = configurator.getConfiguration(ThreadContext.get(CommonConstants.LogParamsL4j2.TN));
		genericResponse = vodServiceV1.ingestContent(vodDTO, config);
		}catch (ApplicationException ae) {
			throw ae;
		}
		catch (Exception e) {
			throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR);
		}
		return ResponseEntity.ok(genericResponse);
	}

	@RequestMapping(value = "/vod/{contentId}", method = RequestMethod.DELETE)
	@ApiOperation(value="deleteContent",notes = "This interface (REST) allows unpublishing of a content in the platform using the DELETE HTTP method . The vod is not physically deleted but the value of the flag 'is published' on the content platform table is set to N")
	@ApiImplicitParams({
		
		@ApiImplicitParam(name="contentId", value="Unique identifier of content in AVS system", dataType="int",required=true,example="150001"),
		
		})
	@ApiResponses({
		@ApiResponse(code=200, message="OK: OK"),
		@ApiResponse(code=500, message="ERR004-GENERIC: GENERIC ERROR")
	})
	public @ResponseBody
	ResponseEntity<Response> deleteContent(@PathVariable Integer contentId){
		Response response = new Response();
		try {
			Configuration config = configurator.getConfiguration();
			vodService.unpublishContent(contentId, config);
		} catch (ApplicationException ae) {
			response.setResultCode(DcqVodIngestorConstants.KO);
			response.setErrorCode("ERR004-GENERIC");
			response.setErrorDescription(ae.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}
		catch (Exception e) {
			response.setResultCode(DcqVodIngestorConstants.KO);
			response.setErrorCode("ERR004-GENERIC");
			response.setErrorDescription(e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			}
		response.setFileName("");
		response.setJobId(null);
		response.setResultObj(null);
		return ResponseEntity.ok(response);
	}
	

}
