package com.accenture.avs.be.dcq.vod.ingestor.v1.manager;

import java.util.List;

import com.accenture.avs.be.dcq.vod.ingestor.dto.Channel;
import com.accenture.avs.persistence.technicalcatalogue.TechnicalPackageEntity;


public interface ChannelV1Manager {	
	public List<Integer> retrieveByDefaultChannelNumber(String defaultChannelNumber);

	public List<TechnicalPackageEntity> selectByPackageId(Integer packageId);
	//public Integer createChannel(Channel inputChannel, Channels channels) throws Exception;
	public Integer createChannel(Channel inputChannel) throws Exception;
}

