package com.accenture.avs.be.dcq.vod.ingestor.gateway.report.output;

import java.util.Map;

import com.accenture.avs.be.dcq.vod.ingestor.gateway.output.GatewayOutput;



/**
 * @author karthik.vadla
 *
 */
public class PublishedRecordsReportOutput implements GatewayOutput {

	private String resultCode;
	private String resultMessage;
	private boolean hasErrors;
	private Object extObject;
	private boolean forceError;
	private Map<String, String> extSessionMap;

	public boolean isForceError() {
		return forceError;
	}

	public PublishedRecordsReportOutput(String resultCode, String resultMessage, boolean hasErrors, Object extObject) {
		this.resultCode = resultCode;
		this.resultMessage = resultMessage;
		this.hasErrors = hasErrors;
		this.extObject = extObject;
	}

	@Override
	public String getResultCode() {
		return resultCode;
	}

	@Override
	public String getResultMessage() {
		return resultMessage;
	}

	@Override
	public boolean hasErrors() {
		return hasErrors;
	}

	@Override
	public Object getExtObject() {
		return extObject;
	}

	@Override
	public Map<String, String> getExtSessionMap() {

		return extSessionMap;
	}
	
	public void setExtSessionMap(Map<String, String> extSessionMap) {
		this.extSessionMap = extSessionMap;
	}

}
