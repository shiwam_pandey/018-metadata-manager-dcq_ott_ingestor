package com.accenture.avs.be.dcq.vod.ingestor.service;

import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;

/**
 * @author karthik.vadla
 *
 */
public interface DcqCacheService {

	GenericResponse refreshCache(Configuration config) throws Exception;
}
