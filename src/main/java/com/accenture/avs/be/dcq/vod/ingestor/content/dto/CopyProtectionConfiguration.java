package com.accenture.avs.be.dcq.vod.ingestor.content.dto;

import java.util.List;

import com.accenture.avs.be.lib.configuration.client.annotations.ManagedConfiguration;

/**
 * 
 * @author karthik.reddy.dasani
 *
 */
@ManagedConfiguration(type = "microservice", label = "copyProtections")
public class CopyProtectionConfiguration {
	private List<CopyProtectionScheme> copyProtections;

	public List<CopyProtectionScheme> getCopyProtections() {
		return copyProtections;
	}

	public void setCopyProtections(List<CopyProtectionScheme> copyProtections) {
		this.copyProtections = copyProtections;
	}

}
