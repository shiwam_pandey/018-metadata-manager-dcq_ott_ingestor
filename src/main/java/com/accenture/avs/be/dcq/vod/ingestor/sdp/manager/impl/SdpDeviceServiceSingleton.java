package com.accenture.avs.be.dcq.vod.ingestor.sdp.manager.impl;

import java.net.MalformedURLException;
import java.net.URL;

import com.accenture.sdp.csmfe.webservices.clients.device.SdpDeviceService;

public class SdpDeviceServiceSingleton {
	
	private static SdpDeviceService service;

	private SdpDeviceServiceSingleton(){}


	private SdpDeviceServiceSingleton(String sdpUrlConnection) throws MalformedURLException{
		String urlString = sdpUrlConnection+ "SdpDeviceService?wsdl";
		URL url =  new URL(urlString);
		if (service == null) { 
		service=new SdpDeviceService(url);
		}
	}

	public static SdpDeviceService getInstance(String sdpUrlConnection) throws MalformedURLException{

		if (service == null) {
			new SdpDeviceServiceSingleton(sdpUrlConnection);
		}
		return service;
	}

}
