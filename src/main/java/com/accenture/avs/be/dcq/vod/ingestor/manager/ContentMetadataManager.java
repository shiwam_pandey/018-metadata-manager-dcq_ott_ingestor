package com.accenture.avs.be.dcq.vod.ingestor.manager;

import java.io.IOException;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionWriterRequestDTO;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ConfigurationException;
import com.accenture.avs.persistence.technicalcatalogue.ContentEntity;

/**
 * @author karthik.vadla
 *
 */
public interface ContentMetadataManager {

	List<DcqVodIngestionWriterRequestDTO> processandWriteContents(List<Integer> assetIdList,
			String transactionNumber, String startTime, String mode, String indexDate, Configuration config)
			throws JsonParseException, JsonMappingException, IOException, ApplicationException, ConfigurationException;

	void dynamicMetadataUpdate(Configuration config);
	ContentEntity getContentByExternalId(String externalId);
	ContentEntity getContentByContentId(Integer contentId);
	void unpublishContents(List<Integer> contentIds,Configuration config) throws ConfigurationException;

}
