package com.accenture.avs.be.dcq.vod.ingestor.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accenture.avs.be.dcq.vod.ingestor.manager.ContentMetadataManager;
import com.accenture.avs.be.dcq.vod.ingestor.manager.DcqVodIngestionManager;
import com.accenture.avs.be.dcq.vod.ingestor.service.DcqVodIngestorService;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.Validator;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.be.framework.exception.ConfigurationException;
import com.accenture.avs.commons.lib.LoggerWrapper;

/**
 * @author karthik.vadla
 *
 */
@Service
public class DcqVodIngestorServiceImpl implements DcqVodIngestorService {
	private static final LoggerWrapper log = new LoggerWrapper(DcqVodIngestorServiceImpl.class);
	@Autowired
private	DcqVodIngestionManager dcqVodIngestionManager;
	
	@Autowired
	private ContentMetadataManager contentMetataManager;
	
	/* (non-Javadoc)
	 * @see com.accenture.avs.be.dcq.vod.service.DcqVodIngestorService#dcqVodIngestor(java.lang.String, java.lang.String, java.lang.String, com.accenture.avs.be.framework.cache.Configuration)
	 */
	@Override
	public GenericResponse dcqVodIngestor(String ingestionType, String mode, String switchAlias, Configuration config) throws ApplicationException {
		
		GenericResponse genericResponse = null;
		try {
			
			Validator.checkParameter("ingestionType", ingestionType, config);
			Validator.checkParameterValidityRange("ingestionType", DcqVodIngestorConstants.ALLOWED_INGESTION_TYPES, ingestionType, config);
			
			if (ingestionType.equalsIgnoreCase(DcqVodIngestorConstants.FULL) && !DcqVodIngestorUtils.isEmpty(mode) && !mode.equals(DcqVodIngestorConstants.REINDEX)) {
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters("mode"));
			}
			if (!DcqVodIngestorUtils.isEmpty(mode) && !DcqVodIngestorUtils.isEmpty(switchAlias) && (!switchAlias.equals(DcqVodIngestorConstants.SHORT_YES) && !switchAlias.equals(DcqVodIngestorConstants.SHORT_NO))) {
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters("switchAlias"));
			}
			TimerTask exportUserdataTimer = new TimerTask() {
				@Override
				public void run() {
					try {
						initiateIngestors(ingestionType, mode, switchAlias, config);
					} catch (ApplicationException e) {
						log.logError(e);
						
					}
				}
			};
			Timer timer = new Timer();
			timer.schedule(exportUserdataTimer, 1000);
			
			genericResponse = new GenericResponse(DcqVodIngestorConstants.ACN_200, DcqVodIngestorConstants.OK,
					null);
		}catch(Exception e) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR, new NestedParameters(e.getMessage()));
		}
		
		return genericResponse;
	}

	/**
	 * @param ingestionType
	 * @param mode
	 * @param switchAlias
	 * @param config
	 * @throws ApplicationException 
	 */
	public synchronized void initiateIngestors(String ingestionType, String mode, String switchAlias, Configuration config) throws ApplicationException {
		
		String indexDate=null;
		DateFormat df = new SimpleDateFormat(DcqVodIngestorConstants.DATE_FORMAT_YYYYMMDDHHMM);
		Date today = Calendar.getInstance().getTime();        
		indexDate = df.format(today);
		try {
		 if (ingestionType.equals("FULL")) {
				boolean result = dcqVodIngestionManager.invokeVodProcedures(config);
				if (result) {
					//log.logMessage("Procedures invoked Successfully, Invoke All Vod Ingestors.");
					dcqVodIngestionManager.initiateIngestors("ALL", mode==null?"":mode,indexDate, config);
				} else {
					//log.logMessage("Error While Invoking the Procedures, Return KO.");
				}
				if(mode!=null && mode.equals(DcqVodIngestorConstants.REINDEX)){
					if(switchAlias==null || switchAlias.equals(DcqVodIngestorConstants.SHORT_YES)) {
						dcqVodIngestionManager.initiateAliasProcess(DcqVodIngestorConstants.SWITCH, config);
						}
						}
			} else {
				
				dcqVodIngestionManager.initiateIngestors(ingestionType, "","", config);
			}
		}catch(Exception e) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR, new NestedParameters(e.getMessage()));
		}
	}

	@Override
	public GenericResponse dynamicUpdate(Configuration config) throws ApplicationException {
		GenericResponse genericResponse = null;
		try {
		TimerTask exportUserdataTimer = new TimerTask() {
			@Override
			public void run() {
				initiateDynamicUpdate(config);
			}
		};
		Timer timer = new Timer();
		timer.schedule(exportUserdataTimer, 1000);
		genericResponse = new GenericResponse(DcqVodIngestorConstants.ACN_200, DcqVodIngestorConstants.OK,
				null);
		}catch(Exception e) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR, new NestedParameters(e.getMessage()));
		}
		return genericResponse;
	}

	/**
	 * @param config
	 */
	private synchronized void initiateDynamicUpdate(Configuration config) {
		contentMetataManager.dynamicMetadataUpdate(config);
	}

	@Override
	public synchronized GenericResponse switckOrRollbackAlias(String mode, Configuration config) throws ApplicationException, ConfigurationException {
		
		GenericResponse genericResponse = null;
		try {
		Validator.checkParameterValidityRange("mode", DcqVodIngestorConstants.ALLOWED_ALIAS_MODES, mode, config);
	
		dcqVodIngestionManager.initiateAliasProcess(mode, config);
		genericResponse = new GenericResponse(DcqVodIngestorConstants.ACN_200, DcqVodIngestorConstants.OK,
				null);
		}catch(Exception e) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR, new NestedParameters(e.getMessage()));
		}
		return genericResponse;
	
	}
	


}
