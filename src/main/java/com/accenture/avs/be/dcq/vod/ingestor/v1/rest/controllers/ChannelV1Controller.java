package com.accenture.avs.be.dcq.vod.ingestor.v1.rest.controllers;

import javax.xml.bind.JAXBException;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.avs.be.dcq.vod.ingestor.dto.Channel;
import com.accenture.avs.be.dcq.vod.ingestor.v1.service.ChannelV1Service;
import com.accenture.avs.be.framework.annotation.AvsRestController;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.cache.Configurator;
import com.accenture.avs.be.framework.configuration.CacheConstants;
import com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.commons.lib.support.CommonConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@AvsRestController
@Api(tags="Channel Ingestion services" ,value = "Channel API's", description = "API's pertaining to create/update vod channels")
public class ChannelV1Controller {

	@Autowired
	private Configurator configurator;
	
	@Autowired
	private ChannelV1Service channelService;
	

	/**
	 * @param channelXml
	 * @return
	 * @throws ApplicationException
	 */
	@RequestMapping(value = "/v1/channel", method = RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value="vodchannelIngestion",notes = "Create/Update VLC, CATCHUP type of channels. "
			+ "\nVLC channel is used to publishing the VOD contents on a specific VLC channel"
			+ "\nCATCHUP channel is used to publishing the CATCHUP events on a specific CATCHUP channel")
	@ApiResponses({
		@ApiResponse(code=400, message="ACN_3000: Missing Parameter [param name]"
				+ "\nACN_3019: Invalid parameter [param name]" 
				+ "\nACN_3258: Parsing Json data failed"),
		@ApiResponse(code=200, message="ACN_200: OK"),
		@ApiResponse(code=500, message="ACN_300: GENERIC ERROR")
	})
	public @ResponseBody
	ResponseEntity<GenericResponse> updateChannel(
			@RequestBody Channel channel) throws ApplicationException {
		GenericResponse genericResponse = null;
		try {
			Configuration config = configurator.getConfiguration(ThreadContext.get(CommonConstants.LogParamsL4j2.TN));
			genericResponse = channelService.upsertChannel( channel, config);
		} catch (ApplicationException ae) {
				throw ae;
			}catch (JAXBException e) {
				throw new ApplicationException(MessageKeys.ERROR_BE_3211_DAO_INVALID_PARAMETER, new NestedParameters("Invalid JSON"));
			}
		catch (Exception e) {
				throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR);
			}
		return ResponseEntity.ok(genericResponse);
	}
	/*
	@RequestMapping(value = "/channel/{channelId}", method = RequestMethod.POST)
	@ApiOperation(value="vodchannelIngestion (For backward compatibility)",notes = "Create/Update VLC, CATCHUP type of channels. "
			+ "\nVLC channel is used to publishing the VOD contents on a specific VLC channel"
			+ "\nCATCHUP channel is used to publishing the CATCHUP events on a specific CATCHUP channel")@ApiResponses({
		@ApiResponse(code=400, message="ERR001-VALIDATION: Missing Parameter"
				+ "\nERR001-VALIDATION: Invalid parameter"),
		@ApiResponse(code=500, message="ERR004-GENERIC: GENERIC ERROR")
	})
	public @ResponseBody
	ResponseEntity<Response> updateLegacyChannel(@PathVariable("channelId") String channelId, 
			@RequestBody String channelXml) throws ApplicationException {
		Response response = new Response();
		try {
			Configuration config = configurator.getConfiguration(ThreadContext.get(CommonConstants.LogParamsL4j2.TN));
			channelService.upsertChannel( channelXml, config);
		} catch (ApplicationException ae) {
			if(ae.getExceptionId().contains("3019")||ae.getExceptionId().contains("3000")) {
				
				if(ae.getParameters().getParameters()[0]!=null && ae.getParameters().getParameters()[0].contains("sdp")) {
					response.setResultCode(DcqVodIngestorConstants.KO);
					response.setErrorCode("ERR008");
					response.setErrorDescription("Invalid Parameter "+ae.getParameters().getParameters()[0]);
					return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
				}else {
					response.setResultCode(DcqVodIngestorConstants.KO);
					response.setErrorCode("ERR001-VALIDATION");
					response.setErrorDescription("Invalid Parameter "+ae.getParameters().getParameters()[0]);
					return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
				}
			}
		}
		catch (Exception e) {
			response.setResultCode(DcqVodIngestorConstants.KO);
			response.setErrorCode("ERR004-GENERIC");
			response.setErrorDescription(e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			
			}
		return ResponseEntity.ok(response);
	}
*/
	
}


