package com.accenture.avs.be.dcq.vod.ingestor.service.impl;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accenture.avs.be.dcq.vod.ingestor.manager.EmfManager;
import com.accenture.avs.be.dcq.vod.ingestor.service.EmfService;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.configuration.CacheConstants;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;

@Service
public class EmfServiceImpl implements EmfService{


    @Autowired
private EmfManager emfManager;


	
	

	@SuppressWarnings("unchecked")
	public GenericResponse activateOrDeactivateEmf(String emfName,String status) throws ApplicationException {
		GenericResponse response=new GenericResponse();
	
		if (StringUtils.isEmpty(emfName)) {
			throw new ApplicationException(
					CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
					new NestedParameters(DcqVodIngestorConstants.EMFCONSTANTS.EMFNAME));
		}
		if(emfName.length()<=50){
			response=emfManager.activateOrReactiveEmf(emfName, status);	
		}
		else{
			throw new ApplicationException(
					CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters(DcqVodIngestorConstants.EMFCONSTANTS.EMFNAME,"emfName not more than 50 characters"));
		}
		return response;
	}
	
	
	}
