package com.accenture.avs.be.dcq.vod.ingestor.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.accenture.avs.commons.lib.LoggerWrapper;

/**
 * @author karthik.vadla
 *
 */
public class XmlHelper {
		private static LoggerWrapper log = new LoggerWrapper(XmlHelper.class);
		XmlHelper() {

		}
	
		public static Document createXMLDocument(String documentString) throws Exception {

			Document doc = null;
			try {
				DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
				domFactory.setNamespaceAware(true);
				domFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
				DocumentBuilder builder = domFactory.newDocumentBuilder();

				InputStream is = new ByteArrayInputStream(documentString.getBytes(DcqVodIngestorConstants.UTF8_ENCODING));

				doc = builder.parse(is);
			} catch (Exception e) {
				log.logError(e);
			}
			return doc;
		}

		/*public static String getXmlStringFromDocument(Document document) throws TransformerException, IOException {
			String xml = null;
			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer();
			StringWriter writer = new StringWriter();
			Result result = new StreamResult(writer);
			Source source = new DOMSource(document);
			transformer.transform(source, result);
			writer.close();
			xml = writer.toString();
			return xml;
		}*/

		public static <T> T unmarshall(Class<T> type, String inputXml) throws JAXBException, UnsupportedEncodingException {
			JAXBContext jc = JAXBContext.newInstance(type);
			ByteArrayInputStream bais = new ByteArrayInputStream(inputXml.getBytes("UTF-8"));
			return type.cast(unmarshall(jc, bais));
		}

		public static Object unmarshallIs(Class<?> type, InputStream is) throws JAXBException {
			JAXBContext jc = JAXBContext.newInstance(type);
			return unmarshall(jc, is);
		}

		public static Object unmarshall(JAXBContext jc, String inputXml) throws JAXBException {
			ByteArrayInputStream bais = new ByteArrayInputStream(inputXml.getBytes());
			return unmarshall(jc, bais);
		}
		
		public static Object latestUnMarshaller(JAXBContext jc,byte[] byteArr) throws JAXBException{
			Unmarshaller u = jc.createUnmarshaller();
			ByteArrayInputStream bais = new ByteArrayInputStream(byteArr);
			//u.setEventHandler(new javax.xml.bind.helpers.DefaultValidationEventHandler());
			Source input = new StreamSource(bais);
			return u.unmarshal(input);
		}
		
		

		private static Object unmarshall(JAXBContext jc, InputStream is) throws JAXBException {
			Unmarshaller u = jc.createUnmarshaller();
			u.setEventHandler(new javax.xml.bind.helpers.DefaultValidationEventHandler());
			Source input = new StreamSource(is);
			return u.unmarshal(input);
		}

		public static String marshall(Class<?> type, Object document) throws JAXBException {
			ByteArrayOutputStream outStream = null;
			try {
				JAXBContext context = JAXBContext.newInstance(type);
				javax.xml.bind.Marshaller m = context.createMarshaller();
				m.setProperty(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.FALSE);
				m.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
				outStream = new ByteArrayOutputStream();
				m.marshal(document, outStream);
				return outStream.toString();
			} catch (Exception e) {
				log.logError(e);
			} finally {
				try {
					if(null!=outStream){
						outStream.close();
					}
				} catch (IOException e) {
					log.logMessage(e.getMessage());
				}

			}
			return null;
		}

		public static List<Element> getElementsFromDocument(Document document) {

			List<Element> responseList = new ArrayList<Element>();
			NodeList nList = document.getChildNodes();

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;
					responseList.add(eElement);

				}
			}

			return responseList;
		}

		/*public static String applyXsltTransformation(String sourceXml, String xsltTransformation) {

			InputStream isXmlFile = null;
			InputStream isXsltTransformation = null;
			ByteArrayOutputStream outStream = null;

			try {
				isXmlFile = new ByteArrayInputStream(sourceXml.getBytes(DcqVodIngestorConstants.UTF8_ENCODING));
				isXsltTransformation = new ByteArrayInputStream(xsltTransformation.getBytes(DcqVodIngestorConstants.UTF8_ENCODING));
				outStream = new ByteArrayOutputStream();

				javax.xml.transform.Source xmlSource = new javax.xml.transform.stream.StreamSource(isXmlFile);
				javax.xml.transform.Source xsltSource = new javax.xml.transform.stream.StreamSource(isXsltTransformation);
				javax.xml.transform.Result result = new javax.xml.transform.stream.StreamResult(outStream);

				// create an instance of TransformerFactory
				javax.xml.transform.TransformerFactory transFact = javax.xml.transform.TransformerFactory.newInstance();
				// transFact.setAttribute("http://saxon.sf.net/feature/version-warning",
				// Boolean.FALSE);
				javax.xml.transform.Transformer trans = transFact.newTransformer(xsltSource);

				trans.transform(xmlSource, result);

				return outStream.toString();
			} catch (Exception e) {
				log.logError(e);
			} finally {
				try {
					if(null!=isXmlFile) {
						isXmlFile.close();
					}
					if(null!=outStream) {
						outStream.close();
					}
					if(null!=isXsltTransformation) {
						isXsltTransformation.close();
					}
				  } catch (IOException e) {
						log.logMessage(e.getMessage());
				}

			}

			return null;

		}*/

		@SuppressWarnings({ "unchecked", "rawtypes" })
		public static String marshallWsResponse(Class<?> type, Object response) {

			ByteArrayOutputStream outStream = null;
			try {

				JAXBContext context = JAXBContext.newInstance(type);
				javax.xml.bind.Marshaller m = context.createMarshaller();
				m.setProperty(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.FALSE);
				m.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
				outStream = new ByteArrayOutputStream();
				m.marshal(new JAXBElement(new QName("", "response"), type, response), outStream);
				return outStream.toString();
			} catch (Exception e) {
				log.logError(e);
			} finally {
				try {
					if(null!=outStream) {
						outStream.close();
					}
				} catch (IOException e) {
                  log.logMessage(e.getMessage());
				}

			}

			return null;
		}

		public static String marshall(JAXBContext jc, Object document) throws JAXBException {
			ByteArrayOutputStream outStream = null;
			try {
				javax.xml.bind.Marshaller m = jc.createMarshaller();
				m.setProperty(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.FALSE);
				m.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
				outStream = new ByteArrayOutputStream();
				m.marshal(document, outStream);
				return outStream.toString();
			} finally {
				DcqVodIngestorUtils.closeStream(outStream);
			}
		}
}
