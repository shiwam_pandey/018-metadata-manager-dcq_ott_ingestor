package com.accenture.avs.be.dcq.vod.ingestor.utils;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Component;

/**
 * @author karthik.vadla
 *
 */
@Component
public interface DcqVodIngestorConstants {
	
	/*String JBOSS_CONFIG_PATH = System.getProperty("jboss.home.dir") + File.separator + "standalone" + File.separator
			+ "configuration";*/
	String DCQ_VOD_INGESTOR_PROP_FILE_NAME = "dcq-vod-ingestor.properties";
	//String BE_PROPERTIES_PATH_DEFAULT = JBOSS_CONFIG_PATH + File.separator + "BE_PROPERTIES";
	String BE_PROPERTIES = System.getenv("BE_PROPERTIES");
	String DCQ_VOD_INGESTOR_ES_SETTINGS_FILE_NAME = "es-settings.properties";
	String DCQ_VOD_INGESTOR_ERROR_PROP_FILE_NAME = "dcq-vod-error.properties";
	String DCQ_VOD_INGESTOR_PERSISTENCE_FILE_NAME = "persistence.properties";
	String DCQ_VOD_INGESTOR_CATEGORY_INPUT_MAPPING_FILE_NAME = "category-input-mapping.properties";
	String DCQ_VOD_INGESTOR_VLC_INPUT_MAPPING_FILE_NAME = "vlccontent-input-mapping.properties";
	
	String DCQ_VOD_INGESTOR_CONFIG_PLUGINS_PATH = "plugins" + File.separator;
	String POST_PROCESSOR_PLUGIN_DIR = BE_PROPERTIES +File.separator+ DCQ_VOD_INGESTOR_CONFIG_PLUGINS_PATH;	
	String DCQ_VOD_INGESTOR_CONFIG_PATH = BE_PROPERTIES + File.separator + "dcq-vod-ingestor-ms" + File.separator;
	String LOG4J_CONFIG_FILE_NAME = "log4j-dcq-vod-ingestor.xml";
	Long DEFAULT_LOG4J_RELOAD_TIME = 6000L;
	Long DEFAULT_SYS_PARAMS_REFRESH_TIME = 5000L;
	String TIMERTASK_RELOAD_TIME = "TIMERTASK_RELOAD_TIME";
	Long DEFAULT_TIMERTASK_RELOAD_TIME = 30000L;
	String DEFAULT_TENANT_ID = "TENANT_1";
	String ALL = "ALL";
	String FALSE = "false";
	String ALL_IN_LOWER = "all";	
	String CONTENT = "CONTENT";
	String CHANNEL_ID = "channelId";
	String CONTENT_ID = "contentId";
	String VIDEO_TYPE = "videoType";
	String START_TIME = "startTime";
	String END_TIME = "endTime";
	
	String CONTENT_IN_LOWER = "content";
	String CATEGORY = "CATEGORY";
	String CATEGORY_IN_LOWER = "category";
	String VODCHANNEL = "VODCHANNEL";
	String VODCHANNEL_IN_LOWER = "vodchannel";
	String VLCCONTENT = "VLCCONTENT";
	String VLCCONTENT_IN_LOWER = "vlccontent";
	Integer DEFAULT_NUM_OF_PROCESSOR_THREADS = 5;
	String DEFAULT_DCQ_VOD_INGESTOR_CONF_PU_NAME = "DCQ_VOD_INGESTOR_CONF_PU";
	String OK = "OK";
	String EMPTY = "";
	String KO = "KO";
	String READY = "READY";
	String IN_PROGRESS = "INPROGRESS";
	String PUBLISHED = "PUBLISHED";
	String FAILED = "FAILED";
	String MAX_RETRY = "3";
	String FICTITIOUS_CONTENT = "FICTITIOUS_CONTENT";
	String LAST_CONTENT = "LAST_CONTENT";
	String ERROR_INFO_ON_EXCEPTION = "Generic Error";
	String SHORT_YES = "Y";
	String SHORT_NO = "N";
	String UTF8_ENCODING = "UTF-8";
	String MAPPING_IN_LOWER = "mapping";
	String SETTINGS_IN_LOWER = "settings";
	String TXT_EXTENSION = ".txt";
	String TRAILER = "TRAILER";
	String FULL = "FULL";
	String[] ALLOWED_INGESTION_TYPES = { CONTENT, CATEGORY, VODCHANNEL, VLCCONTENT, ALL, FULL };
	String TENANTNAME = "tenantName";
	String INGESTIONTYPE = "ingestionType";
	String VODCHANNEL_GETXML_QUERY = "select ch.source_file from  technical_catalogue.channel ch where ch.channel_id=?1";
	String VODCHANNEL_GETTECHPACJAGES_QUERY = "select tp.package_id,tp.package_name,tp.package_type from  technical_catalogue.channel_technical_pkg ctp,technical_catalogue.technical_package tp "
			+ "where tp.package_id=ctp.package_id and ctp.channel_id=?1 and tp.is_enabled='Y'";

	String CHANNEL_PLATFORM_QUERY = "SELECT distinct platform_name  FROM technical_catalogue.channel_platform where is_published='Y'";

	String VLC_CONTENT_QUERY = "SELECT VC.CONTENT_ID,CP.CP_ID,VC.VIDEO_TYPE,VC.START_TIME,VC.END_TIME,C.PC_LEVEL,CHP.PLATFORM_NAME FROM TECHNICAL_CATALOGUE.VLC_CONTENT VC,"
			+ " TECHNICAL_CATALOGUE.CONTENT_PLATFORM CP, TECHNICAL_CATALOGUE.CONTENT C,TECHNICAL_CATALOGUE.CHANNEL_PLATFORM CHP WHERE  VC.CHANNEL_ID=?1 AND VC.PLAYLIST_PUBLISHED_DATE=?2"
			+ " AND VC.CHANNEL_ID=CHP.CHANNEL_ID AND CHP.PLATFORM_NAME=CP.PLATFORM AND CHP.IS_PUBLISHED='Y' AND CP.CONTENT_ID=C.CONTENT_ID AND VC.CONTENT_ID=CP.CONTENT_ID AND VC.VIDEO_TYPE=CP.VIDEO_TYPE"
			+ " AND CP.IS_PUBLISHED='Y'";

	String UPSERT = "UPSERT";
	String DCQ_CATEGORY_INGESTOR = "DCQ Category Ingestor";
	String CATEGORY_INGESTION = "CATEGORY_INGESTION";
	String DCQ_CONTENT_INGESTOR = "DCQ Content Ingestor";
	String CONTENT_INGESTION = "CONTENT_INGESTION";
	String DCQ_VOD_CHANNEL_INGESTION = "DCQ Channel Ingestor";
	String VODCHANNEL_INGESTION = "VODCHANNEL_INGESTION";
	String DCQ_VLC_INGESTOR = "DCQ VLC Ingestor";
	String VLC_INGESTION = "VLC_INGESTION";
	String VLC_PLAYLIST = "VLCPLAYLIST";

	String CONTENT_GLOBAL_INGESTION = "CONTENT_GLOBAL_INGESTION"; // AVS-13771
	
	 String SSL_CERTIFICATE_PATH = System.getenv("ES_SSL_CERTIFICATE_PATH");
	 boolean IS_SSL_ENABLED = Boolean.valueOf(System.getenv("ES_IS_ENABLED_SSL"));
	
	 String VIDEO_URL = "DEFAULT_VIDEO_URL";
	 String NO_CHANNELS = "No channels to write.";
	 String SDP = "SDP";
	 String UPSERT_SOL_OFFER_VOD = "UpsertSolutionOfferVod";
	 String SEARCH_ALL_DEVICE_CHANNELS = "searchAllDeviceChannels";
	 String CREATE_SOLOFFER_AND_PACKAGE = "createSolutionOfferAndPackage";
	 String SOL_OFFER_CHANGE_STATUS = "solutionOfferChangeStatus";
	 String MOD_SOLOFFER_PACKAGE = "modifySolutionOfferAndPackage";
	 String GET_COMMERCIAL_PACKAGES = "getCommercialPackageDetails";
	 String CONFIGURATION = "Configuration";
	 String INTERATIVE_TAGS = "getInterativeTags";
	 String ES_SETTINGS_MAPPINGS_CONFIG = "getEsSettingsMappingsConfigurations";
	 String COPY_PROTECTIONS = "getCopyProtections";
	 String CONCURRENT_STREAM = "ConcurrentStream";
	 String POLICIES = "Policies";
	 String PROPERTIES = "Properties";
	 String[] VALID_CHANNEL_TYPES = {"VLC","SVOD_CATCHUP"};
	 String[] ALLOWED_PC_EXTENDED_RATINGS_TYPES = {"S","T","H","D","A","G"};
	
	public interface WEBXML {
		String LOG4J_CONFIG_FILE = "LOG4J_CONFIG_FILE";
	}

	public interface PROPERTIES {
		String DCQ_VOD_INGESTOR_CONF_PU_NAME = "DCQ_VOD_INGESTOR_CONF_PU_NAME";
		String LOG4J_RELOAD_TIME = "LOG4J_RELOAD_TIME";
		String SYS_PARAMS_REFRESH_TIME = "SYS_PARAMS_REFRESH_TIME";
	}

	public interface ERROR_CODES {
		String ACN_300 = "ACN_300";
		String ACN_301 = "ACN_301";
		String ACN_302 = "ACN_302";
	}

	String HYPHEN_SYMBOL = "-";
	String VLC_CONTENT_CHANNEL_JSON_FIELD = "vlcContent.channel.channelId";
	String VLC_CONTENT_PLAYLISTPUBDATE_JSON_FIELD = "vlcContent.metadata.playlistPublishedDate";
	String SEMICOLON = ";";
	String PIPE = "|";
	String ESCAPE_PIPE = "\\|";
	String UNDERSCORE = "_";
	String URL_SEPARATOR = "/";
	String VARNISH_WILDCARD = "(.*)";
	String PROP_VALUE_SEPARATOR = ";";
	String PUT = "PUT";
	String POST = "POST";
	String GET = "GET";
	String DELETE = "DELETE";
	String APPLICATION_XML = "application/xml";
	String VOD_CHANNEL_PUB_STATUS_QUERY = "SELECT is_published FROM technical_catalogue.channel_platform where channel_id=?1 and platform_name=?2";
	String LOWER_CASE_METADATA = "metadata";
	String COMING_SOON = "comingSoon";
	String LEAVING_SOON = "leavingSoon";
	String RECENTLY_ADDED = "recentlyAdded";

	// Added for AVS-5750
	String CONTENT_SCRIPT = "ctx._source.content=";
	String CONTENT_SCRIPT_PARAM = "params.contentParam";
	String CONTENT_PARAM = "contentParam";
	
	String VOD_CHANNEL_SCRIPT = "ctx._source.vodChannel=";
	String VOD_CHANNEL_SCRIPT_PARAM = "params.vodChannelParam";
	String VOD_CHANNEL_PARAM = "vodChannelParam";
	
	String SUGGEST_SCRIPT = "ctx._source.suggest=";
	String SUGGEST_SCRIPT_PARAM = "params.suggestParam";
	String POPULARITY_SCRIPT = "ctx._source.popularity=";
	String POPULARITY_SCRIPT_PARAM = "params.popularityParam";
	String SEARCH_SCRIPT = "ctx._source.searchMetadata=";
	String SEARCH_SCRIPT_PARAM = "params.searchMetadataParam";
	String SUGGEST_PARAM = "suggestParam";
	String SEARCH_PARAM = "searchMetadataParam";
	String POPULARITY_PARAM = "popularityParam";
	
	String CATEGORY_SCRIPT = "ctx._source.category=";
	String CATEGORY_SCRIPT_PARAM = "params.categoryParam";
	String CATEGORY_PARAM = "categoryParam";
	String CONTENT_METADATA_COMINGSOON = "content.metadata.comingSoon";
	String CONTENT_METADATA_LEAVINGSOON = "content.metadata.leavingSoon";
	String CONTENT_METADATA_RECENTLYADDED="content.metadata.recentlyAdded";
	String ID = "id";

	String SID = "SID";
	String TN = "TN";
	String CLIENT_IP = "clientIp";
	String API_TYPE = "apiType";
	String API = "api";
	String REINDEX = "REINDEX";
	String DATE_FORMAT_YYYYMMDDHHMM = "yyyyMMddHHmm";
	String LIVE = "live";
	String MODE = "mode";
	String WRITE = "write";
	String SWITCH = "SWITCH";
	String ROLLBACK = "ROLLBACK";
	String[] ALLOWED_ALIAS_MODES = {SWITCH, ROLLBACK};
	String SWITCH_ALIAS = "switchAlias";
	String PREFIX_VALUE = "dcq";
	String ACN_200 = "ACN_200";

	// AVS-25272
	public static String CONFIGURATION_MS_IN_PANIC = "N";
	public static String NO = "N";
	public static String YES = "Y";
	public static char CHAR_YES = 'Y';
	public static char CHAR_NO = 'N';
	public static Integer CONNECTION_TIME_OUT = 100000;
	public static Integer SOCKET_TIME_OUT = 100000;
	public static Integer HTTP_MAX_CONNECTIONS = 200;
	public static Integer HTTP_MAX_CONNECTIONS_PER_ROUTE = 20;

	public static final Long CONTENT_OBJECT_TYPE = 1L;
	public static final Long CHAPTER_OBJECT_TYPE = 2L;
	public static final Long SCENE_OBJECT_TYPE = 3L;

	public static final String SHORT_NOT_APPLICABLE = "NA";

	public static final String APPLICATION_JASON = "application/json";
	public static final String RULE_TYPE = "ruleType";

	public static final String PROPERTY_NAME = "propertyName";
	public static final String COMMA = ",";
	public static final String START_INDEX = "startIndex";
	public static final String MAX_RESULTS = "maxResults";
	public static final String FROM = "from";
	public static final String SIZE = "size";
	public static final String LINE_SEPARATOR = "line.separator";
	
	public static final String NO_BUNDLE = "No bundles to write.";
	public static final String INVALID_BUNDLE_CONTENT = "Group of Bundle is not having proper Bundle Content";
	
	public static final String BUNDLE_XSD = "avs_bundle.xsd";
	public static final String	SVOD = "SVOD";
	public static final String	TVOD = "TVOD";
	public static final String	VIDEO = "VIDEO";
	public static final String	IMAGE = "IMAGE";
	public static final String	PKG_BUNDLE = "PKG_BUNDLE";
	public static final String	BUNDLE = "BUNDLE";
	public static final String	GROUP_OF_BUNDLES = "GROUP_OF_BUNDLES";
	public static final String	BUNDLE_GROUP		= "BUNDLE_GROUP";
	public static final String	BUNDLE_VOD			= "BUNDLE_VOD";
	public static final String	SOL_OFF_BUNDLE_NAME	= "SOL_OFF_BUNDLE";
	public static final String	SOLUTION_OFFER		= "SOLUTION_OFFER";
	public static final String	MOVIE = "MOVIE";
	public static final String	EPISODE = "EPISODE";
	public static final String	POSTER = "POSTER";
	public static final String	CATCHUP = "CATCHUP";
	public static final String	SEASON = "SEASON";
	public static final String	SERIES = "SERIES";
	public static final String PREROLL = "PREROLL";
	public static final String MIDROLL = "MIDROLL";
	public static final String POSTROLL = "POSTROLL";
	public static final String CLEAN = "CLEAN";
	public static final String EMBEDDED = "EMBEDDED";
	public final static String ERR_VULNERABLE_REQ="Vulnerable input found in Request Body";
	public final static String SANITIZE_PARAMS = ". Must provide only letters, numbers and spaces";
	public static final String EXTERNAL_ID = "externalIds";
	public static final String EXTERNALID= "externalId";
	public static final String EXTERNAL_CONTENT_ID= "externalContentId";
	public static final String BUNDLE_EXTERNAL_CONTENT_ID= "bundleExternalContentId";
	public static final String RENTAL_MODE= "rentalMode";
	public static final String PRICE_CATEGORY= "priceCategory";
	String RESPONSE_RESULT_OBJ = "resultObj";
	String VLC_CONTENT_PLAYLISTPUBDATE = "playlistPublishedDate";
		
	 List<String> ALLOWED_TYPE_VALUES = Collections.unmodifiableList(Arrays.asList(new String[] { SVOD, TVOD }));
	 List<String> ALLOWED_CONTENT_TYPE_VALUES = Collections.unmodifiableList(Arrays.asList(new String[] { VIDEO, IMAGE, BUNDLE, BUNDLE_GROUP, BUNDLE_VOD, GROUP_OF_BUNDLES }));
	 List<String> ALLOWED_YN_VALUES = Collections.unmodifiableList(Arrays.asList(new String[] { SHORT_YES, SHORT_NO }));
	 List<String> TIME_POSITION_CLASS_VALUES = Collections.unmodifiableList(Arrays.asList(new String[] { PREROLL, MIDROLL, POSTROLL }));
	 List<String> AD_FORMAT_VALUES = Collections.unmodifiableList(Arrays.asList(new String[] { CLEAN, EMBEDDED }));
	 
	
	public interface CHANNEL{
		String CHANNEL_XSD = "avs_channels.xsd";
		String CH_RECORDABLE = "Y";
		String VIDEO_URL = "DEFAULT_VIDEO_URL";
		String CHANNEL_PLATFORMS_JSON_SCHEMA = "updateChannelPlatform.json";/*Added for the story AVS-25549*/
	}
	public interface VOD{
		String CONTENT_XSD = "Schema_VOD_vs_BackEnd.xsd";
		String CH_RECORDABLE = "Y";
		String VIDEO_URL = "DEFAULT_VIDEO_URL";
		String CHANNEL_PLATFORMS_JSON_SCHEMA = "updateChannelPlatform.json";/*Added for the story AVS-25549*/
	}
	
	public interface CATEGORYCONTENT{
		String CATEGORY_CONTENT_XSD = "Schema_CategoryContent.xsd";
		String CH_RECORDABLE = "Y";
		String VIDEO_URL = "DEFAULT_VIDEO_URL";
		String CHANNEL_PLATFORMS_JSON_SCHEMA = "updateChannelPlatform.json";/*Added for the story AVS-25549*/
	}
		
	public interface CATEGORYCONSTANTS
	{
		String CATEGORY_ID="CATEGORY_ID";
		String PARENT_CATEGORY_ID="PARENT_CATEGORY_ID";
		String NAME="NAME";
		String TYPE="TYPE";
		String IS_VISIBLE="IS_VISIBLE";
		String CATALOG_IDENTIFIER="CATALOG_IDENTIFIER";
		String ORDER_ID="ORDER_ID";
		String ADULT="ADULT";
		String EXTERNAL_ID="EXTERNAL_ID";
		String CONTENT_ORDER_TYPE="CONTENT_ORDER_TYPE";
		String CONTENT_ID="CONTENT_ID";
		String RATINGTYPE="RatingType";
		String PCLEVEL="PcLevel";
		String PC_EXTENDED_RATINGS="PcExtendedRatings";
		String IS_PROTECTED="IS_PROTECTED";
		String CATEGORY_INPUT="CATEGORY_INPUT";
		Integer MAX_LENGTH_NAME=50;
	}
	
	public interface CATEGORYCONSTANTSV1
	{
		String CATEGORY_ID="categoryId";
		String PARENT_CATEGORY_ID="parentCategoryId";
		String NAME="name";
		String TYPE="type";
		String IS_VISIBLE="isVisible";
		String CATALOG_IDENTIFIER="catalogueIdentifier";
		String ORDER_ID="orderId";
		String ADULT="isAdult";
		String EXTERNAL_ID="externalId";
		String CONTENT_ORDER_TYPE="contentOrderType";
		String CONTENT_ID="contentId";
		String RATINGTYPE="ratingType";
		String PCLEVEL="pcLevel";
		String PC_EXTENDED_RATINGS="pcExtendedRatings";
		String IS_PROTECTED="isProtected";
	}
	
	public interface EMFCONSTANTS{
		String EMFNAME = "emfName";
		String EMFID = "emfId";
		String PUT_IS_ACTIVE = "Y";
		String DELETE_IS_ACTIVE = "N";
		}
	public interface LANGUAGECONSTANTS{
		String LANGUAGECODE = "langCode";
		String PUT_IS_ACTIVE = "Y";
		String DELETE_IS_ACTIVE = "N";
		}
	public interface PROPERTYNAMECONSTANTS{
		String PROPERTYNAME="propertyName";
	}
	String[] ALLOWED_CATEGORY_TYPES={"LEAF","NODE"};
	String[] ALLOWED_CONTENTORDERTYPES={"TITLEASC","TITLEDESC","CONTRSTARTASC","CONTRSTARTDESC","EPISODEASC","EPISODEDESC","BROADDATEASC","BROADDATEDES","POSITIONASC","POSITIONDESC"};
	String[] ALLOWED_FLAGS={"Y","N"};
	public static final String INVALID_PURCHASE_END_DATE = "Purchase end date is not allowed when purchase start time is empty";
	
	public interface URLS{
	/*	String CONCURRENT_STREAM_MS_URL = System.getenv("CONCURRENT_STREAM_MS_URL");
		String PROPERTIES_CACHE_URL = System.getenv("PROPERTIES_CACHE_URL");
		String SDP_URL = System.getenv("SDP_URL");
		String DCQ_VOD_VARNISH_SERVER_URLS = System.getenv("DCQ_VOD_VARNISH_SERVER_URLS");
		String GET_TRC_COMMERCE_URL = System.getenv("GET_TRC_COMMERCE_URL");
		String SDP_DEVICE_TYPES_URL = System.getenv("SDP_DEVICE_TYPES_URL");*/
		//String COMMERCIAL_PACKAGE_DETAILS_V2_URL = System.getenv("COMMERCIAL_PACKAGE_DETAILS_V2_URL");
	}
	public interface VODCONSTANTSV1
	{
		String EXTERNAL_ID="externalId";
		String TYPE="type";
		String CONTENT_TYPE="contentType";
		String CONTENT_SUB_TYPE="contentSubType";
		String CONTRACT_START_DATE="contractStartDate";
		String CONTRACT_END_DATE="contractEndDate";
		String TITLE="title";
		String GENRES="genres";
		String DURATION="duration";
		String CONTENT_PROVIDER="contentProvider";
		String IS_HD="isHD";
		String PARENTAL_RATING="parentalRating";
		String IS_ENCRYPTED="isEncrypted";
		String LANGUAGE="language";
		String IS_DIS_ALLOWED_ADV="isDisAllowedAdv";
		String IS_GEO_BLOCKED="isGeoBlocked";
		String IS_LATEST="isLatest";
		String IS_NOT_AVAILABLE_OUT_OF_HOME="isNotAvailableOutOfHome";
		String IS_ON_AIR="isOnAir";
		String IS_PARENT_OBJECT="isParentObject";
		String IS_POPULAR_EPISODE="isPopularEpisode";
		String IS_RECOMMENDED="isRecommended";
		String IS_SKIP_JUMP_ENABLED="isSkipJumpEnabled";
		String IS_SURROUND_SOUND="isSurroundSound";
		String IS_TRICK_PLAY_ENABLED="isTrickPlayEnabled";
		String IS_ADULT="isAdult";
		String PLATFORMS="platforms";
		String PLATFORM_NAME="platformName";
		String VIDEO_URL="videoUrl";
		String PICTURE_URL="pictureUrl";
		String TRAILER_URL="trailerUrl";
		String SCENE_ID="sceneId";
		String IS_COPY_PROTECTED ="isCopyProtected";
		String IS_PRIMARY = "isPrimary";
		String CATEGORY_EXTERNAL_ID = "categoryExternalId";

		String PROPERTY_NAME = "propertyName";
		String SECURITY_CODE = "securityCode";
		String SECURITY_OPTION = "securityOption";
		String SUBTITLE_ID = "subtitleId";
		String SUBTITLE_LANGUAGE_NAME = "subtitleLanguageName";
		String IS_DEFAULT = "isDefault";
		String SUB_TYPE = "subType";
		String ORDER_ID = "orderId";
		String CONTENT_ID = "contentId";
		
		String AUDIO_ID = "audioId";
		String AUDIO_LANGUAGE_CODE = "audioLanguageCode";
		String AUDIO_LANGUAGE_NAME = "audioLanguageName";
		String STREAM_ID = "streamId";
		String IS_PREFERRED = "isPreferred";
		
		String NETWORK_ID = "networkId";
		String ADVERTISING_CONTENT_ID = "advertisingContentId";
		String CONTENT_START_TIME_POSITION = "contentStartTimePosition";
		String AD_FORMAT = "adFormat";
		String TIME_POSITION_CLASS = "timePositionClass";
		
	}
}