package com.accenture.avs.be.dcq.vod.ingestor.cache;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.content.dto.GetTransactionRateCardListResponse;
import com.accenture.avs.be.dcq.vod.ingestor.content.dto.GetTransactionRateCardResponse;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorRestUrls;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.JsonUtils;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.utils.Constants;
import com.accenture.avs.be.framework.utils.Utilities;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.commons.lib.LoggerWrapper.OtherSystemCallType;
import com.accenture.avs.commons.lib.httpclient.HttpClientAdapter;
import com.accenture.avs.commons.lib.httpclient.HttpClientAdapter.HttpClientAdapterConfiguration;
import com.accenture.avs.commons.lib.httpclient.HttpClientAdapter.HttpClientException;

/**
 * @author karthik.vadla
 *
 */
@Component
public class TRCCache {

	private static final LoggerWrapper log = new LoggerWrapper(TRCCache.class);
	
	@Autowired
	private DcqVodIngestorRestUrls dcqVodIngestorRestUrl;
	
	private Map<Long,GetTransactionRateCardResponse> trcIdCache;
	private Map<String,GetTransactionRateCardResponse> trcNameCache;
	/**
	 * @param config
	 * @return
	 * @throws ApplicationException
	 */
	public Map<Long, GetTransactionRateCardResponse> loadTRCS(Configuration config) throws ApplicationException{
		
		try{
			log.logMessage("Loading TRC cache");

			 String getTRCUrl = dcqVodIngestorRestUrl.GET_TRC_COMMERCE_URL;
				GetTransactionRateCardListResponse getTransactionRateCardListResponse = loadCommerce(getTRCUrl,config, "0","0");
				if(!Utilities.isEmptyCollection(getTransactionRateCardListResponse.getTransactionRateCardList())){
					
				for(GetTransactionRateCardResponse transactionRateCard : getTransactionRateCardListResponse.getTransactionRateCardList()){
					trcIdCache.put(transactionRateCard.getTransactionRateCardId(), transactionRateCard);
					trcNameCache.put(transactionRateCard.getName(), transactionRateCard);
					
				}
				}
		}catch (HttpClientException hce) {
			log.logError(hce);
			String clientResponse = hce.getResponseBody();
			if((HttpStatus.SERVICE_UNAVAILABLE.value() == hce.getResponseCode())
					||(HttpStatus.NOT_FOUND.value()== hce.getResponseCode() && Utilities.isEmpty(clientResponse))
					||(HttpStatus.NOT_FOUND.value()== hce.getResponseCode() && clientResponse.startsWith("<html>"))) {
				trcIdCache = null;
				trcNameCache = null;
			}
		}catch (Exception e) {
			log.logError(e);
		}
		return trcIdCache;
	}

	/**
	 * @param getTRCUrl
	 * @param config
	 * @param startIndex
	 * @param maxResults
	 * @return
	 * @throws Exception
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 */
	public GetTransactionRateCardListResponse loadCommerce(String getTRCUrl,Configuration config, String startIndex,String maxResults)
			throws Exception, IOException, JsonProcessingException, JsonParseException, JsonMappingException {
		long startTime = new Date().getTime();
		GenericResponse genericResponse = null;
		String resultCode= "";
		String resultDesc = "";
		GetTransactionRateCardListResponse getTransactionRateCardListResponse=null;
		trcIdCache =new HashMap<Long, GetTransactionRateCardResponse>();
		trcNameCache= new HashMap<String,GetTransactionRateCardResponse>();
		ObjectMapper mapper = new ObjectMapper();
		 Map<String, String> headerParamsMap = new HashMap<String, String>();
		 headerParamsMap.put("tenantName", "tenantDefault"); 
		 
		 Map<String, String> paramsMap = new HashMap<String, String>();
		 paramsMap.put(Constants.START_INDEX, startIndex);
		 paramsMap.put(Constants.MAX_RESULTS, maxResults);
		 
		 
		
		HttpClientAdapterConfiguration httpConfig = DcqVodIngestorUtils.getHttpClientAdapterConfiguration("tenantDefault", config);
		try {
			log.logCallToOtherSystemRequestBody("Commerce", "getTRCs", "",
					OtherSystemCallType.INTERNAL);
		String response = HttpClientAdapter.doGet(httpConfig, getTRCUrl, paramsMap, null,headerParamsMap);
		log.logCallToOtherSystemResponseBody("Commerce","getTRCs", DcqVodIngestorConstants.OK,
				OtherSystemCallType.INTERNAL);
		
		JsonNode jsonNode = mapper.readTree(response);
		getTransactionRateCardListResponse = mapper.readValue(jsonNode.findValue("resultObj"), GetTransactionRateCardListResponse.class);
		resultCode = mapper.readValue(jsonNode.findValue("resultCode"), String.class);
		resultDesc = mapper.readValue(jsonNode.findValue("resultDescription"), String.class);
		
		log.logCallToOtherSystemEnd("Commerce", "getTRCs", "", DcqVodIngestorConstants.OK,resultCode,resultDesc,
				System.currentTimeMillis() - startTime, OtherSystemCallType.INTERNAL);
		
		 }catch (HttpClientException hce) {
			log.logError(hce);
			 genericResponse = JsonUtils.parseJson( hce.getResponseBody(),GenericResponse.class);
			String clientResponse = hce.getResponseBody();
			if((HttpStatus.SERVICE_UNAVAILABLE.value() == hce.getResponseCode())
					||(HttpStatus.NOT_FOUND.value()== hce.getResponseCode() && Utilities.isEmpty(clientResponse))
					||(HttpStatus.NOT_FOUND.value()== hce.getResponseCode() && clientResponse.startsWith("<html>"))) {
				trcIdCache = null;
				trcNameCache = null;
			}
		} catch (Exception e) {
			log.logError(e);
		}finally {
			
			DcqVodIngestorUtils.logExternalSystemEnd(genericResponse,"Commerce","getTRCs", 
					null,startTime, OtherSystemCallType.INTERNAL, log);
		}
		return getTransactionRateCardListResponse;
	}
	
     /**
     * @param config
     * @return
     */
    public  Map<String, GetTransactionRateCardResponse> loadTRCList( Configuration config){
		
		try{

			 String getTRCUrl = dcqVodIngestorRestUrl.GET_TRC_COMMERCE_URL;
				
				GetTransactionRateCardListResponse getTransactionRateCardListResponse = loadCommerce(getTRCUrl,config,"0","0");
				for(GetTransactionRateCardResponse transactionRateCard : getTransactionRateCardListResponse.getTransactionRateCardList()){
					trcIdCache.put(transactionRateCard.getTransactionRateCardId(), transactionRateCard);
					trcNameCache.put(transactionRateCard.getName(), transactionRateCard);
				}		
			
		}catch (HttpClientException hce) {
			log.logError(hce);
			String clientResponse = hce.getResponseBody();
			if((HttpStatus.SERVICE_UNAVAILABLE.value() == hce.getResponseCode())
					||(HttpStatus.NOT_FOUND.value()== hce.getResponseCode() && Utilities.isEmpty(clientResponse))
					||(HttpStatus.NOT_FOUND.value()== hce.getResponseCode() && clientResponse.startsWith("<html>"))) {
				trcIdCache = null;
				trcNameCache = null;
			}
		} catch (Exception e) {
			log.logError(e);
		}
		return trcNameCache;
	}
     
 	/**
 	 * @return
 	 */
 	public Map<Long, GetTransactionRateCardResponse> getTrcIdCache() {
		return trcIdCache;
	}

	/**
	 * @return
	 */
	public Map<String, GetTransactionRateCardResponse> getTrcNameCache() {
		return trcNameCache;
	}
}
