package com.accenture.avs.be.dcq.vod.ingestor.manager.impl;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.accenture.avs.be.dcq.vod.ingestor.cache.LanguageCache;
import com.accenture.avs.be.dcq.vod.ingestor.dto.DCQEventCollectorCategoryDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionWriterRequestDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionWriterResponseDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.ESCategoryDoc;
import com.accenture.avs.be.dcq.vod.ingestor.dto.ESCategoryDoc.RequestBean;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Asset;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.MultilangMetadata;
import com.accenture.avs.be.dcq.vod.ingestor.factories.GatewayFactory;
import com.accenture.avs.be.dcq.vod.ingestor.manager.CategoryMetadataManager;
import com.accenture.avs.be.dcq.vod.ingestor.processor.DcqVodIngestionPostProcessor;
import com.accenture.avs.be.dcq.vod.ingestor.processor.VarnishCleanerManager;
import com.accenture.avs.be.dcq.vod.ingestor.processorplugin.DcqVodIngestionProcessorPlugin;
import com.accenture.avs.be.dcq.vod.ingestor.repository.CategoryRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ExtendedContentAttributesRepository;
import com.accenture.avs.be.dcq.vod.ingestor.utils.CacheConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.JsonUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.XmlHelper;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.contentcategory.asset.Category;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.MultiLanguageMetadataDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.VodRequestDTO;
import com.accenture.avs.be.dcq.vod.ingestor.writer.DcqVodIngestionWriter;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ConfigurationException;
import com.accenture.avs.be.framework.utils.BooleanConverter;
import com.accenture.avs.be.framework.utils.Utilities;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.commons.lib.LoggerWrapper.OtherSystemCallType;
import com.accenture.avs.persistence.technicalcatalogue.CategoryEntity;

/**
 * @author karthik.vadla
 *
 */
@Component
public class CategoryMetadataManagerImpl implements CategoryMetadataManager {
	private static final LoggerWrapper log = new LoggerWrapper(CategoryMetadataManagerImpl.class);

	@Autowired
	private ApplicationContext context;
	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private DcqAssetStagingManager dcqAssetStagingManager;
	
	@Autowired
	private DcqVodIngestionWriter dcqVodIngestionWriter;
	
	@Autowired
	private DcqVodIngestionPostProcessor dcqVodIngestionPostProcessor;
	@Autowired
	private VarnishCleanerManager varnishCleanerManager;
	@Autowired
	private LanguageCache languageCache;
	
	@Autowired
	private ExtendedContentAttributesRepository extendedContentAttributesRepository;

	@Override
	public List<DcqVodIngestionWriterRequestDTO> processAndWriteCategories(List<Integer> categoryList,
			String transactionNumber, String startTime, String mode, String indexDate, Configuration config) throws ConfigurationException, ApplicationException, JsonParseException, JsonMappingException, IOException {
		List<DcqVodIngestionWriterRequestDTO> ingestionWriterRequestDTOs = null;
		log.logMessage("Category Ingestor Processor - Start");
		
		for(Integer categoryId : categoryList) {
			ingestionWriterRequestDTOs = constructEsDocsForCategory(categoryId, transactionNumber, mode, indexDate, config);
		
		Set<Integer> publishedIds = null;
		Map<Integer, String> failedIdsMap = null;
		long sTimeWriter = System.currentTimeMillis();
		List<DcqVodIngestionWriterRequestDTO> ingestionWriterRequestDTOsToBePublished = new ArrayList<DcqVodIngestionWriterRequestDTO>();
		if (ingestionWriterRequestDTOs != null && !ingestionWriterRequestDTOs.isEmpty()) {
			publishedIds = new HashSet<Integer>();
			for (DcqVodIngestionWriterRequestDTO dcqVodIngestionWriterRequestDTO : ingestionWriterRequestDTOs) {
				if (dcqVodIngestionWriterRequestDTO.isDeleted()) {
					publishedIds.add(dcqVodIngestionWriterRequestDTO.getEsUniqueId());
				} else {
					ingestionWriterRequestDTOsToBePublished.add(dcqVodIngestionWriterRequestDTO);
				}
			}
		}

		log.logMessage("Category Ingestor Processor Plugin - Start");
		long sTimeProPlugin = System.currentTimeMillis();
		DcqVodIngestionProcessorPlugin dcqVodIngestionProcessorPlugin = GatewayFactory.getDcqVodIngestionProcessorPlugin(config);
		ingestionWriterRequestDTOsToBePublished = dcqVodIngestionProcessorPlugin.updateMetadata(ingestionWriterRequestDTOsToBePublished,
				DcqVodIngestorConstants.CATEGORY, transactionNumber, context);
		log.logMessage("Category Ingestor Processor Plugin - Completed,took: {}" , (System.currentTimeMillis() - sTimeProPlugin) );

		log.logMessage("Category Ingestor Writer - Start");
		DcqVodIngestionWriterResponseDTO dcqVodIngestionWriterResponseDTO = dcqVodIngestionWriter.writeToES(ingestionWriterRequestDTOsToBePublished, mode, indexDate, config);
		if (null != dcqVodIngestionWriterResponseDTO) {
			if (publishedIds == null) {
				publishedIds = dcqVodIngestionWriterResponseDTO.getPublishedIds();
			} else {
				publishedIds.addAll(dcqVodIngestionWriterResponseDTO.getPublishedIds());
			}
			failedIdsMap = dcqVodIngestionWriterResponseDTO.getFailedIdsMap();
		}
		
		//Start:Added as part of Story AVS 9263
		log.logMessage("Event Collector code -  Start");
		if(null != dcqVodIngestionWriterResponseDTO && !CollectionUtils.isEmpty(dcqVodIngestionWriterResponseDTO.getPublishedIds())){
			for(DcqVodIngestionWriterRequestDTO  ingestionWriterRequestDTO :ingestionWriterRequestDTOsToBePublished){					
				if(dcqVodIngestionWriterResponseDTO.getPublishedIds().contains(ingestionWriterRequestDTO.getEsUniqueId())){
					ESCategoryDoc escategoryDocObj = (ESCategoryDoc)ingestionWriterRequestDTO.getEsDocObj();
					DCQEventCollectorCategoryDTO categoryDTO = new DCQEventCollectorCategoryDTO();						
					categoryDTO.setAction(DcqVodIngestorConstants.UPSERT);
					categoryDTO.setContentType(DcqVodIngestorConstants.CATEGORY);
					categoryDTO.setCategoryId(escategoryDocObj.getCategory().getCategoryId());
					categoryDTO.setExtCategoryId(escategoryDocObj.getCategory().getExternalId());
					String[] parts = ingestionWriterRequestDTO.getEsIndex().split("_");
					categoryDTO.setLang(parts[2].toUpperCase());
					String payLoad = JsonUtils.writeAsJsonStringWithoutNull(categoryDTO);										
					DcqVodIngestorUtils.sendEventCollectorReport(DcqVodIngestorConstants.DCQ_CATEGORY_INGESTOR,DcqVodIngestorConstants.CATEGORY_INGESTION,payLoad, config);
				}
			}
		}			
		//End: AVS 9263
		log.logMessage("Category Ingestor Writer - Completed,took: {} ms" , (System.currentTimeMillis() - sTimeWriter));

		Boolean invalidateVarnish = false;
		long sTimePProcescor = System.currentTimeMillis();
		log.logMessage("Category Ingestor Post Procescor - Start");
		if (null != publishedIds && !publishedIds.isEmpty()) {
			dcqVodIngestionPostProcessor.deletePublishedAssets(publishedIds,DcqVodIngestorConstants.CATEGORY, config);
			DcqVodIngestorUtils.sendPublishedReport( publishedIds,null,startTime,DcqVodIngestorConstants.CATEGORY, config);
			invalidateVarnish = true;
		}
		if (null != failedIdsMap && !failedIdsMap.isEmpty()) {
			dcqVodIngestionPostProcessor.updateStatusAsFailed(failedIdsMap,DcqVodIngestorConstants.CATEGORY, config);
		}
		log.logMessage("Category Ingestor Post Procescor - Completed,took: {} ms" , (System.currentTimeMillis() - sTimePProcescor) );

		Boolean invalidateVarnishInPanic = BooleanConverter.getBooleanValue(config.getConstants().getValue(CacheConstants.DCQ_VOD_VARNISH_INVALIDATOR_IN_PANIC));
		if(!invalidateVarnishInPanic && invalidateVarnish){
				log.logMessage("Varnish Invalidator - Start");
				varnishCleanerManager.clean(config);
				log.logMessage("Varnish Invalidator - Completed,took: {} ms" , (System.currentTimeMillis() - sTimePProcescor) );
		}
		
		}
				return null;
	}

	private List<DcqVodIngestionWriterRequestDTO> constructEsDocsForCategory(Integer categoryId,
			String transactionNumber, String mode, String indexDate, Configuration config) {
		long sTime = System.currentTimeMillis();
		List<DcqVodIngestionWriterRequestDTO> categoryDTOsList = null;
		try {
			Map<String, String> languagesCache = languageCache.getLanguages();
			CategoryEntity categoryEntity = categoryRepository.findById(categoryId).get();
			categoryDTOsList = new ArrayList<DcqVodIngestionWriterRequestDTO>();

			ESCategoryDoc esCategoryDoc = new ESCategoryDoc();
			RequestBean bean = new RequestBean();
			bean.setCategoryId(categoryEntity.getCategoryId());
			bean.setParentCategoryId(categoryEntity.getCategory().getCategoryId());
			bean.setType(categoryEntity.getType());
			if(categoryEntity.getContent() != null){
			bean.setContentId(categoryEntity.getContent().getContentId());
			}
			bean.setIsVisible(BooleanConverter.getBooleanValue(categoryEntity.getIsVisible()));
			bean.setExternalId(categoryEntity.getExternalId());
			bean.setOrderId(categoryEntity.getOrderId());
			bean.setChannelCategory(!Utilities.isEmpty(categoryEntity.getChannelCategory())?categoryEntity.getChannelCategory():null);
			bean.setHasNewContent(BooleanConverter.getBooleanValue(categoryEntity.getHasNew()));
			bean.setContentOrderType(categoryEntity.getContentOrderType());
			bean.setAdult(BooleanConverter.getBooleanValue(categoryEntity.getAdult()));
			bean.setTitle(categoryEntity.getName());
			bean.setName(categoryEntity.getName());
			// Added for Story AVS-3702 - DCQ Category Ingestor update for OTTV-IPTV Gap
			String categoryCSV = categoryEntity.getSourceFile();
			Category categoryJsonRequest = convertToJson(categoryCSV);
			if (Objects.isNull(categoryJsonRequest)) {
				if (categoryCSV != null && categoryCSV.indexOf(DcqVodIngestorConstants.SEMICOLON) >= 0) {

					try {
						String[] categoryfIELDS = categoryCSV.split(DcqVodIngestorConstants.SEMICOLON);
						if (categoryfIELDS.length > 11) {
							bean.setRatingType(categoryfIELDS[11]);
						}
						if (categoryfIELDS.length > 12) {
							bean.setPcLevel(categoryfIELDS[12]);
						}

						String[] exeIds = null;
						if (categoryfIELDS.length > 13) {
							if (categoryfIELDS[13] != null
									&& categoryfIELDS[13].indexOf(DcqVodIngestorConstants.PIPE) >= 0) {
								exeIds = categoryfIELDS[13].split(DcqVodIngestorConstants.ESCAPE_PIPE);
							} else {
								exeIds = new String[1];
								exeIds[0] = categoryfIELDS[13];
							}
						}
						bean.setPcExtendedRatings(exeIds);
						// AVS-24270
						if (categoryfIELDS.length > 14) {
							String isProtected = categoryfIELDS[14];
							if (isProtected == null || isProtected.isEmpty()) {
								bean.setIsProtected(false);
							} else {
								bean.setIsProtected(BooleanConverter.getBooleanValue(isProtected));
							}

						}
					} catch (Exception e) {
						log.logError(e);
						log.logMessage(
								"Ignoring exception: {},while constructing ES Doc with CSV data for CategoryId: {}",
								e.getMessage(), categoryId);
					}

				}
			} else {
				mapCatagorytoBeanObject(categoryJsonRequest, bean, categoryId);
			}
			esCategoryDoc.setCategory(bean);

			Map<String, MultilangMetadata> assetMultiLangMap = null;
			Map<String, MultiLanguageMetadataDTO> assetJsonMultiLangMap = null;
			byte[] byteArr = null;
			String representationMode = config.getConstants().getValue(CacheConstants.DCQ_VOD_INGESTOR_CATEGORY_REPRESENTATION_MODE);
			if (!DcqVodIngestorUtils.isEmpty(representationMode)
					&& representationMode.equals(DcqVodIngestorConstants.FICTITIOUS_CONTENT)) {
				/*
				 * Get the contentId from the category table - Retrieve the Ingestor XML of the
				 * above content - From the multilanguage section, get the value of the title
				 * field that will be the categoryName
				 */
				if(categoryEntity.getContent() != null){
				byteArr = extendedContentAttributesRepository.retrieveContentXmlBlob(categoryEntity.getContent().getContentId());
				}
				if (null != byteArr) {
					try {
						JAXBContext jcAsset = JAXBContext.newInstance(Asset.class);
						Asset asset = (Asset) XmlHelper.latestUnMarshaller(jcAsset, byteArr);
						if (null != asset.getMultilangMetadataList()
								&& null != asset.getMultilangMetadataList().getMultilangMetadata()
								&& asset.getMultilangMetadataList().getMultilangMetadata().size() > 0) {
							assetMultiLangMap = new HashMap<String, MultilangMetadata>();
							for (MultilangMetadata multilangMetadata : asset.getMultilangMetadataList()
									.getMultilangMetadata()) {
								assetMultiLangMap.put(multilangMetadata.getLangCode(), multilangMetadata);
							}
						}
					} catch (JAXBException jaxbe) {
						
							String text = new String(byteArr, StandardCharsets.UTF_8);
							VodRequestDTO vodDTO = JsonUtils.parseJson(text, VodRequestDTO.class);
							if (null != vodDTO.getMultiLanguageMetadata()
									&& !vodDTO.getMultiLanguageMetadata().isEmpty()) {
								assetJsonMultiLangMap = new HashMap<String, MultiLanguageMetadataDTO>();
								for (MultiLanguageMetadataDTO multilangMetadata : vodDTO.getMultiLanguageMetadata()) {
									assetJsonMultiLangMap.put(multilangMetadata.getLanguageCode(), multilangMetadata);
								}
							}
							
					}
					}
			}

			for (Map.Entry<String, String> map : languagesCache.entrySet()) {
				DcqVodIngestionWriterRequestDTO writerRequestDTO = new DcqVodIngestionWriterRequestDTO();

				/*
				 * String indexName = SysParamsManager.getSysParamValue(SysParamConstants.
				 * DCQ_VOD_INGESTOR_PREFIX_INDEX_NAME) +
				 * DcqVodIngestorConstants.UNDERSCORE+DcqVodIngestorConstants.CATEGORY_IN_LOWER
				 * + "_" + map.getKey().toLowerCase();
				 * if(mode.equals(DcqVodIngestorConstants.REINDEX)){ indexName =indexName +
				 * DcqVodIngestorConstants.UNDERSCORE + indexDate; }else{ GetAliasesResponse
				 * aliasResponse = DcqVodIngestorUtils.getIndexByAliasName(tenantName,
				 * DcqVodIngestorConstants.CATEGORY_IN_LOWER + "_" +
				 * map.getKey().toLowerCase()); if(!aliasResponse.getAliases().isEmpty()){
				 * indexName = aliasResponse.getAliases().keys().toArray(String.class)[0]; } }
				 */
				String writeAlias = DcqVodIngestorUtils
						.getWriteAlias(DcqVodIngestorConstants.CATEGORY_IN_LOWER + "_" + map.getKey().toLowerCase());

				if (categoryEntity.getIsVisible().equals(DcqVodIngestorConstants.SHORT_YES)) {
					writerRequestDTO.setEsDocObj(replaceMultiLangData(esCategoryDoc, assetMultiLangMap,assetJsonMultiLangMap, map.getKey()));
					writerRequestDTO.setEsUniqueId(categoryId);
					writerRequestDTO.setEsIndexType(DcqVodIngestorConstants.ALL_IN_LOWER);
					writerRequestDTO.setEsIndex(writeAlias);
					writerRequestDTO.setSourceFile(categoryEntity.getSourceFile());
					if (byteArr != null && byteArr.length > 0) {
						writerRequestDTO.setCategoryContentSourceFile(byteArr.toString());// Set the Asset XML of the
																							// content associated to
																							// Category.
					}
					categoryDTOsList.add(writerRequestDTO);
				} else {
					DcqVodIngestorUtils.deleteDocumentFromES(writeAlias, DcqVodIngestorConstants.ALL_IN_LOWER,
							categoryId.toString(), config);
					writerRequestDTO = new DcqVodIngestionWriterRequestDTO();
					writerRequestDTO.setEsUniqueId(categoryId);
					writerRequestDTO.setEsIndex(writeAlias);
					writerRequestDTO.setEsIndexType(DcqVodIngestorConstants.ALL_IN_LOWER);
					writerRequestDTO.setDeleted(true);
					categoryDTOsList.add(writerRequestDTO);

					// Start AVS 9263

					DCQEventCollectorCategoryDTO categoryDTO = new DCQEventCollectorCategoryDTO();
					categoryDTO.setAction(DcqVodIngestorConstants.DELETE);
					categoryDTO.setContentType(DcqVodIngestorConstants.CATEGORY);
					categoryDTO.setCategoryId(categoryEntity.getCategoryId());
					categoryDTO.setExtCategoryId(categoryEntity.getExternalId());
					categoryDTO.setLang(map.getKey().toUpperCase());
					String payLoad = JsonUtils.writeAsJsonStringWithoutNull(categoryDTO);
					DcqVodIngestorUtils.sendEventCollectorReport(DcqVodIngestorConstants.DCQ_CATEGORY_INGESTOR,
							DcqVodIngestorConstants.CATEGORY_INGESTION, payLoad, config);

					// End AVS 9263

				}
			}
			
			
			
			
			
			
			
			
			
			
			log.logMethodEnd(System.currentTimeMillis() - sTime);
		} catch (Exception e) {
			log.logMessage("Error while constructing ES Doc for CategoryId: {},Update the status to FAILED.",
					categoryId);
			log.logError(e);
			List<Integer> categoryIds = new ArrayList<Integer>();
			categoryIds.add(categoryId);
			String errorInfo = DcqVodIngestorUtils.formatString(OtherSystemCallType.INTERNAL,
					DcqVodIngestorUtils.getLoggingMethod(), DcqVodIngestorUtils.getErrorStackTrace(e));

			dcqAssetStagingManager.updateStatus(errorInfo, categoryIds, DcqVodIngestorConstants.CATEGORY, config);
		}
		log.logMessage("Constructing ES Doc(s) for CategoryId: {} -Completed ES Docs count: {},took:{} ms", categoryId,
				(categoryDTOsList == null ? 0 : categoryDTOsList.size()), (System.currentTimeMillis() - sTime));
		return categoryDTOsList;
	}

	private static ESCategoryDoc replaceMultiLangData(ESCategoryDoc esCategoryDoc,
			Map<String, MultilangMetadata> assetMultiLangMap,Map<String, MultiLanguageMetadataDTO> assetJsonMultiLangMap, String langCode)
			throws JsonParseException, JsonMappingException, IOException, JAXBException {
		long sTime = System.currentTimeMillis();
		ESCategoryDoc esCategoryDocWithLang = null;
		if ((null != assetMultiLangMap && !assetMultiLangMap.isEmpty()) || (null != assetJsonMultiLangMap && !assetJsonMultiLangMap.isEmpty())) {

			log.logMessage("Getting the title of content(associated to category) in language: {}", langCode);
			esCategoryDocWithLang = new ESCategoryDoc();
			RequestBean requestBeanWithLang = new RequestBean();
			RequestBean requestBean = esCategoryDoc.getCategory();
			requestBeanWithLang.setCategoryId(requestBean.getCategoryId());
			requestBeanWithLang.setContentId(requestBean.getContentId());
			requestBeanWithLang.setExternalId(requestBean.getExternalId());
			requestBeanWithLang.setIsVisible(requestBean.getIsVisible());
			requestBeanWithLang.setName(requestBean.getName());
			requestBeanWithLang.setChannelCategory(!Utilities.isEmpty(requestBean.getChannelCategory())?requestBean.getChannelCategory():null);
			requestBeanWithLang.setHasNewContent(requestBean.isHasNewContent());
			requestBeanWithLang.setContentOrderType(requestBean.getContentOrderType());
			requestBeanWithLang.setAdult(requestBean.getIsAdult());
			
			requestBeanWithLang.setOrderId(requestBean.getOrderId());
			requestBeanWithLang.setParentCategoryId(requestBean.getParentCategoryId());
			requestBeanWithLang.setType(requestBean.getType());
			// Added for Story AVS-3702 - DCQ Category Ingestor update for OTTV-IPTV Gap
			requestBeanWithLang.setRatingType(requestBean.getRatingType());
			requestBeanWithLang.setPcLevel(requestBean.getPcLevel());
			requestBeanWithLang.setPcExtendedRatings(requestBean.getPcExtendedRatings());
			requestBeanWithLang.setIsProtected(requestBean.getIsProtected());

			if (assetMultiLangMap != null) {

				MultilangMetadata metadata = assetMultiLangMap.get(langCode);
				if (null != metadata && !DcqVodIngestorUtils.isEmpty(metadata.getTitle())) {
					requestBeanWithLang.setTitle(metadata.getTitle());
				}
			} else if (assetJsonMultiLangMap != null) {
				MultiLanguageMetadataDTO metadataJson = assetJsonMultiLangMap.get(langCode);
				if (null != metadataJson && !DcqVodIngestorUtils.isEmpty(metadataJson.getTitle())) {
					requestBeanWithLang.setTitle(metadataJson.getTitle());
				}
			}
			
			esCategoryDocWithLang.setCategory(requestBeanWithLang);
		} else {
			esCategoryDocWithLang = esCategoryDoc;
		}
		log.logMethodEnd(System.currentTimeMillis() - sTime);
		return esCategoryDocWithLang;
	}

	private Category convertToJson(String categoryCSV) {
		Category category = null;
		try {
			category = JsonUtils.parseJson(categoryCSV, Category.class);
		} catch (Exception ex) {
			log.logMessage("Category CSV data", categoryCSV);
		}
		return category;
	}
	
	private void mapCatagorytoBeanObject(Category categoryJsonRequest, RequestBean bean, Integer categoryId) {
		try {
			bean.setRatingType(categoryJsonRequest.getRatingType());
			bean.setPcLevel(categoryJsonRequest.getPcLevel());
			bean.setPcExtendedRatings(categoryJsonRequest.getPcExtendedRatings());
			if (Objects.isNull(categoryJsonRequest.getIsProtected())) {
				bean.setIsProtected(false);
			} else {
				bean.setIsProtected(categoryJsonRequest.getIsProtected().equalsIgnoreCase("Y"));
			}
		} catch (Exception e) {
			log.logMessage("Ignoring exception: {},while constructing ES Doc with CSV data for CategoryId: {}",
					e.getMessage(), categoryId);
		}
	}
}
