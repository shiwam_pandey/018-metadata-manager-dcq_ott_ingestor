package com.accenture.avs.be.dcq.vod.ingestor.v1.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.cache.CopyProtectionsCache;
import com.accenture.avs.be.dcq.vod.ingestor.dto.CopyProtectionConfiguration;
import com.accenture.avs.be.dcq.vod.ingestor.dto.CopyProtectionScheme;
import com.accenture.avs.be.dcq.vod.ingestor.dto.CopyProtectionSchemeOption;
import com.accenture.avs.be.dcq.vod.ingestor.utils.Validator;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.CopyProtectionDTO;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;

@Component
public class CopyProtectionUtilsV1 {

	private static Logger logger = LogManager.getLogger(CopyProtectionUtilsV1.class);

	@Autowired
	private CopyProtectionsCache copyProtectionCache;

	//To Validate and IsCopyProtected and CopyProtections

	public void validateCopyProtections(String isCopyProtected, List<CopyProtectionDTO> inputCopyProtectionList, Configuration config) throws ApplicationException
	{
		if(!Validator.isEmpty(isCopyProtected) && "N".equalsIgnoreCase(isCopyProtected))
		{
			if(!Validator.isEmpty(inputCopyProtectionList))
			{
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters("CopyProtections not allowed when IsCopyProtected is N"));
			}
		}
		else if(!Validator.isEmpty(isCopyProtected) && inputCopyProtectionList!=null&&! Validator.isEmptyCollection(inputCopyProtectionList) )
		{
			findDuplicateCopyProtectionCode(inputCopyProtectionList);
			// Check with master copy validations
			List<CopyProtectionDTO> inputCpCopyProtections = new ArrayList<CopyProtectionDTO>(inputCopyProtectionList);
			if (validateWithMasterProtections(inputCpCopyProtections, config)){
				logger.info("input CopyProtections are matched with Master CopyProtections" );
			}
		}
		else{
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER, new NestedParameters("CopyProtections are missing"));
		}
	}

	public Boolean validateWithMasterProtections(List<CopyProtectionDTO> copyProtections, Configuration config) throws ApplicationException
	{
		boolean validCopyProtections = false;
		Map<String, List<String>> masterCodeOptionMap = new HashMap<String, List<String>>();
		CopyProtectionConfiguration masterCpSchemeList = copyProtectionCache.getCopyProtectionConfiguration();
		
		if(masterCpSchemeList==null) {
			throw new ApplicationException(MessageKeys.ERROR_MS_ACN_902_CONFIGURATION_NOT_AVAILABLE);
		}
		if(logger.isDebugEnabled()) {
		logger.debug("Master CopyProtections retrieved from Configuration-MS "+ masterCpSchemeList.toString());}
		
		if(masterCpSchemeList != null && !Validator.isEmptyCollection(masterCpSchemeList.getCopyProtections())){
			for(CopyProtectionScheme copyProtectionScheme : masterCpSchemeList.getCopyProtections()) {
				List<String> securityOptionList = masterCodeOptionMap.get(copyProtectionScheme.getSecurityCode());
				if(securityOptionList == null) {
					List<String> securityOptions=new ArrayList<>();
					for(CopyProtectionSchemeOption copyProtectionSecurityOption:copyProtectionScheme.getOptions())
					{
						securityOptions.add(copyProtectionSecurityOption.getSecurityOption());
					}
					masterCodeOptionMap.put(copyProtectionScheme.getSecurityCode(), securityOptions);
				}
			}
			logger.info("start: coparision with master copyProtections: ");
			String inputSecurityCode = null;
			for(CopyProtectionDTO inputcp :copyProtections){ 
				inputSecurityCode = inputcp.getSecurityCode();
				if(!Validator.isEmpty(inputSecurityCode)){
					
					if(masterCodeOptionMap.containsKey(inputSecurityCode)){

						if((masterCodeOptionMap.get(inputSecurityCode)).contains(inputcp.getSecurityOption())){
							validCopyProtections = true;
						}else{ //master copyProtection options not matched
							throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters("Master CopyProtection SecurityOption is not matched with input SecurityOption"));
						}
					}else{ //security code not matched with master security code
						throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters("Master CopyProtection SecurityOption is not matched with input SecurityOption"));
					}
					
				}else{
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters("Invalid CopyProtection SecurityCode is passed in input"));
				}
			}
		}else{
			//master copyProtections are not available
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters("Master CopyProtections are not available"));
		}
		return validCopyProtections;
	}


	public static void findDuplicateCopyProtectionCode(List<CopyProtectionDTO> copyProtectionList) throws ApplicationException {
		logger.debug("finding duplicate security code in input copy protections ");
		List<String> securityCodelist = new ArrayList<String>();
		String code = "";
		for (CopyProtectionDTO cpProtection : copyProtectionList) {
				code = cpProtection.getSecurityCode();
				if (securityCodelist.contains(code)) {
					logger.info("duplicate security code found in input copy protections "+ code);
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters("Duplicate CopyProtection security code found in input xml"));
				}
			securityCodelist.add(code);
		}
		logger.info("no duplicate security code found in input copy protections ");
	}
	
	
}
