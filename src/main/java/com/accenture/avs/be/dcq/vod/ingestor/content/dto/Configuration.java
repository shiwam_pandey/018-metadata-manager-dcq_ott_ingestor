package com.accenture.avs.be.dcq.vod.ingestor.content.dto;

import java.util.List;
import java.util.Map;


public class Configuration {
	private Map<String,String> pcLevel;
	private List<String> pkgType;
	private Map<String,String> pcLevelIngestion;

	public Map<String,String> getPcLevel() {
		return pcLevel;
	}

	public void setPcLevel(Map<String,String> pcLevel) {
		this.pcLevel = pcLevel;
	}

	public List<String> getPkgType() {
		return pkgType;
	}

	public void setPkgType(List<String> pkgType) {
		this.pkgType = pkgType;
	}

	public Map<String,String> getPcLevelIngestion() {
		return pcLevelIngestion;
	}

	public void setPcLevelIngestion(Map<String,String> pcLevelIngestion) {
		this.pcLevelIngestion = pcLevelIngestion;
	}
}
