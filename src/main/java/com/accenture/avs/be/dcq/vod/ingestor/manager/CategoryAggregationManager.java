package com.accenture.avs.be.dcq.vod.ingestor.manager;

public interface CategoryAggregationManager {
	
	void reStoreCategoryAggregation();
	void insertCategoryAggregation();
	void deleteCategoryAggregation();

}
