package com.accenture.avs.be.dcq.vod.ingestor.manager;

import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;

/**
 * @author karthik.vadla
 *
 */
public interface DcqVodIngestionManager {

	public boolean invokeVodProcedures(Configuration config);

	public void initiateIngestors(String ingestionType, String mode, String indexDate, Configuration config);

	public void initiateAliasProcess(String switchAlias, Configuration config) throws ApplicationException;

}
