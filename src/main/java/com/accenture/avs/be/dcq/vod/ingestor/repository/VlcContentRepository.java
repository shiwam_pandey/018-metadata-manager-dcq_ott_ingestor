package com.accenture.avs.be.dcq.vod.ingestor.repository;

import java.sql.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.avs.persistence.technicalcatalogue.VlcContentEntity;

/**
 * @author naga.sireesha.meka
 *
 */
@Repository

public interface VlcContentRepository extends JpaRepository<VlcContentEntity,Integer>{

@Transactional
@Modifying
void deleteVlcContentByChannelAndPublishedDate(@Param("channelId") Integer channelId,@Param("playlistPublishedDate") Date playlistPublishedDate);
}
