package com.accenture.avs.be.dcq.vod.ingestor.v1.manager;

import java.io.IOException;
import java.text.ParseException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.VodRequestDTO;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ConfigurationException;

/**
 * @author karthik.vadla
 *
 */
public interface VodManagerV1 {

	public Integer ingestContent(VodRequestDTO vodDTO, Configuration config) throws ConfigurationException, ApplicationException, JsonParseException, JsonMappingException, IOException, ParseException;
	
	
}
