package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;


/**
 * @author karthik.vadla
 *
 */
public class DeviceTypeResponseDTO implements Serializable

{

	private final static long serialVersionUID = 1L;
	@ApiModelProperty(required = true, value = "Total number of results",example = "10")
	private int totalResults;
	@ApiModelProperty(required = true, value = "List of devices that are the ref_device_channel available in AVS system")
	private List<DeviceTypeDTO> deviceTypes;



	public int getTotalResults() {
		return totalResults;
	}

	public void setTotalResults(int totalResults) {
		this.totalResults = totalResults;
	}

	public List<DeviceTypeDTO> getDeviceTypes() {
		return deviceTypes;
	}

	public void setDeviceTypes(List<DeviceTypeDTO> deviceTypes) {
		this.deviceTypes = deviceTypes;
	}



	}

