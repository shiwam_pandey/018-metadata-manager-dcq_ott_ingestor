package com.accenture.avs.be.dcq.vod.ingestor.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.accenture.avs.persistence.technicalcatalogue.ContentEntity;
/**
 * @author karthik.vadla
 *
 */
@Repository
public interface ContentRepository extends JpaRepository<ContentEntity,Integer>{

	/**
	 * @param contentId
	 * @return
	 */
	public String findPcLevelByContentId(@Param("contentId")Integer contentId);

	List<Object[]> retrieveContentDetailsByChannelId(Integer channelId, Date playListDate);
	
	public ContentEntity getcontentByExternalId(@Param("externalContentId")String contentExtId);
	
	public Long getContentIdByExternalId(@Param("externalContentId")String contentExtId);
	
	public ContentEntity getContentByContentId(@Param("contentId")Integer contentId);
	
	ContentEntity retrieveContentDTObyContentId(Integer contentId);

	public Long getMaxId();
	
	List<Integer> validateContentIds(@Param("contentIds")List<Integer> contentId);
}
