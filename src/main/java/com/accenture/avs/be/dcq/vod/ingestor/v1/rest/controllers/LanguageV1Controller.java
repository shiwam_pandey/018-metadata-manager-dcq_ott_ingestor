package com.accenture.avs.be.dcq.vod.ingestor.v1.rest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.avs.be.dcq.vod.ingestor.dto.LanguageDTO;
import com.accenture.avs.be.dcq.vod.ingestor.service.LanguageService;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.framework.annotation.AvsRestController;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.exception.ApplicationException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@AvsRestController
@Api(tags="Metadata Language Services",value = "Language activation API's", description = "API's pertaining to activate,deactivate metadata language")
public class LanguageV1Controller {

	@Autowired
private	LanguageService languageService;
	
	/**
	 * @param langCode
	 * @return
	 * @throws ApplicationException
	 */
	@RequestMapping(value = "/v1/language", method = RequestMethod.PUT)
	@ApiOperation(value="activateLanguage",notes = "To activate metadata language by languageCode")
	@ApiImplicitParams({
		@ApiImplicitParam(name="langCode", value="language ISO code", dataType="string",required = true)})
	@ApiResponses({
		@ApiResponse(code=400, message="ACN_3000: Missing Parameter"),
		@ApiResponse(code=200, message="ACN_200: OK"),
		@ApiResponse(code=500, message="ACN_300: GENERIC ERROR")
	})
	public @ResponseBody ResponseEntity<GenericResponse<LanguageDTO>> setLanguageActive(@RequestParam(required = false) String langCode) throws ApplicationException {
		GenericResponse<LanguageDTO> response = languageService.activateOrDeactivateLanguage(DcqVodIngestorConstants.LANGUAGECONSTANTS.PUT_IS_ACTIVE, langCode);
		return ResponseEntity.ok(response);
	}
	
	//deactive the EMF isActive FLag 
	/**
	 * @param langCode
	 * @return
	 * @throws ApplicationException
	 */
	@RequestMapping(value = "/v1/language", method = RequestMethod.DELETE)
	@ApiOperation(value="deactivateLanguage",notes = "To deactivate metadata language by languageCode")
	@ApiImplicitParams({
		@ApiImplicitParam(name="langCode", value="language ISO code", dataType="string",required = true)})
	@ApiResponses({
		@ApiResponse(code=400, message="ACN_3000: Missing Parameter"),
		@ApiResponse(code=200, message="ACN_200: OK"),
		@ApiResponse(code=500, message="ACN_300: GENERIC ERROR")
	})
	public @ResponseBody ResponseEntity<GenericResponse<LanguageDTO>> setLanguageDeactive(@RequestParam(required = false) String langCode) throws ApplicationException{
		GenericResponse<LanguageDTO> response = languageService.activateOrDeactivateLanguage(DcqVodIngestorConstants.LANGUAGECONSTANTS.DELETE_IS_ACTIVE, langCode);
		return ResponseEntity.ok(response);
	}

	
}
