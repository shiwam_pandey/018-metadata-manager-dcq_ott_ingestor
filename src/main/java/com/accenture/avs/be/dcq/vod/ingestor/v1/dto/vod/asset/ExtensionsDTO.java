package com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author karthik.vadla
 *
 */
@ApiModel(value="extentionsDTO")
public class ExtensionsDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "Y= if reserved to Adult , N= if not reserved to Adult", required = true, example = "Y")
	private String isAdult;
	
	@ApiModelProperty(value = "List of Extension Elements", required = false, example = "")
	private List<CategoryDTO> categories;
	
	@ApiModelProperty(value = "Parental Control KeyWords associated to the Content", required = false, example = "")
	private List<String> extendedRatings;
	
	@ApiModelProperty(value = "Target Device where the Content must be published,The content will be published to the listed platforms and unpublished from all other platforms", required = true, example = "")
	private List<PlatformDTO> platforms;
	
	@ApiModelProperty(value = "List of the TRC related to the Content", required = false, example = "")
	
	private List<TransactionRateCardDTO> transactionRateCards;
	
	public String getIsAdult() {
		return isAdult;
	}
	public void setIsAdult(String isAdult) {
		this.isAdult = isAdult;
	}
	public List<CategoryDTO> getCategories() {
		return categories;
	}
	public void setCategories(List<CategoryDTO> categories) {
		this.categories = categories;
	}
	public List<String> getExtendedRatings() {
		return extendedRatings;
	}
	public void setExtendedRatings(List<String> extendedRatings) {
		this.extendedRatings = extendedRatings;
	}
	public List<PlatformDTO> getPlatforms() {
		return platforms;
	}
	public void setPlatforms(List<PlatformDTO> platforms) {
		this.platforms = platforms;
	}
	public List<TransactionRateCardDTO> getTransactionRateCards() {
		return transactionRateCards;
	}
	public void setTransactionRateCards(List<TransactionRateCardDTO> transactionRateCards) {
		this.transactionRateCards = transactionRateCards;
	}
	public static class CategoryDTO implements Serializable {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		@ApiModelProperty(value = "", required = true, example = "")
		private String categoryExternalId;
		
		@ApiModelProperty(value = "Start date of the availability of the content in the categor", required = false, example = "2015-12-10T00:00:00")
		private Date startDate;
		
		@ApiModelProperty(value = "End date of the availability of the content in the category", required = false, example = "2016-12-10T00:00:00")
		private Date endDate;
		
		@ApiModelProperty(value = "is used for assigning priority to the category", required = true, example = "Y")
		private String isPrimary;
		
		@ApiModelProperty(value = "", required = false, example = "")
		private Long orderId;
		
		public String getCategoryExternalId() {
			return categoryExternalId;
		}
		public void setCategoryExternalId(String categoryExternalId) {
			this.categoryExternalId = categoryExternalId;
		}
		public Date getStartDate() {
			return startDate;
		}
		public void setStartDate(Date startDate) {
			this.startDate = startDate;
		}
		public Date getEndDate() {
			return endDate;
		}
		public void setEndDate(Date endDate) {
			this.endDate = endDate;
		}
		public String getIsPrimary() {
			return isPrimary;
		}
		public void setIsPrimary(String isPrimary) {
			this.isPrimary = isPrimary;
		}
		public Long getOrderId() {
			return orderId;
		}
		public void setOrderId(Long orderId) {
			this.orderId = orderId;
		}
		
	}
	
	@ApiModel(value="Platform")
	public static class PlatformDTO implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		@ApiModelProperty(value = "Platform Name , e.g. 'PCTV’, 'IPAD’,’ANDROID’, 'OTTSTB’Mandatory if parent element is set", required = true, example = "ANDROID")
		private String platformName;
		
		@ApiModelProperty(value = "Video Publishing URL ", required = true, example = "")
		private String videoUrl;
		
		@ApiModelProperty(value = "Trailer Publishing URL.The value of the video url will be overwritten if content transcoding and publishing is demanded to AVS.Mandatory if parent element is set.Notes:-In case the trailerUrl will be filled with empty value, it means that a trailerURL exist and an asset related created by AGL nanoservice creating a possible issue. Then the favorite publishing is avoiding any trailerUrl field is trailerUrl does not exist.", required = false, example = "")
		private String trailerUrl;
		
		@ApiModelProperty(value = "Poster URL. The value of the picture url will be overwritten if content transcoding and publishing is demanded to AVS.Mandatory if parent element is set.Notes:-In case the posterUrl will be filled with empty value, it means that a posterUrl exist and an asset related created by AGL nanoservice creating a possible issue. Then the favorite publishing is avoiding any posterUrl field is posterUrl does not exist.", required = true, example = "")
		private String pictureUrl;
		
		@ApiModelProperty(value = "Decrypt key information of the videoURL", required = false, example = "")
		private String drmInfo;
		
		@ApiModelProperty(value = "Quality video type , Possibile values could be: 'HD’, 'SD’, '3D’, ‘TBD’", required = false, example = "HD")
		private String videoType;
		
		@ApiModelProperty(value = "Metadata Language", required = false, example = "")
		private List<AudioLanguageDTO> audioLanguages;
		
		@ApiModelProperty(value = "Language subtitle , Used for asset transcoding workflow.", required = false, example = "")
		private List<SubTitleDTO> subtitles;
		
		@ApiModelProperty(value = "List of package identifier , Mandatory if parent element is set", required = false, example = "")
		private List<Integer> packageIds;
		
		@ApiModelProperty(value = "Information related the Download & Go functionalities", required = false, example = "")
		private DownloadDTO download;
		
		@ApiModelProperty(value = "Identifier for the specific type of video streaming", required = false, example = "MP4")
		private String streamingType;
		
		@ApiModelProperty(value = "Start date time for indicating when the content is available on the platform , If this value is not passed the value that will be considered is the Contract StartTime tag in the main Asset object", required = false, example = "")
		private Date contractStartDate;
		
		@ApiModelProperty(value = "End date time for indicating when the content is available on the platform , If this value is not passed the value that will be considered is the Contract EndTime tag in the main Asset object", required = false, example = "")
		private Date contractEndDate;
		
		@ApiModelProperty(value = "ExtendedMetadata , Contains a json object with custom fields for customization purpose", required = false, example = "{\"plot\":\"On a far planet...\",\"actorsListDetail\":[{\"Name\":\"Sam Worthington\",\"BirthDate\":\"03/02/1981\",\"BirthCity\":\"New York\"},{\"Name\":\"Sigourney Weaver\",\"BirthDate\":\"04/03/1955\",\"BirthCity\":\"New York\"}]}")
		private String extendedMetadata;
		
		public String getPlatformName() {
			return platformName;
		}

		public void setPlatformName(String platformName) {
			this.platformName = platformName;
		}

		public String getVideoUrl() {
			return videoUrl;
		}

		public void setVideoUrl(String videoUrl) {
			this.videoUrl = videoUrl;
		}

		public String getTrailerUrl() {
			return trailerUrl;
		}

		public void setTrailerUrl(String trailerUrl) {
			this.trailerUrl = trailerUrl;
		}

		public String getPictureUrl() {
			return pictureUrl;
		}

		public void setPictureUrl(String pictureUrl) {
			this.pictureUrl = pictureUrl;
		}

		public String getDrmInfo() {
			return drmInfo;
		}

		public void setDrmInfo(String drmInfo) {
			this.drmInfo = drmInfo;
		}

		public String getVideoType() {
			return videoType;
		}

		public void setVideoType(String videoType) {
			this.videoType = videoType;
		}

		public List<AudioLanguageDTO> getAudioLanguages() {
			return audioLanguages;
		}

		public void setAudioLanguages(List<AudioLanguageDTO> audioLanguages) {
			this.audioLanguages = audioLanguages;
		}

		public List<SubTitleDTO> getSubtitles() {
			return subtitles;
		}

		public void setSubtitles(List<SubTitleDTO> subtitles) {
			this.subtitles = subtitles;
		}

		public List<Integer> getPackageIds() {
			return packageIds;
		}

		public void setPackageIds(List<Integer>packageIds) {
			this.packageIds = packageIds;
		}

		public DownloadDTO getDownload() {
			return download;
		}

		public void setDownload(DownloadDTO download) {
			this.download = download;
		}

		public String getStreamingType() {
			return streamingType;
		}

		public void setStreamingType(String streamingType) {
			this.streamingType = streamingType;
		}

		public Date getContractStartDate() {
			return contractStartDate;
		}

		public void setContractStartDate(Date contractStartDate) {
			this.contractStartDate = contractStartDate;
		}

		public Date getContractEndDate() {
			return contractEndDate;
		}

		public void setContractEndDate(Date contractEndDate) {
			this.contractEndDate = contractEndDate;
		}

		public String getExtendedMetadata() {
			return extendedMetadata;
		}

		public void setExtendedMetadata(String extendedMetadata) {
			this.extendedMetadata = extendedMetadata;
		}

		public static class DownloadDTO implements Serializable {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			
			@ApiModelProperty(value = "Number of downloads available on the platform", required = false, example = "5")
			private Long downloadMaxNumber;
			
			@ApiModelProperty(value = "Number of the max days available after the download of the content on the platform", required = false, example = "30")
			private Long downloadMaxDays;
			
			@ApiModelProperty(value = "Number of the hours available after the first play of the content on the platform", required = false, example = "5")
			private Long downloadHoursFromFirstPlay;
			
			
			public Long getDownloadMaxNumber() {
				return downloadMaxNumber;
			}
			public void setDownloadMaxNumber(Long downloadMaxNumber) {
				this.downloadMaxNumber = downloadMaxNumber;
			}
			public Long getDownloadMaxDays() {
				return downloadMaxDays;
			}
			public void setDownloadMaxDays(Long downloadMaxDays) {
				this.downloadMaxDays = downloadMaxDays;
			}
			public Long getDownloadHoursFromFirstPlay() {
				return downloadHoursFromFirstPlay;
			}
			public void setDownloadHoursFromFirstPlay(Long downloadHoursFromFirstPlay) {
				this.downloadHoursFromFirstPlay = downloadHoursFromFirstPlay;
			}
		}
		
		
	}
	public static class TransactionRateCardDTO implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		@ApiModelProperty(value = "Unique identifier of Transaction Rate card(TRC) in AVS. Mandatory if externalId is not given. TRCId generated automatically during creation of the TRC(Transaction Rate card).", required = false, example = "")
		private Long transactionRateCardId;
		
		@ApiModelProperty(value = "Unique name of Transaction Rate card(TRC). Mandatory if TRCId is not given.", required = false, example = "")
		private String externalId;
		
		@ApiModelProperty(value = "List of the subscriptions related to the TRC. The subscriptions reported inside this list will be the \"enabler\" for the TVOD Commercial Product created during the ingestion. This information is necessary for managing the feature of \"different price based on the subscription of the user\".", required = false, example = "")
		private List<String> enablerCommercialPackages;
		
		public Long getTransactionRateCardId() {
			return transactionRateCardId;
		}
		public void setTransactionRateCardId(Long transactionRateCardId) {
			this.transactionRateCardId = transactionRateCardId;
		}
		public String getExternalId() {
			return externalId;
		}
		public void setExternalId(String externalId) {
			this.externalId = externalId;
		}
		public List<String>  getEnablerCommercialPackages() {
			return enablerCommercialPackages;
		}
		public void setEnablerCommercialPackages(List<String>  enablerCommercialPackages) {
			this.enablerCommercialPackages = enablerCommercialPackages;
		}
		
		
	}
}