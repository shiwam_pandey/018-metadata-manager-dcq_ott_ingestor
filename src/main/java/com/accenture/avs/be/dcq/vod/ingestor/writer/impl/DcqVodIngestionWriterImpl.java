package com.accenture.avs.be.dcq.vod.ingestor.writer.impl;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.codehaus.jackson.map.ObjectMapper;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.cache.LanguageCache;
import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionWriterRequestDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionWriterResponseDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.ESCategoryDoc;
import com.accenture.avs.be.dcq.vod.ingestor.dto.ESVLCContent;
import com.accenture.avs.be.dcq.vod.ingestor.dto.ESVLCContent.VlcContent;
import com.accenture.avs.be.dcq.vod.ingestor.dto.ESVodChannelDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.EsContentDoc;
import com.accenture.avs.be.dcq.vod.ingestor.utils.CacheConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.JsonUtils;
import com.accenture.avs.be.dcq.vod.ingestor.writer.DcqVodIngestionWriter;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.commons.lib.LoggerWrapper.OtherSystemCallType;
import com.accenture.avs.es.responses.GetAliasResponse;
import com.accenture.avs.es.utils.ElasticSearchClientUtils;

/**
 * @author karthik.vadla
 *
 */
@Component
public class DcqVodIngestionWriterImpl implements DcqVodIngestionWriter {
	private static final LoggerWrapper log = new LoggerWrapper(DcqVodIngestionWriterImpl.class);

	@Autowired
	private LanguageCache languageCache;

	@Override
	public DcqVodIngestionWriterResponseDTO writeToES(
			List<DcqVodIngestionWriterRequestDTO> dcqVodIngestionWriterRequestDTOs, String mode, String indexDate,
			Configuration config) throws ApplicationException {
		DcqVodIngestionWriterResponseDTO writerResponseDTO = null;
		BulkProcessor bulkProcessor = null;
		try {

			log.logMessage("Write the Assets to ES.");
			if (dcqVodIngestionWriterRequestDTOs == null || dcqVodIngestionWriterRequestDTOs.isEmpty()) {
				log.logMessage("No Assets to ES.");
				return null;
			}

			Integer esPublishCount = Integer.parseInt(config.getConstants().getValue(CacheConstants.DCQ_VOD_INGESTOR_PUBLISH_SIZE));

			BulkRequestBuilder bulkRequestBuilder = null;
			RestHighLevelClient highLevelRestClient = DcqVodIngestorUtils.getESHighLevelRestClient(config);
			// bulkRequestBuilder = client.prepareBulk();

			String indexPrefix = DcqVodIngestorConstants.PREFIX_VALUE;
			String contentIndex = indexPrefix + DcqVodIngestorConstants.UNDERSCORE
					+ DcqVodIngestorConstants.CONTENT_IN_LOWER;
			String vlcContentIndex = indexPrefix + DcqVodIngestorConstants.UNDERSCORE
					+ DcqVodIngestorConstants.VLCCONTENT_IN_LOWER;
			String vodChannelIndex = indexPrefix + DcqVodIngestorConstants.UNDERSCORE
					+ DcqVodIngestorConstants.VODCHANNEL_IN_LOWER;

			Set<Integer> publishedIdsNew = new HashSet<Integer>();
			Map<Integer, String> failMapNew = new HashMap<Integer, String>();
			for (Integer start = 0; start < dcqVodIngestionWriterRequestDTOs.size(); start += esPublishCount) {
				Integer end = Math.min(start + esPublishCount, dcqVodIngestionWriterRequestDTOs.size());
				List<DcqVodIngestionWriterRequestDTO> subList = dcqVodIngestionWriterRequestDTOs.subList(start, end);
				log.logMessage("No of Doc indexing in Bulk are: {}", subList.size());
				BulkResponse bulkResponse = indexDocs(subList, bulkRequestBuilder, highLevelRestClient);
				BulkItemResponse[] bulkItemResponses = bulkResponse.getItems();
				for (BulkItemResponse b : bulkItemResponses) {
					log.logMessage("ID: {} ;Index:{};Type:{};Fail Msg:{}", b.getId(), b.getIndex(), b.getType(),
							b.getFailureMessage());
					if (null == b.getFailureMessage())
						if (b.getIndex().contains(contentIndex) || b.getIndex().contains(vlcContentIndex)
								|| b.getIndex().contains(vodChannelIndex)) {
							String esId = b.getId();
							String[] esIdSplit = esId.split(DcqVodIngestorConstants.HYPHEN_SYMBOL);
							publishedIdsNew.add(Integer.parseInt(esIdSplit[0]));
						} else {
							publishedIdsNew.add(Integer.parseInt(b.getId()));
						}

					else {
						if (b.getIndex().contains(contentIndex) || b.getIndex().contains(vlcContentIndex)
								|| b.getIndex().contains(vodChannelIndex)) {
							String esId = b.getId();
							String[] esIdSplit = esId.split(DcqVodIngestorConstants.HYPHEN_SYMBOL);
							failMapNew.put(Integer.parseInt(esIdSplit[0]),
									b.getIndex() + ";" + b.getType() + ";" + b.getFailureMessage());
						} else {
							failMapNew.put(Integer.parseInt(b.getId()),
									b.getId() + ";" + b.getIndex() + ";" + b.getType() + ";" + b.getFailureMessage());
						}
					}

				}
			}

			writerResponseDTO = new DcqVodIngestionWriterResponseDTO();
			writerResponseDTO.setFailedIdsMap(failMapNew);
			writerResponseDTO.setPublishedIds(publishedIdsNew);
			log.logMessage("Ids successfully Published to ES: {}", publishedIdsNew);
			log.logMessage("Ids failed to Publish in ES: {}",
					(failMapNew == null ? 0 : failMapNew.keySet().toString()));
		} catch (Exception e) {
			log.logMessage(e.getMessage());
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR,
					new NestedParameters(e.getMessage()));
		} finally {
			if (bulkProcessor != null) {
				try {
					bulkProcessor.close();
				} catch (Exception e) {
					log.logMessage("Error while closing bulkProcessor.", e);
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR,
							new NestedParameters(e.getMessage()));
				}
			}
		}
		return writerResponseDTO;
	}

	@SuppressWarnings("unchecked")
	private BulkResponse indexDocs(List<DcqVodIngestionWriterRequestDTO> dcqVodIngestionWriterRequestDTOs,
			BulkRequestBuilder bulkRequestBuilder, RestHighLevelClient highLevelRestClient)
			throws ApplicationException {
		BulkResponse bulkResponse = null;
		String esDocStr = "";
		String esSuggestStr = "";
		String esPopularityStr = "";
		String esSearchStr = "";
		long startTime = System.currentTimeMillis();
		try {
			BulkRequest bulkRequest = new BulkRequest();
			log.logCallToOtherSystemRequestBody("ElasticSearch", "prepareIndex",
					JsonUtils.writeAsJsonString(dcqVodIngestionWriterRequestDTOs), OtherSystemCallType.INTERNAL);
			for (DcqVodIngestionWriterRequestDTO writerRequestDTO : dcqVodIngestionWriterRequestDTOs) {

				log.logMessage("Index: {};IndexType:{};UniqueId:{}", writerRequestDTO.getEsIndex(),
						writerRequestDTO.getEsIndexType(), writerRequestDTO.getEsUniqueId());

				if (!writerRequestDTO.isDeleted() && writerRequestDTO.getEsIndex() != null) {
					if (writerRequestDTO.getEsIndex().contains(DcqVodIngestorConstants.VLCCONTENT_IN_LOWER)) {
						ESVLCContent esVlcContent = (ESVLCContent) writerRequestDTO.getEsDocObj();
						VlcContent vlcContent = esVlcContent.getVlcContent();
						StringBuilder sb = new StringBuilder();
						sb.append(vlcContent.getChannel().getChannelId().toString());
						sb.append(DcqVodIngestorConstants.HYPHEN_SYMBOL);
						sb.append(vlcContent.getMetadata().getPlaylistPublishedDate());
						sb.append(DcqVodIngestorConstants.HYPHEN_SYMBOL);
						sb.append(vlcContent.getMetadata().getCpId());

						esDocStr = JsonUtils.writeAsJsonStringWithoutNull(writerRequestDTO.getEsDocObj());
						// bulkRequest.add(client.prepareIndex(writerRequestDTO.getEsIndex(),
						// DcqVodIngestorConstants.ALL_IN_LOWER,
						// sb.toString()).setSource(esDocStr,XContentType.JSON));
						bulkRequest.add(
								new IndexRequest(writerRequestDTO.getEsIndex(), DcqVodIngestorConstants.ALL_IN_LOWER,
										sb.toString()).source(esDocStr, XContentType.JSON));
					} else if (writerRequestDTO.getEsIndex().contains(DcqVodIngestorConstants.CONTENT_IN_LOWER)) {

						// AVS 5750 - update or insert the document with script
						esDocStr = JsonUtils.writeAsJsonStringWithoutNull(
								((EsContentDoc) writerRequestDTO.getEsDocObj()).getContent());
						esSuggestStr = JsonUtils.writeAsJsonStringWithoutNull(
								((EsContentDoc) writerRequestDTO.getEsDocObj()).getSuggest());
						esPopularityStr = JsonUtils.writeAsJsonStringWithoutNull(
								((EsContentDoc) writerRequestDTO.getEsDocObj()).getPopularity());
						esSearchStr = JsonUtils.writeAsJsonStringWithoutNull(
								((EsContentDoc) writerRequestDTO.getEsDocObj()).getSearchMetadata());

						// Convert the json string to an object which can be
						// read as key-value pairs so that, it can be set as
						// param value for script.
						Map<String, Object> esContentDocumentMap = new ObjectMapper().readValue(esDocStr,
								HashMap.class);
						Map<String, Object> esSuggestMap = null;
						Map<String, Object> esPopulairtyMap = null;
						if (esSuggestStr != null) {
							esSuggestMap = new ObjectMapper().readValue(esSuggestStr, HashMap.class);
						}
					
						if (esPopularityStr != null) {
							esPopulairtyMap = new ObjectMapper().readValue(esPopularityStr, HashMap.class);
						}

						String contentScript = DcqVodIngestorConstants.CONTENT_SCRIPT
								+ DcqVodIngestorConstants.CONTENT_SCRIPT_PARAM + ";"
								+ DcqVodIngestorConstants.SUGGEST_SCRIPT + DcqVodIngestorConstants.SUGGEST_SCRIPT_PARAM + ";";
						if (esPopularityStr != null) {	
							contentScript += DcqVodIngestorConstants.POPULARITY_SCRIPT
								+ DcqVodIngestorConstants.POPULARITY_SCRIPT_PARAM + ";";
						}
						if (esSearchStr != null) {	
							contentScript += DcqVodIngestorConstants.SEARCH_SCRIPT
								+ DcqVodIngestorConstants.SEARCH_SCRIPT_PARAM;
						}

						// Create a map for passing script params and set the
						// esContentDocumentMap as value
						Map<String, Object> contentParameters = new HashMap<>();
						contentParameters.put(DcqVodIngestorConstants.CONTENT_PARAM, esContentDocumentMap);
						if (esSuggestStr != null) {
							contentParameters.put(DcqVodIngestorConstants.SUGGEST_PARAM, esSuggestMap);
						}
						if (esPopularityStr != null) {
							contentParameters.put(DcqVodIngestorConstants.POPULARITY_PARAM, esPopulairtyMap);
						}
						if (esSearchStr != null) {
							contentParameters.put(DcqVodIngestorConstants.SEARCH_PARAM, ((EsContentDoc) writerRequestDTO.getEsDocObj()).getSearchMetadata());
						}

						// Create Script object with the actual script, type as
						// INLINE and the parameter map object.
						Script contentScriptObject = new Script(ScriptType.INLINE, "painless", contentScript,
								contentParameters);

						// prepare the update builder request object with Script
						// object and add it to bulk request.

						bulkRequest
								.add(new UpdateRequest(writerRequestDTO.getEsIndex(), writerRequestDTO.getEsIndexType(),
										writerRequestDTO.getEsUniqueId() + DcqVodIngestorConstants.HYPHEN_SYMBOL
												+ writerRequestDTO.getPlatformName()).script(contentScriptObject)
														.scriptedUpsert(true).upsert("{}", XContentType.JSON));

					} else if (writerRequestDTO.getEsIndex().contains(DcqVodIngestorConstants.CATEGORY_IN_LOWER)) {

						// AVS 5750 - update or insert the document with script
						esDocStr = JsonUtils.writeAsJsonStringWithoutNull(
								((ESCategoryDoc) writerRequestDTO.getEsDocObj()).getCategory());

						// Convert the json string to an object which can be
						// read as key-value pairs so that, it can be set as
						// param value for script.
						Map<String, Object> esCategoryDocumentMap = new ObjectMapper().readValue(esDocStr,
								HashMap.class);

						String categoryScript = DcqVodIngestorConstants.CATEGORY_SCRIPT
								+ DcqVodIngestorConstants.CATEGORY_SCRIPT_PARAM;

						// Create a map for passing script params and set the
						// esContentDocumentMap as value
						Map<String, Object> categoryParameters = new HashMap<>();
						categoryParameters.put(DcqVodIngestorConstants.CATEGORY_PARAM, esCategoryDocumentMap);

						// Create Script object with the actual script, type as
						// INLINE and the parameter map object.
						Script categoryScriptObject = new Script(ScriptType.INLINE, "painless", categoryScript,
								categoryParameters);

						// prepare the update builder request object with Script
						// object and add it to bulk request.
						bulkRequest.add(
								new UpdateRequest(writerRequestDTO.getEsIndex(), DcqVodIngestorConstants.ALL_IN_LOWER,
										writerRequestDTO.getEsUniqueId().toString()).script(categoryScriptObject)
												.scriptedUpsert(true).upsert("{}", XContentType.JSON));
					} else if (writerRequestDTO.getEsIndex().contains(DcqVodIngestorConstants.VODCHANNEL_IN_LOWER)) {

						esDocStr = JsonUtils.writeAsJsonStringWithoutNull(
								((ESVodChannelDTO) writerRequestDTO.getEsDocObj()).getVodChannel());
						esSuggestStr = JsonUtils.writeAsJsonStringWithoutNull(
								((ESVodChannelDTO) writerRequestDTO.getEsDocObj()).getSuggest());
						

						// Convert the json string to an object which can be
						// read as key-value pairs so that, it can be set as
						// param value for script.
						Map<String, Object> esVodChannelDocumentMap = new ObjectMapper().readValue(esDocStr,
								HashMap.class);
						Map<String, Object> esSuggestMap = new HashMap<>();
						if (esSuggestStr != null) {
							esSuggestMap = new ObjectMapper().readValue(esSuggestStr, HashMap.class);
						}
						

						// String vodChannelScript =
						// "ctx._source.vodChannel=params.vodChannelParam;ctx._source.suggest=params.suggestParam";
						String vodChannelScript = DcqVodIngestorConstants.VOD_CHANNEL_SCRIPT
								+ DcqVodIngestorConstants.VOD_CHANNEL_SCRIPT_PARAM + ";"
								+ DcqVodIngestorConstants.SUGGEST_SCRIPT + DcqVodIngestorConstants.SUGGEST_SCRIPT_PARAM + ";"
								+ DcqVodIngestorConstants.SEARCH_SCRIPT + DcqVodIngestorConstants.SEARCH_SCRIPT_PARAM;// ctx._source.suggest=params.suggestParam";
						// Create a map for passing script params and set the
						// esContentDocumentMap as value
						Map<String, Object> vodChannelParameters = new HashMap<>();
						vodChannelParameters.put(DcqVodIngestorConstants.VOD_CHANNEL_PARAM, esVodChannelDocumentMap);
						vodChannelParameters.put(DcqVodIngestorConstants.SUGGEST_PARAM, esSuggestMap);
						vodChannelParameters.put(DcqVodIngestorConstants.SEARCH_PARAM, ((ESVodChannelDTO) writerRequestDTO.getEsDocObj()).getSearchMetadata());

						// Create Script object with the actual script, type as
						// INLINE and the parameter map object.
						Script vodChannelScriptObject = new Script(ScriptType.INLINE, "painless", vodChannelScript,
								vodChannelParameters);

						bulkRequest.add(
								new UpdateRequest(writerRequestDTO.getEsIndex(), DcqVodIngestorConstants.ALL_IN_LOWER,
										writerRequestDTO.getEsUniqueId() + DcqVodIngestorConstants.HYPHEN_SYMBOL
												+ writerRequestDTO.getPlatformName()).script(vodChannelScriptObject)
														.scriptedUpsert(true).upsert("{}", XContentType.JSON));

						/*
						 * bulkRequest.add( new IndexRequest(writerRequestDTO.getEsIndex(),
						 * DcqVodIngestorConstants.ALL_IN_LOWER,
						 * writerRequestDTO.getEsUniqueId()+DcqVodIngestorConstants.HYPHEN_SYMBOL+
						 * writerRequestDTO.getPlatformName()).source(esDocStr,XContentType.JSON));
						 */
					}
					log.logMessage("Json:\n {}", esDocStr);
				}
			}

			log.logMessage("bulk start----", System.currentTimeMillis());
			byte[]  dcqIngestorUser = DcqVodIngestorUtils.getESDcqIngestorUser();
			byte[] dcqIngestorPassword = DcqVodIngestorUtils.getESDcqIngestorPassword();
			if (dcqIngestorUser != null && dcqIngestorUser.length>0) {
				RequestOptions requestOptions = ElasticSearchClientUtils.getRequestOptions(dcqIngestorUser, dcqIngestorPassword).build();
				bulkResponse = highLevelRestClient.bulk(bulkRequest, requestOptions);
			} else {
				bulkResponse = highLevelRestClient.bulk(bulkRequest, RequestOptions.DEFAULT);
			}
			log.logMessage("bulk end----", System.currentTimeMillis());
			if (log.isDebugEnabled() && null != bulkResponse) {
				log.logCallToOtherSystemResponseBody("ElasticSearch", "prepareIndex",
						JsonUtils.writeAsJsonString(bulkResponse.getItems()), OtherSystemCallType.INTERNAL);
			}
			if (null != bulkResponse) {
				log.logCallToOtherSystemEnd("ElasticSearch", "prepareIndex", "", "OK", "", "",
						System.currentTimeMillis() - startTime, OtherSystemCallType.INTERNAL);
			} else {
				log.logCallToOtherSystemEnd("ElasticSearch", "prepareIndex", "", "KO", "", "",
						System.currentTimeMillis() - startTime, OtherSystemCallType.INTERNAL);
			}

		} catch (Exception e) {
			log.logMessage("indexBulkDocuments-excepion:{}", e.getMessage());
			throw new ApplicationException("Exception while indexBulkDocuments");
		}
		return bulkResponse;
	}

	@Override
	public void dynamicMetadataUpdate(String indexName, String platformName, List<Object[]> comingSoonContents,
			List<Object[]> leavingSoonContents,List<Object[]> recentlyAddedContents, Configuration config) throws ApplicationException {

		log.logMessage("update comingSoon and leavingSoon to ES.");
		try {
			Integer esBulkUpdateCount = Integer
					.parseInt(config.getConstants().getValue(CacheConstants.DCQ_VOD_INGESTOR_UPDATE_BULK_SIZE));
			byte[]  dcqIngestorUser = DcqVodIngestorUtils.getESDcqIngestorUser();
			byte[] dcqIngestorPassword = DcqVodIngestorUtils.getESDcqIngestorPassword();
			RestHighLevelClient highLevelRestlient = DcqVodIngestorUtils.getESHighLevelRestClient(config);

			QueryBuilder totalQuery = QueryBuilders.boolQuery().filter(QueryBuilders.boolQuery()
					.should(QueryBuilders.termQuery(DcqVodIngestorConstants.CONTENT_METADATA_COMINGSOON, true))
					.should(QueryBuilders.termQuery(DcqVodIngestorConstants.CONTENT_METADATA_LEAVINGSOON, true))
					.should(QueryBuilders.termQuery(DcqVodIngestorConstants.CONTENT_METADATA_RECENTLYADDED, true)));
			SearchRequest searchRequest = new SearchRequest(indexName);
			SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
			searchSourceBuilder.query(totalQuery);
			searchRequest.types(DcqVodIngestorConstants.ALL_IN_LOWER);
			searchRequest.source(searchSourceBuilder);
			SearchResponse searchResponse = null;
			if (dcqIngestorUser != null && dcqIngestorUser.length>0) {
				RequestOptions requestOptions = ElasticSearchClientUtils.getRequestOptions(dcqIngestorUser, dcqIngestorPassword).build();
				searchResponse = highLevelRestlient.search(searchRequest, requestOptions);
			} else {
				searchResponse = highLevelRestlient.search(searchRequest, RequestOptions.DEFAULT);
			}
			Long totalHits = searchResponse.getHits().getTotalHits();

			/*
			 * Long totalHits = client.prepareSearch(indexName).setTypes(platformName)
			 * .setQuery(QueryBuilders.boolQuery() .filter(QueryBuilders.boolQuery()
			 * .should(QueryBuilders.termQuery(DcqVodIngestorConstants.
			 * CONTENT_METADATA_COMINGSOON, true))
			 * .should(QueryBuilders.termQuery(DcqVodIngestorConstants.
			 * CONTENT_METADATA_LEAVINGSOON,
			 * true)))).execute().actionGet().getHits().getTotalHits();
			 */
			log.logMessage("No of Doc need to update as default comingSoon and leavingSoon are: {}", totalHits);
			Integer esSearchFrequency = Integer.parseInt(config.getConstants().getValue(CacheConstants.DCQ_ES_SEARCH_FREQUENCY));

			for (Integer startEsSearch = 0; startEsSearch < totalHits; startEsSearch += esSearchFrequency) {
				Integer endEsSearch = Math.min(startEsSearch + esSearchFrequency, totalHits.intValue());
				log.logCallToOtherSystemRequestBody("ElasticSearch", "prepareSearch", "", OtherSystemCallType.INTERNAL);
				long startTime = System.currentTimeMillis();
				QueryBuilder query = QueryBuilders.boolQuery().filter(QueryBuilders.boolQuery()
						.should(QueryBuilders.termQuery(DcqVodIngestorConstants.CONTENT_METADATA_COMINGSOON, true))
						.should(QueryBuilders.termQuery(DcqVodIngestorConstants.CONTENT_METADATA_LEAVINGSOON, true))
						.should(QueryBuilders.termQuery(DcqVodIngestorConstants.CONTENT_METADATA_RECENTLYADDED, true)));
				SearchRequest searchRequestQuery = new SearchRequest(indexName);
				SearchSourceBuilder searchSourceBuilderQuery = new SearchSourceBuilder();
				searchSourceBuilderQuery.query(query).from(startEsSearch).size(endEsSearch);
				searchRequestQuery.types(DcqVodIngestorConstants.ALL_IN_LOWER);
				searchRequestQuery.source(searchSourceBuilderQuery);
				if (dcqIngestorUser != null && dcqIngestorUser.length>0) {
					RequestOptions requestOptions = ElasticSearchClientUtils.getRequestOptions(dcqIngestorUser, dcqIngestorPassword).build();
					searchResponse = highLevelRestlient.search(searchRequest, requestOptions);
				} else {
					searchResponse = highLevelRestlient.search(searchRequest, RequestOptions.DEFAULT);
				}

				/*
				 * SearchResponse response =
				 * client.prepareSearch(indexName).setTypes(platformName)
				 * .setQuery(QueryBuilders.boolQuery() .filter(QueryBuilders.boolQuery()
				 * .should(QueryBuilders.termQuery(DcqVodIngestorConstants.
				 * CONTENT_METADATA_COMINGSOON, true))
				 * .should(QueryBuilders.termQuery(DcqVodIngestorConstants.
				 * CONTENT_METADATA_LEAVINGSOON,
				 * true)))).setFrom(startEsSearch).setSize(endEsSearch).execute().actionGet();
				 */
				SearchHits responseHits = searchResponse.getHits();
				log.logCallToOtherSystemResponseBody("ElasticSearch", "prepareSearch", JsonUtils.writeAsJsonString(responseHits),
						OtherSystemCallType.INTERNAL);
				if (responseHits != null) {
					log.logCallToOtherSystemEnd("ElasticSearch", "prepareSearch", "", "OK", "", "",
							System.currentTimeMillis() - startTime, OtherSystemCallType.INTERNAL);
				} else {
					log.logCallToOtherSystemEnd("ElasticSearch", "prepareSearch", "", "KO", "", "",
							System.currentTimeMillis() - startTime, OtherSystemCallType.INTERNAL);
				}
				List<String> contentIdList = new ArrayList<String>();
				if (responseHits != null) {
					for (SearchHit hit : responseHits) {
						String id = hit.getId();
						contentIdList.add(id);
					}
				}
				log.logMessage("No of Doc retrieving from ElasticSearch are: {}", contentIdList.size());
				// To update comingSoon and leavingSoon value as default N into
				// ElasticSearch
				for (Integer start = 0; start < contentIdList.size(); start += esBulkUpdateCount) {
					Integer end = Math.min(start + esBulkUpdateCount, contentIdList.size());
					List<String> subList = contentIdList.subList(start, end);
					log.logMessage("No of default Doc updating in Bulk are: {}", subList.size());
					updateAsDefault(indexName, platformName, subList, highLevelRestlient);
				}
			}
			// To update comingSoon value into ElasticSearch
			for (Integer start = 0; start < comingSoonContents.size(); start += esBulkUpdateCount) {
				Integer end = Math.min(start + esBulkUpdateCount, comingSoonContents.size());
				List<Object[]> subList = comingSoonContents.subList(start, end);
				log.logMessage("No of comingSoon Doc  updating in Bulk are: {}", subList.size());
				updateComingSoon(subList, highLevelRestlient);
			}
			// To update leavingSoon value into ElasticSearch
			for (Integer start = 0; start < leavingSoonContents.size(); start += esBulkUpdateCount) {
				Integer end = Math.min(start + esBulkUpdateCount, leavingSoonContents.size());
				List<Object[]> subList = leavingSoonContents.subList(start, end);
				log.logMessage("No of leavingSoon Doc updating in Bulk are: {}", subList.size());
				updateLeavingSoon(subList, highLevelRestlient);
			}
			
			// To update recentlyAdded value into ElasticSearch
						for (Integer start = 0; start < recentlyAddedContents.size(); start += esBulkUpdateCount) {
							Integer end = Math.min(start + esBulkUpdateCount, recentlyAddedContents.size());
							List<Object[]> subList = recentlyAddedContents.subList(start, end);
							log.logMessage("No of recentlyAdded Doc updating in Bulk are: {}" , subList.size());
							updateRecentlyAdded(subList, highLevelRestlient);
			}

		} catch (Exception e) {
			log.logMessage(e.getMessage());
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR,
					new NestedParameters(e.getMessage()));
		}
	}
	
	
	private void updateRecentlyAdded(List<Object[]> subList, RestHighLevelClient highLevelRestClient) throws ApplicationException {
		BulkRequest bulkRequest = new BulkRequest();
		try {
			for (Object[] object : subList) {
				bulkRequest.add(new UpdateRequest(object[0].toString(), DcqVodIngestorConstants.ALL_IN_LOWER, object[2].toString()+DcqVodIngestorConstants.HYPHEN_SYMBOL+object[1].toString())
						.doc(jsonBuilder().startObject().startObject(DcqVodIngestorConstants.CONTENT_IN_LOWER)
								.startObject(DcqVodIngestorConstants.LOWER_CASE_METADATA)
								.field(DcqVodIngestorConstants.RECENTLY_ADDED, true).endObject().endObject().endObject()));
			}
			byte[]  dcqIngestorUser = DcqVodIngestorUtils.getESDcqIngestorUser();
			byte[] dcqIngestorPassword = DcqVodIngestorUtils.getESDcqIngestorPassword();
			if (dcqIngestorUser != null && dcqIngestorUser.length>0) {
			RequestOptions requestOptions = ElasticSearchClientUtils.getRequestOptions(dcqIngestorUser, dcqIngestorPassword).build();
			highLevelRestClient.bulk(bulkRequest, requestOptions);
			}else {
				highLevelRestClient.bulk(bulkRequest, RequestOptions.DEFAULT);
			}

		} catch (Exception e) {
			log.logMessage("updateBulkDocuments-excepion {}", e);
			throw new ApplicationException("Exception while updateBulkDocumentss");
		}
		
	}

	/**
	 * To update comingSoon and leavingSoon value as default N into ElasticSearch
	 * 
	 * @param indexName
	 * @param platformName
	 * @param subList
	 * @param client
	 * @throws ApplicationException
	 */
	private void updateAsDefault(String indexName, String platformName, List<String> subList,
			RestHighLevelClient highLevelRestClient) throws ApplicationException {

		BulkRequest bulkRequest = new BulkRequest();

		try {
			for (String contentId : subList) {
				bulkRequest.add(new UpdateRequest(indexName, DcqVodIngestorConstants.ALL_IN_LOWER, contentId).doc(jsonBuilder().startObject()
						.startObject(DcqVodIngestorConstants.CONTENT_IN_LOWER)
						.startObject(DcqVodIngestorConstants.LOWER_CASE_METADATA)
						.field(DcqVodIngestorConstants.COMING_SOON, false)
						.field(DcqVodIngestorConstants.LEAVING_SOON, false).endObject().endObject().endObject()));
			}

			byte[]  dcqIngestorUser = DcqVodIngestorUtils.getESDcqIngestorUser();
			byte[] dcqIngestorPassword = DcqVodIngestorUtils.getESDcqIngestorPassword();
			if (dcqIngestorUser != null && dcqIngestorUser.length>0) {
				RequestOptions requestOptions = ElasticSearchClientUtils.getRequestOptions(dcqIngestorUser, dcqIngestorPassword).build();
				highLevelRestClient.bulk(bulkRequest, requestOptions);
			} else {
				highLevelRestClient.bulk(bulkRequest, RequestOptions.DEFAULT);
			}

		} catch (Exception e) {
			log.logMessage("updateBulkDocuments-excepion {}", e);
			throw new ApplicationException("Exception while updateBulkDocumentss");
		}

	}

	/**
	 * To update comingSoon value into ElasticSearch
	 * 
	 * @param subList
	 * @param client
	 * @throws ApplicationException
	 */
	private void updateComingSoon(List<Object[]> subList, RestHighLevelClient highLevelRestClient)
			throws ApplicationException {
		BulkRequest bulkRequest = new BulkRequest();
		try {
			for (Object[] object : subList) {
				bulkRequest.add(new UpdateRequest(object[0].toString(), DcqVodIngestorConstants.ALL_IN_LOWER, object[2].toString()+DcqVodIngestorConstants.HYPHEN_SYMBOL+object[1].toString())
						.doc(jsonBuilder().startObject().startObject(DcqVodIngestorConstants.CONTENT_IN_LOWER)
								.startObject(DcqVodIngestorConstants.LOWER_CASE_METADATA)
								.field(DcqVodIngestorConstants.COMING_SOON, true).endObject().endObject().endObject()));
			}
			byte[]  dcqIngestorUser = DcqVodIngestorUtils.getESDcqIngestorUser();
			byte[] dcqIngestorPassword = DcqVodIngestorUtils.getESDcqIngestorPassword();
			if (dcqIngestorUser != null && dcqIngestorUser.length>0) {
				RequestOptions requestOptions = ElasticSearchClientUtils.getRequestOptions(dcqIngestorUser, dcqIngestorPassword).build();
				highLevelRestClient.bulk(bulkRequest, requestOptions);
			} else {
				highLevelRestClient.bulk(bulkRequest, RequestOptions.DEFAULT);
			}

		} catch (Exception e) {
			log.logMessage("updateBulkDocuments-excepion {}", e);
			throw new ApplicationException("Exception while updateBulkDocumentss");
		}
	}

	/**
	 * To update leavingSoon value into ElasticSearch
	 * 
	 * @param subList
	 * @param client
	 * @throws ApplicationException
	 */
	private void updateLeavingSoon(List<Object[]> subList, RestHighLevelClient highLevelRestClient)
			throws ApplicationException {
		BulkRequest bulkRequest = new BulkRequest();
		try {
			for (Object[] object : subList) {
				bulkRequest.add(new UpdateRequest(object[0].toString(), DcqVodIngestorConstants.ALL_IN_LOWER, object[2].toString()+DcqVodIngestorConstants.HYPHEN_SYMBOL+object[1].toString())
						.doc(jsonBuilder().startObject().startObject(DcqVodIngestorConstants.CONTENT_IN_LOWER)
								.startObject(DcqVodIngestorConstants.LOWER_CASE_METADATA)
								.field(DcqVodIngestorConstants.LEAVING_SOON, true).endObject().endObject()
								.endObject()));
			}
			byte[]  dcqIngestorUser = DcqVodIngestorUtils.getESDcqIngestorUser();
			byte[] dcqIngestorPassword = DcqVodIngestorUtils.getESDcqIngestorPassword();
			if (dcqIngestorUser != null && dcqIngestorUser.length>0) {
				RequestOptions requestOptions = ElasticSearchClientUtils.getRequestOptions(dcqIngestorUser, dcqIngestorPassword).build();
				highLevelRestClient.bulk(bulkRequest, requestOptions);
			} else {
				highLevelRestClient.bulk(bulkRequest, RequestOptions.DEFAULT);
			}

		} catch (Exception e) {
			log.logMessage("updateBulkDocuments-excepion {}", e);
			throw new ApplicationException("Exception while updateBulkDocumentss");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.accenture.avs.dcq.live.ingestor.interfaces.DcqLiveIngestionWriter#
	 * switchAlias(java.lang.String)
	 */
	@Override
	public void switchAlias(Configuration config) throws ApplicationException {
		try {
			switchAliasContent(config);
			switchAliasCategory(config);
			switchAliasVodChannel(config);
			switchAliasVLCContent(config);
		} catch (Exception e) {
			log.logMessage("switchAlias-excepion {}", e);
			throw new ApplicationException("Exception while switchAlias");
		}

	}

	/**
	 * @param tenantName
	 * @param client
	 * @throws ApplicationException
	 */
	private void switchAliasVLCContent(Configuration config) throws ApplicationException {

		Map<String, String> languageMap = languageCache.getLanguages();

		for (Map.Entry<String, String> language : languageMap.entrySet()) {
			executeSwitchAlias(config, DcqVodIngestorConstants.VLCCONTENT_IN_LOWER + DcqVodIngestorConstants.UNDERSCORE
					+ language.getKey().toLowerCase());
		}

	}

	/**
	 * @param tenantName
	 * @param client
	 * @throws ApplicationException
	 */
	private void switchAliasCategory(Configuration config) throws ApplicationException {
		Map<String, String> languageMap = languageCache.getLanguages();

		for (Map.Entry<String, String> language : languageMap.entrySet()) {
			executeSwitchAlias(config, DcqVodIngestorConstants.CATEGORY_IN_LOWER + DcqVodIngestorConstants.UNDERSCORE
					+ language.getKey().toLowerCase());
		}

	}

	/**
	 * @param tenantName
	 * @param client
	 * @throws ApplicationException
	 */
	private void switchAliasContent(Configuration config) throws ApplicationException {
		Map<String, String> languageMap = languageCache.getLanguages();

		for (Map.Entry<String, String> language : languageMap.entrySet()) {
			executeSwitchAlias(config, DcqVodIngestorConstants.CONTENT_IN_LOWER + DcqVodIngestorConstants.UNDERSCORE
					+ language.getKey().toLowerCase());
		}

	}

	/**
	 * @param tenantName
	 * @param client
	 * @throws ApplicationException
	 */
	private void switchAliasVodChannel(Configuration config) throws ApplicationException {
		executeSwitchAlias(config, DcqVodIngestorConstants.VODCHANNEL_IN_LOWER);
	}

	/**
	 * @param tenantName
	 * @param client
	 * @param aliasName
	 * @throws ApplicationException
	 */
	private void executeSwitchAlias(Configuration config, String aliasName) throws ApplicationException {
		try {

			GetAliasResponse getReadAliasResponse = DcqVodIngestorUtils.getIndexByAliasName(aliasName, config);
			// removing read alias form old indexes
			if (getReadAliasResponse != null) {
				DcqVodIngestorUtils.removeAlias(config, getReadAliasResponse.getIndexName(), aliasName);
			}

			GetAliasResponse getWriteAliasResponse = DcqVodIngestorUtils
					.getIndexByAliasName(DcqVodIngestorUtils.getWriteAlias(aliasName), config);
			// adding read alias to the new indexes
			if (getWriteAliasResponse != null) {
				DcqVodIngestorUtils.createAlias(config, getWriteAliasResponse.getIndexName(), aliasName);
			}

		} catch (Exception e) {
			log.logMessage("indexAliasAssociation-excepion {}", e);
			throw new ApplicationException("Exception while indexAliasAssociation");
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.accenture.avs.dcq.live.ingestor.interfaces.DcqLiveIngestionWriter#
	 * rollbackAlias(java.lang.String)
	 */
	@Override
	public void rollbackAlias(Configuration config) throws ApplicationException {
		try {
			rollbackAliasContent(config);
			rollbackAliasCategory(config);
			rollbackAliasVodChannel(config);
			rollbackAliasVLCContent(config);
		} catch (Exception e) {
			log.logMessage("rollbackAlias-excepion {}", e);
			throw new ApplicationException("Exception while rollbackAlias");
		}
	}

	/**
	 * @param tenantName
	 * @param client
	 * @throws ApplicationException
	 */
	private void rollbackAliasVodChannel(Configuration config) throws ApplicationException {
		executeRollbackAlias(DcqVodIngestorConstants.VODCHANNEL_IN_LOWER, config);
	}

	/**
	 * @param tenantName
	 * @param client
	 * @throws ApplicationException
	 */
	private void rollbackAliasVLCContent(Configuration config) throws ApplicationException {

		Map<String, String> languageMap = languageCache.getLanguages();

		for (Map.Entry<String, String> language : languageMap.entrySet()) {
			executeRollbackAlias(DcqVodIngestorConstants.VLCCONTENT_IN_LOWER + DcqVodIngestorConstants.UNDERSCORE
					+ language.getKey().toLowerCase(), config);
		}

	}

	/**
	 * @param tenantName
	 * @param client
	 * @throws ApplicationException
	 */
	private void rollbackAliasCategory(Configuration config) throws ApplicationException {
		Map<String, String> languageMap = languageCache.getLanguages();

		for (Map.Entry<String, String> language : languageMap.entrySet()) {
			executeRollbackAlias(DcqVodIngestorConstants.CATEGORY_IN_LOWER + DcqVodIngestorConstants.UNDERSCORE
					+ language.getKey().toLowerCase(), config);
		}
	}

	/**
	 * @param tenantName
	 * @param client
	 * @throws ApplicationException
	 */
	private void rollbackAliasContent(Configuration config) throws ApplicationException {
		Map<String, String> languageMap = languageCache.getLanguages();

		for (Map.Entry<String, String> language : languageMap.entrySet()) {
			executeRollbackAlias(DcqVodIngestorConstants.CONTENT_IN_LOWER + DcqVodIngestorConstants.UNDERSCORE
					+ language.getKey().toLowerCase(), config);
		}
	}

	/**
	 * @param tenantName
	 * @param client
	 * @param aliasName
	 * @throws DcqLiveIngestorException
	 */
	private void executeRollbackAlias(String aliasName, Configuration config) throws ApplicationException {
		try {
			GetAliasResponse getWriteAliasResponse = DcqVodIngestorUtils
					.getIndexByAliasName(DcqVodIngestorUtils.getWriteAlias(aliasName), config);
			// removing read alias form new indexes
			if (getWriteAliasResponse != null) {
				DcqVodIngestorUtils.removeAlias(config, getWriteAliasResponse.getIndexName(),
						DcqVodIngestorUtils.getWriteAlias(aliasName));
			}

			GetAliasResponse getReadAliasResponse = DcqVodIngestorUtils.getIndexByAliasName(aliasName, config);
			// adding write alias to the old indexes
			if (getReadAliasResponse != null) {
				DcqVodIngestorUtils.createAlias(config, getReadAliasResponse.getIndexName(),
						DcqVodIngestorUtils.getWriteAlias(aliasName));
			}

		} catch (Exception e) {
			log.logMessage("executeRollbackAlias-excepion {}", e);
			throw new ApplicationException("Exception while executeRollbackAlias");
		}

	}
}
