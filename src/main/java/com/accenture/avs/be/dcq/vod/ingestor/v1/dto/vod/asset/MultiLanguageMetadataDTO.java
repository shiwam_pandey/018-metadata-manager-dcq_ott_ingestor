package com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author karthik.vadla
 *
 */
public class MultiLanguageMetadataDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value="Language code based on the standard ISO 639-2-alpha3 , e.g: eng,hin,tam",required=true,example="eng")
	private String languageCode;
	
	@ApiModelProperty(value="Name of the language , e.g: English",required=true,example="english")
	private String languageName;
	
	@ApiModelProperty(value="Title",required=false,example="PK")
	private String title;
	
	@ApiModelProperty(value="Name of the Genre , e.g: COMEDY,FANTASY",required=false,example="COMEDY")
	private List<String> genres;
	
	@ApiModelProperty(value="Short Description",required=false,example="PK Movie")
	private String shortDescription;
	
	@ApiModelProperty(value="Description in detail",required=false,example="PK Movie")
	private String longDescription;
	
	@ApiModelProperty(value="Name of the country , e.g:USA,UK",required=false,example="USA")
	private List<String> countries;
	
	@ApiModelProperty(value="A brief version of the title",required=false,example="PK Movie")
	private String titleBrief;
	
	@ApiModelProperty(value="List of search keywords",required=false,example="PK,AmirKhan")
	private List<String> searchKeywords;
	
	@ApiModelProperty(value="Title of the Episode",required=false,example="PK Movie")
	private String episodeTitle;
	
	@ApiModelProperty(value="ExtendedMetadata , Contains a json object with custom fields for customization purpose",required=false,example="{\"plot\":\"On a far planet...\",\"actorsListDetail\":[{\"Name\":\"Sam Worthington\",\"BirthDate\":\"03/02/1981\",\"BirthCity\":\"New York\"},{\"Name\":\"Sigourney Weaver\",\"BirthDate\":\"04/03/1955\",\"BirthCity\":\"New York\"}]}")
	private String extendedMetadata;
	
	
	public String getLanguageCode() {
		return languageCode;
	}
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	public String getLanguageName() {
		return languageName;
	}
	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public List<String> getGenres() {
		return genres;
	}
	public void setGenres(List<String> genres) {
		this.genres = genres;
	}
	public String getShortDescription() {
		return shortDescription;
	}
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}
	public String getLongDescription() {
		return longDescription;
	}
	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}
	public List<String> getCountries() {
		return countries;
	}
	public void setCountries(List<String> countries) {
		this.countries = countries;
	}
	public String getTitleBrief() {
		return titleBrief;
	}
	public void setTitleBrief(String titleBrief) {
		this.titleBrief = titleBrief;
	}
	public List<String> getSearchKeywords() {
		return searchKeywords;
	}
	public void setSearchKeywords(List<String> searchKeywords) {
		this.searchKeywords = searchKeywords;
	}
	public String getEpisodeTitle() {
		return episodeTitle;
	}
	public void setEpisodeTitle(String episodeTitle) {
		this.episodeTitle = episodeTitle;
	}
	public String getExtendedMetadata() {
		return extendedMetadata;
	}
	public void setExtendedMetadata(String extendedMetadata) {
		this.extendedMetadata = extendedMetadata;
	}
	
	
}
