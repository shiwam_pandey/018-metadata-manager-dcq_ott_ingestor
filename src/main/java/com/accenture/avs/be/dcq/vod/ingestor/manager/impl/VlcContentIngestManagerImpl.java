package com.accenture.avs.be.dcq.vod.ingestor.manager.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionReaderResponseDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionWriterRequestDTO;
import com.accenture.avs.be.dcq.vod.ingestor.manager.VlcContentIngestManager;
import com.accenture.avs.be.dcq.vod.ingestor.processor.DcqVodIngestionPreProcessor;
import com.accenture.avs.be.dcq.vod.ingestor.processor.DcqVodIngestionProcessor;
import com.accenture.avs.be.dcq.vod.ingestor.reader.DcqVodIngestionReader;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.commons.lib.LoggerWrapper.OtherSystemCallType;

/**
 * @author karthik.vadla
 *
 */
@Component
public class VlcContentIngestManagerImpl implements VlcContentIngestManager {
	private static final LoggerWrapper log = new LoggerWrapper(VlcContentIngestManagerImpl.class);

	@Autowired
	private DcqVodIngestionPreProcessor dcqVodIngestionPreProcessor;

	@Autowired
	private DcqVodIngestionProcessor dcqVodIngestionProcessor;
	@Autowired
	private DcqVodIngestionReader dcqVodIngestionReader;
	@Autowired
	private DcqAssetStagingManager dcqAssetStagingManager;
	
	@Override
	public void intializeVlcContentIngestor(String mode, String indexDate, Configuration config) {
		log.logMessage("Initialize VlcContent Ingestor.");
		EntityManager em = null;
		DcqVodIngestionReaderResponseDTO readerResponseDTO = null;
		try {

			log.logMessage("Refresh Caches,Check & CreateIndexes - Start");
			long sTime = System.currentTimeMillis();
			dcqVodIngestionPreProcessor.refreshCachesCreateIndexes(DcqVodIngestorConstants.VLCCONTENT, mode, indexDate,
					config);
			log.logMessage("Refresh Caches,Check & CreateIndexes - Completed,took: {} ms",
					(System.currentTimeMillis() - sTime));

			long sTimeReader = System.currentTimeMillis();
			log.logMessage("VlcContent Ingestor Reader - Start");
			readerResponseDTO = dcqVodIngestionReader
					.retrieveChannelIdsFromPlaylistStaging(DcqVodIngestorConstants.VLCCONTENT, config);
			log.logMessage("VlcContent Ingestor Reader - Completed,took: {}",
					(System.currentTimeMillis() - sTimeReader));

			if (null == readerResponseDTO || readerResponseDTO.getChannelIdPlaylistDateDTOList() == null
					|| readerResponseDTO.getChannelIdPlaylistDateDTOList().isEmpty()) {
				log.logMessage("No VlcContent to Process, VlcContent Ingestor Completes.");
				return;
			}

			long sTimeProcessor = System.currentTimeMillis();
			log.logMessage("VlcContent Ingestor Processor - Start");
			List<DcqVodIngestionWriterRequestDTO> ingestionWriterRequestDTOs = dcqVodIngestionProcessor
					.processMetadataConstructESDocs(readerResponseDTO, DcqVodIngestorConstants.VLCCONTENT,
							DcqVodIngestorUtils.getTransactionNumber(), mode, indexDate, config);
			log.logMessage("VlcContent Ingestor Processor - Completed,took: {}",
					(System.currentTimeMillis() - sTimeProcessor));

		} catch (ApplicationException e) {
			log.logError(e, "There is an error,Update the status of all VlcContent Channel Ids to FAILED.");
			String errorInfo = DcqVodIngestorUtils.formatString(OtherSystemCallType.INTERNAL,
					DcqVodIngestorUtils.getLoggingMethod(), DcqVodIngestorUtils.getErrorStackTrace(e));
			if(null!=readerResponseDTO) {
			dcqAssetStagingManager.updatePlayListStatus(errorInfo,
					readerResponseDTO.getChannelIdPlaylistDateDTOList(), config);
			}
		} catch (Exception e) {
			log.logError(e, "There is an error,Update the status of all VlcContent Channel Ids to FAILED.");
			String errorInfo = DcqVodIngestorUtils.formatString(OtherSystemCallType.INTERNAL,
					DcqVodIngestorUtils.getLoggingMethod(), DcqVodIngestorUtils.getErrorStackTrace(e));
			if(null!=readerResponseDTO) {
			new DcqAssetStagingManager().updatePlayListStatus(errorInfo,
					readerResponseDTO.getChannelIdPlaylistDateDTOList(), config);
			}
		} finally {
			DcqVodIngestorUtils.closeEM(em);
		}
		log.logMessage("Initialize VlcContent Ingestor - Completed.");

	}
}
