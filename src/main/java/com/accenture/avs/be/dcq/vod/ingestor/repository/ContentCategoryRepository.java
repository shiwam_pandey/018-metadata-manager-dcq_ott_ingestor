package com.accenture.avs.be.dcq.vod.ingestor.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.avs.persistence.technicalcatalogue.ContentCategoryEntity;
/**
 * @author karthik.vadla
 *
 */
@Repository
@Transactional
public interface ContentCategoryRepository extends JpaRepository<ContentCategoryEntity,Integer>{

	/**
	 * @param contentId
	 * @return
	 */
	public List<Object[]> retrieveDetailsByContentId(@Param("contentId")Integer contentId);

	@Modifying
	public void deleteByContentId(@Param("contentId")Integer contentId);
	
	public List<Integer> retriveContentIdsByCategoryIds(@Param("categoryIdList") List<Integer>categoryIdlist);
	
	@Modifying
	public void deleteByCategoryIds(@Param("categoryIdList")List<Integer> categoryIds);

	@Modifying
	public void updatePrimaryForContents(@Param("contentIdList") List<Integer>contentIdlist);

}
