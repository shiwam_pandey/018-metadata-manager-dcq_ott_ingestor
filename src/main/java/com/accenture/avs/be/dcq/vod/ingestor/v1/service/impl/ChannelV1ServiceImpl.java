package com.accenture.avs.be.dcq.vod.ingestor.v1.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.accenture.avs.be.dcq.vod.ingestor.dto.Channel;
import com.accenture.avs.be.dcq.vod.ingestor.dto.ChannelDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.Platforms;
import com.accenture.avs.be.dcq.vod.ingestor.manager.VodChannelMetadataManager;
import com.accenture.avs.be.dcq.vod.ingestor.processor.DcqVodIngestionPreProcessor;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.Validator;
import com.accenture.avs.be.dcq.vod.ingestor.v1.manager.ChannelV1Manager;
import com.accenture.avs.be.dcq.vod.ingestor.v1.service.ChannelV1Service;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.be.framework.exception.ConfigurationException;
import com.accenture.avs.commons.lib.LoggerWrapper;

/**
 * @author naga.sireesha.meka
 *
 */
@Service
public class ChannelV1ServiceImpl implements ChannelV1Service {

	@Autowired
	private ResourceLoader resourceLoader;
	@Autowired
	private ChannelV1Manager channelManager;
	@Autowired
	private DcqVodIngestionPreProcessor dcqVodIngestionPreProcessor;
	@Autowired
	private VodChannelMetadataManager vodChannelMetadataManager;

	private static final LoggerWrapper log = new LoggerWrapper(ChannelV1ServiceImpl.class);

	@Override
	public GenericResponse upsertChannel(Channel channel, Configuration config)
			throws ApplicationException, Exception {
		GenericResponse genericResponse = null;
		ChannelDTO response = null;
		try {
			log.logMessage("call to initializeChannelIngestor ");

			boolean validJson = isJsonValid(channel);
			// boolean validXml = false;
			if (validJson) {
				log.logMessage("Input JSON is valid");
				
				if (Validator.isEmpty(channel)) {
					log.logMessage("No channels to write. The execution is completed.");
					genericResponse = new GenericResponse(DcqVodIngestorConstants.ACN_200, DcqVodIngestorConstants.OK,
							DcqVodIngestorConstants.NO_CHANNELS);
					return genericResponse;
				}

				// ingest into DB
				Integer channelId = ingestToDB(channel, config);
				
				if (channelId != null) {
					List<Integer> assetIdList = new ArrayList<>();
					assetIdList.add(channelId);
					dcqVodIngestionPreProcessor.refreshCachesCreateIndexes(DcqVodIngestorConstants.VODCHANNEL, "", "",
							config);
					vodChannelMetadataManager.processAndWriteVodChannels(assetIdList,
							DcqVodIngestorUtils.getTransactionNumber(), DcqVodIngestorUtils.currentTime(), "", "",
							config);
				}

			}
			genericResponse = new GenericResponse(DcqVodIngestorConstants.ACN_200, DcqVodIngestorConstants.OK,
					response);
		} catch (ApplicationException ae) {
			log.logError(ae);
			throw ae;

		} catch (JAXBException je) {
			log.logError(je);
			throw je;
		} catch (SAXParseException je) {
			log.logError(je);
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters("XML: " + je.getMessage()));

		} catch (SAXException je) {
			log.logError(je);
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters("XML: " + je.getMessage()));

		}catch (Exception e) {
			log.logError(e);
			throw e;
		}
		return genericResponse;
	}

	private Integer ingestToDB(Channel inputChannel, Configuration config)
			throws ApplicationException, Exception, ConfigurationException {
		
		Integer channelId = 0;
			if (isDefaultChannelExists(Integer.valueOf(inputChannel.getChannelId()),
					inputChannel.getDefaultChannelNumber().toString())) {
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
						new NestedParameters("defaultChannelNumber"));
			}
			for (int j = 0; j < inputChannel.getPackageIds().size(); j++) {
				if (Validator.isEmpty(channelManager
						.selectByPackageId(inputChannel.getPackageIds().get(j).intValue()))) {
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters("Package"));
				}
			}
		if (StringUtils.isNotBlank(inputChannel.getType())) {
			if (!Arrays.asList(DcqVodIngestorConstants.VALID_CHANNEL_TYPES).contains(inputChannel.getType())) {
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
						new NestedParameters("type . Expected values VLC,SVOD_CATCHUP"));
			}
		}
		if (!CollectionUtils.isEmpty(inputChannel.getPlatforms())) {
			for (Platforms platform : inputChannel.getPlatforms()) {
				try {
					config.getPlatforms().getValue(platform.getName());
				} catch (ConfigurationException ex) {
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters("platform name"));
				}

			}
		}
		

			channelId = channelManager.createChannel(inputChannel);
		return channelId;
	}

	/**
	 * @param defaultChannelNumber
	 * @param channelId
	 * @param hibernateTemplate
	 * @return boolean
	 */
	private boolean isDefaultChannelExists(Integer channelId, String defaultChannelNumber) {

		boolean flag = false;
		List<Integer> channelIds = channelManager.retrieveByDefaultChannelNumber(defaultChannelNumber);

		if (!Validator.isEmptyCollection(channelIds)) {
			for (Integer id : channelIds) {
				if (!(id.equals(channelId))) {
					flag = true;
				}
			}
		}
		return flag;
	}
	
	private boolean isJsonValid(Channel channel) throws ApplicationException{
		boolean jsonvalidFlag;
		try{
			if(channel == null){
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER, new NestedParameters("Channel Json"));
			}
			
		   if(channel.getChannelId() == 0){
			   throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER, new NestedParameters("ChannelId"));  
		   }
		   checkForStrMaxFifty("Name",channel.getName());
		   checkForStrMaxFifty("Type",channel.getType());
		   if(channel.getOrder() == 0){
			   throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER, new NestedParameters("Order"));  
		   }
		   checkForStrMaxTwoFiftyFive("ExternalId",channel.getExternalId());
		   Validator.sanitizingParamsForSpecialParams(DcqVodIngestorConstants.VODCONSTANTSV1.EXTERNAL_ID,channel.getExternalId());
		   checkForYN("Adult",channel.getAdult());
		   checkForStrMinOneMaxThousand("Description",channel.getDescription());
		   if(CollectionUtils.isEmpty(channel.getPlatforms())){
			   throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER, new NestedParameters("Platforms"));  
		   }else{
			   for(Platforms channelPlatform : channel.getPlatforms()){
				   checkForStrMaxFifty("PlatformName",channelPlatform.getName());
				   Validator.sanitizingParamsForSpecialParams(DcqVodIngestorConstants.VODCONSTANTSV1.PLATFORM_NAME,channelPlatform.getName());
				   checkForYN("Published",channelPlatform.getIsPublished());
				   checkForStrMinZeroMaxThousand("VideoURL",channelPlatform.getVideoURL());
				   checkForStrMinZeroMaxThousand("TrailerURL",channelPlatform.getTrailerURL());
				   checkForStrMaxHundred("LogoSmall",channelPlatform.getLogoSmall());
				   checkForStrMaxHundred("LogoMedium",channelPlatform.getLogoMedium());
				   checkForStrMaxHundred("LogoBig",channelPlatform.getLogoBig());
				   
				   if(StringUtils.isBlank(channelPlatform.getGroup())){
					   throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER, new NestedParameters("platform group"));  
				   }
				   if(channelPlatform.getGroup() !=null && !channelPlatform.getGroup().equalsIgnoreCase("Standard") && !channelPlatform.getGroup().equalsIgnoreCase("Extended")){
					   throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters("group . Expected values Standard,Extended"));  
				   }
			   }
		   }
		   
		   if(CollectionUtils.isEmpty(channel.getPackageIds())){
			   throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER, new NestedParameters("packageIds"));  
		   }
		   
		   checkForStrMinOneMaxThousand("AdvTags",channel.getAdvTags());
		   checkForYN("IsDisAllowedAdv",channel.getIsDisAllowedAdv());
		   checkForYN("IsActive",channel.getIsActive());
		   jsonvalidFlag = true;
		}catch(ApplicationException ae){
			throw ae;
		}catch (Exception e) {
			log.logError(e);
			throw e;
		}
		
		return jsonvalidFlag;
		
	}

	void checkForStrMaxFifty(String paramName,String paramValue) throws ApplicationException{
		
		try{
			if(StringUtils.isBlank(paramValue)){
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER, new NestedParameters(paramName));
			}
			if(paramValue.length() > 50){
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(paramName + "Maximum length is 50"));
			}
			
		}catch(ApplicationException ae){
			log.logError(ae);
			throw ae;
		}
		
	}
	
	
void checkForStrMaxHundred(String paramName,String paramValue) throws ApplicationException{
		
		try{
			if(StringUtils.isBlank(paramValue)){
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER, new NestedParameters(paramName));
			}
			else if(paramValue.length() > 100){
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(paramName + "Maximum length is 100"));
			}
			
		}catch(ApplicationException ae){
			log.logError(ae);
			throw ae;
		}
		
	}
	
	void checkForStrMaxTwoFiftyFive(String paramName,String paramValue) throws ApplicationException{
		
		try{
			
			if(paramValue!=null && paramValue.length() > 255){
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(paramName + "Maximum length is 255"));
			}
			
		}catch(ApplicationException ae){
			log.logError(ae);
			throw ae;
		}
		
	}
	
	
	
	
	 void checkForYN(String paramName,String paramValue)throws ApplicationException{
		 
		 try{
				
				if(paramValue!=null &&!paramValue.equals("Y") && !paramValue.equals("N") ){
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(paramName + ". Expected value Y or N"));
				}
				
			}catch(ApplicationException ae){
				log.logError(ae);
				throw ae;
			}
		 
	 }
	 void checkForStrMinOneMaxThousand(String paramName,String paramValue)throws ApplicationException{
		 
		 try{
				
				if(paramValue!=null && (paramValue.isEmpty())){
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(paramName + ". Cannot be empty"));
				}else if(paramValue!=null && (paramValue.length() > 1000)){
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(paramName + ". Maximum length 1000"));
				}
				
			}catch(ApplicationException ae){
				log.logError(ae);
				throw ae;
			}
		 
	 }
	 
	 
	 
 void checkForStrMinZeroMaxThousand(String paramName,String paramValue)throws ApplicationException{
		 
		 try{
				
				if(paramValue!=null && paramValue.length() > 1000){
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(paramName + ". Maximum length 1000"));
				}
				
			}catch(ApplicationException ae){
				log.logError(ae);
				throw ae;
			}
		 
	 }
	
}


