package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class DeviceTypeDTO implements Serializable{

			private final static long serialVersionUID = 1L;
			@ApiModelProperty(required = true, value = "Unique identifier of deviceType" , example = "1")
			private long deviceTypeId;
			@ApiModelProperty(required = true, value = "Name of the device" , example = "STBTIVUON")
			private String deviceTypeName;
			@ApiModelProperty(required = true, value = "Unique identifier of platform", example = "1")
			private long platformId;
			@ApiModelProperty(required = true, value = "Name of the platform" ,example = "PCTV")
			private String platformName;
			@ApiModelProperty(required = false, value = "List of tuples associated to device type")
			private DeviceTypeDTO.DeviceTypeTuplesList deviceTypeTuplesList;

			/**
			 * Gets the value of the deviceTypeId property.
			 * 
			 */
			public long getDeviceTypeId() {
				return deviceTypeId;
			}

			/**
			 * Sets the value of the deviceTypeId property.
			 * 
			 */
			public void setDeviceTypeId(long value) {
				this.deviceTypeId = value;
			}

			/**
			 * Gets the value of the deviceTypeName property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getDeviceTypeName() {
				return deviceTypeName;
			}

			/**
			 * Sets the value of the deviceTypeName property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setDeviceTypeName(String value) {
				this.deviceTypeName = value;
			}

			/**
			 * Gets the value of the platformId property.
			 * 
			 */
			public long getPlatformId() {
				return platformId;
			}

			/**
			 * Sets the value of the platformId property.
			 * 
			 */
			public void setPlatformId(long value) {
				this.platformId = value;
			}

			/**
			 * Gets the value of the platformName property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getPlatformName() {
				return platformName;
			}

			/**
			 * Sets the value of the platformName property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setPlatformName(String value) {
				this.platformName = value;
			}

			/**
			 * Gets the value of the deviceTypeTuplesList property.
			 * 
			 * @return possible object is
			 *         {@link DeviceTypeDTO.DeviceTypeTuplesList }
			 * 
			 */
			public DeviceTypeDTO.DeviceTypeTuplesList getDeviceTypeTuplesList() {
				return deviceTypeTuplesList;
			}

			/**
			 * Sets the value of the deviceTypeTuplesList property.
			 * 
			 * @param value
			 *            allowed object is
			 *            {@link DeviceTypeDTO.DeviceTypeTuplesList }
			 * 
			 */
			public void setDeviceTypeTuplesList(
					DeviceTypeDTO.DeviceTypeTuplesList value) {
				this.deviceTypeTuplesList = value;
			}

			public static class DeviceTypeTuplesList implements Serializable {

				private final static long serialVersionUID = 1L;
				private List<DeviceTypeDTO.DeviceTypeTuplesList.DeviceTuple> deviceTuple;

				/**
				 * Gets the value of the deviceTuple property.
				 * 
				 * <p>
				 * This accessor method returns a reference to the live list, not a snapshot.
				 * Therefore any modification you make to the returned list will be present
				 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
				 * for the deviceTuple property.
				 * 
				 * <p>
				 * For example, to add a new item, do as follows:
				 * 
				 * <pre>
				 * getDeviceTuple().add(newItem);
				 * </pre>
				 * 
				 * 
				 * <p>
				 * Objects of the following type(s) are allowed in the list
				 * {@link DeviceTypeDTO.DeviceTypeTuplesList.DeviceTuple }
				 * 
				 * 
				 */
				public List<DeviceTypeDTO.DeviceTypeTuplesList.DeviceTuple> getDeviceTuple() {
					if (deviceTuple == null) {
						deviceTuple = new ArrayList<DeviceTypeDTO.DeviceTypeTuplesList.DeviceTuple>();
					}
					return this.deviceTuple;
				}

				public static class DeviceTuple implements Serializable {

					private final static long serialVersionUID = 1L;
					@ApiModelProperty(required = true, value = "Unique identifier of deviceTuple", example = "2")
					private long deviceTupleId;
					@ApiModelProperty(required = true, value = "Name of the brand . If the deviceTypeTuplesList is available then deviceVendor is mandatory",example = "Samsung")
					private String deviceVendor;
					@ApiModelProperty(required = true, value = "Name of the model . If the deviceTypeTuplesList is available then deviceModel is mandatory", example = "Samsung-1893-9DB")
					private String deviceModel;
					@ApiModelProperty(required = true, value = "OS name . If the deviceTypeTuplesList is available then deviceOperatingSystem is mandatory", example = "model-os")
					private String deviceOperatingSystem;
					@ApiModelProperty(required = true, value = "OS version . If the deviceTypeTuplesList is available then deviceOperatingSystemVersion is mandatory",example = "2.13")
					private String deviceOperatingSystemVersion;

					/**
					 * Gets the value of the deviceTupleId property.
					 * 
					 */
					public long getDeviceTupleId() {
						return deviceTupleId;
					}

					/**
					 * Sets the value of the deviceTupleId property.
					 * 
					 */
					public void setDeviceTupleId(long value) {
						this.deviceTupleId = value;
					}

					/**
					 * Gets the value of the deviceVendor property.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					public String getDeviceVendor() {
						return deviceVendor;
					}

					/**
					 * Sets the value of the deviceVendor property.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setDeviceVendor(String value) {
						this.deviceVendor = value;
					}

					/**
					 * Gets the value of the deviceModel property.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					public String getDeviceModel() {
						return deviceModel;
					}

					/**
					 * Sets the value of the deviceModel property.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setDeviceModel(String value) {
						this.deviceModel = value;
					}

					/**
					 * Gets the value of the deviceOperatingSystem property.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					public String getDeviceOperatingSystem() {
						return deviceOperatingSystem;
					}

					/**
					 * Sets the value of the deviceOperatingSystem property.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setDeviceOperatingSystem(String value) {
						this.deviceOperatingSystem = value;
					}

					/**
					 * Gets the value of the deviceOperatingSystemVersion property.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					public String getDeviceOperatingSystemVersion() {
						return deviceOperatingSystemVersion;
					}

					/**
					 * Sets the value of the deviceOperatingSystemVersion property.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setDeviceOperatingSystemVersion(String value) {
						this.deviceOperatingSystemVersion = value;
					}

				}

			}

		}
