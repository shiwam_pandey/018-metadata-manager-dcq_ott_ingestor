package com.accenture.avs.be.dcq.vod.ingestor.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accenture.avs.be.dcq.vod.ingestor.dto.PackageResponseDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.ResponseGetList;
import com.accenture.avs.be.dcq.vod.ingestor.dto.ResponsePackage;
import com.accenture.avs.be.dcq.vod.ingestor.executor.PackageThreadPoolExecutor;
import com.accenture.avs.be.dcq.vod.ingestor.repository.TechnicalPackageRepository;
import com.accenture.avs.be.dcq.vod.ingestor.service.PackageService;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorRestUrls;
import com.accenture.avs.be.dcq.vod.ingestor.utils.Validator;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.configuration.CacheConstants;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.be.framework.exception.ConfigurationException;
import com.accenture.avs.be.framework.utils.Utilities;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.persistence.technicalcatalogue.TechnicalPackageEntity;

/**
 * @author karthik.vadla
 *
 */
@Service
public class PackageServiceImpl implements PackageService{
	private static final LoggerWrapper log = new LoggerWrapper(PackageServiceImpl.class);
	@Autowired
	private TechnicalPackageRepository technicalPackageRepository;
	@Autowired
	private DcqVodIngestorRestUrls dcqVodIngestorRestUrls;
	/* (non-Javadoc)
	 * @see com.accenture.avs.be.dcq.vod.ingestor.service.PackageService#getPackages(java.lang.String, com.accenture.avs.be.framework.cache.Configuration)
	 */
	@Override
	public GenericResponse getPackages(String propertyName, String startIndex, String maxResults, String sortBy, String sortOrder, Configuration config) throws ConfigurationException,ApplicationException, Exception {
		
		GenericResponse genericResponse = null;
		PackageResponseDTO packageResponse = new PackageResponseDTO();
		List<ResponsePackage> respPackageList = new ArrayList<ResponsePackage>();
		List<ResponsePackage> subList = new ArrayList<ResponsePackage>();
		List<String> propertyNames = new ArrayList<>();
		if(!Utilities.isEmpty(propertyName)) {
			if(propertyName.length()<=100){
		 propertyNames = Arrays.asList(propertyName
				.split(","));
		}
			else{
				throw new ApplicationException(
						CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
						new NestedParameters(DcqVodIngestorConstants.PROPERTYNAMECONSTANTS.PROPERTYNAME,"propertyName not more than 100 characters"));
			}
		}
		Validator.validateIntNotNegativeParameter(DcqVodIngestorConstants.START_INDEX, startIndex, config);
		Validator.validateIntNotNegativeParameter(DcqVodIngestorConstants.MAX_RESULTS, maxResults, config);
		
		respPackageList = getPackageResponseList(propertyNames,config);
	
		int from = StringUtils.isBlank(startIndex)? 0 : Integer.parseInt(startIndex);
		int size = StringUtils.isBlank(maxResults)? 50 : Integer.parseInt(maxResults) ;

		if(size > 50) {
			size = 50;
		}

		if(from + size > respPackageList.size()) {
			size = respPackageList.size();
		}else {
			size = from + size;
		}
		
		subList = respPackageList.subList(from, size);
	     packageResponse.setTotalResults(respPackageList.size());
	     packageResponse.setPackages(subList);
		genericResponse = new GenericResponse(DcqVodIngestorConstants.ACN_200,DcqVodIngestorConstants.OK, packageResponse);
		return genericResponse;
	}


	/* (non-Javadoc)
	 * @see com.accenture.avs.be.dcq.vod.ingestor.service.PackageService#getPackagesForLegacy(java.lang.String, com.accenture.avs.be.framework.cache.Configuration)
	 */
	@Override
	public ResponseGetList getPackagesForLegacy(String propertyName, Configuration config) throws ConfigurationException, ApplicationException, Exception {
		ResponseGetList response = new ResponseGetList();
		List<ResponsePackage> respPackageList = new ArrayList<ResponsePackage>();
		List<String> propertyNames = new ArrayList<>();
		if(!Utilities.isEmpty(propertyName)) {
			if(propertyName.length()<=100){
		 propertyNames = Arrays.asList(propertyName
				.split(","));
		}
			else{
				throw new ApplicationException(
						CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
						new NestedParameters(DcqVodIngestorConstants.PROPERTYNAMECONSTANTS.PROPERTYNAME,"propertyName not more than 100 characters"));
			}
		}
		respPackageList = getPackageResponseList(propertyNames, config);

		response.setResultCode(DcqVodIngestorConstants.OK);
		response.setResultObj(respPackageList);
		return response;
	}
	
	/**
	 * @param propertyNames
	 * @param config
	 * @return
	 * @throws ConfigurationException
	 * @throws Exception
	 */
	private List<ResponsePackage> getPackageResponseList(List<String> propertyNames, Configuration config) throws ConfigurationException, ApplicationException, Exception {
	
		List<ResponsePackage> respPackageList = new ArrayList<ResponsePackage>();
		List<TechnicalPackageEntity> technicalPackageList = technicalPackageRepository.retrievePackagesSVOD();
		Integer processorThreads = 5;
		ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(processorThreads);
		List<Future<List<ResponsePackage>>> futureTasks = new ArrayList<Future<List<ResponsePackage>>>();

		for (Integer start = 0; start < technicalPackageList.size(); start += 50) {
			Integer end = Math.min(start + 50, technicalPackageList.size());
			List<TechnicalPackageEntity> subList = technicalPackageList.subList(start, end);
			List<String> technicalPackageIds = new ArrayList<>();
			technicalPackageIds.addAll(subList.stream().map(TechnicalPackageEntity::getPackageId)
					.map(packageId -> String.valueOf(packageId)).collect(Collectors.toList()));
			try {
				log.logMessage("technical pavkage ids: {}", technicalPackageIds.toString());
				PackageThreadPoolExecutor packageThreadPoolExecutor = new PackageThreadPoolExecutor(technicalPackageIds,
						subList, propertyNames, config, dcqVodIngestorRestUrls.COMMERCIAL_PACKAGE_DETAILS_V2_URL);
				Future<List<ResponsePackage>> task = executor.submit(packageThreadPoolExecutor);
				futureTasks.add(task);
			} catch (Exception e) {
				log.logError(e);
			}
		}
		for (Future<List<ResponsePackage>> task : futureTasks) {
			respPackageList.addAll(task.get());
		}
		return respPackageList;
	}
	
	

}