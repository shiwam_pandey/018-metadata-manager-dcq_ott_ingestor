package com.accenture.avs.be.dcq.vod.ingestor.v1.service;

import org.springframework.stereotype.Service;

import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.bundle.asset.BundleDTO;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;

@Service("BundleIngestionServicev1")
public interface BundleIngestionV1Service {	
	GenericResponse bundleIngestion(BundleDTO bundle, Configuration config) throws Exception;
}


