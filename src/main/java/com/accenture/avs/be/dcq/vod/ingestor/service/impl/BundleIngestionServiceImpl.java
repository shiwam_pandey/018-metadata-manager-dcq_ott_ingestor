package com.accenture.avs.be.dcq.vod.ingestor.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.accenture.avs.be.dcq.vod.ingestor.dto.CommercialPackagesResponseDTO.CommercialPackage;
import com.accenture.avs.be.dcq.vod.ingestor.dto.bundle.asset.AddProductInfo;
import com.accenture.avs.be.dcq.vod.ingestor.dto.bundle.asset.Bundle;
import com.accenture.avs.be.dcq.vod.ingestor.dto.bundle.asset.Bundle.BundleContentList;
import com.accenture.avs.be.dcq.vod.ingestor.dto.bundle.asset.BundleContentList.ExternalContentId;
import com.accenture.avs.be.dcq.vod.ingestor.manager.BundleManager;
import com.accenture.avs.be.dcq.vod.ingestor.manager.ContentMetadataManager;
import com.accenture.avs.be.dcq.vod.ingestor.manager.impl.DcqVodIngestorPropertiesManager;
import com.accenture.avs.be.dcq.vod.ingestor.processor.DcqVodIngestionPreProcessor;
import com.accenture.avs.be.dcq.vod.ingestor.sdp.manager.impl.SdpClientManagerImpl;
import com.accenture.avs.be.dcq.vod.ingestor.service.BundleIngestionService;
import com.accenture.avs.be.dcq.vod.ingestor.utils.CacheConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorRestUrls;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.Validator;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.be.framework.exception.ConfigurationException;
import com.accenture.avs.be.framework.utils.Utilities;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.persistence.technicalcatalogue.BundleAggregationEntity;
import com.accenture.avs.persistence.technicalcatalogue.BundleAggregationEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.ContentEntity;
import com.accenture.avs.persistence.technicalcatalogue.ContentPlatformEntity;
import com.accenture.avs.persistence.technicalcatalogue.RelPlatformTechnicalEntity;
import com.accenture.avs.persistence.technicalcatalogue.TechnicalPackageEntity;

/**
 * @author naga.sireesha.meka
 *
 */
@Service
public class BundleIngestionServiceImpl implements BundleIngestionService {

	@Autowired
private	ResourceLoader resourceLoader;

	@Autowired
	private DcqVodIngestorPropertiesManager dcqVodIngestorPropertiesManager;

	@Autowired
private	BundleManager bundleManager;

	@Autowired
	private ContentMetadataManager contentMetadataManager;
	
	@Autowired
	private DcqVodIngestionPreProcessor dcqVodIngestionPreProcessor;
	
	@Autowired
	private DcqVodIngestorRestUrls dcqVodIngestorRestUrls;

	private static final LoggerWrapper log = new LoggerWrapper(BundleIngestionServiceImpl.class);

	private Boolean isBundle;
	private Set<Serializable> bundleAggregationList;
	private List<Integer> assetIdList;

	@Override
	public GenericResponse bundleIngestion(String bundleXml, boolean sync, Configuration config)
			throws ApplicationException, Exception {
		GenericResponse genericResponse = null;
		StringReader reader=null;
		InputStream bundleXSD=null;
		try{
		 bundleXSD = resourceLoader
				.getResource("classpath:dcq-vod-ingestor/" + DcqVodIngestorConstants.BUNDLE_XSD).getInputStream();

		/*
		 *  Replace all unescaped ampersands with escaped
		 * ampersands for AssociateWebSiteUrl
		 */
		if (DcqVodIngestorUtils.checkVulnerablesInInputXML(bundleXml)) {
			throw new ApplicationException(com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters(DcqVodIngestorConstants.ERR_VULNERABLE_REQ));
						}
		final Pattern unescapedAmpersands = Pattern.compile("(&(?!amp;))");
		Matcher m = unescapedAmpersands.matcher(bundleXml);
		String xmlWithAmpersandsEscaped = m.replaceAll("&amp;");
		
		// validating the xml
		boolean validXml = Validator.validateXMLSchema(bundleXSD, xmlWithAmpersandsEscaped);

		if (validXml) {
			log.logMessage("Input XML is valid");
			// un marshalling the xml
			JAXBContext jaxbContext = JAXBContext.newInstance(Bundle.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			 reader = new StringReader(xmlWithAmpersandsEscaped);
			Bundle bundle = (Bundle) unmarshaller.unmarshal(reader);
			if (Validator.isEmpty(bundle)) {
				log.logMessage("No bundle to write. The execution is completed.");
				genericResponse = new GenericResponse(DcqVodIngestorConstants.ACN_200, DcqVodIngestorConstants.OK,
						DcqVodIngestorConstants.NO_BUNDLE);
				return genericResponse;
			}
			
			if (bundle.getProductInfo() != null && bundle.getProductInfo().getPurchaseStartDateTime() == null
					&& bundle.getProductInfo().getPurchaseEndDateTime() != null) {
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
						new NestedParameters(DcqVodIngestorConstants.INVALID_PURCHASE_END_DATE));
			}
			log.logMessage("Ingesting bundle in to DB");
			Integer bundleId = ingestBundleToDB(bundle, config);
			
			if(bundleId != null){
				log.logMessage("Publish contents to ES");
				if(!Utilities.isEmpty(assetIdList) && !assetIdList.isEmpty()){
					dcqVodIngestionPreProcessor.refreshCachesCreateIndexes(DcqVodIngestorConstants.CONTENT, "", "",
							config);
					contentMetadataManager.processandWriteContents(assetIdList, DcqVodIngestorUtils.getTransactionNumber(), DcqVodIngestorUtils.currentTime(), "", "", config);
				}
			}
		}
		genericResponse = new GenericResponse(DcqVodIngestorConstants.ACN_200, DcqVodIngestorConstants.OK,
				null);
		}catch (ApplicationException ae) {
			log.logError(ae);
			throw ae;

		} catch (JAXBException je) {
			log.logError(je);
			throw je;
		} catch (SAXParseException je) {
			log.logError(je);
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters("XML: " + je.getMessage()));

		} catch (SAXException je) {
			log.logError(je);
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters("XML: " + je.getMessage()));

		}catch (Exception e) {
			log.logError(e);
			throw e;
		}finally{
			if(reader!=null){
				reader.close();
			}
			if(bundleXSD!=null)bundleXSD.close();
		}

		return genericResponse;
	}

	/**
	 * This method is used to ingest bundle to DB
	 * @param bundles
	 * @param config
	 * @return 
	 * @throws ApplicationException
	 * @throws ConfigurationException
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	private Integer ingestBundleToDB(Bundle bundle, Configuration config)
			throws ApplicationException, ConfigurationException, JsonParseException, JsonMappingException, IOException {
		ContentEntity bundleContent = Boolean.valueOf(bundle.getBundleExternalContentId().getIsExternalId().value())
				? contentMetadataManager.getContentByExternalId(bundle.getBundleExternalContentId().getValue())
				: contentMetadataManager
						.getContentByContentId(Integer.parseInt(bundle.getBundleExternalContentId().getValue()));
		Validator.validateBundle(bundle, bundleContent,config);
		// Getting all the content of Bundle and Group of Bundle
		Map<ContentEntity, List<ContentEntity>> contentOfAllBundle = getContentOfAllBundle(bundle);

		// Checking the Parent Content is Bundle or Group of Bundle
		isBundle = isBundle(contentOfAllBundle);

		// Checking for if group of bundle doesn't have proper Bundle content
		if (bundle == null || isBundle == null) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(DcqVodIngestorConstants.INVALID_BUNDLE_CONTENT));
		}		
		List<Object> contentsOfGrpOfBundle = new ArrayList<Object>();
		Set<Integer> contentIds = new HashSet<Integer>();
		contentIds.add(bundleContent.getContentId());
		contentsOfGrpOfBundle = bundleManager.getContentIdsByBundleContentId(bundleContent.getContentId());
		assetIdList = new ArrayList<>();
		if (!Utilities.isEmpty(contentsOfGrpOfBundle)) {

			for (Object bundleIdsOfGrpBundle : contentsOfGrpOfBundle) {
				List<Object> bundles = bundleManager.getContentIdsByBundleContentId((Integer)bundleIdsOfGrpBundle);

				if (!Utilities.isEmpty(bundles) && !bundles.isEmpty()) {

					for (Object contentId : bundles) {
						List<Object> bundlecontents = bundleManager.getContentIdsByBundleContentId((Integer)contentId);

						if (bundlecontents.isEmpty()) {
							contentIds.add((Integer)contentId);
						}

					}
				}
				contentIds.add((Integer)bundleIdsOfGrpBundle);
			}

		}

		if (bundle.getBundleContentList() != null) {
			for (BundleContentList.ExternalContentId tempExternalContentId : bundle.getBundleContentList().getExternalContentId()) {
				// Fix for defect 8803
				Long tempExternalContentIdlong = Boolean.valueOf(bundle.getBundleContentList().getIsExternalId().value()) ? contentMetadataManager.getContentByExternalId(tempExternalContentId.getValue()).getContentId() : Long.parseLong(tempExternalContentId.getValue());

				if (!contentIds.toString().contains(tempExternalContentId.getValue())) {
					contentIds.add(tempExternalContentIdlong.intValue());

				}
				List<Object> grpBundle = bundleManager.getContentIdsByBundleContentId(tempExternalContentIdlong.intValue());
				if (!Utilities.isEmpty(grpBundle)) {
					for (Object contentId : grpBundle) {
					contentIds.add((Integer)contentId);
					}
				} else {
					log.logMessage("This is not a bundle in bundle aggregation table..");
				}

			}

		}
		assetIdList.addAll(contentIds);
		// Delete all the details from Bundle Aggregation table.
		bundleManager.deleteFromBundleAggregation(bundleContent.getContentId());
		bundleManager.deleteSVODMapping(bundleContent.getContentId());
		bundleManager.deleteTVODRelPlatform(DcqVodIngestorConstants.PKG_BUNDLE + "_%_" + bundle.getBundleId());
		Set<Integer> svodPackages = bundleManager.getAllSVODPackages(bundleContent);

		// 4.3 change -mapping jaxb to db for blacklist device content table
		bundleManager.getBlacklistDeviceContent(bundle,bundleContent,
				config);

		List<Integer> technicalPackageList = new ArrayList<>();
		TechnicalPackageEntity bundleTechnicalPackage = null;
		// Retrieve Technical Package for Bundle
		if(bundle.getProductInfo() != null && "N".equalsIgnoreCase(config.getConstants().getValue(CacheConstants.SDP_IN_PANIC))) {
		 bundleTechnicalPackage = bundleManager.getTechnicalPackages(bundle, bundleContent,
				isBundle ? DcqVodIngestorConstants.BUNDLE : DcqVodIngestorConstants.BUNDLE_GROUP, config);
		technicalPackageList.add(bundleTechnicalPackage.getPackageId());
		}
		bundleAggregationList = new HashSet<Serializable>();
		technicalPackagesForBundle(config, bundle, contentOfAllBundle, bundleContent, svodPackages,
				technicalPackageList, bundleTechnicalPackage);
		// Start: Insert Relation for Group of Bundle in relPlatformTechnical Table
		Set<ContentEntity> allContents = getAllContentOfGroupOfBundle(contentOfAllBundle);
		technicalPackagesForGroupOfBundle(config, bundle, technicalPackageList, bundleTechnicalPackage,
				 allContents);
		// End: Insert Relation for Group of Bundle in relPlatformTechnical
		// Table Map content for SVOD
		technicalPackagesForContent(bundleContent, svodPackages, allContents);
		// Map content for SVOD
		// 4.3 changes
		if (!technicalPackageList.isEmpty() && "N".equalsIgnoreCase(config.getConstants().getValue(CacheConstants.SDP_IN_PANIC))) {
			String externalId = bundle.getBundleId();
			if (bundle.getProductInfo() != null && bundle.getProductInfo().getAdditionalProductInfoList() != null) {
				for (AddProductInfo addProductInfo : bundle.getProductInfo().getAdditionalProductInfoList()
						.getAddProductInfo()) {
					createSolutionOfferrOnSDP(bundle, technicalPackageList, bundleContent,
							(addProductInfo.getCommerceModel() != null
									&& !addProductInfo.getCommerceModel().trim().isEmpty()
											? externalId + "_" + addProductInfo.getCommerceModel() : externalId),
							addProductInfo.getCommerceModel(),
							addProductInfo.getPriceCategoryList().get(0).getPriceCategoryId(), config);
				}
			} else {
				createSolutionOfferrOnSDP(bundle, technicalPackageList, bundleContent, externalId, null,
						bundle.getProductInfo().getPriceCategory(), config);
			}
		}
		bundleManager.createBundle(bundleAggregationList);
		return bundleContent.getContentId();

	}

	/**
	 * This method is used to create technical packages for content
	 * @param bundleContent
	 * @param svodPackages
	 * @param allContents
	 */
	private void technicalPackagesForContent(ContentEntity bundleContent, Set<Integer> svodPackages,
			Set<ContentEntity> allContents) {
		RelPlatformTechnicalEntity relPlatformTechnical;
		for (ContentEntity innerBundleContent : allContents) {
			for (ContentPlatformEntity contentPlatform : innerBundleContent.getContentPlatforms()) {
				if (DcqVodIngestorConstants.YES.equals(Character.toString(contentPlatform.getIsPublished()))) {
					log.logMessage("Bundle Aggregation Processing for Rel Table Started For Group Of Bundle");
					// Map Real Content with Fictitius Content Package Id :Start
					for (Integer packageId : svodPackages) {
						relPlatformTechnical = bundleManager.getRelPlatformTechnical(innerBundleContent,
								contentPlatform.getCpId(), packageId, bundleContent.getContentId());
						bundleAggregationList.add(relPlatformTechnical);
					}
					// Map Real Content with Fictitius Content Package Id :End
				}
			}
		}
	}

	/**
	 * This method is used to create technical packages for group of bundle
	 * @param config
	 * @param bundle
	 * @param technicalPackageList
	 * @param bundleTechnicalPackage
	 * @param allContents
	 * @throws ConfigurationException
	 */
	private void technicalPackagesForGroupOfBundle(Configuration config, Bundle bundle,
			List<Integer> technicalPackageList, TechnicalPackageEntity bundleTechnicalPackage,
			Set<ContentEntity> allContents) throws ConfigurationException {
		RelPlatformTechnicalEntity relPlatformTechnical;
		TechnicalPackageEntity contentTechnicalPackage;
		if (!isBundle && bundle.getProductInfo() != null) {
			for (ContentEntity innerBundleContent : allContents) {
				for (ContentPlatformEntity contentPlatform : innerBundleContent.getContentPlatforms()) {
					if (DcqVodIngestorConstants.YES.equals(Character.toString(contentPlatform.getIsPublished()))) {
						log.logMessage("Bundle Aggregation Processing for Rel Table Started For Group Of Bundle");
						// Map Real Content with Fictitius Content Package Id
						// :Start
						if (bundleTechnicalPackage != null) {
							relPlatformTechnical = bundleManager.getRelPlatformTechnical(innerBundleContent,
									contentPlatform.getCpId(), bundleTechnicalPackage.getPackageId(), null);
							bundleAggregationList.add(relPlatformTechnical);
						}
						// Map Real Content with Fictitius Content Package Id
						// :End
					}
				}
				if (bundle.getProductInfo() != null && "N".equalsIgnoreCase(config.getConstants().getValue(CacheConstants.SDP_IN_PANIC))) {
					//Technical Package for Contents under Bundle(Which is Part of Group of Bundle)
					contentTechnicalPackage = bundleManager.getTechnicalPackages(bundle, innerBundleContent,
							DcqVodIngestorConstants.BUNDLE_VOD, config);
					technicalPackageList.add(contentTechnicalPackage.getPackageId());
				}
			}
		}
	}

	/**
	 * This method is used to create technical packages for bundle
	 * @param config
	 * @param bundle
	 * @param contentOfAllBundle
	 * @param bundleContent
	 * @param svodPackages
	 * @param technicalPackageList
	 * @param bundleTechnicalPackage
	 * @throws ConfigurationException
	 */
	private void technicalPackagesForBundle(Configuration config, Bundle bundle,
			Map<ContentEntity, List<ContentEntity>> contentOfAllBundle, ContentEntity bundleContent,
			Set<Integer> svodPackages, List<Integer> technicalPackageList,
			TechnicalPackageEntity bundleTechnicalPackage) throws ConfigurationException {
		BundleAggregationEntity bundleAggregation;
		RelPlatformTechnicalEntity relPlatformTechnical;
		TechnicalPackageEntity technicalPackage;
		for (Map.Entry<ContentEntity, List<ContentEntity>> entry : contentOfAllBundle.entrySet()) {
			log.logMessage("Bundle Aggregation Processing Started");
			ContentEntity content = entry.getKey();
			BundleContentList.ExternalContentId externalContentId = getBundleContentExternalContent(content, bundle);
			for (ContentPlatformEntity contentPlatform : content.getContentPlatforms()) {
				if (DcqVodIngestorConstants.YES.equals(Character.toString(contentPlatform.getIsPublished()))) {
					bundleAggregation = getBundleAggregation(bundleContent, content, externalContentId, contentPlatform,
							bundleTechnicalPackage != null ? bundleTechnicalPackage : null);
					bundleAggregationList.add(bundleAggregation);

					if (bundle.getProductInfo() != null
							&& "N".equalsIgnoreCase(config.getConstants().getValue(CacheConstants.SDP_IN_PANIC))) {
						// Map Real Content with Fictitius Content Package Id
						// :Start
						relPlatformTechnical = bundleManager.getRelPlatformTechnical(content, contentPlatform.getCpId(),
								bundleTechnicalPackage.getPackageId(), null);
						bundleAggregationList.add(relPlatformTechnical);
						// Map Real Content with Fictitius Content Package Id
						// :End\
					}
					// Map content for SVOD
					for (Integer packageId : svodPackages) {
						relPlatformTechnical = bundleManager.getRelPlatformTechnical(content, contentPlatform.getCpId(),
								packageId, bundleContent.getContentId());
						bundleAggregationList.add(relPlatformTechnical);
					}
					// Map content for SVOD
				}
			}
			if (bundle.getProductInfo() != null
					&& "N".equalsIgnoreCase(config.getConstants().getValue(CacheConstants.SDP_IN_PANIC))) {
				log.logMessage("Bundle Aggregation Processing for Packages Started");
				// Start: Technical Package for Contents under Bundle
				technicalPackage = bundleManager.getTechnicalPackages(bundle, content,
						isBundle ? DcqVodIngestorConstants.BUNDLE_VOD : DcqVodIngestorConstants.BUNDLE, config);
				technicalPackageList.add(technicalPackage.getPackageId());
				// offerList.add(technicalPackage.getExternalID());
				// End: Technical Package for Contents under Bundle
			}
		}
	}

	/**
	 * This method is used to create solution offer
	 * @param bundle
	 * @param technicalPackageList
	 * @param bundleContent
	 * @param externalId
	 * @param commerceModel
	 * @param priceCategoryId
	 * @param config
	 * @throws ConfigurationException
	 * @throws ApplicationException 
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	private void createSolutionOfferrOnSDP(Bundle bundle, List<Integer> technicalPackageList,
			ContentEntity bundleContent, String externalId, String commerceModel, String priceCategoryId,
			Configuration config) throws ConfigurationException, ApplicationException, JsonParseException, JsonMappingException, IOException {
		log.logMessage("In createSolutionOfferrOnSDP");
		Long solutionOfferId = null;

		List<Long> technicalPackages = new ArrayList<>();
		for (Integer technicalPackage : technicalPackageList) {
			technicalPackages.add(technicalPackage.longValue());
		}
		CommercialPackage sdpSolutionOffer = null;
		String socketTimeOut = config.getConstants().getValue(CacheConstants.HTTP_CLIENT_SOCKET_TIMEOUT);
		String connectiontimeOut = config.getConstants().getValue(CacheConstants.HTTP_CLIENT_CONNECTION_TIMEOUT);
		String sdpTenant = config.getConstants().getValue(CacheConstants.SDP_TENANT);
		String sdpUrlConnection = dcqVodIngestorRestUrls.SDP_URL;
		SdpClientManagerImpl sdpClientService = SdpClientManagerImpl
				.getSdpClientService(Long.parseLong(connectiontimeOut), Long.parseLong(socketTimeOut));
		sdpSolutionOffer = sdpClientService.getSdpSolutionOffer(externalId, dcqVodIngestorRestUrls.COMMERCIAL_PACKAGE_DETAILS_V2_URL, sdpTenant,config);
		if (sdpSolutionOffer == null) {
			log.logMessage("Going to Create Solution Offer ");
			solutionOfferId = sdpClientService.createSolutionOfferAndPackage(bundle, technicalPackages, bundleContent,
					sdpUrlConnection,sdpTenant, isBundle, externalId, commerceModel, priceCategoryId);
			if (solutionOfferId == null) {
				log.logMessage("Creation of SDP Solution Offer failed");
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters("Creation of SDP Solution Offer failed"));
			}
		} else {
			solutionOfferId = sdpSolutionOffer.getCommercialPackageId().longValue();
			log.logMessage("Going to Update Solution Offer");
			if (sdpSolutionOffer.getStatusId() == 4) {
				sdpClientService.changeStatusSolutionOfferOnSDP(solutionOfferId, "Active", sdpUrlConnection,sdpTenant);
			}
			boolean offerIdFlag = sdpClientService.updateSolutionOfferAndPackage(bundle, technicalPackages,
					bundleContent, solutionOfferId,sdpUrlConnection, sdpTenant, isBundle, externalId, commerceModel, priceCategoryId);
			if (!offerIdFlag) {
				log.logMessage(
						"Updation of SDP Solution Offer failed for: " + sdpSolutionOffer.getCommercialPackageId());
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters("Updation of SDP Solution Offer failed"));
			}
			
		}

	}

	/**
	 * @since 4.2 Get All content of Bundle
	 * 
	 * @param Bundle
	 *            bundle
	 * @param Content
	 *            bundleContent
	 * @return Map<Content, List<Content>>
	 * 
	 */
	private Set<ContentEntity> getAllContentOfGroupOfBundle(
			Map<ContentEntity, List<ContentEntity>> contentOfAllBundle) {
		Set<ContentEntity> contents = new LinkedHashSet<>();
		for (Map.Entry<ContentEntity, List<ContentEntity>> entry : contentOfAllBundle.entrySet()) {
			contents.addAll(entry.getValue());
		}
		return contents;
	}

	/**
	 * @since 4.2 For Getting External Content Detail of Bundle
	 * 
	 * @param Content
	 *            bundleContent
	 * @param Content
	 *            content
	 * @param BundleContentList
	 *            .ExternalContentId externalContentId
	 * @param ContentPlatform
	 *            contentPlatform
	 * @param Long
	 *            packageId
	 * @return BundleAggregation
	 * 
	 */
	private BundleAggregationEntity getBundleAggregation(ContentEntity bundleContent, ContentEntity content,
			ExternalContentId externalContentId, ContentPlatformEntity contentPlatform,
			TechnicalPackageEntity technicalPackage) {
		BundleAggregationEntity bundleAggregation = new BundleAggregationEntity();
		bundleAggregation.setContent(content);
		BundleAggregationEntityPK bundlePlatformPk = new BundleAggregationEntityPK();
		bundlePlatformPk.setBundleContentId(bundleContent.getContentId());
		bundlePlatformPk.setCpId(contentPlatform.getCpId());
		bundleAggregation.setId(bundlePlatformPk);
		bundleAggregation.setPosition(externalContentId.getPosition());
		bundleAggregation.setTechnicalPackage(technicalPackage);
		return bundleAggregation;
	}

	/**
	 * @since 4.2 For Getting External Content Detail of Bundle
	 * 
	 * @param Content
	 *            content
	 * @param Bundle
	 *            bundle
	 * @return BundleContentList.ExternalContentId
	 * 
	 */
	private ExternalContentId getBundleContentExternalContent(ContentEntity content, Bundle bundle) {
		BundleContentList.ExternalContentId externalContentId = null;
		for (BundleContentList.ExternalContentId tempExternalContentId : bundle.getBundleContentList()
				.getExternalContentId()) {
			if (content.getExternalContentId() != null && content.getContentId() != null
					&& ((Boolean.valueOf(bundle.getBundleContentList().getIsExternalId().value())
							&& content.getExternalContentId().equals(tempExternalContentId.getValue()))
							|| (!Boolean.valueOf(bundle.getBundleContentList().getIsExternalId().value()) && content
									.getContentId().equals(Integer.parseInt(tempExternalContentId.getValue()))))) {
				externalContentId = tempExternalContentId;
				break;
			}
		}
		return externalContentId;
	}

	/**
	 * Checking for group of bundle
	 * 
	 * @param contentOfAllBundle
	 * @return
	 */
	private Boolean isBundle(Map<ContentEntity, List<ContentEntity>> contentOfAllBundle) {
		Boolean tempFlag = null;
		Boolean flag = null;
		for (Map.Entry<ContentEntity, List<ContentEntity>> entry : contentOfAllBundle.entrySet()) {
			tempFlag = !(entry.getValue() != null && (entry.getValue().size() > 0));
			if (flag == null) {
				flag = tempFlag;
			}
			if (!tempFlag.equals(flag)) {
				flag = null;
				break;
			}
		}
		return tempFlag;
	}

	/**
	 * This method is used to get all the contents of the bundle
	 * @param bundle
	 * @return
	 */
	private Map<ContentEntity, List<ContentEntity>> getContentOfAllBundle(Bundle bundle) {

		ContentEntity content = null;
		List<Object> tempContentIds = null;
		Set<Object> contentIds = null;
		Map<ContentEntity, List<ContentEntity>> contentMap = new LinkedHashMap<ContentEntity, List<ContentEntity>>();
		List<ContentEntity> contents = null;
		assetIdList = new ArrayList<>();
		boolean contentTypeFlag = Boolean.valueOf(bundle.getBundleContentList().getIsExternalId().value());
		for (BundleContentList.ExternalContentId tempExternalContentId : bundle.getBundleContentList()
				.getExternalContentId()) {
			content = contentTypeFlag ? contentMetadataManager.getContentByExternalId(tempExternalContentId.getValue())
					: contentMetadataManager.getContentByContentId(Integer.parseInt(tempExternalContentId.getValue()));

			if (content != null) {
				tempContentIds = bundleManager.getContentIdsByBundleContentId(content.getContentId());
				contentIds = new HashSet<Object>(tempContentIds);
				contents = new ArrayList<ContentEntity>();
				for (Object contentId : contentIds) {
					contents.add(contentMetadataManager.getContentByContentId((Integer) contentId));
					assetIdList.add((Integer)contentId);
				}
				contentMap.put(content, contents);
			}
		}

		return contentMap;
	}
}
