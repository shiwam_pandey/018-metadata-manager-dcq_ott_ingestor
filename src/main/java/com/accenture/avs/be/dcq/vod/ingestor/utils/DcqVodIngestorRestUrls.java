package com.accenture.avs.be.dcq.vod.ingestor.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class DcqVodIngestorRestUrls {
	
	@Value(value = "${concurrent-stream-ms-url.getPolicies}")
	public String CONCURRENT_STREAM_MS_URL;
	
	@Value(value = "${configuration-ms-url.getProperties}")
	public String PROPERTIES_CACHE_URL;
	
	@Value(value = "${sdp-url.base}")
	public String SDP_URL;
	
	@Value(value = "${cache-url.base}")
	public String DCQ_VOD_VARNISH_SERVER_URLS;
	
	@Value(value = "${commerce-ms-url.getTrc}")
	public String GET_TRC_COMMERCE_URL;
	
	@Value(value = "${commerce-ms-url.getCommercialPackages}")
	public String COMMERCIAL_PACKAGE_DETAILS_V2_URL;
	
	@Value(value = "${sdp-url.getDeviceTypes}")
	public String SDP_DEVICE_TYPES_URL;
}
