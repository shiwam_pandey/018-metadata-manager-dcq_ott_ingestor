package com.accenture.avs.be.dcq.vod.ingestor.v1.rest.controllers;

import javax.xml.bind.JAXBException;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.avs.be.dcq.vod.ingestor.service.DcqCacheService;
import com.accenture.avs.be.dcq.vod.ingestor.service.VodService;
import com.accenture.avs.be.framework.annotation.AvsRestController;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.cache.Configurator;
import com.accenture.avs.be.framework.configuration.CacheConstants;
import com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.commons.lib.support.CommonConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@AvsRestController
@Api(tags="DCQ Cache Services" ,value = "DCQ Cache API's", description = "API's pertaining to refresh DCQ specific cache")
public class DcqCacheV1Controller {

	@Autowired
private	Configurator configurator;
	
	@Autowired
	private VodService vodService;
	
	@Autowired
	private DcqCacheService dcqCacheService;
	

	/**
	 * @param channelXml
	 * @return
	 * @throws ApplicationException
	 */
	@RequestMapping(value = "/v1/dcq/cache/refresh", method = RequestMethod.GET)
	@ApiOperation(value="refreshDcqCache",notes = "This interface is used to refresh cache which are required by DCQ -Languages,DeviceChannels,Properties,CopyProtections,Policies,TRCs,ES Mappings and Settings"
)
	@ApiResponses({
		@ApiResponse(code=200, message="ACN_200: OK"),
		@ApiResponse(code=500, message="ACN_300: GENERIC ERROR")
	})
	public @ResponseBody
	ResponseEntity<GenericResponse> refreshDcqCache() throws ApplicationException {
		GenericResponse genericResponse = null;
		try {
			Configuration config = configurator.getConfiguration(ThreadContext.get(CommonConstants.LogParamsL4j2.TN));
			genericResponse = dcqCacheService.refreshCache(config);
		} catch (ApplicationException ae) {
				throw ae;
			}catch (JAXBException e) {
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR, new NestedParameters(e.getMessage()));
			}
		catch (Exception e) {
				throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR);
			}
		return ResponseEntity.ok(genericResponse);
	}
	
}
