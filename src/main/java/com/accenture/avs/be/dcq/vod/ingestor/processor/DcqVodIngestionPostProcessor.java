package com.accenture.avs.be.dcq.vod.ingestor.processor;

import java.util.Map;
import java.util.Set;

import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;

/**
 * @author karthik.vadla
 *
 */
public interface DcqVodIngestionPostProcessor { 
	void updateStatusAsFailed(Map<Integer,String> failedIds,String assetType,Configuration config) throws ApplicationException;
	void updatePlaylistStatusAsFailed( Map<Integer, String> failedIdsMap,Configuration config) throws ApplicationException;
	void deletePublishedAssets(Set<Integer> publishedIds,String assetType,Configuration config) throws ApplicationException;
	void deletePublishedChannel( Set<Integer> publishedIds,Configuration config) throws ApplicationException;
	
}
