package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author karthik.vadla
 *
 */
public class EsContentDoc implements Serializable {

	private static final long serialVersionUID = 1L;
	private RequestBean content;
	private SuggestDTO suggest;
	private PopularityDTO popularity;
	private String[] searchMetadata;

	public String[] getSearchMetadata() {
		return searchMetadata;
	}

	public void setSearchMetadata(String[] searchMetadata) {
		this.searchMetadata = searchMetadata;
	}

	public RequestBean getContent() {
		return content;
	}

	public void setContent(RequestBean content) {
		this.content = content;
	}

	public SuggestDTO getSuggest() {
		return suggest;
	}

	public void setSuggest(SuggestDTO suggest) {
		this.suggest = suggest;
	}

	public PopularityDTO getPopularity() {
		return popularity;
	}

	public void setPopularity(PopularityDTO popularity) {
		this.popularity = popularity;
	}

	public static class SuggestDTO implements Serializable {

		private static final long serialVersionUID = 1L;

		private String[] input;
		private Map<String, String> contexts;

		public String[] getInput() {
			return input;
		}

		public void setInput(String[] input) {
			this.input = input;
		}

		public Map<String, String> getContexts() {
			return contexts;
		}

		public void setContexts(Map<String, String> contexts) {
			this.contexts = contexts;
		}

	}

	public static class PopularityDTO implements Serializable {

		private static final long serialVersionUID = 1L;

		private BigDecimal popularityKPI;
		private BigDecimal popularityBoost;

		public BigDecimal getPopularityKPI() {
			return popularityKPI;
		}

		public void setPopularityKPI(BigDecimal popularityKpi) {
			this.popularityKPI = popularityKpi;
		}

		public BigDecimal getPopularityBoost() {
			return popularityBoost;
		}

		public void setPopularityBoost(BigDecimal popularityBoost) {
			this.popularityBoost = popularityBoost;
		}

	}

	public static class RequestBean implements Serializable {

		private static final long serialVersionUID = 1L;

		private Long contentId;
		private String platformName;

		private MetadataDTO metadata;
		private ContainersDTO containers;
		private List<ChildrenDTO> children;
		private List<PlatformVariantsDTO> platformVariants;
		private List<PropertiesDTO> properties;

		public String getPlatformName() {
			return platformName;
		}

		public void setPlatformName(String platformName) {
			this.platformName = platformName;
		}

		public MetadataDTO getMetadata() {
			return metadata;
		}

		public void setMetadata(MetadataDTO metadata) {
			this.metadata = metadata;
		}

		public Long getContentId() {
			return contentId;
		}

		public void setContentId(Long contentId) {
			this.contentId = contentId;
		}

		public List<PropertiesDTO> getProperties() {
			return properties;
		}

		public void setProperties(List<PropertiesDTO> properties) {
			this.properties = properties;
		}

		public static class MetadataDTO implements Serializable {

			private static final long serialVersionUID = 1L;

			/* Start - AVS-4049 */
			private String objectType;
			private String objectSubtype;
			private Long contentId;
			private String externalId;
			private String title;
			private String episodeTitle;
			private Long duration;
			private Long contractStartDate;
			private Long contractEndDate;
			private Long startTime;
			private Long endTime;
			private Float starRating;
			private String[] pcExtendedRatings;
			private String pcLevelVod;
			private String pcLevel;
			private Boolean isParent;
			private String pictureUrl;
			private Long primaryCategoryId;
			private String[] blacklistDeviceTypes;
			private String[] streamPolicies; // AVS-13658

			// AVS-24272 start
			private Boolean isCopyProtected;
			private List<CopyProtectionDTO> copyProtections;

			public Long getStartTime() {
				return startTime;
			}

			public void setStartTime(Long startTime) {
				this.startTime = startTime;
			}

			public Long getEndTime() {
				return endTime;
			}

			public void setEndTime(Long endTime) {
				this.endTime = endTime;
			}

			public String getPcLevel() {
				return pcLevel;
			}

			public void setPcLevel(String pcLevel) {
				this.pcLevel = pcLevel;
			}

			// AVS-27555
			private String pcVodLabel;

			public String getPcVodLabel() {
				return pcVodLabel;
			}

			public void setPcVodLabel(String pcVodLabel) {
				this.pcVodLabel = pcVodLabel;
			}

			public Boolean getIsCopyProtected() {
				return isCopyProtected;
			}

			public void setIsCopyProtected(Boolean isCopyProtected) {
				this.isCopyProtected = isCopyProtected;
			}

			public List<CopyProtectionDTO> getCopyProtections() {
				return copyProtections;
			}

			public void setCopyProtections(List<CopyProtectionDTO> copyProtections) {
				this.copyProtections = copyProtections;
			}

			/* End - AVS-4049 */

			/*
			 * additional data inclusion to elastic search STAR CR 14890 - AVS 5833 Start
			 */

			public String[] getStreamPolicies() {
				return  streamPolicies;
			}

			public void setStreamPolicies(String[] streamPolicies) {
				this.streamPolicies = streamPolicies;
			}

			private String additionalData;
			private List<AdvertisingInfoListDTO> advertisingInfoList; // AVS-13771

			public List<AdvertisingInfoListDTO> getAdvertisingInfoList() {
				return advertisingInfoList;
			}

			public void setAdvertisingInfoList(List<AdvertisingInfoListDTO> advertisingInfoList) {
				this.advertisingInfoList = advertisingInfoList;
			}

			public String getAdditionalData() {
				return additionalData;
			}

			public void setAdditionalData(String additionalData) {
				this.additionalData = additionalData;
			}

			/* STAR CR 14890 - AVS 5833 End */

			/* Start - Story 2660 in 6.1 */
			private Boolean isSkipJumpEnabled;
			/* End - Story 2660 in 6.1 */

			/* Start - Story 370 in 6.1 */
			private Boolean isTrickPlayEnabled;
			/* End - Story 370 in 6.1 */

			private String longDescription;
			private Long episodeNumber;
			private Long season;
			private Long seriesId;
			private String metadataLanguage;
			private String[] country;
			private String year;
			private String decade;
			private String[] genres;
			private String[] actors;
			private String[] directors;
			private String[] anchors;
			private String[] authors;
			private String filter;
			private Boolean isEncrypted;
			private Boolean isLatest;
			private Boolean isOnAir;
			private Boolean isPopularEpisode;
			private Boolean isGeoBlocked;
			private Boolean isSurroundSound;
			private Boolean isADVAllowed;
			private String[] searchKeywords;
			private String titleBrief;
			private String programReferenceName;
			private Long lastBroadcastDate;
			private Long broadcastChannelId;
			private String broadcastChannelName;
			private Long originalAirDate;
			private String[] availableAlso;
			private String contentProvider;
			private String advTags;
			private String ratingType;
			private String associatedWebSiteUrl;
			private String infoPage;
			private String shortDescription;
			private List<LanguagesDTO> availableLanguages;
			private List<ChaptersDTO> chapters;
			private List<ScenesDTO> scenes;
			private Map<String, String> emfAttributes;
			private Object extendedMetadata;
			private String programCategory;
			private boolean comingSoon;
			private boolean leavingSoon;
			private boolean recentlyAdded;
			/* AVS-13824 Start */
			private Boolean isNotAvailableOutOfHome;

			public Boolean getIsNotAvailableOutOfHome() {
				return isNotAvailableOutOfHome;
			}

			public void setIsNotAvailableOutOfHome(Boolean isNotAvailableOutOfHome) {
				this.isNotAvailableOutOfHome = isNotAvailableOutOfHome;
			}
			/* AVS-13824 End */

			public boolean isComingSoon() {
				return comingSoon;
			}

			public void setComingSoon(boolean comingSoon) {
				this.comingSoon = comingSoon;
			}

			public boolean isLeavingSoon() {
				return leavingSoon;
			}

			public void setLeavingSoon(boolean leavingSoon) {
				this.leavingSoon = leavingSoon;
			}
			public boolean isRecentlyAdded() {
				return recentlyAdded;
			}

			public void setRecentlyAdded(boolean recentlyAdded) {
				this.recentlyAdded = recentlyAdded;
			}


			public Boolean getIsTrickPlayEnabled() {
				return isTrickPlayEnabled;
			}

			public void setIsTrickPlayEnabled(Boolean isTrickPlayEnabled) {
				this.isTrickPlayEnabled = isTrickPlayEnabled;
			}

			/* Start - AVS-4049 */
			public String getObjectType() {
				return objectType;
			}

			public void setObjectType(String objectType) {
				this.objectType = objectType;
			}

			public String getObjectSubtype() {
				return objectSubtype;
			}

			public void setObjectSubtype(String objectSubtype) {
				this.objectSubtype = objectSubtype;
			}

			public Long getContentId() {
				return contentId;
			}

			public void setContentId(Long contentId) {
				this.contentId = contentId;
			}

			public String getExternalId() {
				return externalId;
			}

			public void setExternalId(String externalId) {
				this.externalId = externalId;
			}

			/* Start - Story 2660 in 6.1 */
			public Boolean getIsSkipJumpEnabled() {
				return isSkipJumpEnabled;
			}

			public void setIsSkipJumpEnabled(Boolean isSkipJumpEnabled) {
				this.isSkipJumpEnabled = isSkipJumpEnabled;
			}
			/* End - Story 2660 in 6.1 */

			public String getTitle() {
				return title;
			}

			public void setTitle(String title) {
				this.title = title;
			}

			public String getEpisodeTitle() {
				return episodeTitle;
			}

			public void setEpisodeTitle(String episodeTitle) {
				this.episodeTitle = episodeTitle;
			}

			public Long getDuration() {
				return duration;
			}

			public void setDuration(Long duration) {
				this.duration = duration;
			}

			public Long getContractStartDate() {
				return contractStartDate;
			}

			public void setContractStartDate(Long contractStartDate) {
				this.contractStartDate = contractStartDate;
			}

			public Long getContractEndDate() {
				return contractEndDate;
			}

			public void setContractEndDate(Long contractEndDate) {
				this.contractEndDate = contractEndDate;
			}

			public Float getStarRating() {
				return starRating;
			}

			public void setStarRating(Float starRating) {
				this.starRating = starRating;
			}

			public String[] getPcExtendedRatings() {
					return pcExtendedRatings;
			}

			public void setPcExtendedRatings(String[] pcExtendedRatings) {
				this.pcExtendedRatings = pcExtendedRatings;

			}

			public String getPcLevelVod() {
				return pcLevelVod;
			}

			public void setPcLevelVod(String pcLevelVod) {
				this.pcLevelVod = pcLevelVod;
			}

			public Boolean getIsParent() {
				return isParent;
			}

			public void setIsParent(Boolean isParent) {
				this.isParent = isParent;
			}

			public String getPictureUrl() {
				return pictureUrl;
			}

			public void setPictureUrl(String pictureUrl) {
				this.pictureUrl = pictureUrl;
			}

			public Long getPrimaryCategoryId() {
				return primaryCategoryId;
			}

			public void setPrimaryCategoryId(Long primaryCategoryId) {
				this.primaryCategoryId = primaryCategoryId;
			}

			public String[] getBlacklistDeviceTypes() {
				return blacklistDeviceTypes;
			}

			public void setBlacklistDeviceTypes(String[] blacklistDeviceTypes) {
				this.blacklistDeviceTypes = blacklistDeviceTypes;
			}
			/* End - AVS-4049 */

			public String getShortDescription() {
				return shortDescription;
			}

			public void setShortDescription(String shortDescription) {
				this.shortDescription = shortDescription;
			}

			public String getLongDescription() {
				return longDescription;
			}

			public void setLongDescription(String longDescription) {
				this.longDescription = longDescription;
			}

			public Long getEpisodeNumber() {
				return episodeNumber;
			}

			public void setEpisodeNumber(Long episodeNumber) {
				this.episodeNumber = episodeNumber;
			}

			public Long getSeason() {
				return season;
			}

			public void setSeason(Long season) {
				this.season = season;
			}

			public Long getSeriesId() {
				return seriesId;
			}

			public void setSeriesId(Long seriesId) {
				this.seriesId = seriesId;
			}

			public String getMetadataLanguage() {
				return metadataLanguage;
			}

			public void setMetadataLanguage(String metadataLanguage) {
				this.metadataLanguage = metadataLanguage;
			}

			public String[] getCountry() {
				return country;
			}

			public void setCountry(String[] country) {
				this.country = country;
			}

			public String getYear() {
				return year;
			}

			public void setYear(String year) {
				this.year = year;
			}

			public String getDecade() {
				return decade;
			}

			public void setDecade(String decade) {
				this.decade = decade;
			}

			public String[] getGenres() {
				return genres;
			}

			public void setGenres(String[] genres) {
				this.genres = genres;
			}

			public String[] getActors() {
				return actors;
			}

			public void setActors(String[] actors) {
				this.actors = actors;
			}

			public String[] getDirectors() {
				return directors;
			}

			public void setDirectors(String[] directors) {
				this.directors =  directors;
			}

			public String[] getAnchors() {
				return anchors;
			}

			public void setAnchors(String[] anchors) {
				this.anchors = anchors;
			}

			public String[] getAuthors() {
				return authors;
			}

			public void setAuthors(String[] authors) {
				this.authors = authors;
			}

			public String getFilter() {
				return filter;
			}

			public void setFilter(String filter) {
				this.filter = filter;
			}

			public Boolean getIsEncrypted() {
				return isEncrypted;
			}

			public void setIsEncrypted(Boolean isEncrypted) {
				this.isEncrypted = isEncrypted;
			}

			public Boolean getIsLatest() {
				return isLatest;
			}

			public void setIsLatest(Boolean isLatest) {
				this.isLatest = isLatest;
			}

			public Boolean getIsOnAir() {
				return isOnAir;
			}

			public void setIsOnAir(Boolean isOnAir) {
				this.isOnAir = isOnAir;
			}

			public Boolean getIsPopularEpisode() {
				return isPopularEpisode;
			}

			public void setIsPopularEpisode(Boolean isPopularEpisode) {
				this.isPopularEpisode = isPopularEpisode;
			}

			public Boolean getIsGeoBlocked() {
				return isGeoBlocked;
			}

			public void setIsGeoBlocked(Boolean isGeoBlocked) {
				this.isGeoBlocked = isGeoBlocked;
			}

			public Boolean getIsSurroundSound() {
				return isSurroundSound;
			}

			public void setIsSurroundSound(Boolean isSurroundSound) {
				this.isSurroundSound = isSurroundSound;
			}

			public Boolean getIsADVAllowed() {
				return isADVAllowed;
			}

			public void setIsADVAllowed(Boolean isADVAllowed) {
				this.isADVAllowed = isADVAllowed;
			}

			public String[] getSearchKeywords() {
				return searchKeywords;
			}

			public void setSearchKeywords(String[] searchKeywords) {
				this.searchKeywords = searchKeywords;
			}

			public String getTitleBrief() {
				return titleBrief;
			}

			public void setTitleBrief(String titleBrief) {
				this.titleBrief = titleBrief;
			}

			public String getProgramReferenceName() {
				return programReferenceName;
			}

			public void setProgramReferenceName(String programReferenceName) {
				this.programReferenceName = programReferenceName;
			}

			public Long getLastBroadcastDate() {
				return lastBroadcastDate;
			}

			public void setLastBroadcastDate(Long lastBroadcastDate) {
				this.lastBroadcastDate = lastBroadcastDate;
			}

			public Long getBroadcastChannelId() {
				return broadcastChannelId;
			}

			public void setBroadcastChannelId(Long broadcastChannelId) {
				this.broadcastChannelId = broadcastChannelId;
			}

			public String getBroadcastChannelName() {
				return broadcastChannelName;
			}

			public void setBroadcastChannelName(String broadcastChannelName) {
				this.broadcastChannelName = broadcastChannelName;
			}

			public Long getOriginalAirDate() {
				return originalAirDate;
			}

			public void setOriginalAirDate(Long originalAirDate) {
				this.originalAirDate = originalAirDate;
			}

			public String[] getAvailableAlso() {
				return availableAlso;
			}

			public void setAvailableAlso(String[] availableAlso) {
				this.availableAlso =  availableAlso;
			}

			public String getContentProvider() {
				return contentProvider;
			}

			public void setContentProvider(String contentProvider) {
				this.contentProvider = contentProvider;
			}

			public String getAdvTags() {
				return advTags;
			}

			public void setAdvTags(String advTags) {
				this.advTags = advTags;
			}

			public String getRatingType() {
				return ratingType;
			}

			public void setRatingType(String ratingType) {
				this.ratingType = ratingType;
			}

			public List<LanguagesDTO> getAvailableLanguages() {
				return availableLanguages;
			}

			public void setAvailableLanguages(List<LanguagesDTO> availableLanguages) {
				this.availableLanguages = availableLanguages;
			}

			public List<ChaptersDTO> getChapters() {
				return chapters;
			}

			public void setChapters(List<ChaptersDTO> chapters) {
				this.chapters = chapters;
			}

			public List<ScenesDTO> getScenes() {
				return scenes;
			}

			public void setScenes(List<ScenesDTO> scenes) {
				this.scenes = scenes;
			}

			public Map<String, String> getEmfAttributes() {
				return emfAttributes;
			}

			public void setEmfAttributes(Map<String, String> emfAttributes) {
				this.emfAttributes = emfAttributes;
			}

			public Object getExtendedMetadata() {
				return extendedMetadata;
			}

			public void setExtendedMetadata(Object extendedMetadata) {
				this.extendedMetadata = extendedMetadata;
			}

			public static class LanguagesDTO implements Serializable {

				private static final long serialVersionUID = 1L;

				private String languageCode;

				public String getLanguageCode() {
					return languageCode;
				}

				public void setLanguageCode(String languageCode) {
					this.languageCode = languageCode;
				}

				public String getLanguageName() {
					return languageName;
				}

				public void setLanguageName(String languageName) {
					this.languageName = languageName;
				}

				private String languageName;

			}

			public static class ChaptersDTO implements Serializable {

				private static final long serialVersionUID = 1L;

				private Long chapterId;
				private Long startTime;
				private Long endTime;
				private String briefTitle;
				private Long orderId;
				private String metadataLanguage;

				public Long getChapterId() {
					return chapterId;
				}

				public void setChapterId(Long chapterId) {
					this.chapterId = chapterId;
				}

				public Long getStartTime() {
					return startTime;
				}

				public void setStartTime(Long startTime) {
					this.startTime = startTime;
				}

				public Long getEndTime() {
					return endTime;
				}

				public void setEndTime(Long endTime) {
					this.endTime = endTime;
				}

				public String getBriefTitle() {
					return briefTitle;
				}

				public void setBriefTitle(String briefTitle) {
					this.briefTitle = briefTitle;
				}

				public Long getOrderId() {
					return orderId;
				}

				public void setOrderId(Long orderId) {
					this.orderId = orderId;
				}

				public String getMetadataLanguage() {
					return metadataLanguage;
				}

				public void setMetadataLanguage(String metadataLanguage) {
					this.metadataLanguage = metadataLanguage;
				}

			}

			public static class ScenesDTO implements Serializable {

				private static final long serialVersionUID = 1L;

				private Long sceneId;
				private Long startTime;
				private Long endTime;
				private String title;
				private String briefTitle;
				private String keywords;
				private String genre;
				private Long orderId;
				private String advTag;
				private String event;
				private String action;
				private String location;
				private String extendedLocation;
				private String[] characters;
				private String[] actors;
				private String company;
				private String audio;
				private String[] musicians;
				private String metadataLanguage;
				private Object extendedMetadata;
				private Map<String, String> emfAttributes;

				public Long getSceneId() {
					return sceneId;
				}

				public void setSceneId(Long sceneId) {
					this.sceneId = sceneId;
				}

				public Long getStartTime() {
					return startTime;
				}

				public void setStartTime(Long startTime) {
					this.startTime = startTime;
				}

				public Long getEndTime() {
					return endTime;
				}

				public void setEndTime(Long endTime) {
					this.endTime = endTime;
				}

				public String getTitle() {
					return title;
				}

				public void setTitle(String title) {
					this.title = title;
				}

				public String getBriefTitle() {
					return briefTitle;
				}

				public void setBriefTitle(String briefTitle) {
					this.briefTitle = briefTitle;
				}

				public String getKeywords() {
					return keywords;
				}

				public void setKeywords(String keywords) {
					this.keywords = keywords;
				}

				public String getGenre() {
					return genre;
				}

				public void setGenre(String genre) {
					this.genre = genre;
				}

				public Long getOrderId() {
					return orderId;
				}

				public void setOrderId(Long orderId) {
					this.orderId = orderId;
				}

				public String getAdvTag() {
					return advTag;
				}

				public void setAdvTag(String advTag) {
					this.advTag = advTag;
				}

				public String getEvent() {
					return event;
				}

				public void setEvent(String event) {
					this.event = event;
				}

				public String getAction() {
					return action;
				}

				public void setAction(String action) {
					this.action = action;
				}

				public String getLocation() {
					return location;
				}

				public void setLocation(String location) {
					this.location = location;
				}

				public String getExtendedLocation() {
					return extendedLocation;
				}

				public void setExtendedLocation(String extendedLocation) {
					this.extendedLocation = extendedLocation;
				}

				public String[] getCharacters() {
					return characters;
				}

				public void setCharacters(String[] characters) {
					this.characters = characters;
				}

				public String[] getActors() {
					return actors;
				}

				public void setActors(String[] actors) {
					this.actors = actors;
				}

				public String getCompany() {
					return company;
				}

				public void setCompany(String company) {
					this.company = company;
				}

				public String getAudio() {
					return audio;
				}

				public void setAudio(String audio) {
					this.audio = audio;
				}

				public String[] getMusicians() {
					return musicians;
				}

				public void setMusicians(String[] musicians) {
					this.musicians = musicians;
				}

				public String getMetadataLanguage() {
					return metadataLanguage;
				}

				public void setMetadataLanguage(String metadataLanguage) {
					this.metadataLanguage = metadataLanguage;
				}

				public Map<String, String> getEmfAttributes() {
					return emfAttributes;
				}

				public void setEmfAttributes(Map<String, String> emfAttributes) {
					this.emfAttributes = emfAttributes;
				}

				public Object getExtendedMetadata() {
					return extendedMetadata;
				}

				public void setExtendedMetadata(Object extendedMetadata) {
					this.extendedMetadata = extendedMetadata;
				}

			}

			public String getAssociatedWebSiteUrl() {
				return associatedWebSiteUrl;
			}

			public void setAssociatedWebSiteUrl(String associatedWebSiteUrl) {
				this.associatedWebSiteUrl = associatedWebSiteUrl;
			}

			public String getInfoPage() {
				return infoPage;
			}

			public void setInfoPage(String infoPage) {
				this.infoPage = infoPage;
			}

			public String getProgramCategory() {
				return programCategory;
			}

			public void setProgramCategory(String programCategory) {
				this.programCategory = programCategory;
			}

		}

		public ContainersDTO getContainers() {
			return containers;
		}

		public void setContainers(ContainersDTO containers) {
			this.containers = containers;
		}

		public static class ContainersDTO implements Serializable {

			private static final long serialVersionUID = 1L;

			private List<CategoriesDTO> categories;
			private List<BundlesDTO> bundles;
			private List<ParentsDTO> parents;

			public List<ParentsDTO> getParents() {
				return parents;
			}

			public void setParents(List<ParentsDTO> parents) {
				this.parents = parents;
			}

			public List<BundlesDTO> getBundles() {
				return bundles;
			}

			public void setBundles(List<BundlesDTO> bundles) {
				this.bundles = bundles;
			}

			public List<CategoriesDTO> getCategories() {
				return categories;
			}

			public void setCategories(List<CategoriesDTO> categories) {
				this.categories = categories;
			}

			public static class CategoriesDTO implements Serializable {

				private static final long serialVersionUID = 1L;

				private Integer[] categoryPathIds;
				private String[] externalPathIds;
				private Long categoryId;
				private Long startDate;
				private Long endDate;
				private Long orderId;
				private String categoryName;
				private Boolean isPrimary;

				public Integer[] getCategoryPathIds() {
					return categoryPathIds;
				}

				public void setCategoryPathIds(Integer[] categoryPathIds) {
					this.categoryPathIds = categoryPathIds;
				}

				public String[] getExternalPathIds() {
					return externalPathIds;
				}

				public void setExternalPathIds(String[] externalPathIds) {
					this.externalPathIds = externalPathIds;
				}

				public Long getCategoryId() {
					return categoryId;
				}

				public void setCategoryId(Long categoryId) {
					this.categoryId = categoryId;
				}

				public Long getStartDate() {
					return startDate;
				}

				public void setStartDate(Long startDate) {
					this.startDate = startDate;
				}

				public Long getEndDate() {
					return endDate;
				}

				public void setEndDate(Long endDate) {
					this.endDate = endDate;
				}

				public Long getOrderId() {
					return orderId;
				}

				public void setOrderId(Long orderId) {
					this.orderId = orderId;
				}

				public String getCategoryName() {
					return categoryName;
				}

				public void setCategoryName(String categoryName) {
					this.categoryName = categoryName;
				}

				public Boolean getIsPrimary() {
					return isPrimary;
				}

				public void setIsPrimary(Boolean isPrimary) {
					this.isPrimary = isPrimary;
				}

			}

			public static class BundlesDTO implements Serializable {

				private static final long serialVersionUID = 1L;

				private Long bundleId;
				private String bundleType;
				private String bundleSubtype;
				private Boolean isParent;
				private Long orderId;

				public Long getBundleId() {
					return bundleId;
				}

				public void setBundleId(Long bundleId) {
					this.bundleId = bundleId;
				}

				public String getBundleType() {
					return bundleType;
				}

				public void setBundleType(String bundleType) {
					this.bundleType = bundleType;
				}

				public String getBundleSubtype() {
					return bundleSubtype;
				}

				public void setBundleSubtype(String bundleSubtype) {
					this.bundleSubtype = bundleSubtype;
				}

				public Boolean getIsParent() {
					return isParent;
				}

				public void setIsParent(Boolean isParent) {
					this.isParent = isParent;
				}

				public Long getOrderId() {
					return orderId;
				}

				public void setOrderId(Long orderId) {
					this.orderId = orderId;
				}
			}

			public static class ParentsDTO implements Serializable {

				private static final long serialVersionUID = 1L;

				private Long contentId;
				private String objectType;
				private String objectSubtype;
				private Long orderId;

				public Long getContentId() {
					return contentId;
				}

				public void setContentId(Long contentId) {
					this.contentId = contentId;
				}

				public String getObjectType() {
					return objectType;
				}

				public void setObjectType(String objectType) {
					this.objectType = objectType;
				}

				public String getObjectSubtype() {
					return objectSubtype;
				}

				public void setObjectSubtype(String objectSubtype) {
					this.objectSubtype = objectSubtype;
				}

				public Long getOrderId() {
					return orderId;
				}

				public void setOrderId(Long orderId) {
					this.orderId = orderId;
				}
			}

		}

		public List<ChildrenDTO> getChildren() {
			return children;
		}

		public void setChildren(List<ChildrenDTO> children) {
			this.children = children;
		}

		public static class ChildrenDTO implements Serializable {

			private static final long serialVersionUID = 1L;

			private String type;
			private List<VaraintsDTO> variants;
			private Long contentId;
			private Long orderId;

			public String getType() {
				return type;
			}

			public void setType(String type) {
				this.type = type;
			}

			public List<VaraintsDTO> getVariants() {
				return variants;
			}

			public void setVariants(List<VaraintsDTO> variants) {
				this.variants = variants;
			}

			public Long getContentId() {
				return contentId;
			}

			public void setContentId(Long contentId) {
				this.contentId = contentId;
			}

			public Long getOrderId() {
				return orderId;
			}

			public void setOrderId(Long orderId) {
				this.orderId = orderId;
			}

			public static class VaraintsDTO implements Serializable {

				private static final long serialVersionUID = 1L;

				private Long cpId;
				private String pictureUrl;
				private String resolutionType;
				private String trailerUrl;
				private String videoUrl;

				public String getTrailerUrl() {
					return trailerUrl;
				}

				public void setTrailerUrl(String trailerUrl) {
					this.trailerUrl = trailerUrl;
				}

				public String getVideoUrl() {
					return videoUrl;
				}

				public void setVideoUrl(String videoUrl) {
					this.videoUrl = videoUrl;
				}

				public Long getCpId() {
					return cpId;
				}

				public void setCpId(Long cpId) {
					this.cpId = cpId;
				}

				public String getPictureUrl() {
					return pictureUrl;
				}

				public void setPictureUrl(String pictureUrl) {
					this.pictureUrl = pictureUrl;
				}

				public String getResolutionType() {
					return resolutionType;
				}

				public void setResolutionType(String resolutionType) {
					this.resolutionType = resolutionType;
				}

			}

		}

		public List<PlatformVariantsDTO> getPlatformVariants() {
			return platformVariants;
		}

		public void setPlatformVariants(List<PlatformVariantsDTO> platformVariants) {
			this.platformVariants = platformVariants;
		}

		public static class PlatformVariantsDTO implements Serializable {

			private static final long serialVersionUID = 1L;

			private Long cpId;
			private String videoType;
			private Long downloadMaxNumber;
			private Long downloadMaxDays;
			private Long downloadHoursFromFirstPlay;
			private Boolean hasTrailer;
			private List<SubTitlesLanguagesDTO> subtitlesLanguages;
			private String pictureUrl;
			private List<AudioLanguagesDTO> audioLanguages;
			private List<TechnicalPackageDTO> technicalPackages;
			private Object extendedMetadata;
			private String trailerUrl;
			private String videoUrl;
			private String drmInfo;
			private String streamingType;

			public String getStreamingType() {
				return streamingType;
			}

			public void setStreamingType(String streamingType) {
				this.streamingType = streamingType;
			}

			public String getTrailerUrl() {
				return trailerUrl;
			}

			public void setTrailerUrl(String trailerUrl) {
				this.trailerUrl = trailerUrl;
			}

			public String getVideoUrl() {
				return videoUrl;
			}

			public void setVideoUrl(String videoUrl) {
				this.videoUrl = videoUrl;
			}

			public String getDrmInfo() {
				return drmInfo;
			}

			public void setDrmInfo(String drmInfo) {
				this.drmInfo = drmInfo;
			}

			public Long getCpId() {
				return cpId;
			}

			public void setCpId(Long cpId) {
				this.cpId = cpId;
			}

			public String getVideoType() {
				return videoType;
			}

			public void setVideoType(String videoType) {
				this.videoType = videoType;
			}

			public Long getDownloadMaxNumber() {
				return downloadMaxNumber;
			}

			public void setDownloadMaxNumber(Long downloadMaxNumber) {
				this.downloadMaxNumber = downloadMaxNumber;
			}

			public Long getDownloadMaxDays() {
				return downloadMaxDays;
			}

			public void setDownloadMaxDays(Long downloadMaxDays) {
				this.downloadMaxDays = downloadMaxDays;
			}

			public Long getDownloadHoursFromFirstPlay() {
				return downloadHoursFromFirstPlay;
			}

			public void setDownloadHoursFromFirstPlay(Long downloadHoursFromFirstPlay) {
				this.downloadHoursFromFirstPlay = downloadHoursFromFirstPlay;
			}

			public Boolean getHasTrailer() {
				return hasTrailer;
			}

			public void setHasTrailer(Boolean hasTrailer) {
				this.hasTrailer = hasTrailer;
			}

			public List<SubTitlesLanguagesDTO> getSubtitlesLanguages() {
				return subtitlesLanguages;
			}

			public void setSubtitlesLanguages(List<SubTitlesLanguagesDTO> subtitlesLanguages) {
				this.subtitlesLanguages = subtitlesLanguages;
			}

			public String getPictureUrl() {
				return pictureUrl;
			}

			public void setPictureUrl(String pictureUrl) {
				this.pictureUrl = pictureUrl;
			}

			public List<AudioLanguagesDTO> getAudioLanguages() {
				return audioLanguages;
			}

			public void setAudioLanguages(List<AudioLanguagesDTO> audioLanguages) {
				this.audioLanguages = audioLanguages;
			}

			public List<TechnicalPackageDTO> getTechnicalPackages() {
				return technicalPackages;
			}

			public void setTechnicalPackages(List<TechnicalPackageDTO> technicalPackages) {
				this.technicalPackages = technicalPackages;
			}

			public Object getExtendedMetadata() {
				return extendedMetadata;
			}

			public void setExtendedMetadata(Object extendedMetadata) {
				this.extendedMetadata = extendedMetadata;
			}

			public static class SubTitlesLanguagesDTO implements Serializable {

				private static final long serialVersionUID = 1L;
				private String subtitleId;
				private String subtitleLanguageName;

				public String getSubtitleId() {
					return subtitleId;
				}

				public void setSubtitleId(String subtitleId) {
					this.subtitleId = subtitleId;
				}

				public String getSubtitleLanguageName() {
					return subtitleLanguageName;
				}

				public void setSubtitleLanguageName(String subtitleLanguageName) {
					this.subtitleLanguageName = subtitleLanguageName;
				}

			}

			public static class AudioLanguagesDTO implements Serializable {

				private static final long serialVersionUID = 1L;
				private String audioId;
				private String audioLanguageName;
				private Boolean isPreferred;

				public String getAudioId() {
					return audioId;
				}

				public void setAudioId(String audioId) {
					this.audioId = audioId;
				}

				public String getAudioLanguageName() {
					return audioLanguageName;
				}

				public void setAudioLanguageName(String audioLanguageName) {
					this.audioLanguageName = audioLanguageName;
				}

				public Boolean getIsPreferred() {
					return isPreferred;
				}

				public void setIsPreferred(Boolean isPreferred) {
					this.isPreferred = isPreferred;
				}
			}

			public static class TechnicalPackageDTO implements Serializable {

				private static final long serialVersionUID = 1L;
				private Long packageId;
				private String packageName;
				private String packageType;

				public Long getPackageId() {
					return packageId;
				}

				public void setPackageId(Long packageId) {
					this.packageId = packageId;
				}

				public String getPackageName() {
					return packageName;
				}

				public void setPackageName(String packageName) {
					this.packageName = packageName;
				}

				public String getPackageType() {
					return packageType;
				}

				public void setPackageType(String packageType) {
					this.packageType = packageType;
				}
			}

		}

		public static class PropertiesDTO implements Serializable {
			private static final long serialVersionUID = 1L;
			private String propertyName;
			private Object extendedMetadata;

			public String getPropertyName() {
				return propertyName;
			}

			public void setPropertyName(String propertyName) {
				this.propertyName = propertyName;
			}

			public Object getExtendedMetadata() {
				return extendedMetadata;
			}

			public void setExtendedMetadata(Object extendedMetadata) {
				this.extendedMetadata = extendedMetadata;
			}
		}
	}

	public static class AdvertisingInfoListDTO implements Serializable {

		private static final long serialVersionUID = 1L;

		private String networkID;
		private String advertisingContentId;
		private List<CuePointListDTO> cuePointList;

		public String getNetworkID() {
			return networkID;
		}

		public void setNetworkID(String networkID) {
			this.networkID = networkID;
		}

		public String getAdvertisingContentId() {
			return advertisingContentId;
		}

		public void setAdvertisingContentId(String advertisingContentId) {
			this.advertisingContentId = advertisingContentId;
		}

		public List<CuePointListDTO> getCuePointList() {
			return cuePointList;
		}

		public void setCuePointList(List<CuePointListDTO> cuePointList) {
			this.cuePointList = cuePointList;
		}

	}

	public static class CuePointListDTO implements Serializable {

		private static final long serialVersionUID = 1L;

		private Double contentTimePosition;
		private Double contentEndTimePosition;
		private String timePositionClass;
		private String adFormat;
		private Integer timeToNextAdUnit;
		private String customSlotId;
		private Integer sequence;

		public Double getContentTimePosition() {
			return contentTimePosition;
		}

		public void setContentTimePosition(Double contentTimePosition) {
			this.contentTimePosition = contentTimePosition;
		}

		public Double getContentEndTimePosition() {
			return contentEndTimePosition;
		}

		public void setContentEndTimePosition(Double contentEndTimePosition) {
			this.contentEndTimePosition = contentEndTimePosition;
		}

		public String getTimePositionClass() {
			return timePositionClass;
		}

		public void setTimePositionClass(String timePositionClass) {
			this.timePositionClass = timePositionClass;
		}

		public String getAdFormat() {
			return adFormat;
		}

		public void setAdFormat(String adFormat) {
			this.adFormat = adFormat;
		}

		public Integer getTimeToNextAdUnit() {
			return timeToNextAdUnit;
		}

		public void setTimeToNextAdUnit(Integer timeToNextAdUnit) {
			this.timeToNextAdUnit = timeToNextAdUnit;
		}

		public String getCustomSlotId() {
			return customSlotId;
		}

		public void setCustomSlotId(String customSlotId) {
			this.customSlotId = customSlotId;
		}

		public Integer getSequence() {
			return sequence;
		}

		public void setSequence(Integer sequence) {
			this.sequence = sequence;
		}

	}

	public static class CopyProtectionDTO implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private String securityCode;
		private String securityOption;

		public String getSecurityCode() {
			return securityCode;
		}

		public void setSecurityCode(String securityCode) {
			this.securityCode = securityCode;
		}

		public String getSecurityOption() {
			return securityOption;
		}

		public void setSecurityOption(String securityOption) {
			this.securityOption = securityOption;
		}

	}

}
