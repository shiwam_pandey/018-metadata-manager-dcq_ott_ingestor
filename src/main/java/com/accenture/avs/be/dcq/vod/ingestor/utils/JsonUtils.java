package com.accenture.avs.be.dcq.vod.ingestor.utils;

import java.io.IOException;

import com.accenture.avs.commons.lib.LoggerWrapper;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * @author karthik.vadla
 *
 */
public class JsonUtils {
	private static final LoggerWrapper log = new LoggerWrapper(JsonUtils.class);
	private static ObjectMapper mapper = new ObjectMapper();

	private static ObjectMapper mapperNullValueFilter = new ObjectMapper();

	static {

		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapperNullValueFilter.setSerializationInclusion(Include.NON_NULL);
		mapperNullValueFilter.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	public static <T> T parseJson(String value, Class<T> clazz)
			throws JsonParseException, JsonMappingException, IOException {
		if (value != null && !value.trim().isEmpty()) {
			return mapper.readValue(value, clazz);
		} else {
			return null;
		}
	}

	public static String writeAsJsonString(Object obj) throws JsonParseException, JsonMappingException, IOException {
		if (obj == null) {
			return null;
		} else {
			return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
		}
	}

	public static String writeAsJsonStringWithoutNull(Object obj)
			throws JsonParseException, JsonMappingException, IOException {
		if (obj == null) {
			return null;
		} else {
			return mapperNullValueFilter.writeValueAsString(obj);
		}
	}

	/**
	 * Validates the Json format
	 * 
	 * @param jsonInString
	 * @return
	 */

	public static boolean isJSONValid(String jsonInString) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.readTree(jsonInString);
			return true;
		} catch (IOException e) {
			log.logError(e, "Invalid Json format");
			return false;
		}
	}

	public static String getJsonString(Object obj) {
		String jsonString = null;
		try {
			writeAsJsonString(obj);
		} catch (IOException e) {
			log.logError(e);
		}
		return jsonString;
	}

}
