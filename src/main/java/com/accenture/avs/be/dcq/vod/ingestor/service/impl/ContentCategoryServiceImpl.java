package com.accenture.avs.be.dcq.vod.ingestor.service.impl;

import java.io.InputStream;
import java.io.StringReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import com.accenture.avs.be.dcq.vod.ingestor.dto.contentcategory.asset.Asset;
import com.accenture.avs.be.dcq.vod.ingestor.manager.ContentCategoryManager;
import com.accenture.avs.be.dcq.vod.ingestor.manager.VodManager;
import com.accenture.avs.be.dcq.vod.ingestor.service.ContentCategoryService;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.Validator;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.contentcategory.asset.CategoryContent;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.configuration.CacheConstants;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.commons.lib.LoggerWrapper;


/**
 * @author karthik.vadla
 *
 */
@Service
@Qualifier("contentCategoryService")
public class ContentCategoryServiceImpl implements ContentCategoryService {

	@Autowired
private	ResourceLoader resourceLoader;
	@Autowired
	private ContentCategoryManager contentCategoryManager;
	
	@Autowired
	private VodManager vodManager;
	
	private static final LoggerWrapper log = new LoggerWrapper(ContentCategoryServiceImpl.class);

	@Override
	public GenericResponse ingestContentCategory(String contentCategoryXml,String transactionNumber,Configuration config) throws Exception {
		log.logMessage("call to ingestContentContent ");
		GenericResponse genericResponse = null;
		Asset asset = null;
		StringReader reader=null;
		InputStream contentCategoryXSD=null;
		// validating the xml
		try{
			if(StringUtils.isBlank(contentCategoryXml)){
				throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
						new NestedParameters("input xml data"));
			}
			if (DcqVodIngestorUtils.checkVulnerablesInInputXML(contentCategoryXml)) {
				throw new ApplicationException(com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
						new NestedParameters(DcqVodIngestorConstants.ERR_VULNERABLE_REQ));
							}
			 contentCategoryXSD = resourceLoader.getResource("classpath:dcq-vod-ingestor/"+DcqVodIngestorConstants.CATEGORYCONTENT.CATEGORY_CONTENT_XSD).getInputStream();
		final Pattern unescapedAmpersands = Pattern.compile("(&(?!amp;))");
		Matcher m = unescapedAmpersands.matcher(contentCategoryXml);
		String xmlWithAmpersandsEscaped = m.replaceAll("&amp;");

		boolean validXml = Validator.validateXMLSchema(contentCategoryXSD, xmlWithAmpersandsEscaped);
		if (validXml) {
			log.logMessage("Input XML is valid");
			JAXBContext jaxbContext = JAXBContext.newInstance(Asset.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			 reader = new StringReader(xmlWithAmpersandsEscaped);
			asset = (Asset) unmarshaller.unmarshal(reader);
			if (Validator.isEmpty(asset)) {
				log.logMessage("No category content to write. The execution is completed.");					
				genericResponse = new GenericResponse(DcqVodIngestorConstants.ACN_200, DcqVodIngestorConstants.OK, "");
				return genericResponse;
			}
		}
			
			
		contentCategoryManager.ingestCategoryContent(asset,transactionNumber,config);
			genericResponse = new GenericResponse(DcqVodIngestorConstants.ACN_200, DcqVodIngestorConstants.OK, "");
		} catch (SAXException ex) {
			throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
				new NestedParameters("XML. Details : "+ex.getMessage()));
			
			/*ApplicationException ae = new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,new NestedParameters("XML"));
			NestedObject nestObj= ae.new NestedObject();
			nestObj.setResultObj("Invalid Input XML. Details : "+ex.getMessage());
			ae.setResultObj(nestObj);
			throw ae;*/
			
		}catch (ApplicationException ae) {
			log.logError(ae);
			throw ae;

		}
		catch (Exception e) {
			throw e;
		}finally{
			if(reader!=null){
				reader.close();
			}
			if(contentCategoryXSD!=null)contentCategoryXSD.close();
		}
		return genericResponse;
}

	@Override
	public GenericResponse ingestContentCategory(CategoryContent categoryContent, String transactionNumber,
			Configuration config) throws Exception {
		return null;
	}
	
}
