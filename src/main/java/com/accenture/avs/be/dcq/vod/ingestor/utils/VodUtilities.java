package com.accenture.avs.be.dcq.vod.ingestor.utils;

import java.io.IOException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.xml.bind.JAXBContext;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.cache.CopyProtectionsCache;
import com.accenture.avs.be.dcq.vod.ingestor.cache.LanguageCache;
import com.accenture.avs.be.dcq.vod.ingestor.dto.CopyProtectionConfiguration;
import com.accenture.avs.be.dcq.vod.ingestor.dto.CopyProtectionScheme;
import com.accenture.avs.be.dcq.vod.ingestor.dto.DCQEventCollectorContentDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionWriterRequestDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.EMFAttributesDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.EsContentDoc;
import com.accenture.avs.be.dcq.vod.ingestor.dto.EsContentDoc.AdvertisingInfoListDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.EsContentDoc.CopyProtectionDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.EsContentDoc.CuePointListDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.EsContentDoc.PopularityDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.EsContentDoc.RequestBean;
import com.accenture.avs.be.dcq.vod.ingestor.dto.EsContentDoc.RequestBean.ChildrenDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.EsContentDoc.RequestBean.ChildrenDTO.VaraintsDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.EsContentDoc.RequestBean.ContainersDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.EsContentDoc.RequestBean.ContainersDTO.BundlesDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.EsContentDoc.RequestBean.ContainersDTO.CategoriesDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.EsContentDoc.RequestBean.ContainersDTO.ParentsDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.EsContentDoc.RequestBean.MetadataDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.EsContentDoc.RequestBean.MetadataDTO.ChaptersDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.EsContentDoc.RequestBean.MetadataDTO.LanguagesDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.EsContentDoc.RequestBean.MetadataDTO.ScenesDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.EsContentDoc.RequestBean.PlatformVariantsDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.EsContentDoc.RequestBean.PlatformVariantsDTO.AudioLanguagesDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.EsContentDoc.RequestBean.PlatformVariantsDTO.SubTitlesLanguagesDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.EsContentDoc.RequestBean.PlatformVariantsDTO.TechnicalPackageDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.EsContentDoc.RequestBean.PropertiesDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.EsContentDoc.SuggestDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.AdvertisingInfo;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.AdvertisingInfoList;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Asset;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Chapter;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Chapter.MultiLanguage.ChapterLanguage;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.CopyProtection;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.CuePoint;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.CuePointList;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.EMFAttribute;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.MultilangMetadata;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Platform;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Property;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Scene;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Scene.MultiLanguage.SceneLanguage;
import com.accenture.avs.be.dcq.vod.ingestor.manager.impl.DcqAssetStagingManager;
import com.accenture.avs.be.dcq.vod.ingestor.repository.AudiolangRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.BundleAggregationRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ChannelRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ContentCategoryRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ContentLinkingRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ContentPlatformRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ContentPopularityRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ContentRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ExtendedContentAttributesRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.RelPlatformTechnicalRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.SubtitlesRepository;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ConfigurationException;
import com.accenture.avs.be.framework.utils.BooleanConverter;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.commons.lib.LoggerWrapper.OtherSystemCallType;
import com.accenture.avs.commons.lib.support.Utilities;
import com.accenture.avs.persistence.dto.AudioLangDetailsDTO;
import com.accenture.avs.persistence.dto.BundleDetailsDTO;
import com.accenture.avs.persistence.dto.GroupOfBundleDetailsDTO;
import com.accenture.avs.persistence.dto.SubtitleLangDetailsDTO;
import com.accenture.avs.persistence.dto.TechnicalPackageDetailsDTO;
import com.accenture.avs.persistence.technicalcatalogue.ContentEntity;
import com.accenture.avs.persistence.technicalcatalogue.ContentPlatformEntity;
import com.accenture.avs.persistence.technicalcatalogue.ContentPopularityEntity;

@Component
public class VodUtilities {
	private static final LoggerWrapper log = new LoggerWrapper(VodUtilities.class);

	@Autowired
	private ContentRepository contentRepository;
	
	@Autowired
	private ContentPlatformRepository contentPlatformRepository;
	
	@Autowired
	private ExtendedContentAttributesRepository extendedContentAttributesRepository;

	@Autowired
	private ContentCategoryRepository contentCategoryRepository;

	@Autowired
	private ContentPopularityRepository contentPopularityRepository;
	
	@Autowired
	private ChannelRepository channelRepository;
	
	@Autowired
	private SubtitlesRepository subtitlesRepository;
	
	@Autowired
	private AudiolangRepository audiolangRepository;
	
	@Autowired
	private RelPlatformTechnicalRepository relPlatformTechnicalRepository;
	
	@Autowired
	private BundleAggregationRepository bundleAggregationRepository;
	
	@Autowired
	private ContentLinkingRepository contentLinkingRepository;
	@Autowired
	private LanguageCache languageCache;

	@Autowired
	private CopyProtectionsCache copyProtectionCache;

	
	@Autowired
	private DcqAssetStagingManager dcqAssetStagingManager;
	
	private static JAXBContext jcAsset;
	static {
		try {
			jcAsset = JAXBContext.newInstance(Asset.class);
		} catch (Exception e) {
			log.logError(e);
		}
	}
	public List<DcqVodIngestionWriterRequestDTO> constructEsDocsForContent(Integer contentId, Asset asset, String transactionNumber,Configuration config) {
		log.logMessage("Constructing ES Doc(s) for ContentId: {} -Start", contentId);
		long sTime = System.currentTimeMillis();
		Map<String, String> languagesCache = languageCache.getLanguages();
		List<DcqVodIngestionWriterRequestDTO> esDocsContentList = null;

		try {
			Map<String, MultilangMetadata> assetMultiLangMap = null;
			Map<String, MultilangMetadata> assetDefaultMultiLangMap = null;
			List<LanguagesDTO> assetLanguagesDTOlist = new ArrayList<LanguagesDTO>();
			if (null != asset.getMultilangMetadataList()
					&& null != asset.getMultilangMetadataList().getMultilangMetadata()
					&& asset.getMultilangMetadataList().getMultilangMetadata().size() > 0) {
				assetMultiLangMap = new HashMap<String, MultilangMetadata>();
				for (MultilangMetadata multilangMetadata : asset.getMultilangMetadataList().getMultilangMetadata()) {
					assetMultiLangMap.put(multilangMetadata.getLangCode().toLowerCase(), multilangMetadata);
					LanguagesDTO languagesDTO = new LanguagesDTO();
					languagesDTO.setLanguageCode(multilangMetadata.getLangCode());
					languagesDTO.setLanguageName(multilangMetadata.getLangName());
					assetLanguagesDTOlist.add(languagesDTO);
					if (languageCache.getDefaultLanguage().equalsIgnoreCase(multilangMetadata.getLangCode())) {
						assetDefaultMultiLangMap = new HashMap<String, MultilangMetadata>();
						assetDefaultMultiLangMap.put(languageCache.getDefaultLanguage().toLowerCase(), multilangMetadata);
					}
				}
			}

			Map<Long, Map<String, ChapterLanguage>> chaptersToChapterMultiLangMap = null;
			Map<Long, List<String>> chaptersToChapterPlatformsMap = null;
			Map<String, ChapterLanguage> chapterMultiLangMap = null;
			Map<Long, ChapterLanguage> chapterDefaultLanguageMap = null;
			List<String> chapterPlatforms = null;
			if (null != asset.getChapterList() && null != asset.getChapterList().getChapter()
					&& asset.getChapterList().getChapter().size() > 0) {
				chaptersToChapterMultiLangMap = new HashMap<Long, Map<String, ChapterLanguage>>();
				chaptersToChapterPlatformsMap = new HashMap<Long, List<String>>();
				for (Chapter chapter : asset.getChapterList().getChapter()) {
					if (null != chapter.getMultiLanguage() && null != chapter.getMultiLanguage().getChapterLanguage()
							&& chapter.getMultiLanguage().getChapterLanguage().size() > 0) {
						chapterMultiLangMap = new HashMap<String, Chapter.MultiLanguage.ChapterLanguage>();
						for (int i = 0; i < chapter.getMultiLanguage().getChapterLanguage().size(); i++) {
							ChapterLanguage chapterLanguage = chapter.getMultiLanguage().getChapterLanguage().get(i);
							if (null != chapterLanguage && null != chapterLanguage.getMetadataLanguage()) {
								chapterMultiLangMap.put(chapterLanguage.getMetadataLanguage().toLowerCase(),
										chapterLanguage);
							}
							if (null != chapterLanguage && languageCache.getDefaultLanguage()
									.equalsIgnoreCase(chapterLanguage.getMetadataLanguage())) {
								chapterDefaultLanguageMap = new HashMap<Long, ChapterLanguage>();
								chapterDefaultLanguageMap.put(chapter.getChapterId(), chapterLanguage);
							}
						}
					}
					chaptersToChapterMultiLangMap.put(chapter.getChapterId(), chapterMultiLangMap);

					if (null != chapter.getPlatformList() && null != chapter.getPlatformList().getPlatform()
							&& chapter.getPlatformList().getPlatform().size() > 0) {
						chapterPlatforms = new ArrayList<>();
						for (int i = 0; i < chapter.getPlatformList().getPlatform().size(); i++) {
							Chapter.PlatformList.Platform chapterPlatform = chapter.getPlatformList().getPlatform()
									.get(i);
							if (null != chapterPlatform && null != chapterPlatform.getName()) {
								chapterPlatforms.add(chapterPlatform.getName());
							}
						}
					}
					chaptersToChapterPlatformsMap.put(chapter.getChapterId(), chapterPlatforms);
				}
			}

			Map<Long, Map<String, SceneLanguage>> scenesToSceneMultiLangMap = null;
			Map<Long, List<String>> scenesToScenePlatformsMap = null;
			Map<String, SceneLanguage> sceneMultiLangMap = null;
			Map<Long, SceneLanguage> sceneDefaultLanguageMap = null;
			List<String> scenePlatforms = null;
			if (null != asset.getSceneList() && null != asset.getSceneList().getScene()
					&& asset.getSceneList().getScene().size() > 0) {
				scenesToSceneMultiLangMap = new HashMap<Long, Map<String, SceneLanguage>>();
				scenesToScenePlatformsMap = new HashMap<Long, List<String>>();
				for (Scene scene : asset.getSceneList().getScene()) {
					if (null != scene.getMultiLanguage() && null != scene.getMultiLanguage().getSceneLanguage()
							&& scene.getMultiLanguage().getSceneLanguage().size() > 0) {
						sceneMultiLangMap = new HashMap<String, Scene.MultiLanguage.SceneLanguage>();
						sceneMultiLangMap = new HashMap<String, Scene.MultiLanguage.SceneLanguage>();
						for (int i = 0; i < scene.getMultiLanguage().getSceneLanguage().size(); i++) {
							SceneLanguage sceneLanguage = scene.getMultiLanguage().getSceneLanguage().get(i);
							if (null != sceneLanguage && null != sceneLanguage.getMetadataLanguage()) {
								sceneMultiLangMap.put(sceneLanguage.getMetadataLanguage().toLowerCase(), sceneLanguage);
							}
							if (null != sceneLanguage && languageCache.getDefaultLanguage()
									.equalsIgnoreCase(sceneLanguage.getMetadataLanguage())) {
								sceneDefaultLanguageMap = new HashMap<Long, SceneLanguage>();
								sceneDefaultLanguageMap.put(scene.getSceneId(), sceneLanguage);
							}
						}
					}
					scenesToSceneMultiLangMap.put(scene.getSceneId(), sceneMultiLangMap);

					if (null != scene.getPlatformList() && null != scene.getPlatformList().getPlatform()
							&& scene.getPlatformList().getPlatform().size() > 0) {
						scenePlatforms = new ArrayList<>();
						for (int i = 0; i < scene.getPlatformList().getPlatform().size(); i++) {
							Scene.PlatformList.Platform scenePlatform = scene.getPlatformList().getPlatform().get(i);
							if (null != scenePlatform && null != scenePlatform.getName()) {
								scenePlatforms.add(scenePlatform.getName());
							}
						}
					}
					scenesToScenePlatformsMap.put(scene.getSceneId(), scenePlatforms);
				}
			}

			Map<String, Platform> assetPlatformDataMap = null;
			if (null != asset.getExtensions() && null != asset.getExtensions().getPlatformList()
					&& null != asset.getExtensions().getPlatformList().getPlatform()
					&& asset.getExtensions().getPlatformList().getPlatform().size() > 0) {
				assetPlatformDataMap = new HashMap<String, Platform>();
				for (Platform platform : asset.getExtensions().getPlatformList().getPlatform()) {
					String videoType = (platform.getVideoType() == null || "".equals(platform.getVideoType())) ? "NA"
							: platform.getVideoType();
					assetPlatformDataMap.put(platform.getName() + videoType, platform);
				}
			}

			Map<String, List<EMFAttributesDTO>> assetEmfAttrMap = null;
			Map<String, List<EMFAttributesDTO>> assetEmfAttrDefaultMap = null;
			List<EMFAttributesDTO> emfAttributeDTOList = null;
			List<EMFAttributesDTO> emfAttributeDTODefaultList = null;
			if (null != asset.getEMFAttributeList() && null != asset.getEMFAttributeList().getEMFAttribute()
					&& asset.getEMFAttributeList().getEMFAttribute().size() > 0) {
				assetEmfAttrMap = new HashMap<String, List<EMFAttributesDTO>>();
				assetEmfAttrDefaultMap = new HashMap<String, List<EMFAttributesDTO>>();
				for (int i = 0; i < asset.getEMFAttributeList().getEMFAttribute().size(); i++) {
					EMFAttributesDTO emfAttributesDTO = new EMFAttributesDTO();
					EMFAttribute emf = asset.getEMFAttributeList().getEMFAttribute().get(i);
					emfAttributesDTO.setEmfName(emf.getName());
					emfAttributesDTO.setEmfValue(emf.getValue());

					if (null == assetEmfAttrMap.get(emf.getMetadataLanguage().toLowerCase())) {
						emfAttributeDTOList = new ArrayList<>();
					} else {
						emfAttributeDTOList = assetEmfAttrMap.get(emf.getMetadataLanguage().toLowerCase());
					}
					emfAttributeDTOList.add(emfAttributesDTO);
					assetEmfAttrMap.put(emf.getMetadataLanguage().toLowerCase(), emfAttributeDTOList);

					if (null != emf.getMetadataLanguage()
							&& emf.getMetadataLanguage().equals(languageCache.getDefaultLanguage())) {
						if (null == assetEmfAttrDefaultMap.get(emf.getMetadataLanguage().toLowerCase())) {
							emfAttributeDTODefaultList = new ArrayList<>();
						} else {
							emfAttributeDTODefaultList = assetEmfAttrDefaultMap
									.get(emf.getMetadataLanguage().toLowerCase());
						}
						emfAttributeDTODefaultList.add(emfAttributesDTO);
						assetEmfAttrDefaultMap.put(languageCache.getDefaultLanguage().toLowerCase(),
								emfAttributeDTODefaultList);
					}
				}
			}

			// String preferredLang = asset.getLanguages().getPreferredLang().getLabel();

			/* Categories under Containers */
			// List<CategoriesDTO> categoriesDTOs =
			// retrieveCategoriesForContainers(contentId, entityMgr);

			log.logMessage("Constructing the Categories For Containers - Start");
			List<CategoriesDTO> categoriesDTOs = null;
			Long primaryCatId = null;
			List<Object[]> objects = contentCategoryRepository.retrieveDetailsByContentId(contentId);
			if (null != objects && !objects.isEmpty()) {
				categoriesDTOs = new ArrayList<CategoriesDTO>();
				for (int i = 0; i < objects.size(); i++) {
					Object[] objArr = objects.get(i);
					CategoriesDTO categoriesDTO = new CategoriesDTO();
					String categoryPathIds = (String) objArr[0];
					String[] catPathIds = categoryPathIds == null ? new String[0] : categoryPathIds.split(";");
					Integer[] catPathIdsInt = new Integer[catPathIds.length];
					for (int j = 0; j < catPathIds.length; j++) {
						catPathIdsInt[j] = Integer.parseInt(catPathIds[j]);
					}
					categoriesDTO.setCategoryPathIds(catPathIdsInt);
					String externalPathIds = (String) objArr[1];
					categoriesDTO
							.setExternalPathIds(externalPathIds == null ? new String[0] : externalPathIds.split(";"));
					categoriesDTO.setCategoryId(objArr[2] == null ? null : ((Integer) objArr[2]).longValue());
					Date sDate = (Date) objArr[3];
					categoriesDTO.setStartDate(sDate == null ? null : sDate.getTime());
					Date eDate = (Date) objArr[4];
					categoriesDTO.setEndDate(eDate == null ? null : eDate.getTime());
					categoriesDTO.setOrderId(objArr[5] == null ? null : ((Integer) objArr[5]).longValue());
					categoriesDTO.setCategoryName(objArr[7] == null ? null : objArr[7].toString());
					categoriesDTO.setIsPrimary(objArr[6]== null ? false : Boolean.getBoolean(objArr[6].toString()));
					categoriesDTOs.add(categoriesDTO);

					// Fix for AVS - 6909
					if (!DcqVodIngestorUtils.isEmpty(objArr[6])) {
						String s = objArr[6].toString();
						if (s.equalsIgnoreCase("Y")) {
							primaryCatId = objArr[2] == null ? null : ((Integer) objArr[2]).longValue();
						}
					}
				}
			}

			/* Parents under Containers */
			List<ParentsDTO> parentsDTOs = retrieveParentsForContainers(contentId);
			/* Note - Not Setting Bundles in containersDTO as they are Platform Specific. */

			List<ContentPlatformEntity> contentPlatformEntities = contentPlatformRepository
					.retrieveByContentId(contentId); // Get Both Published &
																// NonPublished
			Set<String> platformNames = new HashSet<String>();
			Set<String> publisedPlatformNames = new HashSet<String>();
			Map<Integer, String> cpIdVideoTypeMap = new HashMap<Integer, String>();
			Map<String, List<Integer>> platformPublishedCpIdsMap = new HashMap<String, List<Integer>>();
			Map<String, Object[]> platformDatesMap = new HashMap<String, Object[]>();
			Map<Integer, Object[]> pictureAndVideoUrlMap = new HashMap<Integer, Object[]>();
			Map<String, String> platformNametoPictureUrlMap = new HashMap<String, String>();
			List<Integer> publishedCpIds = null;
			List<Integer> allPublishedCpIds = new ArrayList<Integer>();
			for (ContentPlatformEntity cpe : contentPlatformEntities) {
				platformNames.add(cpe.getPlatform());
				if (String.valueOf(cpe.getIsPublished()).equals(DcqVodIngestorConstants.SHORT_YES)) {

					publisedPlatformNames.add(cpe.getPlatform());
					String videoType = cpe.getVideoName() == null ? "" : cpe.getVideoName();
					cpIdVideoTypeMap.put(cpe.getCpId(), cpe.getPlatform() + videoType);
					if (platformPublishedCpIdsMap.get(cpe.getPlatform()) == null) {
						publishedCpIds = new ArrayList<Integer>();
					} else {
						publishedCpIds = platformPublishedCpIdsMap.get(cpe.getPlatform());
					}
					publishedCpIds.add(cpe.getCpId());
					allPublishedCpIds.add(cpe.getCpId());
					platformPublishedCpIdsMap.put(cpe.getPlatform(), publishedCpIds);

					Long startDateinMs = cpe.getContractStart() == null ? null : cpe.getContractStart().getTime();
					Long endDateinMs = cpe.getContractEnd() == null ? null : cpe.getContractEnd().getTime();
					Object[] dates = new Object[2];
					dates[0] = startDateinMs;
					dates[1] = endDateinMs;
					platformDatesMap.put(cpe.getPlatform(), dates);

					Object[] urls = new Object[6];
					urls[0] = cpe.getPictureUrl();
					urls[1] = cpe.getVideoUrl();
					urls[2] = cpe.getTrailerUrl();
					urls[3] = cpe.getVideoName();
					urls[4] = cpe.getDrmInfo();
					urls[5] = cpe.getStreamingType();
					pictureAndVideoUrlMap.put(cpe.getCpId(), urls);

					if (null == platformNametoPictureUrlMap.get(cpe.getPlatform())) {
						if (null != cpe.getPictureUrl()) {
							platformNametoPictureUrlMap.put(cpe.getPlatform(), cpe.getPictureUrl());
						}
					}

				}
			}

			/* Get the SubTitle Lang details based on CPIds at once rather than in loop */
			List<SubtitleLangDetailsDTO> subtitleLangDetailsDTOs = null;
			if (null != allPublishedCpIds && !allPublishedCpIds.isEmpty()) {
				subtitleLangDetailsDTOs = subtitlesRepository.retrieveSubTitlesDetailsByCpIds(allPublishedCpIds);
			}
			Map<Integer, List<SubtitleLangDetailsDTO>> cpIdSubTitleLangsMap = null;
			if (null != subtitleLangDetailsDTOs && !subtitleLangDetailsDTOs.isEmpty()) {
				cpIdSubTitleLangsMap = new HashMap<Integer, List<SubtitleLangDetailsDTO>>();
				for (SubtitleLangDetailsDTO dto : subtitleLangDetailsDTOs) {
					List<SubtitleLangDetailsDTO> subtitleLangDetailsDTOList = cpIdSubTitleLangsMap.get(dto.getCpId());
					if (null == subtitleLangDetailsDTOList) {
						subtitleLangDetailsDTOList = new ArrayList<SubtitleLangDetailsDTO>();
						subtitleLangDetailsDTOList.add(dto);
					} else {
						subtitleLangDetailsDTOList.add(dto);
					}
					cpIdSubTitleLangsMap.put(dto.getCpId(), subtitleLangDetailsDTOList);
				}
			}

			/* Get the AudioLang details based on CPIds at once rather than in loop */
			List<AudioLangDetailsDTO> audioLangDetailsDTOs = null;
			if (null != allPublishedCpIds && !allPublishedCpIds.isEmpty()) {
				audioLangDetailsDTOs = audiolangRepository.retrieveAudioDetailsByCPIds(allPublishedCpIds);
			}
			Map<Integer, List<AudioLangDetailsDTO>> cpIdAudioLangsMap = null;
			if (null != audioLangDetailsDTOs && !audioLangDetailsDTOs.isEmpty()) {
				cpIdAudioLangsMap = new HashMap<Integer, List<AudioLangDetailsDTO>>();
				for (AudioLangDetailsDTO dto : audioLangDetailsDTOs) {
					List<AudioLangDetailsDTO> audioLangDetailsDTOList = cpIdAudioLangsMap.get(dto.getCpId());
					if (null == audioLangDetailsDTOList) {
						audioLangDetailsDTOList = new ArrayList<AudioLangDetailsDTO>();
						audioLangDetailsDTOList.add(dto);
					} else {
						audioLangDetailsDTOList.add(dto);
					}
					cpIdAudioLangsMap.put(dto.getCpId(), audioLangDetailsDTOList);
				}
			}

			/*
			 * Get the Technical Package details based on CPIds at once rather than in loop
			 */
			List<TechnicalPackageDetailsDTO> tpDetailsDTOs = null;
			if (null != allPublishedCpIds && !allPublishedCpIds.isEmpty()) {
				tpDetailsDTOs = relPlatformTechnicalRepository.retrieveTechnicalPakcgeDetailsbyCpIds(allPublishedCpIds);
			}
			Map<Integer, List<TechnicalPackageDetailsDTO>> cpIdTpDetailsmap = null;
			if (null != tpDetailsDTOs && !tpDetailsDTOs.isEmpty()) {
				cpIdTpDetailsmap = new HashMap<Integer, List<TechnicalPackageDetailsDTO>>();
				for (TechnicalPackageDetailsDTO dto : tpDetailsDTOs) {
					List<TechnicalPackageDetailsDTO> tpDetailsDTOList = cpIdTpDetailsmap.get(dto.getCpId());
					if (null == tpDetailsDTOList) {
						tpDetailsDTOList = new ArrayList<TechnicalPackageDetailsDTO>();
						tpDetailsDTOList.add(dto);
					} else {
						tpDetailsDTOList.add(dto);
					}
					cpIdTpDetailsmap.put(dto.getCpId(), tpDetailsDTOList);
				}
			}

			/* Get the Bundle Details details based on CPIds at once rather than in loop */
			List<BundleDetailsDTO> bundleDetailsDTOsList = null;
			if (null != allPublishedCpIds && !allPublishedCpIds.isEmpty()) {
				bundleDetailsDTOsList = bundleAggregationRepository.retrieveDetailsDTOByCpIds(allPublishedCpIds);//
			}

			Map<Integer, List<BundleDetailsDTO>> cpIdBundleDetailDTOsMap = null;
			Map<Integer, List<String>> bundleIdToPublishedPlatformsMap = null;
			Map<Integer, Set<Integer>> bundleIdToGoBIdsMap = new HashMap<Integer, Set<Integer>>();
			Map<Integer, GroupOfBundleDetailsDTO> grpOfbundleIdToDetailsDTOMap = new HashMap<Integer, GroupOfBundleDetailsDTO>();
			Map<Integer, List<String>> grpOfbundleIdToPublishedPlatformsMap = null;

			if (null != bundleDetailsDTOsList && !bundleDetailsDTOsList.isEmpty()) {
				List<Integer> bundleIdsList = new ArrayList<Integer>(); // SET
				cpIdBundleDetailDTOsMap = new HashMap<Integer, List<BundleDetailsDTO>>();
				bundleIdToPublishedPlatformsMap = new HashMap<Integer, List<String>>();
				for (BundleDetailsDTO dto : bundleDetailsDTOsList) {
					List<BundleDetailsDTO> bundleDetailsDTOList = cpIdBundleDetailDTOsMap.get(dto.getCpId());
					if (null == bundleDetailsDTOList) {
						bundleDetailsDTOList = new ArrayList<BundleDetailsDTO>();
						bundleDetailsDTOList.add(dto);
					} else {
						bundleDetailsDTOList.add(dto);
					}
					cpIdBundleDetailDTOsMap.put(dto.getCpId(), bundleDetailsDTOList); // cpId to bundleDTOsList map
					bundleIdsList.add(dto.getBundleContentId());
				}

				List<GroupOfBundleDetailsDTO> groupOfBundleDetailsDTOsList = new ArrayList<GroupOfBundleDetailsDTO>();
				if (null != bundleIdsList && !bundleIdsList.isEmpty()) {
					for (Integer bundleContentId : bundleIdsList) {
						List<String> publishedPlatformsForBundle = contentPlatformRepository
								.retrievePublishedPlatformsByContentId(bundleContentId); // Make it in
						bundleIdToPublishedPlatformsMap.put(bundleContentId, publishedPlatformsForBundle); // bundle
																											// contentid
																											// to
																											// published
																											// platforms
																											// map
						// groupof bundle dtos
					}
					List<GroupOfBundleDetailsDTO> gobDetailsDTOsForBundle = bundleAggregationRepository
							.retrieveDetailsByContentId(bundleIdsList); // Make it in
					groupOfBundleDetailsDTOsList.addAll(gobDetailsDTOsForBundle);

				}

				Set<Integer> grpOfbundleIdList = new HashSet<Integer>();
				if (null != groupOfBundleDetailsDTOsList && !groupOfBundleDetailsDTOsList.isEmpty()) {
					grpOfbundleIdToPublishedPlatformsMap = new HashMap<Integer, List<String>>();
					for (GroupOfBundleDetailsDTO gobDTO : groupOfBundleDetailsDTOsList) {
						Set<Integer> gobIds = bundleIdToGoBIdsMap.get(gobDTO.getContentId());
						if (gobIds == null) {
							gobIds = new HashSet<Integer>();
							gobIds.add(gobDTO.getBundleContentId());
						} else {
							gobIds.add(gobDTO.getBundleContentId());
						}
						bundleIdToGoBIdsMap.put(gobDTO.getContentId(), gobIds);
						grpOfbundleIdToDetailsDTOMap.put(gobDTO.getBundleContentId(), gobDTO);
						grpOfbundleIdList.add(gobDTO.getBundleContentId());
					}
					if (!grpOfbundleIdList.isEmpty()) {
						for (Integer grpOfbundleId : grpOfbundleIdList) {
							List<String> publishedPlatformsForGrpOfBundle = contentPlatformRepository
									.retrievePublishedPlatformsByContentId(grpOfbundleId);
							grpOfbundleIdToPublishedPlatformsMap.put(grpOfbundleId, publishedPlatformsForGrpOfBundle); // Group
																														// Of
																														// bundleId
																														// to
																														// published
																														// platforms
																														// map
						}
					}	
					
				}

			}

			// AVS-13771 start
			List<AdvertisingInfoListDTO> advertisingInfoDTOs = retrieveAdvertisingInfoList(asset);

			// AVS-13771 stop

			/* metadata construction */
			MetadataDTO metadataDTO = retrieveMetadata(contentId, asset, assetLanguagesDTOlist, publisedPlatformNames,
					primaryCatId, config);
			/*
			 * Note - Not Setting children , platformVariants to requestBean as they are
			 * Platform Specific.
			 */

			esDocsContentList = new ArrayList<DcqVodIngestionWriterRequestDTO>();
			for (String langCode : languagesCache.keySet()) {

				String writeAlias = DcqVodIngestorUtils
						.getWriteAlias(DcqVodIngestorConstants.CONTENT_IN_LOWER + "_" + langCode.toLowerCase());

				ContainersDTO containersDTO = new ContainersDTO();
				containersDTO.setCategories(categoriesDTOs);
				containersDTO.setParents(parentsDTOs);

				for (String platformName : platformNames) {

					if (null != publisedPlatformNames && publisedPlatformNames.contains(platformName)) {

						MetadataDTO metadataDTOWithLang = replaceMultiLangDataForMetadata(metadataDTO,
								langCode.toLowerCase(), assetMultiLangMap, assetDefaultMultiLangMap, assetEmfAttrMap,
								assetEmfAttrDefaultMap, chaptersToChapterMultiLangMap, scenesToSceneMultiLangMap,
								chapterDefaultLanguageMap, sceneDefaultLanguageMap, chaptersToChapterPlatformsMap,
								scenesToScenePlatformsMap, platformName, config);

						// AVS-13771 start
						if (!advertisingInfoDTOs.isEmpty()) {
							metadataDTOWithLang.setAdvertisingInfoList(advertisingInfoDTOs);
						}
						// AVS-13771 stop
						RequestBean requestBean = new RequestBean();
						requestBean.setContentId(contentId.longValue());
						requestBean.setPlatformName(platformName.toLowerCase());
						List<Integer> cpIds = platformPublishedCpIdsMap.get(platformName);
						log.logMessage("BEFORE PASS NOW: {};CPIds: {}", platformName, cpIds);
						/* bundles under containers are platform specific */

						List<BundlesDTO> bundlesDTOs = getBundlesForContainers(cpIds, platformName,
								cpIdBundleDetailDTOsMap, bundleIdToPublishedPlatformsMap, bundleIdToGoBIdsMap,
								grpOfbundleIdToDetailsDTOMap, grpOfbundleIdToPublishedPlatformsMap);

						if (bundlesDTOs != null && !bundlesDTOs.isEmpty()) {
							containersDTO.setBundles(bundlesDTOs);
						}

						// Moved to out side of the if block to fix the issue AVS-9119
						requestBean.setContainers(containersDTO);

						/*
						 * containersDTO.setBundles(bundlesDTOs);
						 * requestBean.setContainers(containersDTO);
						 */

						List<String> childernSubTypeList = null;
						/* children details are platform specific */
						List<ChildrenDTO> childrenDTOs = retrieveChildrenList(asset, platformName, childernSubTypeList);
						if (childrenDTOs != null && !childrenDTOs.isEmpty()) { // AVS-8247
							requestBean.setChildren(childrenDTOs);
						}

						/* platform Variants are platform specific */
						List<PlatformVariantsDTO> platformVariantsDTOs = null;
						platformVariantsDTOs = retrievePlatformVariantList(platformName, cpIds, cpIdAudioLangsMap,
								cpIdSubTitleLangsMap, cpIdTpDetailsmap, cpIdVideoTypeMap, assetPlatformDataMap,
								childernSubTypeList, pictureAndVideoUrlMap);
						log.logMessage("Lang: {} Platform: {} ;Var-Size: {}", langCode.toLowerCase(), platformName,
								platformVariantsDTOs.size());
						if (platformVariantsDTOs != null && !platformVariantsDTOs.isEmpty()) { // AVS-8247
							requestBean.setPlatformVariants(platformVariantsDTOs);
						}

						/* pictureUrl is platform specific */
						if (null != platformNametoPictureUrlMap.get(platformName)) {
							metadataDTOWithLang.setPictureUrl(platformNametoPictureUrlMap.get(platformName));
						}
						Integer comingSoonDays = Integer
								.parseInt(config.getConstants().getValue(CacheConstants.COMING_SOON_TIME_WINDOW));
						Integer leavingSoonDays = Integer
								.parseInt(config.getConstants().getValue(CacheConstants.LEAVING_SOON_TIME_WINDOW));
						/* contractStart and contractEnd Dates are platform specific */
						Object[] dates = platformDatesMap.get(platformName);
						if (null != dates) {
							Long startDate = dates[0] == null ? null : (Long) dates[0];
							metadataDTOWithLang.setContractStartDate(startDate);
							Long endDate = dates[1] == null ? null : (Long) dates[1];
							metadataDTOWithLang.setContractEndDate(endDate);
							metadataDTOWithLang.setStartTime(startDate);
							metadataDTOWithLang.setEndTime(endDate);
							if (startDate == null) {
								metadataDTOWithLang.setComingSoon(false);
							} else {
								long duration = 0;
								if (comingSoonDays != null) {
									duration = TimeUnit.DAYS.toMillis(comingSoonDays);
								}
								metadataDTOWithLang.setComingSoon((startDate > new Date().getTime()
										&& startDate <= new Date().getTime() + duration) ? true : false);
							}
							if (endDate == null) {
								metadataDTOWithLang.setLeavingSoon(false);
							} else {
								long duration = 0;
								if (leavingSoonDays != null) {
									duration = TimeUnit.DAYS.toMillis(leavingSoonDays);
								}
								metadataDTOWithLang.setLeavingSoon(
										(endDate > new Date().getTime() && endDate <= new Date().getTime() + duration)
												? true
												: false);
							}
							if(startDate!=null){
								long duration = TimeUnit.DAYS.toMillis(1);								
								metadataDTOWithLang.setRecentlyAdded((startDate < new Date().getTime() && new Date().getTime()-startDate <= duration)?true:false);
							}else{
								metadataDTOWithLang.setRecentlyAdded(false);
							}
						}

						requestBean.setMetadata(metadataDTOWithLang);

						// AVS-12973 start
						if (null != asset.getProperties() && !asset.getProperties().getProperty().isEmpty()) {
							List<Property> propertyList = asset.getProperties().getProperty();
							requestBean.setProperties(getpropertiesList(propertyList));
						}

						// AVS-12973 stop

						/*
						 * suggest and payload fields as these fields could be Language/Platform
						 * Specific
						 */
						SuggestDTO suggestDTO = new SuggestDTO();
						List<String> suggestValues = new ArrayList<>();
						 suggestValues = constructSuggestFields(metadataDTOWithLang, config);
						if((metadataDTOWithLang.getIsParent()!=null && metadataDTOWithLang.getIsParent().booleanValue()==true) || metadataDTOWithLang.getIsParent()==null) {
						   suggestDTO.setInput(suggestValues == null ? null : suggestValues.toArray(new String[0]));
						}
						PopularityDTO popularityDTO = constructPopularityDto(contentId);
						
						EsContentDoc esContentDoc = new EsContentDoc();
						esContentDoc.setContent(requestBean);
						esContentDoc.setSuggest(suggestDTO);
						esContentDoc.setPopularity(popularityDTO);
						esContentDoc.setSearchMetadata(suggestValues == null ? null : suggestValues.toArray(new String[0]));
						
						DcqVodIngestionWriterRequestDTO writerRequestDTO = new DcqVodIngestionWriterRequestDTO();
						writerRequestDTO.setEsUniqueId(contentId);
						writerRequestDTO.setEsIndex(writeAlias);
						writerRequestDTO.setEsIndexType(DcqVodIngestorConstants.ALL_IN_LOWER);
						writerRequestDTO.setPlatformName(platformName.toLowerCase());
						writerRequestDTO.setEsDocObj(esContentDoc);
						writerRequestDTO.setSourceFile(asset);
						esDocsContentList.add(writerRequestDTO);
					} else {
						DcqVodIngestorUtils
								.deleteDocumentFromES(
										writeAlias, DcqVodIngestorConstants.ALL_IN_LOWER, contentId.toString()
												+ DcqVodIngestorConstants.HYPHEN_SYMBOL + platformName.toLowerCase(),
										config);

						DcqVodIngestionWriterRequestDTO writerRequestDTO = new DcqVodIngestionWriterRequestDTO();
						writerRequestDTO.setEsUniqueId(contentId);
						writerRequestDTO.setEsIndex(writeAlias);
						writerRequestDTO.setEsIndexType(DcqVodIngestorConstants.ALL_IN_LOWER);
						writerRequestDTO.setPlatformName(platformName.toLowerCase());

						writerRequestDTO.setDeleted(true);
						// AVS-13771 start to get externla id
						RequestBean requestBean = new RequestBean();
						requestBean.setMetadata(metadataDTO);
						EsContentDoc esContentDoc = new EsContentDoc();
						esContentDoc.setContent(requestBean);
						writerRequestDTO.setEsDocObj(esContentDoc);
						// AVS-13771 stop
						esDocsContentList.add(writerRequestDTO);

						// Start AVS 9263
						DCQEventCollectorContentDTO contentDTO = new DCQEventCollectorContentDTO();
						contentDTO.setAction(DcqVodIngestorConstants.DELETE);
						contentDTO.setContentType(DcqVodIngestorConstants.CONTENT);
						contentDTO.setContentId(contentId);
						contentDTO.setPlatform(platformName.toUpperCase());
						contentDTO.setExtContentId(metadataDTO.getExternalId());
						contentDTO.setLang(langCode.toUpperCase());
						String payLoad = JsonUtils.writeAsJsonStringWithoutNull(contentDTO);
						DcqVodIngestorUtils.sendEventCollectorReport(DcqVodIngestorConstants.DCQ_CONTENT_INGESTOR,
								DcqVodIngestorConstants.CONTENT_INGESTION, payLoad, config);

						// End AVS 9263

					}
				}
			}
		} catch (Exception e) {
			log.logError(e,
					"Error while constructing ES Doc for ContentId:" + contentId + ",Update the status to FAILED.");
			List<Integer> contentIds = new ArrayList<Integer>();
			contentIds.add(contentId);
			String errorInfo = DcqVodIngestorUtils.formatString(OtherSystemCallType.INTERNAL,
					DcqVodIngestorUtils.getLoggingMethod(), DcqVodIngestorUtils.getErrorStackTrace(e));
			dcqAssetStagingManager.updateStatus(errorInfo, contentIds, DcqVodIngestorConstants.CONTENT, config);
		}
		log.logMessage("Constructing ES Doc(s) for ContentId: {} -Completed,ES Docs count: {} ,took: {} ms", contentId,
				(esDocsContentList == null ? 0 : esDocsContentList.size()), (System.currentTimeMillis() - sTime));
		return esDocsContentList;
	}

	private PopularityDTO constructPopularityDto(int contentId) {
		PopularityDTO popularityDTO = null;
		ContentPopularityEntity contentPopularityEntity = contentPopularityRepository.findByContentId(contentId);
		if (Objects.nonNull(contentPopularityEntity)) {
			popularityDTO = convertPopularityDto(contentPopularityEntity);
		}else {
			popularityDTO = new PopularityDTO();
			popularityDTO.setPopularityBoost(new BigDecimal("1.0"));
			popularityDTO.setPopularityKPI(new BigDecimal("1.0"));
		}
		return popularityDTO;
	}

	private PopularityDTO convertPopularityDto(ContentPopularityEntity contentPopularityEntity) {
		PopularityDTO popularityDTO = new PopularityDTO();
		popularityDTO.setPopularityKPI(contentPopularityEntity.getPopularityKPI());
		popularityDTO.setPopularityBoost(contentPopularityEntity.getPopularityBoost());
		return popularityDTO;
	}

	private List<BundlesDTO> getBundlesForContainers(List<Integer> cpIds, String platformName,
			Map<Integer, List<BundleDetailsDTO>> cpIdBundleDetailDTOsMap,
			Map<Integer, List<String>> bundleIdToPublishedPlatformsMap, Map<Integer, Set<Integer>> bundleIdToGoBIdsMap,
			Map<Integer, GroupOfBundleDetailsDTO> grpOfbundleIdToDetailsDTOMap,
			Map<Integer, List<String>> grpOfbundleIdToPublishedPlatformsMap) {
		long sTime = System.currentTimeMillis();
		log.logMessage("Constructing the Bundles For Containers for cpIds: {} - Start", cpIds);
		List<BundlesDTO> bundlesDTOs = new ArrayList<BundlesDTO>();
		if (null == cpIds || cpIds.isEmpty()) {
			return bundlesDTOs;
		}

		Set<Integer> bundleIdsSet = new HashSet<Integer>();
		for (Integer cpId : cpIds) {
			if (null != cpIdBundleDetailDTOsMap && !cpIdBundleDetailDTOsMap.isEmpty()
					&& null != cpIdBundleDetailDTOsMap.get(cpId) && !cpIdBundleDetailDTOsMap.get(cpId).isEmpty()) {
				for (BundleDetailsDTO dto : cpIdBundleDetailDTOsMap.get(cpId)) { // for each bundle.
					if (null != bundleIdToPublishedPlatformsMap
							&& !bundleIdToPublishedPlatformsMap.get(dto.getBundleContentId()).isEmpty()
							&& bundleIdToPublishedPlatformsMap.get(dto.getBundleContentId()).contains(platformName)) {
						// only if bundle is published for the platform
						if (bundleIdsSet.contains(dto.getBundleContentId())) {
							continue;
						}
						BundlesDTO bundlesDTO = new BundlesDTO();
						bundlesDTO.setBundleId(
								dto.getBundleContentId() == null ? null : Long.valueOf(dto.getBundleContentId()));
						bundlesDTO.setOrderId(dto.getPosition() == null ? null : Long.valueOf(dto.getPosition()));
						bundlesDTO.setBundleType(dto.getObjectType());
						bundlesDTO.setBundleSubtype(dto.getObjectSubtype());
						bundlesDTO.setIsParent(true);
						bundlesDTOs.add(bundlesDTO);
						bundleIdsSet.add(dto.getBundleContentId());

						if (null != bundleIdToGoBIdsMap && !bundleIdToGoBIdsMap.isEmpty()
								&& null != bundleIdToGoBIdsMap.get(dto.getBundleContentId())
								&& !bundleIdToGoBIdsMap.get(dto.getBundleContentId()).isEmpty()) { // if there are any Grp
																									// Of Bundles
							for (Integer grpBundleId : bundleIdToGoBIdsMap.get(dto.getBundleContentId())) {
								GroupOfBundleDetailsDTO grpBundleDTO = grpOfbundleIdToDetailsDTOMap == null ? null
										: grpOfbundleIdToDetailsDTOMap.get(grpBundleId);
								if (null != grpBundleDTO && null != grpOfbundleIdToPublishedPlatformsMap
										&& !grpOfbundleIdToPublishedPlatformsMap.isEmpty()
										&& grpOfbundleIdToPublishedPlatformsMap.get(grpBundleId)
												.contains(platformName)) {
									// only if Grp Of Bundle is published for the platform
									BundlesDTO grpBundlesDTO = new BundlesDTO();
									grpBundlesDTO.setBundleId(grpBundleDTO.getBundleContentId() == null ? null
											: Long.valueOf(grpBundleDTO.getBundleContentId()));
									grpBundlesDTO.setOrderId(grpBundleDTO.getPosition() == null ? null
											: Long.valueOf(grpBundleDTO.getPosition()));
									grpBundlesDTO.setBundleType(grpBundleDTO.getObjectType());
									grpBundlesDTO.setBundleSubtype(grpBundleDTO.getObjectSubtype());
									grpBundlesDTO.setIsParent(false);
									bundlesDTOs.add(grpBundlesDTO);
								}
							}}
					}
				}
			}
		}
		log.logMethodEnd(System.currentTimeMillis() - sTime);
		return bundlesDTOs;
	}

	private List<String> constructSuggestFields(MetadataDTO metadataDTO, Configuration config)
			throws IllegalArgumentException, IllegalAccessException, ConfigurationException {
		long sTime = System.currentTimeMillis();
		log.logMessage("Constructing Suggest Fields - Start");
		String suggestFields = config.getConstants().getValue(CacheConstants.DCQ_VOD_INGESTOR_CONTENT_SUGGESTIONS_FIELDS);

		List<String> suggestParams = suggestFields == null ? null : Arrays.asList(suggestFields.split(";"));
		List<String> suggestValues = null;
		if ((null != suggestParams && !suggestParams.isEmpty())) {
			suggestValues = new ArrayList<String>();
			Field[] metadataFields = metadataDTO.getClass().getDeclaredFields();
			for (Field field : metadataFields) {
				field.setAccessible(true);
				Object name = field.get(metadataDTO);
				if (null != suggestParams && !suggestParams.isEmpty() && suggestParams.contains(field.getName())) {
					if (null != String.valueOf(name) && !String.valueOf(name).equals("null")
							&& !String.valueOf(name).equals("")) {
						if (field.getType() == String[].class) {
							Object[] arr = (Object[]) name;
							for (int i = 0; i < arr.length; i++) {
								if (null != arr[i]) {
									suggestValues.add(String.valueOf(arr[i]));
								}
							}
						} else {
							suggestValues.add(String.valueOf(name));
						}
					}
				}
			}
		}

		log.logMessage("Suggest Input Values: {}", suggestValues);
		
		log.logMethodEnd(System.currentTimeMillis() - sTime);
		return suggestValues;
	}

	private MetadataDTO replaceMultiLangDataForMetadata(MetadataDTO metadataDTO, String langCode,
			Map<String, MultilangMetadata> assetMultiLangMap, Map<String, MultilangMetadata> assetDefaultMultiLangMap,
			Map<String, List<EMFAttributesDTO>> assetEmfAttrMap,
			Map<String, List<EMFAttributesDTO>> assetEmfAttrDefaultMap,
			Map<Long, Map<String, ChapterLanguage>> chapterMap, Map<Long, Map<String, SceneLanguage>> sceneMap,
			Map<Long, ChapterLanguage> chapterDefaultLangMap, Map<Long, SceneLanguage> sceneDefaultLangMap,
			Map<Long, List<String>> chaptersToChapterPlatformsMap, Map<Long, List<String>> scenesToScenePlatformsMap,
			String platform, Configuration config) throws JsonParseException, JsonMappingException, IOException,
			NumberFormatException, ConfigurationException {
		long sTime = System.currentTimeMillis();
		log.logMessage("Replacing MultiLanguage Data - Start");
		MetadataDTO metadataWithLang = new MetadataDTO();

		/* Start - AVS 4049 */

		metadataWithLang.setObjectType(metadataDTO.getObjectType());
		metadataWithLang.setObjectSubtype(metadataDTO.getObjectSubtype());
		metadataWithLang.setContentId(metadataDTO.getContentId());
		metadataWithLang.setExternalId(metadataDTO.getExternalId());
		metadataWithLang.setDuration(metadataDTO.getDuration());
		metadataWithLang.setContractStartDate(metadataDTO.getContractStartDate());
		metadataWithLang.setContractEndDate(metadataDTO.getContractEndDate());
		metadataWithLang.setStartTime(metadataDTO.getStartTime());
		metadataWithLang.setEndTime(metadataDTO.getEndTime());
		metadataWithLang.setStarRating(metadataDTO.getStarRating());
		metadataWithLang.setPcExtendedRatings(metadataDTO.getPcExtendedRatings());
		metadataWithLang.setPcLevelVod(metadataDTO.getPcLevelVod());
		metadataWithLang.setPcLevel(metadataDTO.getPcLevel());
		metadataWithLang.setIsParent(metadataDTO.getIsParent());
		metadataWithLang.setPictureUrl(metadataDTO.getPictureUrl());
		metadataWithLang.setPrimaryCategoryId(metadataDTO.getPrimaryCategoryId());
		metadataWithLang.setBlacklistDeviceTypes(metadataDTO.getBlacklistDeviceTypes());

		/*
		 * Also Setting multilang fields, will be overriden with multilang value only if
		 * not empty
		 */
		metadataWithLang.setTitle(metadataDTO.getTitle());
		metadataWithLang.setEpisodeTitle(metadataDTO.getEpisodeTitle());

		/* End - AVS 4049 */

		metadataWithLang.setEpisodeNumber(metadataDTO.getEpisodeNumber());

		metadataWithLang.setSeason(metadataDTO.getSeason());
		metadataWithLang.setSeriesId(metadataDTO.getSeriesId());
		metadataWithLang.setYear(metadataDTO.getYear());
		metadataWithLang.setDecade(metadataDTO.getDecade());
		metadataWithLang.setGenres(metadataDTO.getGenres());
		metadataWithLang.setActors(metadataDTO.getActors());
		metadataWithLang.setDirectors(metadataDTO.getDirectors());
		metadataWithLang.setAnchors(metadataDTO.getAnchors());
		metadataWithLang.setAuthors(metadataDTO.getAuthors());
		metadataWithLang.setFilter(metadataDTO.getFilter());
		metadataWithLang.setIsEncrypted(metadataDTO.getIsEncrypted());
		metadataWithLang.setIsLatest(metadataDTO.getIsLatest());
		metadataWithLang.setIsOnAir(metadataDTO.getIsOnAir());
		metadataWithLang.setIsPopularEpisode(metadataDTO.getIsPopularEpisode());
		metadataWithLang.setIsGeoBlocked(metadataDTO.getIsGeoBlocked());
		metadataWithLang.setIsSurroundSound(metadataDTO.getIsSurroundSound());
		metadataWithLang.setIsADVAllowed(metadataDTO.getIsADVAllowed());
		metadataWithLang.setSearchKeywords(metadataDTO.getSearchKeywords());
		metadataWithLang.setTitleBrief(metadataDTO.getTitleBrief());
		metadataWithLang.setProgramReferenceName(metadataDTO.getProgramReferenceName());
		metadataWithLang.setLastBroadcastDate(metadataDTO.getLastBroadcastDate());
		metadataWithLang.setBroadcastChannelId(metadataDTO.getBroadcastChannelId());
		metadataWithLang.setBroadcastChannelName(metadataDTO.getBroadcastChannelName());
		metadataWithLang.setOriginalAirDate(metadataDTO.getOriginalAirDate());
		metadataWithLang.setAvailableAlso(getAvailableAlso(metadataDTO.getAvailableAlso(), platform));
		metadataWithLang.setContentProvider(metadataDTO.getContentProvider());
		metadataWithLang.setAdvTags(metadataDTO.getAdvTags());
		metadataWithLang.setRatingType(metadataDTO.getRatingType());
		metadataWithLang.setAssociatedWebSiteUrl(metadataDTO.getAssociatedWebSiteUrl());
		metadataWithLang.setInfoPage(metadataDTO.getInfoPage());
		metadataWithLang.setAvailableLanguages(metadataDTO.getAvailableLanguages());
		metadataWithLang.setMetadataLanguage(metadataDTO.getMetadataLanguage());
		metadataWithLang.setExtendedMetadata(metadataDTO.getExtendedMetadata());
		/* Start- AVS-5833 */
		metadataWithLang.setAdditionalData(metadataDTO.getAdditionalData());
		/* End - AVS-5833 */
		metadataWithLang.setProgramCategory(metadataDTO.getProgramCategory());

		/*
		 * Also Setting multilang fields, will be overriden with multilang value only if
		 * not empty
		 */
		metadataWithLang.setShortDescription(metadataDTO.getShortDescription());
		metadataWithLang.setLongDescription(metadataDTO.getLongDescription());
		metadataWithLang.setCountry(metadataDTO.getCountry());
		metadataWithLang.setGenres(metadataDTO.getGenres());
		metadataWithLang.setSearchKeywords(metadataDTO.getSearchKeywords());
		metadataWithLang.setTitleBrief(metadataDTO.getTitleBrief());
		metadataWithLang.setIsTrickPlayEnabled(metadataDTO.getIsTrickPlayEnabled());
		metadataWithLang.setIsSkipJumpEnabled(metadataDTO.getIsSkipJumpEnabled());
		/* Start- AVS-13824 */
		metadataWithLang.setIsNotAvailableOutOfHome(metadataDTO.getIsNotAvailableOutOfHome());
		/* End - AVS-13824 */
		Integer comingSoonDays = Integer.parseInt(config.getConstants().getValue(CacheConstants.COMING_SOON_TIME_WINDOW));
		Integer leavingSoonDays = Integer.parseInt(config.getConstants().getValue(CacheConstants.LEAVING_SOON_TIME_WINDOW));

		Long startDate = metadataDTO.getContractStartDate();
		Long endDate = metadataDTO.getContractEndDate();
		metadataWithLang.setComingSoon((startDate > new Date().getTime()
				&& startDate <= new Date().getTime() + TimeUnit.DAYS.toMillis(comingSoonDays)) ? true : false);
		metadataWithLang.setLeavingSoon((endDate > new Date().getTime()
				&& endDate <= new Date().getTime() + TimeUnit.DAYS.toMillis(leavingSoonDays)) ? true : false);
		metadataWithLang.setRecentlyAdded((startDate < new Date().getTime() 
				&& new Date().getTime()-startDate <= TimeUnit.DAYS.toMillis(1))?true:false);
		metadataWithLang.setStreamPolicies(metadataDTO.getStreamPolicies());
		/* replacing MultiLang Data in Metadata */
		MultilangMetadata assetMultilangMetadata = null;
		Boolean useDefault = false;
		if (assetMultiLangMap != null) {
			assetMultilangMetadata = assetMultiLangMap.get(langCode);
			if (assetMultilangMetadata == null) {
				useDefault = true;}
		} else {
			useDefault = true;
		}

		if (useDefault) {
			if (assetDefaultMultiLangMap != null) {
				assetMultilangMetadata = assetDefaultMultiLangMap.get(languageCache.getDefaultLanguage());
			}

		}

		if (null != assetMultilangMetadata) {
			if (!DcqVodIngestorUtils.isEmpty(assetMultilangMetadata.getShortDescription())) {
				metadataWithLang.setShortDescription(assetMultilangMetadata.getShortDescription());
			}
			if (!DcqVodIngestorUtils.isEmpty(assetMultilangMetadata.getLongDescription())) {
				metadataWithLang.setLongDescription(assetMultilangMetadata.getLongDescription());
			}
			if (null != assetMultilangMetadata.getCountryList()
					&& null != assetMultilangMetadata.getCountryList().getCountry()) {
				metadataWithLang
						.setCountry(assetMultilangMetadata.getCountryList().getCountry().toArray(new String[0]));
			}
			if (null != assetMultilangMetadata.getGenreList()
					&& null != assetMultilangMetadata.getGenreList().getGenreItem()) {
				metadataWithLang.setGenres(assetMultilangMetadata.getGenreList().getGenreItem().toArray(new String[0]));
			}
			if (null != assetMultilangMetadata.getSearchKeywordList()
					&& null != assetMultilangMetadata.getSearchKeywordList().getKeyword()) {
				metadataWithLang.setSearchKeywords(
						assetMultilangMetadata.getSearchKeywordList().getKeyword().toArray(new String[0]));
			}
			if (!DcqVodIngestorUtils.isEmpty(assetMultilangMetadata.getTitleBrief())) {
				metadataWithLang.setTitleBrief(assetMultilangMetadata.getTitleBrief());
			}

			if (!DcqVodIngestorUtils.isEmpty(assetMultilangMetadata.getTitle())) {
				metadataWithLang.setTitle(assetMultilangMetadata.getTitle());
			}
			if (!DcqVodIngestorUtils.isEmpty(assetMultilangMetadata.getEpisodeTitle())) {
				metadataWithLang.setEpisodeTitle(assetMultilangMetadata.getEpisodeTitle());
			}

			if (useDefault) {
				metadataWithLang.setMetadataLanguage(languageCache.getDefaultLanguage());
			} else {
				// Fix for the defect AVS-20616
				metadataWithLang.setMetadataLanguage(assetMultilangMetadata.getLangCode());
			}
		}

		Boolean useEmfDefault = false;
		Map<String, String> assetEmfMap = null;
		if (assetEmfAttrMap != null && null != assetEmfAttrMap.get(langCode)) {
			List<EMFAttributesDTO> emfAttributeList = assetEmfAttrMap.get(langCode);
			assetEmfMap = new HashMap<String, String>();
			for (EMFAttributesDTO emfAttributesDTO : emfAttributeList) {
				assetEmfMap.put(emfAttributesDTO.getEmfName(), emfAttributesDTO.getEmfValue());
			}
		} else {
			useEmfDefault = true;
		}

		if (useEmfDefault) {
			if (assetEmfAttrDefaultMap != null && null != assetEmfAttrDefaultMap.get(languageCache.getDefaultLanguage())) {
				List<EMFAttributesDTO> emfAttributeDefaultList = assetEmfAttrMap.get(langCode);
				assetEmfMap = new HashMap<String, String>();
				for (EMFAttributesDTO emfAttributesDTO : emfAttributeDefaultList) {
					assetEmfMap.put(emfAttributesDTO.getEmfName(), emfAttributesDTO.getEmfValue());
				}
			}

		}
		metadataWithLang.setEmfAttributes(assetEmfMap);

		// Chapters with multiLanguage

		List<ChaptersDTO> chaptersDTOsWithLang = new ArrayList<ChaptersDTO>();
		if (null != metadataDTO.getChapters() && metadataDTO.getChapters().size() > 0) {
			for (int i = 0; i < metadataDTO.getChapters().size(); i++) {
				ChaptersDTO chaptersDTO = metadataDTO.getChapters().get(i);
				if (null != chaptersToChapterPlatformsMap
						&& null != chaptersToChapterPlatformsMap.get(chaptersDTO.getChapterId())
						&& chaptersToChapterPlatformsMap.get(chaptersDTO.getChapterId()).contains(platform)) {
					ChaptersDTO chaptersDTOWithLang = new ChaptersDTO();
					chaptersDTOWithLang.setChapterId(chaptersDTO.getChapterId());
					chaptersDTOWithLang.setEndTime(chaptersDTO.getEndTime());
					chaptersDTOWithLang.setStartTime(chaptersDTO.getStartTime());
					chaptersDTOWithLang.setBriefTitle(chaptersDTO.getBriefTitle());
					chaptersDTOWithLang.setOrderId(chaptersDTO.getOrderId());
					chaptersDTOWithLang.setMetadataLanguage(chaptersDTO.getMetadataLanguage());

					Map<String, ChapterLanguage> chapterMultiLangMap = chapterMap.get(chaptersDTO.getChapterId());
					ChapterLanguage chapterLanguage = null;
					Boolean useDefaultLang = false;
					if (null != chapterMultiLangMap) {
						chapterLanguage = chapterMultiLangMap.get(langCode);
						if (chapterLanguage == null) {
							useDefaultLang = true;}
					} else {
						useDefaultLang = true;
					}

					if (useDefaultLang) {
						if (null != chapterDefaultLangMap) {
							chapterLanguage = chapterDefaultLangMap.get(chaptersDTO.getChapterId());

						}
					}
					if (null != chapterLanguage) {
						if (!DcqVodIngestorUtils.isEmpty(chapterLanguage.getBriefTitle())) {
							chaptersDTOWithLang.setBriefTitle(chapterLanguage.getBriefTitle());
						}
						if (!DcqVodIngestorUtils.isEmpty(chapterLanguage.getOrderId())) {
							chaptersDTOWithLang.setOrderId(chapterLanguage.getOrderId());
						}
						if (useDefaultLang) {
							chaptersDTOWithLang.setMetadataLanguage(languageCache.getDefaultLanguage());
						} else {
							chaptersDTOWithLang.setMetadataLanguage(chapterLanguage.getMetadataLanguage());
						}
					}
					chaptersDTOsWithLang.add(chaptersDTOWithLang);
				}
			}
			metadataWithLang.setChapters(chaptersDTOsWithLang);// AVS-8247
		}

		// metadataWithLang.setChapters(chaptersDTOsWithLang); //AVS-8247

		// Scenes with multiLanguage
		List<ScenesDTO> scenesDTOsWithLang = new ArrayList<ScenesDTO>();
		if (null != metadataDTO.getScenes() && metadataDTO.getScenes().size() > 0) {
			for (int i = 0; i < metadataDTO.getScenes().size(); i++) {
				ScenesDTO scenesDTO = metadataDTO.getScenes().get(i);
				if (null != scenesToScenePlatformsMap && null != scenesToScenePlatformsMap.get(scenesDTO.getSceneId())
						&& scenesToScenePlatformsMap.get(scenesDTO.getSceneId()).contains(platform)) {
					ScenesDTO scenesDTOWithLang = new ScenesDTO();
					scenesDTOWithLang.setSceneId(scenesDTO.getSceneId());
					scenesDTOWithLang.setStartTime(scenesDTO.getStartTime());
					scenesDTOWithLang.setEndTime(scenesDTO.getEndTime());
					scenesDTOWithLang.setTitle(scenesDTO.getTitle());
					scenesDTOWithLang.setBriefTitle(scenesDTO.getBriefTitle());
					scenesDTOWithLang.setKeywords(scenesDTO.getKeywords());
					scenesDTOWithLang.setGenre(scenesDTO.getGenre());
					scenesDTOWithLang.setOrderId(scenesDTO.getOrderId());
					scenesDTOWithLang.setAdvTag(scenesDTO.getAdvTag());
					scenesDTOWithLang.setEvent(scenesDTO.getEvent());
					scenesDTOWithLang.setAction(scenesDTO.getAction());
					scenesDTOWithLang.setLocation(scenesDTO.getLocation());
					scenesDTOWithLang.setExtendedLocation(scenesDTO.getExtendedLocation());
					scenesDTOWithLang.setCharacters(scenesDTO.getCharacters());
					scenesDTOWithLang.setActors(scenesDTO.getActors());
					scenesDTOWithLang.setCompany(scenesDTO.getCompany());
					scenesDTOWithLang.setAudio(scenesDTO.getAudio());
					scenesDTOWithLang.setMusicians(scenesDTO.getMusicians());
					scenesDTOWithLang.setMetadataLanguage(scenesDTO.getMetadataLanguage());
					scenesDTOWithLang.setExtendedMetadata(scenesDTO.getExtendedMetadata());
					scenesDTOWithLang.setEmfAttributes(scenesDTO.getEmfAttributes());
					Map<String, SceneLanguage> sceneMultiLangMap = sceneMap.get(scenesDTO.getSceneId());
					SceneLanguage sceneLanguage = null;
					Boolean useDefaultLangFlag = false;
					if (null != sceneMultiLangMap) {
						sceneLanguage = sceneMultiLangMap.get(langCode);
						if (sceneLanguage == null) {
							useDefaultLangFlag = true;}
					} else {
						useDefaultLangFlag = true;
					}
					if (useDefaultLangFlag) {
						if (null != sceneDefaultLangMap) {
							sceneLanguage = sceneDefaultLangMap.get(scenesDTO.getSceneId());
						}
					}
					if (null != sceneLanguage) {
						if (!DcqVodIngestorUtils.isEmpty(sceneLanguage.getTitle())) {
							scenesDTOWithLang.setTitle(sceneLanguage.getTitle());
						}
						if (!DcqVodIngestorUtils.isEmpty(sceneLanguage.getBriefTitle())) {
							scenesDTOWithLang.setBriefTitle(sceneLanguage.getBriefTitle());
						}
						if (!DcqVodIngestorUtils.isEmpty(sceneLanguage.getKeywords())) {
							scenesDTOWithLang.setKeywords(sceneLanguage.getKeywords());
						}
						if (!DcqVodIngestorUtils.isEmpty(sceneLanguage.getGenre())) {
							scenesDTOWithLang.setGenre(sceneLanguage.getGenre());
						}
						if (!DcqVodIngestorUtils.isEmpty(sceneLanguage.getOrderId())) {
							scenesDTOWithLang.setOrderId(sceneLanguage.getOrderId());
						}
						if (!DcqVodIngestorUtils.isEmpty(sceneLanguage.getAdTag())) {
							scenesDTOWithLang.setAdvTag(sceneLanguage.getAdTag());
						}
						if (!DcqVodIngestorUtils.isEmpty(sceneLanguage.getEvent())) {
							scenesDTOWithLang.setEvent(sceneLanguage.getEvent());
						}
						if (!DcqVodIngestorUtils.isEmpty(sceneLanguage.getAction())) {
							scenesDTOWithLang.setAction(sceneLanguage.getAction());
						}
						if (!DcqVodIngestorUtils.isEmpty(sceneLanguage.getLocation())) {
							scenesDTOWithLang.setLocation(sceneLanguage.getLocation());
						}
						if (!DcqVodIngestorUtils.isEmpty(sceneLanguage.getExtendedLocation())) {
							scenesDTOWithLang.setExtendedLocation(sceneLanguage.getExtendedLocation());
						}
						if (!DcqVodIngestorUtils.isEmpty(sceneLanguage.getCharacters())) {
							scenesDTOWithLang.setCharacters(sceneLanguage.getCharacters().split("\\|"));
						}
						if (!DcqVodIngestorUtils.isEmpty(sceneLanguage.getActors())) {
							scenesDTOWithLang.setActors(sceneLanguage.getActors().split("\\|"));
						}
						if (!DcqVodIngestorUtils.isEmpty(sceneLanguage.getCompany())) {
							scenesDTOWithLang.setCompany(sceneLanguage.getCompany());
						}
						if (!DcqVodIngestorUtils.isEmpty(sceneLanguage.getAudio())) {
							scenesDTOWithLang.setAudio(sceneLanguage.getAudio());
						}
						if (!DcqVodIngestorUtils.isEmpty(sceneLanguage.getMusicians())) {
							scenesDTOWithLang.setMusicians(sceneLanguage.getMusicians().split("\\|"));
						}
						if (!DcqVodIngestorUtils.isEmpty(sceneLanguage.getExtendedMetadata())) {
							if (JsonUtils.isJSONValid(sceneLanguage.getExtendedMetadata())) {
								scenesDTOWithLang.setExtendedMetadata(
										JsonUtils.parseJson(sceneLanguage.getExtendedMetadata(), Object.class));
							} else {
								scenesDTOWithLang.setExtendedMetadata(sceneLanguage.getExtendedMetadata());
							}
						}
						if (useDefaultLangFlag) {
							scenesDTOWithLang.setMetadataLanguage(languageCache.getDefaultLanguage());
						} else {
							scenesDTOWithLang.setMetadataLanguage(sceneLanguage.getMetadataLanguage());
						}
						Map<String, String> emfMap = new HashMap<String, String>();
						if (sceneLanguage.getEMFAttributeList() != null
								&& sceneLanguage.getEMFAttributeList().getEMFAttribute() != null
								&& sceneLanguage.getEMFAttributeList().getEMFAttribute().size() > 0) {
							for (int j = 0; j < sceneLanguage.getEMFAttributeList().getEMFAttribute().size(); j++) {
								String emfName = sceneLanguage.getEMFAttributeList().getEMFAttribute().get(j).getName();
								String emfValue = sceneLanguage.getEMFAttributeList().getEMFAttribute().get(j)
										.getValue();
								emfMap.put(emfName, emfValue);
							}

						}

						scenesDTOWithLang.setEmfAttributes(emfMap);
					}
					scenesDTOsWithLang.add(scenesDTOWithLang);
				}
			}
			metadataWithLang.setScenes(scenesDTOsWithLang); // AVS-8247
		}
		// metadataWithLang.setScenes(scenesDTOsWithLang);//AVS-8247

		metadataWithLang.setIsCopyProtected(metadataDTO.getIsCopyProtected());
		metadataWithLang.setCopyProtections(metadataDTO.getCopyProtections());

		// AVS-27555
		metadataWithLang.setPcVodLabel(metadataDTO.getPcVodLabel());

		log.logMethodEnd(System.currentTimeMillis() - sTime);
		return metadataWithLang;
	}

	private String[] getAvailableAlso(String[] availableAlso, String platform) {

		List<String> availableAlsoList = new ArrayList<>();
		for (String item : availableAlso) {
			if (!platform.equals(item)) {
				availableAlsoList.add(item);
			}
		}
		return availableAlsoList.toArray(new String[availableAlsoList.size()]);
	}

	private MetadataDTO retrieveMetadata(Integer contentId, Asset asset, List<LanguagesDTO> availableLangs,
			Set<String> platformNames, Long primaryCatId, Configuration config)
			throws JsonParseException, JsonMappingException, IOException, ConfigurationException {
		long sTime = System.currentTimeMillis();
		log.logMessage("Constructing the Metadata - Start");
		MetadataDTO metadataDTO = new MetadataDTO();
		/* Start - AVS-4049 */
		metadataDTO.setTitle(asset.getTitle());
		metadataDTO.setEpisodeTitle(asset.getEpisodeTitle());
		metadataDTO.setObjectType(asset.getObjectType());
		metadataDTO.setObjectSubtype(asset.getObjectSubtype());
		metadataDTO.setContentId(contentId.longValue());
		metadataDTO.setExternalId(asset.getHouseId());
		metadataDTO.setIsSkipJumpEnabled(DcqVodIngestorUtils.getBooleanValue(asset.getIsSkipJumpEnabled()));
		metadataDTO.setIsTrickPlayEnabled(DcqVodIngestorUtils.getBooleanValue(asset.getIsTrickPlayEnabled()));
		metadataDTO.setDuration((long) asset.getDuration());
		/* AVS-13824 Start */
		if (DcqVodIngestorConstants.SHORT_YES.equalsIgnoreCase(asset.getIsNotAvailableOutOfHome())) {
			metadataDTO.setIsNotAvailableOutOfHome(Boolean.TRUE);
		} else if (DcqVodIngestorConstants.SHORT_NO.equalsIgnoreCase(asset.getIsNotAvailableOutOfHome())) {
			metadataDTO.setIsNotAvailableOutOfHome(Boolean.FALSE);
		} else {
			metadataDTO.setIsNotAvailableOutOfHome(null);
		}
		/* AVS-13824 End */
		long longStartDate = DcqVodIngestorUtils.convertToDate(asset.getStartDateTime()).getTime();
		metadataDTO.setContractStartDate(longStartDate);
		metadataDTO.setStartTime(longStartDate);
		long longEndDate = DcqVodIngestorUtils.convertToDate(asset.getEndDateTime()).getTime();
		metadataDTO.setContractEndDate(longEndDate);
		metadataDTO.setEndTime(longEndDate);
		metadataDTO.setStarRating(asset.getStarRating() == null ? null : asset.getStarRating().floatValue());
		if (asset.getExtensions() != null && asset.getExtensions().getExtendedRatingList() != null
				&& asset.getExtensions().getExtendedRatingList().getExtendedRating().size() > 0) {
			List<String> pcRating = new ArrayList<String>();
			for (int i = 0; i < asset.getExtensions().getExtendedRatingList().getExtendedRating().size(); i++) {
				String rating = asset.getExtensions().getExtendedRatingList().getExtendedRating().get(i);
				pcRating.add(rating);
			}
			metadataDTO.setPcExtendedRatings(pcRating.toArray(new String[0]));
		}
		String pcLevel = contentRepository.findPcLevelByContentId(contentId);
		metadataDTO.setPcLevelVod(pcLevel);
		metadataDTO.setPcLevel(pcLevel);
		metadataDTO.setIsParent(DcqVodIngestorUtils.getBooleanValue(asset.getIsParentObject()));

		// Fix for AVS - 6909
		metadataDTO.setPrimaryCategoryId(primaryCatId);

		if (asset.getBlackListDeviceTypes() != null && asset.getBlackListDeviceTypes().getDeviceType() != null) {
			metadataDTO.setBlacklistDeviceTypes(asset.getBlackListDeviceTypes().getDeviceType().toArray(new String[0]));}

		/* End - AVS-4049 */

		metadataDTO.setShortDescription(asset.getSummaryShort());
		metadataDTO.setLongDescription(asset.getSummaryLong());
		metadataDTO.setEpisodeNumber(asset.getEpisodeId());
		metadataDTO.setSeason(asset.getSeason());
		if (asset.getSeries() != null) { // initialization to empty string is removed for fixing AVS-6019 issue
			metadataDTO.setSeriesId(asset.getSeries());
		}
		metadataDTO.setMetadataLanguage(asset.getLanguage());
		if (asset.getCountriesOfOrigin() != null && asset.getCountriesOfOrigin().getCountryOfOrigin() != null) {
			metadataDTO.setCountry(asset.getCountriesOfOrigin().getCountryOfOrigin().toArray(new String[0]));}
		if (DcqVodIngestorUtils.getYear(asset.getYear()) != null) { // initialization to empty string is removed
			metadataDTO.setYear(DcqVodIngestorUtils.getYear(asset.getYear()));
			if (!metadataDTO.getYear().isEmpty()) {
				metadataDTO.setDecade(metadataDTO.getYear().substring(0, metadataDTO.getYear().length() - 1));
			}
		}
		if (asset.getGenreList() != null && asset.getGenreList().getGenreItem() != null) {
			metadataDTO.setGenres(asset.getGenreList().getGenreItem().toArray(new String[0]));}
		if (asset.getActors() != null && asset.getActors().getActor() != null) {
			metadataDTO.setActors(asset.getActors().getActor().toArray(new String[0]));}
		if (asset.getDirectors() != null && asset.getDirectors().getDirector() != null) {
			metadataDTO.setDirectors(asset.getDirectors().getDirector().toArray(new String[0]));}
		if (asset.getProducers() != null && asset.getProducers().getProducer() != null) {
			metadataDTO.setAuthors(asset.getProducers().getProducer().toArray(new String[0]));}
		if (asset.getAnchors() != null && asset.getAnchors().getAnchor() != null) {
			metadataDTO.setAnchors(asset.getAnchors().getAnchor().toArray(new String[0]));}

		metadataDTO.setFilter("0");
		if (null != asset.getExtensions().getIsAdult()
				&& BooleanConverter.getBooleanValue(asset.getExtensions().getIsAdult().toString())) {
			metadataDTO.setFilter("1");}

		metadataDTO.setIsEncrypted(DcqVodIngestorUtils.getBooleanValue(asset.getIsEncrypted()));
		metadataDTO.setIsLatest(DcqVodIngestorUtils.getBooleanValue(asset.getLatest()));
		metadataDTO.setIsOnAir(DcqVodIngestorUtils.getBooleanValue(asset.getOnAir()));
		metadataDTO.setIsPopularEpisode(DcqVodIngestorUtils.getBooleanValue(asset.getPopularEpisode()));
		metadataDTO.setIsGeoBlocked(asset.getGeoBlocking() == null ? false
				: DcqVodIngestorUtils.getBooleanValue(asset.getGeoBlocking().getDefault()));
		metadataDTO.setIsSurroundSound(DcqVodIngestorUtils.getBooleanValue(asset.getIsSurroundSound()));
		if (null != asset.getIsDisAllowedAdv()) {
			metadataDTO.setIsADVAllowed(!DcqVodIngestorUtils.getBooleanValue(asset.getIsDisAllowedAdv()));
		}
		if (asset.getSearchKeywordList() != null && asset.getSearchKeywordList().getKeyword() != null) {
			metadataDTO.setSearchKeywords(asset.getSearchKeywordList().getKeyword().toArray(new String[0]));}
		metadataDTO.setTitleBrief(asset.getTitleBrief());
		metadataDTO.setProgramReferenceName(asset.getProgramReferenceName());
		metadataDTO.setLastBroadcastDate(DcqVodIngestorUtils.convertToLong(asset.getLastBroadcastDate()));
		if (null != asset.getLastBroadcastChannel()) {
			String broadcastChannelName = channelRepository
					.retreiveChannelNameByChannelId(asset.getLastBroadcastChannel().intValue());
			metadataDTO.setBroadcastChannelId(asset.getLastBroadcastChannel());
			metadataDTO.setBroadcastChannelName(broadcastChannelName);
		}
		Long originalAirDate = DcqVodIngestorUtils.convertToLong(asset.getOriginalAirDate());
		metadataDTO.setOriginalAirDate(originalAirDate);
		metadataDTO.setAvailableAlso(platformNames.toArray(new String[0]));
		metadataDTO.setContentProvider(asset.getContentProvider());
		metadataDTO.setAdvTags(asset.getAdvTags());
		metadataDTO.setRatingType(asset.getRatingType());
		metadataDTO.setAssociatedWebSiteUrl(asset.getAssociatedWebSiteUrl());
		metadataDTO.setInfoPage(asset.getInfoPage());
		if (availableLangs != null && !availableLangs.isEmpty()) { // AVS-8247
			metadataDTO.setAvailableLanguages(availableLangs);
		}

		List<ChaptersDTO> chaptersDTOList = null;
		if (asset.getChapterList() != null && asset.getChapterList().getChapter() != null
				&& asset.getChapterList().getChapter().size() > 0) {
			chaptersDTOList = new ArrayList<ChaptersDTO>();
			for (int i = 0; i < asset.getChapterList().getChapter().size(); i++) {
				ChaptersDTO chaptersDTO = new ChaptersDTO();
				Chapter assetChapter = asset.getChapterList().getChapter().get(i);
				chaptersDTO.setChapterId(assetChapter.getChapterId());
				chaptersDTO
						.setStartTime(DcqVodIngestorUtils.convertToLongChapterSceneTimes(assetChapter.getStartTime()));
				chaptersDTO.setEndTime(DcqVodIngestorUtils.convertToLongChapterSceneTimes(assetChapter.getEndTime()));
				chaptersDTO.setBriefTitle(assetChapter.getBriefTitle());
				chaptersDTO.setOrderId(assetChapter.getOrderId());
				chaptersDTO.setMetadataLanguage(assetChapter.getMetadataLanguage());
				chaptersDTOList.add(chaptersDTO);
			}
			metadataDTO.setChapters(chaptersDTOList);
		}

		List<ScenesDTO> ScenesDTOList = null;
		if (asset.getSceneList() != null && asset.getSceneList().getScene() != null
				&& asset.getSceneList().getScene().size() > 0) {
			ScenesDTOList = new ArrayList<ScenesDTO>();
			for (int i = 0; i < asset.getSceneList().getScene().size(); i++) {
				ScenesDTO scenesDTO = new ScenesDTO();
				Scene scene = asset.getSceneList().getScene().get(i);
				scenesDTO.setSceneId(scene.getSceneId());
				scenesDTO.setStartTime(DcqVodIngestorUtils.convertToLongChapterSceneTimes(scene.getStartTime()));
				scenesDTO.setEndTime(DcqVodIngestorUtils.convertToLongChapterSceneTimes(scene.getEndTime()));
				scenesDTO.setTitle(scene.getTitle());
				scenesDTO.setBriefTitle(scene.getBriefTitle());
				scenesDTO.setKeywords(scene.getKeywords());
				scenesDTO.setGenre(scene.getGenre());
				scenesDTO.setOrderId(scene.getOrderId());
				scenesDTO.setAdvTag(scene.getAdTag());
				scenesDTO.setEvent(scene.getEvent());
				scenesDTO.setAction(scene.getAction());
				scenesDTO.setLocation(scene.getLocation());
				scenesDTO.setExtendedLocation(scene.getExtendedLocation());
				scenesDTO.setCharacters(scene.getCharacters() == null ? null : scene.getCharacters().split("\\|"));
				scenesDTO.setActors(scene.getActors() == null ? null : scene.getActors().split("\\|"));
				scenesDTO.setCompany(scene.getCompany());
				scenesDTO.setAudio(scene.getAudio());
				scenesDTO.setMusicians(scene.getMusicians() == null ? null : scene.getMusicians().split("\\|"));
				scenesDTO.setMetadataLanguage(scene.getMetadataLanguage());
				Map<String, String> map = new HashMap<String, String>();
				if (asset.getSceneList().getScene().get(i).getEMFAttributeList() != null
						&& asset.getSceneList().getScene().get(i).getEMFAttributeList().getEMFAttribute().size() > 0) {
					for (int k = 0; k < asset.getSceneList().getScene().get(i).getEMFAttributeList().getEMFAttribute()
							.size(); k++) {
						EMFAttribute emf = asset.getSceneList().getScene().get(i).getEMFAttributeList()
								.getEMFAttribute().get(k);
						map.put(emf.getName(), emf.getValue());
					}
				}
				scenesDTO.setEmfAttributes(map);
				if (scene.getExtendedMetadata() != null && !scene.getExtendedMetadata().trim().isEmpty()) {
					if (JsonUtils.isJSONValid(scene.getExtendedMetadata())) {
						scenesDTO.setExtendedMetadata(JsonUtils.parseJson(scene.getExtendedMetadata(), Object.class));
					} else {
						scenesDTO.setExtendedMetadata(scene.getExtendedMetadata());
					}
				}
				ScenesDTOList.add(scenesDTO);
			}
			metadataDTO.setScenes(ScenesDTOList);
		}

		/*
		 * Note - Not Setting Emf Attributes to metadataDTO as they are Language
		 * Specific.
		 */
		metadataDTO.setEmfAttributes(null);
		if (asset.getExtendedMetadata() != null && !asset.getExtendedMetadata().trim().trim().isEmpty()) {
			if (JsonUtils.isJSONValid(asset.getExtendedMetadata())) {
				metadataDTO.setExtendedMetadata(JsonUtils.parseJson(asset.getExtendedMetadata(), Object.class));
			} else {
				metadataDTO.setExtendedMetadata(asset.getExtendedMetadata());
			}
		}
		/* Start- AVS-5833 */

		if (asset.getAdditionalData() != null && !(asset.getAdditionalData().isEmpty())) {
			metadataDTO.setAdditionalData(asset.getAdditionalData());
		}

		/* End- AVS-5833 */
		metadataDTO.setProgramCategory(asset.getProgramCategory());

		if (asset.getStreamPolicies() != null && asset.getStreamPolicies().getPolicyId() != null
				&& !asset.getStreamPolicies().getPolicyId().isEmpty()) {
			List<String> streamPolicies = new ArrayList<String>();
			for (int i = 0; i < asset.getStreamPolicies().getPolicyId().size(); i++) {
				String policyId = asset.getStreamPolicies().getPolicyId().get(i);
				streamPolicies.add(policyId);
			}
			metadataDTO.setStreamPolicies(streamPolicies.toArray(new String[0]));
		}

		// AVS-24272 start
		if (!copyProtectionCache.isCopyProtectionInPanic(config) && null == asset.getIsCopyProtected()) {
			log.logMessage("Applying default Copy Protections");
			List<CopyProtectionDTO> copyProtectionList = new ArrayList<CopyProtectionDTO>();
			CopyProtectionDTO esCpProtectionDTO = null;
			CopyProtectionConfiguration masterCpSchemeList = copyProtectionCache.getCopyProtectionSchemes();
			// Get the vod default copy protections
			if (null != masterCpSchemeList && masterCpSchemeList.getCopyProtections() != null) {

				for (CopyProtectionScheme mstrCps : masterCpSchemeList.getCopyProtections()) {
					esCpProtectionDTO = new CopyProtectionDTO();
					if (!Utilities.isEmpty(mstrCps.getVodDefaultOption())) {
						esCpProtectionDTO.setSecurityCode(mstrCps.getSecurityCode());
						esCpProtectionDTO.setSecurityOption(mstrCps.getVodDefaultOption());
						copyProtectionList.add(esCpProtectionDTO);
					}
				}
				if (null != copyProtectionList && !copyProtectionList.isEmpty()) {
					metadataDTO.setIsCopyProtected(Boolean.TRUE);
					metadataDTO.setCopyProtections(copyProtectionList);
					log.logMessage("Default copyprotections are applied");
				} else {
					metadataDTO.setIsCopyProtected(Boolean.FALSE);
				}

			} else {
				metadataDTO.setIsCopyProtected(Boolean.FALSE);
			}
		} else if (copyProtectionCache.isCopyProtectionInPanic(config)
				|| asset.getIsCopyProtected().value().equals(DcqVodIngestorConstants.NO)) {
			metadataDTO.setIsCopyProtected(Boolean.FALSE);
		} else if (asset.getIsCopyProtected().value().equals(DcqVodIngestorConstants.YES)) {
			log.logMessage("Applying content Copy Protections");
			List<CopyProtectionDTO> copyProtections = null;
			CopyProtectionDTO copyProtectionDTO = null;
			if (null != asset.getCopyProtectionList().getCopyProtection()) {
				metadataDTO.setIsCopyProtected(Boolean.TRUE);
				copyProtections = new ArrayList<>();
				for (CopyProtection copyProtection : asset.getCopyProtectionList().getCopyProtection()) {
					copyProtectionDTO = new CopyProtectionDTO();
					copyProtectionDTO.setSecurityCode(copyProtection.getSecurityCode());
					copyProtectionDTO.setSecurityOption(copyProtection.getSecurityOption());
					copyProtections.add(copyProtectionDTO);
				}

				metadataDTO.setCopyProtections(copyProtections);
			}
		}
		// AVS-24272 end

		// AVS-27555
		metadataDTO.setPcVodLabel(asset.getParentalRating());
		log.logMethodEnd(System.currentTimeMillis() - sTime);
		return metadataDTO;
	}

	private List<ParentsDTO> retrieveParentsForContainers(Integer contentId) {
		long sTime = System.currentTimeMillis();
		log.logMessage("Constructing the Parents For Containers - Start");
		List<ParentsDTO> parentsDTOs = null;
		List<Object[]> objects = contentLinkingRepository.retrieveParentContentId(contentId);
		if (null != objects && !objects.isEmpty()) {
			parentsDTOs = new ArrayList<ParentsDTO>();
			for (int i = 0; i < objects.size(); i++) {
				Object[] objArr = objects.get(i);
				ParentsDTO parentsDTO = new ParentsDTO();
				parentsDTO.setContentId(((Integer) objArr[0]).longValue());
				parentsDTO.setOrderId(((Integer) objArr[1]).longValue());
				List<Object[]> typeSubTypeArr = extendedContentAttributesRepository.retrieveTypeSubTypeByContentId((Integer) objArr[0]);
				if (typeSubTypeArr != null) { // initialization to empty string is removed
					if (typeSubTypeArr.get(0) != null) {
						parentsDTO.setObjectType( typeSubTypeArr.get(0)[0].toString());}
					if (typeSubTypeArr.get(0) != null) {
						parentsDTO.setObjectSubtype(typeSubTypeArr.get(0)[1].toString());}
				}
				parentsDTOs.add(parentsDTO);
			}
		}
		log.logMethodEnd(System.currentTimeMillis() - sTime);
		return parentsDTOs;
	}

	private List<ChildrenDTO> retrieveChildrenList(Asset asset, String platform, List<String> childernSubTypeList) {
		long sTime = System.currentTimeMillis();
		log.logMessage("Constructing the ChildrenList - Start");
		List<ChildrenDTO> childrenDTOsList = null;
		if (null != asset.getContentLinking() && null != asset.getContentLinking().getContentLinkingId()
				&& asset.getContentLinking().getContentLinkingId().size() > 0) {
			childrenDTOsList = new ArrayList<ChildrenDTO>();
			childernSubTypeList = new ArrayList<String>();
			for (int i = 0; i < asset.getContentLinking().getContentLinkingId().size(); i++) {
				ChildrenDTO childrenDTO = new ChildrenDTO();
				long childContentId = asset.getContentLinking().getContentLinkingId().get(i).getValue();
				Integer childOrderId = asset.getContentLinking().getContentLinkingId().get(i).getOrderId();
				String childType = asset.getContentLinking().getContentLinkingId().get(i).getSubtype();
				childrenDTO.setContentId(childContentId);
				childrenDTO.setOrderId(childOrderId.longValue());
				childrenDTO.setType(childType);
				List<VaraintsDTO> varaintsDTOs = new ArrayList<VaraintsDTO>();
				List<Object[]> cpDetailsList = contentPlatformRepository
						.retrieveCpDetailsByContentId(Long.valueOf(childContentId).intValue(), platform);
				if (null != cpDetailsList && !cpDetailsList.isEmpty()) {
					for (int x = 0; x < cpDetailsList.size(); x++) {
						Object[] cpDetailsArr = cpDetailsList.get(x);
						VaraintsDTO varaintsDTO = new VaraintsDTO();
						varaintsDTO.setCpId(
								cpDetailsArr[0] == null ? null : Long.valueOf(String.valueOf(cpDetailsArr[0])));
						varaintsDTO.setPictureUrl(cpDetailsArr[1] == null ? null : (String) cpDetailsArr[1]);
						varaintsDTO.setResolutionType(cpDetailsArr[2] == null ? null : (String) cpDetailsArr[2]);
						varaintsDTO.setTrailerUrl(cpDetailsArr[3] == null ? null : (String) cpDetailsArr[3]);
						varaintsDTO.setVideoUrl(cpDetailsArr[4] == null ? null : (String) cpDetailsArr[4]);
						varaintsDTOs.add(varaintsDTO);
					}
				}
				childrenDTO.setVariants(varaintsDTOs);
				childrenDTOsList.add(childrenDTO);
				childernSubTypeList.add(childType);
			}
		}
		log.logMethodEnd(System.currentTimeMillis() - sTime);
		return childrenDTOsList;
	}

	private List<PlatformVariantsDTO> retrievePlatformVariantList(String platform, List<Integer> publishedCPIdsList,
			Map<Integer, List<AudioLangDetailsDTO>> cpIdAudioLangsMap,
			Map<Integer, List<SubtitleLangDetailsDTO>> cpIdSubTitleLangsMap,
			Map<Integer, List<TechnicalPackageDetailsDTO>> cpIdTpDetailsmap, Map<Integer, String> cpIdVideoTypeMap,
			Map<String, Platform> platformDataMap, List<String> childernSubTypeList,
			Map<Integer, Object[]> pictureAndVideoUrlMap) throws JsonParseException, JsonMappingException, IOException {
		long sTime = System.currentTimeMillis();
		log.logMessage("Constructing the PlatformVariant - Start,platform: {};CpIds: {}", platform, publishedCPIdsList);
		List<PlatformVariantsDTO> platformVariantsDTOs = new ArrayList<PlatformVariantsDTO>();
		for (Integer cpId : publishedCPIdsList) {
			Platform assetPlatform = platformDataMap.get(cpIdVideoTypeMap.get(cpId));
			if (null != assetPlatform) {
				PlatformVariantsDTO platformVariantsDTO = new PlatformVariantsDTO();
				platformVariantsDTO.setCpId(cpId.longValue());

				if (null != assetPlatform.getDownload()) {
					platformVariantsDTO.setDownloadMaxNumber(assetPlatform.getDownload().getNumDWNAvailable());
					platformVariantsDTO.setDownloadMaxDays(assetPlatform.getDownload().getMaxDaysAvailable());
					platformVariantsDTO
							.setDownloadHoursFromFirstPlay(assetPlatform.getDownload().getNumHoursFromFirstPlay());
				}
				platformVariantsDTO.setHasTrailer(calculateHasTrailer(assetPlatform, childernSubTypeList));
				platformVariantsDTO.setSubtitlesLanguages(getSubTitlesForPlatformVariant(cpId, cpIdSubTitleLangsMap));
				platformVariantsDTO.setAudioLanguages(getAudioLanguagesForPlatformVariant(cpId, cpIdAudioLangsMap));

				List<TechnicalPackageDTO> technicalPackageDTOs = getTechnicalPackagesForPlatformVariant(cpId,
						cpIdTpDetailsmap);
				platformVariantsDTO.setTechnicalPackages(technicalPackageDTOs);
				if (assetPlatform.getExtendedMetadata() != null
						&& !assetPlatform.getExtendedMetadata().trim().isEmpty()) {
					if (JsonUtils.isJSONValid(assetPlatform.getExtendedMetadata())) {
						platformVariantsDTO.setExtendedMetadata(
								JsonUtils.parseJson(assetPlatform.getExtendedMetadata(), Object.class));
					} else {
						platformVariantsDTO.setExtendedMetadata(assetPlatform.getExtendedMetadata());
					}
				}

				Object[] cpDetails = pictureAndVideoUrlMap.get(cpId);

				if (cpDetails != null) {
					platformVariantsDTO.setPictureUrl(cpDetails[0] == null ? null : (String) cpDetails[0]);
					platformVariantsDTO.setVideoUrl(cpDetails[1] == null ? null : (String) cpDetails[1]);
					platformVariantsDTO.setTrailerUrl(cpDetails[2] == null ? null : (String) cpDetails[2]);
					platformVariantsDTO.setVideoType(cpDetails[3] == null ? null : (String) cpDetails[3]);
					platformVariantsDTO.setDrmInfo(cpDetails[4] == null ? null : (String) cpDetails[4]);
					platformVariantsDTO.setStreamingType(cpDetails[5] == null ? null : (String) cpDetails[5]);
				}
				log.logMessage("Constructing the PlatformVariant - DETAILS: {}:{}", platformVariantsDTO.getCpId(),
						platformVariantsDTO.getVideoType());
				platformVariantsDTOs.add(platformVariantsDTO);
			}
		}
		log.logMethodEnd(System.currentTimeMillis() - sTime);
		return platformVariantsDTOs;
	}

	private Boolean calculateHasTrailer(Platform assetPlatform, List<String> childernSubTypeList) {
		Boolean hasTrailer = false;
		if (null != assetPlatform.getTrailerUrl() && null != assetPlatform.getTrailerUrl().getValue()) {
			hasTrailer = true;
		}
		if (null != childernSubTypeList && childernSubTypeList.contains(DcqVodIngestorConstants.TRAILER)) {
			hasTrailer = true;
		}
		return hasTrailer;
	}

	private List<SubTitlesLanguagesDTO> getSubTitlesForPlatformVariant(Integer cpId,
			Map<Integer, List<SubtitleLangDetailsDTO>> cpIdSubTitleLangsMap) {
		List<SubTitlesLanguagesDTO> subTitlesLanguagesDTOs = null;
		long sTime = System.currentTimeMillis();
		log.logMessage("Constructing the SubTitles For PlatformVariant - Start");
		if (null != cpIdSubTitleLangsMap) {
			List<SubtitleLangDetailsDTO> dtoList = cpIdSubTitleLangsMap.get(cpId);
			if (null != dtoList && !dtoList.isEmpty()) {
				subTitlesLanguagesDTOs = new ArrayList<SubTitlesLanguagesDTO>();
				for (SubtitleLangDetailsDTO dto : dtoList) {
					SubTitlesLanguagesDTO subTitlesLanguagesDTO = new SubTitlesLanguagesDTO();
					subTitlesLanguagesDTO.setSubtitleId(dto.getSubId());
					subTitlesLanguagesDTO.setSubtitleLanguageName(dto.getLabel());
					subTitlesLanguagesDTOs.add(subTitlesLanguagesDTO);
				}
			}
		}
		log.logMethodEnd(System.currentTimeMillis() - sTime);
		return subTitlesLanguagesDTOs;
	}

	private List<AudioLanguagesDTO> getAudioLanguagesForPlatformVariant(Integer cpId,
			Map<Integer, List<AudioLangDetailsDTO>> cpIdAudioLangsMap) {
		List<AudioLanguagesDTO> audioLanguagesDTOs = null;
		long sTime = System.currentTimeMillis();
		log.logMessage("Constructing the AudioLanguages For PlatformVariant - Start");

		if (null != cpIdAudioLangsMap) {
			List<AudioLangDetailsDTO> dtoList = cpIdAudioLangsMap.get(cpId);
			if (null != dtoList && !dtoList.isEmpty()) {
				audioLanguagesDTOs = new ArrayList<AudioLanguagesDTO>();
				for (AudioLangDetailsDTO dto : dtoList) {
					AudioLanguagesDTO audioLanguagesDTO = new AudioLanguagesDTO();
					audioLanguagesDTO.setAudioId(dto.getLangId());
					audioLanguagesDTO.setAudioLanguageName(dto.getLabel());
					audioLanguagesDTO.setIsPreferred(BooleanConverter.getBooleanValue(dto.getIsPreferred()));
					audioLanguagesDTOs.add(audioLanguagesDTO);
				}
			}
		}
		log.logMethodEnd(System.currentTimeMillis() - sTime);
		return audioLanguagesDTOs;
	}

	private List<TechnicalPackageDTO> getTechnicalPackagesForPlatformVariant(Integer cpId,
			Map<Integer, List<TechnicalPackageDetailsDTO>> cpIdTpDetailsmap) {
		long sTime = System.currentTimeMillis();
		log.logMessage("Constructing the TechnicalPackages For PlatformVariant - Start");
		List<TechnicalPackageDTO> technicalPackageDTOs = null;

		if (null != cpIdTpDetailsmap) {
			List<TechnicalPackageDetailsDTO> tpDetailsDTOList = cpIdTpDetailsmap.get(cpId);
			if (null != tpDetailsDTOList && !tpDetailsDTOList.isEmpty()) {
				technicalPackageDTOs = new ArrayList<TechnicalPackageDTO>();
				for (TechnicalPackageDetailsDTO dto : tpDetailsDTOList) {
					TechnicalPackageDTO technicalPackageDTO = new TechnicalPackageDTO();
					technicalPackageDTO.setPackageId(dto.getPackageId().longValue());
					technicalPackageDTO.setPackageName(dto.getPackageName());
					technicalPackageDTO.setPackageType(dto.getPackageType());
					technicalPackageDTOs.add(technicalPackageDTO);
				}
			}
		}

		log.logMethodEnd(System.currentTimeMillis() - sTime);
		return technicalPackageDTOs;
	}

	
	private List<AdvertisingInfoListDTO> retrieveAdvertisingInfoList(Asset asset) {

		List<AdvertisingInfoListDTO> advertisingInfoListDTO = new ArrayList<AdvertisingInfoListDTO>();
		AdvertisingInfoList advertisementInfoList = asset.getAdvertisingInfoList();
		if (advertisementInfoList != null) {
			List<AdvertisingInfo> advertisingInfoList = advertisementInfoList.getAdvertisingInfo();
			if (advertisingInfoList != null && !advertisingInfoList.isEmpty()) {
				for (AdvertisingInfo advertisingInfo : advertisingInfoList) {
					AdvertisingInfoListDTO advertisingInfoDTO = new AdvertisingInfoListDTO();
					List<CuePointListDTO> cuePointListDTO = new ArrayList<CuePointListDTO>();
					CuePointList cuePointList = advertisingInfo.getCuePointList();
					if (null != cuePointList && !cuePointList.getCuePoint().isEmpty()) { // AVS-17086

						List<CuePoint> cuepoints = cuePointList.getCuePoint();
						if (cuepoints != null && !cuepoints.isEmpty()) {
							for (CuePoint cuepoint : cuepoints) {
								CuePointListDTO cuePointDTO = new CuePointListDTO();
								cuePointDTO.setAdFormat(cuepoint.getAdFormat().value());
								cuePointDTO
										.setContentEndTimePosition(cuepoint.getContentEndTimePosition().doubleValue());
								cuePointDTO.setContentTimePosition(cuepoint.getContentTimePosition().doubleValue());
								cuePointDTO.setCustomSlotId(
										cuepoint.getCustomSlotId() != null ? cuepoint.getCustomSlotId() : "");
								if (cuepoint.getSequence() != null) {
									cuePointDTO.setSequence(cuepoint.getSequence().intValue());
								}
								cuePointDTO.setTimePositionClass(cuepoint.getTimePositionClass() != null
										? cuepoint.getTimePositionClass().value()
										: "");
								if (cuepoint.getTimeToNextAdUnit() != null) {
									cuePointDTO.setTimeToNextAdUnit(cuepoint.getTimeToNextAdUnit().intValue());
								}
								cuePointListDTO.add(cuePointDTO);
							}
						}
					}
					advertisingInfoDTO.setAdvertisingContentId(advertisingInfo.getAdvertisingContentId());
					advertisingInfoDTO.setNetworkID(advertisingInfo.getNetworkId());
					if (!cuePointListDTO.isEmpty()) {
						advertisingInfoDTO.setCuePointList(cuePointListDTO);
					}
					advertisingInfoListDTO.add(advertisingInfoDTO);
				}
			}
		}
		return advertisingInfoListDTO;
	}


	// AVS-13771 stop
	// AVS-12973 start
	private List<PropertiesDTO> getpropertiesList(List<Property> propertyList)
			throws JsonParseException, JsonMappingException, IOException {

		List<PropertiesDTO> PropertiesDTOList = new ArrayList<PropertiesDTO>();
		for (Property property : propertyList) {
			PropertiesDTO propertiesDTO = new PropertiesDTO();
			propertiesDTO.setPropertyName(property.getPropertyName());
			if (property.getExtendedMetadata() != null && !property.getExtendedMetadata().trim().isEmpty()) {
				if (JsonUtils.isJSONValid(property.getExtendedMetadata())) {
					propertiesDTO
							.setExtendedMetadata(JsonUtils.parseJson(property.getExtendedMetadata(), Object.class));
				} else {
					propertiesDTO.setExtendedMetadata(property.getExtendedMetadata());
				}
			}
			// propertiesDTO.setExtendedMetadata(property.getExtendedMetadata());
			PropertiesDTOList.add(propertiesDTO);
		}

		return PropertiesDTOList;

	}
	// AVS-12973 stop

	public ContentEntity getContentByExternalId(String externalId){
		return contentRepository.getcontentByExternalId(externalId);
		
	}
	
	public ContentEntity getContentByContentId(Integer contentId){
		return contentRepository.getContentByContentId(contentId);
		
	}
}
