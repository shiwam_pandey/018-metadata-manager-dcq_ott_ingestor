package com.accenture.avs.be.dcq.vod.ingestor.timers;

import java.io.File;
import java.io.FilenameFilter;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.service.DcqCacheService;
import com.accenture.avs.be.dcq.vod.ingestor.service.DcqVodIngestorService;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.cache.Configurator;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.commons.lib.LoggerWrapper;

/**
 * @author karthik.vadla
 *
 */
@Component
public class DcqVodIngestorInitialization {
	private static LoggerWrapper log = new LoggerWrapper(DcqVodIngestorInitialization.class);
	
	public static ClassLoader classLoader;
	@Autowired
	private Configurator configurator;
	@Autowired
	private DcqVodIngestorService dcqVodIngestorService;
	
	@Autowired
	private DcqCacheService dcqCacheService;
	
	
	@Value(("${config.dynamic-update-enabled}"))
	private String isDyamicUpdateEnabled;	
	
	@PostConstruct
	protected void init () throws Exception {
		loadClassLoader();
		Configuration config = configurator.getConfiguration();
		dcqCacheService.refreshCache(config);
		dcqVodIngestorService.dcqVodIngestor("ALL", "", "", config);
	}
	
	/**
	 * @throws ApplicationException
	 */
	private void loadClassLoader() throws ApplicationException {
		URL[] urls = null;
		String pluginDirPath = System.getenv("POST_PROCESSOR_PLUGIN_DIR");
		
		if(StringUtils.isEmpty(pluginDirPath)) {
			log.logMessage("plugin directory path null or empty");
		}else {
			File dir = new File(pluginDirPath);
			if (!dir.exists()){
				log.logMessage("plugin directory does not exist - path: {}", dir.getAbsolutePath());
			}
			urls = getUrls(dir);
		}
		if (DcqVodIngestorUtils.isEmptyArray(urls)) {
			classLoader = Thread.currentThread().getContextClassLoader();
		} else {
			classLoader = URLClassLoader.newInstance(urls, Thread.currentThread().getContextClassLoader());
		}
		
	}	
	
	@Scheduled(fixedDelayString = "${config.dynamic-update-frequency-rate}", initialDelayString = "${config.dynamic-update-frequency-rate}")
	public void dynamicUpdate() throws ApplicationException {
		if(isDyamicUpdateEnabled.equalsIgnoreCase(DcqVodIngestorConstants.SHORT_YES)) {
		Configuration config = configurator.getConfiguration();
		dcqVodIngestorService.dynamicUpdate(config);
		}
	}
	
	
	@Scheduled(fixedDelayString = "${config.dcq-cache-reload-rate}", initialDelayString = "${config.dcq-cache-reload-rate}")
	public void cacheRefresh() throws Exception {
		Configuration config = configurator.getConfiguration();
		dcqCacheService.refreshCache(config);
	}
	
	private URL[] getUrls(File dir) throws ApplicationException {
		ArrayList<URL> result = new ArrayList<URL>();
		URL url = null;
		if (dir == null) {
			return null;
		}
		try {
			File[] files = dir.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					String lowercaseName = name.toLowerCase();
					if (lowercaseName.endsWith(".jar")) {
						return true;
					} else {
						return false;
					}
				}
			});

			if (files != null && files.length >0 ) {
				for (File file : files) {
					url = file.toURI().toURL();
					System.out.println("add file : " + url);
					result.add(url);
				}
				return result.toArray(new URL[files.length]);
			}
			return null;
		} catch (Exception ex) {
			throw new ApplicationException("Error, could not add file " + ( url != null ? url.toExternalForm() : "" + " to system classloader"));
		}
	}
	
}
