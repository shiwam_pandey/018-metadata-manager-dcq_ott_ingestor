package com.accenture.avs.be.dcq.vod.ingestor.v1.rest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.avs.be.dcq.vod.ingestor.dto.EmfDTO;
import com.accenture.avs.be.dcq.vod.ingestor.service.EmfService;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.framework.annotation.AvsRestController;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.exception.ApplicationException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@AvsRestController
@Api(tags="EMF Services",value = "Emf API's", description = "API's pertaining to activate,deactivate emf")
public class EmfV1Controller {

	@Autowired
private	EmfService emfService;
	
	@RequestMapping(value = "/v1/emf", method = RequestMethod.PUT)
	@ApiOperation(value="activate emf",notes = "To activate EMF by emfName, Extended metadata fields are used while ingesting a VOD content")
	@ApiImplicitParams({
		@ApiImplicitParam(name="emfName", value="emf Name", dataType="string",required =true)})
	@ApiResponses({
		@ApiResponse(code=400, message="ACN_3000: Missing Parameter"
				+ "\nACN_3019: Invalid parameter"),
		@ApiResponse(code=200, message="ACN_200: OK"),
		@ApiResponse(code=500, message="ACN_300: GENERIC ERROR")
	})
	public @ResponseBody ResponseEntity<GenericResponse<EmfDTO>> setEmfActive(@RequestParam(required = false) String emfName) throws ApplicationException {
		GenericResponse response = emfService.activateOrDeactivateEmf(emfName, DcqVodIngestorConstants.EMFCONSTANTS.PUT_IS_ACTIVE);
		return ResponseEntity.ok(response);
	}
	
	//deactive the EMF isActive FLag 
	@RequestMapping(value = "/v1/emf", method = RequestMethod.DELETE)
	@ApiOperation(value="deactivate emf",notes = "To deactivate EMF by emfName, Extended metadata fields are used while ingesting a VOD content")
	@ApiImplicitParams({
		@ApiImplicitParam(name="emfName", value="emf Name", dataType="string",required =true)})
	@ApiResponses({
		@ApiResponse(code=400, message="ACN_3000: Missing Parameter"
				+ "\nACN_3019: Invalid parameter"),
		@ApiResponse(code=200, message="ACN_200: OK"),
		@ApiResponse(code=500, message="ACN_300: GENERIC ERROR")
	})
	public @ResponseBody ResponseEntity<GenericResponse<EmfDTO>> setEmfInActive(@RequestParam(required = false) String emfName) throws ApplicationException{
		GenericResponse response = emfService.activateOrDeactivateEmf( emfName, DcqVodIngestorConstants.EMFCONSTANTS.DELETE_IS_ACTIVE);
		return ResponseEntity.ok(response);
	}
	
}
