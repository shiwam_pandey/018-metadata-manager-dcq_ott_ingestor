package com.accenture.avs.be.dcq.vod.ingestor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.avs.persistence.technicalcatalogue.ChannelPlatformEntity;

@Repository
public interface ChannelPlatformRepository extends  JpaRepository<ChannelPlatformEntity,Integer>{

	@Modifying
	@Transactional
	void deleteByChannelId(@Param("channelId")Integer channelId);

}
