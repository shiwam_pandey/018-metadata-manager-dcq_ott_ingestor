package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.io.Serializable;
import java.util.List;

public class CopyProtectionSchemes implements Serializable {

	/**
	 * @author naga.sireesha.meka
	 *
	 */
	private static final long serialVersionUID = 1L;
	
	private List<CopyProtectionScheme> copyProtectionSchemes;

	public List<CopyProtectionScheme> getCopyProtectionSchemes() {
		return copyProtectionSchemes;
	}

	public void setCopyProtectionSchemes(List<CopyProtectionScheme> copyProtectionSchemes) {
		this.copyProtectionSchemes = copyProtectionSchemes;
	}
	
	
}
