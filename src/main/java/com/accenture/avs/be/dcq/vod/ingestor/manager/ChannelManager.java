package com.accenture.avs.be.dcq.vod.ingestor.manager;

import java.util.List;

import com.accenture.avs.be.dcq.vod.ingestor.dto.channel.asset.Channels;
import com.accenture.avs.be.dcq.vod.ingestor.dto.channel.asset.Channels.Channel;
import com.accenture.avs.persistence.technicalcatalogue.TechnicalPackageEntity;


public interface ChannelManager {	
	public List<Integer> retrieveByDefaultChannelNumber(String defaultChannelNumber);

	public List<TechnicalPackageEntity> selectByPackageId(Integer packageId);
	public Integer createChannel(Channel inputChannel, Channels channels) throws Exception;
}
