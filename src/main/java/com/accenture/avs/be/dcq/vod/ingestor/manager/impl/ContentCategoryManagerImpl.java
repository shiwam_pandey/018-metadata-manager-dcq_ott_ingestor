package com.accenture.avs.be.dcq.vod.ingestor.manager.impl;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.stream.Collectors;

import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.avs.be.dcq.vod.ingestor.dto.contentcategory.asset.Category;
import com.accenture.avs.be.dcq.vod.ingestor.dto.contentcategory.asset.ContentId;
import com.accenture.avs.be.dcq.vod.ingestor.dto.contentcategory.asset.ContentList;
import com.accenture.avs.be.dcq.vod.ingestor.dto.contentcategory.asset.FlagTypeYN;
import com.accenture.avs.be.dcq.vod.ingestor.manager.ContentCategoryManager;
import com.accenture.avs.be.dcq.vod.ingestor.manager.ContentMetadataManager;
import com.accenture.avs.be.dcq.vod.ingestor.repository.CategoryRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ContentCategoryRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ContentRepository;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.Validator;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.contentcategory.asset.CategoryContent;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.configuration.CacheConstants;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.persistence.technicalcatalogue.CategoryEntity;
import com.accenture.avs.persistence.technicalcatalogue.ContentCategoryEntity;
import com.accenture.avs.persistence.technicalcatalogue.ContentCategoryEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.ContentEntity;

/**
 * @author karthik.vadla
 *
 */
@Component
@Transactional(rollbackFor = { RuntimeException.class })
public class ContentCategoryManagerImpl implements ContentCategoryManager {

	private static final LoggerWrapper log = new LoggerWrapper(ContentCategoryManagerImpl.class);

	@Autowired
	private ContentRepository contentRepository;
	@Autowired
	private CategoryRepository categoryRepository;
	@Autowired
	private ContentCategoryRepository contentCategoryRepository;
	@Autowired
	private ContentMetadataManager contentMetadataManager;

	@Override
	public CategoryEntity retrieveCategoryByExternald(String externalId) {
		List<CategoryEntity> category = categoryRepository.retrieveByExternald(externalId);
		CategoryEntity categoryEntity = null;
		
		if(category!=null && !category.isEmpty()) {
			categoryEntity = category.get(0);
		}
		
		return categoryEntity;
	}

	@Override
	public CategoryEntity retrieveCategoryByCategoryid(Integer categoryId) {
		return categoryRepository.retrieveByCategoryid(categoryId);
	}

	@Override
	public ContentEntity retrieveContentByExternald(String externalId) {
		return contentRepository.getcontentByExternalId(externalId);
	}

	@Override
	public ContentEntity retrieveContentByContentid(Integer contentId) {
		// TODO Auto-generated method stub
		return contentRepository.getContentByContentId(contentId);
	}

	@Override
	public void ingestCategoryContent(com.accenture.avs.be.dcq.vod.ingestor.dto.contentcategory.asset.Asset asset,
			String transactionNumber,Configuration config) throws Exception {


		List<Integer> categoryListToDelete = null;
		List<ContentCategoryEntity> contentCategoryEntities = new ArrayList<ContentCategoryEntity>();

		try {
			int size = asset.getCategoryList().getCategory().size();
			log.logMessage("categories to process: ", size);
			for (Category cat : asset.getCategoryList().getCategory()) {

				List<ContentCategoryEntity> currentContentCategoryList = new ArrayList<ContentCategoryEntity>();
				log.logMessage("Processing category: " + cat.getCategoryId().getValue());

				// for each category we'll check on avs database if it's
				// present.

				CategoryEntity categoryEntity = null;
				if (cat.getCategoryId().getIsExternal().equals(FlagTypeYN.Y)) {
					categoryEntity = retrieveCategoryByExternald(cat.getCategoryId().getValue());
				} else {
					categoryEntity = retrieveCategoryByCategoryid(Integer.parseInt(cat.getCategoryId().getValue()));
				}
				if (categoryEntity == null) {
					log.logMessage("Category not found : " + cat.getCategoryId().getValue());
					throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters(
									"CategoryId : " + cat.getCategoryId().getValue() + " not found in DB"));

				}

				// for each content we'll check on avs database if it's present.

				ContentList content = cat.getContentList();

				if (content == null) {

					if (categoryListToDelete == null) {
						categoryListToDelete = new ArrayList<Integer>();
					}
					categoryListToDelete.add(categoryEntity.getCategoryId());
				
				} else {

					for (ContentId currentId : content.getContentId()) {

						ContentEntity contentEntity;

						if (content.getIsExternal().equals(FlagTypeYN.Y)) {
							contentEntity = retrieveContentByExternald(currentId.getValue());
						} else {
							contentEntity = retrieveContentByContentid(Integer.parseInt(currentId.getValue()));
						}
						if (contentEntity == null) {
							log.logMessage("Content not found : " + currentId.getValue());
							throw new ApplicationException(
									CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
									new NestedParameters("ContentId : " + currentId.getValue() + " not found in DB"));
							/*
							 * ApplicationException ae = new
							 * ApplicationException(CacheConstants.MessageKeys.
							 * ERROR_BE_ACTION_3019_INVALID_PARAMETER,new
							 * NestedParameters("ContentId")); NestedObject
							 * nestObj= ae.new NestedObject();
							 * nestObj.setResultObj("ContentId : " +
							 * currentId.getValue() + " not found in DB");
							 * ae.setResultObj(nestObj); throw ae;
							 */
						}

						ContentCategoryEntity contentCategory = new ContentCategoryEntity();
						ContentCategoryEntityPK contentCategoryPk = new ContentCategoryEntityPK();
						contentCategoryPk.setCategoryId(categoryEntity.getCategoryId());
						contentCategoryPk.setContentId(contentEntity.getContentId());
						contentCategory.setId(contentCategoryPk);
						if (currentId.getEndDate() != null) {
							contentCategory
							.setEndDate(DcqVodIngestorUtils.xmlGregorianCalendar2Date(currentId.getEndDate()));
						}
						if (currentId.getStartDate() != null) {
							contentCategory.setStartDate(
									DcqVodIngestorUtils.xmlGregorianCalendar2Date(currentId.getStartDate()));
						}
						if(currentId.getIsPrimary()!=null && (!currentId.getIsPrimary().equals("Y") && !currentId.getIsPrimary().equals("N")))
						{
							throw new ApplicationException(
									CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
									new NestedParameters("IsPrimary. Possible values Y or N"));
						}
						contentCategory.setIsPrimary(currentId.getIsPrimary());
						if (currentId.getPosition() != null) {
							contentCategory.setPosition(currentId.getPosition().intValue());

						}

						currentContentCategoryList.add(contentCategory);

					}


				
				}
				contentCategoryEntities.addAll(currentContentCategoryList);
			}
			List<Integer> contentIds = new ArrayList<Integer>();
			writeContentCategory(categoryListToDelete, contentCategoryEntities,contentIds);

			contentIds = contentIds.stream().distinct().collect(Collectors.toList());

			//Update ES
			//check if we can throw applicationException when ES fails
			RestHighLevelClient highLevelRestClient = DcqVodIngestorUtils.getESHighLevelRestClient(config);
			//Checking the ES availability to return error response before starting the Async process
			highLevelRestClient.ping(RequestOptions.DEFAULT);
			processandWriteContents(contentIds, transactionNumber,  String.valueOf(System.currentTimeMillis()), "", null, config);

		} catch (RuntimeException re) {//TO DO change the error message 
			log.logError(re);
			throw new ApplicationException(
					CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters("Not able to save to DB"));

		}catch(ApplicationException ae) {
			log.logError(ae);
			throw ae;
		}
		catch(ConnectException e) {
			log.logError(e);
			throw new Exception("ES not reachable");
		}
		catch (Exception e) {
			throw e;
		}

	}

	@Transactional(rollbackFor = { RuntimeException.class })
	private void writeContentCategory(List<Integer> categoryListToDelete,
			List<ContentCategoryEntity> contentCategoryEntities,List<Integer> contentIds) throws Exception {
		
		List<Integer> primaryContentIds = new ArrayList<Integer>();
		log.logMessage("Checking if any contents to be deleted");
		try {
			if (categoryListToDelete != null && !categoryListToDelete.isEmpty()) {
				log.logMessage("Received contents to delete");
				log.logMessage("Fetching the contentIds of these categories");
				List<Integer> contentIdsToBeDeleted = contentCategoryRepository
						.retriveContentIdsByCategoryIds(categoryListToDelete);
				if (contentIdsToBeDeleted != null) {
					contentIds.addAll(contentIdsToBeDeleted);
				}
				log.logMessage("Deleting category contents");
				contentCategoryRepository.deleteByCategoryIds(categoryListToDelete);
				log.logMessage("Successfully deleted category contents");
			} else {
				log.logMessage("No CategoryContents to delete");
			}

			if (contentCategoryEntities != null && !contentCategoryEntities.isEmpty()) {
				List<Integer> categoryIds = new ArrayList<Integer>();
				for (ContentCategoryEntity contentCategoryEntity : contentCategoryEntities) {

					categoryIds.add(contentCategoryEntity.getId().getCategoryId());
					contentIds.add(contentCategoryEntity.getId().getContentId());
					if (contentCategoryEntity.getIsPrimary() != null
							&& contentCategoryEntity.getIsPrimary().equalsIgnoreCase("Y")) {
						primaryContentIds.add(contentCategoryEntity.getId().getContentId());
						
					}
				}

				// get the contentids based on cateIds and save the contentIds
				List<Integer> categoryContentIds = contentCategoryRepository
						.retriveContentIdsByCategoryIds(categoryIds);
				if (categoryContentIds != null) {
					contentIds.addAll(categoryContentIds);
				}
				// delete the contentCategories based on categoryIds

				contentCategoryRepository.deleteByCategoryIds(categoryIds);
                 if(primaryContentIds!=null && !primaryContentIds.isEmpty()){
				contentCategoryRepository.updatePrimaryForContents(primaryContentIds);
                 }

				contentCategoryRepository.saveAll(contentCategoryEntities);
			}
		} catch (Exception e) {
			throw new RuntimeException();
		}

	}

	@Override
	public void ingestCategoryContent(CategoryContent categoryContent, String transactionNumber, Configuration config)
			throws ApplicationException, Exception {

		List<Integer> categoryListToDelete = null;
		List<ContentCategoryEntity> contentCategoryEntities = new ArrayList<ContentCategoryEntity>();

		try {
//			int size = categoryContentJSON.getCategories().size();
//			log.logMessage("categories to process: ", size);
			//for (CategoryContent category : categoryContentJSON.getCategories()) {
			if(categoryContent!=null){
				CategoryContent category=categoryContent;
				List<ContentCategoryEntity> currentContentCategoryList = new ArrayList<ContentCategoryEntity>();
				log.logMessage("Processing category: " + category.getCategoryId());

				// for each category we'll check on avs database if it's
				// present.

				CategoryEntity categoryEntity = null;
				isValidCategory(category);
				if (Objects.isNull(category.getCategoryId())) {
					categoryEntity = retrieveCategoryByExternald(category.getExternalId());
				} else {
					categoryEntity = retrieveCategoryByCategoryid(category.getCategoryId());
				}
				if (categoryEntity == null) {
					if (Objects.isNull(category.getCategoryId())) {
						log.logMessage("Category not found : " + category.getExternalId());
						throw new ApplicationException(
								CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
								new NestedParameters("ExternalId : " + category.getExternalId() + " not found in DB"));
					} else {
						log.logMessage("Category not found : " + category.getCategoryId());
						throw new ApplicationException(
								CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
								new NestedParameters("CategoryId : " + category.getCategoryId() + " not found in DB"));
					}

				}

				// for each content we'll check on avs database if it's present.

				List<CategoryContent.Content> contents = category.getContents();

				if (contents == null) {

					if (categoryListToDelete == null) {
						categoryListToDelete = new ArrayList<Integer>();
					}
					categoryListToDelete.add(categoryEntity.getCategoryId());
				
				} else {

					
					for (CategoryContent.Content content : contents) {

						ContentEntity contentEntity;
						isValidCategory(content);
						if (Objects.isNull(content.getContentId())) {
							contentEntity = retrieveContentByExternald(content.getExternalContentId());
						} else {
							contentEntity = retrieveContentByContentid(content.getContentId());
						}
						
						if (Objects.nonNull(content.getPosition())) {
							if(content.getPosition()<0){
								log.logMessage("Position should not negative :" + category.getCategoryId());
								throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
										new NestedParameters("Position"));			
							}
						}
						if (contentEntity == null) {
							if (Objects.isNull(content.getContentId())) {
								log.logMessage("Content not found : " + content.getExternalContentId());
								throw new ApplicationException(
										CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
										new NestedParameters("ExternalContentId : " + content.getExternalContentId() + " not found in DB"));
							}else{
								log.logMessage("Content not found : " + content.getContentId());
								throw new ApplicationException(
										CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
										new NestedParameters("ContentId : " + content.getContentId() + " not found in DB"));
							}
							
						}

						ContentCategoryEntity contentCategory = new ContentCategoryEntity();
						ContentCategoryEntityPK contentCategoryPk = new ContentCategoryEntityPK();
						contentCategoryPk.setCategoryId(categoryEntity.getCategoryId());
						contentCategoryPk.setContentId(contentEntity.getContentId());
						contentCategory.setId(contentCategoryPk);
						if (content.getEndDate() != null) {
							contentCategory.setEndDate(content.getEndDate());
						}
						if (content.getStartDate() != null) {
							contentCategory.setStartDate(content.getStartDate());
						}
						if (content.getIsPrimary() != null
								&& (!content.getIsPrimary().equals("Y") && !content.getIsPrimary().equals("N"))) {
							throw new ApplicationException(
									CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
									new NestedParameters("IsPrimary. Possible values Y or N"));
						}
						contentCategory.setIsPrimary(content.getIsPrimary());
						if (content.getPosition() != null) {
							contentCategory.setPosition(content.getPosition());
						}
						currentContentCategoryList.add(contentCategory);
					}
				
				}
				contentCategoryEntities.addAll(currentContentCategoryList);
			}
			List<Integer> contentIds = new ArrayList<Integer>();
			writeContentCategory(categoryListToDelete, contentCategoryEntities, contentIds);

			contentIds = contentIds.stream().distinct().collect(Collectors.toList());
			
			RestHighLevelClient highLevelRestClient = DcqVodIngestorUtils.getESHighLevelRestClient(config);
			//Checking the ES availability to return error response before starting the Async process
			highLevelRestClient.ping(RequestOptions.DEFAULT);
			processandWriteContents(contentIds, transactionNumber,  String.valueOf(System.currentTimeMillis()), "", null, config);

		} catch (RuntimeException re) {// TO DO change the error message
			log.logError(re);
			throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters("Not able to save to DB"));
		} catch (ApplicationException ae) {
			log.logError(ae);
			throw ae;
		} catch(ConnectException e) {
			log.logError(e);
			throw new Exception("ES not reachable");
		} catch (Exception e) {
			throw e;
		}
	}

	private void isValidCategory(CategoryContent category) throws ApplicationException {
		if(Objects.isNull(category.getCategoryId()) && Objects.isNull(category.getExternalId())){
			log.logMessage("Category and External id both are null ");
			throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters("CategoryId and ExternalId both are null"));
		} else {
			Validator.sanitizingParamsForSpecialParams(DcqVodIngestorConstants.EXTERNALID, category.getExternalId());
		}
	}
	
	private void isValidCategory(CategoryContent.Content content) throws ApplicationException {
		if(Objects.isNull(content.getExternalContentId()) && Objects.isNull(content.getContentId())){
			log.logMessage("Content and External id both are null ");
			throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters("ContentId and ExternalContentId both are null"));
		}  else {
			Validator.sanitizingParamsForSpecialParams(DcqVodIngestorConstants.EXTERNAL_CONTENT_ID, content.getExternalContentId());
		}
	}
	
	public void processandWriteContents(List<Integer> contentIds,
			String transactionNumber, String startTime, String mode, String indexDate, Configuration config) throws ApplicationException,Exception {
		try {
			TimerTask writeToEsTimer = new TimerTask() {
				@Override
				public void run() {
					try {
						log.logMessage("Started writing contents to ES");
						contentMetadataManager.processandWriteContents(contentIds, transactionNumber,  String.valueOf(System.currentTimeMillis()), "", null, config);
						log.logMessage("Successfully saved data to ES");
					} catch (ApplicationException e) {
						log.logError(e);
					}catch (Exception e) {
						log.logError(e);
					}
				}
			};
			Timer timer = new Timer();
			timer.schedule(writeToEsTimer,1);
		}
		catch (RuntimeException re) {//TO DO change the error message 
			log.logError(re);
			throw new ApplicationException(
					CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters("Not able to save to DB"));

		}catch(Exception ae) {
			log.logError(ae);
			throw ae;
		}
	}

}
