package com.accenture.avs.be.dcq.vod.ingestor.cache;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.repository.TechnicalPackageRepository;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.persistence.technicalcatalogue.TechnicalPackageEntity;

/**
 * @author karthik.vadla
 *
 */
@Component
public class TechnicalPackageCache {
	private static final LoggerWrapper log = new LoggerWrapper(TechnicalPackageCache.class);

	private Map<Integer, String> technicalPackageCache = new HashMap<Integer, String>();

	@Autowired
	private TechnicalPackageRepository technicalPackageRepository;

	/**
	 * @return
	 */
	public Map<Integer, String> loadTechnicalPackages() {
		try {
			log.logMessage("Loading technicalPackage cache");
			for (TechnicalPackageEntity param : technicalPackageRepository.retrievePackagesNotRvod()) {
				technicalPackageCache.put(param.getPackageId(), param.getPropertyName());
			}

		} catch (Exception e) {
			log.logError(e);
		}
		return technicalPackageCache;
	}

	public Map<Integer, String> getTechnicalPackages() {
		return technicalPackageCache;
	}

}
