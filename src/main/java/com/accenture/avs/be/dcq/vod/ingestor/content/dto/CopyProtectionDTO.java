package com.accenture.avs.be.dcq.vod.ingestor.content.dto;

public class CopyProtectionDTO {
	private String securityCode;
	private String securityOption;
	public String getSecurityCode() {
		return securityCode;
	}
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
	public String getSecurityOption() {
		return securityOption;
	}
	public void setSecurityOption(String securityOption) {
		this.securityOption = securityOption;
	}

}
