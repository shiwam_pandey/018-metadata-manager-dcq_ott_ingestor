package com.accenture.avs.be.dcq.vod.ingestor.service;

import com.accenture.avs.be.dcq.vod.ingestor.content.dto.TransactionRateCardResponse;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;

/**
 * @author karthik.vadla
 *
 */
public interface VodService {

	GenericResponse ingestContent(String vodXml,Configuration config) throws Exception;

	GenericResponse getTRC(String startIndex, String maxResults, Configuration config) throws Exception;

	TransactionRateCardResponse getLegacyTRC(String startIndex, String maxResults, Configuration config) throws Exception;
	
	void unpublishContent(Integer contentId,Configuration config) throws Exception;
}
