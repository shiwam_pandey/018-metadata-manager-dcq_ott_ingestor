package com.accenture.avs.be.dcq.vod.ingestor.cache;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.lib.configuration.ConfigurationClient;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.commons.lib.LoggerWrapper.OtherSystemCallType;

/**
 * @author karthik.vadla
 *
 */
@Component
public class EsSettingsMappingsCache {

	@Autowired
	private	LanguageCache languageCache;
	
	private	Map<String, String> esSettingsMappingsConfigurationsMap;

	private static final LoggerWrapper log = new LoggerWrapper(EsSettingsMappingsCache.class);

	/**
	 * @param config
	 * @return
	 * @throws ApplicationException
	 */
	public void loadEsSettingsMappings(Configuration config) throws ApplicationException {
		long startTime = new Date().getTime();
		boolean finalLogResponse = false;
		
		try {
			Map<String, String> languagesCache = languageCache.getLanguages();
			Integer langCount = languagesCache.size();
			Integer resSize = langCount * 6;
			String[] resources = new String[resSize+2];

			int i = 0;
			for (String langCode : languagesCache.keySet()) {

				resources[i] = DcqVodIngestorConstants.CONTENT_IN_LOWER+DcqVodIngestorConstants.UNDERSCORE+ langCode +DcqVodIngestorConstants.UNDERSCORE +DcqVodIngestorConstants.MAPPING_IN_LOWER;
				i++;
				resources[i] = DcqVodIngestorConstants.CONTENT_IN_LOWER+DcqVodIngestorConstants.UNDERSCORE+ langCode +DcqVodIngestorConstants.UNDERSCORE+DcqVodIngestorConstants.SETTINGS_IN_LOWER;
				i++;

				resources[i] = DcqVodIngestorConstants.CATEGORY_IN_LOWER+DcqVodIngestorConstants.UNDERSCORE+ langCode +DcqVodIngestorConstants.UNDERSCORE+DcqVodIngestorConstants.MAPPING_IN_LOWER;
				i++;
				resources[i] = DcqVodIngestorConstants.CATEGORY_IN_LOWER+ DcqVodIngestorConstants.UNDERSCORE+ langCode +DcqVodIngestorConstants.UNDERSCORE +DcqVodIngestorConstants.SETTINGS_IN_LOWER;
				i++;

				resources[i] = DcqVodIngestorConstants.VLCCONTENT_IN_LOWER +DcqVodIngestorConstants.UNDERSCORE+ langCode +DcqVodIngestorConstants.UNDERSCORE+DcqVodIngestorConstants.MAPPING_IN_LOWER;
				i++;
				resources[i] = DcqVodIngestorConstants.VLCCONTENT_IN_LOWER+DcqVodIngestorConstants.UNDERSCORE + langCode +DcqVodIngestorConstants.UNDERSCORE+DcqVodIngestorConstants.SETTINGS_IN_LOWER;
				i++;
			}
			
			resources[i] = DcqVodIngestorConstants.VODCHANNEL_IN_LOWER+DcqVodIngestorConstants.UNDERSCORE+DcqVodIngestorConstants.MAPPING_IN_LOWER;
			i++;
			resources[i] = DcqVodIngestorConstants.VODCHANNEL_IN_LOWER+DcqVodIngestorConstants.UNDERSCORE+DcqVodIngestorConstants.SETTINGS_IN_LOWER;

			log.logCallToOtherSystemRequestBody(DcqVodIngestorConstants.CONFIGURATION,
					DcqVodIngestorConstants.ES_SETTINGS_MAPPINGS_CONFIG, "", OtherSystemCallType.INTERNAL);
			esSettingsMappingsConfigurationsMap = (Map<String, String>) ConfigurationClient
					.getMicroserviceConfiguration(resources);
			log.logCallToOtherSystemResponseBody(DcqVodIngestorConstants.CONFIGURATION,
					DcqVodIngestorConstants.ES_SETTINGS_MAPPINGS_CONFIG, DcqVodIngestorConstants.OK,
					OtherSystemCallType.INTERNAL);
			log.logCallToOtherSystemEnd(DcqVodIngestorConstants.CONFIGURATION,
					DcqVodIngestorConstants.ES_SETTINGS_MAPPINGS_CONFIG, "", DcqVodIngestorConstants.OK, "", "",
					System.currentTimeMillis() - startTime, OtherSystemCallType.INTERNAL);

		} catch (Exception e) {
			finalLogResponse = true;
			log.logError(e);
		} finally {
			if (finalLogResponse) {
				log.logCallToOtherSystemEnd(DcqVodIngestorConstants.CONFIGURATION,
						DcqVodIngestorConstants.ES_SETTINGS_MAPPINGS_CONFIG, "", DcqVodIngestorConstants.KO, "", "",
						System.currentTimeMillis() - startTime, OtherSystemCallType.INTERNAL);
			}
		}

	}

	public Map<String, String> getEsSettingsMappingsConfigurationsMap() {
		return esSettingsMappingsConfigurationsMap;
	}

}
