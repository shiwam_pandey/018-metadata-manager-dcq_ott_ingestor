package com.accenture.avs.be.dcq.vod.ingestor.rest.controllers;

import javax.xml.bind.JAXBException;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.avs.be.dcq.vod.ingestor.content.dto.TransactionRateCardListDto;
import com.accenture.avs.be.dcq.vod.ingestor.content.dto.TransactionRateCardResponse;
import com.accenture.avs.be.dcq.vod.ingestor.dto.Response;
import com.accenture.avs.be.dcq.vod.ingestor.service.VodService;
import com.accenture.avs.be.framework.annotation.AvsRestController;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.cache.Configurator;
import com.accenture.avs.be.framework.configuration.CacheConstants;
import com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.commons.lib.support.CommonConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@AvsRestController
@Api(tags="TRC Services", value = "TRC API's", description = "API's pertaining to get the Transaction rate cards")
public class TRCController {

	@Autowired
private	Configurator configurator;
	
	@Autowired
	private VodService vodService;
	

	
	/**
	 * @param startIndex
	 * @param maxResults
	 * @return
	 * @throws ApplicationException
	 */
	@RequestMapping(value = "/v1/transactionratecard", method = RequestMethod.GET)
	@ApiOperation(value="getTRCs",notes = "To fetch all Transaction rate card list, this service is used by CMS to link TRC information to the content while ingesting a TVOD content")
	@ApiResponses({
		@ApiResponse(code=400, message="ACN_3019: Invalid parameter"),
		@ApiResponse(code=200, message="ACN_200: OK"),
		@ApiResponse(code=500, message="ACN_300: GENERIC ERROR")
	})
	@ApiImplicitParams({
		@ApiImplicitParam(name = "startIndex", value = "Starting index. Default value 0.", dataType = "int", required = false),
		@ApiImplicitParam(name = "maxResults", value = "Maximum results to retrieve. Default value 50 .If value greater than 50 or not given then default value is considered.", dataType = "int", required = false)})
	public @ResponseBody
	ResponseEntity<GenericResponse<TransactionRateCardListDto>> getTRC(@RequestParam(required= false,value="startIndex") String startIndex, @RequestParam(required= false,value="maxResults") String maxResults) throws ApplicationException{
		
		GenericResponse genericResponse = null;
		try {
			Configuration config = configurator.getConfiguration(ThreadContext.get(CommonConstants.LogParamsL4j2.TN));
			genericResponse = vodService.getTRC(startIndex,maxResults, config);
		} catch (ApplicationException ae) {
				throw ae;
			}catch (JAXBException e) {
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR, new NestedParameters(e.getMessage()));
			}
		catch (Exception e) {
				throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR);
			}
		return ResponseEntity.ok(genericResponse);
	}
	
	/**
	 * @param startIndex
	 * @param maxResults
	 * @return
	 * @throws ApplicationException
	 */
	@Deprecated
	@RequestMapping(value = "/transactionratecard", method = RequestMethod.GET)
	@ApiOperation(value="getTRCs (For backward compatibility)",notes = "Please find the page for the IFA: https://fleet.alm.accenture.com/avswiki/display/AVSM3DOC/.DCQ+VOD+Ingestor-XML+v6.5. \n To fetch all Transaction rate card list, this service is used by CMS to link TRC information to the content while ingesting a TVOD content.")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "from", value = "Starting index", dataType = "int", required = false),
		@ApiImplicitParam(name = "size", value = "Maximum results to retrieve", dataType = "int", required = false)})
	public @ResponseBody
	ResponseEntity<TransactionRateCardResponse> getLegacyTRC(@RequestParam(required=false,value="from") String startIndex, @RequestParam(required=false,value="size") String maxResults) throws ApplicationException{
		TransactionRateCardResponse response = new TransactionRateCardResponse();
		try {
			Configuration config = configurator.getConfiguration(ThreadContext.get(CommonConstants.LogParamsL4j2.TN));
			response = vodService.getLegacyTRC(startIndex,maxResults, config);

			} catch (ApplicationException ae) {
				if(ae.getExceptionId().contains("3019")||ae.getExceptionId().contains("3000")) {
				 response = new TransactionRateCardResponse(Response.RESULT_CODE_KO, "Invalid Parameter "+ae.getParameters().getParameters()[0], "",System.currentTimeMillis(), null);
					return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
				}else {
					new TransactionRateCardResponse(Response.RESULT_CODE_KO, ae.getParameters().getParameters()[0], "",System.currentTimeMillis(), null);
					return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
				}
			}
			catch (Exception e) {
				new TransactionRateCardResponse(Response.RESULT_CODE_KO, e.getMessage(), "",System.currentTimeMillis(), null);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			}
			return ResponseEntity.ok(response);
	}

}
