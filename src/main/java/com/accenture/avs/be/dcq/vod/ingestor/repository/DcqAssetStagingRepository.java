package com.accenture.avs.be.dcq.vod.ingestor.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.avs.persistence.technicalcatalogue.DcqAssetStagingEntity;
/**
 * @author karthik.vadla
 *
 */
@Repository
public interface DcqAssetStagingRepository extends JpaRepository<DcqAssetStagingEntity,Integer>{


	/**
	 * @param publishedIds
	 * @param assetType
	 * @param published
	 * @param inProgress
	 */
	@Modifying
	@Transactional
	public void updateStatusPublished(@Param(DcqAssetStagingEntity.PARAM_IDS) Set<Integer> publishedIds, 
			@Param(DcqAssetStagingEntity.ASSET_TYPE) String assetType, 
			@Param(DcqAssetStagingEntity.PUBLISHED_STATUS) String published, 
			@Param(DcqAssetStagingEntity.PROGRESS_STATUS) String inProgress);


	/**
	 * @param failedAssetIdsList
	 * @param assetType
	 * @return
	 */
	public List<DcqAssetStagingEntity> retrieveFailedAssets(@Param(DcqAssetStagingEntity.PARAM_IDS) List<Integer> failedAssetIdsList,
			@Param(DcqAssetStagingEntity.ASSET_TYPE) String assetType);

	/**
	 * @param publishedIds
	 * @param assetType
	 */
	@Modifying
	@Transactional
	public void deleteAssets(@Param(DcqAssetStagingEntity.PARAM_IDS) Set<Integer> publishedIds, 
			@Param(DcqAssetStagingEntity.ASSET_TYPE)String assetType);


	/**
	 * @param string
	 */
	@Procedure(name = "POPULATE_DCQ_ASSET_STAGING_CONTENT")
	public void POPULATE_DCQ_ASSET_STAGING_CONTENT(@Param("return_message")String string);

	/**
	 * @param string
	 */
	@Procedure(name = "POPULATE_DCQ_ASSET_STAGING_CATEGORY")
	public void POPULATE_DCQ_ASSET_STAGING_CATEGORY(@Param("return_message")String string);

	/**
	 * @param string
	 */
	@Procedure(name = "POPULATE_DCQ_ASSET_STAGING_VODCHANNEL")
	public void POPULATE_DCQ_ASSET_STAGING_VODCHANNEL(@Param("return_message")String string);

	/**
	 * @param string
	 */
	@Procedure(name = "POPULATE_DCQ_PLAYLIST_STAGING")
	public void POPULATE_DCQ_PLAYLIST_STAGING(@Param("return_message")String string);

	/**
	 * @param assetType
	 * @param ready
	 * @param inProgress
	 * @param failed
	 * @param maxRetry
	 * @return
	 */
	public List<Integer> retrieveByAssetType(@Param(DcqAssetStagingEntity.ASSET_TYPE) String assetType, 
			@Param(DcqAssetStagingEntity.READY_STATUS) String ready, 
			@Param(DcqAssetStagingEntity.PROGRESS_STATUS) String inProgress, 
			@Param(DcqAssetStagingEntity.FAILED_STATUS) String failed, 
			@Param(DcqAssetStagingEntity.MAX_RETRY) Integer maxRetry);

	/**
	 * @param assetType
	 */
	@Modifying
	@Transactional
	public void deleteDuplicateIds(String assetType);

	/**
	 * @param assetIds
	 * @param assetType
	 * @param status
	 */
	@Modifying
	@Transactional
	public void updateStatus(@Param(DcqAssetStagingEntity.PARAM_IDS) List<Integer> assetIds, 
			@Param(DcqAssetStagingEntity.ASSET_TYPE) String assetType, 
			@Param(DcqAssetStagingEntity.PARAM_STATUS) String status);

}
