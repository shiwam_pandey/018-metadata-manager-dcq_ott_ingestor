package com.accenture.avs.be.dcq.vod.ingestor.content.dto;
import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * The response bean class for the TECHNICAL_PACKAGE
 * 
 */
public class ResponsePackage  implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;
	private Long packageId;
	private String packageDescription;
	private String packageName;
	private String teckPkgType;
	private String externalId;
	private String adFunded;
	private String propertyName;
	
	
	

    public String getAdFunded() {
		return adFunded;
	}

	public void setAdFunded(String adFunded) {
		this.adFunded = adFunded;
	}

	public ResponsePackage() {
    }

	public Long getPackageId() {
		return this.packageId;
	}
	public void setPackageId(Long packageId) {
		this.packageId = packageId;
	}

	public String getPackageDescription() {
		return this.packageDescription;
	}
	public void setPackageDescription(String packageDescription) {
		this.packageDescription = packageDescription;
	}

	public String getPackageName() {
		return this.packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	//bi-directional many-to-one association to TeckPkgType
	public String getTeckPkgType() {
		return this.teckPkgType;
	}
	public void setTeckPkgType(String teckPkgType) {
		this.teckPkgType = teckPkgType;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	
	

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public boolean equals(Object other) {
		boolean flag;
		if (this == other) {
			flag = true;
		}
		if (other instanceof ResponsePackage) {
			ResponsePackage castOther = (ResponsePackage) other;
			flag = new EqualsBuilder().append(this.getPackageId(), castOther.getPackageId()).isEquals();
		}else {
			flag = false;	
		}
		return flag;
    }
    
	public int hashCode() {
		return new HashCodeBuilder()
			.append(getPackageId())
			.toHashCode();
    }   

	public String toString() {
		return new ToStringBuilder(this)
			.append("packageId", getPackageId())
			.toString();
	}
}
