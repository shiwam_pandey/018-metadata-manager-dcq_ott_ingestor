package com.accenture.avs.be.dcq.vod.ingestor.processorplugin;

import java.util.List;

import org.springframework.context.ApplicationContext;

import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionWriterRequestDTO;
import com.accenture.avs.be.framework.exception.ApplicationException;

/**
 * @author karthik.vadla
 *
 */
public interface DcqVodIngestionProcessorPlugin {
	List<DcqVodIngestionWriterRequestDTO> updateMetadata(List<DcqVodIngestionWriterRequestDTO> dcqVodIngestionWriterRequestDTOs, String assetType,
			String transactionNumber, ApplicationContext context) throws ApplicationException;
}
