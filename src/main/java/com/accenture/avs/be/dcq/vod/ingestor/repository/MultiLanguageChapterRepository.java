package com.accenture.avs.be.dcq.vod.ingestor.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.avs.persistence.technicalcatalogue.MultilanguageChapterEntity;
/**
 * @author karthik.vadla
 *
 */
@Repository
public interface MultiLanguageChapterRepository extends JpaRepository<MultilanguageChapterEntity,Integer>{

	@Modifying
	@Transactional
	void deleteByContentId(@Param("contentId")Long contentId);

	List<MultilanguageChapterEntity> retrieveByChapterIdNotContentId(@Param("chapterId")Long chapterId, @Param("contentId")Long contentId);

}
