package com.accenture.avs.be.dcq.vod.ingestor.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.accenture.avs.persistence.technicalcatalogue.ExtendedContentAttributeEntity;
/**
 * @author karthik.vadla
 *
 */
@Repository
public interface ExtendedContentAttributesRepository extends JpaRepository<ExtendedContentAttributeEntity,Integer>{

	/**
	 * @param contentId
	 * @return
	 */
	public byte[] retrieveContentXmlBlob(Integer contentId);
	/**
	 * @param integer
	 * @return
	 */
	public List<Object[]> retrieveTypeSubTypeByContentId(@Param("contentId") Integer integer);
	public void deleteByContentId(@Param("contentId")Integer contentId);

	
}
