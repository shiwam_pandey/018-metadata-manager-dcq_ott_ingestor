package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.io.Serializable;

/**
 * @author karthik.vadla
 *
 */
public class DcqVodIngestionWriterRequestDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String esIndex;
	private Integer rowId;// Use Only for VlcContent
	private String esIndexType;
	private Integer esUniqueId;
	private Object esDocObj;
	private Object sourceFile;
	private String categoryContentSourceFile;
	private boolean isDeleted;
	private String platformName;

	public String getPlatformName() {
		return platformName;
	}

	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}

	public Integer getRowId() {
		return rowId;
	}

	public void setRowId(Integer rowId) {
		this.rowId = rowId;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Object getEsDocObj() {
		return esDocObj;
	}

	public void setEsDocObj(Object esDocObj) {
		this.esDocObj = esDocObj;
	}

	public String getEsIndex() {
		return esIndex;
	}

	public void setEsIndex(String esIndex) {
		this.esIndex = esIndex;
	}

	public String getEsIndexType() {
		return esIndexType;
	}

	public void setEsIndexType(String esIndexType) {
		this.esIndexType = esIndexType;
	}

	public Integer getEsUniqueId() {
		return esUniqueId;
	}

	public void setEsUniqueId(Integer esUniqueId) {
		this.esUniqueId = esUniqueId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((esDocObj == null) ? 0 : esDocObj.hashCode());
		result = prime * result + ((esIndex == null) ? 0 : esIndex.hashCode());
		result = prime * result + ((esIndexType == null) ? 0 : esIndexType.hashCode());
		result = prime * result + ((esUniqueId == null) ? 0 : esUniqueId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		boolean flag = true;
		if (this == obj) {
			flag = true;
		}
		if (obj == null) {
			flag = false;
		}
		if (obj instanceof DcqVodIngestionWriterRequestDTO) {
			DcqVodIngestionWriterRequestDTO other = (DcqVodIngestionWriterRequestDTO) obj;
			if (esDocObj == null) {
				if (other.esDocObj != null) {
					flag = false;
				}
			} else if (!esDocObj.equals(other.esDocObj)) {
				flag = false;
			}
			if (esIndex == null) {
				if (other.esIndex != null) {
					flag = false;
				}
			} else if (!esIndex.equals(other.esIndex)) {
				flag = false;
			}
			if (esIndexType == null) {
				if (other.esIndexType != null) {
					flag = false;
				}
			} else if (!esIndexType.equals(other.esIndexType)) {
				flag = false;
			}
			if (esUniqueId == null) {
				if (other.esUniqueId != null) {
					flag = false;
				}
			} else if (!esUniqueId.equals(other.esUniqueId)) {
				flag = false;
			}
		} else {
			flag = false;
		}

		return flag;
	}

	@Override
	public String toString() {
		return "DcqVodIngestionWriterRequestDTO [esIndex=" + esIndex + ", esIndexType=" + esIndexType + ", esUniqueId="
				+ esUniqueId + ", esDocObj=" + esDocObj + "]";
	}

	public Object getSourceFile() {
		return sourceFile;
	}

	public void setSourceFile(Object sourceFile) {
		this.sourceFile = sourceFile;
	}

	public String getCategoryContentSourceFile() {
		return categoryContentSourceFile;
	}

	public void setCategoryContentSourceFile(String categoryContentSourceFile) {
		this.categoryContentSourceFile = categoryContentSourceFile;
	}
}
