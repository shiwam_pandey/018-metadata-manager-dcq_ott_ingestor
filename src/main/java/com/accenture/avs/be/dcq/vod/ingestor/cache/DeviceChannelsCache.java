package com.accenture.avs.be.dcq.vod.ingestor.cache;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.sdp.manager.SDPManager;
import com.accenture.avs.be.dcq.vod.ingestor.utils.CacheConstants;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.commons.lib.LoggerWrapper;

/**
 * @author karthik.vadla
 *
 */
@Component
public class DeviceChannelsCache {

	private static final LoggerWrapper log = new LoggerWrapper(DeviceChannelsCache.class);
	//private static Map<String, Long> deviceChannelsCache;
	private  Map<String, Long> deviceChannelsCache;
	
	@Autowired
	private SDPManager sdpManager;
	
	/**
	 * @param config
	 * @return
	 * @throws ApplicationException
	 */
	public  Map<String, Long> loadDeviceChannels(Configuration config) throws ApplicationException{
		try{
			String sdpInPanic = config.getConstants().getValue(CacheConstants.SDP_IN_PANIC);
			
			if(sdpInPanic.equalsIgnoreCase("N")){
				log.logMessage("Loading device channels cache");
				deviceChannelsCache = sdpManager.searchAllDeviceChannels(config);
			}else{
				log.logMessage("SDP in Panic");
			}
		
		}catch (Exception e) {
			log.logError(e);
		}
		return deviceChannelsCache;
	}
	
	
	/**
	 * @return
	 */
	public Map<String, Long> getDeviceChannels(){
		return deviceChannelsCache;
	}
}
