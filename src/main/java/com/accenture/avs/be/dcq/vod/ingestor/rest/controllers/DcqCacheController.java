package com.accenture.avs.be.dcq.vod.ingestor.rest.controllers;

import javax.xml.bind.JAXBException;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.avs.be.dcq.vod.ingestor.service.DcqCacheService;
import com.accenture.avs.be.dcq.vod.ingestor.service.VodService;
import com.accenture.avs.be.framework.annotation.AvsRestController;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.cache.Configurator;
import com.accenture.avs.be.framework.configuration.CacheConstants;
import com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.commons.lib.support.CommonConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@AvsRestController
@Api(tags="DCQ Cache Services" ,value = "DCQ Cache API's", description = "API's pertaining to refresh DCQ specific cache")
public class DcqCacheController {

	@Autowired
private	Configurator configurator;
	
	@Autowired
	private VodService vodService;
	
	@Autowired
	private DcqCacheService dcqCacheService;
	

	/**
	 * @return
	 * @throws ApplicationException
	 */
	@Deprecated
	@RequestMapping(value = "/RefreshCachedParameters", method = RequestMethod.GET)
	@ApiOperation(value = "refreshDcqCache", notes = "Please find the page for the IFA: https://fleet.alm.accenture.com/avswiki/display/AVSM3DOC/.DCQ+VOD+Ingestor-XML+v6.5")
	public @ResponseBody
	ResponseEntity<GenericResponse> refreshDcqCache() throws ApplicationException {
		GenericResponse genericResponse = null;
		try {
			Configuration config = configurator.getConfiguration(ThreadContext.get(CommonConstants.LogParamsL4j2.TN));
			genericResponse = dcqCacheService.refreshCache(config);
		} catch (ApplicationException ae) {
				throw ae;
			}catch (JAXBException e) {
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR, new NestedParameters(e.getMessage()));
			}
		catch (Exception e) {
				throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR);
			}
		return ResponseEntity.ok(genericResponse);
	}

}
