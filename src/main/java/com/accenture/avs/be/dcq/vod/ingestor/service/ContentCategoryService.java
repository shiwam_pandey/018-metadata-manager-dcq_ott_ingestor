package com.accenture.avs.be.dcq.vod.ingestor.service;

import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.contentcategory.asset.CategoryContent;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;

/**
 * @author karthik.vadla
 *
 */
public interface ContentCategoryService {

	GenericResponse ingestContentCategory(String contentCategoryXml,String transactionNumber,Configuration config) throws Exception;
	
	GenericResponse ingestContentCategory(CategoryContent categoryContent,String transactionNumber,Configuration config) throws Exception;
}
