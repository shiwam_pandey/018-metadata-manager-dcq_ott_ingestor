package com.accenture.avs.be.dcq.vod.ingestor.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.avs.persistence.technicalcatalogue.DCQPlaylistStagingEntity;
/**
 * @author karthik.vadla
 *
 */
@Repository
public interface DcqPlaylistStagingRepository extends JpaRepository<DCQPlaylistStagingEntity,Integer>{
	/**
	 * @param publishedIds
	 */
	@Modifying
	@Transactional
	void deleteChannels(@Param("channelIds") Set<Integer> publishedIds);
	
	
	/**
	 * @param failedAssetIdsList
	 * @return
	 */
	List<DCQPlaylistStagingEntity> retrieveFailedAssets(@Param("channelIds") List<Integer> failedAssetIdsList);
	
	
	/**
	 * @param assetType
	 * @return
	 */
	List<Object[]> retrieveChannelIds(@Param(DCQPlaylistStagingEntity.READY_STATUS) String ready, 
			@Param(DCQPlaylistStagingEntity.PROGRESS_STATUS) String inProgress, 
			@Param(DCQPlaylistStagingEntity.FAILED_STATUS) String failed, 
			@Param(DCQPlaylistStagingEntity.MAX_RETRY) Integer maxRetry);
	
	
	/**
	 * 
	 */
	@Modifying
	@Transactional
	void deleteDuplicateIds();
	
	
	/**
	 * @param rowIds
	 */
	@Modifying
	@Transactional
	void updateStatus(@Param("rowIds") List<Integer> rowIds,
			@Param("assetStatus") String status);

}
