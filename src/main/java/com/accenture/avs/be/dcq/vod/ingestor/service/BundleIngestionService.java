package com.accenture.avs.be.dcq.vod.ingestor.service;

import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;

public interface BundleIngestionService {
	GenericResponse bundleIngestion(String bundleXml, boolean sync,Configuration config) throws Exception;
}
