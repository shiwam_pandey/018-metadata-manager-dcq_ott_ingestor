package com.accenture.avs.be.dcq.vod.ingestor.sdp.manager.impl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;
import javax.xml.ws.WebServiceException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;

import com.accenture.avs.be.dcq.vod.ingestor.dto.CommercialPackagesResponseDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.CommercialPackagesResponseDTO.CommercialPackage;
import com.accenture.avs.be.dcq.vod.ingestor.dto.bundle.asset.Bundle;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.JsonUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.Validator;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ConfigurationException;
import com.accenture.avs.be.framework.utils.Utilities;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.commons.lib.LoggerWrapper.OtherSystemCallType;
import com.accenture.avs.commons.lib.httpclient.HttpClientAdapter;
import com.accenture.avs.commons.lib.httpclient.HttpClientAdapter.HttpClientAdapterConfiguration;
import com.accenture.avs.commons.lib.httpclient.HttpClientAdapter.HttpClientException;
import com.accenture.avs.persistence.technicalcatalogue.ContentEntity;
import com.accenture.sdp.csmfe.webservices.clients.device.SdpDeviceService;
import com.accenture.sdp.csmfe.webservices.clients.device.SdpDeviceServiceBinding;
import com.accenture.sdp.csmfe.webservices.clients.device.SearchAllDeviceChannels;
import com.accenture.sdp.csmfe.webservices.clients.device.SearchAllDeviceChannelsResponse;
import com.accenture.sdp.csmfe.webservices.clients.ingestor.IngestorService;
import com.accenture.sdp.csmfe.webservices.clients.ingestor.IngestorServiceBinding;
import com.accenture.sdp.csmfe.webservices.clients.ingestor.SolutionOfferVodListRequest;
import com.accenture.sdp.csmfe.webservices.clients.ingestor.UpsertSolutionOfferVod;
import com.accenture.sdp.csmfe.webservices.clients.ingestor.UpsertSolutionOfferVodRequest;
import com.accenture.sdp.csmfe.webservices.clients.ingestor.UpsertSolutionOfferVodResponse;
import com.accenture.sdp.csmfe.webservices.clients.solutionoffer.BaseResp;
import com.accenture.sdp.csmfe.webservices.clients.solutionoffer.CreateSolutionOfferAndPackage;
import com.accenture.sdp.csmfe.webservices.clients.solutionoffer.CreateSolutionOfferAndPackageRequest;
import com.accenture.sdp.csmfe.webservices.clients.solutionoffer.CreateSolutionOfferAndPackageResponse;
import com.accenture.sdp.csmfe.webservices.clients.solutionoffer.CreateSolutionOfferResp;
import com.accenture.sdp.csmfe.webservices.clients.solutionoffer.ModifySolutionOfferAndPackage;
import com.accenture.sdp.csmfe.webservices.clients.solutionoffer.ModifySolutionOfferAndPackageRequest;
import com.accenture.sdp.csmfe.webservices.clients.solutionoffer.ModifySolutionOfferAndPackageResponse;
import com.accenture.sdp.csmfe.webservices.clients.solutionoffer.ParameterInfoResp;
import com.accenture.sdp.csmfe.webservices.clients.solutionoffer.PartyGroupNameInfoRequest;
import com.accenture.sdp.csmfe.webservices.clients.solutionoffer.PartyGroupNameListRequest;
import com.accenture.sdp.csmfe.webservices.clients.solutionoffer.PriceCategoryLnkInfoRequest;
import com.accenture.sdp.csmfe.webservices.clients.solutionoffer.PriceCategoryLnkListRequest;
import com.accenture.sdp.csmfe.webservices.clients.solutionoffer.SdpSolutionOfferService;
import com.accenture.sdp.csmfe.webservices.clients.solutionoffer.SdpSolutionOfferServiceBinding;
import com.accenture.sdp.csmfe.webservices.clients.solutionoffer.SolutionOfferChangeStatus;
import com.accenture.sdp.csmfe.webservices.clients.solutionoffer.SolutionOfferChangeStatusRequest;
import com.accenture.sdp.csmfe.webservices.clients.solutionoffer.SolutionOfferChangeStatusResponse;
import com.accenture.sdp.csmfe.webservices.clients.solutionoffer.TechnicalPackageListRequest;

/**
 * @author karthik.vadla
 *
 */
public class SdpClientManagerImpl {

	private static LoggerWrapper logger	= new LoggerWrapper(SdpClientManagerImpl.class);
	private long								SDP_CONNECTION_TIMEOUT;
	private long								SDP_RECEIVE_TIMEOUT;
	private static volatile SdpClientManagerImpl	sdpClientService;

	private SdpClientManagerImpl(long connectionTimeout, long receiveTimeout) {
		this.SDP_CONNECTION_TIMEOUT = connectionTimeout;
		this.SDP_RECEIVE_TIMEOUT = receiveTimeout;
	}
	
	//@Autowired
	//DcqVodIngestorRestUrls dcqVodIngestorRestUrls;
	
	public static SdpClientManagerImpl getSdpClientService(long connectionTimeout, long receiveTimeout) {
		if (sdpClientService == null) {
			 synchronized (SdpClientManagerImpl.class) {
				 if (sdpClientService == null) {
					 sdpClientService = new SdpClientManagerImpl(connectionTimeout, receiveTimeout); 
				 }
			 }
		}
		return sdpClientService;
	}

	public UpsertSolutionOfferVodResponse upsertSolutionOfferVodOnSDP(SolutionOfferVodListRequest soList, Long contentId, String sdpUrlConnection, String sdpTenant) throws ApplicationException, JsonGenerationException, JsonMappingException, IOException {
		UpsertSolutionOfferVodResponse response = null;
		long startTime = System.currentTimeMillis();
		String requestBody = "";
		ObjectMapper mapper = null;
		try {
			IngestorService service = SdpIngestorServiceSingleton.getInstance(sdpUrlConnection);
			IngestorServiceBinding port = service.getIngestorServiceBindingPort();
			Holder<String> arg1 = new Holder<String>(sdpTenant);
			Holder<String> arg2= new Holder<String>(contentId.toString());
			BindingProvider bp = (BindingProvider) port;
			bp.getRequestContext().put("javax.xml.ws.client.connectionTimeout", SDP_CONNECTION_TIMEOUT);
			bp.getRequestContext().put("javax.xml.ws.client.receiveTimeout", SDP_RECEIVE_TIMEOUT);
			UpsertSolutionOfferVod upsertSolOffVod = new UpsertSolutionOfferVod();
			UpsertSolutionOfferVodRequest upsertSolOffVodReq = new UpsertSolutionOfferVodRequest();
			upsertSolOffVodReq.setSolutionOffers(soList);
			upsertSolOffVodReq.setContentId(contentId);
			upsertSolOffVod.setUpsertSolutionOfferVodRequest(upsertSolOffVodReq);
			 mapper = new ObjectMapper();
			requestBody = mapper.writeValueAsString(upsertSolOffVod);
			 logger.logCallToOtherSystemRequestBody(DcqVodIngestorConstants.SDP, DcqVodIngestorConstants.UPSERT_SOL_OFFER_VOD, requestBody,
					OtherSystemCallType.INTERNAL);
			response = port.upsertSolutionOfferVod(upsertSolOffVod, arg1, arg2);
			String responseBody = null;
			if(response!=null){
				responseBody = mapper.writeValueAsString(response);
			}
			logger.logCallToOtherSystemResponseBody(DcqVodIngestorConstants.SDP, DcqVodIngestorConstants.UPSERT_SOL_OFFER_VOD, responseBody,
					OtherSystemCallType.INTERNAL);
			if (response!=null && response.getUpsertSolutionOfferVodResponse() != null && response.getUpsertSolutionOfferVodResponse().getResultCode().equals("000")) {
				logger.logMessage("SolutionOfferVodUpset for contentId = " + contentId + " correctly managed.");
				
			}
			
		} catch (MalformedURLException ex) {
			logger.logError(ex,"Encountered error opening connection to sdp");
		}catch(WebServiceException e){
			logger.logError(e,"Encountered error opening connection to sdp");
			
			throw new ApplicationException(MessageKeys.ERROR_MS_ACN_916_SDP_NOT_AVAILABLE);
		}
		catch(Exception e){
			logger.logError(e,"Encountered error opening connection to sdp");
			throw new ApplicationException(MessageKeys.ERROR_MS_ACN_916_SDP_NOT_AVAILABLE);
		}
		finally {
				logger.logMessage("In finally logs");
				logger.logCallToOtherSystemEnd(DcqVodIngestorConstants.SDP,
						DcqVodIngestorConstants.UPSERT_SOL_OFFER_VOD, requestBody, response!=null && !Utilities.isEmpty(response.getUpsertSolutionOfferVodResponse())
						? mapper.writeValueAsString(response.getUpsertSolutionOfferVodResponse()) : null,
								 response!=null && !Utilities.isEmpty(response.getUpsertSolutionOfferVodResponse())
								? response.getUpsertSolutionOfferVodResponse().getResultCode()
								: DcqVodIngestorConstants.KO,
								 response!=null && !Utilities.isEmpty(response.getUpsertSolutionOfferVodResponse())
								? response.getUpsertSolutionOfferVodResponse().getDescription() : null,
						System.currentTimeMillis() - startTime, OtherSystemCallType.INTERNAL);
				
			
		}
		return response;
	}
	
	public SearchAllDeviceChannelsResponse getDeviceChannels(String sdpUrlConnection, String sdpTenant) throws JsonGenerationException, JsonMappingException, IOException {
		
		SearchAllDeviceChannelsResponse response = null;
		long startTime = System.currentTimeMillis();
		ObjectMapper mapper = null;
		try {
		SdpDeviceService service = SdpDeviceServiceSingleton.getInstance(sdpUrlConnection);
		SdpDeviceServiceBinding port = service.getSdpDeviceServiceBindingPort();
		Holder<String> arg1 = new Holder<String>(sdpTenant);
		Holder<String> arg2= new Holder<String>();
		BindingProvider bp = (BindingProvider) port;
		bp.getRequestContext().put("javax.xml.ws.client.connectionTimeout", SDP_CONNECTION_TIMEOUT);
		bp.getRequestContext().put("javax.xml.ws.client.receiveTimeout", SDP_RECEIVE_TIMEOUT);
		SearchAllDeviceChannels sdpDeviceahnnels = new SearchAllDeviceChannels();
		response = port.searchAllDeviceChannels(sdpDeviceahnnels, arg1, arg2);
		 mapper = new ObjectMapper();
		String responseBody = null;
		if(response!=null){
			responseBody = mapper.writeValueAsString(response);
		}
		logger.logCallToOtherSystemResponseBody(DcqVodIngestorConstants.SDP, DcqVodIngestorConstants.SEARCH_ALL_DEVICE_CHANNELS, responseBody,
				OtherSystemCallType.INTERNAL);
		}
		catch (MalformedURLException e) {
			//DcqVodIngestorUtils.checkServiceUnavailability(ex, errorMessage);
			logger.logError(e,"Encountered error opening connection to sdp");
		}
		catch(WebServiceException e){
			response = null;
			logger.logError(e,"Encountered error opening connection to sdp");
		}catch(Exception e){
			logger.logError(e,"Encountered error opening connection to sdp");
		}finally{
				logger.logMessage("In finally logs");
			logger.logCallToOtherSystemEnd(DcqVodIngestorConstants.SDP,
					DcqVodIngestorConstants.SEARCH_ALL_DEVICE_CHANNELS, null, response!=null && !Utilities.isEmpty(response.getSearchAllDeviceChannelsResponse())
					? mapper.writeValueAsString(response.getSearchAllDeviceChannelsResponse()) : null,
							 response!=null && !Utilities.isEmpty(response.getSearchAllDeviceChannelsResponse())
							? response.getSearchAllDeviceChannelsResponse().getResultCode()
							: DcqVodIngestorConstants.KO,
							 response!=null && !Utilities.isEmpty(response.getSearchAllDeviceChannelsResponse())
							? response.getSearchAllDeviceChannelsResponse().getDescription() : null,
					System.currentTimeMillis() - startTime, OtherSystemCallType.INTERNAL);
		}
		return response;
	}
	
	public Long createSolutionOfferAndPackage(Bundle bundle, List<Long> technicalPackageList, ContentEntity bundleContent,String sdpUrlConnection,String sdpTenant, Boolean isBundle, String externalId, String commerceModel, String priceCategory) throws ApplicationException, JsonGenerationException, JsonMappingException, IOException {
		Long generatedSolutionOfferId = null;
		long startTime = System.currentTimeMillis();
		String requestBody = null;
		CreateSolutionOfferAndPackageResponse createSolutionOfferAndPackageResponse = null;
		ParameterInfoResp paramsResp;
		ObjectMapper mapper = null;
		try{
			logger.logMessage("In createSolutionOfferAndPackage Offer on SDP");
			SdpSolutionOfferServiceBinding port = getSdpSolutionOfferServiceBinding(sdpUrlConnection);
			
			CreateSolutionOfferAndPackageRequest createSolutionOfferAndPackageRequest = new CreateSolutionOfferAndPackageRequest();
			
			createSolutionOfferAndPackageRequest.setSolutionOfferName(DcqVodIngestorConstants.SOL_OFF_BUNDLE_NAME + "_" + bundle.getBundleId() + (DcqVodIngestorUtils.isEmpty(commerceModel)? "" : "_" + commerceModel));
			createSolutionOfferAndPackageRequest.setSolutionOfferTitle(DcqVodIngestorConstants.SOLUTION_OFFER + "_" + bundleContent.getTitle());
			if (bundle.getProductInfo().getPurchaseStartDateTime() != null) {
				createSolutionOfferAndPackageRequest.setStartDate(Validator.Date2XmlGregorianCalendar(Validator.xmlGregorianCalendar2Date(bundle.getProductInfo().getPurchaseStartDateTime())));
			}
			if (bundle.getProductInfo().getPurchaseEndDateTime() != null) {
				createSolutionOfferAndPackageRequest.setEndDate(Validator.Date2XmlGregorianCalendar(Validator.xmlGregorianCalendar2Date(bundle.getProductInfo().getPurchaseEndDateTime())));
			}
			createSolutionOfferAndPackageRequest.setExternalId(externalId);
			createSolutionOfferAndPackageRequest.setSolutionOfferType(isBundle ? DcqVodIngestorConstants.BUNDLE : DcqVodIngestorConstants.BUNDLE_GROUP);
			if (bundle.getProductInfo().getBundleDuration() != null) {
				createSolutionOfferAndPackageRequest.setDuration(bundle.getProductInfo().getBundleDuration().longValue());
			}
			createSolutionOfferAndPackageRequest.setVisible(true);
			PartyGroupNameListRequest partyGroupNameListRequest = new PartyGroupNameListRequest();

			PartyGroupNameInfoRequest groupNameInfoRequest = new PartyGroupNameInfoRequest();
			groupNameInfoRequest.setPartyGroupName(DcqVodIngestorConstants.ALL);
			partyGroupNameListRequest.getPartyGroup().add(groupNameInfoRequest);
			createSolutionOfferAndPackageRequest.setPartyGroups(partyGroupNameListRequest);

			createSolutionOfferAndPackageRequest.setCommerceModel((DcqVodIngestorUtils.isEmpty(commerceModel) ? null : commerceModel));
			
			TechnicalPackageListRequest ids = new TechnicalPackageListRequest();
			ids.getTechnicalPackageId().addAll(technicalPackageList);
			createSolutionOfferAndPackageRequest.setTechnicalPackageIds(ids);
			
			PriceCategoryLnkListRequest lnkListRequest = new PriceCategoryLnkListRequest();
			PriceCategoryLnkInfoRequest priceCategoryLnkInfoRequest = new PriceCategoryLnkInfoRequest();
			priceCategoryLnkInfoRequest.setPaymentTypeId(1L);
			priceCategoryLnkInfoRequest.setPriceCategoryName(priceCategory);
			lnkListRequest.getPriceCategory().add(priceCategoryLnkInfoRequest);
			createSolutionOfferAndPackageRequest.setNrcPrices(lnkListRequest);

			CreateSolutionOfferAndPackage createSolutionOfferAndPackage = new CreateSolutionOfferAndPackage();
			createSolutionOfferAndPackage.setCreateSolutionOfferRequest(createSolutionOfferAndPackageRequest);
			Holder<String> arg1 = new Holder<String>(sdpTenant);
			Holder<String> arg2 = new Holder<String>(bundleContent.getContentId().toString());
			 mapper = new ObjectMapper();
			requestBody = mapper.writeValueAsString(createSolutionOfferAndPackage);
			 logger.logCallToOtherSystemRequestBody(DcqVodIngestorConstants.SDP, DcqVodIngestorConstants.CREATE_SOLOFFER_AND_PACKAGE, requestBody,
					OtherSystemCallType.INTERNAL);
			 createSolutionOfferAndPackageResponse = port.createSolutionOfferAndPackage(createSolutionOfferAndPackage, arg1, arg2);
			String responseBody = null;
			CreateSolutionOfferResp createSolutionOfferResponse = null;
			if(createSolutionOfferAndPackageResponse!=null){
				responseBody = mapper.writeValueAsString(createSolutionOfferAndPackageResponse);
				 createSolutionOfferResponse = createSolutionOfferAndPackageResponse.getCreateSolutionOfferAndPackageResponse();
			}
			logger.logCallToOtherSystemResponseBody(DcqVodIngestorConstants.SDP, DcqVodIngestorConstants.CREATE_SOLOFFER_AND_PACKAGE, responseBody,
					OtherSystemCallType.INTERNAL);
			
			
			
			if (createSolutionOfferResponse != null) {
				logger.logMessage("In createSolutionOfferAndPackageResponse Description" + createSolutionOfferResponse.getDescription());

				if (createSolutionOfferResponse.getParameters() != null && createSolutionOfferResponse.getParameters().getParameter() != null && !createSolutionOfferResponse.getParameters().getParameter().isEmpty()) {

					for (ParameterInfoResp infoResp : createSolutionOfferResponse.getParameters().getParameter()) {
						logger.logMessage("In createSolutionOfferAndPackageResponse " + infoResp.getName() + " " + infoResp.getValue());
					}
				}
				if (createSolutionOfferResponse.getResultCode() != null && createSolutionOfferResponse.getResultCode().equals("000")) {
					generatedSolutionOfferId = createSolutionOfferResponse.getSolutionOfferId();
					logger.logMessage("In Create Solution Offer on Soluition Id is" + generatedSolutionOfferId);
					logger.logMessage("CreateSolutionOfferAndPackage ResultCode:" + createSolutionOfferResponse.getResultCode());
				}
			}
			
		}catch (MalformedURLException e) {
			logger.logError(e,"Encountered error opening connection to sdp");
		}catch(WebServiceException e){
			logger.logError(e,"Encountered error opening connection to sdp");
			throw new ApplicationException(MessageKeys.ERROR_MS_ACN_916_SDP_NOT_AVAILABLE);
		}
		catch(Exception e){
			logger.logError(e,"Encountered error opening connection to sdp");
			throw new ApplicationException(MessageKeys.ERROR_MS_ACN_916_SDP_NOT_AVAILABLE);
		}
		finally{
				
				logger.logMessage("In finally logs");
			logger.logCallToOtherSystemEnd(DcqVodIngestorConstants.SDP,
					DcqVodIngestorConstants.CREATE_SOLOFFER_AND_PACKAGE, requestBody,createSolutionOfferAndPackageResponse!=null&& !Utilities.isEmpty(createSolutionOfferAndPackageResponse.getCreateSolutionOfferAndPackageResponse())
					? mapper.writeValueAsString(createSolutionOfferAndPackageResponse.getCreateSolutionOfferAndPackageResponse()) : null,
							createSolutionOfferAndPackageResponse!=null && !Utilities.isEmpty(createSolutionOfferAndPackageResponse.getCreateSolutionOfferAndPackageResponse())
							? createSolutionOfferAndPackageResponse.getCreateSolutionOfferAndPackageResponse().getResultCode()
							: DcqVodIngestorConstants.KO,
							createSolutionOfferAndPackageResponse!=null && !Utilities.isEmpty(createSolutionOfferAndPackageResponse.getCreateSolutionOfferAndPackageResponse())
							? createSolutionOfferAndPackageResponse.getCreateSolutionOfferAndPackageResponse().getDescription() : null,
					System.currentTimeMillis() - startTime, OtherSystemCallType.INTERNAL);
		}
		return generatedSolutionOfferId;
		
	}


	private SdpSolutionOfferServiceBinding getSdpSolutionOfferServiceBinding(String sdpUrlConnection)
			throws MalformedURLException {
		SdpSolutionOfferService service = SdpSolutionOfferServiceSingleton.getInstance(sdpUrlConnection);
		SdpSolutionOfferServiceBinding port = service.getSdpSolutionOfferServiceBindingPort();
		BindingProvider bp = (BindingProvider) port;
		bp.getRequestContext().put("javax.xml.ws.client.connectionTimeout", SDP_CONNECTION_TIMEOUT);
		bp.getRequestContext().put("javax.xml.ws.client.receiveTimeout", SDP_RECEIVE_TIMEOUT);
		return port;
	}
	
	public boolean changeStatusSolutionOfferOnSDP(Long solutionOfferId, String status,String sdpUrlConnection,String sdpTenant) throws ApplicationException, JsonGenerationException, JsonMappingException, IOException {
		boolean changeStatusOK = true;
		long startTime = System.currentTimeMillis();
		String requestBody= "";
		SolutionOfferChangeStatusResponse response = null;
		ObjectMapper mapper = null;
		try {
			SdpSolutionOfferServiceBinding port = getSdpSolutionOfferServiceBinding(sdpUrlConnection);
			SolutionOfferChangeStatus solChangeStatus = new SolutionOfferChangeStatus();
			SolutionOfferChangeStatusRequest solChangeStatusRequest = new SolutionOfferChangeStatusRequest();
			Holder<String> arg1 = new Holder<String>(sdpTenant);
			Holder<String> arg2 = new Holder<String>(solutionOfferId.toString());
			
			solChangeStatusRequest.setSolutionOfferId(solutionOfferId);
			solChangeStatusRequest.setStatus(status);
			solChangeStatus.setSolutionOfferChangeStatusRequest(solChangeStatusRequest);
			
			 mapper = new ObjectMapper();
			requestBody = mapper.writeValueAsString(solChangeStatus);
			 logger.logCallToOtherSystemRequestBody(DcqVodIngestorConstants.SDP, DcqVodIngestorConstants.SOL_OFFER_CHANGE_STATUS, requestBody,
					OtherSystemCallType.INTERNAL);
			
			 response = port.solutionOfferChangeStatus(solChangeStatus, arg1, arg2);
			
			String responseBody = null;
			if(response!=null){
				responseBody = mapper.writeValueAsString(response);
			}
			logger.logCallToOtherSystemResponseBody(DcqVodIngestorConstants.SDP, DcqVodIngestorConstants.SOL_OFFER_CHANGE_STATUS, responseBody,
					OtherSystemCallType.INTERNAL);
			if(response!=null){
			if (response.getSolutionOfferChangeStatusResponse().getResultCode().equals("000")) {
				logger.logMessage("SolutionOfferVod with solutionId " + solutionOfferId + " correctly changedStatus to " + status);
			} else {
				changeStatusOK = false;
				logger.logMessage("Sdp Service in error during changeStatus of the solutionOfferId " + solutionOfferId);
				logger.logMessage("Sdp Service responseCode:'" + response.getSolutionOfferChangeStatusResponse().getResultCode() + "' and responseDescritpion:'" + response.getSolutionOfferChangeStatusResponse().getDescription() + "'");
				if (response.getSolutionOfferChangeStatusResponse().getParameters().getParameter() != null && response.getSolutionOfferChangeStatusResponse().getParameters().getParameter().size() > 0) {
					logger.logMessage("Sdp Service in error for parameter:'" + response.getSolutionOfferChangeStatusResponse().getParameters().getParameter().get(0).getName() + "' with value :'" + response.getSolutionOfferChangeStatusResponse().getParameters().getParameter().get(0).getValue() + "'");
				}
			}
			}
			
		} catch (MalformedURLException e) {
			logger.logError(e,"Encountered error opening connection to sdp");
		}catch(WebServiceException e){
			logger.logError(e,"Encountered error opening connection to sdp");
			throw new ApplicationException(MessageKeys.ERROR_MS_ACN_916_SDP_NOT_AVAILABLE);
		}
		catch(Exception e){
			logger.logError(e,"Encountered error opening connection to sdp");
			throw new ApplicationException(MessageKeys.ERROR_MS_ACN_916_SDP_NOT_AVAILABLE);
		}finally{			
				logger.logMessage("In finally logs");
			logger.logCallToOtherSystemEnd(DcqVodIngestorConstants.SDP,
					DcqVodIngestorConstants.SOL_OFFER_CHANGE_STATUS, requestBody, response!=null && !Utilities.isEmpty(response.getSolutionOfferChangeStatusResponse())
					? mapper.writeValueAsString(response.getSolutionOfferChangeStatusResponse()) : null,
							 response!=null && !Utilities.isEmpty(response.getSolutionOfferChangeStatusResponse())
							? response.getSolutionOfferChangeStatusResponse().getResultCode()
							: DcqVodIngestorConstants.KO,
							 response!=null && !Utilities.isEmpty(response.getSolutionOfferChangeStatusResponse())
							? response.getSolutionOfferChangeStatusResponse().getDescription() : null,
					System.currentTimeMillis() - startTime, OtherSystemCallType.INTERNAL);
		}
		return changeStatusOK;
	}
	
	public boolean updateSolutionOfferAndPackage(Bundle bundle, List<Long> technicalPackageList, ContentEntity bundleContent, Long solutionOfferId, String sdpUrlConnection,String sdpTenant, boolean isBundle, String externalId, String commerceModel, String priceCategory) throws ApplicationException, JsonGenerationException, JsonMappingException, IOException {
		boolean generatedSolutionFlag = false;
		long startTime = System.currentTimeMillis();
		String requestBody = "";
		ModifySolutionOfferAndPackageResponse modifySolutionOfferAndPackageResponse = null;
		ObjectMapper mapper = null;
		try {
			/* for ModifySolutionOfferAndPackage */
			logger.logMessage("In updateSolutionOfferAndPackage Offer on SDP");
			SdpSolutionOfferServiceBinding port = getSdpSolutionOfferServiceBinding(sdpUrlConnection);
			ModifySolutionOfferAndPackageRequest modifySolutionOfferAndPackageRequest = new ModifySolutionOfferAndPackageRequest();
			modifySolutionOfferAndPackageRequest.setSolutionOfferName(DcqVodIngestorConstants.SOL_OFF_BUNDLE_NAME + "_" + bundle.getBundleId() + (commerceModel == null || commerceModel.trim().isEmpty() ? "" : "_" + commerceModel));
			modifySolutionOfferAndPackageRequest.setSolutionOfferTitle(DcqVodIngestorConstants.SOLUTION_OFFER + "_" + bundleContent.getTitle());
			modifySolutionOfferAndPackageRequest.setSolutionOfferId(solutionOfferId);
			if (bundle.getProductInfo().getPurchaseStartDateTime() != null) {
				modifySolutionOfferAndPackageRequest.setStartDate(Validator.Date2XmlGregorianCalendar(Validator.xmlGregorianCalendar2Date(bundle.getProductInfo().getPurchaseStartDateTime())));
			}
			if (bundle.getProductInfo().getPurchaseEndDateTime() != null) {
				modifySolutionOfferAndPackageRequest.setEndDate(Validator.Date2XmlGregorianCalendar(Validator.xmlGregorianCalendar2Date(bundle.getProductInfo().getPurchaseEndDateTime())));
			}
			if (bundle.getProductInfo().getBundleDuration() != null) {
				modifySolutionOfferAndPackageRequest.setDuration(bundle.getProductInfo().getBundleDuration().longValue());
			}
			modifySolutionOfferAndPackageRequest.setExternalId(externalId);
			modifySolutionOfferAndPackageRequest.setSolutionOfferType(isBundle ? DcqVodIngestorConstants.BUNDLE : DcqVodIngestorConstants.BUNDLE_GROUP);
			modifySolutionOfferAndPackageRequest.setVisible(true);

			PartyGroupNameListRequest partyGroupNameListRequest = new PartyGroupNameListRequest();
			PartyGroupNameInfoRequest groupNameInfoRequest = new PartyGroupNameInfoRequest();
			groupNameInfoRequest.setPartyGroupName(DcqVodIngestorConstants.ALL);
			partyGroupNameListRequest.getPartyGroup().add(groupNameInfoRequest);
			modifySolutionOfferAndPackageRequest.setPartyGroups(partyGroupNameListRequest);

			modifySolutionOfferAndPackageRequest.setCommerceModel((commerceModel == null || commerceModel.trim().isEmpty() ? null : commerceModel));

			TechnicalPackageListRequest ids = new TechnicalPackageListRequest();
			ids.getTechnicalPackageId().addAll(technicalPackageList);
			modifySolutionOfferAndPackageRequest.setTechnicalPackageIds(ids);

			PriceCategoryLnkListRequest lnkListRequest = new PriceCategoryLnkListRequest();
			PriceCategoryLnkInfoRequest priceCategoryLnkInfoRequest = new PriceCategoryLnkInfoRequest();
			priceCategoryLnkInfoRequest.setPaymentTypeId(1L);
			priceCategoryLnkInfoRequest.setPriceCategoryName(priceCategory);
			lnkListRequest.getPriceCategory().add(priceCategoryLnkInfoRequest);
			modifySolutionOfferAndPackageRequest.setNrcPrices(lnkListRequest);

			ModifySolutionOfferAndPackage modifySolutionOfferAndPackage = new ModifySolutionOfferAndPackage();
			modifySolutionOfferAndPackage.setModifySolutionOfferAndPackageRequest(modifySolutionOfferAndPackageRequest);
			Holder<String> arg1 = new Holder<String>(sdpTenant);
			Holder<String> arg2 = new Holder<String>(bundleContent.getContentId().toString());
			 mapper = new ObjectMapper();
			requestBody = mapper.writeValueAsString(modifySolutionOfferAndPackage);
			 logger.logCallToOtherSystemRequestBody(DcqVodIngestorConstants.SDP, DcqVodIngestorConstants.MOD_SOLOFFER_PACKAGE, requestBody,
					OtherSystemCallType.INTERNAL);
			 modifySolutionOfferAndPackageResponse = port.modifySolutionOfferAndPackage(modifySolutionOfferAndPackage, arg1, arg2);
			
			String responseBody = null;
			BaseResp response = null;
			if(modifySolutionOfferAndPackageResponse!=null){
				responseBody = mapper.writeValueAsString(modifySolutionOfferAndPackageResponse);
				response = modifySolutionOfferAndPackageResponse.getModifySolutionOfferAndPackageResponse();
			}
			logger.logCallToOtherSystemResponseBody(DcqVodIngestorConstants.SDP, DcqVodIngestorConstants.MOD_SOLOFFER_PACKAGE, responseBody,
					OtherSystemCallType.INTERNAL);
			
			
			if (response != null && response.getResultCode() != null) {
				if (response.getParameters() != null && response.getParameters().getParameter() != null && !response.getParameters().getParameter().isEmpty()) {
					for (ParameterInfoResp infoResp : response.getParameters().getParameter()) {
						logger.logMessage("In ModifySolutionOfferAndPackage " + infoResp.getName() + " " + infoResp.getValue());
					}
				}
				logger.logMessage("ModifySolutionOfferAndPackage ResultCode:" + modifySolutionOfferAndPackageResponse.getModifySolutionOfferAndPackageResponse().getResultCode());
				logger.logMessage("ModifySolutionOfferAndPackage ResultCode:" + modifySolutionOfferAndPackageResponse.getModifySolutionOfferAndPackageResponse().getDescription());
				if (modifySolutionOfferAndPackageResponse.getModifySolutionOfferAndPackageResponse().getResultCode() != null && modifySolutionOfferAndPackageResponse.getModifySolutionOfferAndPackageResponse().getResultCode().equals("000")) {
					generatedSolutionFlag = true;
				}
			}
		}  catch (MalformedURLException e) {
			logger.logError(e,"Encountered error opening connection to sdp");
		}catch(WebServiceException e){
			logger.logError(e,"Encountered error opening connection to sdp");
			throw new ApplicationException(MessageKeys.ERROR_MS_ACN_916_SDP_NOT_AVAILABLE);
		}
		catch(Exception e){
			logger.logError(e,"Encountered error opening connection to sdp");
			throw new ApplicationException(MessageKeys.ERROR_MS_ACN_916_SDP_NOT_AVAILABLE);
		}
		finally{
								logger.logCallToOtherSystemEnd(DcqVodIngestorConstants.SDP,
						DcqVodIngestorConstants.MOD_SOLOFFER_PACKAGE, requestBody,modifySolutionOfferAndPackageResponse!=null && !Utilities.isEmpty( modifySolutionOfferAndPackageResponse.getModifySolutionOfferAndPackageResponse())
						? mapper.writeValueAsString(modifySolutionOfferAndPackageResponse.getModifySolutionOfferAndPackageResponse()) : null,
								modifySolutionOfferAndPackageResponse!=null && !Utilities.isEmpty(modifySolutionOfferAndPackageResponse.getModifySolutionOfferAndPackageResponse())
								? modifySolutionOfferAndPackageResponse.getModifySolutionOfferAndPackageResponse().getResultCode()
								: DcqVodIngestorConstants.KO,
								modifySolutionOfferAndPackageResponse!=null && !Utilities.isEmpty(modifySolutionOfferAndPackageResponse.getModifySolutionOfferAndPackageResponse())
								? modifySolutionOfferAndPackageResponse.getModifySolutionOfferAndPackageResponse().getDescription() : null,
						System.currentTimeMillis() - startTime, OtherSystemCallType.INTERNAL);
		}
		return generatedSolutionFlag;
	}
	
	public CommercialPackage getSdpSolutionOffer(String externalId, String getCommercialPackageDetailsUrl,String sdpTenant,Configuration config) throws ConfigurationException, ApplicationException, JsonParseException, JsonMappingException, IOException{
		CommercialPackagesResponseDTO commercialPackageDto = null;
		CommercialPackage commercialPackage = null;
		Map<String, String> paramsMap = new HashMap<>();
        Map<String, String> headersMap = new HashMap<>();
    	long startTime = System.currentTimeMillis();
    	//String getCommercialPackageDetailsUrl= DcqVodIngestorConstants.URLS.COMMERCIAL_PACKAGE_DETAILS_V2_URL;
		 HttpClientAdapterConfiguration httpconfig = DcqVodIngestorUtils.getHttpClientAdapterConfiguration(sdpTenant, config);
		 if(externalId != null){
			 paramsMap.put(DcqVodIngestorConstants.EXTERNAL_ID, externalId);
		 }
		 String response = null;
	        GenericResponse genericResponse = null;
			try {
	               response = HttpClientAdapter.doGet(httpconfig, getCommercialPackageDetailsUrl, paramsMap, null,headersMap);
	               ObjectMapper mapper = new ObjectMapper();
	               String responseBody = null;
	   			if(response!=null){
	   				responseBody = mapper.writeValueAsString(response);
	   			}
	   			logger.logCallToOtherSystemResponseBody(DcqVodIngestorConstants.SDP, DcqVodIngestorConstants.GET_COMMERCIAL_PACKAGES, responseBody,
	   					OtherSystemCallType.INTERNAL);
	   			
	   			mapper.enable(SerializationConfig.Feature.INDENT_OUTPUT);
				mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	   			JsonNode jsonNode = mapper.readTree(response);
	   			
	   			commercialPackageDto =mapper.readValue(jsonNode.findValue(DcqVodIngestorConstants.RESPONSE_RESULT_OBJ),CommercialPackagesResponseDTO.class);	
	   			if(!Utilities.isEmpty(commercialPackageDto)){
	   				if(!Utilities.isEmptyCollection(commercialPackageDto.getCommercialPackages())) {
	   				commercialPackage = commercialPackageDto.getCommercialPackages().get(0);
					logger.logCallToOtherSystemEnd(DcqVodIngestorConstants.SDP,DcqVodIngestorConstants.GET_COMMERCIAL_PACKAGES, "","OK","","", System.currentTimeMillis()-startTime, OtherSystemCallType.INTERNAL);
				}
	   			}
	   			
	        }
	        
			catch (HttpClientException ex) {
				genericResponse = JsonUtils.parseJson( ex.getResponseBody(),GenericResponse.class);
					logger.logError(ex,"Encountered error opening connection to sdp");
					DcqVodIngestorUtils.checkServiceUnavailability(ex, MessageKeys.ERROR_MS_ACN_901_COMMERCE_NOT_AVAILABLE);
			}catch(Exception e) {
				logger.logError(e);
			}finally{
				DcqVodIngestorUtils.logExternalSystemEnd(genericResponse, DcqVodIngestorConstants.SDP, DcqVodIngestorConstants.GET_COMMERCIAL_PACKAGES, 
						null,startTime, OtherSystemCallType.INTERNAL, logger);
			}
		return commercialPackage;
	}
}
