package com.accenture.avs.be.dcq.vod.ingestor.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.manager.CategoryIngestManager;
import com.accenture.avs.be.dcq.vod.ingestor.manager.ContentIngestManager;
import com.accenture.avs.be.dcq.vod.ingestor.manager.DcqVodIngestionManager;
import com.accenture.avs.be.dcq.vod.ingestor.manager.VlcContentIngestManager;
import com.accenture.avs.be.dcq.vod.ingestor.manager.VodChannelIngestManager;
import com.accenture.avs.be.dcq.vod.ingestor.repository.DcqAssetStagingRepository;
import com.accenture.avs.be.dcq.vod.ingestor.timers.DcqVodIngestorInitialization;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.writer.DcqVodIngestionWriter;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.commons.lib.LoggerWrapper;

/**
 * @author karthik.vadla
 *
 */
@Component
public class DcqVodIngestionManagerImpl implements DcqVodIngestionManager{
	private static LoggerWrapper log = new LoggerWrapper(DcqVodIngestorInitialization.class);
	
	@Autowired
	private DcqAssetStagingRepository dcqAssetStagingRepository;
	
	@Autowired
	private ContentIngestManager contentIngestManager;
	@Autowired
	private CategoryIngestManager categoryIngestManager;
	@Autowired
	private VodChannelIngestManager vodChannelIngestManager;
	@Autowired
	private VlcContentIngestManager vlcContentIngestManager;
	@Autowired
	private DcqVodIngestionWriter dcqVodIngestionWriter;
	
	@Override
	public boolean invokeVodProcedures(Configuration config) {
		dcqAssetStagingRepository.POPULATE_DCQ_ASSET_STAGING_CONTENT("return_message");
		dcqAssetStagingRepository.POPULATE_DCQ_ASSET_STAGING_CATEGORY("return_message");
		dcqAssetStagingRepository.POPULATE_DCQ_ASSET_STAGING_VODCHANNEL("return_message");
		dcqAssetStagingRepository.POPULATE_DCQ_PLAYLIST_STAGING("return_message");
		return true;
	}

	/* (non-Javadoc)
	 * @see com.accenture.avs.be.dcq.vod.manager.DcqVodIngestionManager#initiateIngestors(java.lang.String, java.lang.String, java.lang.String, com.accenture.avs.be.framework.cache.Configuration)
	 */
	@Override
	public synchronized void initiateIngestors(String ingestionType, String mode, String indexDate, Configuration config) {
		log.logMessage("Start the Ingestors for ingestionType: {}", ingestionType);
		if (ingestionType.equals(DcqVodIngestorConstants.ALL)) {
			initiateContentIngestor(mode,indexDate, config);
			initiateCategoryIngestor(mode,indexDate, config);
			initiateVodChannelIngestor(mode,indexDate, config);
			initiateVlcContentIngestor(mode,indexDate,config);
			//log.logMessage("Completed all Ingestors.");
		} else if (ingestionType.equals(DcqVodIngestorConstants.CONTENT)) {
			initiateContentIngestor(mode,indexDate,config);
		} else if (ingestionType.equals(DcqVodIngestorConstants.CATEGORY)) {
			initiateCategoryIngestor(mode,indexDate,config);
		} else if (ingestionType.equals(DcqVodIngestorConstants.VODCHANNEL)) {
			initiateVodChannelIngestor(mode,indexDate,config);
		} else if (ingestionType.equals(DcqVodIngestorConstants.VLCCONTENT)) {
			initiateVlcContentIngestor(mode,indexDate,config);
		}

		
	}

	private void initiateContentIngestor(String mode, String indexDate, Configuration config) {
		long startTime = System.currentTimeMillis();
		log.logMessage("****************** Content Ingestor - Start. ******************");
		contentIngestManager.initiateContentIngestor(mode, indexDate, config);
		log.logMethodEnd(System.currentTimeMillis()-startTime);
	}
	
	private void initiateVlcContentIngestor(String mode, String indexDate, Configuration config) {
		long startTime = System.currentTimeMillis();
		log.logMessage("****************** VlcContent Ingestor - Start. ******************");
		vlcContentIngestManager.intializeVlcContentIngestor(mode, indexDate, config);
		log.logMethodEnd(System.currentTimeMillis()-startTime);
	}

	private void initiateVodChannelIngestor(String mode, String indexDate, Configuration config) {
		long startTime = System.currentTimeMillis();
		log.logMessage("****************** VodChannel Ingestor - Start. ******************");
		vodChannelIngestManager.intializeVodChannelIngestor(mode, indexDate, config);
		log.logMethodEnd(System.currentTimeMillis()-startTime);
	}

	private void initiateCategoryIngestor(String mode, String indexDate, Configuration config) {
		long startTime = System.currentTimeMillis();
		log.logMessage("****************** Category Ingestor - Start. ******************");
		categoryIngestManager.initializeCategoryIngestor(mode, indexDate, config);
		log.logMethodEnd(System.currentTimeMillis()-startTime);
		
	}


	@Override
	public void initiateAliasProcess(String mode, Configuration config) throws ApplicationException {
		
		if(mode.equalsIgnoreCase(DcqVodIngestorConstants.SWITCH)) {
			dcqVodIngestionWriter.switchAlias(config);
		}else {
			dcqVodIngestionWriter.rollbackAlias(config);
		}
	}

}
