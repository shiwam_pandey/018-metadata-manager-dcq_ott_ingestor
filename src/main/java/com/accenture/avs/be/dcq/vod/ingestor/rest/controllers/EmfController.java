package com.accenture.avs.be.dcq.vod.ingestor.rest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.avs.be.dcq.vod.ingestor.dto.Response;
import com.accenture.avs.be.dcq.vod.ingestor.service.EmfService;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.framework.annotation.AvsRestController;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.exception.ApplicationException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@AvsRestController
@Api(tags="EMF Services",value = "Emf API's", description = "API's pertaining to activate,deactivate emf")
public class EmfController {

	@Autowired
private	EmfService emfService;
	
	@Deprecated
	@RequestMapping(value = "/activationEMF", method = RequestMethod.PUT)
	@ApiOperation(value="activateEMF (For backward compatibility)",notes = "Please find the page for the IFA: https://fleet.alm.accenture.com/avswiki/display/AVSM3DOC/.DCQ+VOD+Ingestor-XML+v6.5. \n To activate EMF by emfName, Extended metadata fields are used while ingesting a VOD content")
	@ApiImplicitParams({
		@ApiImplicitParam(name="emfName", value="emf Name", dataType="string")})
	@ApiResponses({
		@ApiResponse(code=400, message="ERR001-VALIDATION: Missing Parameter"
				+ "\nERR001-VALIDATION: Invalid parameter"),
		@ApiResponse(code=500, message="ERR004-GENERIC: GENERIC ERROR")
	})
	public @ResponseBody ResponseEntity<Response> setEmfActive(@RequestParam(required = false) String emfName) throws ApplicationException {
		Response response = new Response();
		GenericResponse emfResp=new GenericResponse();
		try {
			emfResp =  emfService.activateOrDeactivateEmf(emfName, DcqVodIngestorConstants.EMFCONSTANTS.PUT_IS_ACTIVE);
			response.setResultObj(emfResp.getResultObj());
		}catch (ApplicationException ae) {
			if(ae.getExceptionId().contains("3019") && ae.getParameters().getParameters()[0].contains("does not exists")) {
				response.setResultCode(DcqVodIngestorConstants.KO);
				response.setErrorCode("ERR015");
				response.setErrorDescription("EMF does not exist on AVS database.");
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
				}else {
					response.setResultCode(DcqVodIngestorConstants.KO);
					response.setErrorCode("ERR013");
					response.setErrorDescription("EMFID OR EMF Name Invalid.");
					return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
				}
			}
		
		catch (Exception e) {
			response.setResultCode(DcqVodIngestorConstants.KO);
			response.setErrorCode("ERR004-GENERIC");
			response.setErrorDescription(e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			
			}
		return ResponseEntity.ok(response);
		
	}
	
	//deactive the EMF isActive FLag
	@Deprecated
	@RequestMapping(value = "/activationEMF", method = RequestMethod.DELETE)
	@ApiOperation(value="deactivateEMF (For backward compatibility)",notes = "Please find the page for the IFA: https://fleet.alm.accenture.com/avswiki/display/AVSM3DOC/.DCQ+VOD+Ingestor-XML+v6.5. \n To deactivate EMF by emfName, Extended metadata fields are used while ingesting a VOD content")
	@ApiImplicitParams({
		@ApiImplicitParam(name="emfName", value="emf Name", dataType="string")})
	@ApiResponses({
		@ApiResponse(code=400, message="ERR001-VALIDATION: Missing Parameter"
				+ "\nERR001-VALIDATION: Invalid parameter"),
		@ApiResponse(code=500, message="ERR004-GENERIC: GENERIC ERROR")
	})
	public @ResponseBody ResponseEntity<Response> setEmfInActive(@RequestParam(required = false) String emfName) throws ApplicationException{
		Response response = new Response();
		GenericResponse emfResp=new GenericResponse();
		try {
			emfResp = emfService.activateOrDeactivateEmf(emfName, DcqVodIngestorConstants.EMFCONSTANTS.DELETE_IS_ACTIVE);
			response.setResultObj(emfResp.getResultObj());
		}catch (ApplicationException ae) {
			if(ae.getExceptionId().contains("3019") && ae.getParameters().getParameters()[0].contains("does not exists")) {
				response.setResultCode(DcqVodIngestorConstants.KO);
				response.setErrorCode("ERR015");
				response.setErrorDescription("EMF does not exist on AVS database.");
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
				}else {
					response.setResultCode(DcqVodIngestorConstants.KO);
					response.setErrorCode("ERR013");
					response.setErrorDescription("EMFID OR EMF Name Invalid.");
					return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
				}
			}
		
		catch (Exception e) {
			response.setResultCode(DcqVodIngestorConstants.KO);
			response.setErrorCode("ERR004-GENERIC");
			response.setErrorDescription(e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			}
		return ResponseEntity.ok(response);
		}
}

