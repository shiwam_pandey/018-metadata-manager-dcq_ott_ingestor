package com.accenture.avs.be.dcq.vod.ingestor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.avs.persistence.technicalcatalogue.RelContentExtRatEntity;
/**
 * @author karthik.vadla
 *
 */
@Repository
public interface RelContentExtRatRepository extends JpaRepository<RelContentExtRatEntity,Integer>{

	@Modifying
	@Transactional(propagation=Propagation.REQUIRED)
	void deleteByContentId(@Param("contentId")Integer contentId);

	
}
