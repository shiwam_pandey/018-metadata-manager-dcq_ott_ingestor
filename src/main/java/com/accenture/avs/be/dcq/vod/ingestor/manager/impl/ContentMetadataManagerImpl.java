package com.accenture.avs.be.dcq.vod.ingestor.manager.impl;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.accenture.avs.be.dcq.vod.ingestor.cache.LanguageCache;
import com.accenture.avs.be.dcq.vod.ingestor.dto.DCQEventCollectorContentDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.DCQGlobalEventCollectorContentDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionWriterRequestDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionWriterResponseDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.EsContentDoc;
import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Asset;
import com.accenture.avs.be.dcq.vod.ingestor.factories.GatewayFactory;
import com.accenture.avs.be.dcq.vod.ingestor.manager.ContentMetadataManager;
import com.accenture.avs.be.dcq.vod.ingestor.processor.DcqVodIngestionPostProcessor;
import com.accenture.avs.be.dcq.vod.ingestor.processor.VarnishCleanerManager;
import com.accenture.avs.be.dcq.vod.ingestor.processorplugin.DcqVodIngestionProcessorPlugin;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ContentPlatformRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ContentRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ExtendedContentAttributesRepository;
import com.accenture.avs.be.dcq.vod.ingestor.utils.CacheConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.JsonUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.VodUtilities;
import com.accenture.avs.be.dcq.vod.ingestor.utils.XmlHelper;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.VodRequestDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.utils.VodUtilitiesV1;
import com.accenture.avs.be.dcq.vod.ingestor.writer.DcqVodIngestionWriter;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.cache.types.impl.PlatformsRemoteCache.PlatformValue;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ConfigurationException;
import com.accenture.avs.be.framework.utils.BooleanConverter;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.commons.lib.LoggerWrapper.OtherSystemCallType;
import com.accenture.avs.es.manager.ElasticSearchIntegratedClientManager;
import com.accenture.avs.persistence.technicalcatalogue.ContentEntity;

/**
 * @author karthik.vadla
 *
 */
@Component
public class ContentMetadataManagerImpl implements ContentMetadataManager {
	private static final LoggerWrapper log = new LoggerWrapper(ContentMetadataManagerImpl.class);

	@Autowired
	private ApplicationContext context;
	@Autowired
	private ContentRepository contentRepository;
	@Autowired
	private ContentPlatformRepository contentPlatformRepository;
	@Autowired
	private ExtendedContentAttributesRepository extendedContentAttributesRepository;

	
	@Autowired
	private LanguageCache languageCache;

	@Autowired
	private DcqVodIngestionWriter dcqVodIngestionWriter;
	@Autowired
	private DcqVodIngestionPostProcessor dcqVodIngestionPostProcessor;
	@Autowired
	private DcqAssetStagingManager dcqAssetStagingManager;
	@Autowired
	private VarnishCleanerManager varnishCleanerManager;
	private static JAXBContext jcAsset;
	static {
		try {
			jcAsset = JAXBContext.newInstance(Asset.class);
		} catch (Exception e) {
			log.logError(e);
		}
	}
	@Autowired
	private VodUtilities vodUtilities;
	@Autowired
	private VodUtilitiesV1 vodUtilitiesV1;

	@Override
	public List<DcqVodIngestionWriterRequestDTO> processandWriteContents(List<Integer> assetIdList,
			String transactionNumber, String startTime, String mode, String indexDate, Configuration config)
			throws JsonParseException, JsonMappingException, IOException, ApplicationException, ConfigurationException {

		List<DcqVodIngestionWriterRequestDTO> writerRequestDTOList = new ArrayList<DcqVodIngestionWriterRequestDTO>();
		Set<Integer> publishedIds = new HashSet<Integer>();
		int sizeOfWriterBuffer = Integer.parseInt(config.getConstants().getValue(CacheConstants.DCQ_VOD_WRITER_BUFFER_SIZE));
		int numOfAssets = assetIdList.size();
		int i = 0;
		HashMap<Integer, String> deletedMap = new HashMap<Integer, String>();

		for (Integer contentId : assetIdList) {

			List<DcqVodIngestionWriterRequestDTO> writerRequestDTOs = constructEsDocsForContent(contentId,
					transactionNumber, startTime, mode, indexDate, config);

			if (writerRequestDTOs != null && !writerRequestDTOs.isEmpty()) {
				for (DcqVodIngestionWriterRequestDTO dcqVodIngestionWriterRequestDTO : writerRequestDTOs) {
					if (dcqVodIngestionWriterRequestDTO.isDeleted()) {
						publishedIds.add(dcqVodIngestionWriterRequestDTO.getEsUniqueId());
						if (!deletedMap.containsKey(dcqVodIngestionWriterRequestDTO.getEsUniqueId())) {
							EsContentDoc esContentDoc = (EsContentDoc) dcqVodIngestionWriterRequestDTO.getEsDocObj();
							deletedMap.put(dcqVodIngestionWriterRequestDTO.getEsUniqueId(),
									esContentDoc.getContent().getMetadata().getExternalId());
						}
					
					} else {
						writerRequestDTOList.add(dcqVodIngestionWriterRequestDTO);
						}
				}
			}
			// AVS-13771 start
			if (!deletedMap.isEmpty() && writerRequestDTOList.isEmpty()) {
				for (Entry<Integer, String> entry : deletedMap.entrySet()) {
					DCQGlobalEventCollectorContentDTO contentDTO = new DCQGlobalEventCollectorContentDTO();
					contentDTO.setAction(DcqVodIngestorConstants.DELETE);
					contentDTO.setContentType(DcqVodIngestorConstants.CONTENT);
					contentDTO.setContentId(entry.getKey());
					contentDTO.setExtContentId(entry.getValue());
					log.logMessage("call to event collector for Global content : {}", contentDTO.getContentId());
					String payLoad = JsonUtils.writeAsJsonStringWithoutNull(contentDTO);
					DcqVodIngestorUtils.sendEventCollectorReport(DcqVodIngestorConstants.DCQ_CONTENT_INGESTOR,
							DcqVodIngestorConstants.CONTENT_GLOBAL_INGESTION, payLoad, config);
				}
			}
			// AVS-13771 stop
			i++;

			if ((null != writerRequestDTOList && writerRequestDTOList.size() >= sizeOfWriterBuffer)
					|| (i == numOfAssets)) {
				log.logMessage(
						"The writer buffer size is >= {}, or num of assets for this thread have completed, write to ES.",
						sizeOfWriterBuffer);

				List<DCQEventCollectorContentDTO> eventCollectorDtoList = processEventCollectorContentRequest(
						writerRequestDTOList);
				List<DCQGlobalEventCollectorContentDTO> eventCollectorGlobalDtoList = processEventCollectorGlobalRequest(
						writerRequestDTOList);// AVS-13771

				writerRequestDTOList = invokeProcessorPlugin(writerRequestDTOList, transactionNumber, config);
				DcqVodIngestionWriterResponseDTO dcqVodIngestionWriterResponseDTO = invokeWriter(writerRequestDTOList,
						mode, indexDate, config);
				if (null == dcqVodIngestionWriterResponseDTO) {
					invokePostProcessor(publishedIds, null, startTime, config);
				} else {
					publishedIds.addAll(dcqVodIngestionWriterResponseDTO.getPublishedIds());
					invokePostProcessor(publishedIds, dcqVodIngestionWriterResponseDTO.getFailedIdsMap(), startTime,
							config);
				}
				// Start: AVS : 9263
				if (null != dcqVodIngestionWriterResponseDTO
						&& !CollectionUtils.isEmpty(dcqVodIngestionWriterResponseDTO.getPublishedIds())) {

					for (DCQEventCollectorContentDTO eventCollectorDto : eventCollectorDtoList) {
						if (dcqVodIngestionWriterResponseDTO.getPublishedIds()
								.contains(eventCollectorDto.getContentId())) {
							String payLoad = JsonUtils.writeAsJsonStringWithoutNull(eventCollectorDto);
							DcqVodIngestorUtils.sendEventCollectorReport(DcqVodIngestorConstants.DCQ_CONTENT_INGESTOR,
									DcqVodIngestorConstants.CONTENT_INGESTION, payLoad, config);
						}
					}
					// AVS-13771 start
					if (dcqVodIngestionWriterResponseDTO.getFailedIdsMap().isEmpty()) {
						for (DCQGlobalEventCollectorContentDTO globalEventCollectorDto : eventCollectorGlobalDtoList) {
							if (dcqVodIngestionWriterResponseDTO.getPublishedIds()
									.contains(globalEventCollectorDto.getContentId())) {
								log.logMessage("call to event collector for Global content : {}",
										globalEventCollectorDto.getContentId());
								String payLoad = JsonUtils.writeAsJsonStringWithoutNull(globalEventCollectorDto);
								DcqVodIngestorUtils.sendEventCollectorReport(
										DcqVodIngestorConstants.DCQ_CONTENT_INGESTOR,
										DcqVodIngestorConstants.CONTENT_GLOBAL_INGESTION, payLoad, config);
							}
						}
					}
					// AVS-13771 stop
				}
				// End: AVS : 9263

				writerRequestDTOList = new ArrayList<DcqVodIngestionWriterRequestDTO>();

			}
		}
		return null;
	}

	public List<DcqVodIngestionWriterRequestDTO> constructEsDocsForContent(Integer contentId, String transactionNumber,
			String startTime, String mode, String indexDate, Configuration config)
			throws JsonParseException, JsonMappingException, IOException {
		log.logMessage("Constructing ES Doc(s) for ContentId: {} -Start", contentId);
		long sTime = System.currentTimeMillis();
		languageCache.getLanguages();
		Asset asset = null;
		List<DcqVodIngestionWriterRequestDTO> esDocsContentList = null;

		byte[] byteArr = extendedContentAttributesRepository.retrieveContentXmlBlob(contentId);

		if (byteArr == null) {
			log.logMessage("Source file is empty for contentId : {}", contentId);
		} else {
			try {
				asset = (Asset) XmlHelper.latestUnMarshaller(jcAsset, byteArr);
				esDocsContentList = vodUtilities.constructEsDocsForContent(contentId, asset, transactionNumber, config);
			} catch (JAXBException jaxbe) {
				try {
					String text = new String(byteArr, StandardCharsets.UTF_8);
					VodRequestDTO vodDTO = JsonUtils.parseJson(text, VodRequestDTO.class);
					esDocsContentList = vodUtilitiesV1.constructEsDocsForContentJson(contentId, vodDTO,
							transactionNumber, config);
				} catch (Exception e) {
					log.logError(e, "Error while constructing ES Doc for ContentId:" + contentId
							+ ",Update the status to FAILED.");
					List<Integer> contentIds = new ArrayList<Integer>();
					contentIds.add(contentId);
					String errorInfo = DcqVodIngestorUtils.formatString(OtherSystemCallType.INTERNAL,
							DcqVodIngestorUtils.getLoggingMethod(), DcqVodIngestorUtils.getErrorStackTrace(e));
					dcqAssetStagingManager.updateStatus(errorInfo, contentIds, DcqVodIngestorConstants.CONTENT, config);
				}
			} catch (Exception e) {
				log.logError(e,
						"Error while constructing ES Doc for ContentId:" + contentId + ",Update the status to FAILED.");
				List<Integer> contentIds = new ArrayList<Integer>();
				contentIds.add(contentId);
				String errorInfo = DcqVodIngestorUtils.formatString(OtherSystemCallType.INTERNAL,
						DcqVodIngestorUtils.getLoggingMethod(), DcqVodIngestorUtils.getErrorStackTrace(e));
				dcqAssetStagingManager.updateStatus(errorInfo, contentIds, DcqVodIngestorConstants.CONTENT, config);
			}
		}

		log.logMessage("Constructing ES Doc(s) for ContentId: {} -Completed,ES Docs count: {} ,took: {} ms", contentId,
				(esDocsContentList == null ? 0 : esDocsContentList.size()), (System.currentTimeMillis() - sTime));
		return esDocsContentList;
	}

	private List<DcqVodIngestionWriterRequestDTO> invokeProcessorPlugin(
			List<DcqVodIngestionWriterRequestDTO> ingestionWriterRequestDTOs, String transactionNumber,
			Configuration config) throws ApplicationException {
		log.logMessage("Content Ingestor Processor Plugin - Start");
		long sTimeProPlugin = System.currentTimeMillis();
		DcqVodIngestionProcessorPlugin dcqVodIngestionProcessorPlugin = GatewayFactory
				.getDcqVodIngestionProcessorPlugin(config);
		ingestionWriterRequestDTOs = dcqVodIngestionProcessorPlugin.updateMetadata(ingestionWriterRequestDTOs,
				DcqVodIngestorConstants.CONTENT, transactionNumber, context);
		log.logMethodEnd(System.currentTimeMillis() - sTimeProPlugin);
		return ingestionWriterRequestDTOs;
	}

	private DcqVodIngestionWriterResponseDTO invokeWriter(
			List<DcqVodIngestionWriterRequestDTO> ingestionWriterRequestDTOs, String mode, String indexDate,
			Configuration config) throws ApplicationException {
		long sTimeWriter = System.currentTimeMillis();
		log.logMessage("Content Ingestor Writer - Start");
		DcqVodIngestionWriterResponseDTO dcqVodIngestionWriterResponseDTO = dcqVodIngestionWriter
				.writeToES(ingestionWriterRequestDTOs, mode, indexDate, config);
		log.logMethodEnd(System.currentTimeMillis() - sTimeWriter);
		return dcqVodIngestionWriterResponseDTO;
	}

	private void invokePostProcessor(Set<Integer> publishedIds, Map<Integer, String> failedIdsMap, String startTime,
			Configuration config) throws ApplicationException, ConfigurationException {
		Boolean invalidateVarnish = false;
		long sTimePProcescor = System.currentTimeMillis();
		log.logMessage("Content Ingestor Post Procescor - Start");
		if (null != failedIdsMap && !failedIdsMap.isEmpty() && publishedIds != null && !publishedIds.isEmpty()) {
			for (Integer faildId : failedIdsMap.keySet()) {
				if (publishedIds.contains(faildId)) {
					publishedIds.remove(faildId);
				}
			}
		}
		if ((null != publishedIds && !publishedIds.isEmpty())) {
			log.logMessage("Deleting the published Assets with Ids: {}", publishedIds);
			dcqVodIngestionPostProcessor.deletePublishedAssets(publishedIds, DcqVodIngestorConstants.CONTENT, config);

			// Report Gateway
			DcqVodIngestorUtils.sendPublishedReport(publishedIds, null, startTime, DcqVodIngestorConstants.CONTENT,
					config);
			invalidateVarnish = true;
		}

		if (null != failedIdsMap && !failedIdsMap.isEmpty()) {
			dcqVodIngestionPostProcessor.updateStatusAsFailed(failedIdsMap, DcqVodIngestorConstants.CONTENT, config);
		}

		log.logMessage("Content Ingestor Post Procescor - Completed,took: {} ms",
				(System.currentTimeMillis() - sTimePProcescor));

		Boolean invalidateVarnishInPanic = BooleanConverter
				.getBooleanValue(config.getConstants().getValue(CacheConstants.DCQ_VOD_VARNISH_INVALIDATOR_IN_PANIC));
		if (!invalidateVarnishInPanic && invalidateVarnish) {
			log.logMessage("Varnish Invalidator - Start");
			varnishCleanerManager.clean(config);
			log.logMethodEnd(System.currentTimeMillis() - sTimePProcescor);
		}
	}

	/**
	 * @param tenantName
	 * @param transactionNumber
	 */
	public void dynamicMetadataUpdate(String tenantName, String transactionNumber, Configuration config) {

		EntityManager entityMgr = null;
		try {

			long sTimeWriter = System.currentTimeMillis();
			log.logMessage("Content DynamicUpdate writer - Start");

			Map<String, String> languagesCache = languageCache.getLanguages();

			for (String langCode : languagesCache.keySet()) {

				String indexName = DcqVodIngestorUtils
						.getWriteAlias(DcqVodIngestorConstants.CONTENT_IN_LOWER + "_" + langCode.toLowerCase());
				Object[] object = null;
				List<Object[]> comingSoonList = null;
				List<Object[]> leavingSoonList = null;
				List<Object[]> recentlyAddedList =null;
				for (Map.Entry<String, PlatformValue> map : config.getPlatforms().copyCache().entrySet()) {
					comingSoonList = new ArrayList<Object[]>();
					leavingSoonList = new ArrayList<Object[]>();
					leavingSoonList = new ArrayList<Object[]>();
					String platformName = map.getKey().toLowerCase();
					;

					List<Integer> retrieveComingSoonContentIds = contentPlatformRepository.retrieveComingSoonContentIds(
							map.getKey(), config.getConstants().getValue(CacheConstants.COMING_SOON_TIME_WINDOW));

					if (retrieveComingSoonContentIds != null) {
						for (Integer contentId : retrieveComingSoonContentIds) {

							object = new Object[3];
							object[0] = indexName;
							object[1] = platformName;
							object[2] = contentId;
							comingSoonList.add(object);
						}
					}

					List<Integer> retrieveLeavingSoonContentIds = contentPlatformRepository
							.retrieveLeavingSoonContentIds(map.getKey(),
									config.getConstants().getValue(CacheConstants.LEAVING_SOON_TIME_WINDOW));
					if (retrieveLeavingSoonContentIds != null) {
						for (Integer contentId : retrieveLeavingSoonContentIds) {
							object = new Object[3];

							object[0] = indexName;
							object[1] = platformName;
							object[2] = contentId;

							leavingSoonList.add(object);
						}
					}
					log.logMessage("***********************dcqVodIngestionWriter dynamicMetadataUpdate");
					dcqVodIngestionWriter.dynamicMetadataUpdate(indexName, platformName, comingSoonList,
							leavingSoonList,recentlyAddedList, config);

				}

			}
			Boolean invalidateVarnishInPanic = BooleanConverter
					.getBooleanValue(config.getConstants().getValue(CacheConstants.DCQ_VOD_VARNISH_INVALIDATOR_IN_PANIC));
			if (!invalidateVarnishInPanic) {
				long sTimePProcescor = System.currentTimeMillis();
				log.logMessage("Varnish Invalidator - Start");
				varnishCleanerManager.clean(config);
				log.logMessage("Varnish Invalidator - Completed,took: {} ms",
						(sTimePProcescor - System.currentTimeMillis()));
			}

			log.logMethodEnd(sTimeWriter - System.currentTimeMillis());
		} catch (Exception e) {
			log.logError(e);
		} finally {
			DcqVodIngestorUtils.closeEM(entityMgr);
		}

	}

	private List<DCQEventCollectorContentDTO> processEventCollectorContentRequest(
			List<DcqVodIngestionWriterRequestDTO> writerRequestDTOList) {

		List<DCQEventCollectorContentDTO> eventCollectorList = new ArrayList<DCQEventCollectorContentDTO>();
		for (DcqVodIngestionWriterRequestDTO ingestionWriterRequestDTO : writerRequestDTOList) {

			EsContentDoc esContentDoc = (EsContentDoc) ingestionWriterRequestDTO.getEsDocObj();
			DCQEventCollectorContentDTO contentDTO = new DCQEventCollectorContentDTO();
			contentDTO.setAction(DcqVodIngestorConstants.UPSERT);
			contentDTO.setContentType(DcqVodIngestorConstants.CONTENT);
			contentDTO.setContentId(esContentDoc.getContent().getContentId().intValue());
			contentDTO.setExtContentId(esContentDoc.getContent().getMetadata().getExternalId());
			String[] parts = ingestionWriterRequestDTO.getEsIndex().split("_");
			contentDTO.setLang(parts[1].toUpperCase());
			contentDTO.setPlatform(esContentDoc.getContent().getPlatformName().toUpperCase());
			eventCollectorList.add(contentDTO);
		}
		return eventCollectorList;
	}



	// AVS-13771 start
	private List<DCQGlobalEventCollectorContentDTO> processEventCollectorGlobalRequest(
			List<DcqVodIngestionWriterRequestDTO> writerRequestDTOList) {

		List<DCQGlobalEventCollectorContentDTO> eventCollectorList = new ArrayList<DCQGlobalEventCollectorContentDTO>();
		Map<Long, String> contentMap = new HashMap<Long, String>();
		for (DcqVodIngestionWriterRequestDTO ingestionWriterRequestDTO : writerRequestDTOList) {
			EsContentDoc esContentDoc = (EsContentDoc) ingestionWriterRequestDTO.getEsDocObj();
			if (!contentMap.containsKey(esContentDoc.getContent().getContentId())) {
				contentMap.put(esContentDoc.getContent().getContentId(),
						esContentDoc.getContent().getMetadata().getExternalId());
			}
		}
		if (!contentMap.isEmpty()) {
			for (Entry<Long, String> entry : contentMap.entrySet()) {
				DCQGlobalEventCollectorContentDTO contentDTO = new DCQGlobalEventCollectorContentDTO();
				contentDTO.setAction(DcqVodIngestorConstants.UPSERT);
				contentDTO.setContentType(DcqVodIngestorConstants.CONTENT);
				contentDTO.setContentId(entry.getKey().intValue());
				contentDTO.setExtContentId(entry.getValue());
				eventCollectorList.add(contentDTO);
			}
		}
		return eventCollectorList;
	}

	
	// AVS-12973 stop

	@Override
	public void dynamicMetadataUpdate(Configuration config) {
		
		try {

			long sTimeWriter = System.currentTimeMillis();
			log.logMessage("Content DynamicUpdate writer - Start");

			Map<String, String> languagesCache = languageCache.getLanguages();

			for (String langCode : languagesCache.keySet()) {

				String indexName = DcqVodIngestorUtils
						.getWriteAlias(DcqVodIngestorConstants.CONTENT_IN_LOWER + "_" + langCode.toLowerCase());
				Object[] object = null;
				List<Object[]> comingSoonList = null;
				List<Object[]> leavingSoonList = null;
				List<Object[]> recentlyAddedList =null;
				for (Map.Entry<String, PlatformValue> map : config.getPlatforms().copyCache().entrySet()) {
					comingSoonList = new ArrayList<Object[]>();
					leavingSoonList = new ArrayList<Object[]>();
					recentlyAddedList = new ArrayList<Object[]>();
					String platformName = map.getKey().toLowerCase();
					;

					List<Integer> retrieveComingSoonContentIds = contentPlatformRepository.retrieveComingSoonContentIds(
							map.getKey(), config.getConstants().getValue(CacheConstants.COMING_SOON_TIME_WINDOW));

					if (retrieveComingSoonContentIds != null) {
						for (Integer contentId : retrieveComingSoonContentIds) {

							object = new Object[3];
							object[0] = indexName;
							object[1] = platformName;
							object[2] = contentId;
							comingSoonList.add(object);
						}
					}

					List<Integer> retrieveLeavingSoonContentIds = contentPlatformRepository
							.retrieveLeavingSoonContentIds(map.getKey(),
									config.getConstants().getValue(CacheConstants.LEAVING_SOON_TIME_WINDOW));
					if (retrieveLeavingSoonContentIds != null) {
						for (Integer contentId : retrieveLeavingSoonContentIds) {
							object = new Object[3];

							object[0] = indexName;
							object[1] = platformName;
							object[2] = contentId;

							leavingSoonList.add(object);
						}
						List<Integer> retrieveRecentlyAddedContentIds = contentPlatformRepository.retrieveRecentlyAddedContentIds(map.getKey(),
								TimeUnit.DAYS.toMillis(1));
						if(retrieveRecentlyAddedContentIds!=null){
						for(Integer contentId : retrieveRecentlyAddedContentIds){
						 object = new Object[3];
						
						object[0] = indexName;
						object[1] = platformName;
						object[2] = contentId;
						
						recentlyAddedList.add(object);
						}
						}
					}
					log.logMessage("***********************dcqVodIngestionWriter dynamicMetadataUpdate");
					dcqVodIngestionWriter.dynamicMetadataUpdate(indexName, platformName, comingSoonList,
							leavingSoonList,recentlyAddedList, config);

				}
			}
			Boolean invalidateVarnishInPanic = BooleanConverter
					.getBooleanValue(config.getConstants().getValue(CacheConstants.DCQ_VOD_VARNISH_INVALIDATOR_IN_PANIC));
			if (!invalidateVarnishInPanic) {
				long sTimePProcescor = System.currentTimeMillis();
				log.logMessage("Varnish Invalidator - Start");
				varnishCleanerManager.clean(config);
				log.logMessage("Varnish Invalidator - Completed,took: {} ms",
						(sTimePProcescor - System.currentTimeMillis()));
			}

			log.logMethodEnd(sTimeWriter - System.currentTimeMillis());
		} catch (Exception e) {
			log.logError(e);
		}
	}
	public ContentEntity getContentByExternalId(String externalId){
		return contentRepository.getcontentByExternalId(externalId);
		
	}
	
	public ContentEntity getContentByContentId(Integer contentId){
		return contentRepository.getContentByContentId(contentId);
		
	}
	
	@Override
	public void unpublishContents(List<Integer> contentIds, Configuration config) throws ConfigurationException {
		// TODO Auto-generated method stub
		try{
		// Get the language codes
		Map<String, String> languagesCache = languageCache.getLanguages();

		for (String langCode : languagesCache.keySet()) {
			String indexName = DcqVodIngestorUtils
					.getWriteAlias(DcqVodIngestorConstants.CONTENT_IN_LOWER + "_" + langCode.toLowerCase());

			// prepare the uniqueId
			ElasticSearchIntegratedClientManager esClientManager = DcqVodIngestorUtils
					.getElasticSearchIntegratedClientManagerImpl(config);
			byte[]  dcqIngestorUser = DcqVodIngestorUtils.getESDcqIngestorUser();
			byte[] dcqIngestorPassword = DcqVodIngestorUtils.getESDcqIngestorPassword();

			for (Integer contentId : contentIds) {
				for (Map.Entry<String, PlatformValue> map : config.getPlatforms().copyCache().entrySet()) {

					esClientManager.deleteDocumentById(indexName, DcqVodIngestorConstants.ALL_IN_LOWER, dcqIngestorUser,
							dcqIngestorPassword,
							contentId + DcqVodIngestorConstants.HYPHEN_SYMBOL + map.getKey().toLowerCase());

				}
			}

		}
		
		}catch(Exception ex){
			log.logError(ex, "Error while deletion contents from ES");
			throw ex;
		}

	}
}
