package com.accenture.avs.be.dcq.vod.ingestor.v1.dto.contentcategory.asset;

import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * <p>Category Json Object.
 * 
 * <pre>
 *  {  
 * "categoryId":123,
 * "parentCategoryId":123,
 * "name":"ON DEMAND",
 * "type":"NODE",
 * "isVisible":"Y",
 * "catalogueIdentifier":"PCTV",
 * "orderId":123,
 * "isAdult":"N",
 * "externalId":"ext123",
 * "contentOrderType":"TITLEDESC",
 * "contentId":123,
 * "ratingType":"",
 * "pcLevel":"5",
 * "pcExtendedRatings":["A","U/A"],
 * "isProtected":"Y"
 * }
 * </pre>
 * 
 * 
 */


public class Category {

	@ApiModelProperty(value = "unique identifier of category", required = true, example = "123")
	private Integer categoryId;
	
	@ApiModelProperty(value = "Unique identifier of parent category", required = true, example = "123")
	private Integer parentCategoryId;
	
	@ApiModelProperty(value = "Name of the category", required = true, example = "ON DEMAND")
	private String name;
	
	@ApiModelProperty(value = "Type of the category", required = true, example = "NODE")
	private String type;
	
	@ApiModelProperty(value = "Flag to identify the category is visible or not", required = true, example = "Y")
	private String isVisible;
	
	@ApiModelProperty(value = "To identify the category under which platform", required = true, example = "PCTV")
	private String catalogueIdentifier;
	
	@ApiModelProperty(value = "order number of the category", example = "123")
	private Integer orderId;
	
	@ApiModelProperty(value = "To identify the category contains adult content",  example = "N")
	private String isAdult;
	
	@ApiModelProperty(value = "Unique identifier of the category",  example = "ext123")
	private String externalId;
	
	@ApiModelProperty(value = "content order type in ategory",  example = "TITLEDESC")
	private String contentOrderType;
	
	@ApiModelProperty(value = "Unique identifier of the content",  example = "123")
	private Integer contentId;
	
	@ApiModelProperty(value = "rating type of the category",  example = "rating type")
	private String ratingType;
	
	@ApiModelProperty(value = "parental control level",  example = "5")
	private String pcLevel;
	
	@ApiModelProperty(value = "Parental control rating",  example = "[\"A\"]")
	private String[] pcExtendedRatings;
	
	@ApiModelProperty(value = "To identify the category is protected or not",  example = "Y")
	private String isProtected;

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public Integer getParentCategoryId() {
		return parentCategoryId;
	}

	public void setParentCategoryId(Integer parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getIsVisible() {
		return isVisible;
	}

	public void setIsVisible(String isVisible) {
		this.isVisible = isVisible;
	}

	public String getCatalogueIdentifier() {
		return catalogueIdentifier;
	}

	public void setCatalogueIdentifier(String catalogueIdentifier) {
		this.catalogueIdentifier = catalogueIdentifier;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public String getIsAdult() {
		return isAdult;
	}

	public void setIsAdult(String isAdult) {
		this.isAdult = isAdult;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getContentOrderType() {
		return contentOrderType;
	}

	public void setContentOrderType(String contentOrderType) {
		this.contentOrderType = contentOrderType;
	}

	public Integer getContentId() {
		return contentId;
	}

	public void setContentId(Integer contentId) {
		this.contentId = contentId;
	}

	public String getRatingType() {
		return ratingType;
	}

	public void setRatingType(String ratingType) {
		this.ratingType = ratingType;
	}

	public String getPcLevel() {
		return pcLevel;
	}

	public void setPcLevel(String pcLevel) {
		this.pcLevel = pcLevel;
	}

	public String[] getPcExtendedRatings() {
		return pcExtendedRatings;
	}

	public void setPcExtendedRatings(String pcExtendedRatings[]) {
		this.pcExtendedRatings = pcExtendedRatings;
	}

	public String getIsProtected() {
		return isProtected;
	}

	public void setIsProtected(String isProtected) {
		this.isProtected = isProtected;
	}

}

