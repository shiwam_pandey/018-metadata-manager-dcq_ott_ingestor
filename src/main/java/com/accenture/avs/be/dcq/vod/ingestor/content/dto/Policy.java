
package com.accenture.avs.be.dcq.vod.ingestor.content.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;



public class Policy implements Serializable
{

    /**
     * 
     * (Required)
     * 
     */

    private String policyId;
    /**
     * 
     * (Required)
     * 
     */

    private String policyName;
    /**
     * 
     * (Required)
     * 
     */

    private String policyType;
    /**
     * 
     * (Required)
     * 
     */

    private Integer priority;
    /**
     * 
     * (Required)
     * 
     */

    private Boolean isDefault;

    private List<Rule> rules = new ArrayList<Rule>();

    private String property;
    /**
     * 
     * (Required)
     * 
     */

    private Boolean isActive;
    private String policyDescription;
    private final static long serialVersionUID = -6034044620547363511L;

    public String getPolicyDescription() {
		return policyDescription;
	}

	public void setPolicyDescription(String policyDescription) {
		this.policyDescription = policyDescription;
	}

	/**
     * 
     * (Required)
     * 
     * @return
     *     The policyId
     */

    public String getPolicyId() {
        return policyId;
    }

    /**
     * 
     * (Required)
     * 
     * @param policyId
     *     The policyId
     */

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The policyName
     */

    public String getPolicyName() {
        return policyName;
    }

    /**
     * 
     * (Required)
     * 
     * @param policyName
     *     The policyName
     */

    public void setPolicyName(String policyName) {
        this.policyName = policyName;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The policyType
     */

    

    /**
     * 
     * (Required)
     * 
     * @return
     *     The priority
     */

    public Integer getPriority() {
        return priority;
    }

    public String getPolicyType() {
		return policyType;
	}

	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}

	/**
     * 
     * (Required)
     * 
     * @param priority
     *     The priority
     */

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The isDefault
     */

    public Boolean getIsDefault() {
        return isDefault;
    }

    /**
     * 
     * (Required)
     * 
     * @param isDefault
     *     The isDefault
     */

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    /**
     * 
     * @return
     *     The rules
     */

    public List<Rule> getRules() {
        return rules;
    }

    /**
     * 
     * @param rules
     *     The rules
     */

    public void setRules(List<Rule> rules) {
        this.rules = rules;
    }

    /**
     * 
     * @return
     *     The property
     */

    public String getProperty() {
        return property;
    }

    /**
     * 
     * @param property
     *     The property
     */

    public void setProperty(String property) {
        this.property = property;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The isActive
     */

    public Boolean getIsActive() {
        return isActive;
    }

    /**
     * 
     * (Required)
     * 
     * @param isActive
     *     The isActive
     */

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    @Generated("org.jsonschema2pojo")
    public enum PolicyType {

        ALLOWING("allowing"),
        RESTRICTIVE("restrictive");
        private final String value;
        private final static Map<String, Policy.PolicyType> CONSTANTS = new HashMap<String, Policy.PolicyType>();

        static {
            for (Policy.PolicyType c: values()) {
                CONSTANTS.put(c.value, c);
            }
        }

        private PolicyType(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }


        public String value() {
            return this.value;
        }


        public static Policy.PolicyType fromValue(String value) {
            Policy.PolicyType constant = CONSTANTS.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }

    }

}
