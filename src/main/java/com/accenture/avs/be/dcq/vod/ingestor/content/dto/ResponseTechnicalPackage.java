package com.accenture.avs.be.dcq.vod.ingestor.content.dto;

import java.io.Serializable;

public class ResponseTechnicalPackage implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3953207915797912541L;
	private Long packageId;
	private String offerId;

	
	public ResponseTechnicalPackage(Long packageId, String offerId) {
		this.offerId = offerId;
		this.packageId = packageId;
	}
	
	public ResponseTechnicalPackage() {
	}

	public String getOfferId() {
		return offerId;
	}
	public void setOfferId(String offerId) {
		this.offerId = offerId;
	}
	public long getPackageId() {
		return packageId;
	}
	public void setPackageId(Long packageId) {
		this.packageId = packageId;
	}
}
