package com.accenture.avs.be.dcq.vod.ingestor.manager.impl;

import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.avs.be.dcq.vod.ingestor.dto.channel.asset.ChannelPlatformType;
import com.accenture.avs.be.dcq.vod.ingestor.dto.channel.asset.Channels;
import com.accenture.avs.be.dcq.vod.ingestor.dto.channel.asset.Channels.Channel;
import com.accenture.avs.be.dcq.vod.ingestor.dto.channel.asset.FlagTypeYN;
import com.accenture.avs.be.dcq.vod.ingestor.dto.channel.asset.PackageList;
import com.accenture.avs.be.dcq.vod.ingestor.dto.channel.asset.PlatformList;
import com.accenture.avs.be.dcq.vod.ingestor.manager.ChannelManager;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ChannelPlatformRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ChannelRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ChannelTechnicalPkgRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.TechnicalPackageRepository;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.JsonUtils;
import com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.be.framework.utils.Utilities;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.persistence.technicalcatalogue.ChannelEntity;
import com.accenture.avs.persistence.technicalcatalogue.ChannelPlatformEntity;
import com.accenture.avs.persistence.technicalcatalogue.ChannelPlatformEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.ChannelTechnicalPkgEntity;
import com.accenture.avs.persistence.technicalcatalogue.ChannelTechnicalPkgEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.TechnicalPackageEntity;

@Component
@Transactional(propagation = Propagation.REQUIRES_NEW,rollbackFor = { RuntimeException.class })
public class ChannelManagerImpl implements ChannelManager{

	private static final LoggerWrapper log = new LoggerWrapper(ChannelManagerImpl.class);
	
	@Autowired
	private ChannelRepository channelRepository;
	
	@Autowired
	private TechnicalPackageRepository technicalPackageRepository;
	
	@Autowired
	private ChannelPlatformRepository channelPlatformRepository;
	
	@Autowired
	private ChannelTechnicalPkgRepository channelTechnicalPkgRepository;
	
	@Autowired
	private EntityManager entityManager;
	
	@Override
	public List<Integer> retrieveByDefaultChannelNumber(String defaultChannelNumber) {
		List<Integer> channelIds = channelRepository.retrieveByDefaultChannelNumber(defaultChannelNumber);		
		return channelIds;
	}

	@Override
	public List<TechnicalPackageEntity> selectByPackageId(Integer packageId) {
		List<TechnicalPackageEntity> technicalPackage = technicalPackageRepository.selectByPackageId(packageId);		
		return technicalPackage;
	}

	@Override
	public Integer createChannel(Channel inputChannel, Channels channels) throws Exception {
		ChannelEntity channel = new ChannelEntity();
		channel.setChannelId(inputChannel.getChannelId());
		channel.setName(inputChannel.getName());
		channel.setType(inputChannel.getType());
		channel.setOrderId(inputChannel.getOrder());
		if(Utilities.isEmpty(inputChannel.getExternalId())) {
		throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER, new NestedParameters("ExternalId"));
		}
		
		FlagTypeYN isAdult = inputChannel.getAdult();
		
		if(isAdult == null){
			channel.setIsAdult(DcqVodIngestorConstants.SHORT_NO);
		}else{
			channel.setIsAdult(isAdult.value());
		}
		
		channel.setDescription(inputChannel.getDescription());
		channel.setAdvTags(inputChannel.getAdvTags());
		channel.setVideoUrl(DcqVodIngestorConstants.VIDEO_URL);
				
		channel.setDefaultChannelNumber(inputChannel.getDefaultChannelNumber().toString());
		FlagTypeYN isDisAllowedAdv = inputChannel.getIsDisAllowedAdv();
		if(isDisAllowedAdv==null){
			channel.setDisallowedAdv(DcqVodIngestorConstants.NO);
		}else{
			channel.setDisallowedAdv(isDisAllowedAdv.value());
			
		}
		
		if(inputChannel.getBroadcasterName() != null){
			channel.setBroadcasterName(inputChannel.getBroadcasterName());
		}
		
		FlagTypeYN isActive=inputChannel.getIsActive();
		
		if(isActive==null){
			channel.setIsActive(DcqVodIngestorConstants.YES);
		}else{
			channel.setIsActive(isActive.value());
		}
		channel.setIsCatchUp(DcqVodIngestorConstants.NO);
		channel.setIsTimeshift(DcqVodIngestorConstants.NO);
		if(!StringUtils.isEmpty(inputChannel.getExtendedMetadata()) && !JsonUtils.isJSONValid(inputChannel.getExtendedMetadata())){
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters("ExtendedMetadata"));
		}
		getSourcefile(channels,channel);
		
		writeVodChannel(channel,inputChannel.getPlatformList(),inputChannel.getPackageList());
		
		Integer channelId = inputChannel.getChannelId();
		return channelId;
		
	}
	@Transactional(rollbackFor = { RuntimeException.class })
	private void writeVodChannel(ChannelEntity channel, PlatformList platformList, PackageList packageList) throws ApplicationException {
		
		try {
		deleteChannelRelatedData(channel.getChannelId());
		Integer channelId = channelRepository.save(channel).getChannelId();
		List<ChannelPlatformEntity> channelPlatforms = setChannelPlatforms(channelId, platformList);
		
		List<ChannelTechnicalPkgEntity> channelTechnicalPackages = setChannelTechPkg(channelId, packageList);
			
		channelPlatformRepository.saveAll(channelPlatforms);
		channelTechnicalPkgRepository.saveAll(channelTechnicalPackages);
		entityManager.flush();
		entityManager.clear();
	}catch(ApplicationException ae) {
		throw ae;
	}catch(Exception e) {
		throw new RuntimeException();
	}
	}

	private void deleteChannelRelatedData(Integer channelId) {
		channelTechnicalPkgRepository.deleteByChannelId(channelId);
		channelPlatformRepository.deleteByChannelId(channelId);
	}

	/**
	 * Generate the set of ChannelPlatforms starting from the platform list 
	 * passed in the item procesor.
	 * 
	 * @param channel
	 * @param platformList
	 * @return Set of ChannelPlatform
	 * @throws ApplicationException 
	 */
	private List<ChannelPlatformEntity> setChannelPlatforms(Integer channelId,PlatformList platformList) throws ApplicationException {
		List<ChannelPlatformEntity> channelPlatformSet = new ArrayList<ChannelPlatformEntity>();
		
			ChannelPlatformType platform = new ChannelPlatformType();
			
			for (int i=0;i<platformList.getPlatform().size();i++) {
				ChannelPlatformEntity channelPlatform = new ChannelPlatformEntity();
				ChannelPlatformEntityPK cpPk = new ChannelPlatformEntityPK();
				
				platform = platformList.getPlatform().get(i);
				
				//ChannelPlatform Key
				cpPk.setChannelId(channelId);
				cpPk.setPlatformName(platform.getName());
				cpPk.setVideoType("NA");
		
				//ChannelPlatform
			 	channelPlatform.setVideoUrl(platform.getVideoURL());
				channelPlatform.setTrailerUrl(platform.getTrailerURL());
				channelPlatform.setIsPublished(platform.getPublished().value());
				channelPlatform.setChannelGroup(platform.getGroup());
				channelPlatform.setId(cpPk);
				
				//Logos
				channelPlatform.setLogoBig(platform.getLogoBig());
				channelPlatform.setLogoMedium(platform.getLogoMedium());
				channelPlatform.setLogSmall(platform.getLogoSmall());				
				
				if(!StringUtils.isEmpty(platform.getExtendedMetadata()) && !JsonUtils.isJSONValid(platform.getExtendedMetadata())){
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters("ExtendedMetadata"));
				}
				channelPlatformSet.add(channelPlatform);
			}


		return channelPlatformSet;

	}
	
	/**
	 * Generate the set of ChannelTechnicalPackage starting from the package list 
	 * passed in the item procesor.
	 * 
	 * @param channel
	 * @param pkgList
	 * @return Set of ChannelTechnicalPkg
	 */
	private  List<ChannelTechnicalPkgEntity>  setChannelTechPkg(Integer channelId,PackageList pkgList) {
		List<ChannelTechnicalPkgEntity>	technicalPackageList = new ArrayList<>();	
		for(int i=0;i<pkgList.getPackage().size();i++) {
			ChannelTechnicalPkgEntity channelTechnicalPkg = new ChannelTechnicalPkgEntity();
			ChannelTechnicalPkgEntityPK ctpPk = new ChannelTechnicalPkgEntityPK();
			
			//ChannelTechPkg Key
			ctpPk.setChannelId(channelId);
			ctpPk.setPackageId(pkgList.getPackage().get(i).intValue());
			
			//ChannelTechPkg
			channelTechnicalPkg.setId(ctpPk);
			
			technicalPackageList.add(channelTechnicalPkg);
			//channel.getChannelTechnicalPkgs().add(channelTechnicalPkg);
			
		}
		return technicalPackageList;
	}
	
	/**
	 * This method is used to get the source file
	 * @param inputChannel
	 * @param channel
	 * @throws JAXBException
	 */
	private void getSourcefile(Channels inputChannel, ChannelEntity channel) throws JAXBException{
		StringWriter writer = new StringWriter();
		try{
		  JAXBContext contextObj = JAXBContext.newInstance(Channels.class);  
		    Marshaller marshallerObj = contextObj.createMarshaller();  
		    marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true); 
		    marshallerObj.marshal(inputChannel, writer);
		    channel.setSourceFile(writer.toString().getBytes(Charset.forName("UTF-8")));
		}catch(JAXBException e){
			//log.logMessage("Error in setting source file :{}",e);
			throw e;
		}
		
	}
}
