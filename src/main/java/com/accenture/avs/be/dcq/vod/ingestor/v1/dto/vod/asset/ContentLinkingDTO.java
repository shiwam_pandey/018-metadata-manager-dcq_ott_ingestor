package com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author karthik.vadla
 *
 */
public class ContentLinkingDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value="Unique Identifier for child content id",required=true,example="1000007")
	private Long contentId;
	
	@ApiModelProperty(value="shows that this child content is default or not",required=true,example="y")
	private String isDefault;
	
	@ApiModelProperty(value="is required. It is another attribute which define subtype of a content",required=true,example="TRAILER")
	private String subType;
	
	@ApiModelProperty(value="is required. It is order for each content defined by CMS",required=true,example="y")
	private Long orderId;
	
	
	public Long getContentId() {
		return contentId;
	}
	public void setContentId(Long contentId) {
		this.contentId = contentId;
	}
	public String getIsDefault() {
		return isDefault;
	}
	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}
	public String getSubType() {
		return subType;
	}
	public void setSubType(String subType) {
		this.subType = subType;
	}
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	
}
