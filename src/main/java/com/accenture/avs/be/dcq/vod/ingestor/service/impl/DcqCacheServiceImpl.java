package com.accenture.avs.be.dcq.vod.ingestor.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accenture.avs.be.dcq.vod.ingestor.cache.CopyProtectionsCache;
import com.accenture.avs.be.dcq.vod.ingestor.cache.DeviceChannelsCache;
import com.accenture.avs.be.dcq.vod.ingestor.cache.EsSettingsMappingsCache;
import com.accenture.avs.be.dcq.vod.ingestor.cache.LanguageCache;
import com.accenture.avs.be.dcq.vod.ingestor.cache.PoliciesCache;
import com.accenture.avs.be.dcq.vod.ingestor.cache.PropertiesCache;
import com.accenture.avs.be.dcq.vod.ingestor.cache.TRCCache;
import com.accenture.avs.be.dcq.vod.ingestor.service.DcqCacheService;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;


/**
 * @author karthik.vadla
 *
 */
@Service
public class DcqCacheServiceImpl implements DcqCacheService {

	@Autowired
	private LanguageCache langCache;
	@Autowired
private	DeviceChannelsCache deviceChannelsCache;

	@Autowired
	private CopyProtectionsCache copyProtectionCache;
	@Autowired
	private PoliciesCache policiesCache;
	@Autowired
	private PropertiesCache propertiesCache;
	@Autowired
	private TRCCache trcCache;
	@Autowired
	private EsSettingsMappingsCache esCache;;
	

	@Override
	public GenericResponse refreshCache(Configuration config) throws Exception {
		
		GenericResponse genericResponse = null;
		try {
		langCache.loadLanguagesCache();
		deviceChannelsCache.loadDeviceChannels(config);
		propertiesCache.loadProperties(config);
		copyProtectionCache.loadCopyProtectionsCache(config);
		policiesCache.loadPolicies(config);
		trcCache.loadTRCList(config);
		trcCache.loadTRCS(config);
		esCache.loadEsSettingsMappings(config);
		
		 genericResponse = new GenericResponse(DcqVodIngestorConstants.ACN_200, DcqVodIngestorConstants.OK, "");
		}catch(Exception e) {
			 throw e;
		}
		return genericResponse;
}
	
}
