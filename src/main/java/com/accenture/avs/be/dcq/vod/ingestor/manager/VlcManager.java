package com.accenture.avs.be.dcq.vod.ingestor.manager;

import java.sql.Date;
import java.util.List;

import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.persistence.dto.VlcContentDTO;

/**
 * @author karthik.vadla
 *
 */
public interface VlcManager {

	void deleteAndSaveVlcContent(Integer channelId,Date publishedDate,List<VlcContentDTO> vlcContentDTOs);
	void validateContentVideoType(List<Integer> contentIdList, List<VlcContentDTO> vlcContentDTOs) throws ApplicationException;

}
