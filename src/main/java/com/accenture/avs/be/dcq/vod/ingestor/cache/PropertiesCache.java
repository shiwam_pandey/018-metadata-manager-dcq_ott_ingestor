package com.accenture.avs.be.dcq.vod.ingestor.cache;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.content.dto.Properties;
import com.accenture.avs.be.dcq.vod.ingestor.content.dto.Property;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorRestUrls;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.utils.Utilities;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.commons.lib.LoggerWrapper.OtherSystemCallType;
import com.accenture.avs.commons.lib.httpclient.HttpClientAdapter;
import com.accenture.avs.commons.lib.httpclient.HttpClientAdapter.HttpClientAdapterConfiguration;
import com.accenture.avs.commons.lib.httpclient.HttpClientAdapter.HttpClientException;

/**
 * @author karthik.vadla
 *
 */
@Component
public class PropertiesCache {

	private static final LoggerWrapper log = new LoggerWrapper(PropertiesCache.class);
	
	@Autowired
	private DcqVodIngestorRestUrls dcqVodIngestorRestUrl;
	
	private Map<String, String> propertiesCache;

	/**
	 * @param config
	 * @return
	 * @throws ApplicationException
	 */
	public  Map<String, String> loadProperties(Configuration config) throws ApplicationException{
		long startTime = new Date().getTime();
		boolean finalLogResponse = false;
		String resultCode= "";
		String resultDesc = "";
		try{
			log.logMessage("Loading properties cache");
				Properties properties;
				propertiesCache =new HashMap<String, String>();
				ObjectMapper mapper = new ObjectMapper();
				 String getPropertiesUrl = dcqVodIngestorRestUrl.PROPERTIES_CACHE_URL;
				 
				 log.logCallToOtherSystemRequestBody(DcqVodIngestorConstants.CONFIGURATION, DcqVodIngestorConstants.PROPERTIES, "",
							OtherSystemCallType.INTERNAL);
				 
				HttpClientAdapterConfiguration httpConfig = DcqVodIngestorUtils.getHttpClientAdapterConfiguration("tenantDefault", config);
				String response = HttpClientAdapter.doGet(httpConfig, getPropertiesUrl, null, null, null);
				
				
				JsonNode jsonNode = mapper.readTree(response);
				properties = mapper.readValue(jsonNode.findValue("resultObj"), Properties.class);
				resultCode = mapper.readValue(jsonNode.findValue("resultCode"), String.class);
				resultDesc = mapper.readValue(jsonNode.findValue("resultDescription"), String.class);
				log.logCallToOtherSystemEnd(DcqVodIngestorConstants.CONFIGURATION, DcqVodIngestorConstants.PROPERTIES, "", DcqVodIngestorConstants.OK,resultCode,resultDesc,
						System.currentTimeMillis() - startTime, OtherSystemCallType.INTERNAL);
				
				for(Property property : properties.getProperties()){
					propertiesCache.put(property.getPropertyName(), property.getPropertyDescription());
				}
			
		}catch (HttpClientException hce) {
			log.logError(hce);
			String clientResponse = hce.getResponseBody();
			if((HttpStatus.SERVICE_UNAVAILABLE.value() == hce.getResponseCode())
					||(HttpStatus.NOT_FOUND.value()== hce.getResponseCode() && Utilities.isEmpty(clientResponse))
					||(HttpStatus.NOT_FOUND.value()== hce.getResponseCode() && clientResponse.startsWith("<html>"))) {
				propertiesCache = null;
				finalLogResponse = true;
			}
		} catch (Exception e) {
			finalLogResponse = true;
			log.logError(e);
		}finally {
			if(finalLogResponse) {
				log.logCallToOtherSystemEnd(DcqVodIngestorConstants.CONFIGURATION, DcqVodIngestorConstants.PROPERTIES, "", DcqVodIngestorConstants.KO, "", "",
						System.currentTimeMillis() - startTime, OtherSystemCallType.INTERNAL);
			}
		}
		return propertiesCache;
	}
	public  Map<String, String> getProperties(){
		return propertiesCache;
	}
}
