package com.accenture.avs.be.dcq.vod.ingestor.sdp.manager.impl;

import java.net.MalformedURLException;
import java.net.URL;

import com.accenture.sdp.csmfe.webservices.clients.solutionoffer.SdpSolutionOfferService;

public class SdpSolutionOfferServiceSingleton {


	private static SdpSolutionOfferService service;

	private SdpSolutionOfferServiceSingleton(){}


	private SdpSolutionOfferServiceSingleton(String sdpUrlConnection) throws MalformedURLException{
		String urlString = sdpUrlConnection+ "SdpSolutionOfferService?wsdl";
		URL url =  new URL(urlString);
		if (service == null) { // sonar fix
		service=new SdpSolutionOfferService(url);
		}
	}

	public static SdpSolutionOfferService getInstance(String sdpUrlConnection) throws MalformedURLException{

		if (service == null) {
			new SdpSolutionOfferServiceSingleton(sdpUrlConnection);
		}
		return service;
	}
}
