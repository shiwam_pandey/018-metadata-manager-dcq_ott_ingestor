package com.accenture.avs.be.dcq.vod.ingestor.content.dto;

import java.sql.Timestamp;

public class SolutionOfferToWrite {
	private Long solutionOfferId;
	private String name;
	private String description;
	private String title;
	private Long solutionId;
	private String externalId;
	private String costCategoryId;
	private String priceCategoryId;
	private Timestamp startDate;
	private Timestamp endDate;
	
	/**
	 * @return the solutionOfferId
	 */
	public Long getSolutionOfferId() {
		return solutionOfferId;
	}
	/**
	 * @param solutionOfferId the solutionOfferId to set
	 */
	public void setSolutionOfferId(Long solutionOfferId) {
		this.solutionOfferId = solutionOfferId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the solutionId
	 */
	public Long getSolutionId() {
		return solutionId;
	}
	/**
	 * @param solutionId the solutionId to set
	 */
	public void setSolutionId(Long solutionId) {
		this.solutionId = solutionId;
	}
	/**
	 * @return the externalId
	 */
	public String getExternalId() {
		return externalId;
	}
	/**
	 * @param externalId the externalId to set
	 */
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	/**
	 * @return the costCategoryId
	 */
	public String getCostCategoryId() {
		return costCategoryId;
	}
	/**
	 * @param costCategoryId the costCategoryId to set
	 */
	public void setCostCategoryId(String costCategoryId) {
		this.costCategoryId = costCategoryId;
	}
	/**
	 * @return the priceCategoryId
	 */
	public String getPriceCategoryId() {
		return priceCategoryId;
	}
	/**
	 * @param priceCategoryId the priceCategoryId to set
	 */
	public void setPriceCategoryId(String priceCategoryId) {
		this.priceCategoryId = priceCategoryId;
	}
	public Timestamp getStartDate() {
		return startDate;
	}
	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}
	public Timestamp getEndDate() {
		return endDate;
	}
	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

}
