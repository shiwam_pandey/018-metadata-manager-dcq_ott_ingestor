package com.accenture.avs.be.dcq.vod.ingestor.rest.controllers;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.avs.be.dcq.vod.ingestor.service.DcqVodIngestorService;
import com.accenture.avs.be.framework.annotation.AvsRestController;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.cache.Configurator;
import com.accenture.avs.be.framework.configuration.CacheConstants;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.commons.lib.support.CommonConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author karthik.vadla
 *
 */
@RestController
@AvsRestController
@Api(tags="DCQ VOD Ingestor Service" ,value = "DCQ VOD Ingestor Service", description = "API's pertaining to DCQ VOD Ingestor Services")
public class DcqVodIngestorController {

	@Autowired
private	Configurator configurator;
	@Autowired
	private DcqVodIngestorService dcqVodIngestorService;

	@RequestMapping(value = "/v1/ingest/{ingestionType}", method = RequestMethod.GET)
	@ApiOperation(value="dcqVodIngestor",notes = "This service is used to ingest Content, Category, VODChannel, VLC contents to Elastic Search. "
			+ "\nThis is also used to:"
			+ "\nperform a FULL ingestion: re-ingest all valid items present in the DB and store them on Elastic Search"
			+"\nperform a REINDEX: similar to FULL ingestion, but it will create new indexes (from scratch) applying the latest mapping changes")
	@ApiImplicitParams({
		@ApiImplicitParam(name="ingestionType", dataType="string",required=true,example="ALL", value="Type of ingestion to start"
				+"\nCONTENT: invokes DCQ content ingestion (this retrieves from DCQ staging table only)"
				+"\nCATEGORY: invokes DCQ categoryingestion (this retrieves from DCQ staging table only)"
				+"\nVODCHANNEL: invokes DCQ vod channel ingestion (this retrieves from DCQ staging table only)"
				+"\nVLCCONTENT: invokes DCQ vlc content ingestion (this retrieves from DCQ staging table only)"
				+"\nALL:  invokes all of the above ingestors sequentially (this retrieves from DCQ staging table only)"
				+"\nFULL: Retrieves all the contents from DB and ingest them on DCQ)"
				),
		@ApiImplicitParam(name="mode", value="Indexing mode.Mode is applicable for only FULL type and Possible value mode = REINDEX", dataType="string",required=false,example="REINDEX"),
		@ApiImplicitParam(name="switchAlias",dataType="string",required=false,example="N" ,value="To switch the alias in REINDEX mode"
				+"\nAllowed values: Y/N, default = Y"
				+"\nIf the value is Y, then the process will move the read alias (used by DCQ templates) to the new indexes,"
				+"\nIf the value is N, then the reindex procedure will just create the new indexes with write alias and leave the read alias on the previous indexes, In this case, the switchAlias service must be used to SWITCH or ROLLBACK the changes"
				)
		})
	@ApiResponses({
		@ApiResponse(code=200, message="ACN_200: OK"),
		@ApiResponse(code=500, message="ACN_300: GENERIC ERROR")
	})
	public ResponseEntity<GenericResponse> dcqVodIngestor(@PathVariable("ingestionType") String ingestionType, @RequestParam(required = false) String mode,
			@RequestParam(required = false) String switchAlias) throws ApplicationException {
		GenericResponse genericResponse = null;
		try {
			Configuration config = configurator.getConfiguration(ThreadContext.get(CommonConstants.LogParamsL4j2.TN));
			genericResponse = dcqVodIngestorService.dcqVodIngestor(ingestionType, mode, switchAlias, config);
		}catch (Exception e) {
			throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR);
		}
		return ResponseEntity.ok(genericResponse);
	}
	
	@RequestMapping(value = "/v1/dynamicupdate", method = RequestMethod.PUT)
	@ApiOperation(value="dynamicUpdate",notes = "This service is used to update the comingSoon and leavingSoon contents based on content startDate and endDate for each platform."
			+ "\nThis service is scheduled to start at specific interval configured in the sys parameters DCQ_DYNAMIC_UPDATE_FREQUENCY_TIME.")
	@ApiResponses({
		@ApiResponse(code=200, message="ACN_200: OK"),
		@ApiResponse(code=500, message="ACN_300: GENERIC ERROR")
	})
	public ResponseEntity<GenericResponse> dynamicUpdate() throws ApplicationException {
		GenericResponse genericResponse = null;
		try {
			Configuration config = configurator.getConfiguration(ThreadContext.get(CommonConstants.LogParamsL4j2.TN));
			genericResponse = dcqVodIngestorService.dynamicUpdate(config);
		}catch (Exception e) {
			throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR);
		}
		return ResponseEntity.ok(genericResponse);
	}
	
	@RequestMapping(value = "/v1/alias/{mode}", method = RequestMethod.PUT)
	@ApiOperation(value="switchOrRollbackAlias",notes = "This service is mainly used to move the read alias from the previous indexes to new indexes."
			+ "\nThis service is necessary when the FULL ingestion is done with mode=REINDEX and switchAlias=N.")
	@ApiImplicitParams({
		@ApiImplicitParam(name="mode", value="Indexing mode.Mode is applicable for only FULL type and Possible value mode = REINDEX", dataType="string",required=false,example="REINDEX"),
		@ApiImplicitParam(name="switchAlias",dataType="string",required=true,example="SWITCH" ,value="Allowed values:"
				+"\nSWITCH: the process will move the read alias from the previous indexes to new indexes. So the DCQ templates and the DCQ Ingestor will use the new indexes."
				+"\nROLLBACK: the process will move the write alias from the new indexes to previous indexes. So the DCQ templates and the DCQ Ingestor will continue to work on the previous indexes."
				+"\nIf this service is executed multiple times, no issues will raise."
				)
		})
	@ApiResponses({
		@ApiResponse(code=400, message="ACN_3019: Invalid parameter [param name]"),
		@ApiResponse(code=200, message="ACN_200: OK"),
		@ApiResponse(code=500, message="ACN_300: GENERIC ERROR")

	})
	public ResponseEntity<GenericResponse> switchOrRollbackAlias(@PathVariable("mode") String mode) throws ApplicationException {
		GenericResponse genericResponse = null;
		try {
			Configuration config = configurator.getConfiguration(ThreadContext.get(CommonConstants.LogParamsL4j2.TN));
			genericResponse = dcqVodIngestorService.switckOrRollbackAlias(mode,config);
		}catch (Exception e) {
			throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR);
		}
		return ResponseEntity.ok(genericResponse);
	}
	

}
