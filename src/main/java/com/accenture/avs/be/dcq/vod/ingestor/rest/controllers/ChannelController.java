package com.accenture.avs.be.dcq.vod.ingestor.rest.controllers;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.avs.be.dcq.vod.ingestor.dto.Response;
import com.accenture.avs.be.dcq.vod.ingestor.service.ChannelService;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.framework.annotation.AvsRestController;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.cache.Configurator;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.commons.lib.support.CommonConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@AvsRestController
@Api(tags="Channel Ingestion services" ,value = "Channel API's", description = "API's pertaining to create/update vod channels")
public class ChannelController {

	@Autowired
	private Configurator configurator;
	
	@Autowired
	private ChannelService channelService;
	
	@Deprecated
	@RequestMapping(value = "/channel/{channelId}", method = RequestMethod.POST)
	@ApiOperation(value = "updateChannel", notes = "Please find the page for the IFA: https://fleet.alm.accenture.com/avswiki/display/AVSM3DOC/.DCQ+VOD+Ingestor-XML+v6.5")
	public @ResponseBody
	ResponseEntity<Response> updateChannel(@PathVariable("channelId") String channelId, 
			@RequestBody String channelXml) throws ApplicationException {
		Response response = new Response();
		try {
			Configuration config = configurator.getConfiguration(ThreadContext.get(CommonConstants.LogParamsL4j2.TN));
			channelService.upsertChannel( channelXml, config);
		} catch (ApplicationException ae) {
			if(ae.getExceptionId().contains("3019")||ae.getExceptionId().contains("3000")) {
				
				if(ae.getParameters().getParameters()[0]!=null && ae.getParameters().getParameters()[0].contains("sdp")) {
					response.setResultCode(DcqVodIngestorConstants.KO);
					response.setErrorCode("ERR008");
					response.setErrorDescription("Invalid Parameter "+ae.getParameters().getParameters()[0]);
					return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
				}else {
					response.setResultCode(DcqVodIngestorConstants.KO);
					response.setErrorCode("ERR001-VALIDATION");
					response.setErrorDescription("Invalid Parameter "+ae.getParameters().getParameters()[0]);
					return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
				}
			}
		}
		catch (Exception e) {
			response.setResultCode(DcqVodIngestorConstants.KO);
			response.setErrorCode("ERR004-GENERIC");
			response.setErrorDescription(e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			
			}
		return ResponseEntity.ok(response);
	}

}
