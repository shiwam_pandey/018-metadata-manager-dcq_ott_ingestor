package com.accenture.avs.be.dcq.vod.ingestor.v1.manager;

import java.util.List;
import java.util.Set;

import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.bundle.asset.BundleDTO;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ConfigurationException;
import com.accenture.avs.persistence.technicalcatalogue.BlacklistDeviceEntity;
import com.accenture.avs.persistence.technicalcatalogue.ContentEntity;
import com.accenture.avs.persistence.technicalcatalogue.RelPlatformTechnicalEntity;
import com.accenture.avs.persistence.technicalcatalogue.TechnicalPackageEntity;


/**
 * @author naga.sireesha.meka
 *
 */
public interface BundleV1Manager {
	
	public List<Object> getContentIdsByBundleContentId(Integer bundleContentId);
	void deleteFromBundleAggregation(Integer bundleContentId);
	void deleteSVODMapping(Integer bundleId);
	void deleteTVODRelPlatform(String packageName);
	public Set<Integer> getAllSVODPackages(ContentEntity bundleContent);
	TechnicalPackageEntity getTechnicalPackages(BundleDTO bundle, ContentEntity content,String packageType,Configuration config) throws ConfigurationException;
	RelPlatformTechnicalEntity getRelPlatformTechnical(ContentEntity content, Integer cpId, Integer PackageId,
			Integer bundleId);
	void createBundle(Set bundleAggregationList);
	Set<BlacklistDeviceEntity> getBlacklistDeviceContent(BundleDTO bundle, ContentEntity bundleContent,Configuration config) throws ApplicationException;

}

