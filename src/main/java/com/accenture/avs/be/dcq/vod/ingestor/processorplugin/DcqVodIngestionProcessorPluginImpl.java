package com.accenture.avs.be.dcq.vod.ingestor.processorplugin;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionWriterRequestDTO;
import com.accenture.avs.be.framework.exception.ApplicationException;

/**
 * @author karthik.vadla
 *
 */
@Component
public class DcqVodIngestionProcessorPluginImpl implements DcqVodIngestionProcessorPlugin {

	@Override
	public List<DcqVodIngestionWriterRequestDTO> updateMetadata(List<DcqVodIngestionWriterRequestDTO> dcqVodIngestionWriterRequestDTOs, String assetType,
			 String transactionNumber, ApplicationContext context) throws ApplicationException {
		return dcqVodIngestionWriterRequestDTOs;
	}
	
}
