package com.accenture.avs.be.dcq.vod.ingestor.v1.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import com.accenture.avs.be.dcq.vod.ingestor.manager.ContentCategoryManager;
import com.accenture.avs.be.dcq.vod.ingestor.manager.VodManager;
import com.accenture.avs.be.dcq.vod.ingestor.service.ContentCategoryService;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.contentcategory.asset.CategoryContent;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.configuration.CacheConstants;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.commons.lib.LoggerWrapper;

@Service
@Qualifier("ContentCategoryV1")
public class ContentCategoryV1ServiceImpl implements ContentCategoryService {

	@Autowired
	private ResourceLoader resourceLoader;

	@Autowired
	private ContentCategoryManager contentCategoryManager;

	@Autowired
	private VodManager vodManager;
	private static final LoggerWrapper log = new LoggerWrapper(ContentCategoryV1ServiceImpl.class);

	@Override
	public GenericResponse ingestContentCategory(String contentCategoryJSONStr, String transactionNumber,
			Configuration config) throws Exception {

		return null;

	}

	@Override
	public GenericResponse ingestContentCategory(CategoryContent categoryContent, String transactionNumber,
			Configuration config) throws Exception {
		log.logMessage("call to ingestContentContent ");
		GenericResponse genericResponse = null;
		try {
			contentCategoryManager.ingestCategoryContent(categoryContent, transactionNumber, config);
			genericResponse = new GenericResponse(DcqVodIngestorConstants.ACN_200, DcqVodIngestorConstants.OK, "");
		} catch (SAXException ex) {
			throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters("JSON. Details : " + ex.getMessage()));

		} catch (ApplicationException ae) {
			log.logError(ae);
			throw ae;

		} catch (Exception e) {
			throw e;
		}
		return genericResponse;
	}
}


