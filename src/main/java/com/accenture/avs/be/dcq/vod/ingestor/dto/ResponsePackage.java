package com.accenture.avs.be.dcq.vod.ingestor.dto;
import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import io.swagger.annotations.ApiModelProperty;

/**
 * The response bean class for the TECHNICAL_PACKAGE
 * 
 */
public class ResponsePackage  implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(required = true, value = "Technical package Id created on AVS" , example ="3")
	private Long packageId;
	@ApiModelProperty(required = false, value = "Technical Package Description", example ="Live technical package")
	private String packageDescription;
	@ApiModelProperty(required = true, value = "Name of the Technical Package",example ="PKG_TVOD_Pricing_1537770395_60_1515651203")
	private String packageName;
	@ApiModelProperty(required = true, value = "Type of the Technical Package",example = "RVOD")
	private String teckPkgType;
	@ApiModelProperty(required = true, value = "External Id for that technical package" ,example = "TP_EXT_3")
	private String externalId;
	@ApiModelProperty(required = true, value = "flag to identify if a package is included in a ad funded commercial package or not . Possible values Y/N" ,example = "Y")
	private String adFunded;
	@ApiModelProperty(required = false, value = "Property name of the Technical Package", example = "India24")
	private String propertyName;
	
	
	

    public String getAdFunded() {
		return adFunded;
	}

	public void setAdFunded(String adFunded) {
		this.adFunded = adFunded;
	}

	public ResponsePackage() {
    }

	public Long getPackageId() {
		return this.packageId;
	}
	public void setPackageId(Long packageId) {
		this.packageId = packageId;
	}

	public String getPackageDescription() {
		return this.packageDescription;
	}
	public void setPackageDescription(String packageDescription) {
		this.packageDescription = packageDescription;
	}

	public String getPackageName() {
		return this.packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	//bi-directional many-to-one association to TeckPkgType
	public String getTeckPkgType() {
		return this.teckPkgType;
	}
	public void setTeckPkgType(String teckPkgType) {
		this.teckPkgType = teckPkgType;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	
	

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public boolean equals(Object other) {
		boolean flag;
		if (this == other) {
			flag= true;
		}
		if (other instanceof ResponsePackage) {
			ResponsePackage castOther = (ResponsePackage) other;
			flag = new EqualsBuilder().append(this.getPackageId(), castOther.getPackageId()).isEquals();
		} else {
			flag = false;
		}
		return flag;
    }
    
	public int hashCode() {
		return new HashCodeBuilder()
			.append(getPackageId())
			.toHashCode();
    }   

	public String toString() {
		return new ToStringBuilder(this)
			.append("packageId", getPackageId())
			.toString();
	}
}
