package com.accenture.avs.be.dcq.vod.ingestor.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.avs.persistence.dto.SubtitleLangDetailsDTO;
import com.accenture.avs.persistence.technicalcatalogue.SubtitleEntity;
/**
 * @author karthik.vadla
 *
 */
@Repository
public interface SubtitlesRepository extends JpaRepository<SubtitleEntity,Integer>{

	/**
	 * @param allPublishedCpIds
	 * @return
	 */
	public List<SubtitleLangDetailsDTO> retrieveSubTitlesDetailsByCpIds(@Param("cpIds") List<Integer> allPublishedCpIds);

	@Modifying
	@Transactional
	public void deleteByContentId(@Param("contentId")Integer contentId);

}
