package com.accenture.avs.be.dcq.vod.ingestor.v1.dto.contentcategory.asset;

import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class CategoryContent {

	@ApiModelProperty(value = "Unique identifier of the category ID. Note : categoryId or externalId is mandatory", required = false, example = "1")
	private Integer categoryId;

	@ApiModelProperty(value = "Unique identifier of the external ID", required = false, example = "ext1")
	private String externalId;

	private List<Content> contents = null;

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public List<Content> getContents() {
		return contents;
	}

	public void setContents(List<Content> contents) {
		this.contents = contents;
	}

	public static class Content {

		@ApiModelProperty(value = "Unique identifier of the external ID. Note : externalId or contentId is mandatory", required = false, example = "ext2")
		private String externalContentId;

		@ApiModelProperty(value = "Unique identifier of the content ID", required = false, example = "2")
		private Integer contentId;

		@ApiModelProperty(value = "End date of the content in the category", required = false)
		private Date endDate;

		@ApiModelProperty(value = "To identify the content is primary on the category", required = false, allowableValues = "Y , N")
		private String isPrimary;

		@ApiModelProperty(value = "Position of the content in the category", required = false, example = "1")
		private Integer position;

		@ApiModelProperty(value = "Start date of the content in the category", required = false)
		private Date startDate;

		public String getExternalContentId() {
			return externalContentId;
		}

		public void setExternalContentId(String externalContentId) {
			this.externalContentId = externalContentId;
		}

		public Integer getContentId() {
			return contentId;
		}

		public void setContentId(Integer contentId) {
			this.contentId = contentId;
		}

		public Date getEndDate() {
			return endDate;
		}

		public void setEndDate(Date endDate) {
			this.endDate = endDate;
		}

		public String getIsPrimary() {
			return isPrimary;
		}

		public void setIsPrimary(String isPrimary) {
			this.isPrimary = isPrimary;
		}

		public Integer getPosition() {
			return position;
		}

		public void setPosition(Integer position) {
			this.position = position;
		}

		public Date getStartDate() {
			return startDate;
		}

		public void setStartDate(Date startDate) {
			this.startDate = startDate;
		}
	}

}
