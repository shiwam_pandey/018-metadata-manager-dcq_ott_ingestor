package com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author karthik.vadla
 *
 */
public class AudioLanguageDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "Language type identifier,It is a mandatory attribute of the PreferredLang or Lang tags", required = true, example = "1")
	private String audioId;
	
	@ApiModelProperty(value = "audioLanguageCode", required = true, example = "")
	private String audioLanguageCode;
	
	@ApiModelProperty(value = "Language type stream,It is a mandatory attribute of the PreferredLang or Lang tags", required = true, example = "")
	private Integer streamId;
	
	@ApiModelProperty(value = "", required = true, example = "")
	private String audioLanguageName;
	
	@ApiModelProperty(value = "", required = true, example = "")
	private String isPreferred;
	public String getAudioId() {
		return audioId;
	}
	public void setAudioId(String audioId) {
		this.audioId = audioId;
	}
	public String getAudioLanguageCode() {
		return audioLanguageCode;
	}
	public void setAudioLanguageCode(String audioLanguageCode) {
		this.audioLanguageCode = audioLanguageCode;
	}
	public Integer getStreamId() {
		return streamId;
	}
	public void setStreamId(Integer streamId) {
		this.streamId = streamId;
	}
	public String getAudioLanguageName() {
		return audioLanguageName;
	}
	public void setAudioLanguageName(String audioLanguageName) {
		this.audioLanguageName = audioLanguageName;
	}
	public String getIsPreferred() {
		return isPreferred;
	}
	public void setIsPreferred(String isPreferred) {
		this.isPreferred = isPreferred;
	}
	
}
