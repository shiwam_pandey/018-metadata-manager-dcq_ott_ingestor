package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author karthik.vadla
 *
 */
public class PackageResponseDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(required = true, value = "total number of results")
	private int totalResults;
	@ApiModelProperty(required = true, value = "Packages")
	private List<ResponsePackage> packages;
	public int getTotalResults() {
		return totalResults;
	}
	public void setTotalResults(int totalResults) {
		this.totalResults = totalResults;
	}
	public List<ResponsePackage> getPackages() {
		return packages;
	}
	public void setPackages(List<ResponsePackage> packages) {
		this.packages = packages;
	}
	
	
}
