package com.accenture.avs.be.dcq.vod.ingestor.gateway.input;

/**
 * The input all gateways will consume.
 */
public interface GatewayInput {
}
