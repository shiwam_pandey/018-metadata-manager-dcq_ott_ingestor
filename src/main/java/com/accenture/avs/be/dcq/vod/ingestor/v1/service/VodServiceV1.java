package com.accenture.avs.be.dcq.vod.ingestor.v1.service;

import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.vod.asset.VodRequestDTO;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;

/**
 * @author karthik.vadla
 *
 */
public interface VodServiceV1 {

	public GenericResponse ingestContent(VodRequestDTO vodDTO, Configuration config) throws ApplicationException, Exception;
	
}
