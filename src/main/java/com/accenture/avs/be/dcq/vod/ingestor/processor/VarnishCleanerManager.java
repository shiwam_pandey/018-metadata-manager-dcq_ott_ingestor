package com.accenture.avs.be.dcq.vod.ingestor.processor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.utils.CacheConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorRestUrls;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.commons.lib.httpclient.HttpClientAdapter;
import com.accenture.avs.commons.lib.httpclient.HttpClientAdapter.HttpClientAdapterConfiguration;

/**
 * @author karthik.vadla
 *
 */
@Component
public class VarnishCleanerManager implements DcqVodIngestorConstants {
	private static LoggerWrapper log = new LoggerWrapper(VarnishCleanerManager.class);
	
	@Autowired
	private DcqVodIngestorRestUrls dcqVodIngestorRestUrls;
	public VarnishCleanerManager() {

	}

	public void clean(Configuration config) throws ApplicationException {
		long startTime = System.currentTimeMillis();
		try {

			String varnishIpAddressesStr = dcqVodIngestorRestUrls.DCQ_VOD_VARNISH_SERVER_URLS;
			
			if (!DcqVodIngestorUtils.isEmpty(varnishIpAddressesStr)) {
				String[] varnishIpAddresses = varnishIpAddressesStr.split(PROP_VALUE_SEPARATOR);
				String[] apiNames = config.getConstants().getValue(CacheConstants.DCQ_VOD_API_NAMES_TO_CLEAR_IN_VARNISH).split(PROP_VALUE_SEPARATOR);
				for (String url : varnishIpAddresses) {
					for (String apiName : apiNames) {
						callPurgeVarnish(url, apiName, "", "", config); // TODO - Not needed contentId
						Thread.sleep(Long.valueOf(config.getConstants().getValue(CacheConstants.DCQ_VOD_SLEEP_TIME_BETWEEN_VARNISH_INVALIDATION)));
					}
				}
			}
		} catch (Exception e) {
			log.logError(e);
			// throw new ApplicationException(e);
		} finally {
			log.logMessage("Varnish Invalidation Completed, took: {} ms" , (System.currentTimeMillis() - startTime) );
		}
	}

	/**
	 * @param url
	 * @param apiName
	 * @param key
	 * @param value
	 * @param config
	 * @throws Exception
	 */
	private void callPurgeVarnish(String url, String apiName, String key, String value, Configuration config)
			throws Exception {
		String purgeurl = url + URL_SEPARATOR + VARNISH_WILDCARD + apiName + VARNISH_WILDCARD;
		log.logMessage("purgeurl: {}" , purgeurl);
		int connectionTimeOutValue = 0;
		int socketTimeOutValue = 0;
		String tenantName = config.getConstants().getValue(CacheConstants.DCQ_VOD_INGESTOR_TENANT_NAME);
		if(null!=config.getConstants().getValue(CacheConstants.DCQ_VOD_VARNISH_INVALIDATOR_CONNECTION_TIMEOUT)){
		connectionTimeOutValue = Integer.parseInt(config.getConstants().getValue(CacheConstants.DCQ_VOD_VARNISH_INVALIDATOR_CONNECTION_TIMEOUT));
		}
		if(null!=config.getConstants().getValue(CacheConstants.DCQ_VOD_VARNISH_INVALIDATOR_SOCKET_TIMEOUT)){
		socketTimeOutValue = Integer.parseInt(config.getConstants().getValue(CacheConstants.DCQ_VOD_VARNISH_INVALIDATOR_SOCKET_TIMEOUT)) ;
		}
		HttpClientAdapterConfiguration httpConfig = DcqVodIngestorUtils.getHttpClientAdapterConfiguration(tenantName, config);
		httpConfig.setConnectionTimeout(connectionTimeOutValue);
		httpConfig.setSocketTimeout(socketTimeOutValue);
		String resp = HttpClientAdapter.doPurge(httpConfig,purgeurl, null, null,null);
		log.logMessage("Varnish Invalidator Response for : {} is: {}" , purgeurl , resp);
	}
}
