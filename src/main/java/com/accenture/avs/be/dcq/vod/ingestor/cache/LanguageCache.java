package com.accenture.avs.be.dcq.vod.ingestor.cache;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.repository.LanguageRepository;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.persistence.technicalcatalogue.LanguageMetadataEntity;

/**
 * @author karthik.vadla
 *
 */
@Component
public class LanguageCache {
	private static final LoggerWrapper log = new LoggerWrapper(LanguageCache.class);
	private Map<String, String> languageCache = new HashMap<String, String>();
	private String defaultLangCode;

	@Autowired
	private LanguageRepository languageRepository;

	/**
	 * @return
	 */
	public Map<String, String> loadLanguagesCache() {
		log.logMessage("Loading Language cache");
		try {
			for (LanguageMetadataEntity param : languageRepository.findAll()) {
				languageCache.put(param.getLanguageCode(), param.getLanguageName());
				if (!DcqVodIngestorUtils.isEmpty(param.getIsDefault())
						&& String.valueOf(param.getIsDefault()).equals(DcqVodIngestorConstants.SHORT_YES)) {
					defaultLangCode = param.getLanguageCode();
				}
			}
			if (DcqVodIngestorUtils.isEmpty(defaultLangCode)) {
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3212_INTERNAL_SERVER_ERROR,
						new NestedParameters("Erro while loading languages"));
			}

		} catch (Exception e) {
			log.logError(e);
		}
		return languageCache;
	}

	public Map<String, String> getLanguages() {
		return languageCache;
	}

	public String getDefaultLanguage() {
		return defaultLangCode;
	}
}
