package com.accenture.avs.be.dcq.vod.ingestor.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accenture.avs.be.dcq.vod.ingestor.dto.LanguageDTO;
import com.accenture.avs.be.dcq.vod.ingestor.manager.LanguageManager;
import com.accenture.avs.be.dcq.vod.ingestor.service.LanguageService;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.LanguageUtils;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.configuration.CacheConstants;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;

@Service
public class LanguageServiceImpl implements LanguageService {

	@Autowired
private	LanguageManager languageManager;
	@Override
	public GenericResponse<LanguageDTO> activateOrDeactivateLanguage(String status, String langCode) throws ApplicationException {
		// TODO Auto-generated method stub
		GenericResponse<LanguageDTO> response = null;
		
		if (StringUtils.isEmpty(langCode)) {
			throw new ApplicationException(
					CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
					new NestedParameters(DcqVodIngestorConstants.LANGUAGECONSTANTS.LANGUAGECODE));
		}
		if(!LanguageUtils.validateLangCode(langCode))
		{
			throw new ApplicationException(
					CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters(DcqVodIngestorConstants.LANGUAGECONSTANTS.LANGUAGECODE));

		}
		
		response = languageManager.activateOrDeactivateLanguage(status, langCode);
		
		return response;
	}

}
