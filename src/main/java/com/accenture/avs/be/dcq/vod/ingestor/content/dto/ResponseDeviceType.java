package com.accenture.avs.be.dcq.vod.ingestor.content.dto;

import java.io.Serializable;

public class ResponseDeviceType implements Serializable{

	private static final long serialVersionUID = 1L;
	private String resultCode;
	private String errorCode;
	private String errorDescription;
	private Object resultObj;
	public final static String RESULT_CODE_OK = "OK";
	public final static String RESULT_CODE_KO = "KO";
	
	public String getResultCode() {
		return resultCode;
	}
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorDescription() {
		return errorDescription;
	}
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
	public Object getResultObj() {
		return resultObj;
	}
	public void setResultObj(Object resultObj) {
		this.resultObj = resultObj;
	}
}
