package com.accenture.avs.be.dcq.vod.ingestor.executor;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.dto.CommercialPackagesResponseDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.CommercialPackagesResponseDTO.CommercialPackage;
import com.accenture.avs.be.dcq.vod.ingestor.dto.ResponsePackage;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorRestUrls;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.commons.lib.LoggerWrapper.OtherSystemCallType;
import com.accenture.avs.commons.lib.httpclient.HttpClientAdapter;
import com.accenture.avs.commons.lib.httpclient.HttpClientAdapter.HttpClientAdapterConfiguration;
import com.accenture.avs.commons.lib.httpclient.HttpClientAdapter.HttpClientException;
import com.accenture.avs.persistence.technicalcatalogue.TechnicalPackageEntity;

@Component
@Scope("prototype")
public class PackageThreadPoolExecutor implements Callable<List<ResponsePackage>> {
	private static final LoggerWrapper log = new LoggerWrapper(PackageThreadPoolExecutor.class);

	@Autowired
	private DcqVodIngestorRestUrls dcqVodIngestorRestUrl;

	private List<TechnicalPackageEntity> techicalPackages;
	private List<String> technicalPackageIds;
	private List<String> propertyNames;
	private Configuration config;
	private String commercialPackageUrl;

	public PackageThreadPoolExecutor() {
	}

	public PackageThreadPoolExecutor(List<String> technicalPackageIds, List<TechnicalPackageEntity> techicalPackages,
			List<String> propertyNames, Configuration config, String commercialPackageUrl) {
		super();
		this.techicalPackages = techicalPackages;
		this.technicalPackageIds = technicalPackageIds;
		this.propertyNames = propertyNames;
		this.config = config;
		this.commercialPackageUrl = commercialPackageUrl;
	}

	@Override
	public List<ResponsePackage> call() throws Exception {
		long startTime = new Date().getTime();
		CommercialPackagesResponseDTO commercialPackages = null;
		String resultCode = "";
		String resultDesc = "";
		List<ResponsePackage> responsePackages = new ArrayList<>();
		Map<Integer, String> technicalPackageCommerceModelMap = new HashMap<>();
		String technicalPackagesString = String.join(",", technicalPackageIds);
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> headerParamsMap = new HashMap<String, String>();
		Map<String, String> paramsMap = new HashMap<String, String>();
		paramsMap.put("technicalPackages", technicalPackagesString);
		paramsMap.put("includes", "details,technicalPackages");

		try {
			HttpClientAdapterConfiguration httpConfig = DcqVodIngestorUtils
					.getHttpClientAdapterConfiguration("tenantDefault", config);
			String response = HttpClientAdapter.doGet(httpConfig, commercialPackageUrl, paramsMap, null,
					headerParamsMap);

			mapper.enable(SerializationConfig.Feature.INDENT_OUTPUT);
			mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			JsonNode jsonNode = mapper.readTree(response);
			commercialPackages = mapper.readValue(jsonNode.findValue("resultObj"), CommercialPackagesResponseDTO.class);
			resultCode = mapper.readValue(jsonNode.findValue("resultCode"), String.class);
			resultDesc = mapper.readValue(jsonNode.findValue("resultDescription"), String.class);

			log.logCallToOtherSystemEnd("Commerce", "getCommercialPackagesByTechnicalPackages", "",
					DcqVodIngestorConstants.OK, resultCode, resultDesc, System.currentTimeMillis() - startTime,
					OtherSystemCallType.INTERNAL);
			if (commercialPackages != null) {
				for (CommercialPackage commercialPackage : commercialPackages.getCommercialPackages()) {
					if (commercialPackage.getRelationships().containsKey("technicalPackages")) {
						technicalPackageCommerceModelMap.put(Integer.parseInt(
								commercialPackage.getRelationships().get("technicalPackages").get(0).toString()),
								commercialPackage.getCommercialModel());
					}
				}
			}

			if (propertyNames.isEmpty()) {

				for (TechnicalPackageEntity technicalPackage : techicalPackages) {

					if (technicalPackageCommerceModelMap.containsKey(technicalPackage.getPackageId())
							&& (technicalPackage.getPropertyName() == null
									|| technicalPackage.getPropertyName().equals(""))) {
						ResponsePackage responsePackage = new ResponsePackage();

						responsePackage.setAdFunded(
								technicalPackageCommerceModelMap.get(technicalPackage.getPackageId()) != null
										&& !technicalPackageCommerceModelMap.get(technicalPackage.getPackageId())
												.isEmpty() ? "Y" : "N");
						responsePackage.setExternalId(technicalPackage.getExternalId());
						responsePackage.setPackageDescription(technicalPackage.getPackageDescription());
						responsePackage.setPackageId(technicalPackage.getPackageId().longValue());
						responsePackage.setPackageName(technicalPackage.getPackageName());
						responsePackage.setTeckPkgType(technicalPackage.getTeckPackageType());
						responsePackages.add(responsePackage);
					}

				}
			
			} else {
				

				for (TechnicalPackageEntity technicalPackage : techicalPackages) {

					if (technicalPackageCommerceModelMap.containsKey(technicalPackage.getPackageId())
							&& technicalPackage.getPropertyName() != null
							&& !technicalPackage.getPropertyName().isEmpty()
							&& propertyNames.contains(technicalPackage.getPropertyName())) {
						ResponsePackage responsePackage = new ResponsePackage();

						responsePackage.setAdFunded(
								technicalPackageCommerceModelMap.get(technicalPackage.getPackageId()) != null
										&& !technicalPackageCommerceModelMap.get(technicalPackage.getPackageId())
												.isEmpty() ? "Y" : "N");
						responsePackage.setExternalId(technicalPackage.getExternalId());
						responsePackage.setPackageDescription(technicalPackage.getPackageDescription());
						responsePackage.setPackageId(technicalPackage.getPackageId().longValue());
						responsePackage.setPackageName(technicalPackage.getPackageName());
						responsePackage.setTeckPkgType(technicalPackage.getTeckPackageType());
						responsePackage.setPropertyName(technicalPackage.getPropertyName());
						responsePackages.add(responsePackage);
					}

				}
			
			}

		} catch (HttpClientException hce) {
			DcqVodIngestorUtils.checkServiceUnavailability(hce, MessageKeys.ERROR_MS_ACN_901_COMMERCE_NOT_AVAILABLE);
		} catch (Exception e) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3212_INTERNAL_SERVER_ERROR);
		}

		return responsePackages;
	}

}
