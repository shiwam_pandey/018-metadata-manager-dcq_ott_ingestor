package com.accenture.avs.be.dcq.vod.ingestor.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.accenture.avs.persistence.dto.BundleDetailsDTO;
import com.accenture.avs.persistence.dto.GroupOfBundleDetailsDTO;
import com.accenture.avs.persistence.technicalcatalogue.BundleAggregationEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.Modifying;
/**
 * @author karthik.vadla
 *
 */
@Repository
public interface BundleAggregationRepository extends JpaRepository<BundleAggregationEntity,Integer>{

	/**
	 * @param allPublishedCpIds
	 * @return
	 */
	public List<BundleDetailsDTO> retrieveDetailsDTOByCpIds(@Param("cpIds")List<Integer> allPublishedCpIds);
	/**
	 * @param bundleIdsList
	 * @return
	 */
	public List<GroupOfBundleDetailsDTO> retrieveDetailsByContentId(@Param("contentIds")List<Integer> bundleIdsList);
	
	public List<Object> retrieveContentsByBundleContentId(@Param("bundleContentId")Integer bundleContentId);
	
	@Transactional
	@Modifying
	void deleteBundleContentByBundleContentID(@Param("bundleContentId")Integer bundleContentId);
	
	public List<Object> retrieveTechPkdIdsByBundleContentId(@Param("bundleContentId")Integer bundleContentId);
}
