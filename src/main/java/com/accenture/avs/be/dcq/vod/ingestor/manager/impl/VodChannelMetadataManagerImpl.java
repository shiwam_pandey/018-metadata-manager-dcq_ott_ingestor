package com.accenture.avs.be.dcq.vod.ingestor.manager.impl;

import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.rest.RestStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.accenture.avs.be.dcq.vod.ingestor.dto.DCQEventCollectorChannelDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionWriterRequestDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionWriterResponseDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.ESVodChannelDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.ESVodChannelDTO.MetaDataDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.ESVodChannelDTO.PlatformInfoDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.ESVodChannelDTO.SuggestDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.ESVodChannelDTO.TechnicalPackagesDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.ESVodChannelDTO.VodChannel;
import com.accenture.avs.be.dcq.vod.ingestor.dto.Platforms;
import com.accenture.avs.be.dcq.vod.ingestor.dto.TechnicalPackagesResultsDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.channel.asset.ChannelPlatformType;
import com.accenture.avs.be.dcq.vod.ingestor.dto.channel.asset.Channels;
import com.accenture.avs.be.dcq.vod.ingestor.dto.channel.asset.Channels.Channel;
import com.accenture.avs.be.dcq.vod.ingestor.factories.GatewayFactory;
import com.accenture.avs.be.dcq.vod.ingestor.manager.VodChannelMetadataManager;
import com.accenture.avs.be.dcq.vod.ingestor.processor.DcqVodIngestionPostProcessor;
import com.accenture.avs.be.dcq.vod.ingestor.processor.VarnishCleanerManager;
import com.accenture.avs.be.dcq.vod.ingestor.processorplugin.DcqVodIngestionProcessorPlugin;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ChannelRepository;
import com.accenture.avs.be.dcq.vod.ingestor.utils.CacheConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.JsonUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.XmlHelper;
import com.accenture.avs.be.dcq.vod.ingestor.writer.DcqVodIngestionWriter;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.cache.types.impl.PlatformsRemoteCache.PlatformValue;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ConfigurationException;
import com.accenture.avs.be.framework.utils.BooleanConverter;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.es.manager.ElasticSearchIntegratedClientManager;
import com.accenture.avs.persistence.technicalcatalogue.ChannelEntity;
import com.accenture.avs.persistence.technicalcatalogue.ChannelPlatformEntity;

/**
 * @author karthik.vadla
 *
 */
@Component
public class VodChannelMetadataManagerImpl implements VodChannelMetadataManager{

	private static LoggerWrapper log = new LoggerWrapper(VodChannelMetadataManagerImpl.class);
	private static JAXBContext jcChannels;
	
	@Autowired
	private ApplicationContext context;
	@Autowired
	private ChannelRepository channelRepository;
	@Autowired
	private DcqVodIngestionWriter dcqVodIngestionWriter;
	
	@Autowired
	private DcqVodIngestionPostProcessor dcqVodIngestionPostProcessor;
	@Autowired
	private VarnishCleanerManager varnishCleanerManager;
	static {
		try {
			jcChannels = JAXBContext.newInstance(Channels.class);
		} catch (Exception e) {
			log.logError(e);
		}
	}
	
	
	@Override
	public List<DcqVodIngestionWriterRequestDTO> processAndWriteVodChannels(List<Integer> vodChannelIdList, String transactionNumber,
			String startTime, String mode, String indexDate, Configuration config) throws ConfigurationException, ApplicationException, JsonParseException, JsonMappingException, IOException, JAXBException{
		
		List<DcqVodIngestionWriterRequestDTO> ingestionWriterRequestDTOs = null;
		log.logMessage("Category Ingestor Processor - Start");
		
		for(Integer vodChannelId : vodChannelIdList) {
			
			//check if blob contains XML or JSON
			byte[] blobObject  = (byte[]) channelRepository.getChannelXMlBlob(vodChannelId);
			try{
			XmlHelper.latestUnMarshaller(jcChannels,blobObject);
			ingestionWriterRequestDTOs = constructEsDocsForVodChannel(vodChannelId, transactionNumber, mode, indexDate, config);
			}catch(JAXBException jaxbe){
				ingestionWriterRequestDTOs = constructEsDocsForVodChannelJson(vodChannelId, transactionNumber, mode, indexDate, config);
			}
		Set<Integer> publishedIds = null;
		Map<Integer, String> failedIdsMap = null;

		long sTimeWriter = System.currentTimeMillis();
		log.logMessage("Vodchannel Ingestor Writer - Start");
		List<DcqVodIngestionWriterRequestDTO> ingestionWriterRequestDTOsToBePublished = new ArrayList<DcqVodIngestionWriterRequestDTO>();
		if (ingestionWriterRequestDTOs != null && !ingestionWriterRequestDTOs.isEmpty()) {
			publishedIds = new HashSet<Integer>();
			for (DcqVodIngestionWriterRequestDTO dcqVodIngestionWriterRequestDTO : ingestionWriterRequestDTOs) {
				if (dcqVodIngestionWriterRequestDTO.isDeleted()) {
					publishedIds.add(dcqVodIngestionWriterRequestDTO.getEsUniqueId());
				} else {
					ingestionWriterRequestDTOsToBePublished.add(dcqVodIngestionWriterRequestDTO);
				}
			}
		}

		log.logMessage("VodChannel Ingestor Processor Plugin - Start");
		long sTimeProPlugin = System.currentTimeMillis();
		DcqVodIngestionProcessorPlugin dcqVodIngestionProcessorPlugin = GatewayFactory.getDcqVodIngestionProcessorPlugin(config);
		ingestionWriterRequestDTOsToBePublished = dcqVodIngestionProcessorPlugin.updateMetadata(ingestionWriterRequestDTOsToBePublished,
				DcqVodIngestorConstants.VODCHANNEL, transactionNumber, context);
		log.logMessage("VodChannel Ingestor Processor Plugin - Completed,took: {} ms" , (System.currentTimeMillis() - sTimeProPlugin) );

		DcqVodIngestionWriterResponseDTO dcqVodIngestionWriterResponseDTO = dcqVodIngestionWriter.writeToES(ingestionWriterRequestDTOsToBePublished, mode, indexDate, config);
		if (dcqVodIngestionWriterResponseDTO != null) {
			if (publishedIds == null) {
				publishedIds = dcqVodIngestionWriterResponseDTO.getPublishedIds();
			}
			else {
				publishedIds.addAll(dcqVodIngestionWriterResponseDTO.getPublishedIds());
			}
			failedIdsMap = dcqVodIngestionWriterResponseDTO.getFailedIdsMap();
		}
		
		//Start:Added as part of Story AVS 9263
		log.logMessage("Event Collector code -  Start");
		if(null != dcqVodIngestionWriterResponseDTO && !CollectionUtils.isEmpty(dcqVodIngestionWriterResponseDTO.getPublishedIds())){
			for(DcqVodIngestionWriterRequestDTO  ingestionWriterRequestDTO :ingestionWriterRequestDTOsToBePublished){					
				if(dcqVodIngestionWriterResponseDTO.getPublishedIds().contains(ingestionWriterRequestDTO.getEsUniqueId())){
					ESVodChannelDTO vodChannelDTO = (ESVodChannelDTO)ingestionWriterRequestDTO.getEsDocObj();
					DCQEventCollectorChannelDTO channelDTO = new DCQEventCollectorChannelDTO();						
					channelDTO.setAction(DcqVodIngestorConstants.UPSERT);
					channelDTO.setContentType(DcqVodIngestorConstants.VODCHANNEL);
					channelDTO.setChannelId(vodChannelDTO.getVodChannel().getChannelId());
					channelDTO.setExtChannelId(vodChannelDTO.getVodChannel().getMetadata().getExternalId());
					channelDTO.setChannelType(vodChannelDTO.getVodChannel().getMetadata().getType());
					channelDTO.setPlatform(vodChannelDTO.getVodChannel().getPlatformName().toUpperCase());
					String payLoad = JsonUtils.writeAsJsonStringWithoutNull(channelDTO);										
					DcqVodIngestorUtils.sendEventCollectorReport(DcqVodIngestorConstants.DCQ_VOD_CHANNEL_INGESTION,
																	DcqVodIngestorConstants.VODCHANNEL_INGESTION,
																	payLoad, config);
				}
			}
		}			
		//End: AVS 9263
		
		log.logMessage("Vodchannel Ingestor Writer - Completed,took: {} ms" , (System.currentTimeMillis() - sTimeWriter));

		
		Boolean invalidateVarnish = false;
		long sTimePProcescor = System.currentTimeMillis();
		log.logMessage("Vodchannel Ingestor Post Procescor - Start");
		if (null != publishedIds && !publishedIds.isEmpty()) {
			dcqVodIngestionPostProcessor.deletePublishedAssets( publishedIds,DcqVodIngestorConstants.VODCHANNEL, config);
			DcqVodIngestorUtils.sendPublishedReport( publishedIds, null, startTime,
					DcqVodIngestorConstants.VODCHANNEL, config);
		}
		if (null != failedIdsMap && !failedIdsMap.isEmpty()) {
			dcqVodIngestionPostProcessor.updateStatusAsFailed(failedIdsMap,DcqVodIngestorConstants.VODCHANNEL, config);
		}
		log.logMessage("Vodchannel Ingestor Post Procescor - Completed,took: {} ms" , (System.currentTimeMillis() - sTimePProcescor));


		Boolean invalidateVarnishInPanic = BooleanConverter.getBooleanValue(config.getConstants().getValue(CacheConstants.DCQ_VOD_VARNISH_INVALIDATOR_IN_PANIC));
		if(!invalidateVarnishInPanic && invalidateVarnish){
				log.logMessage("Varnish Invalidator - Start");
				varnishCleanerManager.clean(config);
				log.logMessage("Varnish Invalidator - Completed,took: {} ms" , (System.currentTimeMillis() - sTimePProcescor) );
		}
		}
		
		return null;
	}
	
	
	
	
	
	public List<DcqVodIngestionWriterRequestDTO> constructEsDocsForVodChannel(Integer vodChannelId, String transactionNumber,
			String mode, String indexDate, Configuration config) {
		log.logMessage("Constructing ES Doc for VodChannel.");
		List<DcqVodIngestionWriterRequestDTO> vodChannelDTOsList = null;
		try {
			TechnicalPackagesResultsDTO technicalPackageEntity = getTechnicalPackagesResultsDTO(vodChannelId);
			//TechnicalPackagesResultsDTO technicalPackageEntity = channelRepository.getTechnicalPkgId(vodChannelId);
			Channels channels = null;
			
			channels = (Channels) XmlHelper.latestUnMarshaller(jcChannels, technicalPackageEntity.getXmlFile());
			Channel channel = channels.getChannel().get(0);

			ChannelEntity channelEntity = channelRepository.retreiveChannelByChannelId(vodChannelId);
			byte[] byteArr = channelEntity.getSourceFile();
			
			channelEntity.getChannelPlatforms();
			if (byteArr == null) {
				log.logMessage("Source file is empty for channelId : {}" , vodChannelId);
				throw new ApplicationException("No Source File Found.");
			}
			
			
			Map<String, ChannelPlatformType> platformToChannelPlatformTypeMap = new HashMap<String, ChannelPlatformType>();
			
			if (null != channel.getPlatformList() && null != channel.getPlatformList().getPlatform() && ! channel.getPlatformList().getPlatform().isEmpty()) {
				for (ChannelPlatformType channelPlatformType : channel.getPlatformList().getPlatform()) {
					platformToChannelPlatformTypeMap.put(channelPlatformType.getName().toLowerCase(), channelPlatformType);
				}
			}
			List<ChannelPlatformEntity> channelPlatformEntities = channelEntity.getChannelPlatforms();
			String writeAlias = DcqVodIngestorUtils.getWriteAlias(DcqVodIngestorConstants.VODCHANNEL_IN_LOWER);			
			if(null==channelPlatformEntities || channelPlatformEntities.isEmpty()){
				vodChannelDTOsList = new ArrayList<DcqVodIngestionWriterRequestDTO>();
				DcqVodIngestionWriterRequestDTO writerRequestDTO = new DcqVodIngestionWriterRequestDTO();
					writerRequestDTO.setEsUniqueId(vodChannelId);
					writerRequestDTO.setDeleted(true);
					writerRequestDTO.setEsIndex(writeAlias);
					writerRequestDTO.setEsIndexType(DcqVodIngestorConstants.ALL_IN_LOWER); // This is not needed as this dto will not be used for Indexing but will be used for deleting the from ES.
					vodChannelDTOsList.add(writerRequestDTO);

					ElasticSearchIntegratedClientManager esClientManager = DcqVodIngestorUtils.getElasticSearchIntegratedClientManagerImpl(config);
					byte[]  dcqIngestorUser = DcqVodIngestorUtils.getESDcqIngestorUser();
					byte[] dcqIngestorPassword = DcqVodIngestorUtils.getESDcqIngestorPassword();
				Set<String> channelIds = new HashSet<String>();
				channelIds.add(vodChannelId.toString());
				
				//Set<String> platformNamesLower = PlatformsCache.platformNamesLower;
				log.logMessage("Deleting this channel: {} from ES from all platform",vodChannelId);
				for(String chId : channelIds){
					for (Map.Entry<String, PlatformValue> map : config.getPlatforms().copyCache().entrySet()) {
						
						esClientManager.deleteDocumentById(DcqVodIngestorUtils.getWriteAlias(DcqVodIngestorConstants.VODCHANNEL_IN_LOWER), DcqVodIngestorConstants.ALL_IN_LOWER, dcqIngestorUser, dcqIngestorPassword, chId+DcqVodIngestorConstants.HYPHEN_SYMBOL+map.getKey().toLowerCase());
		
						}
				}
				
				return vodChannelDTOsList;
			}
            if(channelPlatformEntities!=null && !channelPlatformEntities.isEmpty()){
				
				//Set<String> platformNamesLower = PlatformsCache.platformNamesLower;
				for (Map.Entry<String, PlatformValue> map : config.getPlatforms().copyCache().entrySet()) {
					DeleteResponse deleteResponse = DcqVodIngestorUtils.deleteDocumentFromES( DcqVodIngestorUtils.getWriteAlias(DcqVodIngestorConstants.VODCHANNEL_IN_LOWER), DcqVodIngestorConstants.ALL_IN_LOWER,vodChannelId.toString()+DcqVodIngestorConstants.HYPHEN_SYMBOL+map.getKey().toLowerCase(), config);
					
					if(deleteResponse.status().equals(RestStatus.OK)){
					//Start:Added as part of Story AVS 9263
					DCQEventCollectorChannelDTO channelDTO = new DCQEventCollectorChannelDTO();
					channelDTO.setAction(DcqVodIngestorConstants.DELETE);
					channelDTO.setContentType(DcqVodIngestorConstants.VODCHANNEL);
					channelDTO.setChannelId(channel.getChannelId());
					channelDTO.setExtChannelId(channel.getExternalId());
					channelDTO.setChannelType(channel.getType());
					channelDTO.setPlatform(map.getKey());							
					String payLoad = JsonUtils.writeAsJsonStringWithoutNull(channelDTO);
					DcqVodIngestorUtils.sendEventCollectorReport(DcqVodIngestorConstants.DCQ_VOD_CHANNEL_INGESTION,	DcqVodIngestorConstants.VODCHANNEL_INGESTION, payLoad, config);
					//End: Added as part of Story AVS 9263
					}
					}
				vodChannelDTOsList = new ArrayList<DcqVodIngestionWriterRequestDTO>();
				for (ChannelPlatformEntity cpe : channelPlatformEntities) {
					
					String platformName = cpe.getId().getPlatformName().toLowerCase();
					log.logMessage("Preparing DcqVodIngestionWriterRequestDTOs");

					VodChannel vodChannel = new VodChannel();
					MetaDataDTO metaDataDTO = new MetaDataDTO();
					vodChannel.setChannelId(vodChannelId);
					vodChannel.setPlatformName(platformName);
					vodChannel.setMetadata(metaDataDTO);
					
					if (channel.getIsActive() != null && channel.getIsActive().toString().equalsIgnoreCase(DcqVodIngestorConstants.SHORT_NO)) {
						metaDataDTO.setActive(false);
					} else {
						metaDataDTO.setActive(true);
					}

					if (channel.getAdult() != null && channel.getAdult().toString().equalsIgnoreCase(DcqVodIngestorConstants.SHORT_YES)) {
						metaDataDTO.setAdult(true);
					} else {
						metaDataDTO.setAdult(false);
					}

					if (channel.getIsDisAllowedAdv() != null && channel.getIsDisAllowedAdv().toString().equalsIgnoreCase(DcqVodIngestorConstants.SHORT_YES)) {
						metaDataDTO.setADVAllowed(false);
					} else {
						metaDataDTO.setADVAllowed(true);
					}

					metaDataDTO.setAdvTags(channel.getAdvTags());
					metaDataDTO.setBroadcasterName(channel.getBroadcasterName());
					metaDataDTO.setChannelId(Integer.valueOf(channel.getChannelId()).longValue());
					if (channel.getDefaultChannelNumber() != null) {
						metaDataDTO.setDefaultChannelNumber(channel.getDefaultChannelNumber().longValue());
					}
					metaDataDTO.setDescription(channel.getDescription());
					metaDataDTO.setShortDescription(channel.getDescription());
					if(channel.getExtendedMetadata() !=null && !channel.getExtendedMetadata().trim().isEmpty()){
						if(JsonUtils.isJSONValid(channel.getExtendedMetadata())){
							metaDataDTO.setExtendedMetadata(JsonUtils.parseJson(channel.getExtendedMetadata(), Object.class));
						}
						else{
							metaDataDTO.setExtendedMetadata(channel.getExtendedMetadata());
						}
					}
					metaDataDTO.setExternalId(channel.getExternalId());
					metaDataDTO.setName(channel.getName());
					metaDataDTO.setTitle(channel.getName());
					metaDataDTO.setOrderId(Integer.valueOf(channel.getOrder()).longValue());
					metaDataDTO.setType(channel.getType());

					TechnicalPackagesDTO[] techPackagesDTOs = technicalPackageEntity.getTechPackagesDTOs();
					vodChannel.setTechnicalPackages(techPackagesDTOs);

					PlatformInfoDTO platformInfoDTO = populatePlatformInfo(platformName, platformToChannelPlatformTypeMap);
					if(platformInfoDTO!=null){
					platformInfoDTO.setTrailerUrl(cpe.getTrailerUrl());
					platformInfoDTO.setVideoUrl(cpe.getVideoUrl());
					}
					DcqVodIngestionWriterRequestDTO dcqVodIngestionWriterRequestDTO = new DcqVodIngestionWriterRequestDTO();

					if (cpe.getIsPublished().equals(DcqVodIngestorConstants.SHORT_YES)) {
						vodChannelDTOsList.add(dcqVodIngestionWriterRequestDTO);
					} else {
						
						dcqVodIngestionWriterRequestDTO.setDeleted(true);
						vodChannelDTOsList.add(dcqVodIngestionWriterRequestDTO);
					
					}
					
					vodChannel.setPlatformInfo(platformInfoDTO);

					dcqVodIngestionWriterRequestDTO.setEsIndex(writeAlias);
					dcqVodIngestionWriterRequestDTO.setEsIndexType(DcqVodIngestorConstants.ALL_IN_LOWER);
					
					dcqVodIngestionWriterRequestDTO.setPlatformName(platformName);
					dcqVodIngestionWriterRequestDTO.setEsUniqueId(vodChannelId);
					SuggestDTO suggestDTO = new SuggestDTO();
					List<String> suggestValues = constructSuggestFields(metaDataDTO, config);
					suggestDTO.setInput(suggestValues == null ? null : suggestValues.toArray(new String[0]));
					
					ESVodChannelDTO vodChannelDTO = new ESVodChannelDTO();
					vodChannelDTO.setVodChannel(vodChannel);
					vodChannelDTO.setSuggest(suggestDTO);
					vodChannelDTO.setSearchMetadata(suggestValues == null ? null : suggestValues.toArray(new String[0]));
					dcqVodIngestionWriterRequestDTO.setEsDocObj(vodChannelDTO);
					dcqVodIngestionWriterRequestDTO.setSourceFile(channel);
				}
				
			}

		} catch (Exception e) {
			log.logError(e,"JAXBException in constructMetadata");
		}

		log.logMessage("Returning DcqVodIngestionWriterRequestDTOs");
		return vodChannelDTOsList;
	}
	
	
	
	public List<DcqVodIngestionWriterRequestDTO> constructEsDocsForVodChannelJson(Integer vodChannelId, String transactionNumber,
			String mode, String indexDate, Configuration config) {
		log.logMessage("Constructing ES Doc for VodChannelJson.");
		List<DcqVodIngestionWriterRequestDTO> vodChannelDTOsList = null;
		try {
			TechnicalPackagesResultsDTO technicalPackageEntity = getTechnicalPackagesResultsDTO(vodChannelId);
			
			com.accenture.avs.be.dcq.vod.ingestor.dto.Channel	channel = (com.accenture.avs.be.dcq.vod.ingestor.dto.Channel) getObject(technicalPackageEntity.getXmlFile());
			
			ChannelEntity channelEntity = channelRepository.retreiveChannelByChannelId(vodChannelId);
			byte[] byteArr = channelEntity.getSourceFile();
			
			channelEntity.getChannelPlatforms();
			if (byteArr == null) {
				log.logMessage("Source file is empty for channelId : {}" , vodChannelId);
				throw new ApplicationException("No Source File Found.");
			}
			
			
			Map<String, Platforms> platformToChannelPlatformTypeMap = new HashMap<String, Platforms>();
			
			if (null != channel.getPlatforms() &&  channel.getPlatforms().size() > 0) {
				for (Platforms channelPlatformType : channel.getPlatforms()) {
					platformToChannelPlatformTypeMap.put(channelPlatformType.getName().toLowerCase(), channelPlatformType);
				}
			}
			List<ChannelPlatformEntity> channelPlatformEntities = channelEntity.getChannelPlatforms();
			String writeAlias = DcqVodIngestorUtils.getWriteAlias(DcqVodIngestorConstants.VODCHANNEL_IN_LOWER);			
			if(null==channelPlatformEntities || channelPlatformEntities.isEmpty()){
				vodChannelDTOsList = new ArrayList<DcqVodIngestionWriterRequestDTO>();
				DcqVodIngestionWriterRequestDTO writerRequestDTO = new DcqVodIngestionWriterRequestDTO();
					writerRequestDTO.setEsUniqueId(vodChannelId);
					writerRequestDTO.setDeleted(true);
					writerRequestDTO.setEsIndex(writeAlias);
					writerRequestDTO.setEsIndexType(DcqVodIngestorConstants.ALL_IN_LOWER); // This is not needed as this dto will not be used for Indexing but will be used for deleting the from ES.
					vodChannelDTOsList.add(writerRequestDTO);

					ElasticSearchIntegratedClientManager esClientManager = DcqVodIngestorUtils.getElasticSearchIntegratedClientManagerImpl(config);
					byte[]  dcqIngestorUser = DcqVodIngestorUtils.getESDcqIngestorUser();
					byte[] dcqIngestorPassword = DcqVodIngestorUtils.getESDcqIngestorPassword();
				Set<String> channelIds = new HashSet<String>();
				channelIds.add(vodChannelId.toString());
				
				//Set<String> platformNamesLower = PlatformsCache.platformNamesLower;
				log.logMessage("Deleting this channel: {} from ES from all platform",vodChannelId);
				for(String chId : channelIds){
					for (Map.Entry<String, PlatformValue> map : config.getPlatforms().copyCache().entrySet()) {
						
						esClientManager.deleteDocumentById(DcqVodIngestorUtils.getWriteAlias(DcqVodIngestorConstants.VODCHANNEL_IN_LOWER), DcqVodIngestorConstants.ALL_IN_LOWER, dcqIngestorUser, dcqIngestorPassword, chId+DcqVodIngestorConstants.HYPHEN_SYMBOL+map.getKey().toLowerCase());
		
						}
				}
				
				return vodChannelDTOsList;
			}
            if(channelPlatformEntities!=null && !channelPlatformEntities.isEmpty()){
				
				//Set<String> platformNamesLower = PlatformsCache.platformNamesLower;
				for (Map.Entry<String, PlatformValue> map : config.getPlatforms().copyCache().entrySet()) {
					DeleteResponse deleteResponse = DcqVodIngestorUtils.deleteDocumentFromES( DcqVodIngestorUtils.getWriteAlias(DcqVodIngestorConstants.VODCHANNEL_IN_LOWER), DcqVodIngestorConstants.ALL_IN_LOWER,vodChannelId.toString()+DcqVodIngestorConstants.HYPHEN_SYMBOL+map.getKey().toLowerCase(), config);
					
					if(deleteResponse.status().equals(RestStatus.OK)){
					//Start:Added as part of Story AVS 9263
					DCQEventCollectorChannelDTO channelDTO = new DCQEventCollectorChannelDTO();
					channelDTO.setAction(DcqVodIngestorConstants.DELETE);
					channelDTO.setContentType(DcqVodIngestorConstants.VODCHANNEL);
					channelDTO.setChannelId(channel.getChannelId());
					channelDTO.setExtChannelId(channel.getExternalId());
					channelDTO.setChannelType(channel.getType());
					channelDTO.setPlatform(map.getKey());							
					String payLoad = JsonUtils.writeAsJsonStringWithoutNull(channelDTO);
					DcqVodIngestorUtils.sendEventCollectorReport(DcqVodIngestorConstants.DCQ_VOD_CHANNEL_INGESTION,	DcqVodIngestorConstants.VODCHANNEL_INGESTION, payLoad, config);
					//End: Added as part of Story AVS 9263
					}
					}
				vodChannelDTOsList = new ArrayList<DcqVodIngestionWriterRequestDTO>();
				for (ChannelPlatformEntity cpe : channelPlatformEntities) {
					
					String platformName = cpe.getId().getPlatformName().toLowerCase();
					log.logMessage("Preparing DcqVodIngestionWriterRequestDTOs");

					VodChannel vodChannel = new VodChannel();
					MetaDataDTO metaDataDTO = new MetaDataDTO();
					vodChannel.setChannelId(vodChannelId);
					vodChannel.setPlatformName(platformName);
					vodChannel.setMetadata(metaDataDTO);
					
					if (channel.getIsActive() != null && channel.getIsActive().toString().equalsIgnoreCase(DcqVodIngestorConstants.SHORT_NO)) {
						metaDataDTO.setActive(false);
					} else {
						metaDataDTO.setActive(true);
					}

					if (channel.getAdult() != null && channel.getAdult().toString().equalsIgnoreCase(DcqVodIngestorConstants.SHORT_YES)) {
						metaDataDTO.setAdult(true);
					} else {
						metaDataDTO.setAdult(false);
					}

					if (channel.getIsDisAllowedAdv() != null && channel.getIsDisAllowedAdv().toString().equalsIgnoreCase(DcqVodIngestorConstants.SHORT_YES)) {
						metaDataDTO.setADVAllowed(false);
					} else {
						metaDataDTO.setADVAllowed(true);
					}

					metaDataDTO.setAdvTags(channel.getAdvTags());
					metaDataDTO.setBroadcasterName(channel.getBroadcasterName());
					metaDataDTO.setChannelId(Integer.valueOf(channel.getChannelId()).longValue());
					if (channel.getDefaultChannelNumber() != null) {
						metaDataDTO.setDefaultChannelNumber(channel.getDefaultChannelNumber().longValue());
					}
					metaDataDTO.setDescription(channel.getDescription());
					metaDataDTO.setShortDescription(channel.getDescription());
					if(channel.getExtendedMetadata() !=null && !channel.getExtendedMetadata().trim().isEmpty()){
						if(JsonUtils.isJSONValid(channel.getExtendedMetadata())){
							metaDataDTO.setExtendedMetadata(JsonUtils.parseJson(channel.getExtendedMetadata(), Object.class));
						}
						else{
							metaDataDTO.setExtendedMetadata(channel.getExtendedMetadata());
						}
					}
					metaDataDTO.setExternalId(channel.getExternalId());
					metaDataDTO.setName(channel.getName());
					metaDataDTO.setTitle(channel.getName());
					metaDataDTO.setOrderId(Integer.valueOf(channel.getOrder()).longValue());
					metaDataDTO.setType(channel.getType());

					TechnicalPackagesDTO[] techPackagesDTOs = technicalPackageEntity.getTechPackagesDTOs();
					vodChannel.setTechnicalPackages(techPackagesDTOs);

					PlatformInfoDTO platformInfoDTO = populateChannelPlatform(platformName, platformToChannelPlatformTypeMap);
					if(platformInfoDTO!=null){
					platformInfoDTO.setTrailerUrl(cpe.getTrailerUrl());
					platformInfoDTO.setVideoUrl(cpe.getVideoUrl());
					}
					DcqVodIngestionWriterRequestDTO dcqVodIngestionWriterRequestDTO = new DcqVodIngestionWriterRequestDTO();

					if (cpe.getIsPublished().equals(DcqVodIngestorConstants.SHORT_YES)) {
						vodChannelDTOsList.add(dcqVodIngestionWriterRequestDTO);
					} else {
						
						dcqVodIngestionWriterRequestDTO.setDeleted(true);
						vodChannelDTOsList.add(dcqVodIngestionWriterRequestDTO);
					
					}
					
					vodChannel.setPlatformInfo(platformInfoDTO);

					dcqVodIngestionWriterRequestDTO.setEsIndex(writeAlias);
					dcqVodIngestionWriterRequestDTO.setEsIndexType(DcqVodIngestorConstants.ALL_IN_LOWER);
					
					dcqVodIngestionWriterRequestDTO.setPlatformName(platformName);
					dcqVodIngestionWriterRequestDTO.setEsUniqueId(vodChannelId);
					SuggestDTO suggestDTO = new SuggestDTO();
							List<String> suggestValues = constructSuggestFields(metaDataDTO, config);
							suggestDTO.setInput(suggestValues == null ? null : suggestValues.toArray(new String[0]));
							
					ESVodChannelDTO vodChannelDTO = new ESVodChannelDTO();
					vodChannelDTO.setVodChannel(vodChannel);
					vodChannelDTO.setSuggest(suggestDTO);
					vodChannelDTO.setSearchMetadata(suggestValues == null ? null : suggestValues.toArray(new String[0]));
					dcqVodIngestionWriterRequestDTO.setEsDocObj(vodChannelDTO);
					dcqVodIngestionWriterRequestDTO.setSourceFile(channel);
				}
				
			}

		} catch (Exception e) {
			log.logError(e,"JAXBException in constructMetadata");
		}

		log.logMessage("Returning DcqVodIngestionWriterRequestDTOs");
		return vodChannelDTOsList;
	}
	
	
	public static PlatformInfoDTO populatePlatformInfo(String platformName, Map<String, ChannelPlatformType> platformToChannelPlatformTypeMap) throws JsonParseException, JsonMappingException, IOException {
		ChannelPlatformType channelPlatformType = platformToChannelPlatformTypeMap.get(platformName);
		PlatformInfoDTO platformInfoDto = null;
		if (null != channelPlatformType) {
			platformInfoDto = new PlatformInfoDTO();
			platformInfoDto.setLogoBig(channelPlatformType.getLogoBig());
			platformInfoDto.setLogoMedium(channelPlatformType.getLogoMedium());
			platformInfoDto.setLogoSmall(channelPlatformType.getLogoSmall());
			if(channelPlatformType.getExtendedMetadata() !=null && !channelPlatformType.getExtendedMetadata().trim().isEmpty()){
				if(JsonUtils.isJSONValid(channelPlatformType.getExtendedMetadata())){
					platformInfoDto.setExtendedMetadata(JsonUtils.parseJson(channelPlatformType.getExtendedMetadata(), Object.class));
				}
				else{
					platformInfoDto.setExtendedMetadata(channelPlatformType.getExtendedMetadata());
				}
			}
		}
		return platformInfoDto;
	}
	
	
	public static PlatformInfoDTO populateChannelPlatform(String platformName, Map<String, Platforms> platformToChannelPlatformTypeMap) throws JsonParseException, JsonMappingException, IOException {
		Platforms channelPlatformType = platformToChannelPlatformTypeMap.get(platformName);
		PlatformInfoDTO platformInfoDto = null;
		if (null != channelPlatformType) {
			platformInfoDto = new PlatformInfoDTO();
			platformInfoDto.setLogoBig(channelPlatformType.getLogoBig());
			platformInfoDto.setLogoMedium(channelPlatformType.getLogoMedium());
			platformInfoDto.setLogoSmall(channelPlatformType.getLogoSmall());
			if(channelPlatformType.getExtendedMetadata() !=null && !channelPlatformType.getExtendedMetadata().trim().isEmpty()){
				if(JsonUtils.isJSONValid(channelPlatformType.getExtendedMetadata())){
					platformInfoDto.setExtendedMetadata(JsonUtils.parseJson(channelPlatformType.getExtendedMetadata(), Object.class));
				}
				else{
					platformInfoDto.setExtendedMetadata(channelPlatformType.getExtendedMetadata());
				}
			}
		}
		return platformInfoDto;
	}
	
	private TechnicalPackagesResultsDTO getTechnicalPackagesResultsDTO(Integer channelId) {
		TechnicalPackagesResultsDTO resultsDTO = new TechnicalPackagesResultsDTO();
		List<Object[]> results = null;
		Object blobObject = null;
		if (channelId != null) {
			results = channelRepository.getTechnicalPkgId(channelId);
			blobObject  = channelRepository.getChannelXMlBlob(channelId);
		}

		if (results != null && !results.isEmpty()) {

			TechnicalPackagesDTO[] technicalPackagesDTOs = new TechnicalPackagesDTO[results.size()];
			resultsDTO.setTechPackagesDTOs(technicalPackagesDTOs);
			for (int i = 0; i < results.size(); i++) {
				TechnicalPackagesDTO technicalPackagesDTO = new TechnicalPackagesDTO();
				technicalPackagesDTOs[i] = technicalPackagesDTO;
				Object[] objs = results.get(i);
				if (objs[0] != null) {
					technicalPackagesDTO.setPackageId(Long.parseLong(objs[0].toString()));
				}
				if (objs[1] != null) {
					technicalPackagesDTO.setPackageName(objs[1].toString());
				}
				if (objs[2] != null) {
					technicalPackagesDTO.setPackageType(objs[2].toString());
				}
			}
			
		}

		if(blobObject!=null){
			resultsDTO.setXmlFile((byte[]) blobObject);
		}
		
		return resultsDTO;
	}
	
	
	
	private static Object getObject(byte[] byteArr) throws IOException, ClassNotFoundException {
		String text = new String(byteArr, StandardCharsets.UTF_8);

		return JsonUtils.parseJson(text, com.accenture.avs.be.dcq.vod.ingestor.dto.Channel.class);
	}
	
	private List<String> constructSuggestFields(MetaDataDTO metadataDTO, Configuration config)
			throws IllegalArgumentException, IllegalAccessException, ConfigurationException {
		long sTime = System.currentTimeMillis();
		log.logMessage("Constructing Suggest Fields - Start");
		String suggestFields = config.getConstants().getValue(CacheConstants.DCQ_VOD_INGESTOR_VODCHANNEL_SUGGESTIONS_FIELDS);

		List<String> suggestParams = suggestFields == null ? null : Arrays.asList(suggestFields.split(";"));
		List<String> suggestValues = null;
		if ((null != suggestParams && !suggestParams.isEmpty())) {
			suggestValues = new ArrayList<String>();
			Field[] metadataFields = metadataDTO.getClass().getDeclaredFields();
			for (Field field : metadataFields) {
				field.setAccessible(true);
				Object name = field.get(metadataDTO);
				if (null != suggestParams && !suggestParams.isEmpty() && suggestParams.contains(field.getName())) {
					if (null != String.valueOf(name) && !String.valueOf(name).equals("null")
							&& !String.valueOf(name).equals("")) {
						if (field.getType() == String[].class) {
							Object[] arr = (Object[]) name;
							for (int i = 0; i < arr.length; i++) {
								if (null != arr[i]) {
									suggestValues.add(String.valueOf(arr[i]));
								}
							}
						} else {
							suggestValues.add(String.valueOf(name));
						}
					}
				}
			}
		}

		log.logMessage("Suggest Input Values: {}", suggestValues);
		log.logMethodEnd(System.currentTimeMillis() - sTime);
		return suggestValues;
	}
	
	}

