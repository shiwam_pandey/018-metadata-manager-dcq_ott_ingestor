package com.accenture.avs.be.dcq.vod.ingestor.service;

import com.accenture.avs.be.dcq.vod.ingestor.content.dto.VlcContentPlaylist;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;

public interface VlcContentService {
	GenericResponse updateVlcContent(Integer channelId,String vlcPublishedDate, String csvVlcContent, boolean sync,boolean defaultVlc,String transactionNumber, Configuration config) throws Exception;
	GenericResponse updateVlcContent(VlcContentPlaylist vlcContentPlaylist,Configuration config,String transactionNumber) throws ApplicationException,Exception;
}
