package com.accenture.avs.be.dcq.vod.ingestor.manager;

import java.util.List;

import javax.xml.bind.JAXBException;

import com.accenture.avs.be.dcq.vod.ingestor.dto.content.asset.Asset;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ConfigurationException;


/**
 * @author karthik.vadla
 *
 */
public interface VodManager {	
	
	public Integer ingestContent(Asset asset, String contentXML, Configuration config) throws JAXBException, ConfigurationException, Exception;
	
	public List<Integer> unpublishContent(Integer contentId,Configuration config) throws Exception;
}
