package com.accenture.avs.be.dcq.vod.ingestor.v1.service.impl;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.accenture.avs.be.dcq.vod.ingestor.dto.CommercialPackagesResponseDTO.CommercialPackage;
import com.accenture.avs.be.dcq.vod.ingestor.manager.ContentMetadataManager;
import com.accenture.avs.be.dcq.vod.ingestor.manager.impl.DcqVodIngestorPropertiesManager;
import com.accenture.avs.be.dcq.vod.ingestor.processor.DcqVodIngestionPreProcessor;
import com.accenture.avs.be.dcq.vod.ingestor.utils.CacheConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorRestUrls;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.Validator;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.bundle.asset.BundleDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.bundle.asset.BundleDTO.AddProductInfo;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.bundle.asset.BundleDTO.BundleContents;
import com.accenture.avs.be.dcq.vod.ingestor.v1.manager.BundleV1Manager;
import com.accenture.avs.be.dcq.vod.ingestor.v1.sdp.manager.impl.SDPV1ClientManagerImpl;
import com.accenture.avs.be.dcq.vod.ingestor.v1.service.BundleIngestionV1Service;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.be.framework.exception.ConfigurationException;
import com.accenture.avs.be.framework.utils.Utilities;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.persistence.technicalcatalogue.BundleAggregationEntity;
import com.accenture.avs.persistence.technicalcatalogue.BundleAggregationEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.ContentEntity;
import com.accenture.avs.persistence.technicalcatalogue.ContentPlatformEntity;
import com.accenture.avs.persistence.technicalcatalogue.RelPlatformTechnicalEntity;
import com.accenture.avs.persistence.technicalcatalogue.TechnicalPackageEntity;

/**
 * @author naga.sireesha.meka
 *
 */
@Service("BundleIngestionServicev1Impl")
public class BundleIngestionV1ServiceImpl implements BundleIngestionV1Service {

	@Autowired
	private ResourceLoader resourceLoader;

	@Autowired
	private DcqVodIngestorPropertiesManager dcqVodIngestorPropertiesManager;

	@Autowired
	private BundleV1Manager bundleManager;

	@Autowired
	private ContentMetadataManager contentMetadataManager;
	
	@Autowired
	private DcqVodIngestionPreProcessor dcqVodIngestionPreProcessor;
	
	@Autowired
	private DcqVodIngestorRestUrls dcqVodIngestorRestUrls;

	private static final LoggerWrapper log = new LoggerWrapper(BundleIngestionV1ServiceImpl.class);

	private Boolean isBundle;
	Set<Serializable> bundleAggregationList = null;
	List<Integer> assetIdList = null;
	
	@Override
	public GenericResponse bundleIngestion(BundleDTO bundle,
			Configuration config) throws Exception {
		GenericResponse genericResponse = null;
		try{
			
			log.logMessage("call to initializeChannelIngestor ");
			
			if (Validator.isEmpty(bundle)) {
				log.logMessage("No bundle to write. The execution is completed.");
				genericResponse = new GenericResponse(DcqVodIngestorConstants.ACN_200, DcqVodIngestorConstants.OK,
						DcqVodIngestorConstants.NO_BUNDLE);
				return genericResponse;
			}
			
			// validating the json
			boolean validJson = isJsonValid(bundle);
			
			if(validJson){
				
				log.logMessage("Ingesting bundle in to DB");
				Integer bundleId = ingestBundleToDB(bundle, config);
				
				if(bundleId != null){
					log.logMessage("Publish contents to ES");
					if(!Utilities.isEmpty(assetIdList) && !assetIdList.isEmpty()){
						dcqVodIngestionPreProcessor.refreshCachesCreateIndexes(DcqVodIngestorConstants.CONTENT, "", "",
								config);
						contentMetadataManager.processandWriteContents(assetIdList, DcqVodIngestorUtils.getTransactionNumber(), DcqVodIngestorUtils.currentTime(), "", "", config);
					}
				}
			}
			genericResponse = new GenericResponse(DcqVodIngestorConstants.ACN_200, DcqVodIngestorConstants.OK,
					null);
			}catch (ApplicationException ae) {
				log.logError(ae);
				throw ae;

			} catch (Exception e) {
				log.logError(e);
				throw e;
			}

			return genericResponse;
	}

	private Integer ingestBundleToDB(BundleDTO bundle, Configuration config) throws ApplicationException, ConfigurationException, JsonParseException, JsonMappingException, IOException {
		
		ContentEntity bundleContent = !Utilities.isEmpty(bundle.getBundleExternalContent().getBundleExternalContentId())
				? contentMetadataManager.getContentByExternalId(bundle.getBundleExternalContent().getBundleExternalContentId())
				: contentMetadataManager
						.getContentByContentId(Integer.parseInt(bundle.getBundleExternalContent().getBundleContentId().toString()));
		validateBundleJson(bundle, bundleContent, config);
		// Getting all the content of Bundle and Group of Bundle
		Map<ContentEntity, List<ContentEntity>> contentOfAllBundle = getContentOfAllBundle(bundle);
		// Checking the Parent Content is Bundle or Group of Bundle
		isBundle = isBundle(contentOfAllBundle);
		// Checking for if group of bundle doesn't have proper Bundle content
		if (bundle == null || isBundle == null) {
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters(DcqVodIngestorConstants.INVALID_BUNDLE_CONTENT));
		}		
		List<Object> contentsOfGrpOfBundle = new ArrayList<Object>();
		Set<Integer> contentIds = new HashSet<Integer>();
		contentIds.add(bundleContent.getContentId());
		contentsOfGrpOfBundle = bundleManager.getContentIdsByBundleContentId(bundleContent.getContentId());
		assetIdList = new ArrayList<>();
		
		if (!Utilities.isEmpty(contentsOfGrpOfBundle)) {

			for (Object bundleIdsOfGrpBundle : contentsOfGrpOfBundle) {
				List<Object> bundles = bundleManager.getContentIdsByBundleContentId((Integer)bundleIdsOfGrpBundle);

				if (!Utilities.isEmpty(bundles) && !bundles.isEmpty()) {

					for (Object contentId : bundles) {
						List<Object> bundlecontents = bundleManager.getContentIdsByBundleContentId((Integer)contentId);

						if (bundlecontents.isEmpty()) {
							contentIds.add((Integer)contentId);
						}

					}
				}
				contentIds.add((Integer)bundleIdsOfGrpBundle);
			}

		}
		
		if (bundle.getBundleContents() != null) {
			for (BundleContents tempExternalContentId : bundle.getBundleContents()) {
				// Fix for defect 8803
				Long tempExternalContentIdlong = !Utilities.isEmpty(tempExternalContentId.getExternalContentId()) ? contentMetadataManager.getContentByExternalId(tempExternalContentId.getExternalContentId()).getContentId() : Long.parseLong(tempExternalContentId.getContentId().toString());

				if (!contentIds.toString().contains(!Utilities.isEmpty(tempExternalContentId.getExternalContentId())?tempExternalContentId.getExternalContentId():tempExternalContentId.getContentId().toString())) {
					contentIds.add(tempExternalContentIdlong.intValue());

				}
				List<Object> grpBundle = bundleManager.getContentIdsByBundleContentId(tempExternalContentIdlong.intValue());
				if (!Utilities.isEmpty(grpBundle)) {
					for (Object contentId : grpBundle) {
					contentIds.add((Integer)contentId);
					}
				} else {
					log.logMessage("This is not a bundle in bundle aggregation table..");
				}

			}

		}
		assetIdList.addAll(contentIds);
		// Delete all the details from Bundle Aggregation table.
		bundleManager.deleteFromBundleAggregation(bundleContent.getContentId());
		bundleManager.deleteSVODMapping(bundleContent.getContentId());
		bundleManager.deleteTVODRelPlatform(DcqVodIngestorConstants.PKG_BUNDLE + "_%_" + bundle.getBundleId());
		Set<Integer> svodPackages = bundleManager.getAllSVODPackages(bundleContent);

		// 4.3 change -mapping jaxb to db for blacklist device content table
		bundleManager.getBlacklistDeviceContent(bundle,bundleContent,
				config);

		List<Integer> technicalPackageList = new ArrayList<>();
		TechnicalPackageEntity bundleTechnicalPackage = null;
		// Retrieve Technical Package for Bundle
		if(bundle.getProductInfo() != null && "N".equalsIgnoreCase(config.getConstants().getValue(CacheConstants.SDP_IN_PANIC))) {
		 bundleTechnicalPackage = bundleManager.getTechnicalPackages(bundle, bundleContent,
				isBundle ? DcqVodIngestorConstants.BUNDLE : DcqVodIngestorConstants.BUNDLE_GROUP, config);
		technicalPackageList.add(bundleTechnicalPackage.getPackageId());
		}
		bundleAggregationList = new HashSet<Serializable>();
		technicalPackagesForBundle(config, bundle, contentOfAllBundle, bundleContent, svodPackages,
				technicalPackageList, bundleTechnicalPackage);
		// Start: Insert Relation for Group of Bundle in relPlatformTechnical Table
		Set<ContentEntity> allContents = getAllContentOfGroupOfBundle(contentOfAllBundle);
		technicalPackagesForGroupOfBundle(config, bundle, technicalPackageList, bundleTechnicalPackage,
				 allContents);
		// End: Insert Relation for Group of Bundle in relPlatformTechnical
		// Table Map content for SVOD
		technicalPackagesForContent(bundleContent, svodPackages, allContents);
		// Map content for SVOD
		// 4.3 changes
		if (!technicalPackageList.isEmpty() && "N".equalsIgnoreCase(config.getConstants().getValue(CacheConstants.SDP_IN_PANIC))) {
			String externalId = bundle.getBundleId();
			if (bundle.getProductInfo() != null && bundle.getProductInfo().getAdditionalProductInfo() != null) {
				for (AddProductInfo addProductInfo : bundle.getProductInfo().getAdditionalProductInfo().getAddProductInfo()) {
					createSolutionOfferrOnSDP(bundle, technicalPackageList, bundleContent,
							(addProductInfo.getCommerceModel() != null
									&& !addProductInfo.getCommerceModel().trim().isEmpty()
											? externalId + "_" + addProductInfo.getCommerceModel() : externalId),
							addProductInfo.getCommerceModel(),
							addProductInfo.getPriceCategories().get(0).getPriceCategoryId(), config);
				}
			} else {
				createSolutionOfferrOnSDP(bundle, technicalPackageList, bundleContent, externalId, null,
						bundle.getProductInfo().getPriceCategory(), config);
			}
		}
		bundleManager.createBundle(bundleAggregationList);
		return bundleContent.getContentId();
	}
	private void createSolutionOfferrOnSDP(BundleDTO bundle, List<Integer> technicalPackageList,
			ContentEntity bundleContent, String externalId, String commerceModel, String priceCategoryId,
			Configuration config) throws ConfigurationException, ApplicationException, JsonParseException, JsonMappingException, IOException{
		log.logMessage("In createSolutionOfferrOnSDP");
		Long solutionOfferId = null;

		List<Long> technicalPackages = new ArrayList<>();
		for (Integer technicalPackage : technicalPackageList) {
			technicalPackages.add(technicalPackage.longValue());
		}
		CommercialPackage sdpSolutionOffer = null;
		String socketTimeOut = config.getConstants().getValue(CacheConstants.HTTP_CLIENT_SOCKET_TIMEOUT);
		String connectiontimeOut = config.getConstants().getValue(CacheConstants.HTTP_CLIENT_CONNECTION_TIMEOUT);
		String sdpTenant = config.getConstants().getValue(CacheConstants.SDP_TENANT);
		String sdpUrlConnection = dcqVodIngestorRestUrls.SDP_URL;
		SDPV1ClientManagerImpl sdpClientService = SDPV1ClientManagerImpl
				.getSdpClientService(Long.parseLong(connectiontimeOut), Long.parseLong(socketTimeOut));
		sdpSolutionOffer = sdpClientService.getSdpSolutionOffer(externalId, dcqVodIngestorRestUrls.COMMERCIAL_PACKAGE_DETAILS_V2_URL, sdpTenant,config);
		if (sdpSolutionOffer == null) {
			log.logMessage("Going to Create Solution Offer ");
			solutionOfferId = sdpClientService.createSolutionOfferAndPackage(bundle, technicalPackages, bundleContent,
					sdpUrlConnection,sdpTenant, isBundle, externalId, commerceModel, priceCategoryId);
			if (solutionOfferId == null) {
				log.logMessage("Creation of SDP Solution Offer failed");
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters("Creation of SDP Solution Offer failed"));
			}
		} else {
			solutionOfferId = sdpSolutionOffer.getCommercialPackageId().longValue();
			log.logMessage("Going to Update Solution Offer");
			if (sdpSolutionOffer.getStatusId() == 4) {
				sdpClientService.changeStatusSolutionOfferOnSDP(solutionOfferId, "Active", sdpUrlConnection,sdpTenant);
			}
			boolean offerIdFlag = sdpClientService.updateSolutionOfferAndPackage(bundle, technicalPackages,
					bundleContent, solutionOfferId,sdpUrlConnection, sdpTenant, isBundle, externalId, commerceModel, priceCategoryId);
			if (!offerIdFlag) {
				log.logMessage(
						"Updation of SDP Solution Offer failed for: " + sdpSolutionOffer.getCommercialPackageId());
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters("Updation of SDP Solution Offer failed"));
			}
			
		}

	}

	private void technicalPackagesForContent(ContentEntity bundleContent, Set<Integer> svodPackages,
			Set<ContentEntity> allContents) {
		RelPlatformTechnicalEntity relPlatformTechnical;
		for (ContentEntity innerBundleContent : allContents) {
			for (ContentPlatformEntity contentPlatform : innerBundleContent.getContentPlatforms()) {
				if (DcqVodIngestorConstants.YES.equals(Character.toString(contentPlatform.getIsPublished()))) {
					log.logMessage("Bundle Aggregation Processing for Rel Table Started For Group Of Bundle");
					// Map Real Content with Fictitius Content Package Id :Start
					for (Integer packageId : svodPackages) {
						relPlatformTechnical = bundleManager.getRelPlatformTechnical(innerBundleContent,
								contentPlatform.getCpId(), packageId, bundleContent.getContentId());
						bundleAggregationList.add(relPlatformTechnical);
					}
					// Map Real Content with Fictitius Content Package Id :End
				}
			}
		}
		
	}

	private void technicalPackagesForGroupOfBundle(Configuration config, BundleDTO bundle,
			List<Integer> technicalPackageList, TechnicalPackageEntity bundleTechnicalPackage,
			Set<ContentEntity> allContents) throws ConfigurationException {
		RelPlatformTechnicalEntity relPlatformTechnical;
		TechnicalPackageEntity contentTechnicalPackage;
		if (!isBundle && bundle.getProductInfo() != null) {
			for (ContentEntity innerBundleContent : allContents) {
				for (ContentPlatformEntity contentPlatform : innerBundleContent.getContentPlatforms()) {
					if (DcqVodIngestorConstants.YES.equals(Character.toString(contentPlatform.getIsPublished()))) {
						log.logMessage("Bundle Aggregation Processing for Rel Table Started For Group Of Bundle");
						// Map Real Content with Fictitius Content Package Id
						// :Start
						if (bundleTechnicalPackage != null) {
							relPlatformTechnical = bundleManager.getRelPlatformTechnical(innerBundleContent,
									contentPlatform.getCpId(), bundleTechnicalPackage.getPackageId(), null);
							bundleAggregationList.add(relPlatformTechnical);
						}
						// Map Real Content with Fictitius Content Package Id
						// :End
					}
				}
				if (bundle.getProductInfo() != null && "N".equalsIgnoreCase(config.getConstants().getValue(CacheConstants.SDP_IN_PANIC))) {
					//Technical Package for Contents under Bundle(Which is Part of Group of Bundle)
					contentTechnicalPackage = bundleManager.getTechnicalPackages(bundle, innerBundleContent,
							DcqVodIngestorConstants.BUNDLE_VOD, config);
					technicalPackageList.add(contentTechnicalPackage.getPackageId());
				}
			}
		}
		
	}

	private Set<ContentEntity> getAllContentOfGroupOfBundle(
			Map<ContentEntity, List<ContentEntity>> contentOfAllBundle) {
		Set<ContentEntity> contents = new LinkedHashSet<>();
		for (Map.Entry<ContentEntity, List<ContentEntity>> entry : contentOfAllBundle.entrySet()) {
			contents.addAll(entry.getValue());
		}
		return contents;
	}

	private void technicalPackagesForBundle(Configuration config, BundleDTO bundle,
			Map<ContentEntity, List<ContentEntity>> contentOfAllBundle, ContentEntity bundleContent,
			Set<Integer> svodPackages, List<Integer> technicalPackageList,
			TechnicalPackageEntity bundleTechnicalPackage) throws ConfigurationException {
		BundleAggregationEntity bundleAggregation;
		RelPlatformTechnicalEntity relPlatformTechnical;
		TechnicalPackageEntity technicalPackage;
		for (Map.Entry<ContentEntity, List<ContentEntity>> entry : contentOfAllBundle.entrySet()) {
			log.logMessage("Bundle Aggregation Processing Started");
			ContentEntity content = entry.getKey();
			BundleContents externalContent = getBundleContentExternalContent(content, bundle);
			for (ContentPlatformEntity contentPlatform : content.getContentPlatforms()) {
				if (DcqVodIngestorConstants.YES.equals(Character.toString(contentPlatform.getIsPublished()))) {
					bundleAggregation = getBundleAggregation(bundleContent, content, externalContent, contentPlatform,
							bundleTechnicalPackage != null ? bundleTechnicalPackage : null);
					bundleAggregationList.add(bundleAggregation);

					if (bundle.getProductInfo() != null
							&& "N".equalsIgnoreCase(config.getConstants().getValue(CacheConstants.SDP_IN_PANIC))) {
						// Map Real Content with Fictitius Content Package Id
						// :Start
						relPlatformTechnical = bundleManager.getRelPlatformTechnical(content, contentPlatform.getCpId(),
								bundleTechnicalPackage.getPackageId(), null);
						bundleAggregationList.add(relPlatformTechnical);
						// Map Real Content with Fictitius Content Package Id
						// :End\
					}
					// Map content for SVOD
					for (Integer packageId : svodPackages) {
						relPlatformTechnical = bundleManager.getRelPlatformTechnical(content, contentPlatform.getCpId(),
								packageId, bundleContent.getContentId());
						bundleAggregationList.add(relPlatformTechnical);
					}
					// Map content for SVOD
				}
			}
			if (bundle.getProductInfo() != null
					&& "N".equalsIgnoreCase(config.getConstants().getValue(CacheConstants.SDP_IN_PANIC))) {
				log.logMessage("Bundle Aggregation Processing for Packages Started");
				// Start: Technical Package for Contents under Bundle
				technicalPackage = bundleManager.getTechnicalPackages(bundle, content,
						isBundle ? DcqVodIngestorConstants.BUNDLE_VOD : DcqVodIngestorConstants.BUNDLE, config);
				technicalPackageList.add(technicalPackage.getPackageId());
				// offerList.add(technicalPackage.getExternalID());
				// End: Technical Package for Contents under Bundle
			}
		}
		
	}

	private BundleAggregationEntity getBundleAggregation(ContentEntity bundleContent, ContentEntity content,
			BundleContents externalContentId, ContentPlatformEntity contentPlatform, TechnicalPackageEntity technicalPackage) {
		BundleAggregationEntity bundleAggregation = new BundleAggregationEntity();
		bundleAggregation.setContent(content);
		BundleAggregationEntityPK bundlePlatformPk = new BundleAggregationEntityPK();
		bundlePlatformPk.setBundleContentId(bundleContent.getContentId());
		bundlePlatformPk.setCpId(contentPlatform.getCpId());
		bundleAggregation.setId(bundlePlatformPk);
		bundleAggregation.setPosition(externalContentId.getPosition());
		bundleAggregation.setTechnicalPackage(technicalPackage);
		return bundleAggregation;
	}

	private BundleContents getBundleContentExternalContent(ContentEntity content, BundleDTO bundle) {
		BundleContents externalContent = null;
		for (BundleContents tempExternalContentId : bundle.getBundleContents()) {
			if (content.getExternalContentId() != null && content.getContentId() != null
					&& ((Utilities.isEmpty(tempExternalContentId.getContentId())
							&& content.getExternalContentId().equals(tempExternalContentId.getExternalContentId()))
							|| (!Utilities.isEmpty(tempExternalContentId.getContentId()) && content
									.getContentId().equals(tempExternalContentId.getContentId())))) {
				externalContent = new BundleContents();
				externalContent =tempExternalContentId;
				break;
			}
		}
		return externalContent;
	}

	private Boolean isBundle(Map<ContentEntity, List<ContentEntity>> contentOfAllBundle) {
		Boolean tempFlag = null;
		Boolean flag = null;
		for (Map.Entry<ContentEntity, List<ContentEntity>> entry : contentOfAllBundle.entrySet()) {
			tempFlag = !(entry.getValue() != null && (entry.getValue().size() > 0));
			if (flag == null) {
				flag = tempFlag;
			}
			if (!tempFlag.equals(flag)) {
				flag = null;
				break;
			}
		}
		return tempFlag;
	}

	private Map<ContentEntity, List<ContentEntity>> getContentOfAllBundle(BundleDTO bundle) {
		ContentEntity content = null;
		List<Object> tempContentIds = null;
		Set<Object> contentIds = null;
		Map<ContentEntity, List<ContentEntity>> contentMap = new LinkedHashMap<ContentEntity, List<ContentEntity>>();
		List<ContentEntity> contents = null;
		assetIdList = new ArrayList<>();
		//boolean contentTypeFlag = Boolean.valueOf(bundle.getBundleContentList().getIsExternalId().value());
		for (BundleContents tempExternalContentId : bundle.getBundleContents()) {
			content =  !Utilities.isEmpty(bundle.getBundleExternalContent().getBundleExternalContentId()) ? contentMetadataManager.getContentByExternalId(tempExternalContentId.getExternalContentId())
					: contentMetadataManager.getContentByContentId(Integer.parseInt(tempExternalContentId.getContentId().toString()));

			if (content != null) {
				tempContentIds = bundleManager.getContentIdsByBundleContentId(content.getContentId());
				contentIds = new HashSet<Object>(tempContentIds);
				contents = new ArrayList<ContentEntity>();
				for (Object contentId : contentIds) {
					contents.add(contentMetadataManager.getContentByContentId((Integer) contentId));
					assetIdList.add((Integer)contentId);
				}
				contentMap.put(content, contents);
			}
		}

		return contentMap;
	}

	private  void validateBundleJson(BundleDTO bundle, ContentEntity bundleContent, Configuration config) throws ApplicationException {
		// TODO Auto-generated method stub
		if(bundleContent == null){
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters("Bundle does not exist"));
		}
		// Validate if the contents in xml are present in the DB, if present return
		// true, else return false
		boolean contentCheckFlag = true;
		String contentName =null; 
		for (BundleContents bundleContents : bundle.getBundleContents()) {
			contentCheckFlag = bundleContent != null;
			if (!contentCheckFlag) {
				contentName = !Utilities.isEmpty(bundle.getBundleExternalContent().getBundleExternalContentId())?bundleContents.getExternalContentId():bundleContents.getContentId().toString();
				break;
			}
		}
		if(!contentCheckFlag){
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters(contentName + " Content of Bundle does not exist"));
		}
		// Set<String> contentDeviceTypeListFromDB = getContentblacklistDevices(bundle);
		// List<String> bundleDeviceTypeListFromXML= new ArrayList<String>();
		if (bundle.getProductInfo() != null) {
			if (bundle.getBundleId() == null) {
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
						new NestedParameters("Bundle Id and Product Info both can be null or it should have value"));
			}
		}
	}

	private boolean isJsonValid(BundleDTO bundle) throws ApplicationException {
		boolean jsonvalidFlag;
		try{
			
			if(bundle.getBundleExternalContent().getBundleContentId() == null && bundle.getBundleExternalContent().getBundleExternalContentId() == null){
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER, new NestedParameters("BundleExternalContent"));				
			} else {
				Validator.sanitizingParamsForSpecialParams(DcqVodIngestorConstants.BUNDLE_EXTERNAL_CONTENT_ID, bundle.getBundleExternalContent().getBundleExternalContentId());
			}
			if(CollectionUtils.isEmpty(bundle.getBundleContents())&&bundle.getBundleContents().size() == 0){
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER, new NestedParameters("BundleContents"));
			}
			if (bundle.getProductInfo() != null){
				if(Utilities.isEmpty(bundle.getProductInfo().getRentalMode())){
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER, new NestedParameters("RentalMode"));	
				} else {
					Validator.sanitizingParamsForSpecialParams(DcqVodIngestorConstants.RENTAL_MODE, bundle.getProductInfo().getRentalMode());
				}
				if(Utilities.isEmpty(bundle.getProductInfo().getPriceCategory())){
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER, new NestedParameters("PriceCategory"));	
				} else {
					Validator.sanitizingParamsForSpecialParams(DcqVodIngestorConstants.PRICE_CATEGORY, bundle.getProductInfo().getPriceCategory());
				}
				if(bundle.getProductInfo().getPurchaseStartDateTime() == null
					&& bundle.getProductInfo().getPurchaseEndDateTime() != null) {
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
						new NestedParameters(DcqVodIngestorConstants.INVALID_PURCHASE_END_DATE));
				}
			}
		   
		   jsonvalidFlag = true;
		}catch(ApplicationException ae){
			throw ae;
		}catch (Exception e) {
			log.logError(e);
			throw e;
		}
		
		return jsonvalidFlag;
	}	
}
