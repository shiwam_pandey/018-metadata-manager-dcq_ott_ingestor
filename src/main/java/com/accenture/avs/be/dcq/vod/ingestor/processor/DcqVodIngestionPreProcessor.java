package com.accenture.avs.be.dcq.vod.ingestor.processor;

import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;

/**
 * @author karthik.vadla
 *
 */
public interface DcqVodIngestionPreProcessor {
	void refreshCachesCreateIndexes(String ingestionType,String mode, String indexDate, Configuration config) throws ApplicationException;
}
