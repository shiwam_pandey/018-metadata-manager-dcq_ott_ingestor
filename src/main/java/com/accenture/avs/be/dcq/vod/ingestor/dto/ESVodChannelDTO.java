package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.io.Serializable;
import java.util.Map;

/**
 * @author phani.gottumukkala
 *
 */
public class ESVodChannelDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private VodChannel vodChannel;
	private SuggestDTO suggest;
	private String[] searchMetadata;

	public String[] getSearchMetadata() {
		return searchMetadata;
	}

	public void setSearchMetadata(String[] searchMetadata) {
		this.searchMetadata = searchMetadata;
	}

	public VodChannel getVodChannel() {
		return vodChannel;
	}

	public void setVodChannel(VodChannel vodChannel) {
		this.vodChannel = vodChannel;
	}

	public SuggestDTO getSuggest() {
		return suggest;
	}

	public void setSuggest(SuggestDTO suggest) {
		this.suggest = suggest;
	}

	public static class SuggestDTO implements Serializable {

		private static final long serialVersionUID = 1L;

		private String[] input;
		private Map<String, String> contexts;

		public String[] getInput() {
			return input;
		}

		public void setInput(String[] input) {
			this.input = input;
		}

		public Map<String, String> getContexts() {
			return contexts;
		}

		public void setContexts(Map<String, String> contexts) {
			this.contexts = contexts;
		}

	}

	public static class VodChannel implements Serializable {

		private static final long serialVersionUID = 1L;

		private int channelId;
		private MetaDataDTO metadata;
		private TechnicalPackagesDTO[] technicalPackages;
		private PlatformInfoDTO platformInfo;
		private String platformName;

		public String getPlatformName() {
			return platformName;
		}

		public void setPlatformName(String platformName) {
			this.platformName = platformName;
		}

		public int getChannelId() {
			return channelId;
		}

		public void setChannelId(int channelId) {
			this.channelId = channelId;
		}

		public MetaDataDTO getMetadata() {
			return metadata;
		}

		public void setMetadata(MetaDataDTO metadata) {
			this.metadata = metadata;
		}

		public TechnicalPackagesDTO[] getTechnicalPackages() {
			return technicalPackages;
		}

		public void setTechnicalPackages(TechnicalPackagesDTO[] technicalPackages) {
			this.technicalPackages = technicalPackages;
		}

		public PlatformInfoDTO getPlatformInfo() {
			return platformInfo;
		}

		public void setPlatformInfo(PlatformInfoDTO platformInfo) {
			this.platformInfo = platformInfo;
		}

	}

	public static class MetaDataDTO implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private Long channelId;
		private String name;
		private String title;
		private String type;
		private String description;
		private String shortDescription;
		private Long orderId;
		private String externalId;
		private String advTags;
		private Long defaultChannelNumber;
		private String broadcasterName;
		private boolean isActive;
		private boolean isADVAllowed;
		private boolean isAdult;
		private Object extendedMetadata;

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getShortDescription() {
			return shortDescription;
		}

		public void setShortDescription(String shortDescription) {
			this.shortDescription = shortDescription;
		}

		public Long getChannelId() {
			return channelId;
		}

		public void setChannelId(Long channelId) {
			this.channelId = channelId;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public Long getOrderId() {
			return orderId;
		}

		public void setOrderId(Long orderId) {
			this.orderId = orderId;
		}

		public String getExternalId() {
			return externalId;
		}

		public void setExternalId(String externalId) {
			this.externalId = externalId;
		}

		public String getAdvTags() {
			return advTags;
		}

		public void setAdvTags(String advTags) {
			this.advTags = advTags;
		}

		public Long getDefaultChannelNumber() {
			return defaultChannelNumber;
		}

		public void setDefaultChannelNumber(Long defaultChannelNumber) {
			this.defaultChannelNumber = defaultChannelNumber;
		}

		public String getBroadcasterName() {
			return broadcasterName;
		}

		public void setBroadcasterName(String broadcasterName) {
			this.broadcasterName = broadcasterName;
		}

		public boolean getIsActive() {
			return isActive;
		}

		public void setActive(boolean isActive) {
			this.isActive = isActive;
		}

		public boolean getIsADVAllowed() {
			return isADVAllowed;
		}

		public void setADVAllowed(boolean isADVAllowed) {
			this.isADVAllowed = isADVAllowed;
		}

		public boolean getIsAdult() {
			return isAdult;
		}

		public void setAdult(boolean isAdult) {
			this.isAdult = isAdult;
		}

		public Object getExtendedMetadata() {
			return extendedMetadata;
		}

		public void setExtendedMetadata(Object extendedMetadata) {
			this.extendedMetadata = extendedMetadata;
		}

	}

	public static class TechnicalPackagesDTO implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private Long packageId;
		private String packageName;
		private String packageType;

		public Long getPackageId() {
			return packageId;
		}

		public void setPackageId(Long packageId) {
			this.packageId = packageId;
		}

		public String getPackageName() {
			return packageName;
		}

		public void setPackageName(String packageName) {
			this.packageName = packageName;
		}

		public String getPackageType() {
			return packageType;
		}

		public void setPackageType(String packageType) {
			this.packageType = packageType;
		}

	}

	public static class PlatformInfoDTO implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		private String logoBig;
		private String logoMedium;
		private String logoSmall;
		private Object extendedMetadata;
		private String trailerUrl;
		private String videoUrl;

		public String getTrailerUrl() {
			return trailerUrl;
		}

		public void setTrailerUrl(String trailerUrl) {
			this.trailerUrl = trailerUrl;
		}

		public String getVideoUrl() {
			return videoUrl;
		}

		public void setVideoUrl(String videoUrl) {
			this.videoUrl = videoUrl;
		}

		public String getLogoBig() {
			return logoBig;
		}

		public void setLogoBig(String logoBig) {
			this.logoBig = logoBig;
		}

		public String getLogoMedium() {
			return logoMedium;
		}

		public void setLogoMedium(String logoMedium) {
			this.logoMedium = logoMedium;
		}

		public String getLogoSmall() {
			return logoSmall;
		}

		public void setLogoSmall(String logoSmall) {
			this.logoSmall = logoSmall;
		}

		public Object getExtendedMetadata() {
			return extendedMetadata;
		}

		public void setExtendedMetadata(Object extendedMetadata) {
			this.extendedMetadata = extendedMetadata;
		}

	}
}
