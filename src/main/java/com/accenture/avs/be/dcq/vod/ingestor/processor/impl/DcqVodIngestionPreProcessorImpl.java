package com.accenture.avs.be.dcq.vod.ingestor.processor.impl;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.cache.LanguageCache;
import com.accenture.avs.be.dcq.vod.ingestor.manager.impl.DcqVodIngestorPropertiesManager;
import com.accenture.avs.be.dcq.vod.ingestor.processor.DcqVodIngestionPreProcessor;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ConfigurationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.es.manager.ElasticSearchAdminClientManager;
import com.accenture.avs.es.responses.GetAliasResponse;

/**
 * @author karthik.vadla
 *
 */
@Component
public class DcqVodIngestionPreProcessorImpl implements DcqVodIngestionPreProcessor{
	private static LoggerWrapper log = new LoggerWrapper(DcqVodIngestionPreProcessorImpl.class);
	
	@Autowired
private	DcqVodIngestorPropertiesManager dcqVodIngestorPropertiesManager;
	
	@Autowired
	private LanguageCache languageCache;
	
	
	@Override
	public void refreshCachesCreateIndexes(String ingestionType, String mode, String indexDate, Configuration config)
			throws ApplicationException {
		
		try {

			Long sTimeCheck = null;
			String isAliasEnabled;
			byte[]  dcqIngestorUser = DcqVodIngestorUtils.getESDcqIngestorUser();
			byte[] dcqIngestorPassword = DcqVodIngestorUtils.getESDcqIngestorPassword();
			ElasticSearchAdminClientManager esAdminCientManager = DcqVodIngestorUtils.getElasticSearchAdminClientManagerImpl(config);
			
			if(mode.equalsIgnoreCase(DcqVodIngestorConstants.REINDEX)){
				 isAliasEnabled = DcqVodIngestorConstants.SHORT_NO;
			}else{
				 isAliasEnabled = DcqVodIngestorConstants.SHORT_YES;
					}
			dcqVodIngestorPropertiesManager.loadEsMappingFiles();
			dcqVodIngestorPropertiesManager.loadEsSettingFiles();
 			if (ingestionType.equals(DcqVodIngestorConstants.CONTENT)) {
				long sTime = System.currentTimeMillis();
				log.logMessage("Refreshing Caches - Start.");
				Map<String, String> languagesCache = languageCache.getLanguages();
				log.logMessage("Refreshing Caches - Completed,took: {}  ms" , (System.currentTimeMillis() - sTime) );
				sTimeCheck = System.currentTimeMillis();
				log.logMessage("Check and Create Indexes if not already present - Start.");
				for (String langCode : languagesCache.keySet()) {
					String prefix = DcqVodIngestorConstants.PREFIX_VALUE;
					
					String indexName = prefix +"_" + DcqVodIngestorConstants.CONTENT_IN_LOWER + "_" + langCode.toLowerCase();
					String writeAlias = DcqVodIngestorUtils.getWriteAlias(DcqVodIngestorConstants.CONTENT_IN_LOWER + "_" + langCode.toLowerCase());
					
					if(mode.equalsIgnoreCase(DcqVodIngestorConstants.REINDEX)){
						indexName = indexName+DcqVodIngestorConstants.UNDERSCORE+indexDate;
						GetAliasResponse aliasResponse = DcqVodIngestorUtils.getIndexByAliasName( DcqVodIngestorUtils.getWriteAlias(DcqVodIngestorConstants.CONTENT_IN_LOWER + "_" + langCode.toLowerCase()), config);
						if(!aliasResponse.getAliases().isEmpty()){
							 String existingIndexName = aliasResponse.getIndexName();
							 
							 //removing write alias
							 DcqVodIngestorUtils.removeAlias(config, existingIndexName, writeAlias);
							}
						
					}else{
						indexName = getIndexByAliasName(config,indexName, DcqVodIngestorConstants.CONTENT_IN_LOWER + "_" + langCode.toLowerCase());
					}
					if (!esAdminCientManager.isIndexExists(indexName,dcqIngestorUser,dcqIngestorPassword)) {

						
						
						String contentSettings = dcqVodIngestorPropertiesManager.getEsSettings(DcqVodIngestorConstants.CONTENT_IN_LOWER+"_"+langCode.toLowerCase());
						if (StringUtils.isBlank(contentSettings)) {
							log.logMessage("Settings not available for {}.Loading default configurations",
									DcqVodIngestorConstants.CONTENT_IN_LOWER + "_" + langCode.toLowerCase());
							contentSettings = dcqVodIngestorPropertiesManager.getEsSettings(DcqVodIngestorConstants.CONTENT_IN_LOWER+"_"+"eng");
						}
								log.logMessage("Content Settings file read {}" ,contentSettings );
						if(DcqVodIngestorUtils.isEmpty(contentSettings)){
						esAdminCientManager.createIndex(indexName,isAliasEnabled,dcqIngestorUser,dcqIngestorPassword);
						}
						else{
						esAdminCientManager.createIndexWithSettings(indexName,dcqIngestorUser,dcqIngestorPassword, contentSettings,isAliasEnabled);
						log.logMessage("Created index for Content with settings - Completed" );
						}
					}
					//Start Changes for Jira Story - AVS-9826
					String indexType = DcqVodIngestorConstants.ALL_IN_LOWER;
					if (!esAdminCientManager.isTypeExistsInIndex(indexName, indexType,dcqIngestorUser,dcqIngestorPassword)) {
						
						
						String contentMapping = dcqVodIngestorPropertiesManager.getEsMapping(DcqVodIngestorConstants.CONTENT_IN_LOWER+"_"+langCode.toLowerCase());
						if (StringUtils.isBlank(contentMapping)) {
							log.logMessage("Mapping not available for {}.Loading default configurations",
									DcqVodIngestorConstants.CONTENT_IN_LOWER + "_" + langCode.toLowerCase());
							contentMapping = dcqVodIngestorPropertiesManager.getEsMapping(DcqVodIngestorConstants.CONTENT_IN_LOWER+"_"+"eng");
						}
						
						
						
						esAdminCientManager.createTypeWithMapping(indexName, indexType,dcqIngestorUser,dcqIngestorPassword,
								contentMapping,isAliasEnabled);
						//creating write alias
						DcqVodIngestorUtils.createAlias(config, indexName, writeAlias);
						
					}
					//End Changes for Jira Story - AVS-9826					
				}
			} else if (ingestionType.equals(DcqVodIngestorConstants.CATEGORY)) {
				long sTime = System.currentTimeMillis();
				log.logMessage("Refreshing Caches - Start.");
				Map<String, String> languagesCache = languageCache.getLanguages();
				log.logMessage("Refreshing Caches - Completed,took: {} ms" , (System.currentTimeMillis() - sTime) );
				sTimeCheck = System.currentTimeMillis();
				log.logMessage("Check and Create Indexes if not already present - Start.");
				for (String langCode : languagesCache.keySet()) {
					String prefix = DcqVodIngestorConstants.PREFIX_VALUE;
					
					String indexName = prefix +"_" +DcqVodIngestorConstants.CATEGORY_IN_LOWER + "_" + langCode.toLowerCase();
					String writeAlias = DcqVodIngestorUtils.getWriteAlias(DcqVodIngestorConstants.CATEGORY_IN_LOWER + "_" + langCode.toLowerCase());
					
					if(mode.equalsIgnoreCase(DcqVodIngestorConstants.REINDEX)){
						indexName = indexName+DcqVodIngestorConstants.UNDERSCORE+indexDate;
						GetAliasResponse aliasResponse = DcqVodIngestorUtils.getIndexByAliasName( DcqVodIngestorUtils.getWriteAlias(DcqVodIngestorConstants.CATEGORY_IN_LOWER + "_" + langCode.toLowerCase()), config);
						if(!aliasResponse.getAliases().isEmpty()){
							 String existingIndexName = aliasResponse.getIndexName();
							 
							 //removing write alias
							 DcqVodIngestorUtils.removeAlias(config, existingIndexName, writeAlias);
							}
					}else{
						indexName = getIndexByAliasName(config, indexName, DcqVodIngestorConstants.CATEGORY_IN_LOWER + "_" + langCode.toLowerCase());
					}
					if (!esAdminCientManager.isIndexExists(indexName,dcqIngestorUser,dcqIngestorPassword)) {
						
					
						String categoySettings = dcqVodIngestorPropertiesManager.getEsSettings(DcqVodIngestorConstants.CATEGORY_IN_LOWER+ "_" + langCode.toLowerCase() );
						if (StringUtils.isBlank(categoySettings)) {
							log.logMessage("Settings not available for {}.Loading default configurations",
									DcqVodIngestorConstants.CATEGORY_IN_LOWER + "_" + langCode.toLowerCase());
							categoySettings = dcqVodIngestorPropertiesManager.getEsSettings(DcqVodIngestorConstants.CATEGORY_IN_LOWER+"_"+"eng");
						}
						log.logMessage("Category Settings file read {}" ,categoySettings );
						if(DcqVodIngestorUtils.isEmpty(categoySettings)){
						esAdminCientManager.createIndex(indexName,isAliasEnabled,dcqIngestorUser,dcqIngestorPassword);
						}
						else{
						esAdminCientManager.createIndexWithSettings(indexName,dcqIngestorUser,dcqIngestorPassword, categoySettings,isAliasEnabled);
						log.logMessage("Created index for Category with settings - Completed" );
						}
					}
					String indexType = DcqVodIngestorConstants.ALL_IN_LOWER;
					if (!esAdminCientManager.isTypeExistsInIndex(indexName, indexType,dcqIngestorUser,dcqIngestorPassword)) {
						
						
						String categoryMapping = dcqVodIngestorPropertiesManager.getEsMapping(DcqVodIngestorConstants.CATEGORY_IN_LOWER+"_"+langCode.toLowerCase());
						if (StringUtils.isBlank(categoryMapping)) {
							log.logMessage("Mapping not available for {}.Loading default configurations",
									DcqVodIngestorConstants.CATEGORY_IN_LOWER + "_" + langCode.toLowerCase());
							categoryMapping = dcqVodIngestorPropertiesManager.getEsMapping(DcqVodIngestorConstants.CATEGORY_IN_LOWER+"_"+"eng");
						}
						
						
						
						esAdminCientManager.createTypeWithMapping(indexName, indexType,dcqIngestorUser,dcqIngestorPassword,
								categoryMapping,isAliasEnabled);
					
						//creating write alias
						DcqVodIngestorUtils.createAlias(config, indexName, writeAlias);
					
					}
				}
			} else if (ingestionType.equals(DcqVodIngestorConstants.VODCHANNEL)) {
				sTimeCheck = System.currentTimeMillis();
				String prefix = DcqVodIngestorConstants.PREFIX_VALUE;
				String indexName = prefix +"_" + DcqVodIngestorConstants.VODCHANNEL_IN_LOWER;
				String writeAlias = DcqVodIngestorUtils.getWriteAlias(DcqVodIngestorConstants.VODCHANNEL_IN_LOWER);
				if(mode.equalsIgnoreCase(DcqVodIngestorConstants.REINDEX)){
					indexName = indexName+DcqVodIngestorConstants.UNDERSCORE+indexDate;
					GetAliasResponse aliasResponse = DcqVodIngestorUtils.getIndexByAliasName( DcqVodIngestorUtils.getWriteAlias(DcqVodIngestorConstants.VODCHANNEL_IN_LOWER), config);
					if(!aliasResponse.getAliases().isEmpty()){
						 String existingIndexName = aliasResponse.getIndexName();
						 
						 //removing write alias
						 DcqVodIngestorUtils.removeAlias(config, existingIndexName, writeAlias);
						}
				}else{
					indexName = getIndexByAliasName(config, indexName, DcqVodIngestorConstants.VODCHANNEL_IN_LOWER);
				}
				if (!esAdminCientManager.isIndexExists(indexName,dcqIngestorUser,dcqIngestorPassword)) {
					
					
					String vodChannelSettings = dcqVodIngestorPropertiesManager.getEsSettings(DcqVodIngestorConstants.VODCHANNEL_IN_LOWER);
					log.logMessage("VOD Channel Settings file read {}" ,vodChannelSettings );
					if(DcqVodIngestorUtils.isEmpty(vodChannelSettings)){
					esAdminCientManager.createIndex(indexName,isAliasEnabled,dcqIngestorUser,dcqIngestorPassword);
					}
					else{
					esAdminCientManager.createIndexWithSettings(indexName,dcqIngestorUser,dcqIngestorPassword, vodChannelSettings,isAliasEnabled);
					log.logMessage("Created index for VOD Channel with settings - Completed" );
					}
				}

				//Start - changes for Jira Story - AVS-9826
				if (!esAdminCientManager.isTypeExistsInIndex(indexName, DcqVodIngestorConstants.ALL_IN_LOWER,dcqIngestorUser,dcqIngestorPassword)) {
					esAdminCientManager.createTypeWithMapping(indexName, DcqVodIngestorConstants.ALL_IN_LOWER,dcqIngestorUser,dcqIngestorPassword,
							dcqVodIngestorPropertiesManager.getEsMapping(DcqVodIngestorConstants.VODCHANNEL_IN_LOWER),isAliasEnabled);
				
					//creating write alias
					DcqVodIngestorUtils.createAlias(config, indexName, writeAlias);
				
				}
				//End - changes for Jira Story - AVS-9826
				
			}else if(ingestionType.equals(DcqVodIngestorConstants.VLCCONTENT)){
				long sTime = System.currentTimeMillis();
				log.logMessage("Refreshing Caches - Start.");
				Map<String, String> languagesCache = languageCache.getLanguages();
				log.logMessage("Refreshing Caches - Completed,took: {} ms" , (System.currentTimeMillis() - sTime) );
				sTimeCheck = System.currentTimeMillis();
				log.logMessage("Check and Create Indexes if not already present - Start.");
				for (String langCode : languagesCache.keySet()) {
					String prefix = DcqVodIngestorConstants.PREFIX_VALUE;
					String indexName = prefix +"_" + DcqVodIngestorConstants.VLCCONTENT_IN_LOWER + "_" + langCode.toLowerCase();
					String writeAlias = DcqVodIngestorUtils.getWriteAlias(DcqVodIngestorConstants.VLCCONTENT_IN_LOWER + "_" + langCode.toLowerCase());
							
					if(mode.equalsIgnoreCase(DcqVodIngestorConstants.REINDEX)){
						indexName = indexName+DcqVodIngestorConstants.UNDERSCORE+indexDate;
						GetAliasResponse aliasResponse = DcqVodIngestorUtils.getIndexByAliasName( DcqVodIngestorUtils.getWriteAlias(DcqVodIngestorConstants.VLCCONTENT_IN_LOWER + "_" + langCode.toLowerCase()), config);
						if(!aliasResponse.getAliases().isEmpty()){
							 String existingIndexName = aliasResponse.getIndexName();
							 
							 //removing write alias
							 DcqVodIngestorUtils.removeAlias(config, existingIndexName, writeAlias);
							}
					}else{
						indexName = getIndexByAliasName(config, indexName, DcqVodIngestorConstants.VLCCONTENT_IN_LOWER + "_" + langCode.toLowerCase());
					}
					if (!esAdminCientManager.isIndexExists(indexName,dcqIngestorUser,dcqIngestorPassword)) {
						
						
						String vlcContentSettings = dcqVodIngestorPropertiesManager.getEsSettings(DcqVodIngestorConstants.VLCCONTENT_IN_LOWER+ "_" + langCode.toLowerCase());
						
						if (StringUtils.isBlank(vlcContentSettings)) {
							log.logMessage("Settings not available for {}.Loading default configurations",
									DcqVodIngestorConstants.VLCCONTENT_IN_LOWER + "_" + langCode.toLowerCase());
							vlcContentSettings = dcqVodIngestorPropertiesManager.getEsSettings(DcqVodIngestorConstants.VLCCONTENT_IN_LOWER+"_"+"eng");
						}
								log.logMessage("VLC Content Settings file read {}" ,vlcContentSettings );
						if(DcqVodIngestorUtils.isEmpty(vlcContentSettings)){
						esAdminCientManager.createIndex(indexName,isAliasEnabled,dcqIngestorUser,dcqIngestorPassword);
						}
						else{
						esAdminCientManager.createIndexWithSettings(indexName,dcqIngestorUser,dcqIngestorPassword, vlcContentSettings,isAliasEnabled);
						log.logMessage("Created index for VLC Content with settings - Completed" );
						}
					}
					//Start - changes for Jira Story - AVS-9826
					if (!esAdminCientManager.isTypeExistsInIndex(indexName, DcqVodIngestorConstants.ALL_IN_LOWER,dcqIngestorUser,dcqIngestorPassword)) {
						
						
						String vlccontentMapping = dcqVodIngestorPropertiesManager.getEsMapping(DcqVodIngestorConstants.VLCCONTENT_IN_LOWER+"_"+langCode.toLowerCase());
						if (StringUtils.isBlank(vlccontentMapping)) {
							log.logMessage("Mapping not available for {}.Loading default configurations",
									DcqVodIngestorConstants.VLCCONTENT_IN_LOWER + "_" + langCode.toLowerCase());
							vlccontentMapping = dcqVodIngestorPropertiesManager.getEsMapping(DcqVodIngestorConstants.VLCCONTENT_IN_LOWER+"_"+"eng");
						}
						
						
						esAdminCientManager.createTypeWithMapping(indexName, DcqVodIngestorConstants.ALL_IN_LOWER,dcqIngestorUser,dcqIngestorPassword,
								vlccontentMapping,isAliasEnabled);
					
						//creating write alias
						DcqVodIngestorUtils.createAlias(config, indexName, writeAlias);
					
					}
					//End - changes for Jira Story - AVS-9826					
				}
			}
        if( null != sTimeCheck){
			log.logMessage("Check and Create Indexes if not already present for {} Ingestion - Completed,took: {} ms" , ingestionType  ,  (System.currentTimeMillis() - sTimeCheck) );
        }
    	log.logMessage("Check and Create Indexes if not already present for {} Ingestion - Completed" , ingestionType );
		} catch (Exception e) {
			log.logMessage(e.getMessage());
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR, new NestedParameters(e.getMessage()));
		}
		
	}
	private String getIndexByAliasName(Configuration config, String indexName, String aliasName) throws ConfigurationException {
		GetAliasResponse writeAliasResponse = DcqVodIngestorUtils.getIndexByAliasName(DcqVodIngestorUtils.getWriteAlias(aliasName),config);
		if(writeAliasResponse!=null && !writeAliasResponse.getAliases().isEmpty()){
			 indexName = writeAliasResponse.getIndexName();
			}else {
				GetAliasResponse aliasResponse = DcqVodIngestorUtils.getIndexByAliasName( aliasName,config);
				if(aliasResponse!=null && !aliasResponse.getAliases().isEmpty()){
					 indexName = aliasResponse.getIndexName();
					 DcqVodIngestorUtils.createAlias(config, indexName, DcqVodIngestorUtils.getWriteAlias(aliasName));
					}
			}
		return indexName;
	}

}
