package com.accenture.avs.be.dcq.vod.ingestor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.accenture.avs.persistence.technicalcatalogue.CategoryAggregationEntity;

@Repository
public interface CategoryAggregationRepository extends JpaRepository<CategoryAggregationEntity,Integer>{

}
