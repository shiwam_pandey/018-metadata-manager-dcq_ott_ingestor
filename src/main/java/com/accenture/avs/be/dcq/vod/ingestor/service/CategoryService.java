package com.accenture.avs.be.dcq.vod.ingestor.service;

import java.io.IOException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ConfigurationException;

/**
 * @author karthik.vadla
 *
 */
public interface CategoryService {

	GenericResponse unpublishCategory(String categoryId, Configuration config) throws ApplicationException, ConfigurationException, JsonParseException, JsonMappingException, IOException;
	 GenericResponse upsertCategory(String csvCategory,Configuration config)  throws ApplicationException, JsonParseException, JsonMappingException, ConfigurationException, IOException ;
}
