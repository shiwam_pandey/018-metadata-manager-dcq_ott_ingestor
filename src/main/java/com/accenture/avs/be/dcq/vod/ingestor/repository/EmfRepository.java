package com.accenture.avs.be.dcq.vod.ingestor.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.accenture.avs.persistence.technicalcatalogue.EmfEntity;
/**
 * @author karthik.vadla
 *
 */
@Repository
public interface EmfRepository extends JpaRepository<EmfEntity,Integer>{

	List<EmfEntity> retrieveByEmfName(@Param("emfName")String emfName);
	Integer getMaxId();

}
