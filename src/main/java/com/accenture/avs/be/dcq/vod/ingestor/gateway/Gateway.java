package com.accenture.avs.be.dcq.vod.ingestor.gateway;

import com.accenture.avs.be.dcq.vod.ingestor.gateway.input.GatewayInput;
import com.accenture.avs.be.dcq.vod.ingestor.gateway.output.GatewayOutput;

/**
 * All gateways must implement this interface.
 */
/**
 * @author karthik.vadla
 *
 * @param <Input>
 * @param <Output>
 */
public interface Gateway<Input extends GatewayInput, Output extends GatewayOutput> {

	public Output execute(Input input);

}
