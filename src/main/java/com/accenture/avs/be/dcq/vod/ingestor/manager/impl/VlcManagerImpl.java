package com.accenture.avs.be.dcq.vod.ingestor.manager.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.avs.be.dcq.vod.ingestor.manager.VlcManager;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ChannelRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ContentPlatformRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ContentRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.VlcContentRepository;
import com.accenture.avs.be.framework.configuration.CacheConstants;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.persistence.dto.VlcContentDTO;
import com.accenture.avs.persistence.technicalcatalogue.VlcContentEntity;
import com.accenture.avs.persistence.technicalcatalogue.VlcContentEntityPK;

/**
 * @author karthik.vadla
 *
 */
@Component
public class VlcManagerImpl implements VlcManager {
	private static final LoggerWrapper log = new LoggerWrapper(VlcManagerImpl.class);
	
	@Autowired
	private VlcContentRepository vlcContentRepository;
	
	@Autowired
	private ContentPlatformRepository contentPlatformRepository;
	
	@Autowired
	private ChannelRepository channelRepository;

	@Autowired
	private ContentRepository contentRepositiry;
	
	private void deleteRelatedVlcContent(Integer channelId, Date publishedDate) {
		vlcContentRepository.deleteVlcContentByChannelAndPublishedDate(channelId, publishedDate);
		
	}

	
	private void saveVlcContents(List<VlcContentDTO> vlcContentDTOs) {
		
		List<VlcContentEntity> vlcContentEntities = new ArrayList<VlcContentEntity>();
		
		for(VlcContentDTO vlcContentDTO:vlcContentDTOs){
			VlcContentEntity vlcContEntity = new VlcContentEntity();
			VlcContentEntityPK vlcContEntityPk = new VlcContentEntityPK();
			
			vlcContEntityPk.setChannelId(vlcContentDTO.getChannelId());
			vlcContEntityPk.setContentId(vlcContentDTO.getContentId());
			vlcContEntityPk.setPlaylistPublishedDate(vlcContentDTO.getPlaylistPublishedDate());
			vlcContEntityPk.setStartTime(vlcContentDTO.getStartTime());
			
			vlcContEntity.setId(vlcContEntityPk);
			vlcContEntity.setEndTime(vlcContentDTO.getEndTime());
			vlcContEntity.setIsDefault(vlcContentDTO.getIsDefault());
			vlcContEntity.setVideoType(vlcContentDTO.getVideoType());
			
			vlcContentEntities.add(vlcContEntity);	
			
		}
		
		vlcContentRepository.saveAll(vlcContentEntities);
		
	}

	@Override
	@Transactional
	public void deleteAndSaveVlcContent(Integer channelId, Date publishedDate, List<VlcContentDTO> vlcContentDTOs) {
		log.logMessage("Deleting existing VLC content");
		deleteRelatedVlcContent(channelId, publishedDate);
		log.logMessage("Successfully deleted existing VLC content");
		log.logMessage("Saving VLC content data");
		saveVlcContents(vlcContentDTOs);
		log.logMessage("Successfully saved VLC content data");
		
	}
	
	@Override
	public void validateContentVideoType(List<Integer> contentIdList, List<VlcContentDTO> vlcContentDTOs) throws ApplicationException {
		long startTime = System.currentTimeMillis();
		log.logMessage("Validating the content videoTypes");
		int channelRecords = channelRepository.validateChannelId(vlcContentDTOs.get(0).getChannelId());
		if(!(channelRecords>0)) {
			throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters("channelId"));
		}
		List<Integer> ContentRecords = contentRepositiry.validateContentIds(contentIdList);
		if(contentIdList.size()>ContentRecords.size()) {
			contentIdList.removeAll(ContentRecords);
			if(!contentIdList.isEmpty()) {
				throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
						new NestedParameters("contentId does not exist in DB :"+contentIdList.get(0)));
			}
		}
		Map<Integer,List<String>> contentIdVideoTypes = null;
		List<Object[]> contentVideoTypesList = contentPlatformRepository.retrieveContentVideoTypesByContentIds(contentIdList);
		if(null!=contentVideoTypesList) {
			contentIdVideoTypes = new HashMap<>();
			for(Object[] contentVideoType: contentVideoTypesList) {
				if(null==contentIdVideoTypes.get((Integer)contentVideoType[0])) {
					List<String> videoTypes = new ArrayList<>();
					videoTypes.add((String)contentVideoType[1]);
					contentIdVideoTypes.put((Integer)contentVideoType[0], videoTypes);
					
				}else {
					contentIdVideoTypes.get((Integer)contentVideoType[0]).add((String)contentVideoType[1]);
				}
			}
			for(VlcContentDTO vlcContentDTO : vlcContentDTOs) {
				if(!(contentIdVideoTypes.get(vlcContentDTO.getContentId()).contains(vlcContentDTO.getVideoType()))) {
					throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters("videoType does not exist in DB for content:"+vlcContentDTO.getContentId()));
				}
			}
		}
		log.logMethodEnd(System.currentTimeMillis()-startTime);
	}

}
