package com.accenture.avs.be.dcq.vod.ingestor.content.dto;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;



/**
 * @author kavya.sree.vanam GetTransactionRateCardResponse
 *
 */
public class TransactionRateCardListDto implements Serializable {

    private static final long serialVersionUID = 1L;
    @ApiModelProperty(required = true, value = "total number of results",dataType ="integer")
    private Long totalResults;
    @ApiModelProperty(required = true, value = "TransactionRateCards")
    private List<GetTransactionRateCardResponse> transactionRateCards;
    
  
    public List<GetTransactionRateCardResponse> getTransactionRateCards() {
		return transactionRateCards;
	}

	public void setTransactionRateCards(List<GetTransactionRateCardResponse> transactionRateCards) {
		this.transactionRateCards = transactionRateCards;
	}

	public Long getTotalResults() {
        return totalResults;
    }
    
    public void setTotalResults(Long totalResults) {
        this.totalResults = totalResults;
    }

    
}
