package com.accenture.avs.be.dcq.vod.ingestor.gateway.report.input;

import java.util.List;
import java.util.Set;

import com.accenture.avs.be.dcq.vod.ingestor.dto.ChannelIdPlaylistDateDTO;
import com.accenture.avs.be.dcq.vod.ingestor.gateway.input.GatewayInput;

/**
 * @author karthik.vadla
 *
 */
public class PublishedRecordsReportInput implements GatewayInput {

	private Set<Integer> assetIds;
	private String contentType;
	private List<ChannelIdPlaylistDateDTO> channelIdPlaylistDateDTOs;
	private String startTime;

	public PublishedRecordsReportInput(Set<Integer> assetIds, String startTime,String contentType) {
		this.assetIds = assetIds;
		this.contentType = contentType;
		this.startTime = startTime;
	}

	public PublishedRecordsReportInput( List<ChannelIdPlaylistDateDTO> channelIdPlaylistDateDTOs, String startTime,String contentType) {
		this.channelIdPlaylistDateDTOs = channelIdPlaylistDateDTOs;
		this.contentType = contentType;
		this.startTime = startTime;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public Set<Integer> getAssetIds() {
		return assetIds;
	}

	public void setAssetIds(Set<Integer> assetIds) {
		this.assetIds = assetIds;
	}

	public List<ChannelIdPlaylistDateDTO> getChannelIdPlaylistDateDTOs() {
		return channelIdPlaylistDateDTOs;
	}

	public void setChannelIdPlaylistDateDTOs(List<ChannelIdPlaylistDateDTO> channelIdPlaylistDateDTOs) {
		this.channelIdPlaylistDateDTOs = channelIdPlaylistDateDTOs;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

}
