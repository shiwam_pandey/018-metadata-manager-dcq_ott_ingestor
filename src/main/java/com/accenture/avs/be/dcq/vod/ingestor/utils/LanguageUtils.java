package com.accenture.avs.be.dcq.vod.ingestor.utils;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class LanguageUtils {
	
	public static boolean validateLangCode(String langCode){
		boolean validateLang=false;
		String[] languages = Locale.getISOLanguages();
		Map<String, Locale> localeMap = new HashMap<String, Locale>(languages.length);
		for (String language : languages) {
		    Locale locale = new Locale(language);
		    localeMap.put(locale.getISO3Language(), locale);
		}
		Set ss=localeMap.keySet();
		for(Object o:ss){if(langCode.equalsIgnoreCase(o.toString())){validateLang=true;}}
		
		return validateLang;
	}

}
