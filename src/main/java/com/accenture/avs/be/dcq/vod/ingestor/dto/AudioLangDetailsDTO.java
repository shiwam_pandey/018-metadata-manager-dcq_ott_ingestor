package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.io.Serializable;

/**
 * @author karthik.vadla
 *
 */
public class AudioLangDetailsDTO implements Serializable{
	private static final long serialVersionUID = -2681353497942819024L;

	private Integer cpId;
	private String langId;
	private String label;
	private String isPreferred;
	
	public AudioLangDetailsDTO(Integer cpId,String langId, String label,String isPreferred) {
		super();
		this.cpId=cpId;
		this.langId = langId;
		this.label = label;
		this.isPreferred = isPreferred;
	}

	public AudioLangDetailsDTO() {

	}

	public Integer getCpId() {
		return cpId;
	}

	public void setCpId(Integer cpId) {
		this.cpId = cpId;
	}

	public String getLangId() {
		return langId;
	}

	public void setLangId(String langId) {
		this.langId = langId;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getIsPreferred() {
		return isPreferred;
	}

	public void setIsPreferred(String isPreferred) {
		this.isPreferred = isPreferred;
	}

}
