package com.accenture.avs.be.dcq.vod.ingestor.executor;

import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.dto.ChannelIdPlaylistDateDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionWriterRequestDTO;
import com.accenture.avs.be.dcq.vod.ingestor.manager.CategoryMetadataManager;
import com.accenture.avs.be.dcq.vod.ingestor.manager.ContentMetadataManager;
import com.accenture.avs.be.dcq.vod.ingestor.manager.VlcContentMetadataManager;
import com.accenture.avs.be.dcq.vod.ingestor.manager.VodChannelMetadataManager;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.exception.ApplicationException;
import  com.accenture.avs.commons.lib.LoggerWrapper;

/**
 * @author karthik.vadla
 *
 */
@Component
@Scope("prototype")
public class DcqThreadPoolExecutor implements Callable<List<DcqVodIngestionWriterRequestDTO>> {
	private static final LoggerWrapper log = new LoggerWrapper(DcqThreadPoolExecutor.class);

	@Autowired
	private ContentMetadataManager contentMetadataManager;
	@Autowired
	private CategoryMetadataManager categoryMetadataManager;
	@Autowired
	private VodChannelMetadataManager vodChannelMetadataManager;
	@Autowired
	private VlcContentMetadataManager vlcContentMetadataManager;
	
	private static DcqThreadPoolExecutor instance;
	private Integer assetId;
	private String assetType;
	private String transactionNumber;
	private Integer channelId;
	private Date playlistDate;
	private Integer rowId;
	private List<Integer> assetIdList;
	private List<ChannelIdPlaylistDateDTO> channelIdPlayList;
	private String startTime;
	private String mode;
	private String indexDate;
	private Configuration config;

	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}

	public Integer getAssetId() {
		return assetId;
	}

	public void setAssetId(Integer assetId) {
		this.assetId = assetId;
	}

	public String getAssetType() {
		return assetType;
	}

	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}

	public String getTransactionNumber() {
		return transactionNumber;
	}

	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}

	public Date getPlaylistDate() {
		return playlistDate;
	}

	public void setPlaylistDate(Date playlistDate) {
		this.playlistDate = playlistDate;
	}

	public Integer getRowId() {
		return rowId;
	}

	public void setRowId(Integer rowId) {
		this.rowId = rowId;
	}

	public List<Integer> getAssetIdList() {
		return assetIdList;
	}

	public void setAssetIdList(List<Integer> assetIdList) {
		this.assetIdList = assetIdList;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getIndexDate() {
		return indexDate;
	}

	public void setIndexDate(String indexDate) {
		this.indexDate = indexDate;
	}

	public Configuration getConfig() {
		return config;
	}

	public void setConfig(Configuration config) {
		this.config = config;
	}

	public List<ChannelIdPlaylistDateDTO> getChannelIdPlayList() {
		return channelIdPlayList;
	}

	public void setChannelIdPlayList(List<ChannelIdPlaylistDateDTO> channelIdPlayList) {
		this.channelIdPlayList = channelIdPlayList;
	}

	public DcqThreadPoolExecutor() {

	}

	public DcqThreadPoolExecutor(Integer assetId, String assetType, String transactionNumber,String startTime,String mode, String indexDate, Configuration config) {
		this.assetId = assetId;
		this.assetType = assetType;
		this.transactionNumber = transactionNumber;
		this.startTime = startTime;
		this.mode = mode;
		this.indexDate = indexDate;
		this.config =  config;
	}
	public DcqThreadPoolExecutor(List<Integer> assetIdList, String assetType, String transactionNumber,String startTime, String mode, String indexDate, Configuration config) {
		this.assetIdList = assetIdList;
		this.assetType = assetType;
		this.transactionNumber = transactionNumber;
		this.startTime = startTime;
		this.mode = mode;
		this.indexDate = indexDate;
		this.config =  config;
	}
	public DcqThreadPoolExecutor(Integer channelId, Date playlistDate, Integer rowId, String assetType,  String transactionNumber,String startTime,String mode, String indexDate, Configuration config) {
		this.channelId = channelId;
		this.playlistDate = playlistDate;
		this.rowId = rowId;
		this.assetType = assetType;
		this.transactionNumber = transactionNumber;
		this.startTime = startTime;
		this.mode = mode;
		this.indexDate = indexDate;
		this.config =  config;
	}
	
	public DcqThreadPoolExecutor( String assetType, List<ChannelIdPlaylistDateDTO> channelIdPlayList,  String transactionNumber,String startTime,String mode, String indexDate, Configuration config) {
		this.channelIdPlayList = channelIdPlayList;
		this.assetType = assetType;
		this.transactionNumber = transactionNumber;
		this.startTime = startTime;
		this.mode = mode;
		this.indexDate = indexDate;
		this.config =  config;
	}

	public static synchronized DcqThreadPoolExecutor getInstance() {
		if (instance == null) {
			instance = new DcqThreadPoolExecutor();
		}
		return instance;
	}

	@Override
	public List<DcqVodIngestionWriterRequestDTO> call() throws ApplicationException {
		List<DcqVodIngestionWriterRequestDTO> ingestionWriterRequestDTOs = null;
		try {
			if (assetType.equals(DcqVodIngestorConstants.CONTENT)) {
				ingestionWriterRequestDTOs = contentMetadataManager.processandWriteContents(assetIdList, transactionNumber,startTime, mode, indexDate, config);
			} else if (assetType.equals(DcqVodIngestorConstants.CATEGORY)) {
				ingestionWriterRequestDTOs = categoryMetadataManager.processAndWriteCategories(assetIdList, transactionNumber,startTime, mode, indexDate, config);
			} else if (assetType.equals(DcqVodIngestorConstants.VODCHANNEL)) {
				ingestionWriterRequestDTOs = vodChannelMetadataManager.processAndWriteVodChannels(assetIdList, transactionNumber,startTime, mode, indexDate, config);
			} else if (assetType.equals(DcqVodIngestorConstants.VLCCONTENT)) {
				ingestionWriterRequestDTOs = vlcContentMetadataManager.processAndWriteVlcContents(channelIdPlayList, transactionNumber,startTime, mode, indexDate, config);
			}
		} catch (Exception e) {
			log.logError(e);
			throw new ApplicationException("");
		}
		return ingestionWriterRequestDTOs;
	}
	
}
