package com.accenture.avs.be.dcq.vod.ingestor.manager.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.dto.ChannelIdPlaylistDateDTO;
import com.accenture.avs.be.dcq.vod.ingestor.repository.DcqAssetStagingRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.DcqPlaylistStagingRepository;
import com.accenture.avs.be.dcq.vod.ingestor.utils.CacheConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.persistence.technicalcatalogue.DCQPlaylistStagingEntity;
import com.accenture.avs.persistence.technicalcatalogue.DcqAssetStagingEntity;

/**
 * @author karthik.vadla
 *
 */
@Component
public class DcqAssetStagingManager {
	private static final LoggerWrapper log = new LoggerWrapper(DcqAssetStagingManager.class);
	
	@Autowired
	private DcqAssetStagingRepository dcqAssetStagingRepository;
	
	@Autowired
	private DcqPlaylistStagingRepository dcqPlaylistStagingRepository;
	
	public void updateStatus(String errorInfo, List<Integer> assetIds,String assetType, Configuration config) {
		try {
			Map<Integer, String> errorchannelIdMap = new HashMap<Integer, String>();
			for (Integer channelId : assetIds) {
				errorchannelIdMap.put(channelId, errorInfo);
			}
			updateStatusAsFailed(errorchannelIdMap,assetType,config);
		} catch (Exception ex) {
			log.logError(ex,"There is an error again,Update the status of all channelIds to FAILED");
		}
	}
	public void updateStatus( String errorInfo,Set<Integer> assetIds,String assetType, Configuration config) {
		try {
			Map<Integer, String> errorchannelIdMap = new HashMap<Integer, String>();
			for (Integer channelId : assetIds) {
				errorchannelIdMap.put(channelId, errorInfo);
			}
			updateStatusAsFailed(errorchannelIdMap,assetType, config);
		} catch (Exception ex) {
			log.logError(ex,"There is an error again,Update the status of all channelIds to FAILED");
		}
	}

	public void updateStatusAsPublished( Set<Integer> publishedIds,String assetType) throws ApplicationException {
		try {
			log.logMessage("Update Status as Published, update end_time.");
			log.logMessage("TRANSACTION_START");
			
			dcqAssetStagingRepository.updateStatusPublished(publishedIds,assetType, DcqVodIngestorConstants.PUBLISHED, DcqVodIngestorConstants.IN_PROGRESS);
			log.logMessage("TRANSACTION_END");
		} catch (Exception e) {
			log.logError(e);
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR, new NestedParameters(e.getMessage()));
		} 
	}
	public void deletePublishedAssets( Set<Integer> publishedIds,String assetType) throws ApplicationException {
		try {
			log.logMessage("delete Published Assets.");
			log.logMessage("TRANSACTION_START");
			
			dcqAssetStagingRepository.deleteAssets(publishedIds,assetType);
			log.logMessage("TRANSACTION_END");
		} catch (Exception e) {
			log.logError(e);
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR, new NestedParameters(e.getMessage()));
		} 
	}

	public void updateStatusAsFailed( Map<Integer, String> failedIdsMap,String assetType, Configuration config) throws ApplicationException {
		try {
			log.logMessage("Update Status as Failed, update error_info,increment process_counter,update end_time.");
			Set<Integer> failedAssetIds = failedIdsMap.keySet();
			List<Integer> failedAssetIdsList = new ArrayList<Integer>(failedAssetIds);
			if(failedAssetIdsList != null && !failedAssetIdsList.isEmpty()){
			List<DcqAssetStagingEntity> dcqAssetStagingEntityList = dcqAssetStagingRepository.retrieveFailedAssets(failedAssetIdsList,assetType);
			for (DcqAssetStagingEntity dcqAssetStagingEntity : dcqAssetStagingEntityList) {
				dcqAssetStagingEntity.setStatus(DcqVodIngestorConstants.FAILED);
				dcqAssetStagingEntity.setErrorInfo(failedIdsMap.get(dcqAssetStagingEntity.getAssetId()));
				dcqAssetStagingEntity.setEndTime(new Date());
				if (dcqAssetStagingEntity.getProcessCounter() == null) {
					dcqAssetStagingEntity.setProcessCounter(1);
				} else if (dcqAssetStagingEntity.getProcessCounter() != null && dcqAssetStagingEntity.getProcessCounter() >= Integer
						.parseInt(config.getConstants().getValue(CacheConstants.DCQ_VOD_INGESTOR_MAX_RETRY))) {
					dcqAssetStagingEntity.setProcessCounter(dcqAssetStagingEntity.getProcessCounter());
				} else {
					dcqAssetStagingEntity.setProcessCounter(dcqAssetStagingEntity.getProcessCounter() + 1);
				}
				dcqAssetStagingRepository.save(dcqAssetStagingEntity);
			}
			}
		} catch (Exception e) {
			log.logError(e);
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR, new NestedParameters(e.getMessage()));
		} 
	}
	public void updatePlayListStatus( String errorInfo,List<ChannelIdPlaylistDateDTO> channelIdPlaylistDateDTOList, Configuration config) {
		try {
			Map<Integer, String> errorchannelIdMap = new HashMap<Integer, String>();
			for (ChannelIdPlaylistDateDTO channelIdPlaylistDateDTO : channelIdPlaylistDateDTOList) {
				errorchannelIdMap.put(channelIdPlaylistDateDTO.getRowId(), errorInfo);
			}
			updatePlaylistStatusAsFailed( errorchannelIdMap, config);
		} catch (Exception ex) {
			log.logError(ex,"There is an error again,Update the status of all channelIds to FAILED");
		}
		
	}

	public void updatePlaylistStatusAsFailed( Map<Integer, String> failedIdsMap, Configuration config) throws ApplicationException {
		try {
			log.logMessage("Update Status as Failed, update error_info,increment process_counter,update end_time.");
			Set<Integer> failedAssetIds = failedIdsMap.keySet();
			List<Integer> failedAssetIdsList = new ArrayList<Integer>(failedAssetIds);
			List<DCQPlaylistStagingEntity> dCQPlaylistStagingEntityList = dcqPlaylistStagingRepository.retrieveFailedAssets(failedAssetIdsList);
			for (DCQPlaylistStagingEntity dcqAssetStagingEntity : dCQPlaylistStagingEntityList) {
				dcqAssetStagingEntity.setStatus(DcqVodIngestorConstants.FAILED);
				dcqAssetStagingEntity.setErrorInfo(failedIdsMap.get(dcqAssetStagingEntity.getRowId()));
				dcqAssetStagingEntity.setEndTime(new Date());
				if (dcqAssetStagingEntity.getProcessCounter() == null) {
					dcqAssetStagingEntity.setProcessCounter(1);
				} else if (dcqAssetStagingEntity.getProcessCounter() != null && dcqAssetStagingEntity.getProcessCounter() >= Integer
						.parseInt(config.getConstants().getValue(CacheConstants.DCQ_VOD_INGESTOR_MAX_RETRY))) {
					dcqAssetStagingEntity.setProcessCounter(dcqAssetStagingEntity.getProcessCounter());
				} else {
					dcqAssetStagingEntity.setProcessCounter(dcqAssetStagingEntity.getProcessCounter() + 1);
				}
			}
		} catch (Exception e) {
			log.logError(e);
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR, new NestedParameters(e.getMessage()));
		}
		
	}
	public void deletePublishedChannel( Set<Integer> publishedIds) throws ApplicationException {
		try {
			log.logMessage("delete Published Channels.");
			log.logMessage("TRANSACTION_START");
			dcqPlaylistStagingRepository.deleteChannels(publishedIds);
			log.logMessage("TRANSACTION_END");
		} catch (Exception e) {
			log.logError(e);
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR, new NestedParameters(e.getMessage()));
		} 
	}
}
