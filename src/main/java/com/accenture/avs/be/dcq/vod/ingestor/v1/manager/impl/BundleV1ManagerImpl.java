package com.accenture.avs.be.dcq.vod.ingestor.v1.manager.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.avs.be.dcq.vod.ingestor.cache.DeviceChannelsCache;

import com.accenture.avs.be.dcq.vod.ingestor.manager.ContentMetadataManager;
import com.accenture.avs.be.dcq.vod.ingestor.repository.BlacklistDeviceContentRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.BundleAggregationRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.RelPlatformTechnicalRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.TechnicalPackageRepository;
import com.accenture.avs.be.dcq.vod.ingestor.utils.CacheConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.Validator;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.bundle.asset.BundleDTO;
import com.accenture.avs.be.dcq.vod.ingestor.v1.dto.bundle.asset.BundleDTO.BundleContents;
import com.accenture.avs.be.dcq.vod.ingestor.v1.manager.BundleV1Manager;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.be.framework.exception.ConfigurationException;
import com.accenture.avs.be.framework.utils.Utilities;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.persistence.technicalcatalogue.BlackListDeviceEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.BlacklistDeviceEntity;
import com.accenture.avs.persistence.technicalcatalogue.ContentEntity;
import com.accenture.avs.persistence.technicalcatalogue.ContentPlatformEntity;
import com.accenture.avs.persistence.technicalcatalogue.RelPlatformTechnicalEntity;
import com.accenture.avs.persistence.technicalcatalogue.RelPlatformTechnicalEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.TechnicalPackageEntity;
import com.accenture.avs.persistence.technicalcatalogue.TeckPkgTypeEntity;


/**
 * @author naga.sireesha.meka
 *
 */
@Component
public class BundleV1ManagerImpl implements BundleV1Manager {
	private static final LoggerWrapper log = new LoggerWrapper(BundleV1ManagerImpl.class);
	
	@Autowired
	private BundleAggregationRepository bundleAggregationRepository;
	
	@Autowired
	private BlacklistDeviceContentRepository blacklistDeviceRepository;
	
	@Autowired
	private RelPlatformTechnicalRepository relPlatformTechnicalRepository;
	
	@Autowired
	private TechnicalPackageRepository packageRepository;
	
	@Autowired 
	private ContentMetadataManager contentMetadataManager;
	
	@Autowired
	private DeviceChannelsCache deviceChannelsCache;

	
	public List<Object> getContentIdsByBundleContentId(Integer bundleContentId){
		return bundleAggregationRepository.retrieveContentsByBundleContentId(bundleContentId);
	}

	@Override
	@Transactional
	public void deleteFromBundleAggregation(Integer bundleContentId) {
		log.logMessage("Deleting existing Bundle Content");
		bundleAggregationRepository.deleteBundleContentByBundleContentID(bundleContentId);
		log.logMessage("Successfully deleted existing Bundle Content");
		
		log.logMessage("Deleting existing Bundle Content from BlacklistDeviceContent");
		blacklistDeviceRepository.deleteByContentId(bundleContentId);
		log.logMessage("Successfully deleted existing Bundle Content from BlacklistDeviceContent");
	}

	@Override
	@Transactional
	public void deleteSVODMapping(Integer bundleId){
		log.logMessage("Deleting SVOD Bundle mapping from RelPlatformTechnical");
		relPlatformTechnicalRepository.deleteByBundleId(bundleId);
		log.logMessage("Successfully deleted SVOD Bundle mapping ");
	}
	
	@Override
	@Transactional
	public void deleteTVODRelPlatform(String packageName) {
		log.logMessage("Deleting TVOD Bundle mapping from RelPlatformTechnical");
		List<TechnicalPackageEntity> technicalPackages = packageRepository.retrievePackageListByName(packageName);
		if(!Utilities.isEmptyCollection(technicalPackages)) {
			for (TechnicalPackageEntity technicalPackageEntity : technicalPackages) {
				relPlatformTechnicalRepository.deleteByPackageId(technicalPackageEntity.getPackageId());
			}
		}
				
	}

	@Override
	public Set<Integer> getAllSVODPackages(ContentEntity bundleContent) {
		log.logMessage("Start of the method getAllSVODPackages");
		Set<Integer> packageIds = new HashSet<Integer>();
		for (ContentPlatformEntity contentPlatform : bundleContent.getContentPlatforms()) {
			if (DcqVodIngestorConstants.CHAR_YES == contentPlatform.getIsPublished()) {
				List<RelPlatformTechnicalEntity> relPlatformTechnicalList = relPlatformTechnicalRepository
						.retriveByContentIdAndContentPlatformId(contentPlatform.getCpId(),
								bundleContent.getContentId());
				for (RelPlatformTechnicalEntity relPlatformTechnical : relPlatformTechnicalList) {
					List<TechnicalPackageEntity> technicalPackagesList = packageRepository
							.selectByPackageId(relPlatformTechnical.getId().getPackageId());
					for (TechnicalPackageEntity technicalPackageEntity : technicalPackagesList) {
						if (DcqVodIngestorConstants.SVOD
								.equals(technicalPackageEntity.getTeckPkgType().getPackageType())) {
							packageIds.add(technicalPackageEntity.getPackageId());
						}
					}
				}
			}

		}
		log.logMessage("End of the method getAllSVODPackages");
		return packageIds;
	}
	
	@Override
	public TechnicalPackageEntity getTechnicalPackages(BundleDTO bundle,ContentEntity content,String packageType, Configuration config) throws ConfigurationException{
		
		TechnicalPackageEntity technicalPackageEntity = null;
		if (bundle.getProductInfo() != null && "N".equalsIgnoreCase(config.getConstants().getValue(CacheConstants.SDP_IN_PANIC))){
			Long packageId = packageRepository.retrievePackageIdByName(DcqVodIngestorConstants.PKG_BUNDLE+"_"+content.getContentId()+"_"+bundle.getBundleId());
			if(packageId == null){
				technicalPackageEntity = prepareTechnicalPackage(bundle.getBundleId(), bundle.getProductInfo().getContentRentalPeriod(), content.getContentId(),packageType);
				packageRepository.save(technicalPackageEntity);
			}else{
				technicalPackageEntity = packageRepository.selectByPackageId(packageId.intValue()).get(0);
			}
		}
		getRelPlatformTechnicalList(content,technicalPackageEntity);
		return technicalPackageEntity;
		
	}

	private TechnicalPackageEntity prepareTechnicalPackage(String bundleId, Integer contentRentalPeriod,
			Integer contentId, String packageType) {
		TechnicalPackageEntity technicalPackage = new TechnicalPackageEntity();
		technicalPackage.setPackageName(DcqVodIngestorConstants.PKG_BUNDLE+"_"+contentId+"_"+bundleId);
		technicalPackage.setIsEnabled(DcqVodIngestorConstants.SHORT_YES);
		technicalPackage.setPackageDescription(DcqVodIngestorConstants.PKG_BUNDLE+"_"+contentId+"_"+bundleId);
		
		TeckPkgTypeEntity teckPkgType = new TeckPkgTypeEntity();
		teckPkgType.setPackageType(packageType);
		technicalPackage.setTeckPkgType(teckPkgType);
		technicalPackage.setRentalPeriod(packageType.equals(DcqVodIngestorConstants.BUNDLE) || packageType.equals(DcqVodIngestorConstants.BUNDLE_GROUP) || contentRentalPeriod == null ? null : contentRentalPeriod);
		return technicalPackage;
	}
	
	private List<RelPlatformTechnicalEntity> getRelPlatformTechnicalList(ContentEntity bundleContent, TechnicalPackageEntity technicalPackage){
		List<RelPlatformTechnicalEntity> relPlatformTechnicalList = new ArrayList<>();
		for (ContentPlatformEntity contentPlatform : bundleContent.getContentPlatforms()) {
			RelPlatformTechnicalEntity relPlatformTechnical = null;
			if (DcqVodIngestorConstants.CHAR_YES == contentPlatform.getIsPublished()) {	
				relPlatformTechnical = getRelPlatformTechnical(bundleContent,contentPlatform.getCpId(),technicalPackage.getPackageId(), null);
				relPlatformTechnicalList.add(relPlatformTechnical);
			}
		}
		if(!relPlatformTechnicalList.isEmpty()){
			relPlatformTechnicalRepository.saveAll(relPlatformTechnicalList);
		}
		return relPlatformTechnicalList;
		
	}
	
	@Override
	public RelPlatformTechnicalEntity getRelPlatformTechnical(ContentEntity content, Integer cpId, Integer PackageId, Integer bundleId){
		RelPlatformTechnicalEntity relPlatformTechnical = relPlatformTechnicalRepository.getSBundleByContentIdAndPackageId(content.getContentId(), cpId, PackageId);
		bundleId = bundleId == null ? -1 : bundleId;
		if(relPlatformTechnical == null){		
			relPlatformTechnical = new RelPlatformTechnicalEntity();
			RelPlatformTechnicalEntityPK platformTechnicalPK = new RelPlatformTechnicalEntityPK();
			platformTechnicalPK.setCpId(cpId);
			platformTechnicalPK.setPackageId(PackageId);
			platformTechnicalPK.setSbundleId(-1);
			relPlatformTechnical.setId(platformTechnicalPK);					
		}
		relPlatformTechnical.setContent(content);
		return relPlatformTechnical;
	}
	
	@Override
	public Set<BlacklistDeviceEntity> getBlacklistDeviceContent(BundleDTO bundle, ContentEntity bundleContent,Configuration config) throws ApplicationException {
		Set<BlacklistDeviceEntity> blacklistDeviceContents = new HashSet<BlacklistDeviceEntity>();
		Set<Object> contentDeviceTypeListFromDB = getContentblacklistDevices(bundle);
		if (bundle.getBlackListDeviceTypes() != null && bundle.getBlackListDeviceTypes() != null
				&& bundle.getBlackListDeviceTypes().size() > 0) {
			//BlackListDeviceTypes blackListDeviceTypes = bundle.getBlackListDeviceTypes();
			if ( bundle.getBlackListDeviceTypes() != null) {
				List<String> deviceTypeList = bundle.getBlackListDeviceTypes();
				if(!deviceTypeList.containsAll(contentDeviceTypeListFromDB)){
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters(Validator.getAppropriateError(bundleContent)));	
				}
				if (deviceTypeList != null && !deviceTypeList.isEmpty()) {
					Map<String, Long> deviceChannels = deviceChannelsCache.getDeviceChannels();
					
					if(deviceChannels==null) {
						throw new ApplicationException(MessageKeys.ERROR_MS_ACN_916_SDP_NOT_AVAILABLE);
					}
					for (Iterator<String> iterator = deviceTypeList.iterator(); iterator.hasNext();) {
						String deviceTypes = iterator.next();
						Long tempExternalContentId;
						if (deviceChannels.containsKey(deviceTypes)) {
							BlacklistDeviceEntity blacklistDeviceContent = new BlacklistDeviceEntity();
							BlackListDeviceEntityPK blacklistDeviceContentPK = new BlackListDeviceEntityPK();
							blacklistDeviceContentPK.setDevicetypeId(deviceChannels.get(deviceTypes));
							tempExternalContentId = !Utilities.isEmpty(bundle.getBundleExternalContent().getBundleExternalContentId())
													? (contentMetadataManager.getContentByExternalId(
															bundle.getBundleExternalContent().getBundleExternalContentId()))
																	.getContentId()
													: Long.parseLong(bundle.getBundleExternalContent().getBundleContentId().toString());
							blacklistDeviceContentPK.setContentId(tempExternalContentId.intValue());
							blacklistDeviceContent.setId(blacklistDeviceContentPK);
							blacklistDeviceContents.add(blacklistDeviceContent);
						}else{
							throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
									new NestedParameters("unknown device type"));
						}

					}
				}

			}

		} else if (!contentDeviceTypeListFromDB.isEmpty()
				&& (bundle.getBlackListDeviceTypes() == null
						|| bundle.getBlackListDeviceTypes() == null
				|| bundle.getBlackListDeviceTypes().size() == 0)){
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters(Validator.getAppropriateMandatoryError(bundleContent)));
		}
		if(!blacklistDeviceContents.isEmpty()){
			blacklistDeviceRepository.saveAll(blacklistDeviceContents);
		}
		return blacklistDeviceContents;

	}	
	
	private Set<Object> getContentblacklistDevices(BundleDTO bundle) {
		
		Set<Object> contentblacklistDevices = new HashSet<>();
		for (BundleContents bundleContent : bundle.getBundleContents()) {
			Integer contentId = !Utilities.isEmpty(bundleContent.getExternalContentId())?contentMetadataManager.getContentByExternalId(bundleContent.getExternalContentId()).getContentId():bundleContent.getContentId();	
			
			List<Long> deviceTypeIds = blacklistDeviceRepository.getDeviceTypeIdByContentId(contentId);
			if(deviceTypeIds != null){
				Map<String, Long> deviceChannels = deviceChannelsCache.getDeviceChannels();
				for(Long deviceTypeId : deviceTypeIds){
					if(deviceChannels.containsValue(deviceTypeId)){
						for(Map.Entry<String, Long> entry : deviceChannels.entrySet()){
							if(entry.getValue().equals(deviceTypeId)){
								contentblacklistDevices.add(entry.getKey());
								break;
							}
						}		
					}
				}
			}
		}
		return contentblacklistDevices;
	}

	@Override
	public void createBundle(Set bundleAggregationList){
		if(!bundleAggregationList.isEmpty()){
			bundleAggregationRepository.saveAll(bundleAggregationList);
		}
	}
}
