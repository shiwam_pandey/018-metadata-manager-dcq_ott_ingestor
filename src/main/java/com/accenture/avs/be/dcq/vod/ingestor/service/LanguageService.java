package com.accenture.avs.be.dcq.vod.ingestor.service;

import com.accenture.avs.be.dcq.vod.ingestor.dto.LanguageDTO;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.exception.ApplicationException;

public interface LanguageService {
	GenericResponse<LanguageDTO> activateOrDeactivateLanguage(String status,String langCode) throws ApplicationException;
}
