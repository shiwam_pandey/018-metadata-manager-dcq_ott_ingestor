package com.accenture.avs.be.dcq.vod.ingestor.utils;

import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class CategoryUtils {
	
	private static Logger log = LogManager.getLogger(CategoryUtils.class);
	
	public static String addCategoryInputLine(String line,Properties categoryMappings){
		int count=line.split(";",-1).length; // added 3 semicolon as part of user story AVS 3723 for defect ADT-13863
		String categoryInputLine=line;
		StringBuffer lineBuffer = new StringBuffer(line);
		if(categoryMappings.containsKey("CATEGORY_INPUT")){
			count++;
		}
		if(categoryMappings.size()!=count) {
			int extraColumns = categoryMappings.size()-count;
			if(extraColumns>0) {
				for(int i=0; i<extraColumns ; i++) {
					//line+=";";
					lineBuffer.append(";");
				}
			}
		}
		
		//Fix for defect AVS-8481 - Given Category input wrong in source_file column - Start
		//line=line+";\""+categoryInputLine+"\""; 
		line=lineBuffer.toString()+";\""+categoryInputLine+"\"";
		boolean namesWithCategoryline=false; 
		if(log.isDebugEnabled()) {
		log.debug("LineTokenizer:doTokenize: Updated input is = "+line);}
		if(categoryMappings!=null){
		if(log.isDebugEnabled()) {
			log.debug("LineTokenizer:doTokenize: Given Category Columns : "+categoryMappings.keys());}
			
			//Check CATEGORY_INPUT present in names list
			if(!categoryMappings.containsKey("CATEGORY_INPUT")){
				namesWithCategoryline=true;
			}
			if(namesWithCategoryline){
				int length=categoryMappings.size()+1;
				/*String[] namesWithCategoryInput = new String[length];  
				
			    for(int cnt=0;cnt<categoryMappings.size();cnt++)
			    {  
			    	namesWithCategoryInput[cnt] = categoryMappings.keys();  
			    }  
			    namesWithCategoryInput[length-1]="CATEGORY_INPUT";
			    names=namesWithCategoryInput;*/
				categoryMappings.put("CATEGORY_INPUT", length-1);
				if(log.isDebugEnabled()) {
			    log.debug("LineTokenizer:doTokenize: After adding new column to save given category input line: "+categoryMappings.keys());}
			}
		}
		
		return line;
	}
}

