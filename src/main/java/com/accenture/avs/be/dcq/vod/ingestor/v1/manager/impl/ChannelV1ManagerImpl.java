package com.accenture.avs.be.dcq.vod.ingestor.v1.manager.impl;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBException;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.avs.be.dcq.vod.ingestor.dto.Channel;
import com.accenture.avs.be.dcq.vod.ingestor.dto.Platforms;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ChannelPlatformRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ChannelRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.ChannelTechnicalPkgRepository;
import com.accenture.avs.be.dcq.vod.ingestor.repository.TechnicalPackageRepository;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.JsonUtils;
import com.accenture.avs.be.dcq.vod.ingestor.v1.manager.ChannelV1Manager;
import com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.be.framework.utils.Utilities;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.persistence.technicalcatalogue.ChannelEntity;
import com.accenture.avs.persistence.technicalcatalogue.ChannelPlatformEntity;
import com.accenture.avs.persistence.technicalcatalogue.ChannelPlatformEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.ChannelTechnicalPkgEntity;
import com.accenture.avs.persistence.technicalcatalogue.ChannelTechnicalPkgEntityPK;
import com.accenture.avs.persistence.technicalcatalogue.TechnicalPackageEntity;
import com.fasterxml.jackson.core.JsonProcessingException;


@Component
@Transactional(propagation = Propagation.REQUIRES_NEW,rollbackFor = { RuntimeException.class })
public class ChannelV1ManagerImpl implements ChannelV1Manager{

	private static final LoggerWrapper log = new LoggerWrapper(ChannelV1ManagerImpl.class);
	
	@Autowired
	private ChannelRepository channelRepository;
	
	@Autowired
	private TechnicalPackageRepository technicalPackageRepository;
	
	@Autowired
	private ChannelPlatformRepository channelPlatformRepository;
	
	@Autowired
	private ChannelTechnicalPkgRepository channelTechnicalPkgRepository;
	
	@Autowired
	private EntityManager entityManager;
	
	@Override
	public List<Integer> retrieveByDefaultChannelNumber(String defaultChannelNumber) {
		List<Integer> channelIds = channelRepository.retrieveByDefaultChannelNumber(defaultChannelNumber);		
		return channelIds;
	}

	@Override
	public List<TechnicalPackageEntity> selectByPackageId(Integer packageId) {
		List<TechnicalPackageEntity> technicalPackage = technicalPackageRepository.selectByPackageId(packageId);		
		return technicalPackage;
	}

	@Override
	public Integer createChannel(Channel inputChannel) throws Exception {
		ChannelEntity channel = new ChannelEntity();
		channel.setChannelId(inputChannel.getChannelId());
		channel.setName(inputChannel.getName());
		channel.setType(inputChannel.getType());
		channel.setOrderId(inputChannel.getOrder());
		if(Utilities.isEmpty(inputChannel.getExternalId())) {
		throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER, new NestedParameters("ExternalId"));
		}
		
		
		if(inputChannel.getAdult() == null){
			channel.setIsAdult(DcqVodIngestorConstants.SHORT_NO);
		}else{
			channel.setIsAdult(inputChannel.getAdult());
		}
		
		channel.setDescription(inputChannel.getDescription());
		channel.setAdvTags(inputChannel.getAdvTags());
		channel.setVideoUrl(DcqVodIngestorConstants.VIDEO_URL);
				
		channel.setDefaultChannelNumber(inputChannel.getDefaultChannelNumber().toString());
		
		if(inputChannel.getIsDisAllowedAdv()==null){
			channel.setDisallowedAdv(DcqVodIngestorConstants.NO);
		}else{
			channel.setDisallowedAdv(inputChannel.getIsDisAllowedAdv());
			
		}
		
		if(inputChannel.getBroadcasterName() != null){
			channel.setBroadcasterName(inputChannel.getBroadcasterName());
		}
		
		
		
		if(inputChannel.getIsActive()!=null){
			channel.setIsActive(inputChannel.getIsActive());
		}else{
			channel.setIsActive(DcqVodIngestorConstants.YES);
		}
		channel.setIsCatchUp(DcqVodIngestorConstants.NO);
		channel.setIsTimeshift(DcqVodIngestorConstants.NO);
		if(!StringUtils.isEmpty(inputChannel.getExtendedMetadata()) && !JsonUtils.isJSONValid(inputChannel.getExtendedMetadata())){
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters("ExtendedMetadata"));
		}
		getSourcefile(inputChannel,channel);
		
		writeVodChannel(channel,inputChannel.getPlatforms(),inputChannel.getPackageIds());
		
		Integer channelId = inputChannel.getChannelId();
		return channelId;
		
	}
	@Transactional(rollbackFor = { RuntimeException.class })
	private void writeVodChannel(ChannelEntity channel, List<Platforms> platformList, List<BigInteger> packages) throws ApplicationException {
		
		try {
		deleteChannelRelatedData(channel.getChannelId());
		Integer channelId = channelRepository.save(channel).getChannelId();
		List<ChannelPlatformEntity> channelPlatforms = setChannelPlatforms(channelId, platformList);
		
		List<ChannelTechnicalPkgEntity> channelTechnicalPackages = setChannelTechPkg(channelId, packages);
			
		channelPlatformRepository.saveAll(channelPlatforms);
		channelTechnicalPkgRepository.saveAll(channelTechnicalPackages);
		entityManager.flush();
		entityManager.clear();
	}catch(ApplicationException ae) {
		throw ae;
	}catch(Exception e) {
		throw new RuntimeException();
	}
	}

	private void deleteChannelRelatedData(Integer channelId) {
		channelTechnicalPkgRepository.deleteByChannelId(channelId);
		channelPlatformRepository.deleteByChannelId(channelId);
	}

	/**
	 * Generate the set of ChannelPlatforms starting from the platform list 
	 * passed in the item procesor.
	 * 
	 * @param channel
	 * @param platformList
	 * @return Set of ChannelPlatform
	 * @throws ApplicationException 
	 */
	private List<ChannelPlatformEntity> setChannelPlatforms(Integer channelId,List<Platforms> platformList) throws ApplicationException {
		List<ChannelPlatformEntity> channelPlatformSet = new ArrayList<ChannelPlatformEntity>();
		
		Platforms platform = new Platforms();
			
			for (int i=0;i<platformList.size();i++) {
				ChannelPlatformEntity channelPlatform = new ChannelPlatformEntity();
				ChannelPlatformEntityPK cpPk = new ChannelPlatformEntityPK();
				
				platform = platformList.get(i);
				
				//ChannelPlatform Key
				cpPk.setChannelId(channelId);
				cpPk.setPlatformName(platform.getName());
				cpPk.setVideoType("NA");
		
				//ChannelPlatform
			 	channelPlatform.setVideoUrl(platform.getVideoURL());
				channelPlatform.setTrailerUrl(platform.getTrailerURL());
				channelPlatform.setIsPublished(platform.getIsPublished());
				channelPlatform.setChannelGroup(platform.getGroup());
				channelPlatform.setId(cpPk);
				
				//Logos
				channelPlatform.setLogoBig(platform.getLogoBig());
				channelPlatform.setLogoMedium(platform.getLogoMedium());
				channelPlatform.setLogSmall(platform.getLogoSmall());				
				
				if(!StringUtils.isEmpty(platform.getExtendedMetadata()) && !JsonUtils.isJSONValid(platform.getExtendedMetadata())){
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER, new NestedParameters("ExtendedMetadata"));
				}
				channelPlatformSet.add(channelPlatform);
			}


		return channelPlatformSet;

	}
	
	/**
	 * Generate the set of ChannelTechnicalPackage starting from the package list 
	 * passed in the item procesor.
	 * 
	 * @param channel
	 * @param pkgList
	 * @return Set of ChannelTechnicalPkg
	 */
	private  List<ChannelTechnicalPkgEntity>  setChannelTechPkg(Integer channelId,List<BigInteger> pkgList) {
		List<ChannelTechnicalPkgEntity>	technicalPackageList = new ArrayList<>();	
		for(int i=0;i<pkgList.size();i++) {
			ChannelTechnicalPkgEntity channelTechnicalPkg = new ChannelTechnicalPkgEntity();
			ChannelTechnicalPkgEntityPK ctpPk = new ChannelTechnicalPkgEntityPK();
			
			//ChannelTechPkg Key
			ctpPk.setChannelId(channelId);
			ctpPk.setPackageId(pkgList.get(i).intValue());
			
			//ChannelTechPkg
			channelTechnicalPkg.setId(ctpPk);
			
			technicalPackageList.add(channelTechnicalPkg);
			//channel.getChannelTechnicalPkgs().add(channelTechnicalPkg);
			
		}
		return technicalPackageList;
	}
	
	/**
	 * This method is used to get the source file
	 * @param inputChannel
	 * @param channel
	 * @throws JAXBException
	 * @throws IOException 
	 * @throws JsonMappingException
	 */
	private void getSourcefile(Channel inputChannel, ChannelEntity channel) throws JsonParseException, JsonMappingException, IOException{
		try{
			
		    channel.setSourceFile(JsonUtils.writeAsJsonStringWithoutNull(inputChannel).getBytes(Charset.forName("UTF-8")));
		}catch(JsonProcessingException e){
			log.logMessage("Error in setting source file :{}",e);
			throw e;
		}
		
	}
}

