package com.accenture.avs.be.dcq.vod.ingestor.service.impl;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.accenture.avs.be.dcq.vod.ingestor.content.dto.VlcContentPlaylist;
import com.accenture.avs.be.dcq.vod.ingestor.content.dto.Playlist;
import com.accenture.avs.be.dcq.vod.ingestor.dto.ChannelIdPlaylistDateDTO;
import com.accenture.avs.be.dcq.vod.ingestor.manager.VlcContentIngestManager;
import com.accenture.avs.be.dcq.vod.ingestor.manager.VlcContentMetadataManager;
import com.accenture.avs.be.dcq.vod.ingestor.manager.VlcManager;
import com.accenture.avs.be.dcq.vod.ingestor.manager.impl.DcqVodIngestorPropertiesManager;
import com.accenture.avs.be.dcq.vod.ingestor.processor.DcqVodIngestionPreProcessor;
import com.accenture.avs.be.dcq.vod.ingestor.service.VlcContentService;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.Validator;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.configuration.CacheConstants;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.persistence.dto.VlcContentDTO;

/**
 * @author naga.sireesha.meka
 *
 */
@Service
public class VlcContentServiceImpl implements VlcContentService {

	@Autowired
private	VlcContentIngestManager vlcContentManager;

	@Autowired
	private DcqVodIngestorPropertiesManager dcqVodIngestorPropertiesManager;
	
	@Autowired
	private VlcManager vlcManager;
	
	@Autowired
	private VlcContentMetadataManager vlcContentMetadataManager;
	
	@Autowired
	private DcqVodIngestionPreProcessor dcqVodIngestionPreProcessor;

	private static final LoggerWrapper log = new LoggerWrapper(VlcContentServiceImpl.class);

	@Override
	public GenericResponse updateVlcContent(Integer channelId, String vlcPublishedDate, String csvVlcContent, boolean sync,
			boolean defaultVlc,String transactionNumber, Configuration config) throws ApplicationException, Exception {
		GenericResponse genericResponse = null;
		List<VlcContentDTO> vlcContentDTOs = null;

		try {
			if (StringUtils.isBlank(csvVlcContent)) {
				throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
						new NestedParameters("Vlc input data in request body"));
			}
			String[] lines = csvVlcContent.split(System.getProperty(DcqVodIngestorConstants.LINE_SEPARATOR));
			if (lines != null && lines.length > 0) {
				
				vlcContentDTOs = readAllLines(lines, defaultVlc);
				log.logMessage("Successfully read VLC data");
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				java.util.Date utilDatePublished = null;
				try {
					
					utilDatePublished = formatter.parse(vlcPublishedDate);
				} catch (ParseException e) {
					
					throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters("playlist published date"));
				}
				if(utilDatePublished!=null)
				{
					deleteSaveVlcContent(vlcContentDTOs,channelId,utilDatePublished,transactionNumber,config);
					
					}		
			} else {
				throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
						new NestedParameters("Vlc input data in request body"));
			}

			genericResponse = new GenericResponse(DcqVodIngestorConstants.ACN_200, DcqVodIngestorConstants.OK,
					DcqVodIngestorConstants.EMPTY);
			
		}catch(DataIntegrityViolationException cex) {
			log.logError(cex);
			throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters(": ChannelId or contentId or videoType does not exist in DB"));
		}
		catch (ApplicationException ae) {
			log.logError(ae);
			throw ae;

		}
		catch (Exception e) {
			throw e;
		}

		return genericResponse;
	}

	public List<VlcContentDTO> readAllLines(String[] lines, boolean defaultVlcFlag) throws ApplicationException, Exception {
		List<VlcContentDTO> vlcContentDTOs = new ArrayList<VlcContentDTO>();
		log.logMessage("Reading input lines and convert to VLC data");
		try {
			Properties vlcContentMappings = dcqVodIngestorPropertiesManager.loadVlcMappingProperties();
			
			String defaultVlc = (defaultVlcFlag) ? "Y" : "N";
			for (String line : lines) {
				String[] fields = null;
				if (line != null && line.indexOf(";") >= 0) {

					fields = line.split(";");
					VlcContentDTO vlcDto = new VlcContentDTO();
					vlcDto.setChannelId(getFieldValueAsInteger("CHANNELID", fields, vlcContentMappings));
					if (vlcDto.getChannelId() == null) {
						throw new ApplicationException(
								CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
								new NestedParameters("channelid"));
					}
					vlcDto.setContentId(getFieldValueAsInteger("CONTENTID", fields, vlcContentMappings));
					if (vlcDto.getContentId() == null) {
						throw new ApplicationException(
								CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
								new NestedParameters("contentid"));
					}


					String playlistPubDateStr = getFieldValueAsString("PLAYLISTPUBLISHEDDATE", fields,
							vlcContentMappings);

					if (StringUtils.isBlank(playlistPubDateStr)) {
						throw new ApplicationException(
								CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
								new NestedParameters("playlist published date"));
					}
					Date playlistPubDateDate = null;
					try {
						playlistPubDateDate = Date.valueOf(playlistPubDateStr);
					} catch (IllegalArgumentException ex) {

						throw new ApplicationException(
								CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
								new NestedParameters("playlist published date"));
					}

					vlcDto.setPlaylistPublishedDate(playlistPubDateDate);

					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					java.util.Date utilDateStart = null;
					java.util.Date utilDateEnd = null;
					try {
						String startTimeStr = getFieldValueAsString("STARTTIME", fields, vlcContentMappings);
						if (StringUtils.isNotBlank(startTimeStr)) {
							utilDateStart = formatter.parse(startTimeStr);
						} else {
							throw new ApplicationException(
									CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
									new NestedParameters("startdate"));
						}

						String endTimeStr = getFieldValueAsString("ENDTIME", fields, vlcContentMappings);
						if (StringUtils.isNotBlank(endTimeStr)) {
							utilDateEnd = formatter.parse(endTimeStr);
						} else {
							throw new ApplicationException(
									CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
									new NestedParameters("enddate"));
						}
					} catch (ParseException e) {
						log.logError(e,
								"Error parsing startDate or endDate from csv. The correct format should be: 'yyyy-MM-dd HH:mm:ss'");
						throw new ApplicationException(
								CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
								new NestedParameters("startdate or enddate"));
					}
					
					if(utilDateStart.compareTo(utilDateEnd) > 0){
						throw new ApplicationException(
								CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
								new NestedParameters(": Start date is greater than end date"));
					}

					Timestamp dateStartFormatted = null;
					Timestamp dateEndFormatted = null;

					if (utilDateStart != null) {
						dateStartFormatted = new Timestamp(utilDateStart.getTime());}
					if (utilDateEnd != null) {
						dateEndFormatted = new Timestamp(utilDateEnd.getTime());}

					vlcDto.setStartTime(dateStartFormatted);
					vlcDto.setEndTime(dateEndFormatted);

					vlcDto.setVideoType(getFieldValueAsString("VIDEOTYPE", fields, vlcContentMappings));
					if (StringUtils.isBlank(vlcDto.getVideoType())) {
						throw new ApplicationException(
								CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
								new NestedParameters("videotype"));
					}

					vlcDto.setIsDefault(defaultVlc);
					vlcContentDTOs.add(vlcDto);
				}else{
					throw new ApplicationException(
							CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
							new NestedParameters("Vlc input data in request body"));
				}

			}
		} catch (ApplicationException e) {
			log.logMessage(e.getMessage());
			 throw e;
		} catch (Exception e) {
			log.logMessage(e.getMessage());
			throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR);
		}
		return vlcContentDTOs;
	}

	/**
	 * returns field value as string as per mapping provided
	 * 
	 * @param key
	 * @param fields
	 * @param vlcContentMappings
	 * @return
	 */
	String getFieldValueAsString(String key, String[] fields, Properties vlcContentMappings) {

		if (vlcContentMappings.containsKey(key)
				&& fields.length > Integer.parseInt(vlcContentMappings.get(key).toString())) {
			return fields[Integer.parseInt(vlcContentMappings.get(key).toString())];
		}

		return null;
	}

	/**
	 * returns field value as long as per mapping provided
	 * 
	 * @param key
	 * @param fields
	 * @param vlcContentMappings
	 * @return
	 * @throws ApplicationException 
	 */
	Integer getFieldValueAsInteger(String key, String[] fields, Properties vlcContentMappings) throws ApplicationException {

		try{
		if (vlcContentMappings.containsKey(key)
				&& fields.length > Integer.parseInt(vlcContentMappings.get(key).toString())
				&& fields[Integer.parseInt(vlcContentMappings.get(key).toString())] != null
				&& fields[Integer.parseInt(vlcContentMappings.get(key).toString())].trim().length() > 0) {

			return Integer.parseInt(fields[Integer.parseInt(vlcContentMappings.get(key).toString())]);
		}
		}catch(Exception ex){
			throw new ApplicationException(
					CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters(key));
		}

		return null;
	}

	@Override
	public GenericResponse updateVlcContent(VlcContentPlaylist vlcContentPlaylist,Configuration config,String transactionNumber) throws ApplicationException,Exception {
		// TODO Auto-generated method stub
		List<Integer> contentIdList = new ArrayList();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		List<VlcContentDTO> vlcContentDTOs = prepareVlcContentPlaylists(vlcContentPlaylist,config,contentIdList);
		if(null!=vlcContentDTOs) {
			vlcManager.validateContentVideoType(contentIdList, vlcContentDTOs);
			deleteSaveVlcContent(vlcContentDTOs,vlcContentPlaylist.getChannelId(),dateFormat.parse(vlcContentPlaylist.getPlaylistPublishedDate()),transactionNumber,config);
		}
		return  new GenericResponse(DcqVodIngestorConstants.ACN_200, DcqVodIngestorConstants.OK,
				DcqVodIngestorConstants.EMPTY);
	}
	
	public List<VlcContentDTO> prepareVlcContentPlaylists(VlcContentPlaylist vlcContentPlaylist,Configuration config,List<Integer> contentIdList) throws ApplicationException,Exception{

		List<VlcContentDTO> vlcContentDtoList = new ArrayList();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE,-1);
		java.util.Date startTime = null;
		java.util.Date endTime = null;
		Date playlistPubDateDate = null;
		if(null!=vlcContentPlaylist) {

			VlcContentDTO vlcContentDto = null;
			
			Validator.checkParameter(DcqVodIngestorConstants.VLC_CONTENT_PLAYLISTPUBDATE, vlcContentPlaylist.getPlaylistPublishedDate(),config);
			try {
				playlistPubDateDate = Date.valueOf(vlcContentPlaylist.getPlaylistPublishedDate());
				if(playlistPubDateDate.compareTo(cal.getTime())<0) {
					throw new ApplicationException(
							CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters("playlistPublishedDate should not be in past"));
				}
				cal.add(Calendar.DATE, 1);
			} catch (IllegalArgumentException ex) {
				throw new ApplicationException(
						CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
						new NestedParameters("playlistPublishedDate"));
			}
			
			Validator.checkParameter(DcqVodIngestorConstants.VLC_PLAYLIST, vlcContentPlaylist.getPlaylist(), config);

			for(Playlist playList : vlcContentPlaylist.getPlaylist())
			{
				vlcContentDto = new VlcContentDTO();
				vlcContentDto.setPlaylistPublishedDate(playlistPubDateDate);
				vlcContentDto.setChannelId((Validator.checkParameterValue(DcqVodIngestorConstants.CHANNEL_ID, String.valueOf(vlcContentPlaylist.getChannelId()), config)));
				Validator.checkParameter(DcqVodIngestorConstants.VIDEO_TYPE, playList.getVideoType(), config);
				Validator.sanitizingParamsForSpecialParams(DcqVodIngestorConstants.VIDEO_TYPE, playList.getVideoType());
				Validator.checkParameter(DcqVodIngestorConstants.CONTENT_ID, playList.getContentId(), config);	
				Validator.checkParameter(DcqVodIngestorConstants.END_TIME,playList.getEndTime(), config);
				Validator.checkParameter(DcqVodIngestorConstants.START_TIME,playList.getStartTime(), config);
				Validator.checkDateValidityFormat(DcqVodIngestorConstants.END_TIME,dateFormat,playList.getEndTime(), config);	
				Validator.checkDateValidityFormat(DcqVodIngestorConstants.START_TIME,dateFormat,playList.getStartTime(), config);	
				vlcContentDto.setVideoType(playList.getVideoType());
				vlcContentDto.setContentId(playList.getContentId());
				contentIdList.add(playList.getContentId());
				endTime = dateFormat.parse(playList.getEndTime());
				startTime = dateFormat.parse(playList.getStartTime());
				if(startTime.before(cal.getTime())) {
					throw new ApplicationException(
							CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters(": StartTime should be in future"));
				
				}
				if(endTime.before(cal.getTime())) {
					throw new ApplicationException(
							CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters(": EndTime should be in future"));
				}
				
				if(dateFormat.parse(playList.getStartTime()).compareTo(dateFormat.parse(playList.getEndTime()))>0) {
					throw new ApplicationException(
							CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters(": Start date is greater than end date"));
				}
				
				vlcContentDto.setEndTime(new Timestamp(endTime.getTime()));
				vlcContentDto.setStartTime(new Timestamp(startTime.getTime()));
				vlcContentDtoList.add(vlcContentDto);	
			}

			
		}
		return vlcContentDtoList;
}
	
	public void deleteSaveVlcContent(List<VlcContentDTO> vlcContentDTOs,Integer channelId,java.util.Date utilDatePublished,String transactionNumber,Configuration config) throws ApplicationException, Exception {
		
		try{
			Date sqlDatePublished = new java.sql.Date(utilDatePublished.getTime());
			vlcManager.deleteAndSaveVlcContent(channelId,sqlDatePublished,vlcContentDTOs);

			ChannelIdPlaylistDateDTO channelIdPlalistDateDto = new ChannelIdPlaylistDateDTO();
			channelIdPlalistDateDto.setChannelId(channelId);
			channelIdPlalistDateDto.setPlaylistDate(utilDatePublished);
			List<ChannelIdPlaylistDateDTO> channelIdPlaylistDateDTOs = new ArrayList<ChannelIdPlaylistDateDTO>();
			channelIdPlaylistDateDTOs.add(channelIdPlalistDateDto);
			log.logMessage("Started writing VLC contents to ES");
			dcqVodIngestionPreProcessor.refreshCachesCreateIndexes(DcqVodIngestorConstants.VLCCONTENT, "","", config);
			vlcContentMetadataManager.processAndWriteVlcContents(channelIdPlaylistDateDTOs, transactionNumber,  String.valueOf(System.currentTimeMillis()), "", null, config);
			log.logMessage("Successfully saved data to ES");
		}
		catch(DataIntegrityViolationException cex) {
			log.logError(cex);
			throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters(": ChannelId or contentId or videoType does not exist in DB"));
		}
		catch (ApplicationException ae) {
			log.logError(ae);
			throw ae;
		}
		catch (Exception e) {
			throw e;
		}
	}
}
