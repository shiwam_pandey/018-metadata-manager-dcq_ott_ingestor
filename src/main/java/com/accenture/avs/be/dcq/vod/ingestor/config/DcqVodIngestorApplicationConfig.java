package com.accenture.avs.be.dcq.vod.ingestor.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.accenture.avs.be.framework.AbstractSpringApplication;

@ComponentScan(basePackages = "com.accenture.avs.*")
@EnableTransactionManagement
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class })
public class DcqVodIngestorApplicationConfig extends AbstractSpringApplication{
	public static void main(String[] args) {
		SpringApplication.run(DcqVodIngestorApplicationConfig.class, args);
	}
	
}
