package com.accenture.avs.be.dcq.vod.ingestor.service.impl;

import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.accenture.avs.be.dcq.vod.ingestor.dto.ChannelDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.channel.asset.Channels;
import com.accenture.avs.be.dcq.vod.ingestor.dto.channel.asset.Channels.Channel;
import com.accenture.avs.be.dcq.vod.ingestor.manager.ChannelManager;
import com.accenture.avs.be.dcq.vod.ingestor.manager.VodChannelMetadataManager;
import com.accenture.avs.be.dcq.vod.ingestor.processor.DcqVodIngestionPreProcessor;
import com.accenture.avs.be.dcq.vod.ingestor.service.ChannelService;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.Validator;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.be.framework.exception.ConfigurationException;
import com.accenture.avs.commons.lib.LoggerWrapper;

/**
 * @author naga.sireesha.meka
 *
 */
@Service
public class ChannelServiceImpl implements ChannelService {

	@Autowired
private	ResourceLoader resourceLoader;
	@Autowired
	private ChannelManager channelManager;
	@Autowired
	private DcqVodIngestionPreProcessor dcqVodIngestionPreProcessor;
	@Autowired
	private VodChannelMetadataManager vodChannelMetadataManager;

	private static final LoggerWrapper log = new LoggerWrapper(ChannelServiceImpl.class);

	@Override
	public GenericResponse upsertChannel(String channelXml, Configuration config)
			throws ApplicationException, Exception {
		GenericResponse genericResponse = null;
		ChannelDTO response = null;
		StringReader reader=null;
		InputStream channelsXSD=null;
		try {
			log.logMessage("call to initializeChannelIngestor ");

			// validating the xml
			// File channelXSD =
			// resourceLoader.getResource("classpath:dcq-vod-ingestor/"+DcqVodIngestorConstants.CHANNEL.CHANNEL_XSD).getFile();
			// File channelXSD = new File("");
			 channelsXSD = resourceLoader
					.getResource("classpath:dcq-vod-ingestor/" + DcqVodIngestorConstants.CHANNEL.CHANNEL_XSD)
					.getInputStream();

			/*
			 * Start - AVS-24794 replace all unescaped ampersands with escaped ampersands
			 * for AssociateWebSiteUrl
			 */
			if (DcqVodIngestorUtils.checkVulnerablesInInputXML(channelXml)) {
				throw new ApplicationException(com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
						new NestedParameters(DcqVodIngestorConstants.ERR_VULNERABLE_REQ));
							}
			final Pattern unescapedAmpersands = Pattern.compile("(&(?!amp;))");
			Matcher m = unescapedAmpersands.matcher(channelXml);
			String xmlWithAmpersandsEscaped = m.replaceAll("&amp;");
			/*
			 * End - AVS-24794 replace all unescaped ampersands with escaped ampersands for
			 * AssociateWebSiteUrl
			 */
			boolean validXml = Validator.validateXMLSchema(channelsXSD, xmlWithAmpersandsEscaped);
			// boolean validXml = false;
			if (validXml) {
				log.logMessage("Input XML is valid");
				// un marshalling the xml
				JAXBContext jaxbContext = JAXBContext.newInstance(Channels.class);
				Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
				 reader = new StringReader(xmlWithAmpersandsEscaped);
				Channels channels = (Channels) unmarshaller.unmarshal(reader);
				if (Validator.isEmpty(channels)) {
					log.logMessage("No channels to write. The execution is completed.");
					genericResponse = new GenericResponse(DcqVodIngestorConstants.ACN_200, DcqVodIngestorConstants.OK,
							DcqVodIngestorConstants.NO_CHANNELS);
					return genericResponse;
				}

				// ingest into DB
				Integer channelId = ingestToDB(channels, config);
				if (channelId != null) {
					List<Integer> assetIdList = new ArrayList<>();
					assetIdList.add(channelId);
					dcqVodIngestionPreProcessor.refreshCachesCreateIndexes(DcqVodIngestorConstants.VODCHANNEL, "", "",
							config);
					vodChannelMetadataManager.processAndWriteVodChannels(assetIdList,
							DcqVodIngestorUtils.getTransactionNumber(), DcqVodIngestorUtils.currentTime(), "", "",
							config);
				}

			}
			genericResponse = new GenericResponse(DcqVodIngestorConstants.ACN_200, DcqVodIngestorConstants.OK,
					response);
		} catch (ApplicationException ae) {
			log.logError(ae);
			throw ae;

		} catch (JAXBException je) {
			log.logError(je);
			throw je;
		} catch (SAXParseException je) {
			log.logError(je);
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters("XML: " + je.getMessage()));

		} catch (SAXException je) {
			log.logError(je);
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters("XML: " + je.getMessage()));

		}catch (Exception e) {
			log.logError(e);
			throw e;
		}finally{
			if(reader!=null){
				reader.close();
			}
			if(channelsXSD!=null)channelsXSD.close();
		}
		return genericResponse;
	}

	private Integer ingestToDB(Channels channels, Configuration config)
			throws ApplicationException, Exception, ConfigurationException {
		Channel inputChannel;
		Integer channelId = 0;
		for (int i = 0; i < channels.getChannel().size(); i++) {
			inputChannel = channels.getChannel().get(i);
			if (isDefaultChannelExists(Integer.valueOf(inputChannel.getChannelId()),
					inputChannel.getDefaultChannelNumber().toString())) {
				throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
						new NestedParameters("defaultChannelNumber"));
			}
			for (int j = 0; j < inputChannel.getPackageList().getPackage().size(); j++) {
				if (Validator.isEmpty(channelManager
						.selectByPackageId(inputChannel.getPackageList().getPackage().get(j).intValue()))) {
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
							new NestedParameters("Package"));
				}
			}

			channelId = channelManager.createChannel(inputChannel, channels);
		}
		return channelId;
	}

	/**
	 * @param defaultChannelNumber
	 * @param channelId
	 * @param hibernateTemplate
	 * @return boolean
	 */
	private boolean isDefaultChannelExists(Integer channelId, String defaultChannelNumber) {

		boolean flag = false;
		List<Integer> channelIds = channelManager.retrieveByDefaultChannelNumber(defaultChannelNumber);

		if (!Validator.isEmptyCollection(channelIds)) {
			for (Integer id : channelIds) {
				if (!(id.equals(channelId))) {
					flag = true;
				}
			}
		}
		return flag;
	}

}
