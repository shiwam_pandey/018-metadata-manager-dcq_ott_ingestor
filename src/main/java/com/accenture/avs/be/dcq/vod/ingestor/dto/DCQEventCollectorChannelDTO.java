package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.io.Serializable;

/**
 * @author s.gudivada
 *
 */
public class DCQEventCollectorChannelDTO implements Serializable{
	private static final long serialVersionUID = -460053666626358917L;
	
	private String action;	
	private String contentType;
	private Integer channelId;
	private String extChannelId;
	private String channelType;
	private String platform;
	
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public Integer getChannelId() {
		return channelId;
	}
	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}
	public String getExtChannelId() {
		return extChannelId;
	}
	public void setExtChannelId(String extChannelId) {
		this.extChannelId = extChannelId;
	}
	public String getChannelType() {
		return channelType;
	}
	public void setChannelType(String channelType) {
		this.channelType = channelType;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
