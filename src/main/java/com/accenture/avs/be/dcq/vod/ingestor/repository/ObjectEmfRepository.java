package com.accenture.avs.be.dcq.vod.ingestor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.avs.persistence.technicalcatalogue.ObjectEmfEntity;
/**
 * @author karthik.vadla
 *
 */
@Repository
public interface ObjectEmfRepository extends JpaRepository<ObjectEmfEntity,Integer>{

	//void deleteByContentId(@Param("contentId")Integer contentId);
	@Modifying
	@Transactional
	void deleteByObjectTypeAndContentId(@Param("contentId") Integer contentId, @Param("objectType")Integer objectType);

}
