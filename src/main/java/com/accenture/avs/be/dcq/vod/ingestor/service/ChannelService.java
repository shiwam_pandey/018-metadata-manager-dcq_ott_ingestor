package com.accenture.avs.be.dcq.vod.ingestor.service;

import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;

public interface ChannelService {
	/**
	 * @param channelXml
	 * @param config
	 * @return
	 * @throws Exception
	 */
	GenericResponse upsertChannel(String channelXml,Configuration config) throws Exception;
}
