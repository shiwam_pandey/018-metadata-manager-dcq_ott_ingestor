package com.accenture.avs.be.dcq.vod.ingestor.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.ThreadContext;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.accenture.avs.be.dcq.vod.ingestor.dto.CategoryCMS;
import com.accenture.avs.be.dcq.vod.ingestor.manager.CategoryManager;
import com.accenture.avs.be.dcq.vod.ingestor.manager.CategoryMetadataManager;
import com.accenture.avs.be.dcq.vod.ingestor.manager.ContentCategoryManager;
import com.accenture.avs.be.dcq.vod.ingestor.manager.ContentMetadataManager;
import com.accenture.avs.be.dcq.vod.ingestor.manager.impl.DcqVodIngestorPropertiesManager;
import com.accenture.avs.be.dcq.vod.ingestor.processor.DcqVodIngestionPreProcessor;
import com.accenture.avs.be.dcq.vod.ingestor.repository.CategoryCMSRepository;
import com.accenture.avs.be.dcq.vod.ingestor.service.CategoryService;
import com.accenture.avs.be.dcq.vod.ingestor.utils.CategoryUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.dcq.vod.ingestor.utils.Validator;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.cache.Configurator;
import com.accenture.avs.be.framework.configuration.CacheConstants;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.be.framework.exception.ConfigurationException;
import com.accenture.avs.commons.lib.LoggerWrapper;
import com.accenture.avs.commons.lib.support.CommonConstants;
import com.accenture.avs.persistence.technicalcatalogue.CategoryCMSEntity;


/**
 * @author karthik.vadla
 *
 */
@Service
public class CategoryServiceImpl implements CategoryService {

	
	private static final LoggerWrapper log = new LoggerWrapper(CategoryServiceImpl.class);
	
	@Autowired
private	CategoryManager categoryManager;
	
	@Autowired
	private ContentCategoryManager contentCategoryManager;
	
	@Autowired
	private DcqVodIngestionPreProcessor dcqVodIngestionPreProcessor;
	
	@Autowired
	private DcqVodIngestorPropertiesManager dcqVodIngestorPropertiesManager;
	
	@Autowired
	private ContentMetadataManager contentMetadataManager;
	@Autowired
	private CategoryMetadataManager categoryMetadataManager;
	
	@Autowired
private Configurator configurator;
	
	@Autowired
	private CategoryCMSRepository categoryCMSRepository;
	
	/* (non-Javadoc)
	 * @see com.accenture.avs.be.dcq.vod.ingestor.service.CategoryService#unpublishCategory(java.lang.String, com.accenture.avs.be.framework.cache.Configuration)
	 */
	@Override
	public GenericResponse unpublishCategory(String categoryId, Configuration config) throws ApplicationException, ConfigurationException, JsonParseException, JsonMappingException, IOException {
		GenericResponse genericResponse = null;
		try {
			
			Validator.validateIntNotNegativeParameter("categoryId", categoryId, config);
			List<Integer> contentIds = categoryManager.unpublishCategory(Integer.parseInt(categoryId), config);
			List<Integer> categoryIds = new ArrayList<>();
			categoryIds.add(Integer.parseInt(categoryId));
			
			dcqVodIngestionPreProcessor.refreshCachesCreateIndexes(DcqVodIngestorConstants.CATEGORY, "","", config);
			categoryMetadataManager.processAndWriteCategories(categoryIds, DcqVodIngestorUtils.getTransactionNumber(),DcqVodIngestorUtils.currentTime(), "", "", config);
			
			if(contentIds!=null && !contentIds.isEmpty()) {
			dcqVodIngestionPreProcessor.refreshCachesCreateIndexes(DcqVodIngestorConstants.CONTENT, "","", config);
			contentMetadataManager.processandWriteContents(contentIds, DcqVodIngestorUtils.getTransactionNumber(),DcqVodIngestorUtils.currentTime(), "", "", config);
			}
		}catch(ApplicationException ae) {
			throw ae;
		}catch(Exception e) {
			throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR);
		}
		genericResponse = new GenericResponse(DcqVodIngestorConstants.ACN_200, DcqVodIngestorConstants.OK, "");
		return genericResponse;
	}
	
	@Override
	public GenericResponse upsertCategory(String csvCategory, Configuration config) throws ApplicationException, JsonParseException, JsonMappingException, ConfigurationException, IOException {
		// TODO Auto-generated method stub
		GenericResponse genericResponse = null;
		List<CategoryCMS> categoryCMSDtos = null;

		try {
			if (StringUtils.isBlank(csvCategory)) {
				throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
						new NestedParameters("category input data"));
			}
			String[] lines = csvCategory.split(System.getProperty(DcqVodIngestorConstants.LINE_SEPARATOR));
			if (lines != null && lines.length > 0) {
				
				categoryCMSDtos = readAllLines(lines);
				insertCategoriesIdPath(categoryCMSDtos);
				categoryManager.ingestCategory(categoryCMSDtos);
				log.logMessage("Successfully read Category data");
							
			
				//save data to ES
				
			} else {
				throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
						new NestedParameters("category input data"));
			}

			genericResponse = new GenericResponse(DcqVodIngestorConstants.ACN_200, DcqVodIngestorConstants.OK,
					DcqVodIngestorConstants.EMPTY);
			
		}
		catch(DataIntegrityViolationException cex) {
			//cex.printStackTrace();
			log.logError(cex);
			throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_3019_INVALID_PARAMETER,
					new NestedParameters("channelId or contentId or videoType"));
		}
		catch (ApplicationException ae) {
			log.logError(ae);
			throw ae;

		}
		return genericResponse;
	}
	
	public List<CategoryCMS> readAllLines(String[] lines) throws ApplicationException {  
		List<CategoryCMS> categoryCMSDTOs = new ArrayList<CategoryCMS>();
		log.logMessage("Reading input lines and convert to VLC data");
		try {
			Properties categoryMappings = dcqVodIngestorPropertiesManager.loadCategoryMappingProperties();
			Configuration config = configurator.getConfiguration(ThreadContext.get(CommonConstants.LogParamsL4j2.TN));
		    String categoryInput = "";
			for (String line : lines) {
				line = CategoryUtils.addCategoryInputLine(line, categoryMappings);
				String[] fields = null;
				if (line != null && line.indexOf(";") >= 0) {
					categoryInput =  line.split("\"")[1];
					fields = line.split(";");
					CategoryCMS categoryCMS=new CategoryCMS();
					categoryCMS.setCategoryId(getFieldValueAsInteger(DcqVodIngestorConstants.CATEGORYCONSTANTS.CATEGORY_ID, fields, categoryMappings,config));
					if (categoryCMS.getCategoryId() == null) {
						throw new ApplicationException(
								CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
								new NestedParameters(DcqVodIngestorConstants.CATEGORYCONSTANTS.CATEGORY_ID));
					}
					categoryCMS.setParentCategoryId(getFieldValueAsInteger(DcqVodIngestorConstants.CATEGORYCONSTANTS.PARENT_CATEGORY_ID, fields, categoryMappings,config));
					if (categoryCMS.getParentCategoryId() == null) {
						throw new ApplicationException(
								CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
								new NestedParameters(DcqVodIngestorConstants.CATEGORYCONSTANTS.PARENT_CATEGORY_ID));
					}
					categoryCMS.setName(getFieldValueAsString(DcqVodIngestorConstants.CATEGORYCONSTANTS.NAME, fields, categoryMappings));
					if (StringUtils.isEmpty(categoryCMS.getName())) {
						throw new ApplicationException(
								CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
								new NestedParameters(DcqVodIngestorConstants.CATEGORYCONSTANTS.NAME));
					}
                    Validator.checkParameterNameLength(DcqVodIngestorConstants.CATEGORYCONSTANTS.NAME, categoryCMS.getName(), DcqVodIngestorConstants.CATEGORYCONSTANTS.MAX_LENGTH_NAME, config);
					categoryCMS.setCategoryType(getFieldValueAsString(DcqVodIngestorConstants.CATEGORYCONSTANTS.TYPE, fields, categoryMappings));
					if (StringUtils.isEmpty(categoryCMS.getCategoryType())) {
						throw new ApplicationException(
								CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
								new NestedParameters(DcqVodIngestorConstants.CATEGORYCONSTANTS.TYPE));
					}
					Validator.checkParameterValidityRange("categoryType", DcqVodIngestorConstants.ALLOWED_CATEGORY_TYPES, categoryCMS.getCategoryType(), config);
					
					categoryCMS.setIsVisible(getFieldValueAsString(DcqVodIngestorConstants.CATEGORYCONSTANTS.IS_VISIBLE, fields, categoryMappings));
					if (StringUtils.isEmpty(categoryCMS.getIsVisible())) {
						throw new ApplicationException(
								CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
								new NestedParameters(DcqVodIngestorConstants.CATEGORYCONSTANTS.IS_VISIBLE));
					}
					Validator.checkParameterValidityRange("IsVisible", DcqVodIngestorConstants.ALLOWED_FLAGS, categoryCMS.getIsVisible(), config);

					categoryCMS.setChannelCategory(getFieldValueAsString(DcqVodIngestorConstants.CATEGORYCONSTANTS.CATALOG_IDENTIFIER, fields, categoryMappings));
					/*if (categoryCMS.getChannelCategory() == null) {
						throw new ApplicationException(
								CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
								new NestedParameters(DcqVodIngestorConstants.CATEGORYCONSTANTS.CATALOG_IDENTIFIER));
					}*/
					categoryCMS.setOrderId(getFieldValueAsLong(DcqVodIngestorConstants.CATEGORYCONSTANTS.ORDER_ID, fields, categoryMappings,config));
					if (categoryCMS.getOrderId() == null) {
						throw new ApplicationException(
								CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
								new NestedParameters(DcqVodIngestorConstants.CATEGORYCONSTANTS.ORDER_ID));
					}
					categoryCMS.setAdult(getFieldValueAsString(DcqVodIngestorConstants.CATEGORYCONSTANTS.ADULT, fields, categoryMappings));
					if (StringUtils.isEmpty(categoryCMS.getAdult())) {
						throw new ApplicationException(
								CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
								new NestedParameters(DcqVodIngestorConstants.CATEGORYCONSTANTS.ADULT));
					}
					Validator.checkParameterValidityRange("isAdult", DcqVodIngestorConstants.ALLOWED_FLAGS, categoryCMS.getAdult(), config);

					categoryCMS.setExternalId(getFieldValueAsString(DcqVodIngestorConstants.CATEGORYCONSTANTS.EXTERNAL_ID, fields, categoryMappings));
					if (StringUtils.isEmpty(categoryCMS.getExternalId())) {
						throw new ApplicationException(
								CacheConstants.MessageKeys.ERROR_BE_ACTION_3000_MISSING_PARAMETER,
								new NestedParameters(DcqVodIngestorConstants.CATEGORYCONSTANTS.EXTERNAL_ID));
					}
					categoryCMS.setContentOrderType(getFieldValueAsString(DcqVodIngestorConstants.CATEGORYCONSTANTS.CONTENT_ORDER_TYPE, fields, categoryMappings));
					categoryCMS.setContentId(getFieldValueAsLong(DcqVodIngestorConstants.CATEGORYCONSTANTS.CONTENT_ID, fields, categoryMappings,config));
					if(StringUtils.isNotBlank(categoryCMS.getContentOrderType())){
						Validator.checkParameterValidityRange("contentOrderType", DcqVodIngestorConstants.ALLOWED_CONTENTORDERTYPES, categoryCMS.getContentOrderType(), config);				
					}
									
					categoryCMS.setAsNew("N");
					if (getFieldValueAsString(DcqVodIngestorConstants.CATEGORYCONSTANTS.CATEGORY_INPUT, fields, categoryMappings)!=null){
						categoryCMS.setSourceFile(categoryInput.getBytes());
						log.logMessage("CategoryFieldSetMapper:mapFieldSet: CATEGORY_INPUT:"+categoryInput);
					}
					
                 categoryCMSDTOs.add(categoryCMS);
				}
			}
		} catch (ApplicationException e) {
			log.logMessage(e.getMessage());
			 throw e;
		} catch (Exception e) {
			log.logMessage(e.getMessage());
			throw new ApplicationException(CacheConstants.MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR);
		}
		return categoryCMSDTOs;
	}

	/**
	 * returns field value as string as per mapping provided
	 * 
	 * @param key
	 * @param fields
	 * @param vlcContentMappings
	 * @return
	 */
	String getFieldValueAsString(String key, String[] fields, Properties vlcContentMappings) {

		if (vlcContentMappings.containsKey(key)
				&& fields.length > Integer.parseInt(vlcContentMappings.get(key).toString())) {
			return fields[Integer.parseInt(vlcContentMappings.get(key).toString())];
		}

		return null;
	}

	/**
	 * returns field value as long as per mapping provided
	 * 
	 * @param key
	 * @param fields
	 * @param vlcContentMappings
	 * @return
	 * @throws ConfigurationException 
	 * @throws ApplicationException 
	 */
	Integer getFieldValueAsInteger(String key, String[] fields, Properties vlcContentMappings,Configuration config) throws ApplicationException, ConfigurationException {

		if (vlcContentMappings.containsKey(key)
				&& fields.length > Integer.parseInt(vlcContentMappings.get(key).toString())
				&& fields[Integer.parseInt(vlcContentMappings.get(key).toString())] != null
				&& fields[Integer.parseInt(vlcContentMappings.get(key).toString())].trim().length() > 0) {
			
			return Validator.checkParameterValue(key,fields[Integer.parseInt(vlcContentMappings.get(key).toString())],config);
		}

		return null;
	}
	
	/**
	 * returns field value as long as per mapping provided
	 * 
	 * @param key
	 * @param fields
	 * @param CategoryMappings
	 * @return
	 * @throws ConfigurationException 
	 * @throws ApplicationException 
	 */
	Long getFieldValueAsLong(String key, String[] fields, Properties vlcContentMappings,Configuration config) throws ApplicationException, ConfigurationException {

		if (vlcContentMappings.containsKey(key)
				&& fields.length > Integer.parseInt(vlcContentMappings.get(key).toString())
				&& fields[Integer.parseInt(vlcContentMappings.get(key).toString())] != null
				&& fields[Integer.parseInt(vlcContentMappings.get(key).toString())].trim().length() > 0) {

			return Validator.validateLongParameter(key,fields[Integer.parseInt(vlcContentMappings.get(key).toString())],config);
		}

		return null;
	}

	
	public void insertCategoriesIdPath(List<CategoryCMS> categories) {
				
		Long categoryId =0L;
		Long parentCategoryId =0L;
		Long pParentCategoryId =0L;
		Long pCategoryId=0L;
		String idRoot ;
		String externalIdRoot  ;
		StringBuffer externalIdRootBuf; // = new StringBuffer(); // fix for ADT 15096
		StringBuffer rootToLeafIdPathBuf; // = new StringBuffer();
		StringBuffer rootToLeafExternalIdPathBuf; // = new StringBuffer();
		
		for (int y=0;y<categories.size();y++){
			// fix for ADT 15096 : start
			externalIdRootBuf = new StringBuffer(); 
			rootToLeafIdPathBuf = new StringBuffer();
			rootToLeafExternalIdPathBuf = new StringBuffer();
			// fix for ADT 15096 : end
			CategoryCMS category = categories.get(y);
			if (Objects.nonNull(category)) {
				categoryId = category.getCategoryId().longValue();
				parentCategoryId = category.getParentCategoryId().longValue();
				idRoot = categoryId.toString();
				externalIdRoot = category.getExternalId();
				if (categoryId.equals(parentCategoryId)) {
					log.logMessage("----root id is == " + idRoot + "   ----");
					log.logMessage("----External root id is == " + externalIdRoot + "   ----");
					category.setIdPath(idRoot);
					category.setExternalidPath(externalIdRoot);
				} else {
					pParentCategoryId = parentCategoryId;
					externalIdRootBuf.append(externalIdRoot);
					do {
						idRoot += ";" + pParentCategoryId;
						CategoryCMSEntity cat = categoryCMSRepository.findByCategoryId(pParentCategoryId.intValue());
						if (cat == null) {
							break;
						} else {

							pParentCategoryId = cat.getParent_category_id().longValue();
							pCategoryId = cat.getCategoryId().longValue();
							externalIdRootBuf.append(";" + cat.getExternalId());
						}
					} while (!pCategoryId.equals(pParentCategoryId));

					/*
					 * logger.info("----root id is == "+idRoot+"   ----");
					 * logger.info("----External root id is == "
					 * +externalIdRoot+"   ----");
					 */
					// to correctly valorized starting from root category to
					// leaf category e.g ROOT;NODE;NODE;LEAF for ID path
					externalIdRoot = externalIdRootBuf.toString();
					String[] words = idRoot.split(";");
					String rootToLeafIdPath = "";

					rootToLeafIdPathBuf.append(rootToLeafIdPath);
					for (int i = 0; i < words.length; i++) {
						String word = words[words.length - 1 - i];
						rootToLeafIdPathBuf.append(word + ";");
						// rootToLeafIdPath+=word+";";
					}
					rootToLeafIdPath = rootToLeafIdPathBuf.toString();
					log.logMessage("Root to leaf id path is " + rootToLeafIdPath);
					// to correctly valorized starting from root category to
					// leaf category e.g ROOT;NODE;NODE;LEAF for External ID
					// path
					words = externalIdRoot.split(";");
					String rootToLeafExternalIdPath = "";
					rootToLeafExternalIdPathBuf.append(rootToLeafExternalIdPath);
					for (int i = 0; i < words.length; i++) {
						String word = words[words.length - 1 - i];
						rootToLeafExternalIdPathBuf.append(word + ";");
						// rootToLeafExternalIdPath+=word+";";
					}
					rootToLeafExternalIdPath = rootToLeafExternalIdPathBuf.toString();
					log.logMessage("Root to leaf External id path is " + rootToLeafExternalIdPath);
					category.setIdPath(rootToLeafIdPath.substring(0, rootToLeafIdPath.length() - 1));
					category.setExternalidPath(
							rootToLeafExternalIdPath.substring(0, rootToLeafExternalIdPath.length() - 1));
					externalIdRoot = null;
					idRoot = null;
				}
			}
		}
	}
}
