package com.accenture.avs.be.dcq.vod.ingestor.processor.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.accenture.avs.be.dcq.vod.ingestor.dto.ChannelIdPlaylistDateDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionReaderResponseDTO;
import com.accenture.avs.be.dcq.vod.ingestor.dto.DcqVodIngestionWriterRequestDTO;
import com.accenture.avs.be.dcq.vod.ingestor.executor.DcqThreadPoolExecutor;
import com.accenture.avs.be.dcq.vod.ingestor.manager.ContentMetadataManager;
import com.accenture.avs.be.dcq.vod.ingestor.processor.DcqVodIngestionProcessor;
import com.accenture.avs.be.dcq.vod.ingestor.utils.CacheConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorConstants;
import com.accenture.avs.be.dcq.vod.ingestor.utils.DcqVodIngestorUtils;
import com.accenture.avs.be.dcq.vod.ingestor.writer.DcqVodIngestionWriter;
import com.accenture.avs.be.framework.cache.Configuration;
import com.accenture.avs.be.framework.configuration.CacheConstants.MessageKeys;
import com.accenture.avs.be.framework.exception.ApplicationException;
import com.accenture.avs.be.framework.exception.ApplicationException.NestedParameters;
import com.accenture.avs.commons.lib.LoggerWrapper;

/**
 * @author karthik.vadla
 *
 */
@Component
public class DcqVodIngestionProcessorImpl implements DcqVodIngestionProcessor {
	private static final LoggerWrapper log = new LoggerWrapper(DcqVodIngestionProcessorImpl.class);

	@Autowired
	private ApplicationContext context;
	@Autowired
private	ContentMetadataManager contentMetadataManager;
	@Autowired
	private DcqVodIngestionWriter dcqVodIngestionWriter;
	@Override
	public List<DcqVodIngestionWriterRequestDTO> processMetadataConstructESDocs(DcqVodIngestionReaderResponseDTO readerResponseDTO, String assetType,
			String transactionNumber, String mode, String indexDate, Configuration config) throws ApplicationException {
		List<DcqVodIngestionWriterRequestDTO> dcqVodIngestionWriterRequestDTOs = null;
		Integer processorThreads = null;
		try {
			log.logMessage("Process/Retrieving Metadata for {} Ingestor and constructing the ES Docs(Json)." , assetType );
			dcqVodIngestionWriterRequestDTOs = new ArrayList<DcqVodIngestionWriterRequestDTO>();
			String processorThreadsValue = config.getConstants().getValue(CacheConstants.DCQ_VOD_INGESTOR_PROCESSOR_THREADS);
			if (DcqVodIngestorUtils.isEmpty(processorThreadsValue)) {
				processorThreads = DcqVodIngestorConstants.DEFAULT_NUM_OF_PROCESSOR_THREADS;
			} else {
				processorThreads = Integer.parseInt(processorThreadsValue);
			}
			log.logMessage("Number of Threads created for {} Processor: {}" , assetType , processorThreads);

			ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(processorThreads);
			List<Future<List<DcqVodIngestionWriterRequestDTO>>> futureTasks = new ArrayList<Future<List<DcqVodIngestionWriterRequestDTO>>>();
			if (assetType.equals(DcqVodIngestorConstants.CONTENT)) {
				Integer i = Integer.valueOf(config.getConstants().getValue(CacheConstants.DCQ_VOD_ASSETS_COUNT_FOR_THREAD));
				log.logMessage("Number of Contents for a Thread are: {}" , i);
				List<Integer> assetIds = readerResponseDTO.getAssetIds();
				for (Integer start = 0; start < assetIds.size(); start += i) {
					Integer end = Math.min(start + i, assetIds.size());
					List<Integer> subList = assetIds.subList(start, end);
					try {
						log.logMessage("SubList for Content: {}" , subList.toString());
						DcqThreadPoolExecutor dcqThreadPoolExecutor = null;
						dcqThreadPoolExecutor = context.getBean(DcqThreadPoolExecutor.class);
						dcqThreadPoolExecutor.setAssetIdList(subList);
						dcqThreadPoolExecutor.setAssetType(assetType);
						dcqThreadPoolExecutor.setTransactionNumber(transactionNumber);
						dcqThreadPoolExecutor.setStartTime(readerResponseDTO.getStartTime());
						dcqThreadPoolExecutor.setMode(mode);
						dcqThreadPoolExecutor.setIndexDate(indexDate);
						dcqThreadPoolExecutor.setConfig(config);
						Future<List<DcqVodIngestionWriterRequestDTO>> task1 = executor.submit(dcqThreadPoolExecutor);
						futureTasks.add(task1);
					} catch (Exception e) {
						log.logError(e);
					}
				}
			} else if (assetType.equals(DcqVodIngestorConstants.CATEGORY) || assetType.equals(DcqVodIngestorConstants.VODCHANNEL)) {
				Integer i = Integer.valueOf(config.getConstants().getValue(CacheConstants.DCQ_VOD_ASSETS_COUNT_FOR_THREAD));
				log.logMessage("Number of Contents for a Thread are: {}" , i);
				List<Integer> assetIds = readerResponseDTO.getAssetIds();
				for (Integer start = 0; start < assetIds.size(); start += i) {
					Integer end = Math.min(start + i, assetIds.size());
					List<Integer> subList = assetIds.subList(start, end);
					 DcqThreadPoolExecutor dcqThreadPoolExecutor= null;
					dcqThreadPoolExecutor = context.getBean(DcqThreadPoolExecutor.class);
					dcqThreadPoolExecutor.setTransactionNumber(transactionNumber);
					dcqThreadPoolExecutor.setAssetIdList(subList);
					dcqThreadPoolExecutor.setAssetType(assetType);
					dcqThreadPoolExecutor.setStartTime(readerResponseDTO.getStartTime());
					dcqThreadPoolExecutor.setMode(mode);
					dcqThreadPoolExecutor.setIndexDate(indexDate);
					dcqThreadPoolExecutor.setConfig(config);
					 
					Future<List<DcqVodIngestionWriterRequestDTO>> task1 = executor.submit(dcqThreadPoolExecutor);
					futureTasks.add(task1);
				}
			} else if (assetType.equals(DcqVodIngestorConstants.VLCCONTENT)) {
				Integer i = Integer.valueOf(config.getConstants().getValue(CacheConstants.DCQ_VOD_ASSETS_COUNT_FOR_THREAD));
				List<ChannelIdPlaylistDateDTO> channelIdPlaylistDateDTOs = readerResponseDTO.getChannelIdPlaylistDateDTOList();
					
					for (Integer start = 0; start < channelIdPlaylistDateDTOs.size(); start += i) {
						Integer end = Math.min(start + i, channelIdPlaylistDateDTOs.size());
						List<ChannelIdPlaylistDateDTO> subList = channelIdPlaylistDateDTOs.subList(start, end);
						 DcqThreadPoolExecutor dcqThreadPoolExecutor= null;
					dcqThreadPoolExecutor = context.getBean(DcqThreadPoolExecutor.class);
					dcqThreadPoolExecutor.setTransactionNumber(transactionNumber);
					dcqThreadPoolExecutor.setChannelIdPlayList(subList);
					dcqThreadPoolExecutor.setAssetType(assetType);
					dcqThreadPoolExecutor.setStartTime(readerResponseDTO.getStartTime());
					dcqThreadPoolExecutor.setMode(mode);
					dcqThreadPoolExecutor.setIndexDate(indexDate);
					dcqThreadPoolExecutor.setConfig(config);
					
					Future<List<DcqVodIngestionWriterRequestDTO>> task1 = executor.submit(dcqThreadPoolExecutor);
					futureTasks.add(task1);
				}
			}
			for (Future<List<DcqVodIngestionWriterRequestDTO>> task1 : futureTasks) {
				try {
					List<DcqVodIngestionWriterRequestDTO> dcqVodIngestionWriterRequestDTOsList = task1.get();
					if (null != dcqVodIngestionWriterRequestDTOsList && !dcqVodIngestionWriterRequestDTOsList.isEmpty()) {
						dcqVodIngestionWriterRequestDTOs.addAll(dcqVodIngestionWriterRequestDTOsList);
					}
				} catch (InterruptedException e) {
					log.logMessage(e.getMessage());
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR, new NestedParameters(e.getMessage()));
				} catch (ExecutionException e) {
					log.logError(e);
					throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR, new NestedParameters(e.getMessage()));
				}
			}
			executor.shutdown();
		} catch (Exception e) {
			log.logError(e);
			throw new ApplicationException(MessageKeys.ERROR_BE_ACTION_300_GENERIC_ERROR, new NestedParameters(e.getMessage()));
		}
		return dcqVodIngestionWriterRequestDTOs;
	}


	@Override
	public void processSwitchAlias(Configuration config) throws ApplicationException {
		dcqVodIngestionWriter.switchAlias(config);
		
	}

	@Override
	public void processRollbackAlias(Configuration config) throws ApplicationException {
		dcqVodIngestionWriter.rollbackAlias(config);
	}


	
	

}
