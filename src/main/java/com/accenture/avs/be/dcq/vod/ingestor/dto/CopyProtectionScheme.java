package com.accenture.avs.be.dcq.vod.ingestor.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * @author naga.sireesha.meka
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CopyProtectionScheme implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String schemeName;
	private String securityCode;
	private List<CopyProtectionSchemeOption> options=new ArrayList<>();
	private String vodDefaultOption;
	private String ChannelDefaultOption;
	private String programDefaultOption;
	
	public CopyProtectionScheme()
	{
		super();
	}
	public String getSchemeName() {
		return schemeName;
	}
	public void setSchemeName(String schemeName) {
		this.schemeName = schemeName;
	}
	public List<CopyProtectionSchemeOption> getOptions() {
		return options;
	}
	public void setOptions(List<CopyProtectionSchemeOption> options) {
		this.options = options;
	}
	public String getVodDefaultOption() {
		return vodDefaultOption;
	}
	public void setVodDefaultOption(String vodDefaultOption) {
		this.vodDefaultOption = vodDefaultOption;
	}
	
	public String getChannelDefaultOption() {
		return ChannelDefaultOption;
	}
	public void setChannelDefaultOption(String channelDefaultOption) {
		ChannelDefaultOption = channelDefaultOption;
	}
	
	public String getProgramDefaultOption() {
		return programDefaultOption;
	}
	public void setProgramDefaultOption(String programDefaultOption) {
		this.programDefaultOption = programDefaultOption;
	}
	public String getSecurityCode() {
		return securityCode;
	}
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
	
}
