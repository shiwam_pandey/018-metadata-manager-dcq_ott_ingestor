package com.accenture.avs.be.dcq.vod.ingestor.content.dto;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class Playlist implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value="Unique Identifier of the content",required=true,example="2346")
	private Integer contentId;
	@ApiModelProperty(value= "start time of the content",required=true,example="2019-02-10 10:10:00")
	private String startTime;
	@ApiModelProperty(value= "End time of the content",required=true,example="2019-02-10 11:10:00")
	private String endTime;
	@ApiModelProperty(value= "Type of the video in the content",required=true,example="SD")
	private String videoType;
	public Integer getContentId() {
		return contentId;
	}
	public void setContentId(Integer contentId) {
		this.contentId = contentId;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getVideoType() {
		return videoType;
	}
	public void setVideoType(String videoType) {
		this.videoType = videoType;
	}
}
