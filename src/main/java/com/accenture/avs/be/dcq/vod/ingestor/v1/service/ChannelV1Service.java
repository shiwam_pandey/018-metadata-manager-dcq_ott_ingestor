package com.accenture.avs.be.dcq.vod.ingestor.v1.service;

import com.accenture.avs.be.dcq.vod.ingestor.dto.Channel;
import com.accenture.avs.be.framework.bean.GenericResponse;
import com.accenture.avs.be.framework.cache.Configuration;

public interface ChannelV1Service {
	/**
	 * @param channelXml
	 * @param config
	 * @return
	 * @throws Exception
	 */
	GenericResponse upsertChannel(Channel channelXml,Configuration config) throws Exception;
}


