package com.accenture.avs.be.dcq.vod.ingestor.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.accenture.avs.persistence.technicalcatalogue.TechnicalPackageEntity;

@Repository
public interface TechnicalPackageRepository extends  JpaRepository<TechnicalPackageEntity,Integer>{

public List<TechnicalPackageEntity> selectByPackageId(@Param("packageId")Integer packageId);

public List<Integer> retrieveTechPackagesNotRVodByContentId(@Param("contentId")Integer contentId);

public Long retrievePackageIdByName(@Param("packageName")String pkgName);

public List<TechnicalPackageEntity> retrievePackageListByName(@Param("packageName")String pkgName);

public List<TechnicalPackageEntity> retrievePackagesNotRvod();
public List<TechnicalPackageEntity> retrievePackagesSVOD();

public List<Object[]> retrievePackagesNotRvodByPackageIds(@Param("packageIds")List<Integer> packageIds);

}
