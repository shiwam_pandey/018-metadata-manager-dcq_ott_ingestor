package com.accenture.avs.be.dcq.vod.ingestor.dto;

import io.swagger.annotations.ApiModelProperty;

public class LanguageDTO {
	
	@ApiModelProperty(required = true, value = "Unique Identifier of Language", example="1")
	private Integer languageId;
	
	@ApiModelProperty(required = true, value = "Language code based on the standard ISO 639-2-alpha3",example ="eng")
	private String languageCode;
	
	@ApiModelProperty(required = false, value = "Language name" ,example = "English")
	private String languageName;
	
	@ApiModelProperty(required = true, value = "Flag Y/N for identifying if a language is active or not active.",dataType = "string",example = "Y")
	private char isActive;
	
	@ApiModelProperty(required = true, value = "Flag Y/N for identifying if a language is the default in the system . By default value =N",dataType = "string",example = "Y")
	private char isDefault;

	public Integer getLanguageId() {
		return languageId;
	}

	public void setLanguageId(Integer languageId) {
		this.languageId = languageId;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getLanguageName() {
		return languageName;
	}

	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

	public char getIsActive() {
		return isActive;
	}

	public void setIsActive(char isActive) {
		this.isActive = isActive;
	}

	public char getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(char isDefault) {
		this.isDefault = isDefault;
	}
	
	

}
